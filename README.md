## CATL - MHP - OEE

## eclipse 开发 ##
mvn eclipse:eclipse

## idea 开发 ##
mvn idea:idea

## 打包 开发环境 ##
mvn clean package -Pdev -Dmaven.test.skip=true
## 打包 测试环境 ##
mvn clean package -Pqty -Dmaven.test.skip=true
## 打包 生产环境 ##
mvn clean package -Ppro -Dmaven.test.skip=true

注:打包生产的时候需要修改version

## jetty run
1. 开发环境 mvn jetty:run -Pdev
2. 测试环境 mvn jetty:run -Pqty

## jetty debug
1. 需要先配置一个环境变量 SET MAVEN_OPTS=-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=y
2. mvn jetty:run -Pqty
3. ide 设置一个对应的remote监听端口


## 开发机
* 应用地址(web端)	http://172.26.164.10:50100/mhp-oee/htmls/index.html
* 应用地址帐号/密码	00071858/init1234
* 客户端调用接口帐号/密码	int_client/test123
* ERP调用接口帐号/密码	int_erp/test123
* 数据库服务器（HANA）地址	IP:172.26.164.10  Instance No:00
* HANA帐号/密码	MHP_OEE/Catl-2017
* 应用服务器地址	172.26.164.10
* 应用服务器帐号/密码	root/Catl-1234
* 部署帐号/密码	site_admin/catl00

## 测试机
* 应用地址(web端)	http://172.26.164.11:50000/mhp-oee/htmls/index.html
* 应用地址帐号/密码	site_admin/init111
* 客户端调用接口帐号/密码	int_client/test123
* ERP调用接口帐号/密码	int_erp/test123
* 数据库服务器（HANA）地址	172.26.164.12
* HANA帐号/密码	MHP_OEE/Atl-12345
* 应用服务器地址	172.26.164.11
* 应用服务器帐号/密码	root/nddon1234  system/init-1234

## 生产机
* 应用地址(web端)	http://172.26.120.31:50200/mhp-oee/htmls/index.html
* 应用地址帐号/密码	71858/atl123456
* 客户端调用接口帐号/密码	int_client/catl1234
* ERP调用接口帐号/密码	int_erp/test123
* 数据库服务器（HANA）地址	172.26.104.12
* HANA帐号/密码	MHP_OEE/Catl-2017

## 部署说明
1. ssh连接服务器
2. chmod 777 -R mhp-oee.war
3. telnet localhost 50008 (端口看实际情况)
4. sap 用户名/密码 (system/init-1234)
5. lsc
6. add deploy
7. deploy /install/OEE_WAR/DeployByJenkins/mhp-oee.war version_rule=all


## 功能设计书
\\nd-share\ATL-Department\IT\SAP CC\02 生产运营\05 Andon\CATL\05-系统实现\01-功能设计说明书

## 惩罚规则 如果出现以下三种情况 请团队成员吃饭 o(∩_∩)o
1. 把冲突文件提交到服务器
2. 把别人的代码覆盖了提交到服务器
3. 提交的代码导致启动不了

## andon生产版本信息查看接口 /mhp-oee/web/public/version
注:在打包到生产环境的时候把对应的pom.xml的version在原来的基础上+1



## 祝大家玩的愉快 ##
