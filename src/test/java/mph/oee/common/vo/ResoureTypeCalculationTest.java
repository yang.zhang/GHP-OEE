package mph.oee.common.vo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import mhp.oee.web.angular.reasoncode.alert.map.management.ReasonCodeAlertUploadVO;
import mhp.oee.web.angular.reasoncode.alert.map.management.ResourceTypeCalculation;
import mhp.oee.web.angular.reasoncode.alert.map.management.excel.parser.ExcelRow;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
public class ResoureTypeCalculationTest {

    @Test
    public void testCRUD() {
        ReasonCodeAlertUploadVO vo = new ReasonCodeAlertUploadVO();

        ExcelRow aExcelRow = new ExcelRow();
        aExcelRow.setResourceType("AAA");
        aExcelRow.setDec("TestA");
        aExcelRow.setAlertCode("Red");
        aExcelRow.setReasonCode("Broken");


        ExcelRow bExcelRow = new ExcelRow();
        bExcelRow.setResourceType("BBB");
        bExcelRow.setDec("TestB");
        bExcelRow.setAlertCode("Grean");
        bExcelRow.setReasonCode("Broken");

        ExcelRow cExcelRow = new ExcelRow();
        cExcelRow.setResourceType("AAA");
        cExcelRow.setDec("TestAA");
        cExcelRow.setAlertCode("Grean");
        cExcelRow.setReasonCode("Good");

        ExcelRow dExcelRow = new ExcelRow();
        dExcelRow.setResourceType("AAA");
        dExcelRow.setDec("TestAAA");
        dExcelRow.setAlertCode("red");
        dExcelRow.setReasonCode("Bad");

        ExcelRow eExcelRow = new ExcelRow();
        eExcelRow.setResourceType("CCC");
        eExcelRow.setDec("TestAAA");
        eExcelRow.setAlertCode("red");
        eExcelRow.setReasonCode("Bad");

        vo.getRows().add(aExcelRow);
        vo.getRows().add(bExcelRow);
        vo.getRows().add(cExcelRow);
        vo.getRows().add(dExcelRow);
        vo.getRows().add(eExcelRow);


        vo.getResTypeCalculation().add(new ResourceTypeCalculation("AAA"));
        vo.getResTypeCalculation().add(new ResourceTypeCalculation("AAA"));
        vo.getResTypeCalculation().add(new ResourceTypeCalculation("BBB"));
        vo.getResTypeCalculation().add(new ResourceTypeCalculation("AAA"));
        vo.getResTypeCalculation().add(new ResourceTypeCalculation("AAA"));
        vo.getResTypeCalculation().add(new ResourceTypeCalculation("CCC"));

        vo.calculationExcelResourceType();

        Assert.assertEquals(3, vo.getResTypeCalculation().size());

        for(ResourceTypeCalculation calculation : vo.getResTypeCalculation()) {
            if(calculation.getResourceType().equals("AAA")) {
                Assert.assertEquals(new Integer(3), calculation.getExcelNumber());
            }
            if(calculation.getResourceType().equals("BBB")) {
                Assert.assertEquals(new Integer(1), calculation.getExcelNumber());
            }
        }
    }

}
