package mhp.oee;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

/**
 * Created by DingLJ on 2017/5/10 010.
 */
public class Base64Test {


    @Test
    public void test_base64(){

        //int_client/test123

        String str = "int_client:test123";

        String encode = Base64.encodeBase64String(str.getBytes());
        System.out.println(encode);

    }

}
