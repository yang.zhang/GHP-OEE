package mhp.oee.component;

import java.text.ParseException;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.common.handle.ReasonCodePriorityBOHandle;
import mhp.oee.dao.gen.ReasonCodePOMapper;
import mhp.oee.dao.gen.ReasonCodePriorityPOMapper;
import mhp.oee.po.gen.ReasonCodePO;
import mhp.oee.po.gen.ReasonCodePriorityPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ReasonCodePriorityVO;
import mhp.oee.web.angular.reasoncode.recognition.ReasonCodeRecognitionService;

/**
 * Created by LinZuK on 2016/7/29.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = { TestRollBackException.class })
public class ReasonCodeRecognitionServiceTest {

    @Resource
    ReasonCodeRecognitionService reasonCodeRecognitionService;
    @Resource
    ReasonCodePOMapper reasonCodePOMapper;
    @Resource
    ReasonCodePriorityPOMapper reasonCodePriorityPOMapper;

    @Test(expected = TestRollBackException.class)
    public void testCRUD() throws BusinessException, TestRollBackException, ParseException {
        try {
            ReasonCodePO po = new ReasonCodePO();
            po.setSite("TEST");
            po.setReasonCode("hehe");
            po.setHandle("ReasonCodeBO:TEST,hehe");
            reasonCodePOMapper.insert(po);
            po = reasonCodePOMapper.selectByPrimaryKey("ReasonCodeBO:TEST,hehe");
            Assert.assertTrue(po != null);

            // change reasonCodePriority
            Date now = new Date();
            Date afterNow=new Date(now.getTime()+ 300000);
            ReasonCodePriorityVO vo = new ReasonCodePriorityVO();
            vo.setReasonCodeBo(po.getHandle());
            vo.setPriority("A");
            vo.setSubPriority("1");
            vo.setInStartDateTime(Utils.datetimeToStr(afterNow));
            reasonCodeRecognitionService.changeReasonCodePriority(vo);
            ReasonCodePriorityPO xxx = reasonCodePriorityPOMapper
                    .selectByPrimaryKey(new ReasonCodePriorityBOHandle(po.getHandle(), "A", "1",
                            DateFormatUtils.format(afterNow, "yyyyMMdd.HHmmss")).getValue());
            Assert.assertTrue(xxx != null);

            // create RecognitionPattern
            /*
             * String activity="RULE_MANUSEL_R"; String reasonCodePriorityBO=
             * "ReasonCodePriorityBO:ReasonCodeBO:TEST,reasonCode,A,1,20160730.142813";
             * String site="TEST"; List<ActivityParamPO>
             * activityParamPos=reasonCodeRecognitionService.getActivityFParam(
             * "ActivityBO:RULE_MANUSEL_R","R"); List<ActivityParamVO>
             * activityParamVos=new LinkedList<ActivityParamVO>();
             * for(ActivityParamPO po:activityParamPos){ ActivityParamVO
             * activityParamVo=Utils.copyObjectProperties(po,
             * ActivityParamVO.class); activityParamVo.setParamSetValue("444");
             * activityParamVos.add(activityParamVo); }
             * reasonCodeRecognitionService.createRecognitionPattern(
             * reasonCodePriorityBO, activity, activityParamVos, site);
             */

            // update RecognitionPattern
            /*
             * String activity="RULE_MANUSEL_R"; String reasonCodePriorityBO=
             * "ReasonCodePriorityBO:ReasonCodeBO:TEST,reasonCode,A,1,20160730.142813";
             * String site="TEST"; List<ActivityParamPO>
             * activityParamPos=reasonCodeRecognitionService.getActivityFParam(
             * "ActivityBO:RULE_MANUSEL_R","R"); List<ActivityParamVO>
             * activityParamVos=new LinkedList<ActivityParamVO>();
             * for(ActivityParamPO po:activityParamPos){ ActivityParamVO
             * activityParamVo=Utils.copyObjectProperties(po,
             * ActivityParamVO.class); activityParamVo.setParamSetValue("5555");
             * activityParamVos.add(activityParamVo); }
             * reasonCodeRecognitionService.updateRecognitionPattern(
             * reasonCodePriorityBO, site, activity, "CHANGE_ACTIVITY",
             * activityParamVos);
             */
            // delete
            // reasonCodeRecognitionService.deleteReasonCodeRecognition("ReasonCodePriorityBO:ReasonCodeBO:TEST_R,SD09,B,2,20160701.000000");

            throw new TestRollBackException();
        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        }
    }
}
