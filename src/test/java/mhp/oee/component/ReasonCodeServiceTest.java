package mhp.oee.component;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.vo.ReasonCodeVO;
import mhp.oee.web.angular.reasoncode.management.ReasonCodeUiService;
import mhp.oee.common.exception.TestRollBackException;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = { TestRollBackException.class })
public class ReasonCodeServiceTest {

    @Resource
    ReasonCodeUiService reasonCodeUiService; 

    @Test (expected = TestRollBackException.class)
    public void testCRUD() throws TestRollBackException {
        try {
            // ---------------------------ReasonCode Start----------------------------
            ReasonCodeVO vo1 = new ReasonCodeVO();
            vo1.setSite("JUNIT");
            vo1.setReasonCode("JUNIT_RSCODE_01");
            vo1.setReasonCodeGroup("JUNIT_RSCODEGROUP_01");
            vo1.setDescription("JUNIT_REASONCODE_01");
            vo1.setEnabled("true");
            vo1.setViewActionCode('C');
            
            List<ReasonCodeVO> reasonCodeVOs = new ArrayList<ReasonCodeVO>();
            reasonCodeVOs.add(vo1);
            
            //test create
            reasonCodeUiService.changeReasonCode(reasonCodeVOs);          
            //test select
            List<ReasonCodeVO> lists = reasonCodeUiService.readAllResonCodeAll("JUNIT", "JUNIT_RSCODEGROUP_01", "true");
            System.out.println("ReasonCodeVO --> " + lists);
            if (lists != null && lists.size() > 0) {
                vo1 = lists.get(0);
                //test update
                vo1.setDescription("CHANGE");
                vo1.setViewActionCode('U');
                reasonCodeVOs = new ArrayList<ReasonCodeVO>();
                reasonCodeVOs.add(vo1);
                reasonCodeUiService.changeReasonCode(reasonCodeVOs);
            }
            // ---------------------------ReasonCode End----------------------------
            
           throw new TestRollBackException();

        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        }
    }
}
