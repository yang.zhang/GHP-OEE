package mhp.oee.component;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.component.production.StdCycleTimeComponent;
import mhp.oee.po.gen.StdCycleTimePO;
import mhp.oee.utils.Utils;
import mhp.oee.web.angular.std.cycletime.management.StdCycleTimeService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = {TestRollBackException.class})
public class CycleTimeComponentTest {

    @Resource
    StdCycleTimeService cycleTimeService;

    @Resource
    StdCycleTimeComponent cycleTimeComponent;

    @Test(expected=TestRollBackException.class)
    public void testCRUD() throws TestRollBackException {

        try {
            StdCycleTimePO vo = this.createVO("001", "ITEM", "DEVICE", "1.2", "7/18/2016");
            vo.setModifiedDateTime(new Date());
            cycleTimeComponent.createStdCycleTime(vo);

            List<StdCycleTimePO> readStdCycleTime = cycleTimeComponent.readStdCycleTime("001", "DEVICE", "ITEM", Utils.getInfiniteDate());
            System.out.println(">>>>>>>>>>>> ......... " + readStdCycleTime.size());
            Assert.assertNotEquals("0", readStdCycleTime.size());

            cycleTimeComponent.createStdCycleTime(this.createVO("001", "ITEM", "DEVICE", "1.4", "12/29/2016"));

            List<StdCycleTimePO> readStdCycleTime2 = cycleTimeComponent.readStdCycleTime("001", "DEVICE", "ITEM", null);

            Assert.assertEquals(2, readStdCycleTime2.size());

            throw new TestRollBackException();

        } catch (BusinessException e) {
            Assert.fail(e.getMessage()); }
         catch (ParseException e) {
            Assert.fail(e.getMessage());
        }

    }

    private StdCycleTimePO createVO(String site, String item, String deviceNo,
                String ppm, String startDate) throws ParseException {
        StdCycleTimePO vo = new StdCycleTimePO();
        vo.setSite(site);
        vo.setItem(item);
        vo.setOperation("*");
        vo.setResrce(deviceNo);

        vo.setPpmTheory(new BigDecimal(ppm));
        vo.setStdCycleTime(new BigDecimal(Utils.getCycleTime(ppm)));
        vo.setEndDate(Utils.getInfiniteDate());



        vo.setStrt(startDate);
        vo.setStartDate(Utils.getDatefromString(startDate));


        return vo;
    }

}
