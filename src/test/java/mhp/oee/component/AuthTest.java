package mhp.oee.component;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import mhp.oee.po.gen.ActivityPermPO;
import mhp.oee.vo.ActivityVO;
import mhp.oee.web.angular.authorization.management.AuthService;
import mhp.oee.web.angular.authorization.management.AuthTransfer;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
public class AuthTest {

    @Resource
    AuthService service;

    @Test
    public void testCRUD() {
        AuthTransfer transfer = new AuthTransfer();

        transfer.getActivityVOs().add(this.getActivityVO("3", "R"));
        transfer.getActivityVOs().add(this.getActivityVO("4", "R"));
        transfer.getActivityVOs().add(this.getActivityVO("4", "RW"));
        transfer.getActivityVOs().add(this.getActivityVO("3", "R"));
        transfer.getActivityVOs().add(this.getActivityVO("5", "RW"));
        transfer.getActivityVOs().add(this.getActivityVO("6", "RW"));
        transfer.getActivityVOs().add(this.getActivityVO("7", "R"));
        transfer.getActivityVOs().add(this.getActivityVO("8", "RW"));
        transfer.getActivityVOs().add(this.getActivityVO("8", "R"));


//        Assert.assertEquals(9, transfer.getActivityVOs().size());


        service.deleteDuplicateAndOverwrite(transfer);

        Assert.assertEquals(6, transfer.getActivityVOs().size());




    }

    private ActivityVO getActivityVO(String act, String mode) {
        ActivityVO vo = new ActivityVO();
        vo.setActivity(act);
        vo.setPermPO(new ActivityPermPO());
        vo.getPermPO().setPermissionMode(mode);

        return vo;
    }

}