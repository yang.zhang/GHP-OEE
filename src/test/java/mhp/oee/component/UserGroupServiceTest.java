package mhp.oee.component;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import ch.qos.logback.access.servlet.Util;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.component.user.UserComponent;
import mhp.oee.dao.gen.ActivityClientPOMapper;
import mhp.oee.dao.gen.ActivityGroupActivityPOMapper;
import mhp.oee.dao.gen.ActivityGroupPOMapper;
import mhp.oee.dao.gen.ActivityPOMapper;
import mhp.oee.dao.gen.ActivityPermPOMapper;
import mhp.oee.po.gen.ActivityClientPO;
import mhp.oee.po.gen.ActivityGroupActivityPO;
import mhp.oee.po.gen.ActivityGroupPO;
import mhp.oee.po.gen.ActivityPO;
import mhp.oee.po.gen.UserGroupPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ActivityPermAndActivityVO;
import mhp.oee.vo.ActivityClientPermAndActivityClientVO;
import mhp.oee.vo.UserGroupMemberVO;
import mhp.oee.vo.UserGroupVO;
import mhp.oee.web.angular.user.group.management.UserGroupService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = { TestRollBackException.class })
public class UserGroupServiceTest {

    @Resource
    UserGroupService userGroupService;
    
    @Resource
    UserComponent userComponent;
    
    @Resource
    ActivityPOMapper activityPOMapper;
    
    @Resource
    ActivityGroupPOMapper activityGroupPOMapper;
    
    @Resource
    ActivityGroupActivityPOMapper activityGroupActivityPOMapper;
    
    @Resource
    ActivityPermPOMapper activityPermPOMapper;
    
    @Resource
    ActivityClientPOMapper activityClientPOMapper;
    
    @Test (expected = TestRollBackException.class)
    public void testCRUD() throws TestRollBackException {
        try {
            // ---------------------------User Group Start----------------------------
            
            //create UserGroup
            UserGroupVO userGroupVO1 = new UserGroupVO();
            userGroupVO1.setSite("JUNIT");
            userGroupVO1.setUserGroup("USER_GROUP_T1");
            userGroupVO1.setDescription("USER_GROUP_TEST1");
            userGroupVO1.setViewActionCode('C');
            
            UserGroupVO userGroupVO2 = new UserGroupVO();
            userGroupVO2.setSite("JUNIT");
            userGroupVO2.setUserGroup("USER_GROUP_T2");
            userGroupVO2.setDescription("USER_GROUP_TEST2");
            userGroupVO2.setViewActionCode('C');
            
            List<UserGroupVO> vos2 = new ArrayList<UserGroupVO>();
            vos2.add(userGroupVO1);
            vos2.add(userGroupVO2);
            userGroupService.changeUserGroup(vos2);
            
            // read and update UserGroup
            List<UserGroupPO> userGroupPOs = userGroupService.readUserGroupByGroup("JUNIT", "USER_GROUP_T1");
            System.out.println("userGroupPO -->" + userGroupPOs);
            if (userGroupPOs != null && userGroupPOs.size() > 0) {
                UserGroupPO userGroupPO =  userGroupPOs.get(0);
                userGroupVO1 = Utils.copyObjectProperties(userGroupPO, UserGroupVO.class);
                userGroupVO1.setViewActionCode('U');
                userGroupVO1.setDescription("CHANGE");
                vos2 = new ArrayList<UserGroupVO>();
                vos2.add(userGroupVO1);
                userGroupService.changeUserGroup(vos2);
            }
            
            // ---------------------------User Group End----------------------------

            
            // ---------------------------User Group Member Start-------------------
            // create UserGroupMember
            UserGroupMemberVO vo1 = new UserGroupMemberVO();
            vo1.setUserGroupBo("UserGroupBO:JUNIT,USER_GROUP_T1");
            vo1.setUserBo("UserBO:JUNIT,USER_ID_T1");
            vo1.setViewActionCode('C');
            
            UserGroupMemberVO vo2 = new UserGroupMemberVO();
            vo2.setUserGroupBo("UserGroupBO:JUNIT,USER_GROUP_T1");
            vo2.setUserBo("UserBO:JUNIT,USER_ID_T2");
            vo2.setViewActionCode('C');
            
            List<UserGroupMemberVO> vos1 = new ArrayList<UserGroupMemberVO>();
            vos1.add(vo1);
            vos1.add(vo2);
            userGroupService.changeUserGroupMember(vos1);
            
            // delete UserGroupMember
            vo1.setHandle("UserGroupMemberBO:UserGroupBO:JUNIT,USER_GROUP_T1,UserBO:JUNIT,USER_ID_T1");
            vo1.setViewActionCode('D');
            vos1 = new ArrayList<UserGroupMemberVO>();
            vos1.add(vo1);
            userGroupService.changeUserGroupMember(vos1);
            
            // ---------------------------User Group Member End-------------------
            
            // ---------------------------Activity PERM Start --------------------

            //create Activity PERM
            ActivityPermAndActivityVO activityPerm1 = new ActivityPermAndActivityVO();
            activityPerm1.setUserGroupBo("UserGroupBO:JUNIT,USER_GROUP_T1");
            activityPerm1.setActivityBo("ActivityBO:ACTIVITY_JUNIT_T1");
            activityPerm1.setPermissionSetting("true");
            activityPerm1.setPermissionMode("R+W");
            activityPerm1.setViewActionCode('C');
            
            ActivityPermAndActivityVO activityPerm2 = new ActivityPermAndActivityVO();
            activityPerm2.setUserGroupBo("UserGroupBO:JUNIT,USER_GROUP_T1");
            activityPerm2.setActivityBo("ActivityBO:ACTIVITY_JUNIT_T2");
            activityPerm2.setPermissionSetting("true");
            activityPerm2.setPermissionMode("R+W");
            activityPerm2.setViewActionCode('C');
            
            List<ActivityPermAndActivityVO> vos = new ArrayList<ActivityPermAndActivityVO>();
            vos.add(activityPerm1);
            vos.add(activityPerm2);
            userGroupService.changeActivityPerm(vos);
            

            //read Activity PERM
            // INIT data 
            // create activity 
            ActivityPO activityPO = new ActivityPO();
            activityPO.setHandle("ActivityBO:ACTIVITY_JUNIT_T1");
            activityPO.setActivity("ACTIVITY_JUNIT_T1");
            activityPO.setDescription("ACTIVITY_JUNIT_TEST1");
            activityPO.setEnabled("true");
            activityPO.setVisible("true");
            activityPO.setExecutionType("P");
            activityPO.setSequenceId(1);
            
            ActivityPO activityPO1 = new ActivityPO();
            activityPO1.setHandle("ActivityBO:ACTIVITY_JUNIT_T2");
            activityPO1.setActivity("ACTIVITY_JUNIT_T2");
            activityPO1.setDescription("ACTIVITY_JUNIT_TEST2");
            activityPO1.setEnabled("true");
            activityPO1.setVisible("true");
            activityPO1.setExecutionType("P");
            activityPO1.setSequenceId(2);
            
            activityPOMapper.insert(activityPO);
            activityPOMapper.insert(activityPO1);
            
            // create activity group 
            ActivityGroupPO po1 = new ActivityGroupPO();
            po1.setHandle("ActivityGroupBO:ACTIVITY_GROUP_JUNIT_T1");
            po1.setActivityGroup("ACTIVITY_GROUP_JUNIT_T1");
            po1.setDescription("ACTIVITY_GROUP_JUNIT_TEST1");
            activityGroupPOMapper.insert(po1);
            
            ActivityGroupPO po2 = new ActivityGroupPO();
            po2.setHandle("ActivityGroupBO:ACTIVITY_GROUP_JUNIT_T2");
            po2.setActivityGroup("ACTIVITY_GROUP_JUNIT_T2");
            po2.setDescription("ACTIVITY_GROUP_JUNIT_TEST2");
            activityGroupPOMapper.insert(po2);
            
            // create activity group activity
            ActivityGroupActivityPO activityGroupActivityPO = new ActivityGroupActivityPO();
            activityGroupActivityPO.setHandle("ActivityGroupActivityBO:ActivityGroupBO:ACTIVITY_GROUP_JUNIT_T1,ActivityBO:ACTIVITY_JUNIT_T1");
            activityGroupActivityPO.setActivityGroupBo("ActivityGroupBO:ACTIVITY_GROUP_JUNIT_T1");
            activityGroupActivityPO.setActivityBo("ActivityBO:ACTIVITY_JUNIT_T1");
            
            ActivityGroupActivityPO activityGroupActivityPO1 = new ActivityGroupActivityPO();
            activityGroupActivityPO1.setHandle("ActivityGroupActivityBO:ActivityGroupBO:ACTIVITY_GROUP_JUNIT_T1,ActivityBO:ACTIVITY_JUNIT_T2");
            activityGroupActivityPO1.setActivityGroupBo("ActivityGroupBO:ACTIVITY_GROUP_JUNIT_T1");
            activityGroupActivityPO1.setActivityBo("ActivityBO:ACTIVITY_JUNIT_T2");
            
            activityGroupActivityPOMapper.insert(activityGroupActivityPO);
          
            
            List<ActivityPermAndActivityVO>  activityVos = userGroupService.getActivityPermByUserGroup("JUNIT", "USER_GROUP_T1");
            System.out.println("activityVos -- >" + activityVos);
            if (activityVos != null && activityVos.size() > 0) {
                for (ActivityPermAndActivityVO activityPermAndActivityVO : activityVos) {
                    if (!Utils.isEmpty(activityPermAndActivityVO.getHandle())){
                        activityPerm1 = activityPermAndActivityVO;
                        activityPerm1.setPermissionSetting("false");
                        activityPerm1.setViewActionCode('U');
                        vos = new ArrayList<ActivityPermAndActivityVO>();
                        vos.add(activityPerm1);
                        userGroupService.changeActivityPerm(vos);
                        break;
                    }
                }
            }
            
            // ---------------------------Activity PERM Start --------------------
            
            // create Activity Client PERM
            ActivityClientPO activityClientPO = new ActivityClientPO();
            activityClientPO.setHandle("ActivityClientBO:ACTIVITY_CLIENT_JUNIT_T1");
            activityClientPO.setActivityClient("ACTIVITY_CLIENT_JUNIT_T1");
            activityClientPO.setDescription("ACTIVITY_CLIENT_JUNIT_TEST1");
            activityClientPOMapper.insert(activityClientPO);
            
            ActivityClientPermAndActivityClientVO activityClientVO = new ActivityClientPermAndActivityClientVO();
            activityClientVO.setHandle("ActivityClientPermBO:UserGroupBO:JUNIT,USER_GROUP_T1,ActivityClientBO:ACTIVITY_CLIENT_JUNIT_T1");
            activityClientVO.setActivityClientBo("ActivityClientBO:ACTIVITY_CLIENT_JUNIT_T1");
            activityClientVO.setUserGroupBo("UserGroupBO:JUNIT,USER_GROUP_T1");
            activityClientVO.setPermissionSetting("true");
            activityClientVO.setViewActionCode('C');
            
            List<ActivityClientPermAndActivityClientVO> activityClientVOs = new ArrayList<ActivityClientPermAndActivityClientVO>();
            activityClientVOs.add(activityClientVO);
            userGroupService.changeActivityClientPerm(activityClientVOs);
            
            // update Activity Client PERM
            List<ActivityClientPermAndActivityClientVO> activityClientVOs2 = userGroupService.getActivityClientPermByUserGroup("JUNIT", "USER_GROUP_T1");
            if (activityClientVOs2 != null && activityClientVOs2.size() > 0) {
                activityClientVO = activityClientVOs2.get(0);
                activityClientVO.setPermissionSetting("false");
                activityClientVO.setViewActionCode('U');
                activityClientVOs = new ArrayList<ActivityClientPermAndActivityClientVO>();
                activityClientVOs.add(activityClientVO);
                userGroupService.changeActivityClientPerm(activityClientVOs);
            }
            
            throw new TestRollBackException();

        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        }
    }
}
