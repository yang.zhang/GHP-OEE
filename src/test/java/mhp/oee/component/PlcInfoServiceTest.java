package mhp.oee.component;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.dao.gen.PlcAccessTypePOMapper;
import mhp.oee.dao.gen.ResourcePOMapper;
import mhp.oee.po.gen.PlcAccessTypePO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.PlcInfoConditionVO;
import mhp.oee.vo.ResourcePlcInfoAndResrceAndPlcAccessTypeVO;
import mhp.oee.vo.ResourcePlcInfoVO;
import mhp.oee.vo.ResourceTypeConditionVO;
import mhp.oee.vo.WorkCenterConditionVO;
import mhp.oee.web.angular.resource.plc.management.PlcInfoService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = { TestRollBackException.class })
public class PlcInfoServiceTest {

    @Resource
    PlcInfoService plcInfoService;
    
    @Resource
    ResourcePOMapper resourcePOMapper;
    
    @Resource
    PlcAccessTypePOMapper plcAccessTypePOMapper;

    @Test(expected = TestRollBackException.class)
    public void testCRUD() throws TestRollBackException {
        try {
            //INIT data 
            ResourcePO resourcePO = new ResourcePO();
            resourcePO.setHandle("ResourceBO:JUNIT,JT_RESRCE_TEST1");
            resourcePO.setSite("JUNIT");
            resourcePO.setResrce("JT_RESRCE_TEST1");
            resourcePO.setDescription("JT_RESRCE_TEST1TTTTT");
            resourcePO.setEnabled("true");
            resourcePOMapper.insert(resourcePO);
            
            PlcAccessTypePO plcAccessTypePO = new PlcAccessTypePO();
            plcAccessTypePO.setHandle("ResourceTypeBO:JUNIT,JT_PLC_ACCESS_TYPE_T1");
            plcAccessTypePO.setSite("JUNIT");
            plcAccessTypePO.setPlcAccessType("JT_PLC_ACCESS_TYPE_T1");
            plcAccessTypePO.setDescription("DES");
            plcAccessTypePOMapper.insert(plcAccessTypePO);
            
            // create
            ResourcePlcInfoVO vo1 = new ResourcePlcInfoVO();
            vo1.setHandle("ResourcePlcInfoBO:JUNIT,ResourceBO:JUNIT,JT_RESRCE_TEST1");
            vo1.setSite("JUNIT");
            vo1.setResourceBo("ResourceBO:JUNIT,JT_RESRCE_TEST1");
            vo1.setPlcIp("JT_PLCIP_1");
            vo1.setPlcPort("8080");
            vo1.setPlcAccessType("JT_PLC_ACCESS_TYPE_T1");
            vo1.setViewActionCode('C');  
            
            List<ResourcePlcInfoVO> resourcePlcInfoVO = new ArrayList<ResourcePlcInfoVO>();
            resourcePlcInfoVO.add(vo1);

            // test create
            plcInfoService.changePlcInfo(resourcePlcInfoVO);
            
            // select ResourcePlcInfo
            String resource[] = {"JT_RESRCE_TEST1"};
            PlcInfoConditionVO plcInfoConditionVO = new PlcInfoConditionVO();
            plcInfoConditionVO.setSite("JUNIT");
            plcInfoConditionVO.setHasResource("true");
            plcInfoConditionVO.setResource(resource);
            plcInfoConditionVO.setLimit(5);
            plcInfoConditionVO.setOffset(0);
            List<ResourcePlcInfoAndResrceAndPlcAccessTypeVO>  vosList = plcInfoService.readResPlcInfoByRes(plcInfoConditionVO);
            List<ResourcePlcInfoVO> vos = Utils.copyListProperties(vosList, ResourcePlcInfoVO.class);
            System.out.println("ResourcePlcInfoVO -->" + vos);
            if (vos != null && vos.size() > 0){
                // test update
                vo1 = vos.get(0);
                vo1.setPlcIp("JT_PLCIP_1_CHANGE");
                vo1.setPlcPort("8081");
                vo1.setViewActionCode('U');
                resourcePlcInfoVO = new ArrayList<ResourcePlcInfoVO>();
                resourcePlcInfoVO.add(vo1);
                plcInfoService.changePlcInfo(resourcePlcInfoVO);
            }

            // test select
            String site = "JUNIT";
            String workCenter = "JT_WORK_CENTER_1";
            List<WorkCenterConditionVO> workCenterConditionVOs = plcInfoService.readWorkCenter(site);
            System.out.println(">>>>>>>>>>>> ......... " + workCenterConditionVOs);

            List<WorkCenterConditionVO> lineVOs = plcInfoService.readLine(site, workCenter);
            System.out.println(">>>>>>>>>>>> ......... " + lineVOs);

            List<ResourceTypeConditionVO> resourceTypeConditionVOs = plcInfoService.readResourceType(site);
            System.out.println(">>>>>>>>>>>> ......... " + resourceTypeConditionVOs);

            throw new TestRollBackException();

        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        }
    }

}
