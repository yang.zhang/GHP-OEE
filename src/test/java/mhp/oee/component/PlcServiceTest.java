package mhp.oee.component;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.vo.ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO;
import mhp.oee.web.angular.resource.plc.management.PlcService;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.dao.gen.ResourcePOMapper;
import mhp.oee.po.gen.ResourcePO;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = { TestRollBackException.class })
public class PlcServiceTest {

    @Resource
    PlcService plcService; 
    
    @Resource
    ResourcePOMapper resourcePOMapper; 

    @Test (expected = TestRollBackException.class)
    public void testCRUD() throws TestRollBackException {
        try {
            // ---------------------------PLC Service Start----------------------------
            //INIT DATA 
            ResourcePO po = new ResourcePO();
            ResrceBOHandle boHandle = new ResrceBOHandle("JUNIT", "JUNIT_RESRCE_01");
            po.setHandle(boHandle.getValue());
            po.setSite("JUNIT");
            po.setResrce("JUNIT_RESRCE_01");
            po.setDescription("JUNIT_RESRCE_01");
            po.setEnabled("true");
            resourcePOMapper.insert(po);
            
            ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO vo1 = new ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO();
            vo1.setSite("JUNIT");
            vo1.setResourceBo(boHandle.getValue());
            vo1.setPlcAddress("JUNIT_PLC_ADDRESS_01");
            vo1.setCategory("JUNIT_CATEGORY_01");
            vo1.setPlcValue("JUNIT_PLC_VALUE_01");
            vo1.setPlcObject("JUNIT_PLC_OBJECT_01");
            vo1.setDescription("JUNIT_TEST");
            vo1.setLogPath("JUNIT_LOG_PATH");
            vo1.setResourcePO(resourcePOMapper.selectByPrimaryKey(boHandle.getValue()));
            vo1.setViewActionCode('C');
            
            List<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO> vos = new ArrayList<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO>();
            vos.add(vo1);
            
            //test create
            plcService.changeResourcePlc(vos);         
            //test select
            List<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO> plcVO = plcService.readResourcePlc("JUNIT", "JUNIT_RESRCE_01", "JUNIT_CATEGORY_01", 10, 0);
            System.out.println("PlcVO --> " + plcVO);
            if (plcVO != null && plcVO.size() > 0) {
                vo1 = plcVO.get(0);
                //test update
                vo1.setLogPath("CHANGE");
                vo1.setViewActionCode('U');
                vos = new ArrayList<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO>();
                vos.add(vo1);
                plcService.changeResourcePlc(vos);
                //test delete
                vo1.setViewActionCode('D');
                vos = new ArrayList<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO>();
                vos.add(vo1);
                plcService.changeResourcePlc(vos);
            }
            // ---------------------------PLC Service End----------------------------
            
           throw new TestRollBackException();

        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        }
    }
}
