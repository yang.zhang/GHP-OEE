package mhp.oee.component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.vo.PlcAccessTypeVO;
import mhp.oee.web.angular.plc.accesstype.management.PlantAccessTypeService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = {TestRollBackException.class})
public class PlcAccessTypeServiceTest {

    @Resource
    PlantAccessTypeService accessTypeService;

    @Test(expected=TestRollBackException.class)
    public void testCRUD() throws TestRollBackException {
        try {
            //test create
            PlcAccessTypeVO plcAccessTypeVO1 = new PlcAccessTypeVO();
            plcAccessTypeVO1.setSite("10000");
            plcAccessTypeVO1.setPlcAccessType("type01");
            plcAccessTypeVO1.setDescription("UnitTest");
            plcAccessTypeVO1.setViewActionCode('C');
            plcAccessTypeVO1.setModifiedDateTime(new Date());


            PlcAccessTypeVO plcAccessTypeVO2 = new PlcAccessTypeVO();
            plcAccessTypeVO2.setSite("10000");
            plcAccessTypeVO2.setPlcAccessType("type02");
            plcAccessTypeVO2.setDescription("UnitTest");
            plcAccessTypeVO2.setViewActionCode('C');
            plcAccessTypeVO2.setModifiedDateTime(new Date());


            List<PlcAccessTypeVO> vos = new ArrayList<PlcAccessTypeVO>();
            vos.add(plcAccessTypeVO1);
            vos.add(plcAccessTypeVO2);

            accessTypeService.changePlcAccess("10000", vos);


            PlcAccessTypeVO vo = new PlcAccessTypeVO();
            vo.setHandle("PlcAccessTypeBO:10000,type01");
            PlcAccessTypeVO vo1 = accessTypeService.getAccessTypeByCode(vo);

            Assert.assertNotNull(vo1);
            Assert.assertEquals("type01", vo1.getPlcAccessType());
            System.out.println(".............>>>>>>>>>>>>>>" + vo1.toString());


            //test change
            vo1.setViewActionCode('U');
            vo1.setDescription("Change");
            vo1.setPlcAccessType("Change");
            vos = new ArrayList<PlcAccessTypeVO>();
            vos.add(vo1);
            accessTypeService.changePlcAccess("10000", vos);



            vo = new PlcAccessTypeVO();
            vo.setHandle("PlcAccessTypeBO:10000,type01");

            PlcAccessTypeVO v2 = accessTypeService.getAccessTypeByCode(vo);
            Assert.assertEquals("Change", v2.getDescription());
            Assert.assertEquals("Change", v2.getPlcAccessType());

            //delete test
            v2.setViewActionCode('D');
            vos = new ArrayList<PlcAccessTypeVO>();
            vos.add(v2);
            accessTypeService.changePlcAccess("10000", vos);

            Assert.assertNull(accessTypeService.getAccessTypeByCode(v2));

            throw new TestRollBackException();
        } catch (BusinessException e) {
            Assert.fail(e.getMessage());
        }

    }

}
