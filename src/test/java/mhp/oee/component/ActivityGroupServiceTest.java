package mhp.oee.component;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.component.activity.ActivityGroupActivityComponent;
import mhp.oee.component.activity.ActivityGroupComponent;
import mhp.oee.vo.ActivityComVO;
import mhp.oee.vo.ActivityVO;
import mhp.oee.web.angular.activity.management.ActivityGroupService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = {TestRollBackException.class})
public class ActivityGroupServiceTest {

    @Resource
    ActivityGroupComponent activityGroupComponent;
    @Resource
    ActivityGroupActivityComponent activityGroupActivityComponent;
    @Autowired
    ActivityGroupService activityGroupService;

    @Test(expected=TestRollBackException.class)
    public void testCRUD() throws TestRollBackException, ParseException {
        try {
            /*List<ActivityGroupVO> avaGrps=new LinkedList<ActivityGroupVO>();
            ActivityGroupVO avaVo1=new ActivityGroupVO();
            avaVo1.setActivityGroup("activity_group1");
            ActivityGroupVO avaVo2=new ActivityGroupVO();
            avaVo2.setActivityGroup("activity_group12");
            avaGrps.add(avaVo1);
            avaGrps.add(avaVo2);

            List<ActivityGroupVO> assGrps=new LinkedList<ActivityGroupVO>();
            ActivityGroupVO assVo1=new ActivityGroupVO();
            assVo1.setActivityGroup("activity_group");
            assGrps.add(assVo1);

            ActivityGroupCombVO vo=new ActivityGroupCombVO();
            vo.setAvaliableActivityGroupVos(avaGrps);
            vo.setAssigendActivityGroupVos(assGrps);

            activityGroupService.changeActivityGroup(vo);
            boolean activity_group1 = activityGroupActivityComponent.existActivityGroupActivity(
                    "ActivityGroupActivityBO:ActivityGroupBO:activity_group1,ActivityBO:RULE_MANUSEL_F");
            boolean activity_group12 = activityGroupActivityComponent.existActivityGroupActivity(
                    "ActivityGroupActivityBO:ActivityGroupBO:activity_group12,ActivityBO:RULE_MANUSEL_F");
            boolean activity_group=activityGroupActivityComponent.existActivityGroupActivity(
                    "ActivityGroupActivityBO:ActivityGroupBO:activity_group,ActivityBO:RULE_MANUSEL_F");
            Assert.assertTrue(!activity_group1);
            Assert.assertTrue(!activity_group12);
            Assert.assertTrue(activity_group);*/
            
            
            //move Activity
            List<ActivityVO> avaActivity=new LinkedList<ActivityVO>();
            ActivityVO avavo=new ActivityVO();
            avavo.setActivity("RULE_MANUSEL_F");
            avaActivity.add(avavo);
            
            List<ActivityVO> assActivity=new LinkedList<ActivityVO>();
            ActivityVO asspo=new ActivityVO();
            asspo.setActivity("RULE_MANUSEL_R");
            assActivity.add(asspo);
            
            
            ActivityComVO activityComVO=new ActivityComVO();
            activityComVO.setActivityGroup("activityComVO");
            activityComVO.setAvaliableActivityVos(avaActivity);
            activityComVO.setAssigendActivityVos(assActivity);
            activityGroupService.moveActivity(activityComVO);
            
            throw new TestRollBackException();
        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        }
    }
}