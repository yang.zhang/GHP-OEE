package mhp.oee.component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.vo.ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStateVO;
import mhp.oee.vo.ReasonCodeResStateVO;
import mhp.oee.vo.ReasonCodeVO;
import mhp.oee.web.angular.reasoncode.management.ReasonCodeResStateService;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.common.handle.ReasonCodeBOHandle;
import mhp.oee.common.handle.ResourceStateBOHandle;
import mhp.oee.component.plant.ReasonCodeComponent;
import mhp.oee.dao.gen.ResourceStatePOMapper;
import mhp.oee.po.gen.ResourceStatePO;
import mhp.oee.utils.Utils;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = { TestRollBackException.class })
public class ReasonCodeResStateServiceTest {

    @Resource
    ReasonCodeResStateService reasonCodeResStateService; 
    
    @Resource
    ReasonCodeComponent reasonCodeComponent;
    
    @Resource
    ResourceStatePOMapper resourceStatePOMapper;

    @Test (expected = TestRollBackException.class)
    public void testCRUD() throws TestRollBackException {
        try {
            // ---------------------------Reason Code Res State Start----------------------------
            //INIT DATA  REASONCODE AND RESSTATE
            ReasonCodeVO reasonCodeVO = new ReasonCodeVO();
            reasonCodeVO.setSite("JUNIT");
            reasonCodeVO.setReasonCode("JUNIT_RSCODE_INIT_01");
            reasonCodeVO.setReasonCodeGroup("JUNIT_RSCODEGROUP_INIT_01");
            reasonCodeVO.setDescription("JUNIT_REASONCODE_INIT_01");
            reasonCodeVO.setEnabled("true");
            reasonCodeComponent.createReasonCode(reasonCodeVO);
            
            ResourceStatePO po1 = new ResourceStatePO();
            ResourceStateBOHandle resourceStateBOHandle = new ResourceStateBOHandle("JUNIT", "JUNIT_STATE_01");
            po1.setHandle(resourceStateBOHandle.getValue());
            po1.setSite("JUNIT");
            po1.setState("JUNIT_STATE_01");
            po1.setDescription("JUNIT_STATE_01");
            po1.setEnabled("true");
            resourceStatePOMapper.insert(po1);
            
            Date now = new Date();
            ReasonCodeResStateVO vo1 = new ReasonCodeResStateVO();
            ReasonCodeBOHandle reasonCodeBoHandle = new ReasonCodeBOHandle("JUNIT", "JUNIT_RSCODE_INIT_01");
            vo1.setReasonCodeBo(reasonCodeBoHandle.getValue());
            vo1.setResourceStateBo(resourceStateBOHandle.getValue());
            vo1.setStartDateTimeIn(Utils.datetimeToStr(now));
            vo1.setStrt(Utils.parseReportDateDay(Utils.datetimeToStr(now)));
            vo1.setViewActionCode('C');
            
            List<ReasonCodeResStateVO> reasonCodeResStateVOs = new ArrayList<ReasonCodeResStateVO>();
            reasonCodeResStateVOs.add(vo1);
            
            //test create
            reasonCodeResStateService.changeResonCodeResourceState(reasonCodeResStateVOs);       
            //test select
            List<ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStateVO> lists = reasonCodeResStateService.readReasonCodeResState("JUNIT", "JUNIT_RSCODEGROUP_INIT_01", null, Utils.datetimeToStr(now), 10, 0);
            System.out.println("ReasonCodeResStateVO --> " + lists);
            if (lists != null && lists.size() > 0) {
                vo1 = Utils.copyObjectProperties(lists.get(0), ReasonCodeResStateVO.class);
                //test Delete
                vo1.setViewActionCode('D');
                reasonCodeResStateVOs = new ArrayList<ReasonCodeResStateVO>();
                reasonCodeResStateVOs.add(vo1);
                reasonCodeResStateService.changeResonCodeResourceState(reasonCodeResStateVOs);
            }
            // ---------------------------Reason Code Res State End----------------------------
            
           throw new TestRollBackException();

        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        } catch (ParseException be) {
            Assert.fail(be.getMessage());
        }
    }
}
