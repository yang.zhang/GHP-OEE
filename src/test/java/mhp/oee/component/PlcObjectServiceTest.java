package mhp.oee.component;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.vo.PlcObjectVO;
import mhp.oee.web.angular.plc.object.management.PlantObjectService;
import mhp.oee.common.exception.TestRollBackException;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = { TestRollBackException.class })
public class PlcObjectServiceTest {

    @Resource
    PlantObjectService plantObjectService; 

    @Test (expected = TestRollBackException.class)
    public void testCRUD() throws TestRollBackException {
        try {
            // ---------------------------Plc Object Start----------------------------
            PlcObjectVO vo1 = new PlcObjectVO();
            vo1.setSite("JUNIT");
            vo1.setPlcObject("JN_OBJ_01");
            vo1.setPlcObjectDescription("JUNIT_OBJECT_01");
            vo1.setCategory("JN_CAT_01");
            vo1.setViewActionCode('C');
            
            List<PlcObjectVO> plcObjectVOs = new ArrayList<PlcObjectVO>();
            plcObjectVOs.add(vo1);
            
            //test create
            plantObjectService.changePlcObjects(plcObjectVOs);          
            //test select
            List<PlcObjectVO> plcObjectVO = plantObjectService.readAllPlcObject("JUNIT", "JN_CAT_01");
            System.out.println("PlcObjectVO --> " + plcObjectVO);
            if (plcObjectVO != null && plcObjectVO.size() > 0) {
                vo1 = plcObjectVO.get(0);
                //test update
                vo1.setPlcObjectDescription("CHANGE");
                vo1.setViewActionCode('U');
                plcObjectVOs = new ArrayList<PlcObjectVO>();
                plcObjectVOs.add(vo1);
                plantObjectService.changePlcObjects(plcObjectVOs);
                //test delete
                vo1.setViewActionCode('D');
                plcObjectVOs = new ArrayList<PlcObjectVO>();
                plcObjectVOs.add(vo1);
                plantObjectService.changePlcObjects(plcObjectVOs);
            }
            // ---------------------------Plc Object End----------------------------
            
           throw new TestRollBackException();

        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        }
    }
}
