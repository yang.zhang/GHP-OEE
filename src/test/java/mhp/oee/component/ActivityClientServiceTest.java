package mhp.oee.component;

import java.text.ParseException;
import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.common.handle.ActivityClientBOHandle;
import mhp.oee.component.activity.ActivityClientComponent;
import mhp.oee.vo.ActivityClientVO;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = {TestRollBackException.class})
public class ActivityClientServiceTest {

    @Resource
    ActivityClientComponent activityClientComponent;

    @Test(expected=TestRollBackException.class)
    public void testCRUD() throws TestRollBackException, ParseException {
        try {
            // create activityClient
            ActivityClientVO vo = new ActivityClientVO();
            vo.setActivityClient("activityClientTest111");
            vo.setDescription("desc");
            vo.setType("A");
            vo.setVisible("true");
            System.err.println(vo);
            activityClientComponent.createActivityClient(vo);
            String handle=new ActivityClientBOHandle(vo.getActivityClient()).getValue();
            vo=activityClientComponent.getActivityClientByPrimaryKey(handle);
            System.out.println(vo);
            Assert.assertTrue(vo!=null);
            
            // update activityClient
            vo.setDescription("changeDesc");
            vo.setFirstModifiedDateTime(vo.getModifiedDateTime());
            activityClientComponent.updateActivityClient(vo);
            vo=activityClientComponent.getActivityClientByPrimaryKey(handle);
            Assert.assertTrue("changeDesc".equals(vo.getDescription()));
            throw new TestRollBackException();
        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        }
    }
}
