package mhp.oee.component;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.web.angular.activity.management.ActivityManageVO;
import mhp.oee.web.angular.activity.management.ActivityParamValueService;
import mhp.oee.web.angular.activity.management.ActivityService;

/**
 * Created by LinZuK on 2016/7/25.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = {TestRollBackException.class})
public class ActivityParamValueServiceTest {

    @Resource
    ActivityParamValueService activityParamValueService;
    @Resource
    ActivityService activityService;

    @Test(expected=TestRollBackException.class)
    public void testCRUD() throws Exception {
        try {

            // C
            List<ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO> vos = new ArrayList<ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO>(1);
            ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO vo = new ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO();
            vo.setActivityBo("ActivityBO:activityTest");
            vo.setActivityDesc("#");
            vo.setParamId("param4");
            vo.setParamDescription("test");
            vo.setParamDefaultValue("test");
            vo.setParamValue("test");
            vo.setViewActionCode('C');
            vos.add(vo);
            activityParamValueService.changeActivityParamValues("TEST", vos);

            System.out.println("<<---------------------- Start ActivityParamValueService Test ---------------------->>");

            List<ActivityManageVO.ActivityConditionVO> activityConditionVOList = activityService.getActivitiesLike("ivityTe"); // ivityTe
//            System.out.println("1 " + JSON.toJSONString(activityConditionVOList));

            List<ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO> activityAndActivityParamAndActivityParamValueVOList
                    = activityParamValueService.getActivityParamValues("TEST", "activityTest");
//            System.out.println("2 " + JSON.toJSONString(activityAndActivityParamAndActivityParamValueVOList));

            System.out.println("<<---------------------- Finish ActivityParamValueService Test ---------------------->>");

        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        throw new TestRollBackException();
    }

}
