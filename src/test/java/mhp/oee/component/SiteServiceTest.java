package mhp.oee.component;

import java.text.ParseException;
import java.util.Date;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.component.plant.SiteComponent;
import mhp.oee.po.gen.SitePO;
import mhp.oee.vo.SiteVO;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = {TestRollBackException.class})
public class SiteServiceTest {

    @Resource
    SiteComponent siteComponent;

    @Test(expected=TestRollBackException.class)
    public void testCRUD() throws TestRollBackException, ParseException {
        try {
            // create TEST site
            SiteVO vo = new SiteVO();
            vo.setSite("JUNIT");
            vo.setDescription("Site JUNIT for running junit");
            vo.setModifiedUser("USER-01");
            vo.setModifiedDateTime(new Date());
            siteComponent.createSite(vo);
            SitePO po = siteComponent.readSiteByPkFields("JUNIT");
            Assert.assertTrue(po != null);

            // update TEST site
            //vo.setDescription("Change to JUnit Text");
            //vo.setHandle("SiteBO:JUNIT");
            //siteComponent.updateSiteByVO(vo);
            //po = siteComponent.readSiteByPkFields("JUNIT");
            //System.out.println(po);
            //Assert.assertTrue("Change to JUnit Text".equals(po.getDescription()));




            // update TEST site with different Modify Date
            //vo.setDescription("different modify date");
            //vo.setHandle("SiteBO:JUNIT");
            //vo.setModifiedDateTime(Utils.getDatefromString("07/11/2016"));
            //try{
            //    siteComponent.updateSiteByVO(vo);
            //} catch (BusinessException e) {
            //    Assert.assertTrue(true);
            //}


            throw new TestRollBackException();

        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        }
    }

}
