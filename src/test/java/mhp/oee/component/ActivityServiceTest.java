package mhp.oee.component;

import java.text.ParseException;
import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.common.handle.ActivityBOHandle;
import mhp.oee.component.activity.ActivityComponent;
import mhp.oee.po.gen.ActivityPO;
import mhp.oee.vo.ActivityVO;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = {TestRollBackException.class})
public class ActivityServiceTest {

    @Resource
    ActivityComponent activityComponent;

    @Test(expected=TestRollBackException.class)
    public void testCRUD() throws TestRollBackException, ParseException {
        try {
            // create activity
            ActivityVO vo = new ActivityVO();
            vo.setActivity("hehe");
            vo.setEnabled("false");
            vo.setVisible("false");
            vo.setClassOrProgram("classOrProgram");
            vo.setDescription("description");
            vo.setExecutionType("T");
            activityComponent.createActivity(vo);
            ActivityPO po=activityComponent.selectActivityByPrimaryKey(new ActivityBOHandle("hehe").getValue());
            System.out.println(po);
            Assert.assertTrue(po != null);
            
            //update activity
            System.out.println("========="+po.getDescription());
            vo.setDescription("changeDesc");
            vo.setFirstModifiedDateTime(po.getModifiedDateTime());
            activityComponent.updateActivity(vo);
            po=activityComponent.selectActivityByPrimaryKey(new ActivityBOHandle("hehe").getValue());
            System.out.println("========="+po.getDescription());
            Assert.assertTrue("changeDesc".equals(po.getDescription()));
            throw new TestRollBackException();
        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        }
    }

}