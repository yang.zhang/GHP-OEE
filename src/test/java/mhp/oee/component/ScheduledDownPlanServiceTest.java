package mhp.oee.component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.component.plant.WorkCenterComponent;
import mhp.oee.vo.ResourceVO;
import mhp.oee.vo.ScheduledDownPlanAndResrceAndWorkCenterVO;
import mhp.oee.vo.WorkCenterVO;
import mhp.oee.web.angular.planned.downtime.maintenance.ScheduledDownPlanService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = { TestRollBackException.class })
public class ScheduledDownPlanServiceTest {

    @Resource
    ScheduledDownPlanService scheduledDownPlanService;
    
    @Resource
    ResourceComponent resourceComponent;
    
    @Resource
    WorkCenterComponent workCenterComponent;
    
    @Test (expected = TestRollBackException.class)
    public void testCRUD() throws TestRollBackException {
        try {
            //INIT DATA
            Calendar cal = Calendar.getInstance();
            cal.set(2016, 2, 3, 0, 0, 0);
            String dtString = "02/13/2016-00/00/00";
            ScheduledDownPlanAndResrceAndWorkCenterVO vo1 = new ScheduledDownPlanAndResrceAndWorkCenterVO();
            vo1.setHandle("ScheduledDownPlanBO:JUNIT,ResourceBO:JUNIT,RESRCE_TEST1," + dtString);
            vo1.setSite("JUNIT");
            vo1.setResrce("RESRCE_TEST1");
            vo1.setResourceBo("ResourceBO:JUNIT,RESRCE_TEST1");
            vo1.setStrt(dtString);
            vo1.setStartDateTimeIn(dtString);
            vo1.setEndDateTimeIn(dtString);
            vo1.setReasonCode("R1");
            vo1.setViewActionCode('C');
            
            ResourceVO vo = new ResourceVO();
            vo.setSite("JUNIT");
            vo.setResrce("RESRCE_TEST1");
            vo.setDescription("RESRCE_TEST1");
            vo.setEnabled("true");
            vo.setWorkArea("T1");
            vo.setLineArea("T2");
            resourceComponent.createResource(vo);
            
            
            WorkCenterVO workCenterVO1 = new WorkCenterVO();
            workCenterVO1.setSite("JUNIT");
            workCenterVO1.setWorkCenter("T1");
            workCenterVO1.setWcCategory("ZLEVEL6");
            
            WorkCenterVO workCenterVO2 = new WorkCenterVO();
            workCenterVO2.setSite("JUNIT");
            workCenterVO2.setWorkCenter("T2");
            workCenterVO2.setWcCategory("LEVEL1");
            
            workCenterComponent.createWorkCenter(workCenterVO1);
            workCenterComponent.createWorkCenter(workCenterVO2);
            
            
            List<ScheduledDownPlanAndResrceAndWorkCenterVO> scheduledDownPlanAndResrceAndWorkCenterVO = new ArrayList<ScheduledDownPlanAndResrceAndWorkCenterVO>();
            scheduledDownPlanAndResrceAndWorkCenterVO.add(vo1);
            
            // test create
            scheduledDownPlanService.changeScheduledDownPlanByResrce(scheduledDownPlanAndResrceAndWorkCenterVO);

            //test select 
            List<ScheduledDownPlanAndResrceAndWorkCenterVO> vos = scheduledDownPlanService.readScheduledDownPlanByResrce("JUNIT", "RESRCE_TEST1", "02/13/2016", "", "", "", 5, 0);
            System.out.println("vos = "  + vos);
         
            // test update
            if (vos != null && vos.size() > 0) {
                vo1 =  vos.get(0);
                vo1.setReasonCode("R3");
                vo1.setViewActionCode('U');
                scheduledDownPlanAndResrceAndWorkCenterVO = new ArrayList<ScheduledDownPlanAndResrceAndWorkCenterVO>();
                scheduledDownPlanAndResrceAndWorkCenterVO.add(vo1);
                scheduledDownPlanService.changeScheduledDownPlanByResrce(scheduledDownPlanAndResrceAndWorkCenterVO);
            }
            
            // test existRepeatTime
            scheduledDownPlanService.existRepeatTime("JUNIT", "RESRCE_TEST1", dtString, dtString, vo1.getHandle());
            
            
            // test delete
            vo1.setViewActionCode('D');
            scheduledDownPlanAndResrceAndWorkCenterVO = new ArrayList<ScheduledDownPlanAndResrceAndWorkCenterVO>();
            scheduledDownPlanAndResrceAndWorkCenterVO.add(vo1);
            scheduledDownPlanService.changeScheduledDownPlanByResrce(scheduledDownPlanAndResrceAndWorkCenterVO);

            throw new TestRollBackException();

        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        } catch (ParseException e) {
            Assert.fail(e.getMessage());
        }
    }

}
