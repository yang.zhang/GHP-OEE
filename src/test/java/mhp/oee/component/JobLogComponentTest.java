package mhp.oee.component;

import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.component.job.JobLogComponent;
import mhp.oee.component.production.StdCycleTimeComponent;
import mhp.oee.utils.Utils;
import mhp.oee.web.angular.std.cycletime.management.StdCycleTimeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by LinZuK on 2016/9/8.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = {TestRollBackException.class})
public class JobLogComponentTest {

    @Resource
    JobLogComponent jobLogComponent;

    @Test(expected=TestRollBackException.class)
    public void testCRUD() throws Exception {
        try {
            Date startDateTime = new Date(); // 实际开始时间
            Date plannedDateTime = Utils.strToReportDateTime(Utils.reportDateTimeToStr(startDateTime)); // 计划开始时间
            // start log
            String handle1 = jobLogComponent.jobStartLog("TEST_JOB", plannedDateTime, startDateTime, Utils.getMonoServer());
            Assert.assertTrue(handle1 != null);
//            String handle2 = jobLogComponent.jobStartLog("TEST_JOB", plannedDateTime, startDateTime);
//            Assert.assertTrue(handle2 == null);
            // end log
            jobLogComponent.jobEndLog(handle1);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        throw new TestRollBackException();
    }

}
