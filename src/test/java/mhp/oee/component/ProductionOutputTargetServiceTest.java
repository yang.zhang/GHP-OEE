package mhp.oee.component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.vo.ProductionOutputTargetAndResrceVO;
import mhp.oee.web.angular.target.yield.management.ProductionOutputTargetService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = { TestRollBackException.class })
public class ProductionOutputTargetServiceTest {

    @Resource
    ProductionOutputTargetService productionOutputTargetService;

    @Test(expected = TestRollBackException.class)
    public void testCRUD() throws TestRollBackException {
        try {
            // create
            ProductionOutputTargetAndResrceVO vo1 = new ProductionOutputTargetAndResrceVO();
            vo1.setHandle("ProductionOutputTargetBO:JUNIT,JT_ITEM_T1,#,JT_RESRCE_TEST1,2016-07-20,E1");
            vo1.setSite("JUNIT");
            vo1.setItem("JT_ITEM_T1");
            vo1.setOperation("#");
            vo1.setResrce("JT_RESRCE_TEST1");
            vo1.setTargeQtyYield(new BigDecimal(12));
            vo1.setTargeQtyYieldUnit("");
            vo1.setTargetDate("2016-07-20");
            vo1.setShift("E1");
            vo1.setShiftDescription("E1DES");
            vo1.setIsDefaultValue("false");
            vo1.setModifiedUser("USER-01");
            vo1.setViewActionCode('C');

            ProductionOutputTargetAndResrceVO vo2 = new ProductionOutputTargetAndResrceVO();
            vo2.setHandle("ProductionOutputTargetBO:JUNIT,JT_ITEM_T1,#,JT_RESRCE_TEST1,2016-07-21,E2");
            vo2.setSite("JUNIT");
            vo2.setItem("JT_ITEM_T1");
            vo2.setOperation("#");
            vo2.setResrce("JT_RESRCE_TEST1");
            vo2.setTargeQtyYield(new BigDecimal(13));
            vo2.setTargeQtyYieldUnit("");
            vo2.setTargetDate("2016-07-21");
            vo2.setShift("E2");
            vo2.setShiftDescription("E2DES");
            vo2.setIsDefaultValue("false");
            vo2.setModifiedUser("USER-01");
            vo2.setViewActionCode('C');

            List<ProductionOutputTargetAndResrceVO> productionOutputTargetAndResrceVOs = new ArrayList<ProductionOutputTargetAndResrceVO>();
            productionOutputTargetAndResrceVOs.add(vo1);
            productionOutputTargetAndResrceVOs.add(vo2);

            // test create
            productionOutputTargetService.changeProductionOutputTarget(productionOutputTargetAndResrceVOs);

            // test update
            vo1.setTargeQtyYield(new BigDecimal(1));
            vo1.setViewActionCode('U');
            productionOutputTargetAndResrceVOs = new ArrayList<ProductionOutputTargetAndResrceVO>();
            productionOutputTargetAndResrceVOs.add(vo1);
            productionOutputTargetService.changeProductionOutputTarget(productionOutputTargetAndResrceVOs);

            // test delete
            vo2.setViewActionCode('D');
            productionOutputTargetAndResrceVOs = new ArrayList<ProductionOutputTargetAndResrceVO>();
            productionOutputTargetAndResrceVOs.add(vo2);
            productionOutputTargetService.changeProductionOutputTarget(productionOutputTargetAndResrceVOs);

            // test existProductionOutputTarget
            String site = "JUNIT";
            String item = "JT_ITEM_T1";
            String resrce = "JT_RESRCE_TEST1";
            String shift = "E1";
            String targetDate = "2016-07-20";
            productionOutputTargetService.existProductionOutputTarget(site, item, resrce, shift, targetDate);

            throw new TestRollBackException();

        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        }
    }

}
