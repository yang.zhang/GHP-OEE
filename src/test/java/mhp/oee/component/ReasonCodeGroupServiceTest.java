package mhp.oee.component;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.vo.ReasonCodeGroupVO;
import mhp.oee.web.angular.reasoncode.group.management.ReasonCodeGroupService;
import mhp.oee.common.exception.TestRollBackException;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
@Transactional(rollbackFor = { TestRollBackException.class })
public class ReasonCodeGroupServiceTest {

    @Resource
    ReasonCodeGroupService reasonCodeGroupService; 

    @Test (expected = TestRollBackException.class)
    public void testCRUD() throws TestRollBackException {
        try {
            // ---------------------------ReasonCodeGroup Start----------------------------
            ReasonCodeGroupVO vo1 = new ReasonCodeGroupVO();
            vo1.setSite("JUNIT");
            vo1.setReasonCodeGroup("JUNIT_RSCODEGROUP_01");
            vo1.setDescription("JUNIT_REASONCODEGROUP_01");
            vo1.setEnabled("true");
            vo1.setViewActionCode('C');
            
            List<ReasonCodeGroupVO> reasonCodeGroupVOs = new ArrayList<ReasonCodeGroupVO>();
            reasonCodeGroupVOs.add(vo1);
            
            //test create
            reasonCodeGroupService.changeReasonCodeGroup(reasonCodeGroupVOs);         
            //test select
            List<ReasonCodeGroupVO> lists = reasonCodeGroupService.readAllResonCodeGroup("JUNIT", "true");
            System.out.println("ReasonCodeGroupVO --> " + lists);
            if (lists != null && lists.size() > 0) {
                vo1 = lists.get(0);
                //test update
                vo1.setDescription("CHANGE");
                vo1.setViewActionCode('U');
                reasonCodeGroupVOs = new ArrayList<ReasonCodeGroupVO>();
                reasonCodeGroupVOs.add(vo1);
                reasonCodeGroupService.changeReasonCodeGroup(reasonCodeGroupVOs);
            }
            // ---------------------------ReasonCodeGroup End----------------------------
            
           throw new TestRollBackException();

        } catch (BusinessException be) {
            Assert.fail(be.getMessage());
        }
    }
}
