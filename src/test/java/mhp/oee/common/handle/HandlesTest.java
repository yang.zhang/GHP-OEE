package mhp.oee.common.handle;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import mhp.oee.utils.Utils;

public class HandlesTest {

    @Test
    public void testHandles() {
        // ActivityBOHandle
        {
            ActivityBOHandle activityBOHandle = new ActivityBOHandle("CT500");
            Assert.assertTrue("ActivityBO:CT500".equals(activityBOHandle.getValue()));
            Assert.assertTrue("CT500".equals(activityBOHandle.getActivity()));
        }
        // ActivityGroupBOHandle
        {
            ActivityGroupBOHandle activityGroupBOHandle = new ActivityGroupBOHandle("PRODUCTION");
            Assert.assertTrue("ActivityGroupBO:PRODUCTION".equals(activityGroupBOHandle.getValue()));
            Assert.assertTrue("PRODUCTION".equals(activityGroupBOHandle.getActivityGroup()));
        }
        // ActivityPermBOHandle
        {
            ActivityPermBOHandle activityPermBOHandle = new ActivityPermBOHandle("UserGroupBO:JUNIT,USER-GROUP-01", "ActivityBO:JUNIT,CT500");
            Assert.assertTrue("ActivityPermBO:UserGroupBO:JUNIT,USER-GROUP-01,ActivityBO:JUNIT,CT500".equals(activityPermBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(activityPermBOHandle.getSite()));
            Assert.assertTrue("USER-GROUP-01".equals(activityPermBOHandle.getUserGroup()));
            Assert.assertTrue("CT500".equals(activityPermBOHandle.getActivity()));
        }
        // ItemBOHandle
        {
            ItemBOHandle itemBOHandle = new ItemBOHandle("JUNIT", "MATERIAL-01", "A");
            Assert.assertTrue("ItemBO:JUNIT,MATERIAL-01,A".equals(itemBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(itemBOHandle.getSite()));
            Assert.assertTrue("MATERIAL-01".equals(itemBOHandle.getItem()));
            Assert.assertTrue("A".equals(itemBOHandle.getRevision()));
        }
        // SiteBOHandle
        {
            SiteBOHandle siteBOHandle = new SiteBOHandle("JUNIT");
            Assert.assertTrue("SiteBO:JUNIT".equals(siteBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(siteBOHandle.getSite()));
        }
        // UserBOHandle
        {
            UserBOHandle userBOHandle = new UserBOHandle("JUNIT", "USER-01");
            Assert.assertTrue("UserBO:JUNIT,USER-01".equals(userBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(userBOHandle.getSite()));
            Assert.assertTrue("USER-01".equals(userBOHandle.getUser()));
        }
        // UserGroupBOHandle
        {
            UserGroupBOHandle userGroupBOHandle = new UserGroupBOHandle("JUNIT", "USER-GROUP-01");
            Assert.assertTrue("UserGroupBO:JUNIT,USER-GROUP-01".equals(userGroupBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(userGroupBOHandle.getSite()));
            Assert.assertTrue("USER-GROUP-01".equals(userGroupBOHandle.getUserGroup()));
        }
        // ActivityClientBOHandle
        {
            ActivityClientBOHandle activityClientBOHandle = new ActivityClientBOHandle("CT500");
            Assert.assertTrue("ActivityClientBO:CT500".equals(activityClientBOHandle.getValue()));
            Assert.assertTrue("CT500".equals(activityClientBOHandle.getActivityClient()));
        }
        // ActivityClientPermBOHandle
        {
            ActivityClientPermBOHandle activityClientPermBOHandle = new ActivityClientPermBOHandle(
                    "UserGroupBO:JUNIT,UserGroup", "ActivityClientBO:ActivityClent");
            Assert.assertTrue("ActivityClientPermBO:UserGroupBO:JUNIT,UserGroup,ActivityClientBO:ActivityClent"
                    .equals(activityClientPermBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(activityClientPermBOHandle.getSite()));
            Assert.assertTrue("UserGroup".equals(activityClientPermBOHandle.getUserGroup()));
            Assert.assertTrue("ActivityClent".equals(activityClientPermBOHandle.getActivityClient()));
        }
        // ActivityGroupActivityBOHandle
        {
            ActivityGroupActivityBOHandle activityGroupActivityBOHandle = new ActivityGroupActivityBOHandle(
                    "ActivityGroupBO:ActivityGroup", "ActivityBO:Activity");
            Assert.assertTrue("ActivityGroupActivityBO:ActivityGroupBO:ActivityGroup,ActivityBO:Activity"
                    .equals(activityGroupActivityBOHandle.getValue()));
            Assert.assertTrue("ActivityGroup".equals(activityGroupActivityBOHandle.getActivityGroup()));
            Assert.assertTrue("Activity".equals(activityGroupActivityBOHandle.getActivity()));
        }
        // ActivityParamBOHandle
        {
            ActivityParamBOHandle activityParamBOHandle = new ActivityParamBOHandle("ActivityBO:Activity", "param");
            Assert.assertTrue("ActivityParamBO:ActivityBO:Activity,param".equals(activityParamBOHandle.getValue()));
            Assert.assertTrue("Activity".equals(activityParamBOHandle.getActivity()));
            Assert.assertTrue("param".equals(activityParamBOHandle.getParamID()));
        }
        // ColorLightBOHandle
        {
            ColorLightBOHandle colorLightBOHandle = new ColorLightBOHandle("ResourceBO:JUNIT,RESRCE");
            Assert.assertTrue("ColorLightBO:ResourceBO:JUNIT,RESRCE".equals(colorLightBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(colorLightBOHandle.getSite()));
            Assert.assertTrue("RESRCE".equals(colorLightBOHandle.getResource()));
        }
        // PlcAccessTypeBOHandle
        {
            PlcAccessTypeBOHandle plcAccessTypeBOHandle = new PlcAccessTypeBOHandle("JUNITValue", "plcAccessType");
            Assert.assertTrue("PlcAccessTypeBO:JUNITValue,plcAccessType".equals(plcAccessTypeBOHandle.getValue()));
            Assert.assertTrue("JUNITValue".equals(plcAccessTypeBOHandle.getSite()));
            Assert.assertTrue("plcAccessType".equals(plcAccessTypeBOHandle.getPlcAccessType()));
        }
        // PlcCategoryBOHandle
        {
            PlcCategoryBOHandle plcCategoryBOHandle = new PlcCategoryBOHandle("JUNIT", "category");
            Assert.assertTrue("PlcCategoryBO:JUNIT,category".equals(plcCategoryBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(plcCategoryBOHandle.getSite()));
            Assert.assertTrue("category".equals(plcCategoryBOHandle.getCatagory()));

        }
        // PlcObjectBOHandle
        {
            PlcObjectBOHandle plcObjectBOHandle = new PlcObjectBOHandle("JUNIT", "PLCOBJ");
            Assert.assertTrue("PlcObjectBO:JUNIT,PLCOBJ".equals(plcObjectBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(plcObjectBOHandle.getSite()));
            Assert.assertTrue("PLCOBJ".equals(plcObjectBOHandle.getPlcObject()));
        }
        // ProductionOutputFirstBOHandle
        {
            ProductionOutputFirstBOHandle productionOutputFirstBOHandle = new ProductionOutputFirstBOHandle("JUNIT",
                    "resrce", "operation", "operationRevision", "item", "itemRevision", "20160628-23595959");
            Assert.assertTrue(
                    "ProductionOutputFirstBO:JUNIT,resrce,operation,operationRevision,item,itemRevision,20160628-23595959"
                            .equals(productionOutputFirstBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(productionOutputFirstBOHandle.getSite()));
            Assert.assertTrue("resrce".equals(productionOutputFirstBOHandle.getResrce()));
            Assert.assertTrue("operation".equals(productionOutputFirstBOHandle.getOperation()));
            Assert.assertTrue("operationRevision".equals(productionOutputFirstBOHandle.getOperationRevision()));
            Assert.assertTrue("item".equals(productionOutputFirstBOHandle.getItem()));
            Assert.assertTrue("itemRevision".equals(productionOutputFirstBOHandle.getItemRevision()));
            Assert.assertTrue("20160628-23595959".equals(productionOutputFirstBOHandle.getStrt()));
        }
        // ProductionOutputRealtimeBOHandle
        {
            ProductionOutputRealtimeBOHandle productionOutputRealtimeBOHandle = new ProductionOutputRealtimeBOHandle(
                    "test");
            Assert.assertTrue("ProductionOutputRealtimeBO:test".equals(productionOutputRealtimeBOHandle.getValue()));
        }
        // ProductionOutputTargetBOHandle
        {
            ProductionOutputTargetBOHandle productionOutputTargetBOHandle = new ProductionOutputTargetBOHandle("JUNIT",
                    "item", "operation", "resrce", "targetDate", "shift");
            Assert.assertTrue("ProductionOutputTargetBO:JUNIT,item,operation,resrce,targetDate,shift"
                    .equals(productionOutputTargetBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(productionOutputTargetBOHandle.getSite()));
            Assert.assertTrue("item".equals(productionOutputTargetBOHandle.getItem()));
            Assert.assertTrue("operation".equals(productionOutputTargetBOHandle.getOperation()));
            Assert.assertTrue("resrce".equals(productionOutputTargetBOHandle.getResrce()));
            Assert.assertTrue("targetDate".equals(productionOutputTargetBOHandle.getTargetDate()));
            Assert.assertTrue("shift".equals(productionOutputTargetBOHandle.getShift()));
        }
        // ProductionOutputFollowUpBOHandle
        {
            ProductionOutputFollowUpBOHandle productionOutputFollowUpBOHandle = new ProductionOutputFollowUpBOHandle(
                    "JUNIT", "resrce", "operation", "operationRevision", "item", "itemRevision", "20160628-23595959");
            Assert.assertTrue(
                    "ProductionOutputFollowUpBO:JUNIT,resrce,operation,operationRevision,item,itemRevision,20160628-23595959"
                            .equals(productionOutputFollowUpBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(productionOutputFollowUpBOHandle.getSite()));
            Assert.assertTrue("resrce".equals(productionOutputFollowUpBOHandle.getResrce()));
            Assert.assertTrue("operation".equals(productionOutputFollowUpBOHandle.getOperation()));
            Assert.assertTrue("operationRevision".equals(productionOutputFollowUpBOHandle.getOperationRevision()));
            Assert.assertTrue("item".equals(productionOutputFollowUpBOHandle.getItem()));
            Assert.assertTrue("itemRevision".equals(productionOutputFollowUpBOHandle.getItemRevision()));
            Assert.assertTrue("20160628-23595959".equals(productionOutputFollowUpBOHandle.getStrt()));
        }
        // ResrceBOHandle
        {
            ResrceBOHandle ResrceBOHandle = new ResrceBOHandle("JUNIT", "RESRCE");
            Assert.assertTrue("ResourceBO:JUNIT,RESRCE".equals(ResrceBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(ResrceBOHandle.getSite()));
            Assert.assertTrue("RESRCE".equals(ResrceBOHandle.getResrce()));
        }
        // RealtimePpmBOHandle
        {
            RealtimePpmBOHandle realtimePpmBOHandle = new RealtimePpmBOHandle("ResourceBO:JUNIT,resrce");
            Assert.assertTrue("RealtimePpmBO:ResourceBO:JUNIT,resrce".equals(realtimePpmBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(realtimePpmBOHandle.getSite()));
            Assert.assertTrue("resrce".equals(realtimePpmBOHandle.getResrce()));
        }
        // ReasonCodeBOHandle
        {
            ReasonCodeBOHandle reasonCodeBOHandle = new ReasonCodeBOHandle("JUNIT", "reasonCode");
            Assert.assertTrue("ReasonCodeBO:JUNIT,reasonCode".equals(reasonCodeBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(reasonCodeBOHandle.getSite()));
            Assert.assertTrue("reasonCode".equals(reasonCodeBOHandle.getReasonCode()));
        }
        // ReasonCodeAlertBOHandle
        {
            ReasonCodeAlertBOHandle reasonCodeAlertBOHandle = new ReasonCodeAlertBOHandle("ResourceTypeBO:JUNIT,RT-01", "ALERT_SEQ_ID_01",
                    "ReasonCodeBO:JUNIT,RC-01", "20160101-120000");
            Assert.assertTrue("ReasonCodeAlertBO:ResourceTypeBO:JUNIT,RT-01,ALERT_SEQ_ID_01,ReasonCodeBO:JUNIT,RC-01,20160101-120000"
                    .equals(reasonCodeAlertBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(reasonCodeAlertBOHandle.getSite()));
            Assert.assertTrue("RT-01".equals(reasonCodeAlertBOHandle.getResourceType()));
            Assert.assertTrue("ALERT_SEQ_ID_01".equals(reasonCodeAlertBOHandle.getAlertSequenceId()));
            Assert.assertTrue("RC-01".equals(reasonCodeAlertBOHandle.getReasonCode()));
            Assert.assertTrue("20160101-120000".equals(reasonCodeAlertBOHandle.getStrt()));
        }
        // ReasonCodeGroupBOHandle
        {
            ReasonCodeGroupBOHandle reasonCodeGroupBOHandle = new ReasonCodeGroupBOHandle("JUNIT", "reasonCodeGroup");
            Assert.assertTrue("ReasonCodeGroupBO:JUNIT,reasonCodeGroup".equals(reasonCodeGroupBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(reasonCodeGroupBOHandle.getSite()));
            Assert.assertTrue("reasonCodeGroup".equals(reasonCodeGroupBOHandle.getReasonCodeGroup()));
        }
        // ReasonCodePriorityBOHandle
        {
            ReasonCodePriorityBOHandle reasonCodePriorityBOHandle = new ReasonCodePriorityBOHandle(
                    "ReasonCodeBO:JUNIT,RC-01", "PRIORITY-HIGH", "MEDIUM", "20160101-120000");
            Assert.assertTrue("ReasonCodePriorityBO:ReasonCodeBO:JUNIT,RC-01,PRIORITY-HIGH,MEDIUM,20160101-120000"
                    .equals(reasonCodePriorityBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(reasonCodePriorityBOHandle.getSite()));
            Assert.assertTrue("RC-01".equals(reasonCodePriorityBOHandle.getReasonCode()));
            Assert.assertTrue("PRIORITY-HIGH".equals(reasonCodePriorityBOHandle.getPriority()));
            Assert.assertTrue("MEDIUM".equals(reasonCodePriorityBOHandle.getSubPriority()));
            Assert.assertTrue("20160101-120000".equals(reasonCodePriorityBOHandle.getStrt()));
        }
        // ReasonCodeResStateBOHandle
        {
            ReasonCodeResStateBOHandle reasonCodeResStateBOHandle = new ReasonCodeResStateBOHandle(
                    "ReasonCodeBO:JUNIT,reasonCode", "ResourceStateBO:JUNIT,state", "20160628-23595959");
            Assert.assertTrue("ReasonCodeResourceStateBO:ReasonCodeBO:JUNIT,reasonCode,ResourceStateBO:JUNIT,state,20160628-23595959"
                    .equals(reasonCodeResStateBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(reasonCodeResStateBOHandle.getSite()));
            Assert.assertTrue("reasonCode".equals(reasonCodeResStateBOHandle.getReasonCode()));
            Assert.assertTrue("state".equals(reasonCodeResStateBOHandle.getResourceState()));
            Assert.assertTrue("20160628-23595959".equals(reasonCodeResStateBOHandle.getStrt()));
        }
        // ResourceStateBOHandle
        {
            ResourceStateBOHandle resourceStateBOHandle = new ResourceStateBOHandle("JUNIT", "state");
            Assert.assertTrue("ResourceStateBO:JUNIT,state".equals(resourceStateBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(resourceStateBOHandle.getSite()));
            Assert.assertTrue("state".equals(resourceStateBOHandle.getState()));
        }
        // ReasonCodeRuleBOHandle
        {
            ReasonCodeRuleBOHandle reasonCodeRuleBOHandle = new ReasonCodeRuleBOHandle(
                    "ReasonCodePriorityBO:ReasonCodeBO:JUNIT,reasonCode", "rule");
            Assert.assertTrue("ReasonCodeRuleBO:ReasonCodePriorityBO:ReasonCodeBO:JUNIT,reasonCode,rule"
                    .equals(reasonCodeRuleBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(reasonCodeRuleBOHandle.getSite()));
            Assert.assertTrue("reasonCode".equals(reasonCodeRuleBOHandle.getReasonCode()));
            Assert.assertTrue("rule".equals(reasonCodeRuleBOHandle.getRule()));
        }
        // ReasonCodeRuleParamBOHandle
        {
            ReasonCodeRuleParamBOHandle reasonCodeRuleParamBOHandle = new ReasonCodeRuleParamBOHandle(
                    "JUNIT","ReasonCodeRuleBO:ReasonCodePriorityBO:ReasonCodeBO:TEST_R,SD10,B,2,20160701.000000,FILTER", "*", "PA01");
            Assert.assertTrue(
                    "ReasonCodeRuleParamBO:JUNIT,ReasonCodeRuleBO:ReasonCodePriorityBO:ReasonCodeBO:TEST_R,SD10,B,2,20160701.000000,FILTER,*,PA01"
                            .equals(reasonCodeRuleParamBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(reasonCodeRuleParamBOHandle.getSite()));
            Assert.assertTrue("SD10".equals(reasonCodeRuleParamBOHandle.getReasonCode()));
            Assert.assertTrue("B".equals(reasonCodeRuleParamBOHandle.getPriority()));
            Assert.assertTrue("2".equals(reasonCodeRuleParamBOHandle.getSubPriority()));
            Assert.assertTrue("20160701.000000".equals(reasonCodeRuleParamBOHandle.getStrt()));
            Assert.assertTrue("FILTER".equals(reasonCodeRuleParamBOHandle.getRule()));
            Assert.assertTrue("*".equals(reasonCodeRuleParamBOHandle.getResourceType()));
            Assert.assertTrue("PA01".equals(reasonCodeRuleParamBOHandle.getParamID()));
        }
        // ResourceAlertLogBOHandle
        {
            ResourceAlertLogBOHandle resourceAlertLogBOHandle = new ResourceAlertLogBOHandle("Test");
            Assert.assertTrue("Test".equals(resourceAlertLogBOHandle.getValue()));
        }
        // ResourceMeUserBOHandle
        {
            ResourceMeUserBOHandle resourceMeUserBOHandle = new ResourceMeUserBOHandle("ResourceBO:JUNIT,RESRCE",
                    "meUserID", "shift");
            Assert.assertTrue("ResourceMeUserBO:ResourceBO:JUNIT,RESRCE,meUserID,shift"
                    .equals(resourceMeUserBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(resourceMeUserBOHandle.getSite()));
            Assert.assertTrue("RESRCE".equals(resourceMeUserBOHandle.getResrce()));
            Assert.assertTrue("meUserID".equals(resourceMeUserBOHandle.getMeUseId()));
            Assert.assertTrue("shift".equals(resourceMeUserBOHandle.getShift()));
        }
        // ResourcePlcBOHandle
        {
            ResourcePlcBOHandle resourcePlcBOHandle = new ResourcePlcBOHandle("JUNIT", "ResourceBO:JUNIT,RES-01", "ADDR.001",
                    "10000");
            Assert.assertTrue(
                    "ResourcePlcBO:JUNIT,ResourceBO:JUNIT,RES-01,ADDR.001,10000".equals(resourcePlcBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(resourcePlcBOHandle.getSite()));
            Assert.assertTrue("RES-01".equals(resourcePlcBOHandle.getResrce()));
            Assert.assertTrue("ADDR.001".equals(resourcePlcBOHandle.getPlcAddress()));
            Assert.assertTrue("10000".equals(resourcePlcBOHandle.getPlcValue()));
        }
        // ResourcePlcInfoBOHandle
        {
            ResourcePlcInfoBOHandle resourcePlcInfoBOHandle = new ResourcePlcInfoBOHandle("JUNIT", "ResourceBO:JUNIT,RESRCE");
            Assert.assertTrue("ResourcePlcInfoBO:JUNIT,ResourceBO:JUNIT,RESRCE".equals(resourcePlcInfoBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(resourcePlcInfoBOHandle.getSite()));
            Assert.assertTrue("RESRCE".equals(resourcePlcInfoBOHandle.getResrce()));
        }
        // ResourceReasonCodeLogBOHandle
        {
            ResourceReasonCodeLogBOHandle resourceReasonCodeLogBOHandle = new ResourceReasonCodeLogBOHandle("JUNIT",
                    "ResourceBO:JUNIT,RESRCE", "20160628-23595959");
            Assert.assertTrue("ResourceReasonCodeLogBO:JUNIT,ResourceBO:JUNIT,RESRCE,20160628-23595959"
                    .equals(resourceReasonCodeLogBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(resourceReasonCodeLogBOHandle.getSite()));
            Assert.assertTrue("RESRCE".equals(resourceReasonCodeLogBOHandle.getResrce()));
            Assert.assertTrue("20160628-23595959".equals(resourceReasonCodeLogBOHandle.getStrt()));
        }
        // ResourceReasonLogBOHandle
        {
            ResourceReasonLogBOHandle resourceReasonLogBOHandle = new ResourceReasonLogBOHandle("test");
            Assert.assertTrue("ResourceReasonLogBO:test".equals(resourceReasonLogBOHandle.getValue()));
        }
        // ResourceTimeLogBOHandle
        {
            ResourceTimeLogBOHandle resourceTimeLogBOHandle = new ResourceTimeLogBOHandle(
                    "JUNIT", "ResourceBO:JUNIT,RESRCE", "20160628-23595959");
            Assert.assertTrue(
                    "ResourceTimeLogBO:JUNIT,ResourceBO:JUNIT,RESRCE,20160628-23595959".equals(resourceTimeLogBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(resourceTimeLogBOHandle.getSite()));
            Assert.assertTrue("RESRCE".equals(resourceTimeLogBOHandle.getResrce()));
            Assert.assertTrue("20160628-23595959".equals(resourceTimeLogBOHandle.getStrt()));
        }
        // ResourceTypeBOHandle
        {
            ResourceTypeBOHandle resourceTypeBOHandle = new ResourceTypeBOHandle("JUNIT", "RESOURCETYPE");
            Assert.assertTrue("ResourceTypeBO:JUNIT,RESOURCETYPE".equals(resourceTypeBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(resourceTypeBOHandle.getSite()));
            Assert.assertTrue("RESOURCETYPE".equals(resourceTypeBOHandle.getResourceType()));
        }
        // ResourceTypeResourceBOHandle
        {
            ResourceTypeResourceBOHandle resourceTypeResourceBOHandle = new ResourceTypeResourceBOHandle(
                    "ResourceTypeBO:JUNIT,RESOURCETYPE", "ResourceBO:JUNIT,RESRCE");
            Assert.assertTrue("ResourceTypeResourceBO:ResourceTypeBO:JUNIT,RESOURCETYPE,ResourceBO:JUNIT,RESRCE"
                    .equals(resourceTypeResourceBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(resourceTypeResourceBOHandle.getSite()));
            Assert.assertTrue("RESOURCETYPE".equals(resourceTypeResourceBOHandle.getResourceType()));
            Assert.assertTrue("RESRCE".equals(resourceTypeResourceBOHandle.getResrce()));
        }
        // ScheduledDownPlanBOHandle
        {
            ScheduledDownPlanBOHandle scheduledDownPlanBOHandle = new ScheduledDownPlanBOHandle("JUNIT",
                    "ResourceBO:JUNIT,RESRCE", "20160628-23595959");
            Assert.assertTrue("ScheduledDownPlanBO:JUNIT,ResourceBO:JUNIT,RESRCE,20160628-23595959"
                    .equals(scheduledDownPlanBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(scheduledDownPlanBOHandle.getSite()));
            Assert.assertTrue("RESRCE".equals(scheduledDownPlanBOHandle.getResrce()));
            Assert.assertTrue("20160628-23595959".equals(scheduledDownPlanBOHandle.getStrt()));
        }
        // StdCycleTimeBOHandle
        {
            Date now = new Date();
            StdCycleTimeBOHandle stdCycleTimeBOHandle = new StdCycleTimeBOHandle("JUNIT", "MAT-01", "OP-01",
                    "RES-01", Utils.datetimeMsToStr(now));
            Assert.assertTrue(("StdCycleTimeBO:JUNIT,MAT-01,OP-01,RES-01," + Utils.datetimeMsToStr(now)).equals(stdCycleTimeBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(stdCycleTimeBOHandle.getSite()));
            Assert.assertTrue("MAT-01".equals(stdCycleTimeBOHandle.getItem()));
            Assert.assertTrue("OP-01".equals(stdCycleTimeBOHandle.getOperation()));
            Assert.assertTrue("RES-01".equals(stdCycleTimeBOHandle.getResrce()));
            Assert.assertTrue(Utils.datetimeMsToStr(now).equals(stdCycleTimeBOHandle.getStrt()));
        }
        // UserGroupMemberBOHandle
        {
            UserGroupMemberBOHandle userGroupMemberBOHandle = new UserGroupMemberBOHandle(
                    "UserGroupBO:JUNIT,USER-GROUP-01", "UserBO:JUNIT,USER-01");
            Assert.assertTrue("UserGroupMemberBO:UserGroupBO:JUNIT,USER-GROUP-01,UserBO:JUNIT,USER-01"
                    .equals(userGroupMemberBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(userGroupMemberBOHandle.getSite()));
            Assert.assertTrue("USER-GROUP-01".equals(userGroupMemberBOHandle.getUserGroup()));
            Assert.assertTrue("USER-01".equals(userGroupMemberBOHandle.getUser()));
        }
        // WorkCenterBOHandle
        {
            WorkCenterBOHandle workCenterBOHandle = new WorkCenterBOHandle("JUNIT", "workCenter");
            Assert.assertTrue("WorkCenterBO:JUNIT,workCenter".equals(workCenterBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(workCenterBOHandle.getSite()));
            Assert.assertTrue("workCenter".equals(workCenterBOHandle.getWorkCenter()));
        }
        // WorkCenterMemberBOHandle
        {
            WorkCenterMemberBOHandle workCenterMemberBOHandle = new WorkCenterMemberBOHandle(
                    "WorkCenterBO:JUNIT,workCenter", "WorkCenterBO:JUNIT,workCenter");
            Assert.assertTrue("WorkCenterMemberBO:WorkCenterBO:JUNIT,workCenter,WorkCenterBO:JUNIT,workCenter"
                    .equals(workCenterMemberBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(workCenterMemberBOHandle.getSite()));
            Assert.assertTrue("workCenter".equals(workCenterMemberBOHandle.getWorkCenter()));
        }
        // WorkCenterShiftBOHandle
        {
            WorkCenterShiftBOHandle workCenterShiftBOHandle = new WorkCenterShiftBOHandle(
                    "WorkCenterBO:JUNIT,workCenter", "shift");
            Assert.assertTrue(
                    "WorkCenterShiftBO:WorkCenterBO:JUNIT,workCenter,shift".equals(workCenterShiftBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(workCenterShiftBOHandle.getSite()));
            Assert.assertTrue("shift".equals(workCenterShiftBOHandle.getShift()));
        }
        // ClientLoginLogBOHandle
        {
            Date now = new Date();
            ClientLoginLogBOHandle clientLoginLogBOHandle = new ClientLoginLogBOHandle(
                    "UserBO:JUNIT,USER-01", "ResrceBO:JUNIT,RES-01", Utils.datetimeMsToStr(now));
            String handle = "ClientLoginLogBO:UserBO:JUNIT,USER-01,ResrceBO:JUNIT,RES-01" + "," + Utils.datetimeMsToStr(now);
            Assert.assertTrue(handle.equals(clientLoginLogBOHandle.getValue()));
            Assert.assertTrue("JUNIT".equals(clientLoginLogBOHandle.getSite()));
            Assert.assertTrue("USER-01".equals(clientLoginLogBOHandle.getUser()));
            Assert.assertTrue("RES-01".equals(clientLoginLogBOHandle.getResrce()));
            Assert.assertTrue(Utils.datetimeMsToStr(now).equals(clientLoginLogBOHandle.getStrt()));
        }
    }

}
