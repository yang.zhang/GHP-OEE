package mhp.oee.kafka;

import kafka.producer.KeyedMessage;
import mhp.oee.common.exception.TestRollBackException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by SAP-GW06 on 2017/4/25.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
public class ProducterTest {

    @Autowired
    private kafka.javaapi.producer.Producer<String, String> kafkaProducer;

    private String kafkaTopic = "TOPIC_ANDON_RESOURCE_PARAM-LOG_001";

    @Test
    public void test(){
//        String msg = "{\n" +
//                "    \"timestamp\":\"2017-02-14 10:40:03.183\",\n" +
//                "    \"site\":\"2001\",\n" +
//                "    \"resrce\":\"WXXX1092\",\n" +
//                "    \"plcAddress\":\"ND0001\",\n" +
//                "    \"resourceState\":\"D\",\n" +
//                "    \"plcValue\":\"100\"\n" +
//                "}";
//        String msg = "{\n" +
//                "\"items\":  \n" +
//                "   [\n" +
//                "   { \n" +
//                "            \"timestamp\":\"2017-04-10 09:21:03.183\",\n" +
//                "            \"site\":\"2001\",\n" +
//                "            \"resrce\":\"WXXX0001\",\n" +
//                "            \"actionCode\":\"START\",\n" +
//                "            \"plcAddress\":\"ND001\",\n" +
//                "            \"reasonCode\":\"SD09\",\n" +
//                "            \"plcValue\":\"42\",\n" +
//                "            \"maintenanceNotification\":\"111\",\n" +
//                "            \"maintenanceOrder\":\"\",\n" +
//                "            \"meUserId\":\"\"       \n" +
//                "    }\n" +
//                "    ]\n" +
//                "}";
//        String msg = "{\n" +
//                "    \"items\":[\n" +
//                "        {\n" +
//                  "            \"timestamp\":\"2017-04-10 09:21:03.183\",\n" +
//                "            \"site\":\"2001\",\n" +
//                "            \"resrce\":\"WXXX0001\",\n" +
//                "            \"plcAddressAlert\":\"DX0001\",\n" +
//                "            \"plcValueAlert\":\"11\",\n" +
//                "            \"resourceState\":\"D\",\n" +
//                "            \"plcAddress\":\"ND0001\",\n" +
//                "            \"plcValue\":\"45\"\n" +
//                "        },\n" +
//                "        {\n" +
//                "            \"timestamp\":\"2017-04-10 09:48:13.521\",\n" +
//                "            \"site\":\"2001\",\n" +
//                "            \"resrce\":\"WXXX0001\",\n" +
//                "            \"plcAddressAlert\":\"DX0001\",\n" +
//                "            \"resourceState\":\"D\",\n" +
//                "            \"plcValueAlert\":\"1\",\n" +
//                "            \"plcAddress\":\"ND0001\",\n" +
//                "            \"plcValue\":\"100\"\n" +
//                "        }\n" +
//                "    ]\n" +
//                "}";
//        String msg = "{\n" +
//                "    \"site\":\"2001\",\n" +
//                "    \"timestamp\":\"2017-04-10 09:21:03.183\",\n" +
//                "    \"resrce\":\"WXXX0001\",\n" +
//                "    \"qtyYield\":100,\n" +
//                "    \"qtyScrap\":10,\n" +
//                "    \"qtyInput\":200,\n" +
//                "    \"qtyTotal\":100\n" +
//                "}";

        String msg = "{\n" +
                "    \"timestamp\":\"2017-05-17 12:23:03.183\",\n" +
                "    \"site\":\"1030\",\n" +
                "    \"resource_type\":\"xxx\",\n" +
                "    \"resource\":\"XXX0003\",\n" +
                "    \"processesParamKV\":[\n" +
                "        {\n" +
                "            \"CCC\":\"12\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"DDD\":\"3456\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"uploadDatetime\":\"2017-05-17 12:00:00\"\n" +
                "}";

        kafkaProducer.send(new KeyedMessage<String, String>(kafkaTopic, msg));


        while (true){

        }

    }

}
