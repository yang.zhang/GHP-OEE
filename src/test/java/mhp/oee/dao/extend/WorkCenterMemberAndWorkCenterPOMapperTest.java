package mhp.oee.dao.extend;

import mhp.oee.BaseDaoTest;
import mhp.oee.po.gen.WorkCenterPO;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DingLJ on 2017/4/27 027.
 */
public class WorkCenterMemberAndWorkCenterPOMapperTest extends BaseDaoTest{

    @Autowired
    private WorkCenterMemberAndWorkCenterPOMapper workCenterMemberAndWorkCenterPOMapper;

    @Test
    public void test_selectLineByWorkCenterAndSites(){

        List<String> siteList = new ArrayList<String>();
        siteList.add("2001");
        siteList.add("1030");

        List<String> workAreaList = new ArrayList<String>();
        workAreaList.add("WorkCenterBO:2001,010");
        workAreaList.add("22");

        String[] lines = new String[]{};


        List<WorkCenterPO> list = this.workCenterMemberAndWorkCenterPOMapper.selectLineByWorkCenterAndSites(siteList,workAreaList,lines);


        if(list != null){

            for (WorkCenterPO p : list){
                System.out.println(ToStringBuilder.reflectionToString(p));
            }


        }

    }

}
