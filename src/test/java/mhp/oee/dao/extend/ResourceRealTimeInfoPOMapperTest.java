package mhp.oee.dao.extend;

import mhp.oee.BaseDaoTest;
import mhp.oee.enumClass.LightColor;
import mhp.oee.enumClass.ResourceRealTimeDimension;
import mhp.oee.po.extend.ResourceRealTimeDetailPO;
import mhp.oee.po.extend.ResourceRealTimeInfoPO;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Test;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DingLJ on 2017/4/26 026.
 */
public class ResourceRealTimeInfoPOMapperTest extends BaseDaoTest{


    @Autowired
    private ResourceRealTimeInfoPOMapper resourceRealTimeInfoPOMapper;

    @Test
    public void test_select(){

        ResourceRealTimeDimension dimension = ResourceRealTimeDimension.LINE_AREA;

        List<String> siteList = new ArrayList<String>();
        siteList.add("2001");


        List<String> workAreaList = new ArrayList<String>();
//        workAreaList.add("1");

        List<String> lineAreaList = new ArrayList<>();
//        lineAreaList.add("333");
//        lineAreaList.add("444");

        List<String> resourceTypeList = new ArrayList<>();
//        resourceTypeList.add("5555");
//        resourceTypeList.add("6666");

        List<ResourceRealTimeInfoPO> list = this.resourceRealTimeInfoPOMapper.select(dimension,siteList,null,lineAreaList,resourceTypeList);


        if(list != null){

            for (ResourceRealTimeInfoPO p : list){
                System.out.println(ToStringBuilder.reflectionToString(p));
            }

        }


    }

    @Test
    public void test_selectRealTimeDetail(){


        LightColor color = LightColor.GREEN;

        List<String> siteList = new ArrayList<String>();
        siteList.add("2001");


        List<String> workAreaList = new ArrayList<String>();
//        workAreaList.add("1");

        List<String> lineAreaList = new ArrayList<>();
//        lineAreaList.add("333");
//        lineAreaList.add("444");

        List<String> resourceTypeList = new ArrayList<>();
//        resourceTypeList.add("5555");
//        resourceTypeList.add("6666");

        List<ResourceRealTimeDetailPO> list = this.resourceRealTimeInfoPOMapper.selectRealTimeDetail(color,siteList,workAreaList,lineAreaList,resourceTypeList);


        if(list != null){

            for (ResourceRealTimeDetailPO p : list){
                System.out.println(ToStringBuilder.reflectionToString(p));
            }

        }

    }

}
