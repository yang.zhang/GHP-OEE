package mhp.oee.dao.extend;

import mhp.oee.BaseDaoTest;
import mhp.oee.dao.gen.WorkCenterPOMapper;
import mhp.oee.po.gen.WorkCenterPO;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DingLJ on 2017/4/27 027.
 */
public class WorkCenterPOMapperTest extends BaseDaoTest{

    @Autowired
    private WorkCenterPOMapper workCenterPOMapper;

    @Test
    public void test_selectWorkArea(){

        List<String> siteList = new ArrayList<>();
        siteList.add("2001");
        siteList.add("1030");

        String[] descArr = new String[]{};



        List<WorkCenterPO> list = this.workCenterPOMapper.selectWorkCenter(siteList,descArr);

        if(list != null ){
            for (WorkCenterPO p : list){
                System.out.println(ToStringBuilder.reflectionToString(p));
            }
        }


    }
}
