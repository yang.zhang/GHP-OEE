package mhp.oee.dao.extend;

import com.google.gson.Gson;
import mhp.oee.common.exception.TestRollBackException;
import mhp.oee.common.handle.OperationParamBOHandle;
import mhp.oee.po.extend.ResourceParamMapperPO;
import mhp.oee.po.extend.ResourceParamPO;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by DingLJ on 2017/4/17 017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:/config/spring/appcontext-*.xml" })
public class ResourceParamPOMapperTest {


    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private ResourceParamPOMapper resourceParamPOMapper;

    @Test
    public void test_batchInsertResourceParams(){


//        List<ResourceParamPO> list = new ArrayList<ResourceParamPO>();
//
//
//        ResourceParamPO p1 = new ResourceParamPO();
//        p1.setSite("1030");
//        p1.setResource("x001");
//        p1.setParamKey("FMTEMPZ4");
//        p1.setParamValue("123");
//        p1.setCreateDatetime(new Date());
//
//
//        ResourceParamPO p2 = new ResourceParamPO();
//        p2.setSite("1030");
//        p2.setResource("x002");
//        p2.setParamKey("FMTEMPZ5");
//        p2.setParamValue("456");
//        p2.setCreateDatetime(new Date());
//
//
//
//        list.add(p1);
//        list.add(p2);
//
//        int updates = this.resourceParamPOMapper.batchInsertResourceParams(list);



        String site = "1030";
        String resourceType = "xx";
        String resource = "xx002";
        String key = "TestKey";
        String value = "789";
        Date uploadDatetime = new Date();
        Date now = new Date();

        int updates = this.resourceParamPOMapper.insert(site,resourceType,resource,key,value,uploadDatetime,now);



        System.out.println("[updates = " + updates + " ]");

    }

    @Test
    public void test_insertParamMapper(){

        ResourceParamMapperPO resourceParamMapperPO = new ResourceParamMapperPO();

        resourceParamMapperPO.setSite("1030");
        resourceParamMapperPO.setResourceType("xxx003");
        resourceParamMapperPO.setParamName("温度");
        resourceParamMapperPO.setParamKey("TEM");
        OperationParamBOHandle handle = new OperationParamBOHandle(resourceParamMapperPO.getSite(),resourceParamMapperPO.getResourceType(),resourceParamMapperPO.getParamKey());
        resourceParamMapperPO.setHandle(handle.getValue());

        int updates = this.resourceParamPOMapper.insertMapper(resourceParamMapperPO);
        System.out.println(updates);

    }

    @Test
    public void test_selectParamMappers(){

        String site = null;
        String resourceType = null;
        String key = "xxx03";


        List<ResourceParamMapperPO> list = this.resourceParamPOMapper.selectMappers(site,resourceType,key);



        if(list != null){

            for ( ResourceParamMapperPO p : list ){
                System.out.println(new Gson().toJson(p));
            }

        }



    }

}
