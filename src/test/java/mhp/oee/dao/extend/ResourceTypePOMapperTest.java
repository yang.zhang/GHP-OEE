package mhp.oee.dao.extend;

import mhp.oee.BaseDaoTest;
import mhp.oee.dao.gen.ResourceTypePOMapper;
import mhp.oee.po.gen.ResourceTypePO;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DingLJ on 2017/4/27 027.
 */
public class ResourceTypePOMapperTest extends BaseDaoTest {


    @Autowired
    private ResourceTypePOMapper resourceTypePOMapper;


    @Test
    public void test_selectBySites(){


        List<String> siteList = new ArrayList<String>();

        siteList.add("2001");
        siteList.add("2001");

        List<ResourceTypePO> list = this.resourceTypePOMapper.selectBySites(siteList);

        if(list != null){

            for (ResourceTypePO p : list){
                System.out.println(ToStringBuilder.reflectionToString(p));
            }

        }

    }

}
