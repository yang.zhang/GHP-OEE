var AppModuleName = "AndonApp"; 
var utilsModule = angular.module('utilsModule', []);
var loginModule = angular.module('loginModule', []); 
var homeModule = angular.module('homeModule', []); 
var userModule = angular.module('userModule', []); 
var plcModule = angular.module('plcModule', []); 
var workModule = angular.module('workModule', []); 
var reasonCodeModule = angular.module('reasonCodeModule', []); 
var theoryPPMModule = angular.module('theoryPPMModule', []); 
var outputTargetModule = angular.module('outputTargetModule', []); 
var plannedStopModule = angular.module('plannedStopModule', []); 
var yieldMaintainModule = angular.module('yieldMaintainModule', []); 
var statementModule = angular.module('statementModule', []);
var alertModule = angular.module('alertModule', []); 
var reverseModule = angular.module('reverseModule', []);
var dataPickerModule= angular.module('dataPickerModule', []);
var moveModule= angular.module('moveModule', []);
var dropdownModule= angular.module('dropdownModule', []);
var clientModule = angular.module('clientModule', []);

var AppModule = angular
	.module(AppModuleName, [  
	// 'ngAnimate',
	// 'angular-echarts',
	'echarts-ng',
	'ngTouch',
	'frapontillo.bootstrap-switch',
	'ui.grid', 'ui.grid.pagination', 'ui.grid.cellNav', 'ui.grid.edit', 
	'ui.grid.rowEdit', 'ui.grid.validate', 'ui.grid.autoResize', 'ui.grid.selection','ui.grid.pinning',
	'ui.select', 'ngSanitize',
	'ui.grid.selection',
	'ui.grid.resizeColumns',
	'ngStorage',
	'ui.router', 
	'ui.bootstrap',
	'ui.bootstrap.modal',
	'ui.bootstrap.collapse',
	'ui.bootstrap.tooltip',
	'ngFileUpload',
	'oc.lazyLoad', 
	'utilsModule',
	'loginModule',
	'homeModule',
	'userModule',
	'plcModule',
	'workModule',
	'reasonCodeModule',
	'theoryPPMModule',
	'outputTargetModule',
	'plannedStopModule',
	'yieldMaintainModule',
	'statementModule',
	'reverseModule',
	'alertModule',
	'dataPickerModule',
	'moveModule',
	'dropdownModule',
	'clientModule'
]);

angular
	.module(AppModuleName)
	.run(["$rootScope", "$timeout", "$state", "UtilsService", function ($rootScope, $timeout, $state, UtilsService){
		
		$rootScope.rootConfig = { 
			currentLang: ROOTCONFIG.AndonConfig.currentLang,
			userInfo: {
				name: '王世峰',
				title: '人力资源'
			}
		};
		// $timeout(function (){
		// 	$state.go("andon.home");
		// }, 10000);
		$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParam){
			if(toState.name != 'andon.home'){
				if(angular.isUndefined(fromState) || fromState == null || UtilsService.isEmptyString(fromState.name)){
					$timeout(function (){
						$state.go("andon.home");
					}, 0);
					// $state.go("andon.home");
				}
			}			
		});
		

		$rootScope.rootDatas = { };

	}]);

angular
	.module(AppModuleName)
	.config([
		"$stateProvider",
		"$urlRouterProvider",
		'$ocLazyLoadProvider',
		'uiSelectConfig',
		function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, uiSelectConfig) {

		// $urlRouterProvider.otherwise("/andon/NetworkInfo/network");
		// $urlRouterProvider.otherwise("/andon/ReasonCode/ReasonCodeAndDeviceStatus");
		$urlRouterProvider.otherwise("/andon/homepage");
		uiSelectConfig.theme = 'bootstrap';  // bootstrap select2  selectize

		$ocLazyLoadProvider.config({
			debug: true
		});
		/**
		 * 定义所有模块的路由
		 */
		$stateProvider
			.state('login', {
				url: '/login',
				templateUrl: 'modules/login/login.html',
				controller: 'loginCtrl'
			})
			.state('andon', {
				abstract: true,
				url: '/andon',
				templateUrl: 'modules/homePage/absHomePage.html',
				controller: 'absHomePageCtrl'
			})
			.state('andon.home', {
				url: '/homepage',
				templateUrl: 'modules/homePage/HomeMain.html',
				controller: 'homePageCtrl'
			})
			.state('andon.UserManager',{  
				url:'/UserManager',
				templateUrl:'modules/user/userManager/UserManager.html',
				controller:'userManagerCtrl'
			})
			.state('andon.UserGroupManager',{
				url:'/UserGroupManager',
				templateUrl:'modules/user/userGroupManager/UserGroupManager.html',
				controller:'userGroupManagerCtrl'
			})
			.state('andon.UserGroupManager.BasicInfo',{
				url:'/BasicInfo',
				templateUrl:'modules/user/userGroupManager/basicInfo/BasicInfo.html',
				controller:'basicInfoCtrl'
			})
			.state('andon.UserGroupManager.Privilege',{
				url:'/Privilege',
				templateUrl:'modules/user/userGroupManager/privilege/Privilege.html',
				controller:'privilegeCtrl'
			})
			.state('andon.FactoryManager',{
				url:'/FactoryManager',
				templateUrl:'modules/user/factoryManager/FactoryManager.html',
				controller:'factoryManagerCtrl'
			})
			.state('andon.UserGroupManager.platformsPrivilege',{
				url:'/platformsPrivilege',
				templateUrl:'modules/user/userGroupManager/platformsPrivilege/platformsPrivilege.html',
				controller:'platformsPrivilegeCtrl'
			})
			.state('andon.UsersUpload', {
				url: '/UserUpload',
				templateUrl: 'modules/user/upload/upload.html',
				controller: 'UserUploadCtrl'
			})

			/// PLC 模块路由定义：
			.state('andon.plcDataCategoryManager',{
				url:'/plcDataCategoryManager',
				templateUrl:'modules/plc/dataType/typeManager.html',
				controller:'PlcDataTypeCtrl'
			})
			.state('andon.plcDataItemManager',{
				url:'/plcDataItemManager',
				templateUrl:'modules/plc/dataItem/DataItemManager.html',
				controller:'dataItemManagerCtrl'
			})
			.state('andon.plcProtocolTypeManager', {
				url: '/plcProtocolTypeManager',
				templateUrl:'modules/plc/protocolType/protocolTypeManager.html',
				controller: 'protocolTypeManagerCtrl'
			})
			.state('andon.alertComment', {
				url: '/alertComment',
				templateUrl:'modules/alert/comment/alertComment.html',
				controller: 'alertCommentController'
			})
			.state('andon.NetworkInfo',{
				url:'/NetworkInfo',
				templateUrl:'modules/plc/deviceMaintain/NetworkInfo.html',
				controller:'networkInfoCtrl'
			})
			.state('andon.NetworkInfo.network',{
				url:'/network',
				templateUrl:'modules/plc/deviceMaintain/network/network.html',
				controller:'networkCtrl'
			})
			.state('andon.NetworkInfo.AddressInfo',{
				url:'/AddressInfo',
				templateUrl:'modules/plc/deviceMaintain/addressInfo/AddressInfo.html',
				controller:'addressInfoCtrl'
			})
			.state('andon.NetworkInfo.AddInfoPart',{
				url:'/AddInfoPart',
				templateUrl:'modules/plc/deviceMaintain/addInfoPart/AddInfoPart.html',
				controller:'addInfoPartCtrl'
			})
			.state('andon.TreatyTypeManager',{
				url:'/TreatyTypeManager',
				templateUrl:'modules/plc/treatyType/TreatyTypeManager.html',
				controller:'treatyTypeManagerCtrl'
			})
			.state('andon.BasicWork',{
				url:'/BasicWork',
				templateUrl:'modules/work/basicWork/BasicWork.html',
				controller:'basicWorkCtrl'
			})
			.state('andon.BasicWork.WorkInfo',{
				url:'/WorkInfo',
				templateUrl:'modules/work/basicWork/workInfo/WorkInfo.html',
				controller:'workInfoCtrl'
			})
			.state('andon.BasicWork.WorkParam',{
				url:'/WorkParam',
				templateUrl:'modules/work/basicWork/workParams/WorkParam.html',
				controller:'workParamCtrl'
			})
			.state('andon.BasicWork.WorkGroup',{
				url:'/WorkGroup',
				templateUrl:'modules/work/basicWork/workGroup/WorkGroup.html',
				controller:'workGroupCtrl'
			}) 
			.state('andon.WorkGroupManager',{
				url:'/WorkGroupManager',
				templateUrl:'modules/work/workGroupManager/WorkGroupManager.html',
				controller:'workGroupManagerCtrl'
			}) 
			.state('andon.ClientWorkManager',{ 
				url:'/ClientWorkManager',
				templateUrl:'modules/work/clientWorkManager/ClientWorkManager.html',
				controller:'clientWorkManagerCtrl'
			})
			.state('andon.WorkParamDesign',{
				url:'/WorkParamDesign',
				templateUrl:'modules/work/workParamDesign/WorkParamDesign.html',
				controller:'workParamDesignCtrl'
			})
			.state('andon.ReasonCode',{
				url:'/ReasonCode',
				templateUrl:'modules/reasonCode/ReasonCode.html',
				controller:'reasonCodeCtrl'
			})
			/*.state('andon.jobAndServerNode',{
				url:'/jobAndServerNode',
				templateUrl:'modules/job/jobAndServerNodeManager.html',
				controller:'jobAndServerNodeManagerCtrl'
			})*/
			.state('andon.jobManager',{
				url:'/jobManager',
				templateUrl:'modules/job/jobManager/jobManager.html',
				controller:'jobManagerCtrl'
			})
			/*.state('andon.job.serverNode',{
				url:'/job/serverNode',
				templateUrl:'modules/job/serverNode.html',
				controller:'serverNodeCtrl'
			})*/
			.state('andon.ReasonCode.ReasonCodeGroup',{
				url:'/ReasonCodeGroup',
				templateUrl:'modules/reasonCode/ReasonCodeGroup/ReasonCodeGroup.html',
				controller:'reasonCodeGroupCtrl'
			}) 
			.state('andon.ReasonCode.ReasonCodeMaintain',{
				url:'/ReasonCodeMaintain',
				templateUrl:'modules/reasonCode/reasonCodeMaintain/ReasonCodeMaintain.html',
				controller:'reasonCodeMaintainCtrl'
			}) 
			.state('andon.ReasonCode.ReasonCodeAndDeviceStatus',{
				url:'/ReasonCodeAndDeviceStatus',
				templateUrl:'modules/reasonCode/reasonCodeAndDeviceStatus/ReasonCodeAndDeviceStatus.html',
				controller:'reasonCodeAndDeviceStatusCtrl'
			}) 
			.state('andon.ReasonCodeRulesManager', {
				url:'/ReasonCodeRulesManager',
				templateUrl:'modules/reasonCode/identifyRulesManager/rulemanager.html',
				controller:'reasonCodeRulesManagerCtrl'
			})

			.state('andon.TheoryPPM',{
				url:'/TheoryPPM',
				templateUrl:'modules/theoryPPM/TheoryPPM.html',
				controller:'theoryPPMCtrl'
			})
			.state('andon.TheoryPPM.GroupMaintain',{
				// url:'/GroupMaintain',
				url:'/QueryManager',
				templateUrl:'modules/theoryPPM/groupMaintain/GroupMaintain.html',
				controller:'groupMaintainCtrl'
			}).state('andon.TheoryPPM.QueryManager',{
				// url:'/QueryManager',
				url:'/GroupMaintain',
				templateUrl:'modules/theoryPPM/queryManager/QueryManager.html',
				controller:'queryManagerCtrl'
			})
			.state('andon.OutputTarget',{
				url:'/OutputTarget',
				templateUrl:'modules/outputTarget/OutputTarget.html',
				controller:'outputTargetCtrl'
			})
			.state('andon.PlannedStop',{
				url:'/PlannedStop',
				templateUrl:'modules/plannedStop/PlannedStop.html',
				controller:'plannedStopCtrl'
			})
			.state('andon.YieldMaintain',{
				url:'/YieldMaintain',
				templateUrl:'modules/yieldMaintain/YieldMaintain.html',
				controller:'yieldMaintainCtrl'
			})

			// WEB045 中 报警信息原因代码对照关系管理 
			.state('andon.AlertAndReasonCode',{
				url:'/AlertAndReasonCode',
				templateUrl:'modules/alert/reasonCode/alertAndReasonCode.html',
				controller:'alertAndReasonCodeCtrl'
			})
			.state('andon.AlertAndReasonCode.CurrentEffect',{
				url:'/CurrentEffect',
				templateUrl:'modules/alert/reasonCode/currentEffect/CurrentEffect.html',
				controller:'currentEffectCtrl'
			})
			.state('andon.AlertAndReasonCode.HistoryDatas',{
				url:'/HistoryDatas',
				templateUrl:'modules/alert/reasonCode/historyDatas/HistoryDatas.html',
				controller:'historyDatasCtrl'
			})
			.state('andon.Statement',{
				url:'/Statement',
				templateUrl:'modules/statement/Statement.html',
				controller:'statementCtrl'
			})

			//设备状态报表
			.state('andon.Statement.DeviceStatusStatement',{
				url:'/DeviceStatusStatement',
				templateUrl:'modules/statement/deviceStatusStatement/DeviceStatusStatement.html',
				controller:'deviceStatusStatementCtrl'
			})
			//报警备注看板
			.state('andon.Statement.AlarmRemarkStatement',{
				url:'/AlarmRemarkStatement',
				templateUrl:'modules/statement/alarmRemarkStatement/alarmRemarkStatement.html',
				controller:'AlarmRemarkStatementCtrl'
			})
			//设备实时报表
			.state('andon.Statement.DeviceRealTimeStatement',{
				url:'/DeviceRealTimeStatement',
				templateUrl:'modules/statement/deviceRealTimeStatement/DeviceRealTimeStatement.html',
				controller:'deviceRealTimeStatementCtrl'
			})
			//设备宏观状态看板
			.state('andon.Statement.DeviceGlobalStatusStatement',{
				url:'/DeviceGlobalStatusStatement',
				templateUrl:'modules/statement/DeviceGlobalStatusStatement/deviceGlobalStatusStatement.html',
				controller:'DeviceGlobalStatusController'
			})
			//产出报表
			.state('andon.OutputStatement',{
				url:'/OutputStatement',
				templateUrl:'modules/statement/outputStatement/OutputStatement.html',
				controller:'outputStatementCtrl'
			})
			//OEE报表
			.state('andon.OEEStatement',{
				url:'/OEEStatement',
				templateUrl:'modules/statement/oeeStatement/OEEStatement.html',
				controller:'OEEStatementCtrl'
			})
			//PPM报表
			.state('andon.PPMStatement',{
				url:'/PPMStatement',
				templateUrl:'modules/statement/PPMStatement/PPMStatement.html',
				controller:'PPMStatementCtrl'
			})
			.state('andon.Echart',{
				url:'/Echart',
				templateUrl:'modules/statement/echart/echart.html',
				controller:'echartCtrl'
			})
			.state('andon.Statement.OEEDetail',{	
				url:'/OEEDetail',
				templateUrl:'modules/statement/OEEDetail/OEEDetail.html',
				controller:'OEEDetailCtrl'
			})
			.state('andon.Statement.RSDetail',{
				url:'/RSDetail',
				templateUrl:'modules/statement/RSDetail/RSDetail.html',
				controller:'RSDetailCtrl'
			})
			//PPM数据导出报表
			.state('andon.ppmExportStatement',{
				url:'/ppmExportStatement',
				templateUrl:'modules/statement/ppmExportStatement/ppmExportStatement.html',
				controller:'ppmExportStatementCtrl'
			})
			//停机原因柏拉图报表
			.state('andon.Statement.shutdownReason', {
                    url: '/shutdownReason',
                    templateUrl: 'modules/statement/shutdownReason/shutdownReason.html',
                    controller: 'shutdownReasonCtrl'
                })
            //生产微计划报表
            .state('andon.Statement.produceMicroPlan', {
                    url: '/produceMicroPlan',
                    templateUrl: 'modules/statement/produceMicroPlan/produceMicroPlan.html',
                    controller: 'produceMicroPlanCtrl'
                })
             //OEE趋势分析报表
            .state('andon.Statement.oeeTrendAnalysisStatement', {
                    url: '/oeeTrendAnalysisStatement',
                    templateUrl: 'modules/statement/oeeTrendAnalysisStatement/oeeTrendAnalysisStatement.html',
                    controller: 'oeeTrendAnalysisStatementCtrl'
                })
             //报警时间&次数对比分析报表
            .state('andon.Statement.alarmTimeAndCount', {
                    url: '/alarmTimeAndCount',
                    templateUrl: 'modules/statement/alarmTimeAndCount/alarmTimeAndCount.html',
                    controller: 'alarmTimeAndCountController'
               })
             //办公电脑访问_故障隐患通知单创建
            .state('andon.faultAndHiddenTroubleNotice', {
                    url: '/faultAndHiddenTroubleNotice',
                    templateUrl: 'modules/client/faultAndHiddenTroubleNotice/faultAndHiddenTroubleNotice.html',
                    controller: 'faultAndHiddenTroubleNoticeController'
                })
             //办公电脑访问_通知单工单处理
            .state('andon.officeComputerWorkOrderProcess', {
                    url: '/officeComputerWorkOrderProcess',
                    templateUrl: 'modules/client/officeComputerWorkOrderProcess/officeComputerWorkOrderProcess.html',
                    controller: 'officeComputerWorkOrderProcessController'
                })
			//CM维修结果分析录入
			.state('andon.batchCreateWorkOrder', {
				url: '/batchCreateWorkOrder',
				templateUrl: 'modules/client/batchCreateWorkOrder/batchCreateWorkOrder.html',
				controller: 'batchCreateWorkOrderController'
			});
	}]);

/**
 * 定义日期 filter, 用于首页头部的时实日期展示
 */
angular
	.module(AppModuleName)
	.filter("adHeaderBarTimeFilter", [function(){
		return function(timeDate){
			return timeDate.pattern("yyyy年MM月dd日 EEE HH:mm");
		}
	}]);

/**
 * 定义搜索结果高亮显示的 filter, 主要用于ui-select的搜索功能支持
 */
angular
	.module(AppModuleName)
	.filter('highlight', ['$sce', function($sce){
		return function(text, searchStr){
			if(!searchStr || searchStr==""){
				return $sce.trustAsHtml(text);
			}
			if(angular.isUndefined(text)){
			    return "";
			}
			text = text.toString();
			if (text.toLowerCase().indexOf(searchStr.toLowerCase()) == -1) {
			    return text;
			}
			var regex = new RegExp(searchStr, 'gi');
			var result = text.replace(regex, '<span style="color:red;">$&</span>');
			return $sce.trustAsHtml(result);
		}
	}]);

/**
 * 用于展示时的日期转换：默认格式为 yyyy-MM-dd
 */
angular
	.module(AppModuleName)
	.filter('inputDateFormat', ['UtilsService', '$filter', function (UtilsService, $filter){
		return function(text, formatStr){
			var fStr = formatStr;
			if(UtilsService.isEmptyString(fStr)){
				fStr = "yyyy-MM-dd";
			}
			var textType = typeof text;
			if( textType.toLowerCase() === "string" ){
				return text;
			}else if( textType.toLowerCase() === "object" ){
				return $filter('date')(text, fStr);
				// return text.pattern(fStr);
			}
			return text;
		}
	}]);

/**
 * 定义 BodyCtrl ,用于定义一些公共的方法 和 变量$scope.bodyConfig
 */
angular
	.module(AppModuleName)
	.controller('BodyCtrl', [
		'$scope', '$rootScope', '$timeout', 'i18nService','$state', '$location',
		'HttpAppService', '$q', '$uibModal', 'UtilsService','homeService',
		'$document',
		function ($scope, $rootScope, $timeout, i18nService,$state, $location,
			HttpAppService, $q, $uibModal, UtilsService,homeService,
				  $document) {

		$scope.bodyConfig = {
			historyCache:[],
			openLeftSlider: true,    // 左侧栏是否打开
			userName: '王世峰',		 
			userCode: '',            // 用于传给设备状态实时看板 
			userTitle: '人力资源',

			showLoginBut:true, //是否显示右上角注销按钮

			showContentModel : false,  // 是否正在显示局部遮罩 
			keepContentMsg : null,      // 是否正在显示全局遮罩
			showBodyModel : false,
			keepBodyMsg : null,

			currentTime: null,
			navToggle:false,//导航栏折叠
			alertIsClosed:false,//600ms后关闭警告框

			//往上伸缩用到的参数
			animateOut:false,
			animateSelect:false,


			enabled_version : ROOTCONFIG.AndonConfig.enabled_version,
			alerts: [
				//{ type: 'danger', msg: '危险提示' },
				//{ type: 'success', msg: '成功提示' }
			],
			factories: [
				{
					name: '新能源A号工厂'
				},
				{
					name: '新能源B号工厂'
				},
				{
					name: '新能源C号工厂'
				}
			],	
			currentFactory: null, //当前工厂 
			factoryListCopy : [], //备份工厂列表
			factoryList: [
				{id: 0, name: '请选择工厂', code: 0, icon: 'ion-ios-keypad' },
				{id: 1, name: '工厂1000', code: 1000, icon: 'ion-compose' },
		        {id: 2, name: '工厂2000', code: 2000, icon: 'ion-edit' },
		        {id: 3, name: '工厂3000', code: 3000, icon: 'ion-heart-broken' },
		        {id: 4, name: '工厂4000', code: 4000, icon: 'ion-scissors' },
		        {id: 5, name: '工厂5000', code: 5000, icon: 'ion-paper-airplane' }
			],	
			functionSearch: '',  // 左侧栏搜索内容
			functionsListDefaults : [],
			//functionsListDefaults1 : [],
			functionsListDefaults1: [
				{
					title: 'PLC数据维护', isGroup: true, active: false,
					childs: [
						{   title: 'PLC数据类型管理', distState: '#/andon/plcDataCategoryManager',
							distStateName:['andon.plcDataCategoryManager'],isGroup: false, active: false },  // andon.plcDataTypeManager
						{	title: 'PLC数据项管理',distState: '#/andon/plcDataItemManager',
							distStateName:['andon.plcDataItemManager'],isGroup: false, active: false },
						{	title: 'PLC协议类型管理', distState: '#/andon/plcProtocolTypeManager',
							distStateName:['andon.plcProtocolTypeManager'],isGroup: false, active: false },
						{	title: 'PLC地址管理', distState: '#/andon/NetworkInfo/network',
							distStateName:['andon.NetworkInfo.network','andon.NetworkInfo.AddressInfo',
								'andon.NetworkInfo.AddInfoPart'],
							isGroup: false, active: false }
					]
				},
				{
					title: '信息配置管理', isGroup: true, active: false,
					childs: [
						{ title: '用户管理', distState: '#/andon/UserManager',
							distStateName:['andon.UserManager'],active: false },
						{ title: '用户组管理', distState: '#/andon/UserGroupManager/BasicInfo',
							distStateName:['andon.UserGroupManager.BasicInfo',
								'andon.UserGroupManager.platformsPrivilege',
								'andon.UserGroupManager.Privilege'],active: false },
						{ title: '作业管理', distState: '#/andon/BasicWork/WorkInfo',
							distStateName:['andon.BasicWork.WorkInfo',
								'andon.BasicWork.WorkParam',
								'andon.BasicWork.WorkGroup'],active: false },
						{ title: '作业组管理', distState: '#/andon/WorkGroupManager',
							distStateName:['andon.WorkGroupManager'],active: false },
						{ title: '作业参数值设定', distState: '#/andon/WorkParamDesign',
							distStateName:['andon.WorkParamDesign'],active: false },
						{ title: '客户端作业管理', distState: '#/andon/ClientWorkManager',
							distStateName:['andon.ClientWorkManager'],active: false },
						{ title: '工厂管理', distState: '#/andon/FactoryManager',
							distStateName:['andon.FactoryManager'],active: false },
						{ title: '原因代码管理', distState: '#/andon/ReasonCode/ReasonCodeGroup',
							distStateName:['andon.ReasonCode.ReasonCodeGroup',
								'andon.ReasonCode.ReasonCodeMaintain',
								'andon.ReasonCode.ReasonCodeAndDeviceStatus'],active: false },
						{ title: '原因代码识别规则管理', distState: '#/andon/ReasonCodeRulesManager',
							distStateName:['andon.ReasonCodeRulesManager'],active: false },
						{ title: '报警信息与原因代码', distState: '#/andon/AlertAndReasonCode/CurrentEffect',
							distStateName:['andon.AlertAndReasonCode.CurrentEffect',
								'andon.AlertAndReasonCode.HistoryDatas'],active: false },
						{ title: '理论PPM', distState: '#/andon/TheoryPPM/GroupMaintain',
							distStateName:['andon.TheoryPPM.GroupMaintain',
								'andon.TheoryPPM.QueryManager',],active: false },
						{ title: '产出目标值', distState: '#/andon/OutputTarget',
							distStateName:['andon.OutputTarget'],active: false },
						{ title: '计划停机维护', distState: '#/andon/PlannedStop',
							distStateName:['andon.PlannedStop'],active: false },
						{ title: '产量维护', distState: '#/andon/YieldMaintain',
							distStateName:['andon.YieldMaintain'],active: false },
						{ title: '用户上传', distState: '#/andon/UserUpload',
							distStateName:['andon.UsersUpload'],active: false }
					]
				},
				{
					title: '报表看板', isGroup: true, active: false,
					childs: [
						{  title: '设备状态明细报表', distState: '#/andon/Statement/DeviceStatusStatement',
							distStateName:['andon.Statement.DeviceStatusStatement'],active: false },
						{  title: '报警备注看板', distState: '#/andon/Statement/AlarmRemarkStatement',
							distStateName:['andon.Statement.AlarmRemarkStatement'],active: false },
						{  title: '设备实时报表', distState: '#/andon/Statement/DeviceTimelyStatement',
							distStateName:['andon.Statement.DeviceTimelyStatement'],active: false },
						{  title: '设备宏观状态看板', distState: '#/andon/Statement/DeviceGlobalStatusStatement',
							distStateName:['andon.Statement.DeviceGlobalStatusStatement'],active: false },
						{  title: '产出报表', distState: '#/andon/OutputStatement',
							distStateName:['andon.OutputStatement'],active: false },
						{  title: 'OEE报表', distState: '#/andon/OEEStatement',
							distStateName:['andon.OEEStatement'],active: false },
						{  title: 'PPM报表', distState: '#/andon/PPMStatement',
							distStateName:['andon.PPMStatement'],active: false },
						{ title: 'OEE明细报表', distState: '#/andon/Statement/OEEDetail',
								distStateName:['andon.Statement.OEEDetail'],active: false },
						{ title: '设备状态明细报表', distState: '#/andon/Statement/RSDetail',
								distStateName:['andon.Statement.RSDetail'],active: false },
						{ title: 'PPM数据导出报表', distState: '#/andon/ppmExportStatement',
							distStateName:['andon.ppmExportStatement'],active: false },
						{title: '停机原因柏拉图报表', distState: '#/andon/shutdownReason',
			                    distStateName: ['andon.shutdownReason'], active: false},

					]
				}
			],
			functionsList: [],
			disState : [
				{   title: 'PLC数据类型管理', distState: '#/andon/plcDataCategoryManager',
					distStateName:['andon.plcDataCategoryManager'],isGroup: false, active: false },  // andon.plcDataTypeManager
				{	title: 'PLC数据项管理',distState: '#/andon/plcDataItemManager',
					distStateName:['andon.plcDataItemManager'],isGroup: false, active: false },
				{	title: 'PLC协议类型管理', distState: '#/andon/plcProtocolTypeManager',
					distStateName:['andon.plcProtocolTypeManager'],isGroup: false, active: false },
				{	title: 'PLC地址管理', distState: '#/andon/NetworkInfo/network',
					distStateName:['andon.NetworkInfo.network','andon.NetworkInfo.AddressInfo',
						'andon.NetworkInfo.AddInfoPart'],
					isGroup: false, active: false },
				{ title: '用户管理', distState: '#/andon/UserManager',
					distStateName:['andon.UserManager'],active: false },
				{ title: '用户组管理', distState: '#/andon/UserGroupManager/BasicInfo',
					distStateName:['andon.UserGroupManager.BasicInfo',
						'andon.UserGroupManager.platformsPrivilege',
						'andon.UserGroupManager.Privilege'],active: false },
				{ title: '作业管理', distState: '#/andon/BasicWork/WorkInfo',
					distStateName:['andon.BasicWork.WorkInfo',
						'andon.BasicWork.WorkParam',
						'andon.BasicWork.WorkGroup'],active: false },
				{ title: '作业组管理', distState: '#/andon/WorkGroupManager',
					distStateName:['andon.WorkGroupManager'],active: false },
				{ title: '客户端作业管理', distState: '#/andon/ClientWorkManager',
					distStateName:['andon.ClientWorkManager'],active: false },
				{ title: '工厂管理', distState: '#/andon/FactoryManager',
					distStateName:['andon.FactoryManager'],active: false },
				/*{ title: 'job管理', distState: '#/andon/jobAndServerNode',
					distStateName:['andon.jobAndServerNode'],active: false },*/
				{ title: 'job管理', distState: '#/andon/jobManager',
					distStateName:['andon.jobManager'],active: false },
				/*{ title: 'serverNode', distState: '#/andon/job/serverNode',
					distStateName:['andon.job.serverNode'],active: false },*/
				{ title: '原因代码管理', distState: '#/andon/ReasonCode/ReasonCodeGroup',
					distStateName:['andon.ReasonCode.ReasonCodeGroup',
						'andon.ReasonCode.ReasonCodeMaintain',
						'andon.ReasonCode.ReasonCodeAndDeviceStatus'],active: false },
				{ title: '原因代码识别规则管理', distState: '#/andon/ReasonCodeRulesManager',
					distStateName:['andon.ReasonCodeRulesManager'],active: false },
				{ title: '报警信息与原因代码', distState: '#/andon/AlertAndReasonCode/CurrentEffect',
					distStateName:['andon.AlertAndReasonCode.CurrentEffect',
						'andon.AlertAndReasonCode.HistoryDatas'],active: false },
				{ title: '理论PPM', distState: '#/andon/TheoryPPM/GroupMaintain',
					distStateName:['andon.TheoryPPM.GroupMaintain',
						'andon.TheoryPPM.QueryManager',],active: false },
				{ title: '产出目标值', distState: '#/andon/OutputTarget',
					distStateName:['andon.OutputTarget'],active: false },
				{ title: '计划停机维护', distState: '#/andon/PlannedStop',
					distStateName:['andon.PlannedStop'],active: false },
				{ title: '产量维护', distState: '#/andon/YieldMaintain',
					distStateName:['andon.YieldMaintain'],active: false },
				{  title: '设备状态明细报表', distState: '#/andon/Statement/DeviceStatusStatement',
					distStateName:['andon.Statement.DeviceStatusStatement'],active: false },
				{  title: '报警备注看板', distState: '#/andon/Statement/AlarmRemarkStatement',
					distStateName:['andon.Statement.AlarmRemarkStatement'],active: false },
				{  title: '设备宏观状态看板', distState: '#/andon/Statement/DeviceGlobalStatusStatement',
					distStateName:['andon.Statement.DeviceGlobalStatusStatement'],active: false },
				{  title: '设备实时报表', distState: '#/andon/Statement/DeviceRealTimeStatement',
					distStateName:['andon.Statement.DeviceRealTimeStatement'],active: false },
				{  title: '产出报表', distState: '#/andon/OutputStatement',
					distStateName:['andon.OutputStatement'],active: false },
				{  title: 'OEE报表', distState: '#/andon/OEEStatement',
					distStateName:['andon.OEEStatement'],active: false },
				{  title: 'PPM报表', distState: '#/andon/PPMStatement',
					distStateName:['andon.PPMStatement'],active: false },
				{ title: 'OEE明细报表', distState: '#/andon/Statement/OEEDetail',
						distStateName:['andon.Statement.OEEDetail'],active: false },
				{ title: '设备状态明细报表', distState: '#/andon/Statement/RSDetail',
						distStateName:['andon.Statement.RSDetail'],active: false },
				{ title: 'PPM数据导出报表', distState: '#/andon/ppmExportStatement',
							distStateName:['andon.ppmExportStatement'],active: false },
                {title: '停机原因柏拉图报表', distState: '#/andon/Statement/shutdownReason',
                    distStateName: ['andon.Statement.shutdownReason'], active: false},
                {title: '生产微计划报表', distState: '#/andon/Statement/produceMicroPlan',
                    distStateName: ['andon.Statement.produceMicroPlan'], active: false},
                {title: 'OEE趋势分析报表', distState: '#/andon/Statement/oeeTrendAnalysisStatement',
                    distStateName: ['andon.Statement.oeeTrendAnalysisStatement'], active: false},
                {title: '报警时间&次数对比分析报表', distState: '#/andon/Statement/alarmTimeAndCount',
                    distStateName: ['andon.Statement.alarmTimeAndCount'], active: false},
				{ title: '作业参数值设定', distState: '#/andon/WorkParamDesign',
					distStateName:['andon.AlertAndReasonCode.CurrentEffect',
						'andon.WorkParamDesign'],active: false },
				{ title: '用户上传', distState: '#/andon/UserUpload',
					distStateName:['andon.UsersUpload'],active: false },
				{ title: '报警备注管理', distState: '#/andon/alertComment',
					distStateName:['andon.alertComment'],active: false },
				{ title: '故障隐患通知单创建', distState: '#/andon/faultAndHiddenTroubleNotice',
					distStateName:['andon.faultAndHiddenTroubleNotice'],active: false },
				{ title: '通知单工单处理', distState: '#/andon/officeComputerWorkOrderProcess',
					distStateName:['andon.officeComputerWorkOrderProcess'],active: false },
				{ title: '批量创建工单', distState: '#/andon/batchCreateWorkOrder',
					distStateName:['andon.batchCreateWorkOrder'],active: false },
			],

			leftListAnalyOk: false
		};

		/**
		 * 
		 * [init Body Controller的初始化代码]
		 * 
		 */
		$scope.init = function (){
			$scope.bodyConfig.debugVersion = ROOTCONFIG.AndonConfig.debugVersion;
			$scope.bodyConfig.functionsList = angular.copy($scope.bodyConfig.functionsListDefaults);
			i18nService.setCurrentLang($rootScope.rootConfig.currentLang);

			__initDateTimePicker();

			$scope.bodyConfig.currentTime = new Date();
			var delayTime = (60-$scope.bodyConfig.currentTime.getSeconds())*1000;
			UtilsService.log(angular.toJson($scope.bodyConfig.functionsList,true));
			$timeout(function(){
				__updateNowTime();
			}, delayTime);
		};

		//test linkPlc 返回上位机
		$scope.linkPlc=function(){
			if(window.localStorage.plcIp){
				window.location.href="plcIndex.html";
			}else {
				window.location.href="clientIndex.html";
			}

		};

		function __initDateTimePicker(){
			jQuery.datetimepicker.setLocale('zh');
		}

		$scope.init();

		/**
		 * 
		 * [当前页面内容未保存，提示是否退出]
		 * 
		 */
		$scope.$on("$stateChangeStart",function(event, toState, toParams, fromState, fromParam){
			var fromI = 0;
			var fromJ = 0;
			for(var i=0;i<$scope.bodyConfig.functionsList.length;i++){
				for(var j=0;j<$scope.bodyConfig.functionsList[i].childs.length;j++){
					for(var k=0;k<$scope.bodyConfig.functionsList[i].childs[j].distStateName.length;k++){
						if(fromState.name == $scope.bodyConfig.functionsList[i].childs[j].distStateName[k]){
							fromI = i;
							fromJ = j;
						}
						if(toState.name == $scope.bodyConfig.functionsList[i].childs[j].distStateName[k]){
							if($scope.bodyConfig.overallWatchChanged){
								var parent = i;
								var child =j;
								event.preventDefault();// 取消默认跳转行为
								$state.go(fromState.name);
								if(fromState.name == "andon.UserGroupManager.BasicInfo" ||
									fromState.name == "andon.UserGroupManager.platformsPrivilege" ||
									fromState.name == "andon.UserGroupManager.Privilege" ||
									fromState.name == "andon.UserManager"
									//|| fromState.name == "andon.UserGroupManager.BasicInfo" ||
									//fromState.name == "andon.UserGroupManager.BasicInfo" ||
									//fromState.name == "andon.UserGroupManager.BasicInfo" ||
									//fromState.name == "andon.UserGroupManager.BasicInfo" ||
									//fromState.name == "andon.UserGroupManager.BasicInfo" ||
									//fromState.name == "andon.UserGroupManager.BasicInfo"
								){
									var promise =UtilsService.confirm('处于编辑状态，是否退出', '提示', ['确定', '取消']);
								}else{
									var promise =UtilsService.confirm('还有修改未保存，是否退出', '提示', ['确定', '取消']);
								}
								promise.then(function(){
									console.log(fromI);
									console.log(fromJ);
									$scope.bodyConfig.functionsList[fromI].active = false;
									$scope.bodyConfig.functionsList[fromI].isGroup = false;
									$scope.bodyConfig.functionsList[fromI].childs[fromJ].active = false;

									$scope.bodyConfig.functionsList[parent].active = true;
									$scope.bodyConfig.functionsList[parent].isGroup = true;
									$scope.bodyConfig.functionsList[parent].childs[child].active = true;

									$scope.bodyConfig.overallWatchChanged = false;
									$state.go(toState.name);
								}, function(){
									//取消停留在原页面
								});
							}
						}

					};
				};
			};
		}); 
		/**
		 * 
		 * [用于监控页面跳转之后的 当前Activity 变量的更新，确保ROOTCONFIG.AndonConfig.CURRENTACTIVITY始终与当前页面相对应]
		 * 
		 */
		$scope.$on("$stateChangeSuccess", function (event2, toState, toParams, fromState, fromParam){

			for(var i=0;i<$scope.bodyConfig.functionsList.length;i++){
				for(var j=0;j<$scope.bodyConfig.functionsList[i].childs.length;j++){
					for(var k=0;k<$scope.bodyConfig.functionsList[i].childs[j].distStateName.length;k++){
						if(toState && toState.name == $scope.bodyConfig.functionsList[i].childs[j].distStateName[k]){
							$scope.bodyConfig.currentActivity = $scope.bodyConfig.functionsList[i].childs[j].activity;
							ROOTCONFIG.AndonConfig.CURRENTACTIVITY = $scope.bodyConfig.currentActivity;
							UtilsService.setHeight();
							//return;
						};
					};
				};
			};

		});

		$scope.downloadExcelTemplate = function(module){
			if(UtilsService.isEmptyString(module)){
				return;
			}
			var url;
			if(module == "ppm"){
				url = HttpAppService.URLS.DOWN_EXCEL_TEMPLATE_PPM;
			}else if(module == "user"){
				url = HttpAppService.URLS.DOWN_EXCEL_TEMPLATE_USER;
			}else if(module == "reasonCode"){
				url = HttpAppService.URLS.DOWN_EXCEL_TEMPLATE_REASONCODE;
			}else if(module == "plcAddress"){
				url = HttpAppService.URLS.DOWN_EXCEL_TEMPLATE_PLCADDRESS;
			}else if(module == "workOrder"){
				url = HttpAppService.URLS.DOWN_EXCEL_TEMPLATE_WORKORDER;
			}else{
				return;
			}
			url = url.replace("#{SITE}#", HttpAppService.getSite());
			window.open(url);
		};

		/**
		 * 
		 * [addAlert 添加提示]
		 * @param {[string]} type [提示类型:默认为warning, danger-红色、success-绿色、warning-黄色]
		 * @param {[string]} msg  [提示信息主体内容]
		 * 
		 */
		$scope.addAlert = function(type, msg) { 
			console.log(msg);
			if(UtilsService.isEmptyString(msg) && !(type == 'danger' || type == '' || type == 'success' || type == 'info' || type == '') ){
				var msg = type;
			}
			// 检测是否已经提示了，防止500、401等错误的多次提示
			for(var x = 0; x < $scope.bodyConfig.alerts.length; x++){
				if($scope.bodyConfig.alerts[x].msg == msg){
					return;
				}
			}

			var i = $scope.bodyConfig.alerts.length;
			// console.log($scope.bodyConfig.alerts.length);
			if(arguments.length == 1){ 					
				$scope.bodyConfig.alerts.push({ msg: arguments[0] }); 			
			}else{ 																
				$scope.bodyConfig.alerts.push({type: type, msg: msg}); 			
			}
		    var outTime = 3000;
		    if(type == 'danger'){
		    	outTime = 5000;
		    }
			// console.log($scope.bodyConfig.alerts.length);
			$timeout(function (){
				$scope.bodyConfig.alertIsClosed = true;
			}, outTime-500);
		    $timeout(function (){
				$scope.bodyConfig.alertIsClosed = false;
		    	$scope.closeAlert($scope.bodyConfig.alerts.length);
		    }, outTime); 
		};

		/**
		 * 
		 * 初始化日期选择控件:依据id进行初始化 
		 * @param  {String} id   需要进行初始化的日期控件元素的ID
		 * @param  {Object} opts 日期控件其他相关参数设置,默认设置如下
		 *                       {
		 *                       	showTime: false,
		 *                       	format: 'Y-m-d'
		 *                       }
		 *                       
		 */
		$scope.initDateTimePicker = function(id, opts,format,showTime,timepicker){
			console.log();
			var defaultOpts = {
				showTime: showTime,
				format: format,
				timepicker:timepicker,
			};
			var optsNew = angular.extend(angular.copy(defaultOpts), opts);
			jQuery(id).datetimepicker(defaultOpts);
		};
		
		/**
		 * 
		 * [confirm 确认框提示，提供两个按钮：<确定>、<取消>, 按钮文字可自定义]
		 * @param  {[string]} msg       [提示信息主体内容]
		 * @param  {[string]} title     [提示框标题,默认为:'提示']
		 * @param  {[array]} btnLables  [按钮文字数组:默认为['确定','取消']]
		 * @return {[promise]}          [confirm的promise, 点击左侧按钮为resolve, 右侧按钮为reject]
		 *
		 * eg:
		 * 		var promise = promise$scope.confirm('', '提示', ['确定', '取消']);
		 * 		promise.then(function(){
		 * 			// 点击了左侧按钮
		 * 		}, function(){
		 * 			// 点击了右侧按钮
		 * 		});
		 * 		
		 */
		$scope.confirm = function(msg, title, btnLables){
			return UtilsService.confirm(msg, title, btnLables);
		};	
		
		/**
		 * 
		 * [clearAlerts 清除掉所有的alert提示框]
		 * 
		 */
		$scope.clearAlerts = function(){
			$scope.bodyConfig.alerts = [];
		};	
		/**
		 * 
		 * [closeAlert 关闭掉指定alert]
		 * @param  {[int]} index [要删除的alert对应的index]
		 * 
		 */
		$scope.closeAlert = function(index) { 
			$scope.bodyConfig.alerts.splice(index-1, 1);
		}; 	
		
		/**
		 * 
		 * [factorySelectChange 改变工厂后调用该方法进行接口和界面处理]
		 * 		1、重置当前工厂相关变量 $scope.bodyConfig.currentFactory、ROOTCONFIG.AndonConfig.SITE
		 * 		2、重新加载左侧列表: __leftList
		 * 		3、回到首页
		 * @param  {[object]} $select [ui-select的$select对象]
		 * 
		 */
		$scope.factorySelectChange = function($select){
			//$scope.bodyConfig.currentFactory = $select.selected;
			//$scope.bodyConfig.factoryList = $scope.bodyConfig.factoryListCopy;
			$scope.bodyConfig.factoryList = angular.copy($scope.bodyConfig.factoryListCopy);
			ROOTCONFIG.AndonConfig.SITE = $scope.bodyConfig.currentFactory.code;
			$scope.bodyConfig.functionsListDefaults = [];
			__leftList();
			$state.go("andon.home");
		};  
		
		$scope.searchStrChanged = function(){
			var result = []; //angular.copy(functionsList);
			var searchStr = $scope.bodyConfig.functionSearch;
			console.log(searchStr);
			if(searchStr == ""){
				$scope.bodyConfig.functionsList = $scope.bodyConfig.functionsListDefaults;
				return;
			}

			angular.forEach($scope.bodyConfig.functionsListDefaults, function(father, index, array){

					if(!angular.isUndefined(father.isGroup) && father.isGroup!=null && father.isGroup){ // father是个分组
						var childs = father.childs;
						var thisFather = {
							title: father.title,
							isGroup: father.isGroup,
							active: father.active,
							distState: father.distState,
							childs: []
						};
						for(var i = 0; i < childs.length; i++){
							if(childs[i].title.toUpperCase().indexOf(searchStr.toUpperCase()) >= 0){
								thisFather.childs.push(angular.copy(childs[i]));
							}
						}
						if(thisFather.childs.length > 0 || thisFather.title.toUpperCase().indexOf(searchStr.toUpperCase())>=0){
							result.push(thisFather);
						}
					}else{ // father是个功能块，而不是组
						if(item.title.toUpperCase().indexOf(searchStr.toUpperCase()) >= 0){
							result.push(angular.copy(item));
						}
					}
			});
			$scope.bodyConfig.functionsList = result;
			for(var j = 0; j < $scope.bodyConfig.functionsList.length; j++){
				$scope.bodyConfig.functionsList[j].active = true;
			} 
		}; 

		$scope.bodyDatas = {
			userInfo: {
			}
		};

		/**
		 * 
		 * [clickChildMenuOtherSystem 用于在新窗口打开设备状态实时看板界面]
		 * 
		 */
		$scope.clickChildMenuOtherSystem = function(clickedItem){
			$scope.clickChildMenu(clickedItem);
			var url = clickedItem.distState;
			var protocol = $location.protocol(),
				server = $location.host(),
				port = $location.port(),
				site = HttpAppService.getSite();
			url = url.replace("%PROTOCOL%", protocol);
			url = url.replace("%SERVER%", server);
			url = url.replace("%PORT%", port);
			url = url.replace("%SITE%", site);

			if(url.indexOf("userId=") < 0){
				if(url.indexOf('?') > 0 && url.indexOf('?') != url.length-1){
					url += "&userId="+$scope.bodyConfig.userCode;
				}else{
					url += "?userId="+$scope.bodyConfig.userCode;
				}
			}			
			window.open(url, 'RealtimeDashboard');
		};

		$scope.needNgClickToOtherTab = function(child){
			//var need = child.distState.startsWith('http://') || child.distState.startsWith('https://') || child.distState.startsWith("%PROTOCOL%");
			//return need;
			var need = child.distState.startsWith('http://') || child.distState.startsWith('https://') || child.distState.startsWith("%PROTOCOL%");
			if(need)
			{
				return 1;
			}else{
				return 0;
			}
		};

		$scope.navToggle = function(){
			$scope.bodyConfig.navToggle = !$scope.bodyConfig.navToggle;

			UtilsService.setHeight();

			//发送广播 重新刷新echart
			$rootScope.$broadcast("refreshEchart",{});
		};
		
		/**
		 * 
		 * [clickChildMenu 点击左侧栏子菜单时触发]
		 * @param  {[object]} clickedItem [点击的了菜单对象]
		 * 
		 */
		$scope.clickChildMenu = function(clickedItem){

			var parents = $scope.bodyConfig.functionsList;
			for(var i = 0; i < parents.length; i++){
				var parent = parents[i];
				var childs = parent.childs;
				for(var j = 0; j<childs.length; j++){
					if(!$scope.bodyConfig.overallWatchChanged){
						if(childs[j].$$hashKey == clickedItem.$$hashKey){
							for(var k=0;k<$scope.bodyConfig.functionsList.length;k++){
								if($scope.bodyConfig.functionsList[i].$$hashKey == $scope.bodyConfig.functionsList[k].$$hashKey){
									$scope.bodyConfig.functionsList[k].isGroup = true;
									$scope.bodyConfig.functionsList[k].active = true;
								}else{
									$scope.bodyConfig.functionsList[k].isGroup = false;
									$scope.bodyConfig.functionsList[k].active = false;
								}
							}
							$scope.bodyConfig.functionsList[i].childs[j].active = true;
						}else{
							childs[j].active = false;
						}
					}
				}
			}

			UtilsService.setHeight();

			// clickedItem.active = true;
			if(!$scope.$$phase) {
	        	$scope.$apply();
	        }
		};

		/**
		 * 
		 * [clickParentMenu 点击左侧栏一级菜单时触发]
		 * @param  {[object]} clickedParentItem [点击的一级菜单对象]
		 * 
		 */

		$scope.clickParentMenu = function(clickedParentItem){

			var parents = $scope.bodyConfig.functionsList;
			for(var i = 0; i < parents.length; i++){
				if(parents[i].$$hashKey == clickedParentItem.$$hashKey){
					parents[i].isGroup = true;
					parents[i].active = true;

				}else{
					parents[i].isGroup = false;
					parents[i].active = false;
				}
			}

			UtilsService.setHeight();

			// clickedParentItem.active = !etmpActive;
			if(!$scope.$$phase) {
	        	$scope.$apply();
	        }
		};

		/**
		 * 
		 * [__updateNowTime 时实更新首页头部栏中的当前时间信息]
		 * @return {[type]} [description]
		 * 
		 */
		function __updateNowTime(){
			$scope.bodyConfig.currentTime = new Date();
			$timeout(function(){
				__updateNowTime();
			}, 60000);
		};

		/**
		 * 
		 * [showBodyModel 展示全局遮罩]
		 * @param  {[string]} msg [遮罩的提示信息]
		 * 
		 */
		$scope.showBodyModel = function(msg){
			$scope.bodyConfig.showBodyModel = true;
			$scope.bodyConfig.keepBodyMsg = msg;
		};
		/**
		 * 
		 * [hideBodyModel 隐藏全局遮罩]
		 * 
		 */
		$scope.hideBodyModel = function(){
			$scope.bodyConfig.showBodyModel = false;
		};
		/**
		 * 
		 * [showContentModel 显示局部遮罩]
		 * @param  {[string]} msg [遮罩提示信息]
		 * 
		 */
		$scope.showContentModel = function(msg){
			$scope.bodyConfig.showContentModel = true;
			$scope.bodyConfig.keepContentMsg = msg;
		};
		/**
		 * 
		 * [hideContentModel 隐藏局部遮罩]
		 * 
		 */
		$scope.hideContentModel = function(){
			$scope.bodyConfig.showContentModel = true;
		};  
		/**
		 * 
		 * [__leftList 请求左侧栏菜单数据]
		 * 
		 */
		function __leftList(){ 
			if(ROOTCONFIG.AndonConfig.enabled_leftList == true){
				homeService
					.userpurview()
					.then(function (resultDatas){
						UtilsService.log(angular.toJson(resultDatas,true));
						if(resultDatas.response && resultDatas.response.activityGroupVOs.length > 0){
							var current = $state.current.name;
							for(var i=0;i<resultDatas.response.activityGroupVOs.length;i++){
								$scope.bodyConfig.functionsListDefaults[i] = (
								{
									title: resultDatas.response.activityGroupVOs[i].description,
									isGroup: true,
									active: false,
									childs : [],
									icon : ""
								}
								);
								if(resultDatas.response.activityGroupVOs[i].activityGroup == "AUTH_GROUP"){
									$scope.bodyConfig.functionsListDefaults[i].icon = "fa fa-key"//权限配置管理
								}else if(resultDatas.response.activityGroupVOs[i].activityGroup == "CONFIG_GROUP"){
									$scope.bodyConfig.functionsListDefaults[i].icon = "fa fa-cog"//信息配置管理
								}else if(resultDatas.response.activityGroupVOs[i].activityGroup == "REPORT_GROUP"){
									$scope.bodyConfig.functionsListDefaults[i].icon = "fa fa-table"//实时看板
								}else if(resultDatas.response.activityGroupVOs[i].activityGroup == "REPORT_GROUP_DT"){
									$scope.bodyConfig.functionsListDefaults[i].icon = "fa fa-bar-chart"//D/T报表
								}else if(resultDatas.response.activityGroupVOs[i].activityGroup == "REPORT_GROUP_PPM"){
									$scope.bodyConfig.functionsListDefaults[i].icon = "fa fa-area-chart"//PPM报表
								}else if(resultDatas.response.activityGroupVOs[i].activityGroup == "REPORT_GROUP_YIELD"){
									$scope.bodyConfig.functionsListDefaults[i].icon = "fa fa-line-chart"//产量报表
								}else if(resultDatas.response.activityGroupVOs[i].activityGroup == "REPORT_GROUP_OEE"){
									$scope.bodyConfig.functionsListDefaults[i].icon = "fa fa-pie-chart"//OEE报表
								}else if(resultDatas.response.activityGroupVOs[i].activityGroup == "REPORT_GROUP_ALARM"){
									$scope.bodyConfig.functionsListDefaults[i].icon = "fa fa-table"//报警报表
								}else{
									$scope.bodyConfig.functionsListDefaults[i].icon = "fa fa-folder"
								}
								for(var j=0;j<resultDatas.response.activityGroupVOs[i].activityVOs.length;j++){
									$scope.bodyConfig.functionsListDefaults[i].childs[j] = {
										title: resultDatas.response.activityGroupVOs[i].activityVOs[j].description,
										distState: resultDatas.response.activityGroupVOs[i].activityVOs[j].classOrProgram,
										permPO : resultDatas.response.activityGroupVOs[i].activityVOs[j].permPO,
										activity: resultDatas.response.activityGroupVOs[i].activityVOs[j].activity,
										active: false,
										isGroup: false,
										distStateName : []
									}
									//console.log($scope.bodyConfig.functionsListDefaults[i].childs[j]);
									for(var k=0;k<$scope.bodyConfig.disState.length;k++){
										if(resultDatas.response.activityGroupVOs[i].activityVOs[j].classOrProgram == $scope.bodyConfig.disState[k].distState){
											$scope.bodyConfig.functionsListDefaults[i].childs[j].distStateName = $scope.bodyConfig.disState[k].distStateName;
										}
									}
								}
							}
							for(var i=0;i<$scope.bodyConfig.functionsListDefaults.length;i++){
								for(var j=0;j<$scope.bodyConfig.functionsListDefaults[i].childs.length;j++){
									for(var k=0;k<$scope.bodyConfig.functionsListDefaults[i].childs[j].distStateName.length;k++){
										if($scope.bodyConfig.functionsListDefaults[i].childs[j].distStateName[k] == current){
											$scope.bodyConfig.functionsListDefaults[i].childs[j].active = true;
											$scope.bodyConfig.functionsListDefaults[i].active = true;
											$scope.bodyConfig.functionsListDefaults[i].isGroup = true;
										}
									}
								}
							}
							$scope.init();
							$scope.bodyConfig.leftListAnalyOk = true;
						}
					},function (resultDatas){ //TODO 检验失败
						$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
					});
			}else{
				// console.log("000");
				$scope.bodyConfig.functionsListDefaults = $scope.bodyConfig.functionsListDefaults1
				$scope.init();
			} 
		}   

		/**
		 * 
		 * 获取用户信息数据，获取成功后，依据用户信息去获取左侧栏菜单列表和默认工厂
		 * 
		 */
		homeService
			.userinfos()
			.then(function (resultDatas){
				if(resultDatas.response ) {
					if(resultDatas.response.lastName == undefined){
						resultDatas.response.lastName = "";
					}
					if(resultDatas.response.firstName == undefined){
						resultDatas.response.firstName = "";
					}
					$scope.bodyConfig.userName = resultDatas.response.lastName + resultDatas.response.firstName;
					$scope.bodyConfig.userTitle = resultDatas.response.department;
					$scope.bodyConfig.userCode = resultDatas.response.user;
					$scope.bodyConfig.currentFactory = resultDatas.response.defaultSite;
					//$rootScope.editFlag = resultDatas.response.department;
					if(ROOTCONFIG.AndonConfig.enabled_leftList){
						ROOTCONFIG.AndonConfig.SITE = resultDatas.response.defaultSite;
					}	
					homeService
						.usersitelist(resultDatas.response.user)
						.then(function (resultDatas){
							if(resultDatas.response ) {
								$scope.bodyConfig.factoryList = [];
								for(var i=0;i<resultDatas.response.length;i++){
									$scope.bodyConfig.factoryList.push({id: resultDatas.response[i].handle, name: resultDatas.response[i].description, code: resultDatas.response[i].site, icon: 'ion-ios-keypad' });
									if($scope.bodyConfig.currentFactory == resultDatas.response[i].site){
										$scope.bodyConfig.currentFactory = {id: resultDatas.response[i].handle, name: resultDatas.response[i].description, code: resultDatas.response[i].site, icon: 'ion-ios-keypad' };
									}
								}
							}
							$scope.bodyConfig.factoryListCopy = angular.copy($scope.bodyConfig.factoryList);
							//console.log($scope.bodyConfig.factoryList);
						},function (resultDatas){ //TODO 检验失败
							$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
						})
					__leftList();
				}
			},function (resultDatas){ //TODO 检验失败
				$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
			});	
		
		/**
		 * 
		 * [modulesRWFlag 依据activity所对应的state,来获取当前用户对该activity的权限信息]
		 * @param  {[string]} distState [ 要查询权限的state, eg:#/andon/NetworkInfo/network ]
		 * @return {[boolean]}          [ 当前用户对该activity是否有编辑权限 ]
		 * 
		 */
		$scope.modulesRWFlag = function(distState){
			// return true;
			var ok = $scope.bodyConfig.leftListAnalyOk;
			if(!ok){
				return false;
			}
			var infoRW = __getRwFlagHelper(distState);
			while(angular.isUndefined(infoRW) || infoRW== null || angular.isUndefined(infoRW.read)||infoRW.read==null){
				infoRW = __getRwFlagHelper(distState);
			}
			return infoRW.write;
		};

		function __getRwFlagHelper(distState){
			var infoRW = {};
			if($scope.bodyConfig.functionsListDefaults.length > 0){
				for(var i=0;i<$scope.bodyConfig.functionsListDefaults.length;i++){
					for(var j=0;j<$scope.bodyConfig.functionsListDefaults[i].childs.length;j++){
						if($scope.bodyConfig.functionsListDefaults[i].childs[j].distState == distState){
							if($scope.bodyConfig.functionsListDefaults[i].childs[j].permPO.permissionMode == "R"){
								infoRW.read = true;
								infoRW.write = false;
							}else if($scope.bodyConfig.functionsListDefaults[i].childs[j].permPO.permissionMode == "RW"){
								infoRW.read = true;
								infoRW.write = true;
							}
						}
					}
				}
				// console.log(infoRW);
				return infoRW;
			}
		}

			//为了时间控件而写的广播
		$scope.clickHideDate = function(){
			$scope.$broadcast('dateMouthHide', 'W');
		};

		}]);
