var AppModuleName = "AndonApp"; 
var utilsModule = angular.module('utilsModule', []); 
var loginModule = angular.module('loginModule', []);
var homeModule = angular.module('homeModule', []);
var userModule = angular.module('userModule', []);
var plcModule = angular.module('plcModule', []);
var workModule = angular.module('workModule', []);
var reasonCodeModule = angular.module('reasonCodeModule', []);
var theoryPPMModule = angular.module('theoryPPMModule', []);
var outputTargetModule = angular.module('outputTargetModule', []);
var plannedStopModule = angular.module('plannedStopModule', []);
var yieldMaintainModule = angular.module('yieldMaintainModule', []);
var deviceStatusStatementModule = angular.module('deviceStatusStatementModule', []);
var outputStatementModule = angular.module('outputStatementModule', []);
var alertModule = angular.module('alertModule', []);

var reverseModule = angular.module('reverseModule', []);

var AppModule = angular
	.module(AppModuleName, [ 
	// 'ngAnimate',
	// 'angular-echarts',
	'echarts-ng',
	'ngTouch',
	'frapontillo.bootstrap-switch',
	'ui.grid', 'ui.grid.pagination', 'ui.grid.cellNav', 'ui.grid.edit', 
	'ui.grid.rowEdit', 'ui.grid.validate', 'ui.grid.autoResize', 'ui.grid.selection',
	'ui.select', 'ngSanitize',
	'ui.grid.selection',
	'ui.grid.resizeColumns', 
	'ngStorage',
	'ui.router', 
	'ui.bootstrap',
	'ui.bootstrap.modal',
	'ui.bootstrap.collapse',
	'ngFileUpload',
	'oc.lazyLoad', 
	'utilsModule',
	'loginModule',
	'homeModule',
	'userModule',
	'plcModule',
	'workModule',
	'reasonCodeModule',
	'theoryPPMModule',
	'outputTargetModule',
	'plannedStopModule',
	'yieldMaintainModule',
	'deviceStatusStatementModule',
	'reverseModule',
	'outputStatementModule',
	'alertModule'
]);

angular
	.module(AppModuleName)
	.run(["$rootScope", function ($rootScope) {

		$rootScope.rootConfig = { 
			currentLang: ROOTCONFIG.AndonConfig.currentLang,
			userInfo: {
				name: '王世峰',
				title: '人力资源'
			}
		};

		$rootScope.rootDatas = { };

	}]);

angular
	.module(AppModuleName)
	.config([
		"$stateProvider",
		"$urlRouterProvider",
		'$ocLazyLoadProvider',
		'uiSelectConfig',
		function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, uiSelectConfig) {

		// $urlRouterProvider.otherwise("/andon/NetworkInfo/network");
		// $urlRouterProvider.otherwise("/andon/ReasonCode/ReasonCodeAndDeviceStatus");
		$urlRouterProvider.otherwise("/andon/homepage");
		uiSelectConfig.theme = 'bootstrap';  // bootstrap select2  selectize

		$ocLazyLoadProvider.config({
			debug: true
		});
		
		$stateProvider
			.state('login', {
				url: '/login',
				templateUrl: 'modules/login/login.html',
				controller: 'loginCtrl'
			})
			.state('andon', {
				abstract: true,
				url: '/andon',
				templateUrl: 'modules/homePage/absHomePage.html',
				controller: 'absHomePageCtrl'
			})
			.state('andon.home', {
				url: '/homepage',
				templateUrl: 'modules/homePage/HomeMain.html',
				controller: 'homePageCtrl'
			})
			.state('andon.UserManager',{  
				url:'/UserManager',
				templateUrl:'modules/user/userManager/UserManager.html',
				controller:'userManagerCtrl'
			})
			.state('andon.UserGroupManager',{
				url:'/UserGroupManager',
				templateUrl:'modules/user/userGroupManager/UserGroupManager.html',
				controller:'userGroupManagerCtrl'
			})
			.state('andon.UserGroupManager.BasicInfo',{
				url:'/BasicInfo',
				templateUrl:'modules/user/userGroupManager/basicInfo/BasicInfo.html',
				controller:'basicInfoCtrl'
			})
			.state('andon.UserGroupManager.Privilege',{
				url:'/Privilege',
				templateUrl:'modules/user/userGroupManager/Privilege/Privilege.html',
				controller:'privilegeCtrl'
			})
			.state('andon.FactoryManager',{
				url:'/FactoryManager',
				templateUrl:'modules/user/factoryManager/FactoryManager.html',
				controller:'factoryManagerCtrl'
			})
			.state('andon.UserGroupManager.platformsPrivilege',{
				url:'/platformsPrivilege',
				templateUrl:'modules/user/userGroupManager/platformsPrivilege/platformsPrivilege.html',
				controller:'platformsPrivilegeCtrl'
			})

			/// PLC 模块路由定义：
			.state('andon.plcDataCategoryManager',{
				url:'/plcDataCategoryManager',
				templateUrl:'modules/plc/dataType/typeManager.html',
				controller:'PlcDataTypeCtrl'
			})
			.state('andon.plcDataItemManager',{
				url:'/plcDataItemManager',
				templateUrl:'modules/plc/dataItem/DataItemManager.html',
				controller:'dataItemManagerCtrl'
			})
			.state('andon.plcProtocolTypeManager', {
				url: '/plcProtocolTypeManager',
				templateUrl:'modules/plc/protocolType/protocolTypeManager.html',
				controller: 'protocolTypeManagerCtrl'
			})
			.state('andon.NetworkInfo',{
				url:'/NetworkInfo',
				templateUrl:'modules/plc/deviceMaintain/NetworkInfo.html',
				controller:'networkInfoCtrl'
			})
			.state('andon.NetworkInfo.network',{
				url:'/network',
				templateUrl:'modules/plc/deviceMaintain/network/network.html',
				controller:'networkCtrl'
			})
			.state('andon.NetworkInfo.AddressInfo',{
				url:'/AddressInfo',
				templateUrl:'modules/plc/deviceMaintain/addressInfo/AddressInfo.html',
				controller:'addressInfoCtrl'
			})
			.state('andon.NetworkInfo.AddInfoPart',{
				url:'/AddInfoPart',
				templateUrl:'modules/plc/deviceMaintain/addInfoPart/AddInfoPart.html',
				controller:'addInfoPartCtrl'
			})
			.state('andon.TreatyTypeManager',{
				url:'/TreatyTypeManager',
				templateUrl:'modules/plc/treatyType/TreatyTypeManager.html',
				controller:'treatyTypeManagerCtrl'
			})
			.state('andon.BasicWork',{
				url:'/BasicWork',
				templateUrl:'modules/work/basicWork/BasicWork.html',
				controller:'basicWorkCtrl'
			})
			.state('andon.BasicWork.WorkInfo',{
				url:'/WorkInfo',
				templateUrl:'modules/work/basicWork/workInfo/WorkInfo.html',
				controller:'workInfoCtrl'
			})
			.state('andon.BasicWork.WorkParam',{
				url:'/WorkParam',
				templateUrl:'modules/work/basicWork/workParams/WorkParam.html',
				controller:'workParamCtrl'
			})
			.state('andon.BasicWork.WorkGroup',{
				url:'/WorkGroup',
				templateUrl:'modules/work/basicWork/workGroup/WorkGroup.html',
				controller:'workGroupCtrl'
			}) 
			.state('andon.WorkGroupManager',{
				url:'/WorkGroupManager',
				templateUrl:'modules/work/workGroupManager/WorkGroupManager.html',
				controller:'workGroupManagerCtrl'
			}) 
			.state('andon.ClientWorkManager',{ 
				url:'/ClientWorkManager',
				templateUrl:'modules/work/clientWorkManager/ClientWorkManager.html',
				controller:'clientWorkManagerCtrl'
			}) 
			.state('andon.ReasonCode',{
				url:'/ReasonCode',
				templateUrl:'modules/reasonCode/ReasonCode.html',
				controller:'reasonCodeCtrl'
			})
			.state('andon.ReasonCode.ReasonCodeGroup',{
				url:'/ReasonCodeGroup',
				templateUrl:'modules/reasonCode/ReasonCodeGroup/ReasonCodeGroup.html',
				controller:'reasonCodeGroupCtrl'
			}) 
			.state('andon.ReasonCode.ReasonCodeMaintain',{
				url:'/ReasonCodeMaintain',
				templateUrl:'modules/reasonCode/reasonCodeMaintain/ReasonCodeMaintain.html',
				controller:'reasonCodeMaintainCtrl'
			}) 
			.state('andon.ReasonCode.ReasonCodeAndDeviceStatus',{
				url:'/ReasonCodeAndDeviceStatus',
				templateUrl:'modules/reasonCode/reasonCodeAndDeviceStatus/ReasonCodeAndDeviceStatus.html',
				controller:'reasonCodeAndDeviceStatusCtrl'
			}) 
			.state('andon.ReasonCodeRulesManager', {
				url:'/ReasonCodeRulesManager',
				templateUrl:'modules/reasonCode/identifyRulesManager/rulemanager.html',
				controller:'reasonCodeRulesManagerCtrl'
			})

			.state('andon.TheoryPPM',{
				url:'/TheoryPPM',
				templateUrl:'modules/theoryPPM/TheoryPPM.html',
				controller:'theoryPPMCtrl'
			})
			.state('andon.TheoryPPM.GroupMaintain',{
				url:'/GroupMaintain',
				templateUrl:'modules/theoryPPM/groupMaintain/GroupMaintain.html',
				controller:'groupMaintainCtrl'
			}).state('andon.TheoryPPM.QueryManager',{
				url:'/QueryManager',
				templateUrl:'modules/theoryPPM/queryManager/QueryManager.html',
				controller:'queryManagerCtrl'
			})
			.state('andon.OutputTarget',{
				url:'/OutputTarget',
				templateUrl:'modules/outputTarget/OutputTarget.html',
				controller:'outputTargetCtrl'
			})
			.state('andon.PlannedStop',{
				url:'/PlannedStop',
				templateUrl:'modules/plannedStop/PlannedStop.html',
				controller:'plannedStopCtrl'
			})
			.state('andon.YieldMaintain',{
				url:'/YieldMaintain',
				templateUrl:'modules/yieldMaintain/YieldMaintain.html',
				controller:'yieldMaintainCtrl'
			})

			// WEB045 中 报警信息原因代码对照关系管理 
			.state('andon.AlertAndReasonCode',{
				url:'/AlertAndReasonCode',
				templateUrl:'modules/alert/reasonCode/AlertAndReasonCode.html',
				controller:'alertAndReasonCodeCtrl'
			})
			.state('andon.AlertAndReasonCode.CurrentEffect',{
				url:'/CurrentEffect',
				templateUrl:'modules/alert/reasonCode/currentEffect/CurrentEffect.html',
				controller:'currentEffectCtrl'
			})
			.state('andon.AlertAndReasonCode.HistoryDatas',{
				url:'/HistoryDatas',
				templateUrl:'modules/alert/reasonCode/historyDatas/HistoryDatas.html',
				controller:'historyDatasCtrl'
			})

			//设备状态报表
			.state('andon.DeviceStatusStatement',{
				url:'/DeviceStatusStatement',
				templateUrl:'modules/statement/deviceStatusStatement/DeviceStatusStatement.html',
				controller:'deviceStatusStatementCtrl'
			})
			//产出报表
			.state('andon.OutputStatement',{
				url:'/OutputStatement',
				templateUrl:'modules/statement/outputStatement/OutputStatement.html',
				controller:'outputStatementCtrl'
			})
		;
	}]);

angular
	.module(AppModuleName)
	.filter("adHeaderBarTimeFilter", [function(){
		return function(timeDate){
			return timeDate.pattern("yyyy年MM月dd日 EEE HH:mm");
		} 
	}]); 

angular
	.module(AppModuleName)
	.filter('highlight', ['$sce', function($sce){
		return function(text, searchStr){
			if(!searchStr || searchStr==""){
				return $sce.trustAsHtml(text);
			}
			if(angular.isUndefined(text)){
			    return "";
			}
			text = text.toString();
			if (text.toLowerCase().indexOf(searchStr.toLowerCase()) == -1) {
			    return text;
			}
			var regex = new RegExp(searchStr, 'gi');
			var result = text.replace(regex, '<span style="color:red;">$&</span>');
			return $sce.trustAsHtml(result);
		}
	}]);


angular
	.module(AppModuleName)
	.controller('BodyCtrl', [
		'$scope', '$rootScope', '$timeout', 'i18nService',
		'HttpAppService', '$q', '$uibModal', 'UtilsService',
		function ($scope, $rootScope, $timeout, i18nService,
			HttpAppService, $q, $uibModal, UtilsService) { 
		
		$scope.bodyConfig = {	
			openLeftSlider: true,	
			userName: '王世峰',	
			userTitle: '人力资源',

			showLoginBut:window.localStorage.plcIp, //是否显示右上角注销按钮
			
			currentTime: null,	
			alerts: [ 
				// { type: 'danger', msg: '危险提示' },
				// { type: 'success', msg: '成功提示' }		
			],				
			factories: [
				{
					name: '新能源A号工厂'
				},
				{
					name: '新能源B号工厂'
				},
				{
					name: '新能源C号工厂'
				}	
			],	
			currentFactory: null,	
			factoryList: [
				{id: 0, name: '请选择工厂', code: 0, icon: 'ion-ios-keypad' },
				{id: 1, name: '工厂1000', code: 1000, icon: 'ion-compose' },
		        {id: 2, name: '工厂2000', code: 2000, icon: 'ion-edit' },
		        {id: 3, name: '工厂3000', code: 3000, icon: 'ion-heart-broken' },
		        {id: 4, name: '工厂4000', code: 4000, icon: 'ion-scissors' },
		        {id: 5, name: '工厂5000', code: 5000, icon: 'ion-paper-airplane' }	
			],	
			functionSearch: '',	
			functionsListDefaults: [
				{
					title: 'PLC数据维护', isGroup: true, active: false,
					childs: [
						{   title: 'PLC数据类型管理', distState: '#/andon/plcDataCategoryManager',
							distStateName:['andon.plcDataCategoryManager'],isGroup: false, active: false },  // andon.plcDataTypeManager
						{	title: 'PLC数据项管理',distState: '#/andon/plcDataItemManager',
							distStateName:['andon.plcDataItemManager'],isGroup: false, active: false },
						{	title: 'PLC协议类型管理', distState: '#/andon/plcProtocolTypeManager',
							distStateName:['andon.plcProtocolTypeManager'],isGroup: false, active: false },
						{	title: 'PLC地址管理', distState: '#/andon/NetworkInfo/network',
							distStateName:['andon.NetworkInfo.network','andon.NetworkInfo.AddressInfo',
								'andon.NetworkInfo.AddInfoPart'],
							isGroup: false, active: false }
					]
				},
				{
					title: '信息配置管理', isGroup: true, active: false,
					childs: [
						{ title: '用户管理', distState: '#/andon/UserManager',
							distStateName:['andon.UserManager'],active: false },
						{ title: '用户组管理', distState: '#/andon/UserGroupManager/BasicInfo',
							distStateName:['andon.UserGroupManager.BasicInfo',
								'andon.UserGroupManager.platformsPrivilege',
								'andon.UserGroupManager.Privilege'],active: false },
						{ title: '作业管理', distState: '#/andon/BasicWork/WorkInfo',
							distStateName:['andon.BasicWork.WorkInfo',
								'andon.BasicWork.WorkParam',
								'andon.BasicWork.WorkGroup'],active: false },
						{ title: '作业组管理', distState: '#/andon/WorkGroupManager',
							distStateName:['andon.WorkGroupManager'],active: false },
						{ title: '客户端作业管理', distState: '#/andon/ClientWorkManager',
							distStateName:['andon.ClientWorkManager'],active: false },
						{ title: '工厂管理', distState: '#/andon/FactoryManager',
							distStateName:['andon.FactoryManager'],active: false },
						{ title: '原因代码管理', distState: '#/andon/ReasonCode/ReasonCodeGroup',
							distStateName:['andon.ReasonCode.ReasonCodeGroup',
								'andon.ReasonCode.ReasonCodeMaintain',
								'andon.ReasonCode.ReasonCodeAndDeviceStatus'],active: false },
						{ title: '原因代码识别规则管理', distState: '#/andon/ReasonCodeRulesManager',
							distStateName:['andon.ReasonCodeRulesManager'],active: false },
						{ title: '报警信息与原因代码', distState: '#/andon/AlertAndReasonCode/CurrentEffect',
							distStateName:['andon.AlertAndReasonCode.CurrentEffect',
								'andon.AlertAndReasonCode.HistoryDatas'],active: false },
						{ title: '理论PPM', distState: '#/andon/TheoryPPM/GroupMaintain',
							distStateName:['andon.TheoryPPM.GroupMaintain',
								'andon.TheoryPPM.QueryManager',],active: false },
						{ title: '产出目标值', distState: '#/andon/OutputTarget',
							distStateName:['andon.OutputTarget'],active: false },
						{ title: '计划停机维护', distState: '#/andon/PlannedStop',
							distStateName:['andon.PlannedStop'],active: false },
						{ title: '产量维护', distState: '#/andon/YieldMaintain',
							distStateName:['andon.YieldMaintain'],active: false }
					]
				},
				{
					title: '报表看板', isGroup: true, active: false,
					childs: [
						{  title: '设备状态明细报表', distState: '#/andon/DeviceStatusStatement',
							distStateName:['andon.DeviceStatusStatement'],active: false },
						{  title: '产出报表', distState: '#/andon/OutputStatement',
							distStateName:['andon.OutputStatement'],active: false },
						{  title: 'test', distState: '',
							distStateName:'',active: false },
						{  title: '条目四', distState: '',
							distStateName:'',active: false },
					]
				} 
			], 
			functionsList: [] 
		};

		$scope.$on("$stateChangeSuccess", function (event2, toState, toParams, fromState, fromParam){
			for(var i=0;i<$scope.bodyConfig.functionsList.length;i++){
				for(var j=0;j<$scope.bodyConfig.functionsList[i].childs.length;j++){
					for(var k=0;k<$scope.bodyConfig.functionsList[i].childs[j].distStateName.length;k++){
						if(toState.name == $scope.bodyConfig.functionsList[i].childs[j].distStateName[k]){
							void 0;
							$scope.bodyConfig.functionsList[i].active = true;
							$scope.bodyConfig.functionsList[i].childs[j].active = true;
							break;
						};
					};
				};
			};
		});
		
		$scope.addAlert = function(type, msg) {			
			var i = $scope.bodyConfig.alerts.length;	
			if(arguments.length == 1){ 					
				$scope.bodyConfig.alerts.push({ msg: arguments[0] }); 			
			}else{ 																
				$scope.bodyConfig.alerts.push({type: type, msg: msg}); 			
			}	
		    var outTime = 1500;
		    if(type == 'danger'){
		    	outTime = 3000;
		    }
		    $timeout(function (){
		    	$scope.closeAlert(i);
		    }, outTime); 
		}; 				
		
		// var promise = promise$scope.confirm('', '提示', ['确定', '取消']);		
		$scope.confirm = function(msg, title, btnLables){
			return UtilsService.confirm(msg, title, btnLables);
		};
		// $timeout(function(){	
			// 	var pro = $scope.confirm('你好', '提示');
			// 	pro.then(function(){
			// 		console.log("跑了");
			// 	}, function(){
			// 		console.log("...");
			// 	});	
		// }, 1000);	
		
		$scope.clearAlerts = function(){
			$scope.bodyConfig.alerts = [];
		};
		$scope.closeAlert = function(index) { 
			$scope.bodyConfig.alerts.splice(index, 1); 
		}; 
		
		$scope.factorySelectChange = function($select){
			$scope.bodyConfig.currentFactory = $select.selected;
			HttpAppService.URLS.site = $scope.bodyConfig.currentFactory.code;
		};
		
		$scope.searchStrChanged = function(){
			var result = []; //angular.copy(functionsList);
			var searchStr = $scope.bodyConfig.functionSearch;
			void 0;
			if(searchStr == ""){
				$scope.bodyConfig.functionsList = $scope.bodyConfig.functionsListDefaults;
				return;
			}
			angular.forEach($scope.bodyConfig.functionsListDefaults, function(father, index, array){

					if(!angular.isUndefined(father.isGroup) && father.isGroup!=null && father.isGroup){ // father是个分组
						var childs = father.childs;
						var thisFather = {
							title: father.title,
							isGroup: father.isGroup,
							active: father.active,
							distState: father.distState,
							childs: []
						};
						for(var i = 0; i < childs.length; i++){
							if(childs[i].title.toUpperCase().indexOf(searchStr.toUpperCase()) >= 0){
								thisFather.childs.push(angular.copy(childs[i]));
							}
						}
						if(thisFather.childs.length > 0 || thisFather.title.toUpperCase().indexOf(searchStr.toUpperCase())>=0){
							result.push(thisFather);
						}
					}else{ // father是个功能块，而不是组
						if(item.title.toUpperCase().indexOf(searchStr.toUpperCase()) >= 0){
							result.push(angular.copy(item));
						}
					}
			});
			$scope.bodyConfig.functionsList = result;
			for(var j = 0; j < $scope.bodyConfig.functionsList.length; j++){
				$scope.bodyConfig.functionsList[j].active = true;
			} 
		}; 

		$scope.bodyDatas = {
			userInfo: {
			}
		};

		$scope.clickChildMenu = function(clickedItem){ 
			var parents = $scope.bodyConfig.functionsList;
			for(var i = 0; i < parents.length; i++){ 
				var parent = parents[i];
				// if(parent.active){
					var childs = parent.childs;
					for(var j = 0; j<childs.length; j++){
						if(childs[j].$$hashKey == clickedItem.$$hashKey){
							childs[j].active = true;
						}else{
							childs[j].active = false;
						}
					}
				// }
			}
			// clickedItem.active = true;
			if(!$scope.$$phase) {
	        	$scope.$apply();
	        }
		};
		$scope.clickParentMenu = function(clickedParentItem){
			// var etmpActive = angular.isUndefined(clickedParentItem.active) && clickedParentItem.active!=null &&clickedParentItem.active ? false : true;
			var parents = $scope.bodyConfig.functionsList;
			for(var i = 0; i < parents.length; i++){
				if(parents[i].$$hashKey == clickedParentItem.$$hashKey){
					parents[i].active = parents[i].active;
				}else{
					parents[i].active = false;
				}
				var childs = parents[i].childs;
				for(var j = 0; j<childs.length; j++){
					if(j==0){
						childs[j].active = true;
					}else{
						childs[j].active = false;
					}
				}
			}
			// clickedParentItem.active = !etmpActive;	
			if(!$scope.$$phase) {
	        	$scope.$apply();
	        }		
		};

		$scope.init = function (){
			$scope.bodyConfig.functionsList = angular.copy($scope.bodyConfig.functionsListDefaults);
			i18nService.setCurrentLang($rootScope.rootConfig.currentLang);
			$scope.bodyConfig.currentFactory = $scope.bodyConfig.factoryList[0];
			$scope.bodyConfig.currentTime = new Date();
			var delayTime = (60-$scope.bodyConfig.currentTime.getSeconds())*1000;
			void 0;
			$timeout(function(){
				__updateNowTime();
			}, delayTime);
		};

		$scope.init();

		function __updateNowTime(){
			void 0;
			$scope.bodyConfig.currentTime = new Date();
			$timeout(function(){
				__updateNowTime();
			}, 60000);
		}




	}]);




