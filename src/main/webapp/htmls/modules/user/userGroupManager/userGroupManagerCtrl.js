userModule.controller('userGroupManagerCtrl', [
    '$scope', '$http', '$q', 'UtilsService',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants', 'gridUtil','$uibModal','userService','$state',
    function ($scope, $http, $q, UtilsService, HttpAppService, $timeout, $interval,
              uiGridConstants, gridUtil,$uibModal,userService,$state){
        $scope.config = {
            basicWorkMenuList:[
                {
                    title:'基础信息',
                    distState:'#/andon/BasicWork/WorkInfo1',
                    distStateName:'andon.UserGroupManager.BasicInfo',
                    isActive:true
                },
                {
                    title:'平台权限',
                    distState:'#/andon/BasicWork/WorkParam2',
                    distStateName:'andon.UserGroupManager.platformsPrivilege',
                    isActive:true
                },
                {
                    title:'客户端权限',
                    distState:'#/andon/BasicWork/WorkGroup3',
                    distStateName:'andon.UserGroupManager.Privilege',
                    isActive:true
                }],
            isHidden : false,
            searchInfos : null,
            searchInfosBack : null,  // 查询成功后赋值，若与userSearch不一致，则禁用编辑按钮
            editDisabled : true,
            isNoValue : false,
            //info
            userGroupItem : [],
            userGroupAvailable : [],
            userGroupUnAvailable : [],
            userGroupItemCopy : [],
            userGroupAvailableCopy : [],
            userGroupUnAvailableCopy : [],
            //infobtn
            editBasicBtnDis : false,
            keepBasicBtnDis : false,
            cancleBasicBtnDis : false,
            description : null,
            //客户端
            selectedAvailableUserList : [],
            selectedAssignUserList : [],
            selectedAvailableUserListCopy : [],
            selectedAssignUserListCopy : [],
            editCustomersBtnDis : false,
            keepCustomersBtnDis : false,
            cancleCustomersBtnDis : false,
            //平台
            selectedPlatforms : [],
            selectedPlatformsCopy : [],
            editPlatformsBtnDis : false,
            keepPlatformsBtnDis : false,
            canclePlatformsBtnDis : false,
            editRW : false,
    }

        $scope.config.editRW = $scope.modulesRWFlag("#"+$state.$current.url.source);
        //输入框
        $scope.searchChange = function(){
            if($scope.config.searchInfos == null || $scope.config.searchInfos == ""){
                $scope.config.editDisabled = true;
                $scope.config.editBasicBtnDis = false;
                $scope.config.keepBasicBtnDis = false;
                $scope.config.cancleBasicBtnDis = false;

                $scope.config.editCustomersBtnDis = false;
                $scope.config.keepCustomersBtnDis = false;
                $scope.config.cancleCustomersBtnDis = false;

                $scope.config.editPlatformsBtnDis = false;
                $scope.config.keepPlatformsBtnDis = false;
                $scope.config.canclePlatformsBtnDis = false;
            }else{
                $scope.config.editDisabled = false;
            }
            if(!UtilsService.isEmptyString($scope.config.searchInfosBack)){
                if($scope.config.searchInfosBack.toUpperCase() != $scope.config.searchInfos.toUpperCase()){
                    $scope.config.editBasicBtnDis = false;
                    $scope.config.editPlatformsBtnDis = false;
                    $scope.config.editCustomersBtnDis = false;
                }else{
                    $scope.config.editBasicBtnDis = true;
                    $scope.config.editPlatformsBtnDis = true;
                    $scope.config.editCustomersBtnDis = true;
                }
            }
        };
//查询
        $scope.showUserGroup = function(){
            //if($scope.config.searchInfos == null){
            //    $scope.config.isNoValue = true;
            //    return;
            //}
            if($scope.config.editRW && $scope.config.cancleBasicBtnDis){
                return;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/user/userGroupManager/model/ModalParamSearchShow.html',
                controller: 'modalInfoSearchShowCtrl',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope : $scope,
                resolve:{
                    groupItems:function(){
                        return $scope.config.searchInfos;
                    }
                },

            });
            modalInstance.result.then(function (searchResults) {
                $scope.config.searchInfos = searchResults;
                $scope.searchChange();
            });
        };

        $scope.selectUserGroup = function(){

            $scope.config.editBasicBtnDis = false;
            $scope.config.keepBasicBtnDis = false;
            $scope.config.cancleBasicBtnDis = false;

            $scope.config.editCustomersBtnDis = false;
            $scope.config.keepCustomersBtnDis = false;
            $scope.config.cancleCustomersBtnDis = false;

            $scope.config.editPlatformsBtnDis = false;
            $scope.config.keepPlatformsBtnDis = false;
            $scope.config.canclePlatformsBtnDis = false;

            $scope.config.userGroupItem = [];
            $scope.config.userGroupAvailable = [];
            $scope.config.userGroupUnAvailable = [];
            $scope.config.userGroupItemCopy = [];
            $scope.config.userGroupAvailableCopy = [];
            $scope.config.userGroupUnAvailableCopy = [];
            $scope.config.selectedAvailableUserList = [];
            $scope.config.selectedAssignUserList = [];
            $scope.config.selectedAvailableUserListCopy = [];
            $scope.config.selectedAssignUserListCopy = [];
            $scope.config.selectedPlatforms = [];
            $scope.config.selectedPlatformsCopy = [];

            __userGroupSearch($scope.config.searchInfos );

        };

        $scope.editUserGroup = function(){

        }
//添加新用户
        $scope.addUserGroup = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/user/userGroupManager/model/ModalInfoSearchAdd.html',
                controller: 'modalInfoSearchAddCtrl',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope : $scope,
                resolve:{
                    items:function(){
                        return "";
                    }
                },

            });
            modalInstance.result.then(function (searchResults) {
                $scope.config.searchInfos = searchResults;
                $scope.config.editDisabled = false;
                $scope.selectUserGroup();            });
        }
        $scope.$on("$stateChangeSuccess", function (event2, toState, toParams, fromState, fromParam){
            for(var i = 0 ; i < $scope.config.basicWorkMenuList.length ; i++){
                var item = $scope.config.basicWorkMenuList;
                if(fromState && toState.name == item[i].distStateName){
                    item[i].isActive = true
                }else{
                    item[i].isActive = false;
                }
                if(fromState && toState.name == "andon.UserGroupManager.BasicInfo"){
                    $scope.config.isHidden = false;
                }else{
                    $scope.config.isHidden = true;
                }
            }
        });

        //查询
        function __userGroupSearch(user_group){
            userService
                .userGroupSearch(user_group)
                .then(function (resultDatas){
                    $scope.config.searchInfosBack = $scope.config.searchInfos;
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.userGroupItem = resultDatas.response;
                        $scope.config.userGroupItemCopy = angular.copy(resultDatas.response);
                        $scope.config.description = $scope.config.userGroupItem[0].description;
                        $scope.config.editBasicBtnDis = true;
                        $scope.config.editCustomersBtnDis = true;
                        $scope.config.editPlatformsBtnDis = true;
                        __userGroupAvailable();
                        __userGroupUnAvailable($scope.config.searchInfos);
                        __selectedAvailableUserList();
                        __selectedAssignUserList($scope.config.searchInfos);
                        __selectedPlatforms($scope.config.searchInfos);
                        return;
                    }else{
                        $scope.config.description = "";
                        $scope.addAlert("","无此用户组");
                        $scope.config.editBasicBtnDis = false;
                        $scope.config.editCustomersBtnDis = false;
                        $scope.config.editPlatformsBtnDis = false;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }
        function __userGroupAvailable(){
            userService
                .userGroupAvailable()
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.userGroupAvailable = resultDatas.response;
                        //console.log($scope.config.userGroupAvailable);
                        for(var i=0;i<$scope.config.userGroupAvailable.length;i++){
                            $scope.config.userGroupAvailable[i].isHover = false;
                            $scope.config.userGroupAvailable[i].isSelected = false;
                            $scope.config.userGroupAvailable[i].isFlag = "C";
                        }
                        $scope.config.userGroupAvailableCopy = angular.copy($scope.config.userGroupAvailable);
                        return;
                        //console.log($scope.config.userGroupUnAvailable);
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }
        function __userGroupUnAvailable(user_group){
            userService
                .userGroupUnAvailable(user_group)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.userGroupUnAvailable = resultDatas.response;
                        for(var i=0;i<$scope.config.userGroupUnAvailable.length;i++){
                            $scope.config.userGroupUnAvailable[i].isHover = false;
                            $scope.config.userGroupUnAvailable[i].isSelected = false;
                            $scope.config.userGroupUnAvailable[i].isFlag = "D";
                        }
                        //console.log($scope.config.userGroupUnAvailable);
                        $scope.config.userGroupUnAvailableCopy = angular.copy($scope.config.userGroupUnAvailable);
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }
        //客户
        function __selectedAvailableUserList(){
            userService
                .userGroupCustomerWork()
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.selectedAvailableUserList = resultDatas.response;
                        //console.log($scope.config.selectedAvailableUserList);
                        for(var i=0;i<$scope.config.selectedAvailableUserList.length;i++){
                            $scope.config.selectedAvailableUserList[i].isHover = false;
                            $scope.config.selectedAvailableUserList[i].isSelected = false;
                            $scope.config.selectedAvailableUserList[i].isFlag = "C";
                        }
                        $scope.config.selectedAvailableUserListCopy = angular.copy($scope.config.selectedAvailableUserList);
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }
        function __selectedAssignUserList(user_group){
            userService
                .userGroupCustomerUnWork(user_group)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.selectedAssignUserList = resultDatas.response;
                        for(var i=0;i<$scope.config.selectedAssignUserList.length;i++){
                            $scope.config.selectedAssignUserList[i].isHover = false;
                            $scope.config.selectedAssignUserList[i].isSelected = false;
                            $scope.config.selectedAssignUserList[i].isFlag = "D";
                        }
                        $scope.config.selectedAssignUserListCopy = angular.copy($scope.config.selectedAssignUserList);
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }

        function __selectedPlatforms(user_group){
            userService
                .userGroupPlatforms(user_group)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        for(var i=0;i<resultDatas.response.length;i++){
                            if(resultDatas.response[i].permissionSetting == true || resultDatas.response[i].permissionSetting == false
                            ||resultDatas.response[i].permissionSetting == "true" || resultDatas.response[i].permissionSetting == "false"){
                                if(resultDatas.response[i].permissionMode == "RW"){
                                    resultDatas.response[i].edit = true;
                                    resultDatas.response[i].read = true;
                                    resultDatas.response[i].viewActionCode = "U";
                                }else if(resultDatas.response[i].permissionMode == "R"){
                                    resultDatas.response[i].edit = false;
                                    resultDatas.response[i].read = true;
                                    resultDatas.response[i].viewActionCode = "U";
                                }else if (resultDatas.response[i].permissionMode == "" || resultDatas.response[i].permissionMode == undefined){
                                    resultDatas.response[i].edit = false;
                                    resultDatas.response[i].read = false;
                                    resultDatas.response[i].viewActionCode = "U";
                                }
                            }else{
                                resultDatas.response[i].edit = false;
                                resultDatas.response[i].read = false;
                                resultDatas.response[i].viewActionCode = "C";
                            }
                        }
                        //console.log(resultDatas.response);
                        $scope.config.selectedPlatforms = changeArr(resultDatas.response);
                        $scope.config.selectedPlatformsCopy = angular.copy($scope.config.selectedPlatforms);
                        //console.log($scope.config.selectedPlatforms);
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }
        var changeArr = function(Arr){
            var map = {};
            var dest = [];
            for(var i = 0; i < Arr.length; i++){
                var ai = Arr[i];
                if(!map[ai.activityGroup]){
                    if(Arr[i].activityBo != undefined){
                        dest.push({
                            activityGroup: ai.activityGroup,
                            activityGroupDes: ai.activityGroupDes,
                            isCollapsed : true,
                            detail: [ai]
                        });
                    }
                    else{
                        dest.push({
                            activityGroup: ai.activityGroup,
                            activityGroupDes: ai.activityGroupDes,
                            isCollapsed : true,
                            detail: []
                        });
                    }
                    map[ai.activityGroup] = ai;
                }else{
                    for(var j = 0; j < dest.length; j++){
                        var dj = dest[j];
                        if(dj.activityGroup == ai.activityGroup && ai.activity != undefined){
                            dj.detail.push(ai);
                            break;
                        }
                    }
                }
            }
            return dest;
        }
        $scope.upModel = function(){
            $scope.config.searchInfos = angular.uppercase($scope.config.searchInfos);
        }
    }]);
userModule.controller('modalInfoSearchAddCtrl', [
    '$scope', '$http', '$q',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants', 'gridUtil','userService','$uibModalInstance',
    function ($scope, $http, $q, HttpAppService, $timeout, $interval,
              uiGridConstants, gridUtil,userService,$uibModalInstance){
        $scope.datas = {
            customers : null,
            description : null,
            keepBtn : true
        }
        $scope.customersChange = function(){
            $scope.datas.customers = angular.uppercase($scope.datas.customers);
            if($scope.datas.customers == null){
                $scope.datas.keepBtn = true;
            }else{
                $scope.datas.keepBtn = false;
            }
        }
        $scope.cancel = function (){
            $uibModalInstance.dismiss("cancle");
        }
        $scope.saveAddInfo = function(){
            var items = [{
                "site" : HttpAppService.getSite(),
                "userGroup" : $scope.datas.customers,
                "description" : $scope.datas.description,
                "viewActionCode" : "C"
            }];
            userService
                .userGroupKeepAvailable(items)
                .then(function (resultDatas){
                    if(resultDatas.myHttpConfig.status == 200){
                        $scope.addAlert("success","保存成功");
                        $uibModalInstance.close($scope.datas.customers);
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }

    }]);

userModule.controller('modalInfoSearchShowCtrl', [
    '$scope', '$http', '$q',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants', 'gridUtil','groupItems','userService','$uibModalInstance',
    function ($scope, $http, $q, HttpAppService, $timeout, $interval,
              uiGridConstants, gridUtil,groupItems,userService,$uibModalInstance){
        $scope.gridModalSearchResult = {
            columnDefs:[
                {
                    name:"userGroup",displayName:'用户组',
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    name:"description",displayName:'用户组描述',
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }

                },
            ],
            data : [],
            onRegisterApi: __onRegisterApi
        }
        function __onRegisterApi(gridApi){
            $scope.gridApi = gridApi;
            //选择任一行或一列时触发
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
                $uibModalInstance.close(newRowCol.row.entity.userGroup);
            });
        };

        $scope.cancel = function (){
            $uibModalInstance.dismiss("cancle");
        }
        userService
            .userGroupData(groupItems)
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridModalSearchResult.data = resultDatas.response;
                    // $scope.config.deviceTypes = $scope.config.deviceTypes.concat(resultDatas.response);
                    return;
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
            });

        $scope.upModel = function(){
            $scope.config.searchInfos = angular.uppercase($scope.config.searchInfos);
        }
    }]);