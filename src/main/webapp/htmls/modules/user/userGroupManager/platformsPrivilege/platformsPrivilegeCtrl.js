userModule.controller('platformsPrivilegeCtrl', [
    '$scope', '$http', '$q',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants', 'gridUtil', 'UtilsService','userService',
    function ($scope, $http, $q, HttpAppService, $timeout, $interval,
              uiGridConstants, gridUtil, UtilsService,userService){

        //var a = $q(function () {
        //    $q('#datetimepicker1').datetimepicker({
        //        format: 'YYYY-MM-DD',
        //        locale: moment.locale('zh-cn')
        //    });
        //    $q('#datetimepicker2').datetimepicker({
        //        format: 'YYYY-MM-DD hh:mm',
        //        locale: moment.locale('zh-cn')
        //    });
        //});
        $scope.cancleBasicBtn = function(){
            $scope.bodyConfig.overallWatchChanged = false;
            $scope.config.keepPlatformsBtnDis = false;
            $scope.config.canclePlatformsBtnDis = false;
            $scope.config.selectedPlatforms = $scope.config.selectedPlatformsCopy;
            $scope.config.cancleBasicBtnDis = false;
            $scope.config.editPlatformsBtnDis = true;
        }
        $scope.editBasicBtn = function(){
            $scope.bodyConfig.overallWatchChanged = true;
            $scope.config.keepPlatformsBtnDis = true;
            $scope.config.canclePlatformsBtnDis = true;
            $scope.config.cancleBasicBtnDis = true;
            $scope.config.editPlatformsBtnDis = false;
        }
        //$scope.keepBasicBtn = function(){
        //    if($scope.gridPlannedStop.data.length>0){
        //        var promise = UtilsService.confirm(msg, title, btnLables);   eg: UtilsService.confirm('是否保存', '提示', ['确定', '取消']);
        //        promise
        //            .then(function(){ // ok
        //                $scope.keepBasicBtn1();
        //            }, function(){   // cancel
        //                return;
        //            });
        //    }
        //}
        //保存
        $scope.keepBasicBtn = function(){
            $scope.showBodyModel("正在保存");
            var itemsArr = [];
            for(var i=0;i<$scope.config.selectedPlatforms.length;i++){
                for(var j=0;j<$scope.config.selectedPlatforms[i].detail.length;j++){
                    itemsArr.push($scope.config.selectedPlatforms[i].detail[j]);
                }
            }
            var itemsArrCopy = [];
            for(var i=0;i<$scope.config.selectedPlatformsCopy.length;i++){
                for(var j=0;j<$scope.config.selectedPlatformsCopy[i].detail.length;j++){
                    itemsArrCopy.push($scope.config.selectedPlatformsCopy[i].detail[j]);
                }
            }
            var arrInfos = [];
            //console.log(itemsArr);
            //console.log(itemsArrCopy);
            for(var k=0;k<itemsArr.length;k++){
                for(var l=0;l<itemsArrCopy.length;l++){
                    if(itemsArr[k].activityGroup == itemsArrCopy[l].activityGroup && itemsArr[k].activity == itemsArrCopy[l].activity
                      &&  itemsArr[k].permissionMode != itemsArrCopy[l].permissionMode)  {
                        arrInfos.push(itemsArr[k]);
                    }
                }
            }
            if(arrInfos.length<1){
                $scope.hideBodyModel();
                // $scope.config.cancleBasicBtnDis = false;
                $scope.addAlert("","暂无数据需要保存");
                return;
            }
            //客户
            console.log(arrInfos);
            var infos = [];
            for(var i=0;i<arrInfos.length;i++){
                for(var j=0;j<arrInfos.length;j++){
                    if(arrInfos[i].activity == arrInfos[j].activity && arrInfos[i].activityGroup != arrInfos[j].activityGroup
                        && arrInfos[i].permissionMode != arrInfos[j].permissionMode){
                        infos.push(arrInfos[i].activityGroupDes +"下"+arrInfos[i].activityDes);
                    }
                }
            }
            console.log(infos);
            if(infos.length >0){
                $scope.addAlert("",infos.join(",")+"相同活动权限不同,无法保存");
                $scope.hideBodyModel();
                return;
            }
            userService
                .userGroupKeepPlatforms(arrInfos)
                .then(function (resultDatas){
                    $scope.config.cancleBasicBtnDis = false;
                    if(resultDatas.myHttpConfig.status == 200){
                        $scope.hideBodyModel();
                        $scope.bodyConfig.overallWatchChanged = false;
                        $scope.addAlert("success","保存成功");
                        $scope.config.keepPlatformsBtnDis = false;
                        $scope.config.canclePlatformsBtnDis = false;
                        $scope.config.editPlatformsBtnDis = true;
                        $scope.selectUserGroup();
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.hideBodyModel();
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });


        }
        //编辑
        $scope.editFlag = function(item){
            item.edit = !item.edit;
            if(item.edit == true){
                item.read = true;
            }
            $scope.editOrRead(item);
        }
        $scope.readFlag = function(item){
            item.read = !item.read;
            if(item.read == false){
                item.edit = false;
            }
            $scope.editOrRead(item);
        }
        $scope.editOrRead = function(item){
            if(item.edit == true && item.read == true){
                item.permissionMode = "RW";
                item.permissionSetting = true;
            }
            if(item.edit == false && item.read == true){
                item.permissionMode = "R";
                item.permissionSetting = true;
            }
            if(item.edit == false && item.read == false){
                item.permissionMode = "";
                item.permissionSetting = false;
            }
        }

    }]);