userModule.controller('basicInfoCtrl', [
    '$scope', '$http', '$q',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants', 'gridUtil', 'UtilsService','userService',
    function ($scope, $http, $q, HttpAppService, $timeout, $interval,
              uiGridConstants, gridUtil, UtilsService,userService){
        //用户组的相互切换
        $scope.selectedAvailableUserList = function(items){
            __selectedAvailableUserList(items);
        };

        $scope.selectedAssignUserList = function(items){
            __selectedAssignUserList(items);
        };

        $scope.reverseRight = function(){
            for(var i = $scope.config.userGroupAvailable.length - 1; i >= 0; i--){
                var item = $scope.config.userGroupAvailable[i];
                if(item.isSelected){
                    $scope.config.userGroupUnAvailable.push(angular.copy(item));
                    $scope.config.userGroupAvailable = UtilsService.removeArrayByIndex($scope.config.userGroupAvailable, i);

                }
            }
        };

        $scope.reverseLeft = function(){
            for(var i = $scope.config.userGroupUnAvailable.length - 1; i >= 0 ; i--){
                var item = $scope.config.userGroupUnAvailable[i];
                if(item.isSelected){
                    $scope.config.userGroupAvailable.push(angular.copy(item));
                    $scope.config.userGroupUnAvailable = UtilsService.removeArrayByIndex($scope.config.userGroupUnAvailable, i);
                }
            }
        };

        function __selectedAssignUserList(items){
            var usedList = $scope.config.userGroupAvailable;
            for(var i = 0;i<usedList.length;i++){
                if(usedList[i].isSelected){
                    usedList[i].isSelected = false;
                }
            };
            items.isSelected = !items.isSelected;
        };

        function __selectedAvailableUserList(items){
            var usedList = $scope.config.userGroupUnAvailable;
            for(var i = 0;i<usedList.length;i++){
                if(usedList[i].isSelected){
                    usedList[i].isSelected = false;
                }
            };
            items.isSelected = !items.isSelected;
        };
        //编辑
        $scope.editBasicBtn = function(){
            $scope.bodyConfig.overallWatchChanged = true;
            $scope.config.keepBasicBtnDis = true;
            $scope.config.cancleBasicBtnDis = true;
            $scope.config.editBasicBtnDis = false;
        }
        //$scope.keepBasicBtn = function(){
        //    if($scope.gridPlannedStop.data.length>0){
        //        var promise = UtilsService.confirm(msg, title, btnLables);   eg: UtilsService.confirm('是否保存', '提示', ['确定', '取消']);
        //        promise
        //            .then(function(){ // ok
        //                $scope.keepBasicBtn1();
        //            }, function(){   // cancel
        //                return;
        //            });
        //    }
        //}
        //保存
        $scope.keepBasicBtn = function(){
            //$scope.config.userGroupUnAvailable =$scope.config.userGroupUnAvailableCopy;
            //$scope.config.userGroupAvailable = $scope.config.userGroupAvailableCopy;
            //console.log($scope.config.description);
            //console.log($scope.config.userGroupItemCopy[0].description);
            $scope.showBodyModel("正在保存");
            if($scope.config.description == $scope.config.userGroupItemCopy[0].description){
                console.log("111");
            }else{
                //用户
                var items = [{
                    "site" : $scope.config.userGroupItem[0].site,
                    "userGroup" : $scope.config.userGroupItem[0].userGroup,
                    "description" : $scope.config.description,
                    "handle" : $scope.config.userGroupItem[0].handle,
                    "createdDateTime" : $scope.config.userGroupItem[0].createdDateTime,
                    "modifiedDateTime" : $scope.config.userGroupItem[0].modifiedDateTime,
                    "viewActionCode" : "U"
                }];
                userService
                    .userGroupKeepAvailable(items)
                    .then(function (resultDatas){
                        if(resultDatas.myHttpConfig.status == 200){
                            $scope.bodyConfig.overallWatchChanged = false;
                            $scope.hideBodyModel();
                            $scope.config.editBasicBtnDis = true;
                            $scope.addAlert("success","保存成功");

                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.hideBodyModel();
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    });
            }


            var itemsArr = [];
            for(var i=0;i<$scope.config.userGroupAvailable.length;i++){
                if($scope.config.userGroupAvailable[i].isFlag == "D"){
                    itemsArr.push({
                        "site" : $scope.config.userGroupAvailable[i].site,
                        "userGroupBo" : $scope.config.userGroupItem[0].handle,
                        "userBo" : $scope.config.userGroupAvailable[i].handle,
                        "viewActionCode" : "D",
                        "enabled" : $scope.config.userGroupAvailable[i].enabled,
                        "enabledModifiedDateTime" : $scope.config.userGroupAvailable[i].enabledModifiedDateTime,
                        "handle" : $scope.config.userGroupAvailable[i].handle,
                        "modifiedDateTime" : $scope.config.userGroupAvailable[i].modifiedDateTime,
                        "modifiedUser" : $scope.config.userGroupAvailable[i].modifiedUser,
                        "userId" : $scope.config.userGroupAvailable[i].userId,
                    });
                }
            }
            for(var i=0;i<$scope.config.userGroupUnAvailable.length;i++){
                if($scope.config.userGroupUnAvailable[i].isFlag == "C") {
                    itemsArr.push({
                        "site" : $scope.config.userGroupUnAvailable[i].site,
                        "userGroupBo" : $scope.config.userGroupItem[0].handle,
                        "userBo" : $scope.config.userGroupUnAvailable[i].handle,
                        "viewActionCode" : "C",
                        "enabled" : $scope.config.userGroupUnAvailable[i].enabled,
                        "enabledModifiedDateTime" : $scope.config.userGroupUnAvailable[i].enabledModifiedDateTime,
                        "handle" : $scope.config.userGroupUnAvailable[i].handle,
                        "modifiedDateTime" : $scope.config.userGroupUnAvailable[i].modifiedDateTime,
                        "modifiedUser" : $scope.config.userGroupUnAvailable[i].modifiedUser,
                        "userId" : $scope.config.userGroupUnAvailable[i].userId,
                    });
                }
            }
            if(itemsArr.length < 1){

            }else{
                //客户
                userService
                    .userGroupKeepUnAvailable(itemsArr)
                    .then(function (resultDatas){
                        if(resultDatas.myHttpConfig.status == 200){
                            $scope.bodyConfig.overallWatchChanged = false;
                            $scope.hideBodyModel();
                            $scope.config.editBasicBtnDis = true;
                            $scope.addAlert("success","保存成功");                            
                            $scope.selectUserGroup();
                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.hideBodyModel();
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    });

            }
            if($scope.config.description == $scope.config.userGroupItemCopy[0].description && itemsArr.length < 1){
                $scope.hideBodyModel();
                $scope.addAlert("","暂无数据保存");
                return;
            }
            $scope.config.keepBasicBtnDis = false;
            $scope.config.cancleBasicBtnDis = false;

        }
        $scope.cancleBasicBtn = function(){
            $scope.bodyConfig.overallWatchChanged = false;
            $scope.config.userGroupUnAvailable =$scope.config.userGroupUnAvailableCopy;
            $scope.config.userGroupAvailable = $scope.config.userGroupAvailableCopy;
            $scope.config.keepBasicBtnDis = false;
            $scope.config.cancleBasicBtnDis = false;
            $scope.config.editBasicBtnDis = true;
        }
    }]);
userModule.filter("reverse",['UtilsService',function(UtilsService){
    return function(input,param){
        //console.log(input);
        //console.log(param);
        var itemArr = [];
        for(var i=0;i<param.length;i++){
            for(var j=0;j<input.length;j++){
                if(param[i].handle == input[j].handle && input[j].isFlag == "C"){
                    itemArr.push(input[j]);
                    input = UtilsService.removeArrayByIndex(input, j);
                }
            }
        }
        //console.log(itemArr);
        for(var i=0;i<itemArr.length;i++){
            for(var j=0;j<input.length;j++){
                if(itemArr[i].handle == input[j].handle && input[j].isFlag == "C"){
                    input = UtilsService.removeArrayByIndex(input, j);
                }
            }
        }
        return input;
    }
}]);