userModule.controller('privilegeCtrl', [
    '$scope', '$http', '$q',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants', 'gridUtil', 'UtilsService','userService',
    function ($scope, $http, $q, HttpAppService, $timeout, $interval,
              uiGridConstants, gridUtil, UtilsService,userService){

//已分配到未分配的切换
        $scope.selectedAvailableUserList = function(items){
            __selectedAvailableUserList(items);
        };

        $scope.selectedAssignUserList = function(items){
            __selectedAssignUserList(items);
        };

        $scope.reverseRight = function(){
            for(var i = $scope.config.selectedAvailableUserList.length - 1; i >= 0; i--){
                var item = $scope.config.selectedAvailableUserList[i];
                if(item.isSelected){
                    $scope.config.selectedAssignUserList.push(angular.copy(item));
                    $scope.config.selectedAvailableUserList = UtilsService.removeArrayByIndex($scope.config.selectedAvailableUserList, i);

                }
            }
        };

        $scope.reverseLeft = function(){
            for(var i = $scope.config.selectedAssignUserList.length - 1; i >= 0 ; i--){
                var item = $scope.config.selectedAssignUserList[i];
                if(item.isSelected){
                    $scope.config.selectedAvailableUserList.push(angular.copy(item));
                    $scope.config.selectedAssignUserList = UtilsService.removeArrayByIndex($scope.config.selectedAssignUserList, i);
                }
            }
        };

        function __selectedAssignUserList(items){
            var usedList = $scope.config.selectedAvailableUserList;
            for(var i = 0;i<usedList.length;i++){
                if(usedList[i].isSelected){
                    usedList[i].isSelected = false;
                }
            };
            items.isSelected = !items.isSelected;
        };

        function __selectedAvailableUserList(items){
            var usedList = $scope.config.selectedAssignUserList;
            for(var i = 0;i<usedList.length;i++){
                if(usedList[i].isSelected){
                    usedList[i].isSelected = false;
                }
            };
            items.isSelected = !items.isSelected;
        };
        //$scope.keepBasicBtn = function(){
        //    if($scope.gridPlannedStop.data.length>0){
        //        var promise = UtilsService.confirm('是否保存', '提示', ['确定', '取消']);
        //        promise
        //            .then(function(){ // ok
        //                $scope.keepBasicBtn1();
        //            }, function(){   // cancel
        //                return;
        //            });
        //    }
        //}
        //保存
        $scope.keepBasicBtn = function(){
            $scope.showBodyModel("正在保存");
            //console.log($scope.config.selectedAvailableUserList);
            //console.log($scope.config.selectedAssignUserList);

            var itemsArr = [];
            for(var i=0;i<$scope.config.selectedAvailableUserList.length;i++){
                if($scope.config.selectedAvailableUserList[i].isFlag == 'D'){
                    itemsArr.push({
                        "handle" : $scope.config.selectedAvailableUserList[i].handle,
                        "activityClient" : $scope.config.selectedAvailableUserList[i].activityClient,
                        "description" : $scope.config.selectedAvailableUserList[i].description,
                        "activityClientBo" : $scope.config.selectedAvailableUserList[i].activityClientBo,
                        "modifiedDateTime" : $scope.config.selectedAvailableUserList[i].modifiedDateTime,
                        "viewActionCode" : "U",
                        "permissionSetting":"false",
                        "userGroupBo" : $scope.config.userGroupItem[0].handle,
                    });
                }
            }
            for(var i=0;i<$scope.config.selectedAssignUserList.length;i++){
                if($scope.config.selectedAssignUserList[i].isFlag == 'C') {
                    itemsArr.push({
                        //"handle" : $scope.config.selectedAssignUserList[i].handle,
                        "activityClient" : $scope.config.selectedAssignUserList[i].activity,
                        "description" : $scope.config.selectedAssignUserList[i].description,
                        "userGroupBo" : $scope.config.userGroupItem[0].handle,
                        "activityClientBo" : $scope.config.selectedAssignUserList[i].activityClientBo,
                        //"modifiedDateTime" : $scope.config.selectedAssignUserList[i].modifiedDateTime,
                        "viewActionCode" : "C",
                        "permissionSetting":"true",
                    });
                }
            }
            if(itemsArr.length<1){
                $scope.hideBodyModel();
                // $scope.config.cancleBasicBtnDis = false;
                $scope.addAlert("","暂无数据需要保存");
                return;
            }
            //客户
            userService
                .userGroupKeepCustomerPlatforms(itemsArr)
                .then(function (resultDatas){
                    $scope.config.cancleBasicBtnDis = false;
                    $scope.hideBodyModel();
                    if(resultDatas.myHttpConfig.status == 200){
                        $scope.addAlert("success","保存成功");
                        $scope.bodyConfig.overallWatchChanged = false;
                        $scope.config.keepCustomersBtnDis = false;
                        $scope.config.cancleCustomersBtnDis = false;
                        $scope.config.editCustomersBtnDis = true;
                        $scope.selectUserGroup();
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.hideBodyModel();
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });


        }
        //取消
        $scope.cancleBasicBtn = function(){
            $scope.bodyConfig.overallWatchChanged = false;
            $scope.config.selectedAssignUserList = $scope.config.selectedAssignUserListCopy;
            $scope.config.selectedAvailableUserList =$scope.config.selectedAvailableUserListCopy;
            $scope.config.userGroupItem =$scope.config.userGroupItemCopy;
            $scope.config.keepCustomersBtnDis = false;
            $scope.config.cancleCustomersBtnDis = false;
            $scope.config.cancleBasicBtnDis = false;
            $scope.config.editCustomersBtnDis = true;
        }
        //编辑
        $scope.editBasicBtn = function(){
            $scope.bodyConfig.overallWatchChanged = true;
            $scope.config.keepCustomersBtnDis = true;
            $scope.config.cancleCustomersBtnDis = true;
            $scope.config.cancleBasicBtnDis = true;
            $scope.config.editCustomersBtnDis = false;
        }
    }]);