userModule.controller('userManagerCtrl', [
    '$scope', '$http', '$q',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants', 'gridUtil','UtilsService','userService','$state',
    function ($scope, $http, $q, HttpAppService, $timeout, $interval,
              uiGridConstants, gridUtil,UtilsService,userService,$state){

        $scope.config = {
            userSearch : null,
            userSearchBack: null, // 查询成功后赋值，若与userSearch不一致，则禁用编辑按钮
            queryBtn : true,
            editBtn : true,
            cancleBtn : false,
            cancleHide : false,
            keepBtn : false,
            keepHide : false,
            enabled : false,
            selectedAvailableUserList: [],
            selectedAssignUserList: [],
            userItem : [],
            selectedAvailableUserListCopy : [],
            selectedAssignUserListCopy : [],
            userItemCopy : [],
            isNoValue : false,
            editRw : false,
            originEnabledModifiedDatetime:''
        };
        // $scope.config.editRW = $scope.modulesRWFlag("#"+$state.$current.url.source);
        $scope.config.editRw = $scope.modulesRWFlag("#"+$state.$current.url.source);

        $scope.selectedAvailableUserList = function(items){
            __selectedAvailableUserList(items);
        };

        $scope.selectedAssignUserList = function(items){
            __selectedAssignUserList(items);
        };
//左侧框变化
        $scope.reverseRight = function(){
            for(var i = $scope.config.selectedAvailableUserList.length - 1; i >= 0; i--){
                var item = $scope.config.selectedAvailableUserList[i];
                if(item.isSelected){
                    $scope.config.selectedAssignUserList.push(angular.copy(item));
                    $scope.config.selectedAvailableUserList = UtilsService.removeArrayByIndex($scope.config.selectedAvailableUserList, i);

                }
            }
        };
//右侧框变化
        $scope.reverseLeft = function(){
            for(var i = $scope.config.selectedAssignUserList.length - 1; i >= 0 ; i--){
                var item = $scope.config.selectedAssignUserList[i];
                if(item.isSelected){
                    $scope.config.selectedAvailableUserList.push(angular.copy(item));
                    $scope.config.selectedAssignUserList = UtilsService.removeArrayByIndex($scope.config.selectedAssignUserList, i);
                }
            }
        };
        $scope.keyUpFun = function(e){
            var keycode = window.event ? e.keyCode : e.which;
            if(keycode==13){
                console.log("回车");
            }
        };
        
        function __selectedAssignUserList(items){
            var usedList = $scope.config.selectedAvailableUserList;
            for(var i = 0;i<usedList.length;i++){
                if(usedList[i].isSelected){
                    usedList[i].isSelected = false;
                }
            };
            items.isSelected = !items.isSelected;
        };
        function __selectedAvailableUserList(items){
            var usedList = $scope.config.selectedAssignUserList;
            for(var i = 0;i<usedList.length;i++){
                if(usedList[i].isSelected){
                    usedList[i].isSelected = false;
                }
            };
            items.isSelected = !items.isSelected;
        };
        //查询更改
        $scope.queryInfosChange = function(){
            if($scope.config.userSearch != null || $scope.config.userSearch != ""){
                $scope.config.queryBtn = false;
            }
            if(!UtilsService.isEmptyString($scope.config.userSearchBack)){
                if($scope.config.userSearch.toUpperCase() != $scope.config.userSearchBack.toUpperCase()){
                    $scope.config.editBtn = true;
                }else{
                    $scope.config.editBtn = false;
                }
            }
        }
        function __initConfigs() {
            $scope.config.editBtn = false;
            $scope.config.keepHide = false;
            $scope.config.cancleHide = false;
            $scope.config.selectedAssignUserList = [];
            $scope.config.selectedAssignUserList = [];
            $scope.config.userItem = [];
            $scope.config.userItemCopy = [];
        }
        //查询
        $scope.queryData = function(){
            if($scope.config.editRw && $scope.config.cancleHide){
                return;
            }
            $scope.config.keepHide = false;
            $scope.config.cancleHide = false;
            $scope.config.selectedAvailableUserList = [];
            $scope.config.selectedAssignUserList = [];
            $scope.config.userItem = null;
            if($scope.config.userSearch == null || $scope.config.userSearch == ""){
                $scope.config.isNoValue = true;
                return;
            }
            $scope.config.selectedAssignUserList = [];
            if($scope.config.keepHide == true){
                var promise = UtilsService.confirm('重新查询将会将刷新现有数据', '提示', ['确定', '取消']);
                promise
                    .then(function(){ // ok
                        __initConfigs();
                        __userQueryUseDatas($scope.config.userSearch);
                    }, function(){   // cancel

                    });
            }else{
                $scope.config.editBtn = false;
                __userQueryUseDatas($scope.config.userSearch);
            }
        }
        //编辑
        $scope.editData = function(){
            $scope.bodyConfig.overallWatchChanged = true;
            $scope.config.keepHide = true;
            $scope.config.cancleHide = true;
        }
        //取消
        $scope.cancleData = function(){
            $scope.bodyConfig.overallWatchChanged = false;
            $scope.config.keepHide = false;
            $scope.config.cancleHide = false;
            $scope.config.userItem =  $scope.config.userItemCopy;
            $scope.config.selectedAssignUserList = $scope.config.selectedAssignUserListCopy;
            $scope.config.selectedAvailableUserList = $scope.config.selectedAvailableUserListCopy;
        }


        function __searchResult(){

        };
        function __userQueryUseDatas(userSearch){
            userService
                .userQueryUseDatas(userSearch)
                .then(function (resultDatas){
                    //console.log("------------");
                    $scope.config.userSearchBack = $scope.config.userSearch;
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.selectedAssignUserList = resultDatas.response;
                        $scope.config.selectedAssignUserListCopy = angular.copy(resultDatas.response);
                        for(var i=0;i<$scope.config.selectedAssignUserList.length;i++){
                            $scope.config.selectedAssignUserList[i].isHover = false;
                            $scope.config.selectedAssignUserList[i].isSelected = false;
                            $scope.config.selectedAssignUserList[i].isFlag = "C";
                        }
                        __userSearch($scope.config.userSearch);
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }

        //查询
        function __userSearch(userSearch){

            var arr=[];
            userService
                .userQueryDatas(userSearch)
                .then(function (resultDatas){
                    if(resultDatas.response.message == "No such user"){
                        $scope.config.editBtn = true;
                        $scope.config.selectedAssignUserList = [];
                        $scope.addAlert("","无此用户");
                        return;
                    }
                    if((resultDatas.response.userPO == undefined || resultDatas.response.userPO == "" || resultDatas.response.userPO == null) && resultDatas.response.serverUserDetailsVO.defaultSite == undefined){
                        $scope.config.editBtn = true;
                        $scope.config.selectedAssignUserList = [];
                        $scope.addAlert("","无此用户");
                        return;
                    }
                    if(resultDatas.response){
                        for(var i=0;i<resultDatas.response.originUserGroupMember.length;i++){
                            for(var j=0;j<$scope.config.selectedAssignUserList.length;j++){
                                if(resultDatas.response.originUserGroupMember[i].userGroupBo == $scope.config.selectedAssignUserList[j].handle){
                                    arr.push($scope.config.selectedAssignUserList[j]);
                                }
                            }
                        }
                        console.log(arr);
                        $scope.config.selectedAvailableUserList = angular.copy(arr);
                        $scope.config.userItem = resultDatas.response;
                        $scope.config.userItemCopy = angular.copy(resultDatas.response);
                        if(resultDatas.response.userPO != undefined ){
                            if(resultDatas.response.userPO.enabled == "true"){
                                $scope.config.enabled = true;

                            }else{
                                $scope.config.enabled = false;
                            }
                            $scope.config.originEnabledModifiedDatetime=resultDatas.response.userPO.enabledModifiedDateTime;
                            $scope.config.userItem.userPO.enabledModifiedDateTime = UtilsService.serverFommateDateTimeShow(new Date(resultDatas.response.userPO.enabledModifiedDateTime));

                        }
                        for(var i=0;i<$scope.config.selectedAvailableUserList.length;i++){
                            $scope.config.selectedAvailableUserList[i].isHover = false;
                            $scope.config.selectedAvailableUserList[i].isSelected = false;
                            $scope.config.selectedAvailableUserList[i].isFlag = "D";
                        }
                        $scope.config.selectedAvailableUserListCopy = angular.copy($scope.config.selectedAvailableUserList);
                        console.log($scope.config.selectedAvailableUserList);
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }
        //保存
        $scope.keepData = function(){
            if($scope.config.enabled == true){
                var enabled = 'true';
            }else{
                var enabled = 'false';
            }
            if($scope.config.selectedAvailableUserListCopy.length == $scope.config.selectedAvailableUserList.length && $scope.config.userItemCopy.userPO.enabled == enabled){
                for(var i=0;i<$scope.config.selectedAvailableUserList.length;i++){
                    if($scope.config.selectedAvailableUserList[i].isFlag == "C"){
                        var num = 0;
                        break;
                    }
                }
                if(num == 0){
                }else{
                    $scope.addAlert("","暂无数据需要保存");
                    return;
                }
            }


            if($scope.config.selectedAvailableUserList.length ==0){
                $scope.addAlert("","未分配用户组无法保存，请为用户分配用户组");
                return;
            }
            $scope.showBodyModel("正在保存");
            $scope.config.keepHide = false;
            $scope.config.cancleHide = false;
            if($scope.config.userItem.userPO!=undefined&&$scope.config.userItem.userPO!=""&&$scope.config.userItem.userPO!=null){
            	$scope.config.userItem.userPO.enabledModifiedDateTime=$scope.config.originEnabledModifiedDatetime;
            }
            var itemsArr = $scope.config.userItem;
            if($scope.config.selectedAvailableUserList.userPO != undefined ){
                if( $scope.config.enabled = true){
                    $scope.config.selectedAvailableUserList.userPO.enabled == "true";
                }else{
                    $scope.config.selectedAvailableUserList.userPO.enabled == "false";
                }
            }
            //$scope.numFlag = 0;
            //for(var i=0;i<$scope.config.selectedAssignUserList;i++){
            //    if($scope.config.selectedAssignUserList[i].isFlag = "D"){
            //        $scope.numFlag++;
            //    }
            //}
            //for(var i=0;i<$scope.config.selectedAvailableUserList;i++){
            //    if($scope.config.selectedAvailableUserList[i].isFlag = "C"){
            //        $scope.numFlag++;
            //    }
            //}
            //if($scope.numFlag > 0){
            //
            //}else{
            //    itemsArr.originUserGroupMember = [];
            //}
            itemsArr.updateUserGroup= $scope.config.selectedAvailableUserList;
            console.log(itemsArr);
            userService
                .userKeepDatas(itemsArr,$scope.config.userSearch)
                .then(function (resultDatas){
                    $scope.hideBodyModel();
                    //console.log(resultDatas);
                    if(resultDatas.myHttpConfig.status == 200){
                        $scope.bodyConfig.overallWatchChanged = false;
                        $scope.addAlert("success","保存成功");
                        $scope.queryData();
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.hideBodyModel();
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }
        $scope.upModel = function(e){
            $scope.config.userSearch = angular.uppercase($scope.config.userSearch);
            //回车查询
            var keycode = window.event ? e.keyCode : e.which;
            if(keycode==13){
                $scope.queryData();
            }
        }
    }]);