userModule.service('userService', [
	"HttpAppService",
	function (HttpAppService) {

		return {
			siteList: requestSiteList,
			siteSave: requestSiteSave,

			userGroupData : userGroupData,
			userGroupSearch : userGroupSearch,
			userGroupAvailable : userGroupAvailable,
			userGroupUnAvailable :userGroupUnAvailable,
			userGroupKeepAvailable : userGroupKeepAvailable,
			userGroupKeepUnAvailable : userGroupKeepUnAvailable,
			userGroupPlatforms : userGroupPlatforms,
			userGroupKeepPlatforms : userGroupKeepPlatforms,
			userGroupCustomerWork : userGroupCustomerWork,
			userGroupCustomerUnWork : userGroupCustomerUnWork,
			userGroupKeepCustomerPlatforms : userGroupKeepCustomerPlatforms,

			userQueryDatas : userQueryDatas,
			userQueryUseDatas : userQueryUseDatas,
			userKeepDatas : userKeepDatas,

		};

		function requestSiteList(){
			var url = HttpAppService.URLS.SITE_LIST
				.replace("#{site}#", HttpAppService.getSite)
			return HttpAppService.get({
				url: url
			});
		}

		function requestSiteSave(paramActions){
			var url = HttpAppService.URLS.SITE_SAVE;
			return HttpAppService.post({
				url: url,
				paras : paramActions
			});
		}

		function userGroupData(user_group){
			var url = HttpAppService.URLS.USER_GROUP_DATA
					.replace("#{site}#", HttpAppService.getSite)
					.replace("#{user_group}#", user_group)
				;
			return HttpAppService.get({
				url: url
			});
		};
		function userGroupSearch(user_group){
			var url = HttpAppService.URLS.USER_GROUP_SEARCH
					.replace("#{site}#", HttpAppService.getSite)
					.replace("#{user_group}#", user_group)
				;
			return HttpAppService.get({
				url: url
			}); 
		}; 
		function userGroupAvailable(){
			var url = HttpAppService.URLS.USER_GROUP_AVAILABLE
				.replace("#{site}#", HttpAppService.getSite)
			return HttpAppService.get({
				url: url
			}); 
		}; 

		function userGroupUnAvailable(user_group){
			var url = HttpAppService.URLS.USER_GROUP_UNAVAILABLE
					.replace("#{site}#", HttpAppService.getSite)
					.replace("#{user_group}#", user_group)
				;
			return HttpAppService.get({
				url: url
			});
		};
		function userGroupKeepAvailable(paramActions){
			var url = HttpAppService.URLS.USER_GROUP_KEEPAVAILABLE;
			return HttpAppService.post({
				url: url,
				paras : paramActions
			});
		}

		function userGroupKeepUnAvailable(paramActions){
			var url = HttpAppService.URLS.USER_GROUP_KEEPUNAVAILABLE;
			return HttpAppService.post({
				url: url,
				paras : paramActions
			});
		}
		function userGroupPlatforms(user_group){
			var url = HttpAppService.URLS.USER_GROUP_PLATFORMS
					.replace("#{site}#", HttpAppService.getSite)
					.replace("#{user_group}#", user_group)
				;
			return HttpAppService.get({
				url: url
			});
		};
		function userGroupKeepPlatforms(paramActions){
			var url = HttpAppService.URLS.USER_GROUP_KEEP_PLATFORMS;
			return HttpAppService.post({
				url: url,
				paras : paramActions
			});
		}
		
		function userGroupCustomerWork(){
			var url = HttpAppService.URLS.USER_GROUP_CUSTOMER_WORK
				.replace("#{site}#", HttpAppService.getSite)
			return HttpAppService.get({
				url: url
			});
		};
		
		function userGroupCustomerUnWork(user_group){
			var url = HttpAppService.URLS.USER_GROUP_CUSTOMER_UNWORK
					.replace("#{site}#", HttpAppService.getSite)
					.replace("#{user_group}#", user_group)
				;
			return HttpAppService.get({
				url: url
			});
		};
		function userGroupKeepCustomerPlatforms(paramActions){
			var url = HttpAppService.URLS.USER_GROUP_KEEP_CUSTOMER_PLATFORMS;
			return HttpAppService.post({
				url: url,
				paras : paramActions
			});
		};
		function userKeepDatas(paramActions,item){
			var url = HttpAppService.URLS.USER_KEEP_DATAS
				.replace("#{site}#", HttpAppService.getSite)
				.replace("#{userId}#",item)
			return HttpAppService.post({
				url: url,
				paras : paramActions
			});
		};
		function userQueryDatas(item){
			var url = HttpAppService.URLS.OFFICE_USER_QUERY_DATAS
					.replace("#{site}#", HttpAppService.getSite)
					.replace("#{userId}#", item)
				;
			return HttpAppService.get({
				url: url
			});
		};
		function userQueryUseDatas(item){
			var url = HttpAppService.URLS.USER_QUERY_USE_DATAS
					.replace("#{site}#", HttpAppService.getSite)
					.replace("#{userId}#", item)

				;
			return HttpAppService.get({
				url: url
			});
		};
		function userQueryUnUseDatas(item){
			var url = HttpAppService.URLS.USER_QUERY_UN_USE_DATAS
					.replace("#{site}#", HttpAppService.getSite)
					.replace("#{user_group}#", item)
				;
			return HttpAppService.get({
				url: url
			});
		};
}]);

userModule.run([
	'$templateCache',
	function($templateCache){
		$templateCache.put('andon-ui-grid-tpls/factory-site', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.site\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);
		$templateCache.put('andon-ui-grid-tpls/userUpload-user', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.user\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);
		$templateCache.put('andon-ui-grid-tpls/userUpload-userGroup', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.userGroup\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);
	}]);