userModule.controller('factoryManagerCtrl', [ 
    '$scope', '$http', '$q', '$state',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants', 'gridUtil', 'userService',
    'uiGridValidateService', 'UtilsService',
    function ($scope, $http, $q, $state, HttpAppService, $timeout, $interval,
              uiGridConstants, gridUtil, userService,
              uiGridValidateService, UtilsService){
        
        $scope.config = {
            gridApi: null,
            btnDisabledDelete: true,
            deletedRows:[],
            addRowsNum: 1,
            canEdit: false
        };

        $scope.config.canEdit = $scope.modulesRWFlag("#"+$state.$current.url.source);


        $scope.gridFactory = {
            /*paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes, //[5, 10, 15, 20, 50, 100],
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize, //10, 
            useExternalPagination: true, */

            showGridFooter: false, 
            modifierKeysToMultiSelectCells: false, 
            enableRowSelection: false,
            enableSelectAll: false,

            data: [],
            onRegisterApi: __onRegisterApi,

            columnDefs: [
                { 
                    field: 'handle', name: 'handle', visible: false
                },
                {   
                    field: 'site', name: 'site', displayName: '工厂代码', minWidth: 150, 
                    validators: { required: true }, 
                    enableCellEdit: $scope.config.canEdit, enableCellEditOnFocus: false, 
                    maxLength: 6,
                    cellTooltip: function(row, col){  return row.entity[col.colDef.name];  }, 
                    // cellTemplate: 'andon-ui-grid-tpls/dataCategory-categoryCode',
                    cellTemplate: 'andon-ui-grid-tpls/factory-site', cellClass:'grid-no-editable ad-uppercase',
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row;
                        var col = arguments[0].col;
                        // 非新增不可编辑 、新增行可编辑 
                        if(!angular.isUndefined(row.entity) && row.entity!=null && !angular.isUndefined(row.entity.handle) && row.entity.handle!=null && row.entity.handle!=""){
                            return false;
                        }else{
                            return true;
                        }
                    }
                }, 
                {
                    field: 'description', name: 'description', displayName: '工厂描述', minWidth: 200,
                    enableCellEdit: $scope.config.canEdit, enableCellEditOnFocus: false, validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                }, 
                { 
                    field: 'isDeleted', name: 'isDeleted', displayName: '删除', width: 60, 
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    //visible: $scope.config.canEdit,
                    visible:false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return "删除该行"; }, 
                    cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col"><span class="ion-trash-a"></span></div>',
                    headerTemplate: '<div class="xbr-delete-col-header">删除</div>'
                }
            ]
        };

        function __onRegisterApi(gridApi){ 
            $scope.config.gridApi = gridApi;
            $scope.config.gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
                console.log(" ----- navigate ----- "); 
                if(newRowCol.col.field == "isDeleted" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                    __checkCanDeleteData(newRowCol);
                }
            }); 
            $scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                var promise = $q.defer();
                if(gridApi.rowEdit){
                    gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
                }
                
                if(oldValue == newValue){
                    promise.resolve();
                    return;
                }
                rowEntity.hasChanged = true;
                $scope.bodyConfig.overallWatchChanged = true;
                if(colDef.field === "site" || colDef.name === "site"){
                    if(UtilsService.isEmptyString(rowEntity.site)){
                        return;
                    }   
                    rowEntity.site = rowEntity.site.toUpperCase();
                    if(UtilsService.isContainZhcn(rowEntity.site)){
                        promise.reject();
                        $scope.addAlert('', '工厂代码不能为汉字!');
                        uiGridValidateService.setInvalid(rowEntity, colDef);
                        rowEntity.siteZhcnIsValid = false;
                        return;
                    }
                    if(rowEntity.site.length > 6){
                        rowEntity.siteLengthTooLargeIsValid = false;
                        uiGridValidateService.setInvalid(rowEntity, colDef);
                        $scope.addAlert('',"工厂代码最大长度为6位!");
                        promise.reject();
                    }else{ 
                        uiGridValidateService.setValid(rowEntity, colDef);
                        rowEntity.siteLengthTooLargeIsValid = true;
                        promise.resolve();
                    }   
                    rowEntity.siteZhcnIsValid = true;

                }else{
                    promise.resolve();
                }
            }); 
            gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
                gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
            });
            // //选择相关
            // gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
            //     var count = $scope.config.gridApi.selection.getSelectedCount();
            //     $scope.config.btnDisabledDelete = count > 0 ? false : true;
            //     gridRow.entity.isSelected = gridRow.isSelected;
            // });
            // gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            //     var count = $scope.config.gridApi.selection.getSelectedCount();
            //     $scope.config.btnDisabledDelete = count > 0 ? false : true;
            //     for(var i = 0; i < gridRows.length; i++){
            //         gridRows[i].entity.isSelected = gridRows[i].isSelected;
            //     }
            // });
        };

        $scope.addRows = function(){
            $scope.bodyConfig.overallWatchChanged = true;
            for(var i = 0; i < $scope.config.addRowsNum; i++){
                $scope.gridFactory.data.unshift({
                    hasChanged: true
                });
            }
        };

        ////// 逻辑检验 ///////////
        function __checkCanDeleteData(newRowCol){
            var entity = newRowCol.row.entity;
            var handle = entity.handle;
            var hashKey = entity.$$hashKey;
            if(angular.isUndefined(handle) || handle==null || handle==""){ // 处理新增行 
                __deleteRowDataByHashKey(hashKey);
                return;
            } 
            // 处理历史行，需要检验是否已经被引用
            // plcService
            //     .requestPlcCategoryIsUsed(entity.category)
            //     .then(function (resultDatas){
                    __deleteRowDataByHashKey(hashKey);
            //     }, function (errorResultDatas){
            //         $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
            //     });
        };

        function __deleteRowDataByHashKey(hashKey){
            for(var i = 0; i < $scope.gridFactory.data.length; i++){
                if(hashKey == $scope.gridFactory.data[i].$$hashKey){
                    $scope.bodyConfig.overallWatchChanged = true;
                    $scope.config.deletedRows.push($scope.gridFactory.data[i]);
                    $scope.gridFactory.data = UtilsService.removeArrayByIndex($scope.gridFactory.data, i);
                    break; 
                }
            }
        };

        $scope.checkAndDeleteSelectedRows = function(){
            //$scope.config.deletedRows = [];
            //var deletedRowIndexs = [];
            //var rows = $scope.config.gridApi.selection.getSelectedRows();
            //for(var i = 0; i < rows.length; i++){
            //    var row = rows[i];
            //    if( !angular.isUndefined(row.handle) && row.handle!=null && row.handle!=""){
            //        row.canDeleted = true;
            //        row.isSelected = true;
            //    }else{
            //        row.isSelected = true;
            //        row.canDeleted = true;
            //    }
            //}
            //__chekForDeleteRows();
            $scope.bodyConfig.overallWatchChanged = true;
            __chekForDeleteRows();
        };

        function __chekForDeleteRows(){
            $scope.config.deletedRows = [];
            var rows = $scope.config.gridApi.selection.getSelectedRows();
            if(rows.length > 0){
                for(var i =0;i<rows.length;i++){
                    __deleteRowDataByHashKey(rows[i].$$hashKey);
                };
                console.log($scope.config.deletedRows);
            }else{
                $scope.addAlert("info","先选择要删除的行");
            }
            //var canDeletes = true;
            //var deletedRowIndexs = [];
            //var rows = $scope.gridFactory.data;
            //for(var i = 0; i < rows.length; i++){
            //    var row = rows[i];
            //    if(row.isSelected){
            //        if(row.canDeleted !== true){
            //            canDeletes = false;
            //        }
            //    }
            //}
            //if(canDeletes){
            //    for (var i = rows.length - 1; i >= 0; i--) {
            //        var row = rows[i];
            //        if(row.isSelected){
            //            row.isDeleted = true;
            //            deletedRowIndexs.push(i);
            //            $scope.config.deletedRows.push(angular.copy(row));
            //        }
            //    };
            //}else{
            //    return;
            //}
            //var length = deletedRowIndexs.length;
            //for(var i = 0; i < length; i++){
            //    $scope.gridFactory.data.splice(deletedRowIndexs[i], 1);
            //    // $scope.gridFactory.data = UtilsService.removeArrayByIndex($scope.gridFactory.data, deletedRowIndexs.pop());
            //}
        };


        $scope.saveAllAcitons = function(){
            if(!__checkCanSaveDatas()){
                return;
            };
            var paramActions = []; 
            for(var i = 0; i < $scope.gridFactory.data.length; i++){
                var row = $scope.gridFactory.data[i]; 
                if(row.hasChanged){
                    // var viewActionCode = angular.isUndefined(row.handle) ? 'C' : 'U';
                    if(angular.isUndefined(row.handle)){ //创建新数据 ，若是删除中存在该条数据，则直接忽略该行,并在后面将删除操作也忽略
                        if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
                            paramActions.push({
                                "handle": row.handle, 
                                //"site": HttpAppService.getSite(),
                                "site": row.site, 
                                "category": row.category,  
                                "description": row.description,  
                                "viewActionCode": 'C'  
                            });  
                        }else{}
                    }else{ //修改已存于服务器的行数据 ，若删除列表中有该行，则直接忽略该修改提交，并在后面将删除操作添加进来
                        if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
                            paramActions.push({
                                "handle": row.handle, 
                                //"site": HttpAppService.getSite(),
                                "site": row.site, 
                                "category": row.category,  
                                "description": row.description,  
                                "viewActionCode": 'U',
                                "modifiedDateTime": row.modifiedDateTime
                            });
                        }else{}
                    }
                }
            }
            for(var i = 0; i < $scope.config.deletedRows.length; i++){
                var deleteTempRow = $scope.config.deletedRows[i];
                if(!angular.isUndefined(deleteTempRow.handle)){// 服务器端数据的删除操作,直接添加进去即可
                    paramActions.push({
                        "handle": deleteTempRow.handle,
                        "site": deleteTempRow.site,
                        "description": deleteTempRow.description, 
                        "viewActionCode": 'D',
                        "modifiedDateTime": deleteTempRow.modifiedDateTime
                    });
                }else{ }
            } 
            if(paramActions.length > 0){
                userService
                    .siteSave(paramActions)
                        .then(function (resultDatas){
                        $scope.bodyConfig.overallWatchChanged = false;
                        $scope.addAlert('success', '保存成功!');
                        __requestTableDatas();
                    },function (resultDatas){ 
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
                // plcService 
                //     .requestPlcCategoriesChange(paramActions)
                //     .then(function (resultDatas){ 
                //         $scope.addAlert('success', '保存成功!');
                //         __requestTableDatas();
                //     },function (resultDatas){ 
                //         $scope.addAlert('danger', '保存失败!');
                //     });
            }else{ 
                $scope.addAlert('没有修改,无需保存!');
            }
        };

        function __checkCanSaveDatas(){
            for(var i = 0; i < $scope.gridFactory.data.length; i++){
                var item = $scope.gridFactory.data[i];
                if(item.hasChanged){
                    var rules = [
                        { field: 'site',  emptyDesc: '第'+(i+1)+'行: 工厂不能为空!' },
                        { field: 'description', emptyDesc: '第'+(i+1)+'行: 数据类型描述不能为空!' }
                    ];
                    for(var ruleIndex in rules){
                        var rule = rules[ruleIndex];
                        if(angular.isUndefined(item[rule.field]) || item[rule.field] == null || item[rule.field] == ""){
                            $scope.addAlert('', rule.emptyDesc);
                            return false;
                        }
                    }   
                    // if(!angular.isUndefined(item.dataTypeIsValid) && item.dataTypeIsValid==false ){ // 修改后未通过校验
                    if(item.dataTypeIsValid === false){
                        $scope.addAlert('', '第'+(i+1)+'行: 该数据类型代码已经存在!');
                        return false;
                    }
                    if(item.siteZhcnIsValid === false){
                        $scope.addAlert('', '第'+(i+1)+'行: 数据项代码不能为汉字!');
                        return false;
                    }
                    if(item.siteLengthTooLargeIsValid === false){
                        $scope.addAlert('', '第'+(i+1)+'行: 工厂代码最大长度为6位!');
                        return false;
                    }
                }
            }
            return true;
        };

        function __deletedRowsExitThisRowByHashKey(rowHashKey){
            for(var n = 0; n < $scope.config.deletedRows; n++){
                if(rowHashKey == $scope.config.deletedRows[n].$$hashKey){
                    return true;
                }
            }
            return false;
        };  


        $scope.init = function(){
            //console.log($(".andon-main-block").height());
            //console.log($(".plc-choose").height());
            //console.log(angular.element(".plc-menu").height());
            //$("#andon-grid-table").css('height',600);
            __requestTableDatas();
        };
        $scope.init();

        function __requestTableDatas(){
            $scope.config.deletedRows = [];
            userService
                .siteList()
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.gridFactory.data = resultDatas.response;
                    }
                    return;
                }, function (resultDatas){
                    $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
                });
        };


    }]);