outputTargetModule.service('outputTargetService', [
    'HttpAppService',
    function (HttpAppService) { 

    return { 
    	//用于数据传递

    	// 下拉值接口：设备编码  物料编码、班次
    	dataMaterialCodes: requestDataMaterialCodes,
    	dataShifts: requestDataShifts,

    	outputTargetList: queryOutputTargetList,
    	outputTargetListCount: queryOutputTargetListCount,
    	outputTargetExist: queryOutputTargetExist,
    	outputTargetSave: queryOutputTargetSave

    };

    function requestDataMaterialCodes(model){ 
		var url = HttpAppService.URLS.XLZ_MATERIAL_CODES
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{MODEL}#", model)
					;  
		return HttpAppService.get({ 
			url: url 
		}); 
	};  
	
	function requestDataShifts(resrce, workCenter){ 
		var url = HttpAppService.URLS.XLZ_SHIFTS
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{RESRCE}#", resrce)
					.replace("#{WORK_CENTER}#", workCenter)
					; 
		return HttpAppService.get({ 
			url: url 
		}); 
	}; 

	// required: 		item、shift、start_data_time、end_date_time、work_center、line、pageIndex、pageCount
		// not required: 	resrce、resource_type、model
		//{ 
		// 	item:
		// 	shift:
		// 	startDataTime:
		// 	endDateTime:
		// 	workCenter:
		// 	line:
		// 	pageIndex:
		// 	pageCount:

		// 	resrce:
		// 	resourceType:
		// 	model:
		//} 
	//outputTargetService.queryOutputTargetList
	function queryOutputTargetList(params){ 
		var url = HttpAppService.URLS.OUTPUT_TARGET_LIST
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{ITEM}#", params.item)
					.replace("#{RESRCE}#", params.resrce)
					.replace("#{SHIFT}#", params.shift)
					.replace("#{STAET_DATE_TIME}#", params.startDateTime)
					.replace("#{END_DATE_TIME}#", params.endDateTime)
					.replace("#{MODEL}#", params.model)
					.replace("#{PAGE_INDEX}#", params.pageIndex)
					.replace("#{PAGE_COUNT}#", params.pageCount)
					.replace("#{WORK_CENTER}#", params.workCenter)
					.replace("#{LINE}#", params.line)
					.replace("#{SHIFT_DESC}#", params.shiftDesc)
				.replace("#{resource_type}#", params.resource_type)
			;
		return HttpAppService.get({ 
			url: url
		});
	};
	function queryOutputTargetListCount(params){
		var url = HttpAppService.URLS.OUTPUT_TARGET_LIST_COUNT
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{ITEM}#", params.item)
					.replace("#{RESRCE}#", params.resrce)
					.replace("#{SHIFT}#", params.shift)
					.replace("#{STAET_DATE_TIME}#", params.startDateTime)
					.replace("#{END_DATE_TIME}#", params.endDateTime)
					.replace("#{MODEL}#", params.model)
					.replace("#{WORK_CENTER}#", params.workCenter)
					.replace("#{LINE}#", params.line)
					.replace("#{SHIFT_DESC}#", params.shiftDesc)
				.replace("#{resource_type}#", params.resource_type)
					; 
		return HttpAppService.get({ 
			url: url
		});
	}


	//{ 
		// 	item:
		// 	shift:
		// 	targetDate:

		// 	resrce:
	//} 
	function queryOutputTargetExist(params){
		var url = HttpAppService.URLS.OUTPUT_TARGET_EXIST
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{ITEM}#", params.item)
					.replace("#{RESRCE}#", params.resrce)
					.replace("#{SHIFT}#", params.shift)
					.replace("#{TARGET_DATE}#", params.targetDate)
					;  
		return HttpAppService.get({
			url: url
		});
	};	
	// {
		// 	handle:
		// 	site: 
		// 	item: 
		// 	operation: "#",
		// 	resrce:
		// 	targetQtyYield:
		// 	targetQtyYieldUnit:
		// 	targetDate:
		// 	shift:
		// 	shiftDescription:
		// 	isDefaultValue:
		// 	modifiedUser:
		// 	viewActionCode:
	// } 
	function queryOutputTargetSave(paramActions){
		var url = HttpAppService.URLS.OUTPUT_TARGET_SAVE; 
		return HttpAppService.post({ 
			url: url, 
			paras: paramActions 
		}); 
	}; 


}]);


outputTargetModule
    .run([
        '$templateCache',
        function ($templateCache){
            $templateCache.put('andon-ui-grid-outtarget/isDefaultValue-checkbox',
                "<div title=\"{{col.cellTooltip(row,col)}}\" ng-click=\"row.entity.isDefaultValue = row.entity.isDefaultValue\" class=\"ui-grid-cell-contents flex-center\"  ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><input type=\"checkbox\" ng-true-value=\"'true'\" ng-click=\"row.entity.isDefaultValue = row.entity.isDefaultValue\" disabled class=\"checkbox-column\"  ng-model=\"row.entity.isDefaultValue\" /></div>"
            );
			$templateCache.put('andon-ui-grid-tpls/network-targeQtyYield',
				"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.targeQtyYield\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
			);
        }]);
















