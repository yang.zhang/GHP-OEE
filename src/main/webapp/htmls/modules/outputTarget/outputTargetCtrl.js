/**
 *
 * [ 产出目标值维护界面所对应Controller: outputTargetCtrl ]
 *
 */
outputTargetModule.controller('outputTargetCtrl',[
    '$scope', '$http', '$q', '$uibModal',
    'HttpAppService', '$timeout', 'outputTargetService',
    'uiGridConstants','$uibModal', 'plcService','yieldMaintainService',
    'UtilsService','uiGridValidateService','$state',
    function ($scope, $http, $q, $uibModal,
            HttpAppService, $timeout, outputTargetService,
            uiGridConstants,$uibModal, plcService,yieldMaintainService,
              UtilsService,uiGridValidateService,$state) {

    $scope.outputTargetTitle = '产出目标值维护';

    $scope.config = {
        gridApi: null,
        deletedRows: [],   // 缓存删除且未提交的表格行数据

        $workCenterSelect: null,    // ui-select 生产区域对象
        $lineSelect: null,          // ui-select 拉线对象
        $deviceTypeSelect: null,    // ui-select 设备类型对象
        $deviceNumSelect: null,     // ui-select 设备编码对象

        btnDisabledQuery: true,     // 是否禁用<查询>按钮
        btnDisabledDelete: true,    // 是否禁用<删除>按钮
        btnDisabledAdd: true,       // 是否禁用<新增>按钮
        btnDisabledSave: true,      // 是否禁用<保存>按钮
        btnDisabledEdit: true,      // 是否禁用<一键编辑>按钮

        currentModel: null,
        targetOutput: null,         // 一键编辑:目标产出值

        defaultSelectItems: [
            {
                descriptionNew:"",
                description:"",
                workCenter: null,
                resrce: null,
                shift: null
            },
            {
                descriptionNew:"--- 请选择 ---",
                description:"--- 请选择 ---",
                workCenter: null,
                resrce: null,
                shift: null
            }
        ],
        defaultSelectItem: {
            descriptionNew:"--- 请选择 ---",
            description:"--- 请选择 ---",
            workCenter: null,
            resrce: null,
            shift: null
        },
        workCenters: [],            // 生产区域下拉列表
        currentWorkCenter: null,    // 当前所选生产区域
        lines: [],                  // 拉线下拉列表
        currentLine: [],          // 当前所选拉线对象
        deviceTypes: [],            // 设备类型下拉列表
        currentDeviceType: null,    // 当前所选设备类型对象
        deviceNums: [],             // 设备编码下拉列表
        currentDeviceNum: null,     // 当前所选设备编码对象

        materialCodes: [],          // 物料编码下拉列表
        currentMaterialCode: null,  // 当前所选物料编码对象
        shifts: [],                 // 班次下拉列表
        currentShift: null,         // 当前所选班次对象

        queryManDateStart: UtilsService.serverFommateDateShow(new Date()),    // 查询条件:开始日期
        queryManDateEnd: UtilsService.serverFommateDateShow(new Date()),      // 查询条件:结束日期
        canEdit : false             // 当前用户是否对该activity有编辑权限
    };

    $scope.config.canEdit = $scope.modulesRWFlag("#"+$state.$current.url.source);
        //拉线多选帮助
        $scope.toWireLineHelp = function () {
            if($scope.config.currentWorkCenter == null){
                $scope.addAlert('','请先选择生产区域');
                return;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/lineSelectModal.html',
                controller: 'LineSelectMoreCtrl',
                size: 'lg',
                backdrop: 'false',
                scope: $scope,
                openedClass: 'flex-center-parent',
                resolve: {
                    selectLineModal: function () {
                        return {workLine: $scope.config.lines};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                console.log(selectedItem);
                $scope.config.currentLine = selectedItem.lineDatas;
                $scope.config.currentLineDesc = selectedItem.lineName;
                if($scope.config.currentLine.length >0){
                    $scope.config.showSelectLine = false;
                }
                $scope.lineChanged();
            }, function () {
            });
        };
        $scope.deleteAll = function(item){
            if(item == 'line'){
                $scope.config.currentLine = [];
                $scope.config.currentLineDesc = [];
                $scope.config.deviceNums = [];
                $scope.config.currentDeviceNum = null;
                $scope.config.showSelectLine = true;
            }
        }
    $scope.gridOutputTarget = {
            enablePagination: true,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,
            paginationCurrentPage: 1,
            // paginationTemplate: 'ui-grid/pagination',
            totalItems: 0,


            useExternalPagination: true,

            showGridFooter: false,
            modifierKeysToMultiSelectCells: false,
            data: [],
            columnDefs: [
                {
                    field: 'handle', name: 'handle', visible: false
                },
                {
                    field: 'resourcePO.resrce', name: 'resourcePOResrce', displayName: '设备编码', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'resourcePO.asset', name: 'ziChanNum', displayName: '资产号', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'resourcePO.description', name: 'resourceName', displayName: '设备名称', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },cellClass:'grid-no-editable',
                    // cellTemplate: 'andon-ui-grid-tpls/network-plcIp',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'model', name: 'model', displayName: 'Model', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },cellClass:'grid-no-editable',
                    // cellTemplate: 'andon-ui-grid-tpls/network-plcPort',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'item', name: 'item', displayName: '物料编码', visible: true, minWidth: 150,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'shiftDescription', name: 'shiftDescription', displayName: '班次', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },cellClass:'grid-no-editable',
                    // cellTemplate: 'andon-ui-grid-tpls/network-plcPort',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'targeQtyYield', name: 'targeQtyYield', displayName: '目标产出值', visible: true, minWidth: 100,
                    enableCellEdit: $scope.config.canEdit, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },cellClass:'text-align-right',
                    cellTemplate: 'andon-ui-grid-tpls/network-targeQtyYield',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return true;
                        // var row = arguments[0].row,
                        //     col = arguments[0].col;
                        // if(angular.isUndefined(row.entity.targetDate) || row.entity.targetDate == null || row.entity.targetDate == "" || row.entity.targetDate=="*"){
                        //     return false;
                        // }
                        // return true;
                    }
                },
                {
                    field: 'targetDate', name: 'targetDate', displayName: '日期', visible: true, minWidth: 100,
                    enableCellEdit: $scope.config.canEdit, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    // cellTemplate: 'andon-ui-grid-tpls/network-plcPort',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row,
                            col = arguments[0].col;
                        if(angular.isUndefined(row.entity.targetDate) || row.entity.targetDate == null || row.entity.targetDate == "" || row.entity.targetDate=="*"){
                            return false;
                        }
                        return true;
                    }
                },
                {
                    field: 'isDefaultValue', name: 'isDefaultValue', displayName: '设为默认值', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },cellClass:'grid-no-editable',
                    cellTemplate: 'andon-ui-grid-outtarget/isDefaultValue-checkbox',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'isDeleted', name: 'isDeleted', displayName: '删除', width: 40,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    visible: $scope.config.canEdit,
                    cellTooltip: function(row, col){ return "删除该行"; },
                    cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col"><span class="ion-trash-a"></span></div>',
                    headerTemplate: '<div class="xbr-delete-col-header">删除</div>'
                }
            ],
            onRegisterApi: __onRegisterApi
        };

    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        $scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue) {
            if(colDef.field === "targeQtyYield" || colDef.name === "targeQtyYield") {
                if(isNaN(parseInt(rowEntity.targeQtyYield)) || parseInt(rowEntity.targeQtyYield)<0){
                    uiGridValidateService.setInvalid(rowEntity, colDef);
                    rowEntity.dataTypeIsValid = false;
                    return;
                }else{
                    $scope.bodyConfig.overallWatchChanged = true;
                    rowEntity.dataTypeIsValid = true;
                }
            }
        });
        gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
                if(newRowCol.col.field == "isDeleted" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                var entity = newRowCol.row.entity;
                if(!angular.isUndefined(entity.isDefaultValue) && entity.isDefaultValue == "true"){
                    //默认目标产出值不允许删除
                    $scope.addAlert('danger', '默认目标产出值不允许删除!');
                }else{
                    var hashKey = entity.$$hashKey;
                    __deleteRowDataByHashKey(hashKey);
                }
            }
        });
        // 分页相关
        $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
            // console.log("刷新界面 第 "+currentPage+" 页,每页 "+pageSize+"个元素!");
            $scope.bodyConfig.overallWatchChanged = false;
            __requestTableDatas(currentPage, pageSize);
            return true;
        }, $scope);

        $scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                var promise = $q.defer();
                if($scope.config.gridApi.rowEdit){
                    $scope.config.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
                }
                // if(oldValue == newValue){
                //     promise.resolve();
                //     return;
                // }
                $scope.config.btnDisabledSave = false;
                rowEntity.hasChanged = true;
                return;
                if(colDef.field === "targeQtyYield" || colDef.name === "targeQtyYield"){
                    var newTarget = window.parseInt(newValue);
                    rowEntity.hasChanged = true;
                    if(newTarget.toString()!='NaN' && newTarget.toString()>=0){
                        promise.resolve();
                        rowEntity.targetQtyYieldIsValid = true;
                        uiGridValidateService.setValid(rowEntity, colDef);
                    }else{
                        $scope.addAlert('danger', '目标产出值必须为数值且不能为负数!');
                        uiGridValidateService.setInvalid(rowEntity, colDef);
                        promise.reject();
                        rowEntity.targetQtyYieldIsValid = false;
                    }
                }else{
                    promise.resolve();
                }
        });
        $scope.config.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
            $scope.config.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
        });
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            var count = $scope.config.gridApi.selection.getSelectedCount();
            $scope.config.btnDisabledDelete = count > 0 ? false : true;
            $scope.config.btnDisabledEdit = count > 0 ? false : true;
            gridRow.entity.isSelected = gridRow.isSelected;
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            var count = $scope.config.gridApi.selection.getSelectedCount();
            $scope.config.btnDisabledDelete = count > 0 ? false : true;
            $scope.config.btnDisabledEdit = count > 0 ? false : true;
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
        });
    };

    function __deletedRowsExitThisRowByHashKey(rowHashKey){
        for(var n = 0; n < $scope.config.deletedRows; n++){
            if(rowHashKey == $scope.config.deletedRows[n].$$hashKey){
                return true;
            }
        }
        return false;
    };

        /**
         * 将数组转为String，以逗号分隔
         * */
    function __arrayToString (array){
        var lineString = [];
        for(var i=0;i<array.length;i++){
            lineString[i] = array[i].workCenter;
        }
        return lineString.toString();
    };
    /**
     *
     * [saveAllAcitons 点击保存按钮触发该方法]
     *
     */
    $scope.saveAllAcitons = function(){
        for(var i = 0; i < $scope.gridOutputTarget.data.length; i++) {
            if($scope.gridOutputTarget.data[i].dataTypeIsValid == false){
                $scope.addAlert("","数据存在错误,无法保存");
                return;
            }
        }
            var paramActions = [];
        for(var i = 0; i < $scope.gridOutputTarget.data.length; i++){
            var row = $scope.gridOutputTarget.data[i];
            if(row.hasChanged){
                // var viewActionCode = angular.isUndefined(row.handle) ? 'C' : 'U';
                if(angular.isUndefined(row.handle)){ //创建新数据 ，若是删除中存在该条数据，则直接忽略该行,并在后面将删除操作也忽略
                    if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
                        var paramAction = angular.copy(row);
                        paramAction.resrce = paramAction.resourcePO.resrce;
                        paramAction.viewActionCode = "C";
                        paramAction.site = HttpAppService.getSite();
                        paramActions.push(paramAction);
                    }else{}
                }else{ //修改已存于服务器的行数据 ，若删除列表中有该行，则直接忽略该修改提交，并在后面将删除操作添加进来
                    if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
                        var paramAction = angular.copy(row);
                        paramAction.viewActionCode = "U";
                        paramActions.push(paramAction);
                    }else{}
                }
            }
        }
        for(var i = 0; i < $scope.config.deletedRows.length; i++){
            var deleteTempRow = $scope.config.deletedRows[i];
            if(!angular.isUndefined(deleteTempRow.handle)){// 服务器端数据的删除操作,直接添加进去即可
                var paramAction = angular.copy(deleteTempRow);
                paramAction.viewActionCode = "D";
                paramActions.push(paramAction);
            }else{ }
        }
        if(paramActions.length > 0){
            outputTargetService
                .outputTargetSave(paramActions)
                .then(function (resultDatas){
                    $scope.addAlert('success', '保存成功!');
                    $scope.bodyConfig.overallWatchChanged = false;
                    __requestTableDatas();
                },function (resultDatas){
                    var resultStr = '保存失败,'+resultDatas.myHttpConfig.statusDesc;
                    var errMsg = resultDatas.headers("error-message");
                    if(!UtilsService.isEmptyString(errMsg)){
                        var errMsgObj = JSON.parse(errMsg);
                        var objStr = errMsgObj.errorJson;
                        if(!UtilsService.isEmptyString(objStr)){
                            var obj = JSON.parse(objStr);
                            // <设备编码>、<物料编码>、<班次描述>在<日期>的目标产出值已存在，不允许新增
                            if(angular.isUndefined(obj) || obj == null){
                            }else{
                                resultStr = "<"+obj.resrce+"><"+obj.item+"><"+window.decodeURI(window.escape(obj.shiftDescription))+">在<"+obj.targetDate+">的目标产出值已存在，不允许新增!";
                            }
                        }
                    }
                    $scope.addAlert('danger', resultStr);
                });
        }else{
            $scope.addAlert('没有修改,无需保存!');
        }
    };

    /**
     *
     * [queryDatas 点击查询按钮触发该方法]
     *
     */
    $scope.queryDatas = function(){
        if($scope.bodyConfig.overallWatchChanged){
            var promise =UtilsService.confirm('还有修改未保存，是否继续', '提示', ['确定', '取消']);
            promise.then(function(){
                $scope.gridOutputTarget.paginationCurrentPage = 1;
                __requestTableDatasCount();
                __requestTableDatas();
            }, function(){
                //取消停留在原页面
                $scope.bodyConfig.overallWatchChanged = true;
            });
        }else{
            $scope.gridOutputTarget.paginationCurrentPage = 1;
            __requestTableDatasCount();
            __requestTableDatas();
        };

    };

    /**
     *
     * [deletedRows 点击删除按钮触发该方法]
     *
     */
    $scope.deletedRows = function(){
        var selectRows = $scope.config.gridApi.selection.getSelectedRows();
        $scope.config.btnDisabledSave = false;
        for(var i = 0; i<selectRows.length; i++){
            if(selectRows[i].isDefaultValue == "true"){
                selectRows[i].canDeleted = false;
            }else{
                selectRows[i].canDeleted = true;
            }
        }
        for(var i = $scope.gridOutputTarget.data.length-1; i >= 0; i--){
            if($scope.gridOutputTarget.data[i].canDeleted == true && $scope.gridOutputTarget.data[i].isSelected == true){
                $scope.config.btnDisabledSave = false;
                $scope.config.deletedRows.push(angular.copy($scope.gridOutputTarget.data[i]));
                $scope.gridOutputTarget.data.splice(i, 1);
                $scope.bodyConfig.overallWatchChanged = true;
            }else if($scope.gridOutputTarget.data[i].isSelected == true){
                $scope.addAlert('', "第"+(i+1)+"行: 默认目标产出值不允许删除");
            }
        }
    };

    /**
     *
     * [modalQueryMaterialsCode 物料编码model选择框]
     *
     */
    $scope.modalQueryMaterialsCode = function(){
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modules/yieldMaintain/model/ModalQueryMaterialsCode.html',
            controller: 'ModelQueryMaterialsCodeCtr',
            size: 'lg',
            backdrop:'false',
            openedClass:'flex-center-parent',
            scope : $scope,
            resolve: {
               modalMaterialsCodeOptions: function () {
                   return {
                        multiSelect: false
                   };
               }
            }
        });
        modalInstance.result.then(function (selectedMaterials) {
            if(!angular.isUndefined(selectedMaterials) && !angular.isUndefined(selectedMaterials.Materials) && selectedMaterials.Materials!=null && selectedMaterials.Materials.length >0 ){
                $scope.config.currentModel = selectedMaterials.model;
                $scope.config.currentMaterialCode = selectedMaterials.Materials[0];
                $scope.config.currentMaterialCode.itemBack = selectedMaterials.Materials[0].item;
                __setBtnsDisable();
            }
        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    }

    /**
     *
     * [editHandle 点击一键编辑按钮触发该方法]
     *
     */
    $scope.editHandle = function(){
        var targetOutput = $scope.config.targetOutput;
        if(angular.isUndefined(targetOutput) || targetOutput == null){
            $scope.addAlert("","请先输入目标产出值!");
            return;
        }else{
            var newTarget = window.parseInt(targetOutput);
            if(newTarget.toString()!='NaN' && newTarget.toString()>=0){}else{
                $scope.addAlert('danger', '目标产出值必须为数值且不能为负数!');
                return;
            }
        }
        var selectedRows = $scope.config.gridApi.selection.getSelectedGridRows();
        for(var i = 0; i < selectedRows.length; i++){
            var row = selectedRows[i];
            row.entity.targeQtyYield = targetOutput;
            row.entity.hasChanged = true;
        }
        $scope.bodyConfig.overallWatchChanged = true;
        $scope.config.btnDisabledSave = false;
    };

    /**
     *
     * [modalOutputTarget 点击新增按钮触发该方法,在弹出的model中进行新增操作]
     *
     */
    $scope.modalOutputTarget = function(){
        var modelNeedDatas = __initNeedNewResourceDatas();

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalOutputTarget.html',
            controller: 'ModalOutputTargetCtrl',
            size: 'lg',
            backdrop:'false',
            openedClass:'flex-center-parent',
            scope:$scope,
            resolve: {
               outputTargetModelData: function () {
                   return {
                       modelNeedDatas : modelNeedDatas,
                       currentDeviceNum:$scope.config.currentDeviceNum
                   };
               }
            }
        });
        // }
        modalInstance.result.then(function (closeDataObj) {
            $scope.config.btnDisabledSave = false;
            var rows = closeDataObj.gridSelectionRows;
            var startTime = UtilsService.serverFommateDateShow(closeDataObj.obj.startTime);
            var targetOutput = closeDataObj.obj.targetOutput;
            var materialCode = closeDataObj.obj.materialCode;
            var shift = closeDataObj.obj.shiftPo;
            if(angular.isUndefined($scope.gridOutputTarget.data) || $scope.gridOutputTarget.data == null){
                $scope.gridOutputTarget.data = [];
            }
            for(var i = 0; i < rows.length; i++){
                $scope.gridOutputTarget.data.unshift({
                    item: materialCode.item,
                    // model: $scope.config.currentModel.model,
                    model:materialCode.model, // TODO
                    resourcePO: rows[i].resourcePo,
                    shift: shift.shift,
                    shiftDescription: shift.description,
                    targetDate: startTime,
                    targeQtyYield: targetOutput,
                    isDefaultValue: false,
                    hasChanged: true
                });
            }
            $scope.bodyConfig.overallWatchChanged = true;
            // //滚动到新增那一行
            $scope.config.gridApi.cellNav.scrollToFocus($scope.gridOutputTarget.data[$scope.gridOutputTarget.data.length-1]);
        }, function () {
            // $log.info('Modal dismissed at: ' + new Date());
        });
    };


    function __initNeedNewResourceDatas(){
        var resultDatas = [];
        for(var i = 0; i< $scope.config.deviceNums.length; i++){
            if($scope.config.deviceNums[i].resrce == null){
                continue;
            }
            resultDatas.push({
                resourcePo:$scope.config.deviceNums[i],
                resourceTypePo: $scope.config.currentDeviceType,
            });
        }
        //if($scope.config.currentDeviceType.resourceType == null){
        //    // for(var i = 0; i < $scope.config.deviceTypes.length; i++){
        //        // if($scope.config.deviceTypes[i].resourceType == null){ continue; }
        //        for(var j = 0; j < $scope.config.deviceNums.length; j++){
        //            if($scope.config.deviceNums[j].resrce == null){ continue; }
        //            resultDatas.push({
        //                resourcePo:$scope.config.deviceNums[j]
        //                //resourceDescription: $scope.config.deviceNums[j].description,
        //                //resourceAsset: $scope.config.deviceNums[j].asset,
        //                //resourceResrce: $scope.config.deviceNums[j].resrce,
        //                // resourceTypePo: $scope.config.deviceTypes[i],
        //                //shiftPo: $scope.config.currentShift,
        //                // materialCode: $scope.config.currentMaterialCode.materialCode
        //            });
        //        }
        //    // }
        //}else{
        //    for(var j = 0; j < $scope.config.deviceNums.length; j++){
        //        if($scope.config.deviceNums[j].resrce == null){ continue; }
        //        resultDatas.push({
        //            resourcePo:$scope.config.deviceNums[j],
        //            resourceTypePo: $scope.config.currentDeviceType,
        //            //shiftPo: $scope.config.currentShift,
        //
        //            //resourceDescription: $scope.config.deviceNums[j].description,
        //            //resourceAsset: $scope.config.deviceNums[j].asset,
        //            //resourceResrce: $scope.config.deviceNums[j].resrce,
        //            //resourceType: $scope.config.currentDeviceType.resourceType,
        //            //shift: $scope.config.currentShift.shift,
        //            //shiftDescription:$scope.config.currentShift.description,
        //            // materialCode: $scope.config.currentMaterialCode.materialCode
        //        });
        //    }


        var returnObj = {
            queryData: {
                materialCode: $scope.config.currentMaterialCode,
                shiftPo: $scope.config.currentShift,
                //shiftDescription:$scope.config.currentShift.description,
            },
            gridRowDatas: resultDatas
        };
        return returnObj;
    };

    function __requestTableDatas(pageIndex, pageCount){
        var pageIndexX = pageIndex,
            pageCountX = pageCount;
        if(angular.isUndefined(pageIndexX) || pageIndexX == null || pageIndexX <= 0){
            pageIndexX = 1;
        }
        if(angular.isUndefined(pageCountX) || pageCountX == null || pageCountX <= 0){
            pageCountX = $scope.gridOutputTarget.paginationPageSize;
        }
        var params = {
            item: $scope.config.currentMaterialCode.item,
            shift: $scope.config.currentShift.shift,
            shiftDesc: $scope.config.currentShift.description,
            startDateTime: UtilsService.serverFommateDate(new Date($scope.config.queryManDateStart)),
            endDateTime: UtilsService.serverFommateDate(new Date($scope.config.queryManDateEnd)),
            workCenter: $scope.config.currentWorkCenter.workCenter,
            line: __arrayToString($scope.config.currentLine),
            pageIndex: pageIndexX,
            pageCount: pageCountX,
            model:$scope.config.currentMaterialCode.model?$scope.config.currentMaterialCode.model:null,
            resrce:$scope.config.currentDeviceNum?$scope.config.currentDeviceNum.resrce:null,
            resource_type : $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:null,
        };

        outputTargetService
            .outputTargetList(params)
            .then(function (resultDatas){
                $scope.bodyConfig.overallWatchChanged = false;
                $scope.gridOutputTarget.paginationCurrentPage = pageIndexX;
                if(resultDatas.response && resultDatas.response.length > 0){
                    for(var i = 0; i < resultDatas.response.length; i++){
                        resultDatas.response[i].isSelected = false;
                        if(angular.isUndefined(resultDatas.response[i].isDefaultValue) || resultDatas.response[i].isDefaultValue==null){
                            // resultDatas.response[i].isDefaultValue = 'X';
                            // resultDatas.response[i].handle = "xxx";
                        }
                    }
                    //btnDisabledQuery: true,
                    //    btnDisabledDelete: true,
                    //    btnDisabledAdd: true,
                    //    btnDisabledSave: true,
                    //    btnDisabledEdit: true,
                    $scope.config.btnDisabledQuery = false;
                    $scope.config.btnDisabledDelete = true;
                    $scope.config.btnDisabledSave = true;
                    $scope.config.btnDisabledEdit = true;

                    $scope.gridOutputTarget.data = resultDatas.response;
                    return;
                }else{
                    $scope.gridOutputTarget.data = [];
                    $scope.addAlert('', '暂无数据!');
                }
            }, function (resultDatas){
                $scope.gridOutputTarget.paginationCurrentPage = pageIndexX;
                $scope.gridOutputTarget.data = [];
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });
    };

    function __requestTableDatasCount(){
        var params = {
            item: $scope.config.currentMaterialCode.item,
            shift: $scope.config.currentShift.shift,
            shiftDesc: $scope.config.currentShift.description,
            startDateTime: UtilsService.serverFommateDate(new Date($scope.config.queryManDateStart)),
            endDateTime: UtilsService.serverFommateDate(new Date($scope.config.queryManDateEnd)),
            workCenter: $scope.config.currentWorkCenter.workCenter,
            line: __arrayToString($scope.config.currentLine),
            model:$scope.config.currentMaterialCode.model?$scope.config.currentMaterialCode.model:null,
            resrce:$scope.config.currentDeviceNum?$scope.config.currentDeviceNum.resrce:null,
            resource_type : $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:null,
        };

        outputTargetService
            .outputTargetListCount(params)
            .then(function (resultDatas){
                $scope.bodyConfig.overallWatchChanged = false;
                $scope.gridOutputTarget.totalItems = resultDatas.response;
                // $scope.gridOutputTarget.totalItems = resultDatas.response;
            }, function (resultDatas){
                $scope.gridOutputTarget.totalItems = 0;
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });
    };

    function __deleteRowDataByHashKey(hashKey){
        $scope.bodyConfig.overallWatchChanged = true;
        for(var i = $scope.gridOutputTarget.data.length - 1; i >= 0; i--){
            if(hashKey == $scope.gridOutputTarget.data[i].$$hashKey){
                $scope.config.btnDisabledSave = false;
                $scope.config.deletedRows.push(angular.copy($scope.gridOutputTarget.data[i]));
                $scope.gridOutputTarget.data.splice(i, 1);
            }
        }
    } ;

    $scope.init = function(){
        __initDropdowns('init');
        __requestDropdownWorkCenters();
        __requestDropdownDeviceTypes();
        __requestDropdownMaterialCodes();
    };

    $scope.init();

    function __initDropdowns(type){

        if(type == 'init'){
            $scope.config.workCenters = [];
            $scope.config.currentWorkCenter = null;
            //$scope.config.workCenters.push($scope.config.defaultSelectItem);
            //$scope.config.currentWorkCenter = $scope.config.workCenters[0];
            $scope.config.deviceTypes = [];
            $scope.config.currentDeviceType = null;
            //$scope.config.deviceTypes = angular.copy($scope.config.defaultSelectItems); // [];
            // $scope.config.deviceTypes.push($scope.config.defaultSelectItem);
            // $scope.config.deviceTypes.push($scope.config.defaultSelectItem);
            //$scope.config.currentDeviceType = $scope.config.deviceTypes[0];

            $scope.config.materialCodes = [];
            $scope.config.currentMaterialCode = null;
            //$scope.config.materialCodes.push($scope.config.defaultSelectItem);
            //$scope.config.currentMaterialCode = $scope.config.materialCodes[0];


            //$scope.config.shifts.push($scope.config.defaultSelectItem);
            //$scope.config.currentShift = $scope.config.shifts[0];
        }

        if(type == 'init' || type == 'workCenterChanged'){
            $scope.config.lines = [];
            $scope.config.currentLine = [];
            //$scope.config.lines = angular.copy($scope.config.defaultSelectItems);
            //$scope.config.currentLine = $scope.config.lines[0];
            $scope.config.deviceNums = [];
            $scope.config.currentDeviceNum = null;
            //$scope.config.deviceNums = angular.copy($scope.config.defaultSelectItems); // [];
            // $scope.config.deviceNums.push($scope.config.defaultSelectItem);
            // $scope.config.deviceNums.push($scope.config.defaultSelectItem);
            //$scope.config.currentDeviceNum = $scope.config.deviceNums[0];
            // $scope.config.currentDeviceNums = [];

            $scope.config.shifts = [];
            $scope.config.currentShift = null;
            //$scope.config.shifts.push($scope.config.defaultSelectItem);
            //$scope.config.currentShift = $scope.config.shifts[0];
            $scope.deleteAll('line');
        }

        if(type == 'lineChanged'){
            $scope.config.deviceNums = [];
            $scope.config.currentDeviceNum = null;
            //$scope.config.deviceNums = angular.copy($scope.config.defaultSelectItems); // [];
            // $scope.config.deviceNums.push($scope.config.defaultSelectItem);
            // $scope.config.deviceNums.push($scope.config.defaultSelectItem);
            //$scope.config.currentDeviceNum = $scope.config.deviceNums[0];
            // $scope.config.currentDeviceNums = [];

            // $scope.config.shifts = [];
            // $scope.config.shifts.push($scope.config.defaultSelectItem);
            // $scope.config.currentShift = $scope.config.shifts[0];
        }

        if(type == 'deviceTypeChanged'){
            $scope.config.deviceNums = [];
            $scope.config.currentDeviceNum = null;
            //$scope.config.deviceNums = angular.copy($scope.config.defaultSelectItems); // [];
            // $scope.config.deviceNums.push($scope.config.defaultSelectItem);
            // $scope.config.deviceNums.push($scope.config.defaultSelectItem);
            //$scope.config.currentDeviceNum = $scope.config.deviceNums[0];
            // $scope.config.currentDeviceNums = [];

            // $scope.config.shifts = [];
            // $scope.config.shifts.push($scope.config.defaultSelectItem);
            // $scope.config.currentShift = $scope.config.shifts[0];
        }

        if(type == 'deviceNumChanged'){
            // $scope.config.shifts = [];
            // $scope.config.shifts.push($scope.config.defaultSelectItem);
            // $scope.config.currentShift = $scope.config.shifts[0];
        }
    } ;

    $scope.tipSelectClick = function(type){
        if(type == 'line'){
            $scope.config.$workCenterSelect.toggle();
        }else if(type == 'deviceNum'){
            $scope.config.$workCenterSelect.toggle();
        }else if(type == 'shift'){
            $scope.config.$deviceNumSelect.toggle();
        }
    } ;
    /////   生成区域、级联选择
    $scope.workCenterChanged = function(){
        if($scope.config.currentWorkCenter == null || $scope.config.currentWorkCenter.workCenter==null){
            __initDropdowns('workCenterChanged');
        }else{
            __initDropdowns('workCenterChanged');

            //拉线
            var workCenter = $scope.config.currentWorkCenter.workCenter;
            var line = __arrayToString($scope.config.currentLine);
            var resourceType = $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:null;
            __requestDropdownLines(workCenter);

            //设备编码
            __requestDropdownDeviceCodes(workCenter,line,resourceType);
            //var line = $scope.config.currentLine.workCenter;
            //var resourceType = $scope.config.currentDeviceType.resourceType;
            //if($scope.config.currentLine.workCenter!=null){
            //    __requestDropdownDeviceCodes(workCenter, line, resourceType);
            //}
            __requestDropdownShifts();
        }
        __setBtnsDisable();
    };

    function __setBtnsDisable(){
        if(!$scope.config.currentWorkCenter
            || !$scope.config.currentShift
            || !$scope.config.currentMaterialCode
            || UtilsService.isEmptyString($scope.config.currentMaterialCode.item)
            /*|| $scope.config.currentMaterialCode.item != $scope.config.currentMaterialCode.itemBack*/){
            $scope.config.btnDisabledAdd = true;
            $scope.config.btnDisabledQuery = true;
            $scope.config.btnDisabledDelete = true;
            $scope.config.btnDisabledEdit = true;
            return;
        }
        $scope.config.btnDisabledAdd = false;
        $scope.config.btnDisabledQuery = false;
        // $scope.config.btnDisabledEdit = false;
        //$scope.config.btnDisabledSave = false;
        // $scope.config.btnDisabledDelete = false;
    };

    /**
     *
     * [lineChanged 拉线下拉列表选择更改时触发]
     *
     */
    $scope.lineChanged = function(){
        __initDropdowns('lineChanged');
        var workCenter = $scope.config.currentWorkCenter.workCenter;
        var line = __arrayToString($scope.config.currentLine);
        var resourceType = $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:null;

        __requestDropdownDeviceCodes(workCenter, line,resourceType);
        // if($scope.config.currentDeviceType != null && $scope.config.currentDeviceType.resourceType!=null){
        //     $scope.config.btnDisabledAdd = false;
        //     $scope.config.btnDisabledQuery = false;
        // }
        __setBtnsDisable();
    };

    /**
     *
     * [deviceTypeChanged 设备类型下拉列表选择更改时触发]
     *
     */
    $scope.deviceTypeChanged = function(){
        //if($scope.config.currentDeviceType == null || $scope.config.currentDeviceType.resourceType==null){
        //    __initDropdowns('deviceTypeChanged');
        //    var workCenter = $scope.config.currentWorkCenter.workCenter;
        //    var line = $scope.config.currentLine.workCenter;
        //    __requestDropdownDeviceCodes(workCenter, line, null);
        //}else{
        //    __initDropdowns('deviceTypeChanged');
        //    var workCenter = $scope.config.currentWorkCenter.workCenter;
        //    var line = $scope.config.currentLine.workCenter;
        //    var resourceType = $scope.config.currentDeviceType.resourceType;
        //
        //    __requestDropdownDeviceCodes(workCenter, line, resourceType);
        //    // if($scope.config.currentLine != null && $scope.config.currentLine.workCenter!=null){
        //    //     $scope.config.btnDisabledQuery = false;
        //    // }
        //}


        __initDropdowns('deviceTypeChanged');
        var workCenter = $scope.config.currentWorkCenter.workCenter;
        var line = __arrayToString($scope.config.currentLine);
        var resourceType = $scope.config.currentDeviceType.resourceType;
        __requestDropdownDeviceCodes(workCenter, line, resourceType);
        __setBtnsDisable();
    };

    /**
     *
     * [deviceNumChanged 设备编码下拉列表选择更改时触发]
     *
     */
    $scope.deviceNumChanged = function(){
        //if($scope.config.currentDeviceNum.resrce != null){
        //    __initDropdowns('deviceNumChanged');
        //    // __requestDropdownShifts();
        //}
        //__setBtnsDisable();
    };

    /**
     *
     * [materialCodeChanged 物料编码下拉列表选择更改时触发]
     *
     */
    $scope.materialCodeChanged = function(){
        __setBtnsDisable();
    	/*if($scope.config.currentWorkCenter!=""&&$scope.config.currentShift!=""&&$scope.config.currentShift!=null&&$scope.config.currentMaterialCode.item!=""){
    		$scope.config.btnDisabledQuery=false;
    	}else{
    		$scope.config.btnDisabledQuery=true;
    	}*/
    };

    /**
     *
     * [banCiChanged 班次下拉列表选择更改时触发]
     *
     */
    $scope.banCiChanged = function(){
        __setBtnsDisable();
    };

    function __requestDropdownWorkCenters(){
        plcService
                .dataWorkCenters()
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.workCenters = [];
                        //$scope.config.workCenters.push($scope.config.defaultSelectItem);
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].workCenter+"   描述:"+resultDatas.response[i].description;
                            $scope.config.workCenters.push(resultDatas.response[i]);
                        }
                        // $scope.config.workCenters = $scope.config.workCenters.concat(resultDatas.response);
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                if(resultDatas.myHttpConfig.isSessionOut == true || resultDatas.myHttpConfig.isTimeout == true || resultDatas.myHttpConfig.status == 1 || resultDatas.myHttpConfig.status == -1){
                }else{
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                }
            });
    };

    function __requestDropdownLines(workCenter){

        plcService
                .dataLines(workCenter)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.lines = [];
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].workCenter+"   描述:"+resultDatas.response[i].description;
                            $scope.config.lines.push(resultDatas.response[i]);
                        }
                        //$scope.config.$lineSelect.toggle();
                        // $scope.config.lines = $scope.config.lines.concat(resultDatas.response);
                        return;
                    }
                    $scope.config.lines = [];
                },function (resultDatas){ //TODO 检验失败
                    $scope.config.lines = [];
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
    };

    function __requestDropdownDeviceTypes(){
        //$scope.config.deviceTypes = angular.copy($scope.config.defaultSelectItems);
        plcService
                .dataResourceTypes()
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.deviceTypes = [];
                        $scope.config.deviceTypes.push($scope.config.defaultSelectItem);
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].resourceType+"   描述:"+resultDatas.response[i].description;
                            $scope.config.deviceTypes.push(resultDatas.response[i]);
                        }
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                if(resultDatas.myHttpConfig.isSessionOut == true || resultDatas.myHttpConfig.isTimeout == true || resultDatas.myHttpConfig.status == 1 || resultDatas.myHttpConfig.status == -1){
                }else{
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                }
            });
    };

    function __requestDropdownDeviceCodes(workCenter, line, resourceType){
        yieldMaintainService
                .requestDataResourceCodesByLine(workCenter, line, resourceType)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.deviceNums = [];
                        $scope.config.deviceNums.push($scope.config.defaultSelectItem);
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].resrce+"   描述:"+resultDatas.response[i].description;
                            $scope.config.deviceNums.push(resultDatas.response[i]);
                        }
                        // $scope.config.deviceNums = $scope.config.deviceNums.concat(resultDatas.response);
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
    };

    function __requestDropdownMaterialCodes(){
        // var resultDatas = {
            //     response: [
            //         {code: 'material-code1', description: 'material-code1-desc'},
            //         {code: 'material-code2', description: 'material-code2-desc'},
            //         {code: 'material-code3', description: 'material-code3-desc'}
            //     ]
            // };
            // for(var i = 0; i < resultDatas.response.length; i++){
            //     resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].code+"   描述:"+resultDatas.response[i].description;
            //     $scope.config.materialCodes.push(resultDatas.response[i]);
            // }
        //
        $scope.config.materialCodes = [];
        $scope.config.materialCodes.push($scope.config.defaultSelectItem);

        var model = "MODEL_1";
        outputTargetService
            .dataMaterialCodes(model)
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    for(var i = 0; i < resultDatas.response.length; i++){
                        resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].item+"   描述:"+resultDatas.response[i].description;
                        $scope.config.materialCodes.push(resultDatas.response[i]);
                    }
                    // $scope.config.deviceNums = $scope.config.deviceNums.concat(resultDatas.response);
                    return;
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
    };

    function __requestDropdownShifts(){
        // var resultDatas = {
        //     response: [
        //         {code: 'banci-1', description: 'banci-1-desc'},
        //         {code: 'banci-2', description: 'banci-2-desc'},
        //         {code: 'banci-3', description: 'banci-3-desc'}
        //     ]
        // };
        // for(var i = 0; i < resultDatas.response.length; i++){
        //     resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].code+"   描述:"+resultDatas.response[i].description;
        //     $scope.config.shifts.push(resultDatas.response[i]);
        // }
        var workCenter = $scope.config.currentWorkCenter.workCenter;
        $scope.config.shifts = [];
        outputTargetService
            .dataShifts(null, workCenter)
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    for(var i = 0; i < resultDatas.response.length; i++){
                        resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].shift+"   描述:"+resultDatas.response[i].description;
                        $scope.config.shifts.push(resultDatas.response[i]);
                    }
                    // $scope.config.deviceNums = $scope.config.deviceNums.concat(resultDatas.response);
                    return;
                }
            },function (resultDatas){ //TODO 检验失败
                __initDropdowns("deviceNumChanged");
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
    };

}]);

/**
 *
 * [ 新增产出目标值model界面所对应的Controller: ModalOutputTargetCtrl ]
 *
 */
outputTargetModule.controller('ModalOutputTargetCtrl',[
    '$scope','$uibModalInstance','outputTargetService',
    'outputTargetModelData', 'UtilsService',
    function ($scope, $uibModalInstance, outputTargetService,
        outputTargetModelData, UtilsService) {

        $scope.config = {
            gridApi: null,

            needValidRemoteCount: 0,
            btnDisabledOk: true,

            modalOutputDate : UtilsService.serverFommateDateShow(new Date()), // 开始日期
            materialCode: null, // 物料编码
            shiftPo: null,
            targetOutput: '' // 目标产出
        };

        $scope.selectResourceOptions = {
            enablePagination: false,
            showGridFooter: false,
            modifierKeysToMultiSelectCells: false,
            data: [],
            multiSelect: true,
            columnDefs: [
                {
                    field: 'handle', name: 'handle', visible: false
                },
                {
                    field: 'resourceTypePo.description', name: 'resourceTypePo.description', displayName: '设备类型', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true }, visible: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'resourcePo.resrce', name: 'resourcePo.resrce', displayName: '设备编码', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'resourcePo.asset', name: 'resourcePo.asset', displayName: '资产号', visible: true, minWidth: 150,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'resourcePo.description', name: 'resourcePo.description', displayName: '设备名称', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                }
            ],
            onRegisterApi: __onRegisterApi
        };

        function __onRegisterApi(gridApi){
            $scope.config.gridApi = gridApi;

            // 选择相关
            $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                //var count = $scope.config.gridApi.selection.getSelectedCount();
                //$scope.config.btnDisabledOk = count > 0 ? false : true;
                __setBtnDisabled();
            });
            $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                //var count = $scope.config.gridApi.selection.getSelectedCount();
                //$scope.config.btnDisabledOk = count > 0 ? false : true;
                __setBtnDisabled();
            });
        };
        //{modelNeedDatas : modelNeedDatas,currentDeviceNum:$scope.config.currentDeviceNum}

        $scope.init = function(){
            $scope.selectResourceOptions.data =[];
            var data = outputTargetModelData.modelNeedDatas.gridRowDatas;
            if(outputTargetModelData.currentDeviceNum == null || outputTargetModelData.currentDeviceNum.resrce == null){
                $scope.selectResourceOptions.data = data;
            }else{
                console.log(outputTargetModelData.currentDeviceNum.resrce);
                for(var i=0;i<data.length;i++){
                    if(data[i].resourcePo.resrce == outputTargetModelData.currentDeviceNum.resrce){
                        $scope.selectResourceOptions.data.push(data[i]);
                    }
                }
            }
            console.log($scope.selectResourceOptions.data);
            $scope.config.materialCode = outputTargetModelData.modelNeedDatas.queryData.materialCode;
            $scope.config.shiftPo = outputTargetModelData.modelNeedDatas.queryData.shiftPo;
        };

        $scope.init();

        $scope.targetOutputChanged = function(){
            __targetOutputChanged();
        };

        function __targetOutputChanged(){
            __setBtnDisabled();
        };

        function __setBtnDisabled(){
            var count = $scope.config.gridApi.selection.getSelectedCount();
            var targetOutput = $scope.config.targetOutput;
            var date = $scope.config.modalOutputDate;
            if(count ==0 || targetOutput == null || targetOutput == "" || date ==null || date == ""){
                $scope.config.btnDisabledOk = true;
                return;
            }
            $scope.config.btnDisabledOk = false;
        };

        /**
         *
         * [ok 点击确定按钮触发该函数]
         *
         */
        $scope.ok = function () {
            var targetOutput = $scope.config.targetOutput;
            if(!angular.isUndefined(targetOutput) && targetOutput != null && targetOutput != ""){
                var newTarget = window.parseInt($scope.config.targetOutput);
                if(newTarget.toString()!='NaN' && newTarget.toString()>=0){ }else{
                     $scope.addAlert('info', '目标产出值必须为数值且不能为负数!');
                    //alert('目标产出值必须为数值且不能为负数!');
                    return;
                }
            }
            if(targetOutput == null){
                $scope.addAlert('info', '目标产出值不能为空!');
                return;
            }
            var d = new Date($scope.config.modalOutputDate);
            if(angular.isUndefined(d) || d == null ){
                 $scope.addAlert('info', '开始日期不能为空!');
                //alert('', '开始日期不能为空!');
                return;
            }else if(d.getTime() + (24*60*60*1000) <= (new Date()).getTime()){
                 $scope.addAlert('info', '不能新增当日以前日期的目标产出值!');
                //alert('不能新增当日以前日期的目标产出值!');
                return;
            }
            //{
                //  item:
                //  shift:
                //  targetDate:

                //  resrce:
            //}
            var selectedRows = $scope.config.gridApi.selection.getSelectedGridRows();
            $scope.config.needValidRemoteCount = selectedRows.length;
            for(var i = 0; i < selectedRows.length; i++){
                var entity = selectedRows[i].entity;
                //<设备编码>、<物料编码>、<班次描述>在<日期>
                var params = {
                    item: $scope.config.materialCode.item, //entity.resourceAsset,
                    shift: $scope.config.shiftPo.shift,
                    targetDate: UtilsService.serverFommateDate(new Date($scope.config.modalOutputDate)),
                    resrce: entity.resourcePo.resrce
                };
                outputTargetService
                    .outputTargetExist(params)
                    .then(function (resultDatas){
                        $scope.config.needValidRemoteCount--;
                        // if(resultDatas.response){ //已经存在:true
                        //     console.log("已经存在");
                        //      $scope.addAlert('danger', '该数据类型已经存在!');
                        //     entity.canAdded = false;
                        // }else{  //不存在冲突，可以使用
                        //     console.log("不存在冲突，可以使用");
                        //     entity.canAdded = true;
                        // }
                        var canAdded = false;
                        if(resultDatas.response){ //已经存在:true
                            // console.log("已经存在");
                            $scope.addAlert('danger', '该数据类型已经存在!');
                            canAdded = false;
                        }else{  //不存在冲突，可以使用
                            // console.log("不存在冲突，可以使用");
                            canAdded = true;
                        }
                        for(var x = 0; x < selectedRows.length; x++){
                            var handle = selectedRows[x].entity.resourcePo.handle;
                            var url = resultDatas.config.url;
                            var resrce = url.substring(url.indexOf('resrce')+7, url.indexOf('&shift='));
                            // var shift = url.substring(url.indexOf('shift')+6, url.indexOf('&target_date='));
                            if(selectedRows[x].entity.resourcePo.resrce == resrce){
                                selectedRows[x].entity.canAdded = canAdded;
                            }
                        }
                        __realAddAcion();
                    },function (resultDatas){ //TODO 检验失败
                        $scope.config.needValidRemoteCount--;
                        // $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                        __realAddAcion();
                    });
            }
            function __realAddAcion(){
                if($scope.config.needValidRemoteCount > 0){
                    return;
                }
                var canAddedRows = true;
                var inValidRowsCount = 0;
                var rows = $scope.config.gridApi.selection.getSelectedGridRows();
                for(var i = 0; i < rows.length; i++){
                    if(angular.isUndefined(rows[i].entity.canAdded) || rows[i].entity.canAdded==null || rows[i].entity.canAdded==false ){
                        canAddedRows = false;
                        inValidRowsCount++;
                    }
                }
                // if(canAddedRows){
                if (inValidRowsCount==0) {
                    $uibModalInstance.close({
                        gridSelectionRows: $scope.config.gridApi.selection.getSelectedRows(),
                        obj: {
                            materialCode: $scope.config.materialCode,
                            startTime: new Date($scope.config.modalOutputDate),
                            targetOutput: $scope.config.targetOutput,
                            shiftPo:$scope.config.shiftPo
                        }
                    });
                }
            }
        };

        /**
         *
         * [cancel 点击取消按钮触发该函数]
         *
         */
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

}]);




