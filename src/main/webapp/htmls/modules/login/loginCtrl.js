loginModule.controller('loginCtrl', [
	'$scope', '$http', 'HttpAppService', '$state',
	function ($scope, $http, HttpAppService, $state) {
	
	$scope.config = {
		username: '',
		password: ''
	};

	$scope.login = function(){
		var url = HttpAppService.URLS.LOGIN;
		$http.post(url,{
			j_username: $scope.config.username,
			j_password: $scope.config.password
		})
        .success(function (data, status, headers, config) {
        	$state.go("andon.home");
        })
        .error(function (){
        	$state.go("andon.home");
        });
	};


}]);