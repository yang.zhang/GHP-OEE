alertModule.service('alertService', [ 
	'HttpAppService', 
	function (HttpAppService) { 

	return { 
		// 删除相关
		alertReasonCodeIsUsed: alertReasonCodeIsUsed,
		alertReasonCodeChange: alertReasonCodeChange,

		// 设备类型输入帮助  
		alertResrceTypeAll: alertResrceTypeAll,

		// 当前生效查询 
		alertReasonCodeAlertCurrent: alertReasonCodeAlertCurrent,
		alertReasonCodeAlertCurrentCount: alertReasonCodeAlertCurrentCount,


		// 历史数据 -- > 历史变更查询 
		alertReasonCodeAlertHisChange: alertReasonCodeAlertHisChange,
		alertReasonCodeAlertHisChangeCount: alertReasonCodeAlertHisChangeCount,


		// 历史数据 -- > 历史状态查询
		alertReasonCodeAlertHisStatus: alertReasonCodeAlertHisStatus,
		alertReasonCodeAlertHisStatusCount: alertReasonCodeAlertHisStatusCount,

		// 文件下载  
		// ALARM_EXPORT_EXCEL: baseUrl + "reasoncode/alert/site/#{SITE}#/export_excel?resrce_type=#{RESRCE_TYPE}#"
		 alertExportExcel: alertExportExcel,
		//确定
		conmmitSave : conmmitSave,
		keepDelete : keepDelete,
		
		getAlertCommentList:getAlertCommentList,
		alertCommentIsUsed : alertCommentIsUsed,
		changeAlertComment:changeAlertComment
	};
	// alertReasonCodeIsUsed: alertReasonCodeIsUsed,
	// 	alertReasonCodeChange: alertReasonCodeChange,

	function alertReasonCodeIsUsed(handle){
		var url = HttpAppService.URLS.ALARM_REASON_CODE_IS_USED
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{HANDLE}#", handle)
					;
		return HttpAppService.get({
			url: url
		});
	}

	function alertReasonCodeChange(paramActions){
		var url = HttpAppService.URLS.ALARM_REASON_CODE_CHANGE
				.replace("#{SITE}#", HttpAppService.getSite())
			;
		return HttpAppService.post({
			url: url,
			paras: paramActions
		});
	}

	function alertResrceTypeAll(resrceType){
		var url = HttpAppService.URLS.ALARM_RESRCE_TYPE_ALL
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{RESRCE_TYPE}#", resrceType)
					;
		return HttpAppService.get({
			url: url
		});
	}

	function alertReasonCodeAlertCurrent(resrceType,index,count){
		console.log(resrceType+index+count);
		var url = HttpAppService.URLS.ALARM_REASON_CODE_ALERT_CURRENT
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{RESRCE_TYPE}#", resrceType)
				.replace("#{index}#", index)
				.replace("#{count}#", count)

			;
		return HttpAppService.get({
			url: url
		});
	}
		function alertReasonCodeAlertCurrentCount(resrceType){
			var url = HttpAppService.URLS.ALARM_REASON_CODE_ALERT_CURRENT_COUNT
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{RESRCE_TYPE}#", resrceType)
				;
			return HttpAppService.get({
				url: url
			});
		}

	function alertReasonCodeAlertHisStatus(resrceType, time,index,count){
		var url = HttpAppService.URLS.ALARM_REASON_CODE_ALERT_HIS_STATUS
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{RESRCE_TYPE}#", resrceType)
					.replace("#{TIME}#", time)
				.replace("#{index}#", index)
				.replace("#{count}#", count)
					;
		return HttpAppService.get({
			url: url
		});
	}

	function alertReasonCodeAlertHisChange(resrceType, time,index,count){
		var url = HttpAppService.URLS.ALARM_REASON_CODE_ALERT_HIS_CHANGE
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{RESRCE_TYPE}#", resrceType)
					.replace("#{TIME}#", time)
				.replace("#{index}#", index)
				.replace("#{count}#", count)
			;
		return HttpAppService.get({
			url: url
		});
	}
		function alertReasonCodeAlertHisStatusCount(resrceType, time){
			var url = HttpAppService.URLS.ALARM_REASON_CODE_ALERT_HIS_STATUS_COUNT
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{RESRCE_TYPE}#", resrceType)
					.replace("#{TIME}#", time)
				;
			return HttpAppService.get({
				url: url
			});
		}

		function alertReasonCodeAlertHisChangeCount(resrceType, time){
			var url = HttpAppService.URLS.ALARM_REASON_CODE_ALERT_HIS_CHANGE_COUNT
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{RESRCE_TYPE}#", resrceType)
					.replace("#{TIME}#", time)
				;
			return HttpAppService.get({
				url: url
			});
		}
		function alertExportExcel(resrceType){
			var url = HttpAppService.URLS.ALARM_EXPORT_EXCEL
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{RESRCE_TYPE}#", resrceType)
				;
			url = HttpAppService.handleCommenUrl(url);
			return url;
		}
		function conmmitSave(paramActions){
			var url = HttpAppService.URLS.ALERT_COMMIT_UPLOAD_FILE
					.replace("#{SITE}#", HttpAppService.getSite())
				;
			return HttpAppService.post({
				url: url,
				paras: paramActions
			});
		}
		function keepDelete(paramActions){
			var url = HttpAppService.URLS.ALERT_KEEP_FILE
					.replace("#{SITE}#", HttpAppService.getSite())
				;
			return HttpAppService.post({
				url: url,
				paras: paramActions
			});
		}
		
		function getAlertCommentList(resourceTypes,enabled){
			var url = HttpAppService.URLS.ALERT_COMMENT_LIST
			.replace("#{SITE}#", HttpAppService.getSite())
			.replace("#{RESOURCE_TYPE}#", resourceTypes)
			.replace('#{ENABLED}#',enabled);
			return HttpAppService.get({
				url: url
			});
		}
		function alertCommentIsUsed(commentCode){
			var url = HttpAppService.URLS.ALERT_COMMENT_USED
			.replace("#{SITE}#", HttpAppService.getSite())
			.replace("#{COMMENT_CODE}#", commentCode);
			return HttpAppService.get({
				url: url
			});
		}
		
		function changeAlertComment(paramActions){
			var url = HttpAppService.URLS.CHANGE_ALERT_COMMENT
					.replace("#{SITE}#", HttpAppService.getSite());
			return HttpAppService.post({
				url: url,
				paras: paramActions
			});
		}
		

}]);

alertModule
	.run([
		'$templateCache',
		function ($templateCache){ 

		// $templateCache.put('andon-ui-grid-tpls/dataCategory-site', 
		// 	"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.site\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		// ); 

			$templateCache.put('andon-ui-grid-rscg/alertGroup-checkbox',
				"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents flex-center\"  ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><input type=\"checkbox\" class=\"checkbox-column\" ng-disabled='true'  ng-checked =\"row.entity.enabled\" /></div>"
			);
			$templateCache.put('andon-ui-grid-rscg/alertGroup-checkbox-used',
				"<div class=\"ui-grid-cell-contents flex-center\" ><input type=\"checkbox\" class=\"checkbox-column\" ng-disabled='true' ng-true-value=\"'true'\" ng-false-value=\"'false'\" ng-model=\"row.entity.used\"/>"
			);

			$templateCache.put('andon-ui-grid-rscg/alertGroup-checkbox-current',
				"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents flex-center\"  ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><input type=\"checkbox\" class=\"checkbox-column\" ng-disabled='true'  ng-checked =\"row.entity.enabled\" /></div>"
			);
			$templateCache.put('andon-ui-grid-rscg/alerComment-checkbox-current',
					"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents flex-center\"  ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><input type=\"checkbox\" class=\"checkbox-column\"  ng-model=\"row.entity.enabled\"/></div>"
				);

			$templateCache.put('andon-ui-grid-tpls/addInfo-Resource-alert',
				"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.Resource\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
			);
			$templateCache.put('andon-ui-grid-tpls/addInfo-AlertCode-alert',
				"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.AlertCode\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
			);
			$templateCache.put('andon-ui-grid-tpls/addInfo-Description-alert',
				"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.Description\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
			);
			$templateCache.put('andon-ui-grid-tpls/addInfo-ReasonCode-alert',
				"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.ReasonCode\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
			);
}]);
