alertModule.controller('modalCurrentImportCtrl', [
    '$scope', '$http', '$q','UtilsService',
    'HttpAppService', '$timeout', '$interval','uiGridValidateService',
    'uiGridConstants', 'gridUtil','$uibModalInstance','Upload','alertService',
    function ($scope, $http, $q, UtilsService ,
              HttpAppService, $timeout, $interval,uiGridValidateService,
              uiGridConstants, gridUtil,$uibModalInstance,Upload,alertService){


        $scope.config = {
            gridApi: null,
            gridApiSaveError: null,
            isUploading: false,
            uploadError: false,
            uploadOk: false,

            selectedFile: null,

            parseFileOk: false,
            parseFileOkNum: 0,

            saveFileError: false,

            uploadProgressStr: '',
            progressPercentage: 0,
            startDate : null,
            saveFileBtn : true,
            commitData : null,
            datetimeBtn : false,
            errorTipStrDefault: '文件上传或解析失败,请检查文件格式并重新上传!',
            errorTipStr: ''
        };
        $scope.config.startDate = UtilsService.serverFommateDateTimeShow(new Date((new Date().getTime() + 60*60*1000)));

        // 关闭模态框 
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        //表格配置
        $scope.gridErrGroupMaintain = {
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,
            data: [],
            onRegisterApi: __onRegisterApi,
            columnDefs:[
                {
                    field: 'Resource', name: 'Resource', displayName: '设备类型', minWidth: 200,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellTemplate: 'andon-ui-grid-tpls/addInfo-Resource-alert',
                    cellClass:'grid-no-editable',
                    cellTooltip: __cellToolTip,
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },  
                {
                    field: 'AlertCode', name: 'AlertCode', displayName: '报警信息虚拟编码', minWidth: 200,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellClass:'grid-no-editable',
                    cellTemplate: 'andon-ui-grid-tpls/addInfo-AlertCode-alert',
                    cellTooltip: __cellToolTip,
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'Description', name: 'Description', displayName: '报警信息描述', minWidth: 200,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellClass:'grid-no-editable',
                    cellTemplate: 'andon-ui-grid-tpls/addInfo-Description-alert',
                    cellTooltip: __cellToolTip,
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },{
                    field: 'ReasonCode', name: 'ReasonCode', displayName: '原因代码', minWidth: 200,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellClass:'grid-no-editable',
                    cellTemplate: 'andon-ui-grid-tpls/addInfo-ReasonCode-alert',
                    cellTooltip: __cellToolTip,
                    cellEditableCondition: function($scope){
                        return false;
                    }
                }
            ]
        };
        //错误表格
        $scope.gridErrGroupMaintainSaveError = {
            data: [],
            onRegisterApi: __onRegisterApi,
            columnDefs: [
                {
                    field: 'resourceType', name: 'resourceType', displayName: '设备类型', minWidth: 200,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    // cellTemplate: 'andon-ui-grid-tpls/addInfo-Resource-alert',
                    cellClass:'grid-no-editable',
                    cellTooltip: __cellToolTip,
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },  
                {
                    field: 'alertSequenceId', name: 'alertSequenceId', displayName: '报警信息虚拟编码', minWidth: 200,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellClass:'grid-no-editable',
                    // cellTemplate: 'andon-ui-grid-tpls/addInfo-AlertCode-alert',
                    cellTooltip: __cellToolTip,
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'description', name: 'description', displayName: '报警信息描述', minWidth: 200,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellClass:'grid-no-editable',
                    // cellTemplate: 'andon-ui-grid-tpls/addInfo-Description-alert',
                    cellTooltip: __cellToolTip,
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },{
                    field: 'reasonCode', name: 'reasonCode', displayName: '原因代码', minWidth: 200,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellClass:'grid-no-editable',
                    // cellTemplate: 'andon-ui-grid-tpls/addInfo-ReasonCode-alert',
                    cellTooltip: __cellToolTip,
                    cellEditableCondition: function($scope){
                        return false;
                    }
                }
            ]
        };
        //.onRegisterApi = __onRegisterApiSaveError;

        function __cellToolTip(row, col){
            var str = '';// row.entity[col.colDef.name];
            var errors = row.entity[col.colDef.name+"Exceptions"];
            if(!angular.isUndefined(errors) && errors != null && errors.length > 0){
                for(var i = 0; i < errors.length; i++){
                    if(i > 0){
                        str += "\n";
                    }
                    str += errors[i];
                }
            }
            return str;
        }


        function __onRegisterApi(gridApi){
            $scope.config.gridApi = gridApi;
        };
        function __onRegisterApiSaveError(gridApi){
            $scope.config.gridApiSaveError = gridApi;  
        };

        $scope.$watch('config.selectedFile', function(newVal, oldVal){
            if(newVal==null){
                $scope.config.uploadProgressStr = '';
                $scope.config.progressPercentage = 0;
            }
        });

        // upload on file select or drop
        $scope.upload = function () {
            if($scope.config.startDate == null || $scope.config.startDate == ""){
                $scope.addAlert("","请选择生效日期");
            }
            //console.log(UtilsService.serverFommateDate($scope.config.startDate))
            if($scope.config.uploadOk){ //重新上传:初始化相关变量
                $scope.repareForUpload();
                return;
            }
            var file = $scope.config.selectedFile;
            $scope.config.isUploading = true;
            $scope.config.uploadOk = false;
            $scope.config.uploadError = false;
            $scope.config.progressPercentage = 0;
            var url = HttpAppService.URLS.ALERT_VERIFY_UPLOAD_FILE
                .replace('#{SITE}#', HttpAppService.getSite())
                .replace('#{startDate}#', UtilsService.serverFommateDateTime(new Date($scope.config.startDate)));
            // url = url + "&permactivity="+ROOTCONFIG.AndonConfig.CURRENTACTIVITY;
            // url = url + "&permsite="+HttpAppService.getSite();
            url = HttpAppService.handleCommenUrl(url);
            Upload.upload({
                // url: 'mhp-oee/web/rest/plant/site/1000/plc_category',
                url: url,
                data: {file: file}
            }).then(function (resp) {
                $scope.config.datetimeBtn = true;
                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                $scope.config.commitData = resp.data;
                $scope.config.saveFileBtn = false;
                $scope.config.isUploading = false;
                $scope.config.uploadOk = true;
                $scope.config.uploadError = false;
                $scope.config.uploadProgressStr = '';
                $scope.config.progressPercentage = 0;
                $scope.gridCurrentGroupMaintain.data = resp.data.resTypeCalculation;
                var errors = resp.data.exceptionRowArray;
                if(angular.isUndefined(errors) || errors == null || errors.length == 0){
                    // 文件没有解析错误信息
                    errors = [];
                    __handleErrors(errors);
                    $scope.config.parseFileOk = true;
                    $scope.config.parseFileOkNum = resp.data.rows.length;
                }else{
                    __handleErrors(errors);
                }   
            }, function (resp) {
                console.log('Error status: ' + resp.status);
                console.log(resp);
                $scope.config.isUploading = false;
                $scope.config.uploadOk = false;
                $scope.config.uploadError = true;
                $scope.config.uploadProgressStr = '';
                $scope.config.progressPercentage = 0;
                $scope.config.parseFileOk = false;


                var errStr = resp.headers("error-message");
                var errObj = JSON.parse(errStr);
                var code = errObj.code;
                var codeDesc = HttpAppService.getDescByExceptionCode(code);
                $scope.addAlert('', codeDesc);
                $scope.config.errorTipStr = $scope.config.errorTipStrDefault+"  : "+codeDesc;
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                $scope.config.uploadProgressStr = progressPercentage + "%";
                $scope.config.progressPercentage = progressPercentage;
            });
        };

        $scope.drap = function(){
            console.log(arguments);
            $scope.config.isUploading = false;
            $scope.config.uploadError = false;
            $scope.config.uploadOk = false;
            $scope.config.saveFileError = false;
        };
        $scope.select = function($file){
            $scope.config.selectedFile = $file;
            $scope.config.isUploading = false;
            $scope.config.uploadError = false;
            $scope.config.uploadOk = false;
            $scope.config.saveFileError = false;
        }
        $scope.repareForUpload = function(){
            $scope.config.datetimeBtn = false;
            $scope.config.selectedFile = null;
            $scope.config.isUploading = false;
            $scope.config.uploadError = false;
            $scope.config.uploadOk = false;
            $scope.config.parseFileOk = false;
            $scope.config.parseFileOkNum = 0;
            $scope.config.saveFileError = false;
            $scope.config.uploadProgressStr = '';
            $scope.config.progressPercentage = 0;
        };
        //错误信息处理
        function __handleErrors(errors){ 
            var resultDatas = [];
            for(var i = 0; i < errors.length; i++){
                var err = errors[i];
                var data = err.originDataMap;
                if(!angular.isUndefined(data) && data != null){
                    data.ReasonCode = data.reasonCodeBo;
                    data.Description = data.description;
                    data.AlertCode = data.alertSequenceId;
                    data.Resource = data.resourceTypeBo;
                }
                var list = err.exceptionList;
                var rowNum = err.rowNumber;
                for(var j = 0; j < list.length; j++){
                    var exe = list[j];
                    for(key in exe){
                        var keyName;
                        if(key == "resourceTypeBo"){
                            keyName = "ResourceExceptions"
                        }else if(key == "alertSequenceId"){
                            keyName = "AlertCodeExceptions"
                        }else if(key == "description"){
                            keyName = "DescriptionExceptions"
                        }else if(key == "reasonCodeBo"){
                            keyName = "ReasonCodeExceptions"
                        }else{
                            keyName = key+"Exceptions";
                        }
                        data[keyName] = exe[key];
                        // 转换 exception 为中文描述
                        for(var xx = 0; xx < data[keyName].length; xx++){
                            data[keyName][xx] = "文件第"+rowNum+"行: "+__corvetToChinaese(data[keyName][xx]);
                        }
                    }
                }
                resultDatas.push(data);
            }
            console.log(resultDatas);
            if(resultDatas.length > 0){
                $scope.gridErrGroupMaintain.data = resultDatas;
                $timeout(function(){
                    __setInValidCells();
                }, 500);
            }
        }
        function __corvetToChinaese(codeStr){
            if(UtilsService.isEmptyString(codeStr)){
                return codeStr;
            }
            var code = codeStr.substring(codeStr.indexOf("[")+1, codeStr.indexOf("]"));
            var codeDesc = HttpAppService.getDescByExceptionCode(code);
            return codeDesc;
        }
        // uiGridValidateService.setValid(rowEntity, colDef);
        var colDefNamesNeedValid = ['Resource','AlertCode','Description','ReasonCode'];
        function __setInValidCells(){
            var datas = $scope.gridErrGroupMaintain.data;
            for(var i = 0; i < datas.length; i++){
                var rowEntity = datas[i];
                for(var colName in colDefNamesNeedValid){
                    var colNameError = colDefNamesNeedValid[colName]+"Exceptions";
                    if(!angular.isUndefined(rowEntity[colNameError]) && rowEntity[colNameError] != null && rowEntity[colNameError].length > 0){
                        uiGridValidateService.setInvalid(rowEntity,$scope.config.gridApi.grid.getColDef(colDefNamesNeedValid[colName]));
                    }
                }
            }
        }
        //保存数据
        $scope.saveFile = function(){ 
            if($scope.config.uploadOk && !$scope.config.parseFileOk){
                $scope.addAlert("","数据解析失败,无法保存");
                return;
            }
            var item = $scope.config.commitData;
            console.log(item);
            alertService
                .conmmitSave(item)
                .then(function (resultDatas){
                    if(resultDatas.myHttpConfig.status == 200){
                        $uibModalInstance.dismiss('cancel');
                        $scope.addAlert("success","保存成功"+resultDatas.response.commitNumber+"条数据");
                    }else{ 
                        $uibModalInstance.dismiss('cancel');
                        $scope.addAlert("","保存失败");
                    }
                },function (resultDatas){ //TODO 检验失败
                    var data = resultDatas.config.data.rows;
                    for(var i=0;i<data.length;i++){
                        for(var j=0;j<data.length;j++){
                            if(data[i] == data[j] ){}
                        }
                    }   
                    if(resultDatas.myHttpConfig.status == 400){
                        var msg = resultDatas.headers('error-message');
                        if(!UtilsService.isEmptyString(msg)){
                            var msgObj = JSON.parse(msg);
                            var errorJson = msgObj.errorJson;
                            if(!UtilsService.isEmptyString(errorJson)){
                                var errorJsonObj = JSON.parse(errorJson);
                                errorJsonObj.description = UtilsService.covertUTF8ToZhcn(errorJsonObj.descriptionUnicode);
                                errorJsonObj.resourceType = errorJsonObj.resourceTypeBo.substring(errorJsonObj.resourceTypeBo.lastIndexOf(",")+1);
                                errorJsonObj.reasonCode = errorJsonObj.reasonCodeBo.substring(errorJsonObj.reasonCodeBo.lastIndexOf(",")+1);
                                $scope.gridErrGroupMaintainSaveError.data = [errorJsonObj];
                                $scope.config.saveFileError = true;
                            }   
                        }       
                    }else{      
                        // $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    }    
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }
        $scope.gridCurrentGroupMaintain = {
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,
            data: [],
            onRegisterApi: __onRegisterApi,
            columnDefs:[

                {
                    field: 'resourceType', name: 'resourceType', displayName: '设备类型', minWidth: 200,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'databaseNumber', name: 'databaseNumber', displayName: '数据库条数', minWidth: 200,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'excelNumber', name: 'excelNumber', displayName: '导入数据条数', minWidth: 200,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },]
        };
    }]);

