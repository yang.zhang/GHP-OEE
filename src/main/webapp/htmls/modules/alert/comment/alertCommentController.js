alertModule.controller('alertCommentController',[
	'$scope', '$http', '$q', 'UtilsService', '$state',
    'HttpAppService', '$timeout', 'plcService','alertService',
    'uiGridConstants','$uibModal', 'uiGridValidateService','uiGridCellNavConstants',
    function ($scope, $http, $q, UtilsService, $state,
    		HttpAppService, $timeout, plcService,alertService,
            uiGridConstants,$uibModal, uiGridValidateService,uiGridCellNavConstants) {

        $scope.config = {
        	gridApi: null,
        	addRowsNum: 1,
			deletedRows: [],  // 缓存删除且未提交的表格行数据

			btnDisabledDelete: true,  // 是否禁用<删除>按钮
			btnDisabledSave: true,    // 是否禁用<保存>按钮

			canEdit: false,			  // 当前用户是否对该activity有编辑权限
			showResourceTypeIcon:true,
			
			disabledSearchBtn:true,    //禁用查询按钮
			disabledAddBtn:true,       //禁用新增按钮
			showRowNum:false,
			checkStatus:true,           //默认启用勾选框状态
			localData:[]
        };	
        
        //设备类型帮助
        $scope.ResourceTypeList = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/deviceTypeSelectMoreModal.html',
                controller: 'resourceTypeController',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectTypeMoreDevice: function () {
                        return {type : 'list'};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.resourceType = selectedItem.resourceType;
                $scope.config.resourceTypeDesc = selectedItem.resourceTypeDesc;
                
                if($scope.config.resourceType.length>1){
                	$scope.config.addRowsNum=$scope.config.resourceType.length;
                	$scope.config.showRowNum=false;
                }else{
                	$scope.config.addRowsNum=1;
                	$scope.config.showRowNum=true;
                }
                
                
                $scope.config.disabledSearchBtn=false;
                $scope.config.disabledAddBtn=false;	
                $scope.config.showResourceTypeIcon=false;
            }, function () {
            });
        };
        
        $scope.search = function(){
        	var resourceTypes=$scope.config.resourceType;
        	var enabled=$scope.config.checkStatus;
        	__requestTableDatas(resourceTypes,enabled); 
        }; 
        
        $scope.deleteAll=function (){
        	$scope.config.resourceType='';
        	 $scope.config.resourceTypeDesc='';
        	 $scope.config.showResourceTypeIcon=true;
        	 
        	 $scope.config.disabledSearchBtn=true;
             $scope.config.disabledAddBtn=true;	
        	 
        }
        
        $scope.config.canEdit = $scope.modulesRWFlag("#"+$state.$current.url.source);

        $scope.alertCommentGridOptions = {
        	enablePagination: false,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,
            paginationCurrentPage: 1,
            totalItems: 0,
            useExternalPagination: true,
		    showGridFooter: false, 
		    modifierKeysToMultiSelectCells: false, 
		    columnDefs: [
		    	{ 
		    		field: 'handle', name: 'handle', visible: false
		    	},
/*		    	{
		    		field: 'commentCode', 
		    		name: 'commentCode', 
		    		displayName: '报警备注代码', 
		    		minWidth: 150, 
		    		enableCellEdit: true, 
		    		enableCellEditOnFocus: false, 
		    		validators: { required: true },
		    		cellTooltip: function(row, col){  return row.entity[col.colDef.name];  }, 
		    		//cellTemplate: 'andon-ui-grid-tpls/accessType-typeCode', 
		    		cellClass:'grid-no-editable ad-uppercase',
		    		cellEditableCondition: function($scope){
		    			var row = arguments[0].row;
		    			var col = arguments[0].col;
		    			// 非新增不可编辑 、新增行可编辑 
		    			if(!angular.isUndefined(row.entity) && row.entity!=null && !angular.isUndefined(row.entity.handle) && row.entity.handle!=null && row.entity.handle!=""){
		    				return false;
		    			}else{
		    				return true;
		    			}
		    		}
		    	},*/ 
		    	{
		    		field: 'description', 
		    		name: 'description', 
		    		displayName: '报警备注描述', 
		    		minWidth: 200,
		    		enableCellEdit: $scope.config.canEdit, 
		    		enableCellEditOnFocus: false, 
		    		validators: {  required: true },
		    		cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
		    		cellTemplate: 'andon-ui-grid-tpls/dataCategory-categoryDesc' 
		    	},
		    	{
		    		field: 'resourceTypeDesc', 
		    		name: 'resourceTypeDesc', 
		    		displayName: '设备类型', 
		    		minWidth: 200,
		    		enableCellEdit: false,
		    		enableCellEditOnFocus: false, 
		    		validators: {  required: true },
		    		cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
		    	},
		    	{
 		    	 	field: 'edit', name: 'edit', displayName: '编辑设备类型', width: 100, 
 		    		enableCellEdit: false, enableCellEditOnFocus: false, 
 		    		visible: $scope.config.canEdit,
 		    		enableColumnMenu: false, enableColumnMenus: false,
 		    		cellTooltip: function(row, col){ return "编辑设备类型"; }, 
 		    		cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col"><span class="glyphicon glyphicon-pencil"></span></div>',
 		    		headerTemplate: '<div class="xbr-delete-col-header">编辑设备类型</div>'
 		    	},
		    	{
                    field:"enabled",
                    type:'boolean',
                    name:"enabled",displayName:'启用',width: 50,
                    cellTooltip: function(row, col){ return "启用"; },
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTemplate:'andon-ui-grid-rscg/alerComment-checkbox-current'
                },
		    ],
		    data: [],
		    onRegisterApi: __onRegisterApi
        };

        /**
         * 
         * [addRows 点击新增按钮时触发]
         * 
         */
        $scope.addRows = function(){
			if(angular.isUndefined($scope.alertCommentGridOptions.data) || $scope.alertCommentGridOptions.data == null){
				$scope.alertCommentGridOptions.data = [];
			}
			for(var i = 0; i < $scope.config.addRowsNum; i++){
				$scope.bodyConfig.overallWatchChanged = true;
				var resourceType="";
				var resourceTypeDesc="";
				if($scope.config.resourceType.length>1){
					resourceType=$scope.config.resourceType[i];
					resourceTypeDesc=$scope.config.resourceTypeDesc[i];
				}else{
					resourceType=$scope.config.resourceType[0];
					resourceTypeDesc=$scope.config.resourceTypeDesc[0];
				}
				$scope.alertCommentGridOptions.data.unshift({
					site: HttpAppService.getSite(),
					hasChanged: true,
					resourceType:resourceType,
					resourceTypeDesc:resourceTypeDesc,
					enabled:true,
				});
			}
			$scope.config.btnDisabledSave = false;
		};

		/**
         * 
         * [checkAndDeleteSelectedRows 点击删除按钮时触发]
         * 
         */
		$scope.checkAndDeleteSelectedRows = function(){
			var rows = $scope.config.gridApi.selection.getSelectedRows();
			for(var i = 0; i < rows.length; i++){
				var row = rows[i];
				if( !angular.isUndefined(row.handle) && row.handle!=null && row.handle!=""){
					alertService.alertCommentIsUsed(row.commentCode)
						.then(function (resultDatas){ 
							var endIndex = resultDatas.config.url.indexOf('&');
							var commentCode = resultDatas.config.url.substring(resultDatas.config.url.lastIndexOf("comment_code=")+13, endIndex);
							if(resultDatas.response === true){ // 不允许删除
								for(var i = 0; i < $scope.alertCommentGridOptions.data.length; i++){
									if($scope.alertCommentGridOptions.data[i].commentCode == commentCode){
										$scope.alertCommentGridOptions.data[i].canDeleted = false;
										break;
									}
								}
								$scope.addAlert('danger', commentCode+" 已被引用,不允许删除!");
							}else{
								for(var i = 0; i < $scope.alertCommentGridOptions.data.length; i++){
									if($scope.alertCommentGridOptions.data[i].commentCode == commentCode){
										$scope.alertCommentGridOptions.data[i].canDeleted = true;
										break;
									}
								}
								__chekForDeleteRows();
							}
						}, function (errorResultDatas){
							$scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
						});
				}else{
					row.canDeleted = true;
					row.isSelected = true;
				}
			}
			__chekForDeleteRows();
		};
		function __chekForDeleteRows(){
			var canDeletes = true;
			var deletedRowIndexs = [];
			var rows = $scope.alertCommentGridOptions.data;
			for(var i = 0; i < rows.length; i++){
				var row = rows[i];
				if(row.isSelected){
					if(row.canDeleted !== true){
						canDeletes = false;
					}
				}
			}
			if(canDeletes){
				for (var i = rows.length - 1; i >= 0; i--) {
					var row = rows[i];
					if(row.isSelected){
						$scope.bodyConfig.overallWatchChanged = true;
						row.isDeleted = true;
				        deletedRowIndexs.push(i);
				        $scope.config.deletedRows.push(angular.copy(row));
					}
				};
			}else{
				return;
			}
			var length = deletedRowIndexs.length;
			for(var i = 0; i < deletedRowIndexs.length; i++){
				var index = deletedRowIndexs[i];
				$scope.alertCommentGridOptions.data.splice(index,1);
				// $scope.alertCommentGridOptions.data = UtilsService.removeArrayByIndex($scope.alertCommentGridOptions.data, deletedRowIndexs.pop());
			}
		};

		/**
         * 
         * [saveAllAcitons 点击保存按钮时触发]
         * 
         */
		$scope.saveAllAcitons = function(){
			if(!__checkCanSaveDatas()){ return; };
			var paramActions = []; 
			for(var i = 0; i < $scope.alertCommentGridOptions.data.length; i++){
				var row = $scope.alertCommentGridOptions.data[i]; 
				for(var x=0;x<$scope.config.localData.length;x++){
					if(i==x){
						if($scope.alertCommentGridOptions.data[i].enabled!=$scope.config.localData[x].enabled){
							row.hasChanged=true;
						}
					}
				}
				if(row.hasChanged){
					if(angular.isUndefined(row.handle)){ //创建新数据 ，若是删除中存在该条数据，则直接忽略该行,并在后面将删除操作也忽略
						if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
							var enabled="false";
							if(row.enabled==true){
								enabled="true";
							}
							paramActions.push({ 
								"handle": row.handle, 
								"site": row.site, 
								"commentCode": row.commentCode, 
								"resourceType":row.resourceType,
								"resourceTypeDesc":row.resourceTypeDesc,
								"description": row.description,
								"enabled":enabled,
								"viewActionCode": 'C'  
							});  
						}else{}
					}else{ //修改已存于服务器的行数据 ，若删除列表中有该行，则直接忽略该修改提交，并在后面将删除操作添加进来
						if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
							var enabled="false";
							if(row.enabled==true){
								enabled="true";
							}
							paramActions.push({
								"handle": row.handle,
								"site": row.site, 
								"commentCode": row.commentCode, 
								"resourceType":row.resourceType,
								"resourceTypeDesc":row.resourceTypeDesc,
								"description": row.description,
								"enabled":enabled,
								"viewActionCode": 'U',
								"modifiedDateTime": row.modifiedDateTime
							});
						}else{}
					}
				}
			}	
			for(var i = 0; i < $scope.config.deletedRows.length; i++){
				var deleteTempRow = $scope.config.deletedRows[i];
				if(!angular.isUndefined(deleteTempRow.handle)){// 服务器端数据的删除操作,直接添加进去即可
					paramActions.push({
						"handle": deleteTempRow.handle,
						"site": deleteTempRow.site,
						"commentCode": deleteTempRow.commentCode, 
						"resourceType":deleteTempRow.resourceType,
						"resourceTypeDesc":deleteTempRow.resourceTypeDesc,
						"description": deleteTempRow.description,
						"enabled":deleteTempRow.enabled,
						"viewActionCode": 'D',
						"modifiedDateTime": deleteTempRow.modifiedDateTime
					});
				}else{ }
			}	
			if(paramActions.length > 0){
				$scope.showBodyModel("正在保存");
				alertService .changeAlertComment(paramActions)
					.then(function (resultDatas){ 
						$scope.hideBodyModel();
						$scope.bodyConfig.overallWatchChanged = false;
						$scope.config.deletedRows = [];
						$scope.addAlert('success', '保存成功!');
						var resourceTypes=$scope.config.resourceType;
			        	var enabled=$scope.config.checkStatus;
			        	__requestTableDatas(resourceTypes,enabled); 
					},function (resultDatas){ 
						$scope.hideBodyModel();
						$scope.bodyConfig.overallWatchChanged = false;
						$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
					});
			}else{ 
				$scope.addAlert('没有修改,无需保存!');
			}	
		}; 	
		/**
         * 
         * [__checkCanSaveDatas 保存前的数据检验]
         * 
         */	
		function __checkCanSaveDatas(){
			for(var i = 0; i < $scope.alertCommentGridOptions.data.length; i++){
				var item = $scope.alertCommentGridOptions.data[i];
				if(item.hasChanged){
					var rules = [
						{ field: 'site',  emptyDesc: '第'+(i+1)+'行: 工厂不能为空!' },
						/*{ field: 'commentCode', emptyDesc: '第'+(i+1)+'行: 报警备注代码不能为空!' },*/
						{ field: 'description', emptyDesc: '第'+(i+1)+'行: 报警备注描述不能为空!' }
					];
					for(var ruleIndex in rules){
						var rule = rules[ruleIndex];
						if(angular.isUndefined(item[rule.field]) || item[rule.field] == null || item[rule.field] == ""){
							$scope.addAlert('', rule.emptyDesc);
							return false;
						}
					}
					/*if(item.plcAccessTypeCountIsValid === false){
						$scope.addAlert('', '第'+(i+1)+'行: 协议类型代码必须是6位!');
						return false;
					}*/
					if(item.plcAccessTypeZhcnIsValid === false){
						$scope.addAlert('', '第'+(i+1)+'行: 报警备注代码不能为汉字!');
						return false;
					}
					if(!angular.isUndefined(item.plcAccessTypeIsValid) && item.plcAccessTypeIsValid!=null && item.plcAccessTypeIsValid == false){
						$scope.addAlert('', '第'+(i+1)+'行: 报警备注代码已经存在!');
						return false;
					}
				}
			}
			return true;
		}	
		function __deletedRowsExitThisRowByHashKey(rowHashKey){
			for(var n = 0; n < $scope.config.deletedRows; n++){
				if(rowHashKey == $scope.config.deletedRows[n].$$hashKey){
					return true;
				}
			}
			return false;
		}	

		
        function __onRegisterApi(gridApi){
			$scope.config.gridApi = gridApi;

			gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
				/*if(newRowCol.col.field == "edit" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
					editResourceType(newRowCol);
				}*/
			});

			$scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
					var promise = $q.defer(); 
					if($scope.config.gridApi.rowEdit){
						$scope.config.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
					}

					if(oldValue == newValue || colDef.field == 'isSelected'){
						promise.resolve();
						return;
					}
					rowEntity.hasChanged = true;      		 
					if(colDef.field === "plcAccessType" || colDef.name === "plcAccessType"){ 
						if(UtilsService.isEmptyString(rowEntity.plcAccessType)){
							return;
						}	
						rowEntity.plcAccessType = rowEntity.plcAccessType.toUpperCase();
						if(UtilsService.isContainZhcn(rowEntity.plcAccessType)){
							promise.reject();
							$scope.addAlert('', '协议类型代码不能为汉字!');
							uiGridValidateService.setInvalid(rowEntity, colDef);
							rowEntity.plcAccessTypeZhcnIsValid = false;
							return;
						}	
						if(rowEntity.plcAccessType.length != 6){
							promise.reject();
							$scope.addAlert('', '协议类型代码必须是6位!');
							uiGridValidateService.setInvalid(rowEntity, colDef);
							rowEntity.plcAccessTypeCountIsValid = false;
							return;
						}   
						rowEntity.plcAccessTypeZhcnIsValid = true;
                    	rowEntity.plcAccessTypeCountIsValid = true;
						plcService 
				  			.plcProtocolTypeExit(newValue) 
				  			.then(function (resultDatas){ 
								if(resultDatas.response){ //已经存在:true
									$scope.addAlert('danger', '该协议类型已经存在!');
									uiGridValidateService.setInvalid(rowEntity, colDef);
									rowEntity.plcAccessTypeIsValid = false;
									promise.reject();
								}else{	//不存在冲突，可以使用
									promise.resolve();
									rowEntity.hasChanged = true;
									rowEntity.plcAccessTypeIsValid = true;
									uiGridValidateService.setValid(rowEntity, colDef);
								}
							},function (resultDatas){ 
								$scope.addAlert(resultDatas.myHttpConfig.statusDesc);
							}); 
					}else{
						promise.resolve();
					}
			}); 

			$scope.config.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
				$scope.config.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
			});

			//选择相关
			$scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
				gridRow.entity.isSelected = gridRow.isSelected;
				var count = $scope.config.gridApi.selection.getSelectedCount();
				$scope.config.btnDisabledDelete = count > 0 ? false : true;
			});
			$scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
				var count = $scope.config.gridApi.selection.getSelectedCount();
				$scope.config.btnDisabledDelete = count > 0 ? false : true;
				for(var i = 0; i < gridRows.length; i++){
					gridRows[i].entity.isSelected = gridRows[i].isSelected;
				}
			});
		}

        function editResourceType(newRowCol){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/deviceTypeSelectMoreModal.html',
                controller: 'resourceTypeController',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectTypeMoreDevice: function () {
                        return {type : 'edit'};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
            		var hashKey = newRowCol.row.entity.$$hashKey;
            		for(var i=0;i<$scope.alertCommentGridOptions.data.length;i++){
                		if(hashKey==$scope.alertCommentGridOptions.data[i].$$hashKey){
                			if($scope.alertCommentGridOptions.data[i].resourceType!=selectedItem.resourceType[0]){
                				newRowCol.row.entity.hasChanged=true;
                			}
                			$scope.alertCommentGridOptions.data[i].resourceType=selectedItem.resourceType[0];
                			$scope.alertCommentGridOptions.data[i].resourceTypeDesc=selectedItem.resourceTypeDesc[0];
                			break;
                		}
                	}
            		
            }, function () {
            });
        };
        
		function __requestTableDatas(resourceTypes,enabled){
			alertService.getAlertCommentList(resourceTypes,enabled).then(function (resultDatas){
					$scope.bodyConfig.overallWatchChanged = false;
					$scope.config.btnDisabledSave = false;
					if(resultDatas.response && resultDatas.response.length > 0){
						for(var i = 0; i < resultDatas.response.length; i++){
							resultDatas.response[i].isSelected = false;
							if(resultDatas.response[i].enabled == "true")
							{
								resultDatas.response[i].enabled=true;
							}
							else{
								resultDatas.response[i].enabled=false;
							}
						}
						$scope.alertCommentGridOptions.data = resultDatas.response;
						$scope.config.localData=angular.copy(resultDatas.response);
						return;
					}else{
						$scope.alertCommentGridOptions.data = [];
						$scope.addAlert('', '暂无数据!');
					}
				}, function (resultDatas){
					$scope.config.btnDisabledSave = true;
					$scope.alertCommentGridOptions.data = [];
					$scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
				}); 
		} 

		/**
		 * 
		 * [__deleteRowDataByHashKey 依据hashkey来删除对应的表格行数据]
		 * @param  {[string]} hashKey [hashKey]
		 * 
		 */
		function __deleteRowDataByHashKey(hashKey){
			for(var i = 0; i < $scope.alertCommentGridOptions.data.length; i++){
				if(hashKey == $scope.alertCommentGridOptions.data[i].$$hashKey){
					$scope.bodyConfig.overallWatchChanged = true;
					var obj = angular.copy($scope.alertCommentGridOptions.data[i]);
					$scope.config.deletedRows.push(obj);
					$scope.alertCommentGridOptions.data = UtilsService.removeArrayByIndex($scope.alertCommentGridOptions.data, i);
					break; 
				}
			}
		}

		function __checkCanDeleteData(newRowCol){
			var entity = newRowCol.row.entity;
			var handle = entity.handle;
			var hashKey = entity.$$hashKey;
			if(angular.isUndefined(handle) || handle==null || handle==""){ // 处理新增行 
				__deleteRowDataByHashKey(hashKey);
				return;
			}
			// 处理历史行，需要检验是否已经被引用
			plcService
				.plcProtocolTypeIsUsed(entity.plcAccessType)
				.then(function (resultDatas){
					if(resultDatas.response){
						entity.canDeleted = false;
						$scope.addAlert('', '该协议类型已被引用,不可删除!');
					}else{
						entity.canDeleted = true;
						__deleteRowDataByHashKey(hashKey);	
					}					
				}, function (errorResultDatas){
					$scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
				});
		}

		$scope.$on(uiGridCellNavConstants.CELL_NAV_EVENT, function (evt, rowCol, modifierDown) {
			if(rowCol.col.field == "edit" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
				editResourceType(rowCol);
			}
		});

    }]);