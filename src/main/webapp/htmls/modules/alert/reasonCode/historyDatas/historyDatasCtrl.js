alertModule.controller('historyDatasCtrl', [
    '$scope', '$http',	'HttpAppService', '$timeout',
    'uiGridConstants','$uibModal','alertService','UtilsService','$state','$rootScope',
    function ($scope, $http, HttpAppService, $timeout,
              uiGridConstants,$uibModal,alertService,UtilsService,$state,$rootScope) {
        $scope.config = {
            changeHistory : true,
            statusHistory : false,
            queryBtn : true,
            deleteBtn:true,
            bigDate : UtilsService.serverFommateDateShow(new Date()),
            date : UtilsService.serverFommateDateShow(new Date()),
            searchInfo : null,
            count : null,
            isNoValue : false,
            deletedRows:[],
            editRW : false,
        };
        //权限控制
        $scope.config.editRW = $rootScope.historyEditRW;
        //表格配置
        $scope.gridHistoryDatas = {
            enablePagination: true,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,

            useExternalPagination: true,
            paginationCurrentPage: 1,
            totalItems: 0,

            showGridFooter: false,
            modifierKeysToMultiSelectCells: false,
            data: [],
            //onRegisterApi: __onRegisterApi,
            columnDefs: [
                {
                    field: 'handle', name: 'handle', visible: false, minWidth: 0, maxWidth: 0
                },
                {
                    field: 'resourceTypePO.resourceType',name: 'resourceTypePO.resourceType',
                    displayName: '设备类型',minWidth: 95,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'resourceTypePO.description', name: 'resourceTypePO.description',
                    displayName: '设备类型描述', minWidth: 115,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){  return row.entity[col.colDef.name];  },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'alertSequenceId', name: 'alertSequenceId',
                    displayName: '序号', minWidth: 150,
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: {  required: true },
                    cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                },
                //{
                //    field:"resourceTypePO.enabled",name:"resourceTypePO.enabled",displayName:'启用',width: 50,
                //    cellTooltip: function(row, col){ return "启用"; },
                //    enableCellEdit: false, enableCellEditOnFocus: false,
                //    enableColumnMenu: false, enableColumnMenus: false,
                //    cellTemplate:'andon-ui-grid-rscg/alertGroup-checkbox'
                //}
                {
                    field: 'reasonCodePO.reasonCode', name: 'reasonCodePO.reasonCode',
                    displayName: '原因代码', minWidth: 85,
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: {  required: true },
                    cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                },
                {
                    field: 'reasonCodePO.description', name: 'reasonCodePO.description',
                    displayName: '原因代码描述', minWidth: 150,
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: {  required: true },
                    cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                },
                {
                    field: 'description', name: 'description', displayName: '报警', minWidth: 200,
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: {  required: true },
                    cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                },
                {
                    field: 'startDateTimeIn', name: 'startDateTimeIn',
                    displayName: '生效开始日期', minWidth: 175,
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: {  required: true },
                    cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                },
                {
                    field: 'endDateTimeIn', name: 'endDateTimeIn',
                    displayName: '失效结束日期', minWidth: 175,
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: {  required: true },
                    cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                },
                {
                    // field:"resourceTypePO.enabled",name:"resourceTypePO.enabled",displayName:'启用',width: 50,
                    field:"used",name:"used",displayName:'是否已使用',width: 90,
                    cellTooltip: function(row, col){ return "是否已启用"; },
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTemplate:'andon-ui-grid-rscg/alertGroup-checkbox-used'
                },{
                    field: 'isDeleted', name: 'isDeleted', displayName: '删除', width: 60,
                    enableCellEdit: false, enableCellEditOnFocus: false,visible: $scope.config.editRW,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return "删除该行"; },
                    cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col"><span class="ion-trash-a"></span></div>',
                    headerTemplate: '<div class="xbr-delete-col-header">删除</div>'
                }

            ],
            onRegisterApi: __onRegisterApi
        };

        function __onRegisterApi(gridApi){ 
            $scope.config.gridApi = gridApi;
            // 分页相关
            $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                 console.log("刷新界面 第 "+currentPage+" 页,每页 "+pageSize+"个元素!");
                __requestTableDatas(currentPage, pageSize);
                return true;
            }, $scope);

            // 选择相关
            $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                gridRow.entity.isSelected = gridRow.isSelected;
                var count = $scope.config.gridApi.selection.getSelectedCount();
                $scope.config.deleteBtn = count > 0 ? false : true;
            });
            $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                for(var i = 0; i < gridRows.length; i++){
                    gridRows[i].entity.isSelected = gridRows[i].isSelected;
                }
                var count = $scope.config.gridApi.selection.getSelectedCount();
                $scope.config.deleteBtn = count > 0 ? false : true;
            });
            //删除
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
                if(newRowCol.col.field == "isDeleted" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                    __checkCanDeleteData(newRowCol);
                }
            });
        };
        function __deleteRowDataByHashKey(hashKey){
            for(var i = 0; i < $scope.gridHistoryDatas.data.length; i++){
                if(hashKey == $scope.gridHistoryDatas.data[i].$$hashKey){
                    $scope.config.deletedRows.push($scope.gridHistoryDatas.data[i]);
                    $scope.gridHistoryDatas.data = UtilsService.removeArrayByIndex($scope.gridHistoryDatas.data, i);
                    break;
                }
            }
        };
        //删除相关
        function __checkCanDeleteData(newRowCol){
            var entity = newRowCol.row.entity;
            var handle = entity.handle;
            var hashKey = entity.$$hashKey;
            if(angular.isUndefined(handle) || handle==null || handle==""){ // 处理新增行
                __deleteRowDataByHashKey(hashKey);
                return;
            }
            // 处理历史行，需要检验是否已经被引用
            if(entity.used == true){ // 不允许删除
                entity.canDeleted = false;
                $scope.addAlert('danger', "要删除的列已被引用,不允许删除!");
            }else{
                entity.canDeleted = true;
                __deleteRowDataByHashKey(hashKey);
            }
        };
        $scope.searchDeviceType = function(){
            if($scope.config.searchInfo == null || $scope.config.searchInfo == ''){
                $scope.config.searchInfo == '';
            }
            __searchDeviceType();
        };
        //设备类型
        function __searchDeviceType(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/alert/modal/ModalCurrentSearchResult.html',
                controller: 'modalCurrentSearchResultCtrl',
                size: 'lg',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope : $scope,
                resolve:{
                    alertitems:function(){
                        return $scope.config.searchInfo;
                    }
                },

            });
            modalInstance.result.then(function (searchResults) {
                $scope.config.searchInfo = searchResults.join(",");
                $scope.searchChange();
                //if(searchResults != null || searchResults != undefined){
                //    $scope.config.queryBtn  = false;
                //}
                //console.log($scope.config.searchInfo);
                //console.log(searchResults);
            });
        };
        //输入框变化
        $scope.searchChange = function(){
            if($scope.config.changeHistory == true){
                if($scope.config.searchInfo == null || $scope.config.searchInfo == ""
                || $scope.config.bigDate == null || $scope.config.bigDate == ""){
                    $scope.config.queryBtn = true;
                }else{
                    $scope.config.queryBtn = false;
                }
            }else{
                if($scope.config.searchInfo == null || $scope.config.searchInfo == ""
                    || $scope.config.date == null || $scope.config.date == ""){
                    $scope.config.queryBtn = true;
                }else{
                    $scope.config.queryBtn = false;
                }
            }
        };
        //单选框改变
        $scope.changeHistory = function(){
            $scope.config.changeHistory = true;
            $scope.config.statusHistory = false;
            $scope.searchChange();
        };

        $scope.statusHistory = function(){
            $scope.config.changeHistory = false;
            $scope.config.statusHistory = true;
            $scope.searchChange();
        };
        //查询数据
        $scope.queryDatas = function(){
            $scope.config.deletedRows = [];
            $scope.gridHistoryDatas.data = [];
            var resrceType = $scope.config.searchInfo;
            var index = 1;
            $scope.gridHistoryDatas.paginationCurrentPage = 1;
            var count = $scope.gridHistoryDatas.paginationPageSize;
            if($scope.config.changeHistory == true){
                if($scope.config.bigDate == null){
                    $scope.addAlert("","请选择生效日期大于");
                    return;
                }
                var time = UtilsService.serverFommateDate(new Date($scope.config.bigDate));
                console.log(time);
                __selectedChangeHistoryList(resrceType, time,index,count);
                __selectedChangeHistoryListCount(resrceType, time);
            }else{
                if($scope.config.date == null){
                    $scope.addAlert("","请选择日期");
                    return;
                }
                var time = UtilsService.serverFommateDate(new Date($scope.config.date));
                __selectedstatusHistoryList(resrceType, time,index,count);
                __selectedstatusHistoryListCount(resrceType, time);
            }
        };

        $scope.deleteHistoryDatas = function(){
            __deleteHistoryDatas();
        };
        //查询接口
        function __selectedChangeHistoryList(resrceType, time,index,count){
            alertService
                .alertReasonCodeAlertHisChange(resrceType, time,index,count)
                .then(function (resultDatas){
                    console.log(resultDatas);
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.gridHistoryDatas.data = resultDatas.response;
                        for(var i=0;i<$scope.gridHistoryDatas.data.length;i++){
                            $scope.gridHistoryDatas.data[i].endDateTimeIn = UtilsService.serverStringFormatDateTime($scope.gridHistoryDatas.data[i].endDateTimeIn);
                            $scope.gridHistoryDatas.data[i].startDateTimeIn = UtilsService.serverStringFormatDateTime($scope.gridHistoryDatas.data[i].startDateTimeIn);
                        }
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        };

        function __selectedstatusHistoryList(resrceType, time,index,count){
            alertService
                .alertReasonCodeAlertHisStatus(resrceType, time,index,count)
                .then(function (resultDatas){
                    console.log(resultDatas);
                    $scope.gridHistoryDatas.paginationCurrentPage = index;
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.gridHistoryDatas.data = resultDatas.response;
                        for(var i=0;i<$scope.gridHistoryDatas.data.length;i++){
                            $scope.gridHistoryDatas.data[i].endDateTimeIn = UtilsService.serverStringFormatDateTime($scope.gridHistoryDatas.data[i].endDateTimeIn);
                            $scope.gridHistoryDatas.data[i].startDateTimeIn = UtilsService.serverStringFormatDateTime($scope.gridHistoryDatas.data[i].startDateTimeIn);
                        }
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.gridHistoryDatas.paginationCurrentPage = index;
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        };

        function __selectedChangeHistoryListCount(resrceType, time){
            alertService
                .alertReasonCodeAlertHisChangeCount(resrceType, time)
                .then(function (resultDatas){
                    console.log(resultDatas);
                    if(resultDatas.myHttpConfig.status == 200){
                        $scope.gridHistoryDatas.totalItems = resultDatas.response;
                        console.log($scope.gridHistoryDatas.totalItems);
                        if($scope.gridHistoryDatas.totalItems == 0){
                            $scope.addAlert("","暂无数据!");
                        }
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        };

        function __selectedstatusHistoryListCount(resrceType, time){
            alertService
                .alertReasonCodeAlertHisStatusCount(resrceType, time)
                .then(function (resultDatas){
                    console.log(resultDatas);
                    if(resultDatas.myHttpConfig.status == 200){
                        $scope.gridHistoryDatas.totalItems = resultDatas.response;
                        console.log($scope.gridHistoryDatas.totalItems);
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        };
        //查询数据
        function __requestTableDatas(pageIndex, pageCount){
            var pageIndexX = 1;
            var pageCountX = $scope.gridHistoryDatas.paginationPageSize;
            if(angular.isUndefined(pageIndex) || pageIndex == null || pageIndex<=1){
                pageIndexX = 1;
            }else{
                pageIndexX = pageIndex;
            }
            if(angular.isUndefined(pageCount) || pageCount == null || pageCount<1){
                pageCountX = $scope.gridHistoryDatas.paginationPageSize;
            }else{
                pageCountX = pageCount;
            }
            var resrceType = $scope.config.searchInfo;
            if($scope.config.changeHistory == true){
                if($scope.config.bigDate == null && $scope.config.bigDate == ""){
                    $scope.addAlert("","请选择生效日期大于");
                    return;
                }
                var time = UtilsService.serverFommateDate(new Date($scope.config.bigDate));
                __selectedChangeHistoryList(resrceType, time,pageIndex,pageCount);
                __selectedChangeHistoryListCount(resrceType, time);
            }else{
                if($scope.config.date == null || $scope.config.date == ""){
                    $scope.addAlert("","请选择日期");
                    return;
                }
                var time = UtilsService.serverFommateDate(new Date($scope.config.date));
                __selectedstatusHistoryList(resrceType, time,pageIndex,pageCount);
                __selectedstatusHistoryListCount(resrceType, time);
            }
        };
        //删除
        function __deleteHistoryDatas(){
            var count = $scope.config.gridApi.selection.getSelectedCount();
            var selectedRow = $scope.config.gridApi.selection.getSelectedRows();
            console.log(count);
            console.log(selectedRow);
            if(count == 0){
                $scope.addAlert('info','请先选择要删除的列');
            }else{
                for(var i = 0;i<selectedRow.length;i++){
                    if(selectedRow[i].used == "true"){
                        $scope.addAlert('danger','要删除的列已被引用，无法删除');
                    }else{
                        __deleteHistoryByHashKey(selectedRow[i].$$hashKey)
                    };
                };
            };
        };

        function __deleteHistoryByHashKey(hashKey){
            for(var i = 0; i < $scope.gridHistoryDatas.data.length; i++){
                if(hashKey == $scope.gridHistoryDatas.data[i].$$hashKey){
                    $scope.bodyConfig.overallWatchChanged = true;
                    $scope.config.deletedRows.push($scope.gridHistoryDatas.data[i]);
                    $scope.gridHistoryDatas.data = UtilsService.removeArrayByIndex($scope.gridHistoryDatas.data, i);
                    break;
                }
            }
        };
        //大写控制
        $scope.upModel = function(){
            $scope.config.searchInfo = angular.uppercase($scope.config.searchInfo);
        };
        $scope.keepDelete = function(){
            var item = [];
            for(var i=0;i<$scope.config.deletedRows.length;i++){
                item.push(
                    {
                        "handle": $scope.config.deletedRows[i].handle,
                        "viewActionCode" : 'D'
                    }
                );
            }
            alertService
                .keepDelete(item)
                .then(function (resultDatas){
                    console.log(resultDatas);
                    if(resultDatas.myHttpConfig.status == 200){
                        $scope.addAlert("success","保存成功");
                        $scope.queryDatas();
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });        }
    }]);