alertModule.controller('currentEffectCtrl', [
    '$scope', '$http',	'HttpAppService', '$timeout',
    'uiGridConstants','$uibModal','alertService','UtilsService','$state','$rootScope',
    function ($scope, $http, HttpAppService, $timeout,
              uiGridConstants,$uibModal,alertService,UtilsService,$state,$rootScope) {

        $scope.config={
            searchInfo : null,
            pullBtn : true,
            pushBtn : true,
            queryBtn : true,
            isNoValue : false,
            editRW : false,
            deletedRows : [],
            deleteBtn : true
        };
        $scope.config.editRW = $scope.modulesRWFlag("#"+$state.$current.url.source);
        $rootScope.historyEditRW = $scope.config.editRW;
        //表格参数
        $scope.gridCurrentEffect = {
            enablePagination: true,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,

            useExternalPagination: true,
            paginationCurrentPage: 1,
            totalItems: 0,

            showGridFooter: false,
            modifierKeysToMultiSelectCells: false,
            data: [],
            onRegisterApi: __onRegisterApi,
            columnDefs: [
                {
                    field: 'handle', name: 'handle', visible: false, minWidth: 0, maxWidth: 0
                },
                {
                    field: 'resourceTypePO.resourceType',name: 'resourceTypePO.resourceType',
                    displayName: '设备类型',minWidth: 95,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    validators: { required: true }, /*cellClass:'grid-no-editable',*/
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'resourceTypePO.description', name: 'resourceTypePO.description',
                    displayName: '设备类型描述', minWidth: 115,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){  return row.entity[col.colDef.name];  },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'alertSequenceId', name: 'alertSequenceId',
                    displayName: '报警信息虚拟编码', minWidth: 150,
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                },
                {
                    field: 'reasonCodePO.reasonCode', name: 'reasonCodePO.reasonCode',
                    displayName: '原因代码', minWidth: 85,
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                },
                {
                    field: 'reasonCodePO.description', name: 'reasonCodePO.description',
                    displayName: '原因代码描述', minWidth: 150,
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                },
                {
                    field: 'description', name: 'description', displayName: '报警', minWidth: 200,
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                },
                {
                    field: 'startDateTimeIn', name: 'startDateTimeIn', displayName: '生效日期', minWidth: 175,
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                },
                {
                    // field:"resourceTypePO.enabled",name:"resourceTypePO.enabled",displayName:'启用',width: 50,
                    field:"used",name:"used",displayName:'是否已使用',width: 90,
                    cellTooltip: function(row, col){ return "是否已启用"; },
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTemplate:'andon-ui-grid-rscg/alertGroup-checkbox-used'
                }
            ],
        };
        function __onRegisterApi(gridApi){
            $scope.config.gridApi = gridApi;
            // 分页相关
            $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                console.log("刷新界面 第 "+currentPage+" 页,每页 "+pageSize+"个元素!");
                __requestTableDatas(currentPage, pageSize);
                return true;
            }, $scope);

            //选择相关
            $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                gridRow.entity.isSelected = gridRow.isSelected;
                __selectAny();
            });
            $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                for(var i = 0; i < gridRows.length; i++){
                    gridRows[i].entity.isSelected = gridRows[i].isSelected;
                }
                __selectAny();
            });
        }
        function __selectAny(){
            for(var i=0;i<$scope.gridCurrentEffect.data.length;i++){
                if($scope.gridCurrentEffect.data[i].isSelected == true){
                    $scope.config.deleteBtn = false;
                    return;
                }
            }
            $scope.config.deleteBtn = true;
        }

        //删除
        $scope.checkAndDeleteSelectedRows = function(){
            console.log($scope.gridCurrentEffect.data);
            //$scope.bodyConfig.overallWatchChanged = true;
            var deleteArrY = [];
            var deleteArrN = [];
            for(var i=0;i<$scope.gridCurrentEffect.data.length;i++){
                if($scope.gridCurrentEffect.data[i].used == "true" && $scope.gridCurrentEffect.data[i].isSelected){
                    deleteArrN.push($scope.gridCurrentEffect.data[i]);
                    $scope.gridCurrentEffect.data[i].canDeleted = false;
                }else if($scope.gridCurrentEffect.data[i].used == "false" && $scope.gridCurrentEffect.data[i].isSelected){
                    deleteArrY.push($scope.gridCurrentEffect.data[i]);
                    $scope.gridCurrentEffect.data[i].canDeleted = true;
                }
            }
            console.log(deleteArrN);
            console.log(deleteArrY);

            if(deleteArrN.length > 1){
                //for(var i=0;i<deleteArrY.length;i++){
                $scope.addAlert("","当前信息已使用,不允许删除");
                return;
                //}
            }
            __chekForDeleteRows();
        };

        function __chekForDeleteRows(){
            var canDeletes = true;
            var deletedRowIndexs = [];
            var rows = $scope.gridCurrentEffect.data;
            for(var i = 0; i < rows.length; i++){
                var row = rows[i];
                if(row.isSelected){
                    if(row.canDeleted !== true){
                        canDeletes = false;
                    }
                }
            }
            if(canDeletes){
                for (var i = rows.length - 1; i >= 0; i--) {
                    var row = rows[i];
                    if(row.isSelected){
                        row.isDeleted = true;
                        deletedRowIndexs.push(i);
                        $scope.config.deletedRows.push(angular.copy(row));
                    }
                };
            }else{
                return;
            }
            var length = deletedRowIndexs.length;
            for(var i = 0; i < length; i++){
                $scope.gridCurrentEffect.data.splice(deletedRowIndexs[i],1);
                // $scope.virtualGridOptions.data = UtilsService.removeArrayByIndex($scope.virtualGridOptions.data, deletedRowIndexs[0]);
            }
            //$scope.config.btnDisabledDelete = true;
        };
        $scope.keepDelete = function(){
            if($scope.config.deletedRows.length < 1){
                $scope.addAlert("","暂无数据需要保存");
                return;
            }
        }
        //查询设备类型
        $scope.searchDeviceType = function(){
            if($scope.config.searchInfo == null || $scope.config.searchInfo == ''){
                $scope.config.searchInfo == '';
            }
            __searchDeviceType();
        };
        //导入
        $scope.importCurrentEffect = function(){
            __importCurrentEffect();
        };



        function __searchDeviceType(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/alert/modal/ModalCurrentSearchResult.html',
                controller: 'modalCurrentSearchResultCtrl',
                size: 'lg',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope : $scope,
                resolve:{
                    alertitems:function(){
                        return $scope.config.searchInfo;
                    }
                },

            });
            modalInstance.result.then(function (searchResults) {
                $scope.config.searchInfo = searchResults.join(",");
                $scope.config.queryBtn  = false;
                console.log(searchResults);
            });
        };

        function __importCurrentEffect(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/alert/import/ModalCurrentImport.html',
                controller: 'modalCurrentImportCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                //resolve:{
                //    items:function(){
                //        return $scope.basicWorkBodyConfig.executionTypeList;
                //    }
                //},

            });
            //modalInstance.result.then(function (searchResults) {
            //    $scope.basicWorkBodyConfig.searchBasicWork = searchResults.activity;
            //    $scope.basicWorkBodyConfig.copyWorkInfoData=[];
            //    $scope.queryBasicWork();
            //});
        };

        $scope.searchChange = function(){
            if($scope.config.searchInfo == null || $scope.config.searchInfo == ""){
                $scope.config.queryBtn = true;
            }else{
                $scope.config.queryBtn = false;
            }
        }
        //查询数据
        $scope.queryData = function(){
            $scope.gridCurrentEffect.data = [];
            $scope.gridCurrentEffect.paginationCurrentPage = 1;
            __selectedDeviceTypeForData($scope.config.searchInfo,1,$scope.gridCurrentEffect.paginationPageSize );
            __selectedDeviceTypeForDataCount($scope.config.searchInfo);
        }
        //导出
        $scope.pullFile = function(){
            $scope.config.pushBtn = false;
            __pullDeviceTypeForData($scope.config.searchInfo);
        }
        function __selectedDeviceTypeForData(alertitems,index,count){
            alertService
                .alertReasonCodeAlertCurrent(alertitems,index,count)
                .then(function (resultDatas){
                    $scope.gridCurrentEffect.paginationCurrentPage = index;
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.gridCurrentEffect.data = resultDatas.response;
                        console.log($scope.gridCurrentEffect.data );
                        for(var i=0;i<$scope.gridCurrentEffect.data.length;i++){
                            // $scope.gridCurrentEffect.data[i].used = 'true';
                            $scope.gridCurrentEffect.data[i].startDateTimeIn = UtilsService.serverStringFormatDateTime($scope.gridCurrentEffect.data[i].startDateTimeIn);
                        }
                        $scope.config.pullBtn = false;
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.gridCurrentEffect.paginationCurrentPage = index;
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }
        function __selectedDeviceTypeForDataCount(alertitems){
            alertService
                .alertReasonCodeAlertCurrentCount(alertitems)
                .then(function (resultDatas){
                    console.log(resultDatas);
                    if(resultDatas.myHttpConfig.status == 200){
                        $scope.gridCurrentEffect.totalItems = resultDatas.response;
                        if($scope.gridCurrentEffect.totalItems == 0){
                            $scope.addAlert("","暂无数据!");
                        }
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }
        function __pullDeviceTypeForData(resrceType){
            window.open(alertService
                .alertExportExcel(resrceType))
            console.log(alertService
                .alertExportExcel(resrceType));
        }
        function __requestTableDatas(pageIndex, pageCount){
            var pageIndexX = 1;
            var pageCountX = $scope.gridCurrentEffect.paginationPageSize;
            if(angular.isUndefined(pageIndex) || pageIndex == null || pageIndex<=1){
                pageIndexX = 1;
            }else{
                pageIndexX = pageIndex;
            }
            if(angular.isUndefined(pageCount) || pageCount == null || pageCount<1){
                pageCountX = $scope.gridCurrentEffect.paginationPageSize;
            }else{
                pageCountX = pageCount;
            }
            console.log($scope.config.searchInfo+pageIndexX+pageCountX)
            __selectedDeviceTypeForData($scope.config.searchInfo,pageIndexX,pageCountX );
            __selectedDeviceTypeForDataCount($scope.config.searchInfo);
        }
        $scope.upModel = function(){
            $scope.config.searchInfo = angular.uppercase($scope.config.searchInfo);
        }
    }]);