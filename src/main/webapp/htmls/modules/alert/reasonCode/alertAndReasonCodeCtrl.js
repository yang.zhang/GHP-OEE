alertModule.controller('alertAndReasonCodeCtrl', [
	'$scope', '$http',	'HttpAppService', '$timeout',
	'uiGridConstants',
	function ($scope, $http, HttpAppService, $timeout,
			  uiGridConstants) {

		$scope.config = {
			alertReasonCodeMenuList:[{
				title:'当前生效',
				distState: '#/andon/AlertAndReasonCode/CurrentEffect',
				distStateName:'andon.AlertAndReasonCode.CurrentEffect',
				isActive:true
			},{
				title:'历史数据',
				distState: '#/andon/AlertAndReasonCode/HistoryDatas',
				distStateName:'andon.AlertAndReasonCode.HistoryDatas',
				isActive:false
			}]
		};

		$scope.$on("$stateChangeSuccess", function (event2, toState, toParams, fromState, fromParam){
			for(var i = 0 ; i < $scope.config.alertReasonCodeMenuList.length ; i++){
				var item = $scope.config.alertReasonCodeMenuList;
				if(fromState && toState.name == item[i].distStateName){
					item[i].isActive = true
				}else{
					item[i].isActive = false;
				}
			}
		});


	}]);