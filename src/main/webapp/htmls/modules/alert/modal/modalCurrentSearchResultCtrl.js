alertModule.controller('modalCurrentSearchResultCtrl', [
    '$scope', '$http', '$q','UtilsService',
    'HttpAppService', '$timeout', '$interval','uiGridValidateService',
    'uiGridConstants', 'gridUtil','$uibModalInstance','alertitems','alertService',
    function ($scope, $http, $q, UtilsService ,
              HttpAppService, $timeout, $interval,uiGridValidateService,
              uiGridConstants, gridUtil,$uibModalInstance,alertitems,alertService){

        $scope.config = {
            isFirstSelect:true
        };
        $scope.gridModalCurrentEffect = {

            showGridFooter: false,
            modifierKeysToMultiSelectCells: false,
            data: [],
            onRegisterApi: __onRegisterApi,
            columnDefs: [
                //{
                //    field: 'handle', name: 'handle', visible: false, minWidth: 0, maxWidth: 0
                //},
                {
                    field: 'resourceType',name: 'type', displayName: '设备类型',
                    enableCellEdit: true, enableCellEditOnFocus: false,
                    validators: { required: true }, /*cellClass:'grid-no-editable',*/
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'description', name: 'description', displayName: '设备类型描述', minWidth: 150,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    enableCellEdit: true, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){  return row.entity[col.colDef.name];  },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                //{
                //    field: 'number', name: 'number', displayName: '序号', minWidth: 200,
                //    enableCellEdit: true, enableCellEditOnFocus: false, validators: {  required: true },
                //    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                //    cellEditableCondition: function($scope){
                //        return false;
                //    }
                //},
                {
                    field:"enabled",name:"enabled",displayName:'启用',width: 50,
                    cellTooltip: function(row, col){ return "启用"; },
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTemplate:'andon-ui-grid-rscg/alertGroup-checkbox'
                }

            ],data : [],
            onRegisterApi : __onRegisterApi
        };
        //for(var i=0;i<$scope.gridModalCurrentEffect.data.length;i++) {
        //    $scope.gridModalCurrentEffect.data[i].isSelected == false;
        //}
        function __onRegisterApi(gridApi){
            $scope.config.gridApi = gridApi;
            //选择任一行或一列时触发
            //$scope.config.gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
            //    $uibModalInstance.close(newRowCol.row.entity);
            //});
            //选择相关
            $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                //$scope.config.gridApi.selection.setModifierKeysToMultiSelect(true);
                gridRow.entity.isSelected = gridRow.isSelected;
                //console.log(gridRow.entity);
                //$uibModalInstance.close(gridRow.entity);
            });
            $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                for(var i = 0; i < gridRows.length; i++){
                    //$scope.config.gridApi.selection.unSelectRow(gridRows[i].entity,event);
                    gridRows[i].entity.isSelected = gridRows[i].isSelected;

                }
            });
        };

        //关闭模态框
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function __selectedDeviceTypeList(alertitems){
            alertService
                .alertResrceTypeAll(alertitems)
                .then(function (resultDatas){
                    //console.log(resultDatas);
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.gridModalCurrentEffect.data = resultDatas.response;
                        if($scope.config.isFirstSelect)
                        {
                            $timeout(function(){
                                $scope.config.isFirstSelect=false;
                                /* To hide the blank gap when use selecting and grouping */
                                $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                                $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                            },0);
                        }
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }
        __selectedDeviceTypeList(alertitems);
        //确定操作
        $scope.confirm = function(){
            var arr = [];
            for(var i=0;i<$scope.gridModalCurrentEffect.data.length;i++){
                if($scope.gridModalCurrentEffect.data[i].isSelected == true){
                    arr.push($scope.gridModalCurrentEffect.data[i].resourceType);
                }
            }
            $uibModalInstance.close(arr);
        }
    }]);

