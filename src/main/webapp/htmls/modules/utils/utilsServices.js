utilsModule 
	.factory('UtilsService', [
		'$filter', '$uibModal', '$q','$timeout',
		function ($filter, $uibModal, $q,$timeout) {

		return {
			setHeight:__setHeight,//实时监控左边菜单栏的高度
			removeArrayByIndex : __removeArrayByIndex, 		
			// 传入日期对象eg:new Date()、返回服务器规定的格式：精确到天 		
			// UtilsService.serverFommateDate(dateObj) 		
			serverFommateDate: __serverFommateDate,
			serverFommateYear:__serverFommateYear,
			serverFommateDateShow: __serverFommateDateShow,
			// 传入日期对象eg:new Date()、返回服务器规定的格式：精确到秒		 
			// UtilsService.serverFommateDateTime(dateObj) 		
			serverFommateDateTime: __serverFommateDateTime,
			serverFommateDateTimeShow: __serverFommateDateTimeShow,
			serverFommateDateTimeFrontShow:__serverFommateDateTimeFrontShow,
			serverFormateDateMouth : __serverFormateDateMouth,
			//"MM/dd/yyyy-hh/mm/ss   TO   Date yyyy/mm/dd-hh/mm/ss"
			serverStringFormatDateTime:__serverStringFormatDateTime,
			//"yyyy/mm/dd"
			serverFormateDateReport:__serverFormateDateReport,
			//"yyyy/mm/dd"
			serverFormateDateTimeReport:__serverFormateDateTimeReport,
			normalFormatDateTimeReport:normalFormatDateTimeReport,
			//设备状态明细报表
			OEEStartFommateDateTimeShow:__oeeStartFommateDateTimeShow,
			OEEEndFommateDateTimeShow:__oeeEndFommateDateTimeShow,

			
			resourceSDStartFommateDateTimeShow:__resourceSDStartFommateDateTimeShow,
			resourceSDEndFommateDateTimeShow:__resourceSDEndFommateDateTimeShow,
			//将单词转换为大写
			toUpperCaseWord: __toUpperCaseWord,		

			isEmptyString: __isEmptyString,
			isEmptyArray: __isEmptyArray,

			// utf-8转换为中文
			covertUTF8ToZhcn: __covertUTF8ToZhcn,
			isContainZhcn: __isContainZhcn,
			addMonth:__addMonth,
			getMonthDate : __getLastDate,
			log:log,
			dateFormatNormal:dateFormatNormal,
			

			//////// UI 提示相关 
			/**
			 * 用法:   			
			 * 		var promise = UtilsService.confirm(msg, title, btnLables);   eg: UtilsService.confirm('', '提示', ['确定', '取消']);
			 * 		promise 	
			 * 			.then(function(){ // ok 
			 * 					
			 * 			}, function(){   // cancel 
			 * 					
			 * 			});		
			 * 					
			 */
			confirm: __confirm,
			alert: __alert,
			alertAutoHidden: alertAutoHidden,
			isNull:__isNull,
			removeDul:__removeDul,
			toDecimal2:toDecimal2
		};
			/**
			 *
			 * [toDecimal 用于保留2位小数然后再拼%]
			 * @param  {string} x {需要格式化数据}
			 *                        {
 		 *                        	url: ''
 		 *                        }
			 * @return {string}    {格式化后数据}
			 *
			 */
			function toDecimal2(data) {
				//var f = parseFloat(x);
				//if (isNaN(f)) {
				//	return false;
				//}
				//var f = Math.round(x*100)/100;
				//var s = f.toString();
				//var rs = s.indexOf('.');
				//if (rs < 0) {
				//	rs = s.length;
				//	s += '.';
				//}
				//while (s.length <= rs + 2) {
				//	s += '0';
				//}
				//s=parseFloat(s);
				//s=s*100;
				//return s+"%";

				var strData = parseFloat(data)*100;
				strData = Math.round(strData);
				strData/=1.00;
				var ret = strData.toString()+"%";
				return ret;
			}

			function __removeDul(array){
				 var res = [];
				 var json = {};
				 for(var i = 0; i < array.length; i++){
				  if(!json[array[i]]){
				   res.push(array[i]);
				   json[array[i]] = 1;
				  }
				 }
				 return res;
			}
			function __isNull(value){
				if(value==null||value==''||value=='undefined'){
					return true;
				}else{
					return false;
				}
			}
			function __setHeight(){
				var window_height = $(window).height();//窗口高度
				var main_header = $('.main-header').height();//导航头部高度
				$timeout(function(){
					$(".myGrid,.myMiddleGrid,.myLittleGrid").css('min-height', 319);
					$(".myGrid").css('height',window_height-main_header-150);
					$(".myMiddleGrid").css('height',window_height-main_header-288);
					$(".myLittleGrid").css('height',window_height-main_header-320);
					$(".content-wrapper, .right-side").css('min-height', $('.main-sidebar').height());
				}, 600);
			};
			/**
			 *
			 * [__getLastDate 获取一个月的最后一天会开始一天]
			 * @param  {[date]} dateObject [日期]
			 * start end
			 * @return 日期
			 *
			 */
			function __addMonth(startDate,diffMont){
				var startY=startDate.getFullYear();
				var startM=startDate.getMonth();
				var startD=startDate.getDate();
				return new Date(startY,startM+diffMont,startD);
			}
			function __getLastDate(dateObject,item){
				var year = dateObject.getFullYear();
				var month = dateObject.getMonth()+1;
				var yearNow = new Date().getFullYear();
				var monthNow = new Date().getMonth()+1;
				if(item == "end"){
					if(month == monthNow && year == yearNow){
						return __serverFommateDate(new Date());
					}else{
						var date = new Date(year,month,0);
						var day = date.getDate();
						if(month < 10){
							return "0"+month+"/"+day + "/"+year;
						}else{
							return month+"/"+day + "/"+year;
						}
					}
				}else{
					if(month < 10){
						return "0"+month+"/"+"01" + "/"+year;
					}else{
						return month+"/"+"01" + "/"+year;
					}
				}
			}
		/**
		 * 
		 * [__isContainZhcn 判断一个字符串是否包含中文]
		 * @param  {[string]} str [要进行判定的字符串]
		 * @return {[boolean]}  [是否包含中文]
		 * 
		 */
		function __isContainZhcn(str){
			if(__isEmptyString(str)){
				return false;
			}
			return /[\u4e00-\u9fa5]+/.test(str);
		}


		function __serverStringFormatDateTime(dateString){
			if(dateString){
				var date = dateString.split('-');
				date[1] = date[1].replace('/',':').replace('/',':');
				date = date.join(' ');
				date = __serverFommateDateTimeFrontShow(new Date(date));
				return date;
			};
		};

		/**
		 * 
		 * [__covertUTF8ToZhcn 将UTF-8代码转换为UTF-8字符串]
		 * 		eg: \u6211 --> 我
		 * @param  {[string]} str [待转换的UTF-8代码]
		 * @return {[string]}     [转换后的UTF-8字符串]
		 * 
		 */
		function __covertUTF8ToZhcn(str){
			if(__isEmptyString(str)){
				return "";
			}
			var resultStr = window.unescape(str.replace(/\u/g, "%u"));
			return resultStr.replace(/\\/g, "");
			//return window.unescape(str);
		}
		// function __covertUTF8ToZhcn(str){
		// 	var resultStr = window.unescape(str.replace(/\u/g, "%u"));
		// 	return resultStr.replace(/\\/g, "");
		// }

		function alertAutoHidden(msg, title, dismiss){ // 显示时间:eg: 300，以ms为单位
			alert(msg);
		}

		/**
		 * 
		 * [__isEmptyString 判断字符串是否为空字符串]
		 * @param  {[string]} str  [要判定的字符串]
		 * @return {[boolean]}     [是否为undefined、null、'']
		 * 
		 */
		function __isEmptyString(str){
			return angular.isUndefined(str) || str == null || str == '' ? 
					true 
					: (str.trim && str.trim() == '');
		};
		/**
		 * 
		 * [__isEmptyArray 判断字符串是否为空数组、undefined、null]
		 * @param  {[string]} str  [要判定的数组]
		 * @return {[boolean]}     [是否为undefined、null、空数组]
		 * 
		 */
		function __isEmptyArray(array){
			return angular.isUndefined(array) || array == null || array.length == 0;
		}
		/**
		 * 
		 * [__confirm confirm提示]
		 * 		!! 建议使用 BodyCtrl 中的 $scope.confirm
		 * 		具体用法参考 BodyCtrl 中的 $scope.confirm
		 * @param  {[string]} msg       [confirm提示内容]
		 * @param  {[string]} title     [confirm提示标题]
		 * @param  {[array]} btnLables  [按钮文字数组:默认为['确定','取消']]
		 * @param  {[object]} scope     [scope对象,需要使用其进行参数传递]
		 * @return {[promise]}          [confirm的promise, 点击左侧按钮为resolve, 右侧按钮为reject]
		 * 
		 */
		function __confirm(msg, title, btnLables,scope){
			var okLabel = '确定';
			var cancelLabel = '取消';
			if(!angular.isUndefined(btnLables) && btnLables!=null && btnLables.length > 0 ){
				okLabel = btnLables[0];
				cancelLabel = btnLables[1];
			}
			var titleX = angular.isUndefined(title) || title == null || title == '' ? '提示' : title;
			var promise = $q.defer();
			var confirmModalInstance = $uibModal.open({
	            animation: true,
	            templateUrl: 'modules/public/model/confirm_model.html',
	            controller: ['$scope', '$uibModalInstance', 'confirmInfo',function($scope, $uibModalInstance, confirmInfo){
	            	$scope.config = {
	            		title: confirmInfo.title, 
	            		bodyContent: confirmInfo.body
	            	};
	            	$scope.cancel = function(){ 
	            		$uibModalInstance.dismiss('cancel'); 
	            	}; 
	            	$scope.ok = function(){ 
	            		$uibModalInstance.close({ 
	            			comfirmOk: true 
	            		}); 
	            	}; 
	            }], 
	            size: 'sm',
	            backdrop:'static',
	            backdropClass: 'ad-public-model-confirm-bg',
	            openedClass:'flex-center-parent ad-public-model-confirm-open',
	            scope : scope,
	            resolve: { 
	               confirmInfo: function () {
	                   return {
	                        title: titleX,
	                        body: msg
	                   };
	               }
	            }
	        });
	        confirmModalInstance.result.then(function (okDatas) {
	           if(okDatas.comfirmOk){
	           		promise.resolve();
	           }else{
	           		promise.reject();
	           }
	        }, function () {  
	            promise.reject();  
	        });  
	        return promise.promise;
		}
		/**
		 * 
		 * [__alert alert提示]
		 * @param  {[string]} msg        [alert提示内容]
		 * @param  {[string]} title      [alert提示标题]
		 * @param  {[array]} btnLables   []
		 * @param  {[object]} scope      [scope对象，需要使用其进行参数传递]
		 * @return {[promise]}           [alert的promise]
		 * 
		 */
		function __alert(msg, title, btnLables,scope){
			var okLabel = '确定';
			var cancelLabel = '取消';
			if(!angular.isUndefined(btnLables) && btnLables!=null && btnLables.length > 0 ){
				okLabel = btnLables[0];
				cancelLabel = btnLables[1];
			}
			var titleX = angular.isUndefined(title) || title == null || title == '' ? '提示' : title;
			var promise = $q.defer();
			var confirmModalInstance = $uibModal.open({
	            animation: true,
	            templateUrl: 'modules/public/model/alert_model.html',
	            controller: ['$scope', '$uibModalInstance', 'confirmInfo',function($scope, $uibModalInstance, confirmInfo){
	            	$scope.config = {
	            		title: confirmInfo.title, 
	            		bodyContent: confirmInfo.body
	            	};
	            	$scope.cancel = function(){ 
	            		$uibModalInstance.dismiss('cancel'); 
	            	}; 
	            	$scope.ok = function(){ 
	            		$uibModalInstance.close({ 
	            			comfirmOk: true 
	            		}); 
	            	}; 
	            }], 
	            size: 'sm',
	            backdrop:'static',
	            backdropClass: 'ad-public-model-confirm-bg',
	            openedClass:'flex-center-parent ad-public-model-confirm-open',
	            scope : scope,
	            resolve: { 
	               confirmInfo: function () {
	                   return {
	                        title: titleX,
	                        body: msg
	                   };
	               }
	            }
	        });
	        confirmModalInstance.result.then(function (okDatas) {
	           promise.resolve();
	        }, function () {  
	           // promise.resolve();
	        });  
	        return promise.promise;
		}
		/**
		 * 
		 * [__removeArrayByIndex 依据索引删除数组中某个元素]
		 * @param  {[Array]} array [需要删除的数组]
		 * @param  {[int]} index [需要删除的索引]
		 * @return {[Array]}       [删除后的结果]
		 * 
		 */
		function __removeArrayByIndex(array, index){
			return array.slice(0,index).concat(array.slice(index+1));
		};
		/**
		 * 
		 * [__serverFormateDateReport 报表模块的日期转化:将服务器传过来的日期转为展示格式]
		 * @param  {[date]} dateObject [要转换的日期对象]
		 * @return {[string]}            [返回展示所用的日期格式字符串]
		 * 
		 */
		function __serverFormateDateReport(dateObject){
			if(angular.isUndefined(dateObject) || dateObject==null){
				return null;
			}
			// return $filter('date')(dateObject, 'yyyy-MM-dd');
			return $filter('date')(dateObject, 'yyyy/MM/dd');
		};
		/**
		 * 
		 * [__serverFormateDateTimeReport 针对报表模块接口:将日期转换为接口所需日期格式]
		 * @param  {[date]} dateObject   [要转换的日期对象]
		 * @return {[string]}            [接口所需日期格式]
		 * 
		 */
		function __serverFormateDateTimeReport(dateObject){
			if(angular.isUndefined(dateObject) || dateObject==null){
				return null;
			}
			// return $filter('date')(dateObject, 'yyyy-MM-dd');
			return $filter('date')(dateObject, 'yyyy/MM/dd HH:mm:ss');
		};
		/**
		 *
		 * [normalFormatDateTimeReport 针对报表模块接口:将时间戳转换为接口所需日期格式]
		 * @param  {[date]} dateObject   [要转换的时间字符串]
		 * @return {[string]}            [接口所需日期格式]
		 * @author xu.pan
		 */
		function normalFormatDateTimeReport(strDate){
			var date=new Date(strDate);
			if(!date){
				return null;
			}
			return $filter('date')(date.getTime(), 'yyyy-MM-dd HH:mm:ss');
		};
		/**
		 * 
		 * [__serverFommateDate 针对接口:将日期对象转换为接口所需日期格式字符串,eg:12/28/2016]
		 * @param  {[date]} dateObject [要转换的日期]
		 * @return {[string]}          [所需格式字符串]
		 * 
		 */
		function __serverFommateDate(dateObject){
			if(angular.isUndefined(dateObject) || dateObject==null){
				return null;
			}
			// return $filter('date')(dateObject, 'yyyy-MM-dd');
			return $filter('date')(dateObject, 'MM/dd/yyyy');
		}
		
		function __serverFommateYear(dateObject){
			if(angular.isUndefined(dateObject) || dateObject==null){
				return null;
			}
			return $filter('date')(dateObject, 'yyyy');
		}


			function __serverFormateDateMouth(dateObject){
				if(angular.isUndefined(dateObject) || dateObject==null){
					return null;
				}
				// return $filter('date')(dateObject, 'yyyy-MM-dd');
				return $filter('date')(dateObject, 'yyyy-MM');
			}
		/**
		 * 
		 * [__serverFommateDate 针对接口:将日期对象转换为接口所需日期格式字符串,eg:12/28/2016-23/58/58]
		 * @param  {[date]} dateObject [要转换的日期]
		 * @return {[string]}          [所需格式字符串]
		 * 
		 */
		function __serverFommateDateTime(dateObject){
			if(angular.isUndefined(dateObject) || dateObject==null){
				return null;
			}
			return $filter('date')(dateObject, 'MM/dd/yyyy-HH/mm/ss');
		}
		function __serverFommateDateShow(dateObject){
			if(angular.isUndefined(dateObject) || dateObject==null){
				return null;
			}
			// return $filter('date')(dateObject, 'yyyy-MM-dd');
			return $filter('date')(dateObject, 'yyyy-MM-dd');
		}

		function __serverFommateDateTimeShow(dateObject){
			if(angular.isUndefined(dateObject) || dateObject==null){
				return null;
			}
			return $filter('date')(dateObject, 'yyyy-MM-dd HH:mm:ss');
		}
		function __oeeStartFommateDateTimeShow(dateObject){
			if(angular.isUndefined(dateObject) || dateObject==null){
				return null;
			}
			return $filter('date')(dateObject, 'yyyy-MM-dd');
		}
		function __oeeEndFommateDateTimeShow(dateObject){
			if(angular.isUndefined(dateObject) || dateObject==null){
				return null;
			}
			return $filter('date')(dateObject, 'yyyy-MM-dd');
		}
		function __resourceSDStartFommateDateTimeShow(dateObject){
			if(angular.isUndefined(dateObject) || dateObject==null){
				return null;
			}
			return $filter('date')(dateObject, 'yyyy-MM-dd')+' 00:00';
		}
		function __resourceSDEndFommateDateTimeShow(dateObject){
			if(angular.isUndefined(dateObject) || dateObject==null){
				return null;
			}
			console.info(dateObject);
			return $filter('date')(dateObject, 'yyyy-MM-dd HH:mm');
		}
		function __serverFommateDateTimeFrontShow(dateObject){
			if(angular.isUndefined(dateObject) || dateObject==null){
				return null;
			}
			return $filter('date')(dateObject, 'yyyy-MM-dd HH:mm:ss');
		}
		/**
		 * 
		 * [__toUpperCaseWord 将字符串中的小写转换为大写]
		 * @param  {[string]} words [转换前的字符串]
		 * @return {[string]}       [转换为大写的字符串]
		 * 
		 */
		function __toUpperCaseWord(words){
			var contain = [];
			if(words){
				for(var i=0;i<words.length;i++){
					contain[i] = words.charAt(i).toUpperCase();
				}
				return contain.join('');
			}

		}

		/** add by panxu
		 * [log 当部署服务器上时不允许打印]
		 * @param param需要打印的信息
		 */
		function log(param) {
			if (ROOTCONFIG.AndonConfig.allowLog) {
					console.log(param);
			}
		}

		/** add by panxu
		 * [格式化时间]
		 * @param Tue Sep 20 2016 00:00:00 GMT+0800 (CST)
		 */
		function dateFormatNormal(param) {
			var date=new Date(param);
			var month=date.getMonth()+1;
			var day=date.getDate();
			var hour=date.getHours();
			var minute=date.getMinutes();
			if(month<10){
				month="0"+month;
			}
			if(day<10){
				day="0"+day;
			}
			if(hour<10){
				hour="0"+hour;
			}
			if(minute<10){
				minute="0"+minute;
			}

			return date.getFullYear()+"-"+month+"-"+day+" "+hour+":"+minute;
		}

	}]);

utilsModule 
 	.factory('HttpAppService', [ 
 		"$http", "$q", "UtilsService",
 		function ($http, $q, UtilsService){
			//baseUrl:http://10.17.170.135:51000/mhp-oee/web/rest/
 		var baseUrlPrefix = ROOTCONFIG.AndonConfig.basePath + ROOTCONFIG.AndonConfig.appPath;
 		var baseUrl = baseUrlPrefix + ROOTCONFIG.AndonConfig.requestPrefix;

 		/**
 		 * 
 		 * [responseStatus response-code对应的错误描述]
 		 * 		200: 	请求成功
 		 * 		401: 	session超时
 		 * 		403: 	session超时
 		 * 		413: 	当前登录用户权限被更改
 		 * 		400: 	后台所有主动报错的请求,通过在header中添加error-message来进行传递错误信息
 		 * @type {Array}
 		 * 
 		 */
 		var responseStatus = [
 			{ code: '500', desc: '服务器内部错误: 代码错误' },
 			{ code: '502', desc: '服务器内部错误: 网关设置错误' }
 		];
 		/**
 		 * 
 		 * [errorMessages error-code定义]
 		 * 		前后台的error-code定义集：
 		 * 			response code 为400时, response header中的error-message字段中的error-code
 		 * 			上传文件检验的exception结果中，使用error-code来传递信息，由前台代码来进行转换
 		 * @type {Object}
 		 * 
 		 */
 		var errorMessages = {
 			'10001': '数据项代码没有维护', 
 			'10028': '创建记录失败', 
 			'10002': '数据项描述没有维护', 
 			'10022': '删除记录失败', 
 			'10003': '数据库字段不存在', 
 			'10020': '记录已经存在', 
 			'10025': '已经存在重复的时间', 
 			'10004': '输入日期早于或等于已有日期', 
 			'10005': '无效的开始时间格式', 
 			'10006': '无效的输入参数', 
 			'10007': '无效的项目', 
 			'10008': '无效的数字格式', 
 			'10009': '输入日期早于或等于已有日期', 
 			'10010': '无效的原因代码',
 			'10011': '无效的设备',
 			'10012': '无效的设备状态',
 			'10024': '无效的时间',
 			'10029': '用户没有权限',
 			'10031': '该记录在其他表中被引用，不能被删除',
 			'10013': '无效的作业中心',
 			'10036': '记录enable状态为false',
 			'10014': '字段不能为空',
 			'10015': '存在重复的记录',
 			'10016': '没有指定的维护通知或命令',
 			'10035': '不存在该记录或关联的记录',
 			'10030': '记录不存在',
 			'10017': '数据记录错误',
 			'10018': '寄存器值没有维护',
 			'10037': '未知的规则',
 			'10033': '未知的操作码',
 			'10021': '更新记录错误',
 			'10019': '上传文件格式错误',
 			'10034': '该规则已使用,无法编辑或删除',
 			'10038': '无效的生效日期',
 			'10039': '输入日期早于或等于当前日期',
 			'10040': '用户输入的数据类型与数据项不存在依赖关系',
			'10041': '输入的model编码必须是6位或7位',
            '10042': 'PPM 报表公式参数未维护',
            '10043': 'PPM 报表公式参数维护不齐全，或维护错误',
			'10044': '生效日期必须大于当前时间',
			'10045': '数据项已存在PLC地址对应，不允许新增',
			'10049': '报警备注已经被引用，不允许修改',
			'10046': 'ip和设备不匹配',
        };
	 	// HttpAppService.URLS.PLC_CATEGORY_LIST
	 	// HttpAppService.URLS.PLC_CATEGORY_EXIT
 		return {
 			post: post, 
 			get: get, 
 			getSite: __getSite, 
 			handleCommenUrl: __handleCommenUrl,
 			/**
 			 * 
 			 * [URLS 定义项目中所有请求的请求路径]
 			 * @type {Object}
 			 * 
 			 */
 			URLS: {
 				// SITE: '1000', 
 				// mhp-oee
 				
 				LOGIN: baseUrl + "plant/site/#{SITE}#/j_security_check",
 				
 				//////////////////////////////////////////////////////////////////////// 
 				////// Excel上传文件模板下载地址 : 
 				//////////////////////////////////////////////////////////////////////// 
 				// ppm
 				DOWN_EXCEL_TEMPLATE_PPM: baseUrlPrefix + "download/std/cycletime/management/site/#{SITE}#/excel",
 				// user
 				DOWN_EXCEL_TEMPLATE_USER: baseUrlPrefix + "download/user/management/site/#{SITE}#/excel",
				// reason code
				DOWN_EXCEL_TEMPLATE_REASONCODE: baseUrlPrefix + "download/reasoncode/site/#{SITE}#/alert/excel",
				// plc address
				DOWN_EXCEL_TEMPLATE_PLCADDRESS: baseUrlPrefix + "download/plant/site/#{SITE}#/resource/excel",
				
 				//////////////////////////////////////////////////////////////////////// 
 				////// 下拉值 相关接口 : 
 				//////////////////////////////////////////////////////////////////////// 
 				//	生产区域接口get 
 				XLZ_WORK_CENTERS: baseUrl + "plant/site/#{SITE}#/work_center_condition", 
 				//	拉线接口get 
 				XLZ_LINES: baseUrl + "plant/site/#{SITE}#/line_condition?work_center=#{WORK_CENTER}#",
 				// XLZ_LINES: baseUrl + "plant/site//line_condition/#{WORK_CENTER}#", 
 				//	设备类型接口get 
 				XLZ_RESOURCE_TYPES: baseUrl + "plant/site/#{SITE}#/resource_type_condition",
				//	设备类型接口get new with choose工厂
				XLZ_RESOURCE_TYPES_NEW: baseUrl + "report/help/resource_type?sites=#{SITE}#",
				//	设备编码接口get
 				// XLZ_RESOURCE_CODES: baseUrl + "plant/site/#{SITE}#/resource_condition/#{WORK_CENTER}#/#{LINE}#/#{RESOURCE_TYPE}#", 
 				// XLZ_RESOURCE_CODES: baseUrl + "plant/site/resource_condition",
 				XLZ_RESOURCE_CODES: baseUrl + "plant/site/#{SITE}#/resource_condition?resource_type=#{RESOURCE_TYPE}#&line=#{LINE}#&work_center=#{WORK_CENTER}#", 
 				
 				XLZ_PLC_RESOURCE_CODES: baseUrl + "plant/site/#{SITE}#/plc_resource_condition?resource_type=#{RESOURCE_TYPE}#&line=#{LINE}#&work_center=#{WORK_CENTER}#", 
 				
 				
 				// PLC模块公共接口 
 				PLC_FILE_UPLOAD: baseUrl + "plant/site/#{SITE}#/resource/upload_file", 
 				// PLC数据类型 相关接口：是否存在、列表  
 				PLC_CATEGORY_EXIT: baseUrl + "plant/site/#{SITE}#/exist_plc_category/#{PLC_CATEGORY}#", 
 				PLC_CATEGORY_LIST: baseUrl + "plant/site/#{SITE}#/plc_category", 
 				PLC_CATEGORY_ISUSED: baseUrl + "plant/site/#{SITE}#/is_plc_category_used/#{PLC_CATEGORY}#", 
 				PLC_CATEGORIES_CHANGE: baseUrl + "plant/change_plc_categories",   // post请求 
 				// PLC数据项管理 相关接口:   
 				// PLC_DATAITEM_LIST: baseUrl + "plant/site/#{SITE}#/plc_object/#{PLC_CATEGORY}#", 
 				PLC_DATAITEM_LIST: baseUrl + "plant/site/#{SITE}#/plc_object?plc_category=#{PLC_CATEGORY}#&index=#{INDEX}#&count=#{COUNT}#", 
 				PLC_DATAITEM_LIST_ALL: baseUrl + "plant/site/#{SITE}#/plc_object_all?plc_category=#{PLC_CATEGORY}#",
 				PLC_DATAITEM_COUNT: baseUrl + "plant/site/#{SITE}#/plc_object_count?plc_category=#{PLC_CATEGORY}#", 
 				// PLC_DATAITEM_ISUSED: baseUrl + "plant/site/#{SITE}#/is_plc_object_used/#{PLC_OBJECT}#", 
 				PLC_DATAITEM_ISUSED: baseUrl + "plant/site/#{SITE}#/is_plc_object_used?plc_object=#{PLC_OBJECT}#", 
 				
 				// PLC_DATAITEM_EXIST: baseUrl + "plant/site/#{SITE}#/exist_plc_object/#{PLC_OBJECT}#", 
 				PLC_DATAITEM_EXIST: baseUrl + "plant/site/#{SITE}#/exist_plc_object_obj_controller?plc_object=#{PLC_OBJECT}#",
 				PLC_DATAITEM_CHANGE: baseUrl + "plant/change_plc_object", 
 				// PLC协议类型管理  #{PLC_PROTOCOL_TYPE} 
 				PLC_PROTOCOLTYPE_LIST: baseUrl + "plant/site/#{SITE}#/plc_access_type", 
 				PLC_PROTOCOLTYPE_EXIT: baseUrl + "plant/site/#{SITE}#/exist_plc_access_type?plc_access_type=#{PLC_ACCESS_TYPE}#", //"plant/site/#{SITE}#/exist_plc_access_type", 
 				PLC_PROTOCOLTYPE_ISUSED: baseUrl + "plant/site/#{SITE}#/is_plc_access_type?plc_access_type=#{PLC_ACCESS_TYPE}#", 
 				// PLC_PROTOCOLTYPE_CHANGE: baseUrl + "plant/change_plc_access_type", 
 				PLC_PROTOCOLTYPE_CHANGE: baseUrl + "plant/site/#{SITE}#/change_plc_access_type",
 				
 				////////////////////////////////////////////////////////////////////////
 				////// 上位机网络维护 相关接口 : 列表、查询数量、保存按钮 
 				////////////////////////////////////////////////////////////////////////
 				// PLC_NETINFO_LIST: baseUrl + "plant/site/plc_info", 
 				PLC_NETINFO_LIST: baseUrl + "plant/site/#{SITE}#/plc_info?index=#{PAGE_INDEX}#&count=#{PAGE_COUNT}#&resource=#{RESOURCE_S}#&resource_type=#{RESOURCE_TYPE}#&work_center=#{WORK_CENTER}#&line=#{LINE}#",
 				// PLC_NETINFO_COUNT: baseUrl + "plant/site/plc_info_count",
 				PLC_NETINFO_COUNT: baseUrl +"plant/site/#{SITE}#/plc_info_count?resource=#{RESOURCE_S}#&resource_type=#{RESOURCE_TYPE}#&work_center=#{WORK_CENTER}#&line=#{LINE}#",

 				PLC_NETINFO_CHANGE: baseUrl + "plant/change_plc_info",
 				PLC_NETINFO_ISUSED: baseUrl + "",
				PLC_NETINFO_EXIST_PCIP:baseUrl + "plant/site/#{SITE}#/exist_pc_ip?pc_ip=#{PC_IP}#",

 				////////////////////////////////////////////////////////////////////////
 				////// PLC地址信息维护 相关接口 : 初始化、查询、新增前验证、修改前验证、保存按钮	   
 				////////////////////////////////////////////////////////////////////////
 				// 初始化接口：GET		
 				// PLC_ADDRESS_INIT: baseUrl + "plant/site/#{SITE}#/plc_info_condition",
 				// 列表查询接口：POST		
 				// PLC_ADDRESS_LIST: baseUrl + "plant/site/resource_plc",
 				PLC_ADDRESS_LIST: baseUrl + "plant/site/#{SITE}#/resource_plc?plc_category=#{PLC_CATEGORY}#&resource=#{RESOURCE}#&index=#{PAGEINDEX}#&count=#{PAGECOUNT}#",
 				// PLC_ADDRESS_COUNT: baseUrl + "plant/site/#{SITE}#/resource_plc_count/#{RESOURCE}#/#{PLC_CATEGORY}#",
 				PLC_ADDRESS_COUNT: baseUrl + "plant/site/#{SITE}#/resource_plc_count?plc_category=#{PLC_CATEGORY}#&resource=#{RESOURCE}#",
 				// 新增前验证接口：POST 
 				// PLC_ADDRESS_EXIST_RESOURCE: baseUrl + "plant/site/exist_resource_plc", 				
 				// 修改前验证接口：
 				PLC_ADDRESS_EXIST_PLC_OBJECT: baseUrl + "plant/site/#{SITE}#/exist_plc_object?plc_address=#{PLC_ADDRESS}#&plc_value=#{PLC_VALUE}#&resource_bo=#{RESOURCE_BO}#&plc_object=#{PLC_OBJECT}#",
 				// 新增前验证: 
 				PLC_ADDRESS_EXIST_RESOURCE_PLC: baseUrl + "plant/site/#{SITE}#/exist_resource_plc?plc_address=#{PLC_ADDRESS}#&plc_value=#{PLC_VALUE}#&resource_bo=#{RESOURCE_BO}#",
 				PLC_ADDRESS_EXIST_RESOURCE_PLC_OBJ: baseUrl + "plant/site/#{SITE}#/exist_resource_plc_obj?plc_object=#{PLC_OBJECT}#&resource_bo=#{RESOURCE_BO}#",

 				// PLC_ADDRESS_EXIST_OBJECT: baseUrl + "plant/site/exist_plc_object",
 				// PLC_ADDRESS_EXIST_OBJECT: baseUrl + "plant/site/#{SITE}#/exist_plc_object_obj_controller?plc_object=#{PLC_CATEGORY}#",
 				// 修改前验证接口: post --> get 
 				// PLC_ADDRESS_IS_USED: "plant/site/#{SITE}#/is_plc_object_used?plc_object=#{PLC_OBJECT}#",
 				// 保存按钮接口：POST		
 				PLC_ADDRESS_CHANGE: baseUrl + "plant/change_resource_plc",

				PLC_EXPORT_EXCEL: baseUrl + "plant/site/{SITE}/resource_plc/export_excel",

 				////////////////////////////////////////////////////////////////////////
 				////// 原因代码、原因代码组 维护 相关接口 :  	
 				////////////////////////////////////////////////////////////////////////
 				REASON_CODE_GROUP: baseUrl + "plant/site/#{SITE}#/reason_code_group",
 				REASON_CODE_GROUP_EXIST: baseUrl + "plant/site/#{SITE}#/exist_reason_code_group?reason_code_group=#{REASON_CODE_GROUP}#",
 				REASON_CODE_GROUP_CHANGE: baseUrl + "plant/change_reason_code_group",
 				
 				//根据原因代码组获取原因代码
				REASON_CODE_LIST: baseUrl + "plant/site/#{SITE}#/reason_code_page?reason_code_group=#{REASON_CODE_GROUP}#&index=#{INDEX}#&count=#{COUNT}#",
 				REASON_CODE_EXIST: baseUrl + "plant/site/#{SITE}#/exist_reason_code?reason_code=#{REASON_CODE}#",
 				REASON_CODE_SAVE: baseUrl + "plant/change_reason_code",
				REASON_CODE_COUNT: baseUrl + "plant/site/#{SITE}#/reason_code_page_count?reason_code_group=#{REASON_CODE_GROUP}#",

				REASON_CODE_LIST_ALL:baseUrl + "plant/site/#{SITE}#/reason_code_all?reason_code_group=#{REASON_CODE_GROUP}#&enabled=#{ENABLED}#",
				REASON_CODE_AND_DEVICE_STATUS:baseUrl +"plant/site/#{SITE}#/reason_code_res_state?reason_code_group=#{REASON_CODE_RES_STATE}#&date_time=#{DATE_TIME}#&index=#{INDEX}#&count=#{COUNT}#",
				REASON_CODE_AND_DEVICE_STATUS_COUNT:baseUrl +"plant/site/#{SITE}#/reason_code_res_state_count?reason_code_group=#{REASON_CODE_RES_STATE}#&date_time=#{DATE_TIME}#",
				REASON_CODE_RES_STATE_ADD:baseUrl + "plant/site/#{SITE}#/reason_code_res_state_add?reason_code_group=#{REASON_CODE_GROUP}#",
				DEVICE_STATUS_LIST:baseUrl + "plant/site/#{SITE}#/resource_state?enabled=#{ENABLED}#",
				REASON_CODE_RES_STATUS_VALIDATE_BEFORE_ADD:baseUrl + "plant/site/#{SITE}#/check_reason_code_res_state?reaon_code=#{REASON_CODE}#&date_time=#{DATE_TIME}#&state=#{STATE}#",
				DEVICE_STATUS_SAVE: baseUrl + "plant/change_reason_code_res_state",
				
				
				
 				////////////////////////////////////////////////////////////////////////
 				////// 工厂管理维护 相关接口 : 
 				////////////////////////////////////////////////////////////////////////
 				SITE_LIST: baseUrl + "site/site_list",
 				SITE_SAVE: baseUrl + "site/change_site",
 				
 				////// 工厂管理维护 相关接口 : 
 				JOB_SELECT: baseUrl +"job/job_select",
 				JOB_RUNTIME: baseUrl +"job/job_run_time_select?activity_bo=#{activity_bo}#",
 				JOB_LOG_LIST: baseUrl+"job/job_run_status_list?job_name=#{job_name}#&job_run_time=#{job_run_time}#&job_run_day=#{job_run_day}#",
 				JOB_EXECUTE: baseUrl+"job/reason_code_recognize_job",
 				
 				////// 工厂管理维护 相关接口 : 
 				////////////////////////////////////////////////////////////////////////
 				////// 理论PPM模块 相关接口 : 
 				////////////////////////////////////////////////////////////////////////
 				PPM_UPLOAD_FILE: baseUrl + "std/cycletime/management/site/#{SITE}#/upload_file",
 				PPM_LIST: baseUrl + "std/cycletime/management/site/#{SITE}#/search?item=#{ITEM}#&resrce=#{RESRCE}#&date=#{DATE}#&index=#{INDEX}#&count=#{COUNT}#&model=#{MODEL}#&resourceType=#{RESOURCE_TYPE}#&line=#{LINE}#&workCenter=#{WORK_CENTER}#",
 				PPM_LIST_COUNT: baseUrl + "std/cycletime/management/site/#{SITE}#/total_number/search?item=#{ITEM}#&resrce=#{RESRCE}#&date=#{DATE}#&model=#{MODEL}#&resourceType=#{RESOURCE_TYPE}#&line=#{LINE}#&workCenter=#{WORK_CENTER}#",
 				PPM_SAVE: baseUrl + "std/cycletime/management/site/#{SITE}#/change_std_cycle_time",
 				PPM_VALID_FOR_CREATE: baseUrl + "std/cycletime/management/site/#{SITE}#/check_existing_cycletime",

 				////////////////////////////////////////////////////////////////////////
 				////// 产出目标值维护 相关接口 : 物料编码、班次、
 				////////////////////////////////////////////////////////////////////////
 				// required: 		model 
 				XLZ_MATERIAL_CODES: baseUrl + "plant/site/#{SITE}#/item?model=#{MODEL}#", 
				// not required: 	resrce、work_center 
				XLZ_SHIFTS: baseUrl + "plant/site/#{SITE}#/shift?work_center=#{WORK_CENTER}#", // resrce=#{RESRCE}#&work_center=#{WORK_CENTER}# 
				// required: 		item、shift、shift_desc、start_data_time、end_date_time、work_center、line、pageIndex、pageCount 
				// not required: 	resrce、resource_type、model 
				OUTPUT_TARGET_LIST: baseUrl + "plant/site/#{SITE}#/production_output_target?item=#{ITEM}#&resrce=#{RESRCE}#&shift=#{SHIFT}#&shift_desc=#{SHIFT_DESC}#&start_date_time=#{STAET_DATE_TIME}#&end_date_time=#{END_DATE_TIME}#&page_index=#{PAGE_INDEX}#&page_count=#{PAGE_COUNT}#&work_center=#{WORK_CENTER}#&line=#{LINE}#&model=#{MODEL}#&resource_type=#{resource_type}#",
				OUTPUT_TARGET_LIST_COUNT: baseUrl + "plant/site/#{SITE}#/production_output_target_count?item=#{ITEM}#&resrce=#{RESRCE}#&shift=#{SHIFT}#&shift_desc=#{SHIFT_DESC}#&start_date_time=#{STAET_DATE_TIME}#&end_date_time=#{END_DATE_TIME}#&work_center=#{WORK_CENTER}#&line=#{LINE}#&model=#{MODEL}#&resource_type=#{resource_type}#",
				// required: 		item、shift、target_date     
				// not required: 	resrce      				
				OUTPUT_TARGET_EXIST: baseUrl + "plant/site/#{SITE}#/exist_production_output_target?item=#{ITEM}#&resrce=#{RESRCE}#&shift=#{SHIFT}#&target_date=#{TARGET_DATE}#",
				OUTPUT_TARGET_SAVE: baseUrl + "plant/change_production_output_target",
				
				
				////////////////////////////////////////////////////////////////////////
				////// 作业、作业组、用户、用户组、客户端管理
				////////////////////////////////////////////////////////////////////////
				//通过关键字搜索出作业参数
				GET_SEARCH_RESULT_BY_KEY: baseUrl + "plant/activity/getActivityInputAssist?activity=#{ACTIVITY}#",
				//通过作业搜索所需结果
				GET_SEARCH_RESULT_BY_WORK: baseUrl + "plant/activity/getActivityBasicInfo?activity=#{ACTIVITY}#",
				//判断是否可以删除
				WORK_PARAM_IS_USED:baseUrl + "plant/activity/is_param_used?paramID=#{PARAMID}#",
				//保存作业参数
				SAVE_WORK_PARAMS: baseUrl + "plant/activity/change_activityParam",



				//保存作业基本信息
				SAVE_CHANGED_WORK_INFO: baseUrl+"plant/activity/change_activity",


				//已分配作业组查询接口
				GET_ASSIGEND_ACTIVITY_GROUP: baseUrl + "plant/activity/getAssigendActivityGroup?activity=#{ACTIVITY}#",
				//可用作业组查询接口
				GET_AVAILABLE_ACTIVITY_GROUP: baseUrl + "plant/activity/getAvailableActivityGroup?activity=#{ACTIVITY}#",
				//保存作业组
				SAVE_BASIC_WORKGROUP: baseUrl + "plant/activity/move_activityGroup",


				//通过关键字搜索客户端作业
				GET_CLIENT_SEARCH_RESULT_BY_KEY:baseUrl +"plant/activityClient/getActivityClientForInputAssist?activityClient=#{ACTIVITYCLIENT}#",
				//通过客户端作业查询数据
				GET_SEARCH_RESULT_BY_CLIENT_WORK: baseUrl + "plant/activityClient/getActivityClientByPrimaryKey?activityClient=#{ACTIVITYCLIENT}#",
				//保存客户端
				SAVE_CLIENT_WORK: baseUrl + "plant/activityClient/change_activityClient",


				//通过关键字搜索作业组
				GET_WORK_GROUP_SEARCH_RESULT_BY_KEY:baseUrl + "plant/activityGroup/getActivityGroupInputAssist?activityGroup=#{ACTIVITYGROUP}#",
				//通过作业组查询数据
				GET_SEARCH_RESULT_BY_WORK_GROUP: baseUrl + "plant/activityGroup/getActivityGroupByPrimaryKey?activityGroup=#{ACTIVITYGROUP}#",
				//获得作业组管理模块的已分配作业
				GET_ASSIGEND_ACTIVITY_GROUP_MANAGER:baseUrl+"plant/activityGroup/getAssigendActivity?activityGroup=#{ACTIVITYGROUP}#",
				//获得作业组管理模块的可用作业
				GET_AVAILABLE_ACTIVITY_GROUP_MANAGER:baseUrl+"plant/activityGroup/getAvaliableActivity?activityGroup=#{ACTIVITYGROUP}#",
				//保存作业组增删改
				SAVE_CHANGED_WORK_GROUP:baseUrl+"plant/activityGroup/changeActivityGroup",
				//保存移动作业组
				SAVE_MOVE_WORK_GROUP:baseUrl+"plant/activityGroup/moveActivity",



				//通过关键字搜索出作业参数设定值
				GET_WORK_PARAM_DESIGN_BY_KEY: baseUrl + "plant/activity/param/activities?activity=#{ACTIVITY}#",
				//通过作业参数设定值查询数据
				GET_SEARCH_RESULT_BY_WORK_PARAM_DESIGN: baseUrl + "plant/activity/param/site/#{SITE}#/activity_param_values?activity=#{ACTIVITY}#",
				//保存作业参数设定值
				SAVE_WORK_PARAM_DESIGN: baseUrl+"plant/activity/param/site/#{SITE}#/change_activity_param_values",
				////////////////////////////////////////////////////////////////////////
				////// 用户、用户组、客户端管理
				////////////////////////////////////////////////////////////////////////
				USER_QUERY_DATAS : baseUrl + "user/management/site/#{site}#/search?userId=#{userId}#&plcIp=#{plcIp}#",

				//办公PC下
				OFFICE_USER_QUERY_DATAS : baseUrl + "user/management/site/#{site}#/search?userId=#{userId}#",

				USER_QUERY_USE_DATAS : baseUrl + "user/management/site/#{site}#/usergroups?userId=#{userId}#",
				USER_KEEP_DATAS : baseUrl + "user/management/site/#{site}#/#{userId}#/update_usergroup",
				USER_UPLOAD: baseUrl + "user/management/site/#{SITE}#/upload_file",


				USER_GROUP_DATA : baseUrl + "plant/site/#{site}#/user_group_help?user_group=#{user_group}#",
				USER_GROUP_SEARCH : baseUrl + "plant/site/#{site}#/user_group?user_group=#{user_group}#",
				USER_GROUP_AVAILABLE : baseUrl + "plant/site/#{site}#/user_all",
				USER_GROUP_UNAVAILABLE : baseUrl + "/plant/site/#{site}#/user_used?user_group=#{user_group}#",
				USER_GROUP_KEEPAVAILABLE : baseUrl + "plant/change_user_group",
				USER_GROUP_KEEPUNAVAILABLE : baseUrl + "plant/change_user_group_member",
				USER_GROUP_PLATFORMS : baseUrl + "plant/#{site}#/activity_perm?user_group=#{user_group}#",
				USER_GROUP_KEEP_PLATFORMS : baseUrl + "plant/change_activity_perm",

				USER_GROUP_CUSTOMER_WORK : baseUrl + "plant/#{site}#/activity_client_all",
				USER_GROUP_CUSTOMER_UNWORK : baseUrl + "plant/#{site}#/activity_client_perm?user_group=#{user_group}#",
				USER_GROUP_KEEP_CUSTOMER_PLATFORMS : baseUrl + "plant/change_activity_client_perm",

				////////////////////////////////////////////////////////////////////////
				////// 停机计划 相关接口 :原因代码 、 查询、保存  删除 
				////////////////////////////////////////////////////////////////////////
				STOP_PLAN_REASON_LIST : baseUrl + "plant/site/#{SITE}#/reason_code",
				STOP_PLAN_QUERY_LIST : baseUrl + "plant/site/#{SITE}#/scheduled_down_plan?resrce=#{resrce}#&time=#{time}#&work_center=#{work_center}#&line=#{line}#&index=#{index}#&count=#{count}#&resource_type=#{resource_type}#",
				STOP_PLAN_QUERY_LIST_COUNT : baseUrl + "plant/site/#{SITE}#/scheduled_down_plan_count?resrce=#{resrce}#&time=#{time}#&work_center=#{work_center}#&line=#{line}#&resource_type=#{resource_type}#",
				STOP_PLAN_VERIFICATION_DATA : baseUrl +"plant/site/#{SITE}#/exist_repeat_time?resrce=#{resrce}#&start_date_time=#{start_date_time}#&end_date_time=#{end_date_time}#&handle=#{handle}#",
				STOP_PLAN_KEEP_DATA : baseUrl +"plant/scheduled_down_plan",
				////////////////////////////////////////////////////////////////////////
				////// 产量维护 相关接口 :原因代码 、 查询、保存  删除
				////////////////////////////////////////////////////////////////////////
				YIELD_MAINTAIN_QUERY_DATA : baseUrl +"yield/site/#{site}#/production_out?resrce=#{resrce}#&item=#{item}#&productionDate=#{productionDate}#&startTime=#{startTime}#&shift=#{shift}#&endTime=#{endTime}#&shiftDescription=#{shiftDescription}#",
				YIELD_MAINTAIN_KEEP_DATA : baseUrl +"yield/change_production_out",
				YIELD_MAINTAIN_SHIFTS: baseUrl + "yield/site/#{site}#/workCenterShift?resrce=#{resrce}#",
				YIELD_MAINTAIN_QUERY_RESRSCE: baseUrl + "yield/site/#{site}#/resource?resrce=#{resrce}#&lineArea=#{lineArea}#&workArea=#{workArea}#&resourceType=#{resourceType}#",
				YIELD_MAINTAIN_ITEM : baseUrl +"yield/site/#{site}#/check_existing_item?item=#{item}#",
				YIELD_MAINTAIN_RESRCE : baseUrl +"yield/site/#{site}#/check_existing_resource?resrce=#{resrce}#",


				////////////////////////////////////////////////////////////////////////
				////// 原因代码识别规则管理 
				////////////////////////////////////////////////////////////////////////
				// 查询 -“原因代码”输入帮助: a、REASON_CODE_T1
				REASON_RULE_DISTINGUISH: baseUrl + "plant/reason_code/recognition/site/#{SITE}#/reason_code?reasonCode=#{REASON_CODE}#",
				// 查询 – 历史变更记录 原因代码、优先级、规则生效日期大于:
				REASON_RULE_HIS_CHANGE_LOG: baseUrl + "plant/reason_code/recognition/site/#{SITE}#/history_change_log?reasonCode=#{REASON_CODE}#&priority=#{PRIORITY}#&dateTime=#{DATE_TIME}#",
				// 查询 – 历史状态记录
				REASON_RULE_HIS_STATUS_LOG: baseUrl + "plant/reason_code/recognition/site/#{SITE}#/history_status_log?reasonCode=#{REASON_CODE}#&priority=#{PRIORITY}#&dateTime=#{DATE_TIME}#",
				// 识别方式作业 输入帮助
				REASON_RULE_DISTINGUISH_INPUT_HELP: baseUrl + "plant/reason_code/recognition/recognition_input_help?activity=#{ACTIVITY}#",
				// 防呆验证作业 输入帮助
				REASON_RULE_CHECK_INPUT_HELP: baseUrl + "plant/reason_code/recognition/filter_input_help?activity=#{ACTIVITY}#",
				// 识别优先级增删改接口: POST
				REASON_RULE_PRIORITY_CHANGE: baseUrl + "plant/reason_code/recognition/changeReasonCodePriority",
				
				// 原因代码详情：识别优先级、识别方式、防呆验证: 
				REASON_RULE_DETAIL_ALL: baseUrl + "plant/reason_code/recognition/detail_info?reasonCodePriorityBo=#{REASON_CODE_PRIORITY_BO}#",
				
				// “详细信息”-“防呆验证”参数值按钮: 
				REASON_RULE_DETAIL_STAY_PARAMS: baseUrl + "plant/reason_code/recognition/site/#{SITE}#/filter_param_button?reasonCodeRuleBoF=#{REASON_CODE_RULE_BO_F}#&activityFParamId=#{ACTIVITY_F_PARAM_ID}#",
				// “新建”- “防呆验证”By设备类型参数值保存   POST
				REASON_RULE_NEW_TYPE_PARAM_SAVE: baseUrl + "plant/reason_code/recognition/site/#{SITE}#/change_resource_type_param", 
				
				
				
				// 识别优先级详情 
				REASON_RULE_PRIORITY_DETAIL: baseUrl + "plant/reason_code/recognition/ReasonCodePriorityDetail?reasonCodePriorityBO=#{REASON_CODE_PRIORITY_BO}#",
				// 识别方式详情 “详细信息”-“识别方式作业”修改
				REASON_RULE_PATTERN_UPDATE: baseUrl + "plant/reason_code/recognition/updateRecognitionPattern", 
				
				// REASON_RULE_PATTERN_DETAIL: baseUrl + "plant/reason_code/recognition/recognitionPatternDetail?activity=#{ACTIVITY}#&type=#{TYPE}#",
				REASON_RULE_PATTERN_DETAIL: baseUrl + "plant/reason_code/recognition/activity_param?activityBo=#{ACTIVITY_BO}#&executionType=#{EXECUTION_TYPE}#",
				// “详细信息”-“防呆验证作业”修改      
				REASON_RULE_ACTIVITY_PARAM: baseUrl + "plant/reason_code/recognition/activity_param?activityBo=#{ACTIVITY_BO}#&executionType=#{EXECUTION_TYPE}#", // 
				// “详细信息”- “防呆验证”编辑保存  ！！！POST   
				REASON_RULE_RULE_PARAM_F_CHANGE: baseUrl + "plant/reason_code/recognition/site/#{SITE}#/change_rule_param_f",
				
				// “新建”-“防呆验证”参数值按钮  
				REASON_RULE_NEW_FILTER_PARAM: baseUrl + "plant/reason_code/recognition/site/#{SITE}#/filter_param_button_for_new?activityBo=#{ACTIVITY_BO}#&activityFParamId=#{ACTIVITY_F_PARAM_ID}#", 
				// “新建”-“防呆验证作业”保存   POST 
				REASON_RULE_NEW_FILTER_ACTIVITY_SAVE: baseUrl + "plant/reason_code/recognition/site/#{SITE}#/save_filter_activity", 
				// "新建"-验证作业信息保存
				// REASON_RULE_CHANGE_RULE_F: 
				// 识别方式新增接口    POST
				REASON_RULE_NEW_C_PATTERN: baseUrl + "plant/reason_code/recognition/createRecognitionPattern?activity=#{ACTIVITY}#&reasonCodePriorityBO=#{REASONCODE_PRIORIY_BO}#&site=#{SITE}#",
				// 识别方式修改接口	POST
				REASON_RULE_NEW_U_PATTERN: baseUrl + "plant/reason_code/recognition/updateRsecognitionPattern", 
				// 删除 				
				REASON_RULE_NEW_D_RECOGNITION: baseUrl + "plant/reason_code/recognition/deleteReasonCodeRecognition",  // ?reasonCodePriorityBO=#{REASON_CODE_PRIORITY_BO}#
				
				//  http://localhost:8080/mhp-oee/web/rest/   ?handle=ReasonCodeAlertBO:ResourceTypeBO:1030,ACMT,ALERT_ACMT0001,ReasonCodeBO:1030,NP08,20160808.124257

				////////////////////////////////////////////////////////////////////////
				////// WEB045 中 报警信息原因代码对照关系管理 
				////////////////////////////////////////////////////////////////////////
				ALARM_REASON_CODE_IS_USED: baseUrl + "reasoncode/site/#{SITE}#/alert/commit/is_reason_code_alert_used?handle=#{HANDLE}#", 
				ALARM_REASON_CODE_CHANGE: baseUrl + "reasoncode/alert/commit/change_reason_code_alert",
				// 设备类型输入帮助  
				ALARM_RESRCE_TYPE_ALL: baseUrl + "reasoncode/site/#{SITE}#/alert/commit/resrce_type_all?resrce_type=#{RESRCE_TYPE}#",
				// 当前生效查询 
				ALARM_REASON_CODE_ALERT_CURRENT: baseUrl + "reasoncode/site/#{SITE}#/alert/commit/reason_code_alert_current?resrce_type=#{RESRCE_TYPE}#&index=#{index}#&count=#{count}#",
				ALARM_REASON_CODE_ALERT_CURRENT_COUNT: baseUrl + "reasoncode/site/#{SITE}#/alert/commit/reason_code_alert_current_count?resrce_type=#{RESRCE_TYPE}#",
				// 历史数据 -- > 历史变更查询  
				ALARM_REASON_CODE_ALERT_HIS_CHANGE: baseUrl + "reasoncode/site/#{SITE}#/alert/commit/reason_code_alert_his_change?resrce_type=#{RESRCE_TYPE}#&time=#{TIME}#&index=#{index}#&count=#{count}#",
				ALARM_REASON_CODE_ALERT_HIS_CHANGE_COUNT: baseUrl + "reasoncode/site/#{SITE}#/alert/commit/reason_code_alert_his_change_count?resrce_type=#{RESRCE_TYPE}#&time=#{TIME}#",

				// 历史数据 -- > 历史状态查询  
				ALARM_REASON_CODE_ALERT_HIS_STATUS: baseUrl + "reasoncode/site/#{SITE}#/alert/commit/reason_code_alert_his_status?resrce_type=#{RESRCE_TYPE}#&time=#{TIME}#&index=#{index}#&count=#{count}#",
				ALARM_REASON_CODE_ALERT_HIS_STATUS_COUNT: baseUrl + "reasoncode/site/#{SITE}#/alert/commit/reason_code_alert_his_status_count?resrce_type=#{RESRCE_TYPE}#&time=#{TIME}#",

				// 文件下载  

				ALARM_EXPORT_EXCEL: baseUrl + "reasoncode/alert/site/#{SITE}#/export_excel?resrce_type=#{RESRCE_TYPE}#",
				//上传
				ALERT_COMMIT_UPLOAD_FILE: baseUrl + "reasoncode/site/#{SITE}#/alert/commit/upload_file",
				//验证
				ALERT_VERIFY_UPLOAD_FILE: baseUrl + "reasoncode/site/#{SITE}#/alert/verify/upload_file?startDate=#{startDate}#",

				ALERT_KEEP_FILE: baseUrl + "reasoncode/alert/commit/change_reason_code_alert",

				////////////////////////////////////////////////////////////////////////
				////// 权限控制 baseUrlPrefix

				////// 权限控制baseUrlPrefix
				////////////////////////////////////////////////////////////////////////
				USER_PURVIEW : baseUrlPrefix + "auth/management/site/#{SITE}#",
				USER_INFOS : baseUrlPrefix + "auth/logon_user",
				USER_SITELIST : baseUrlPrefix + "auth/site/#{site}#/user_sitelist?userId=#{userId}#",
				USER_LOGOUT: baseUrlPrefix + "auth/logout",
				////////////////////////////////////////////////////////////////////////
				////// 产出报表控制
				////////////////////////////////////////////////////////////////////////
				OUTPUTREPORT_AREA : baseUrl + "report/site/#{site}#/work_center_help?work_center=#{work_center}#",
				//新的生产区域接口 with choose工厂site
				OUTPUTREPORT_AREA_NEW : baseUrl + "report/help/work_center?sites=#{sites}#&work_center=#{work_center}#",
				OUTPUTREPORT_LINE : baseUrl + "report/site/#{site}#/line_help?line=#{line}#&work_center=#{work_center}#",
				//新的拉线接口   with choose工厂site
				OUTPUTREPORT_LINE_NEW : baseUrl + "report/help/lines?sites=#{sites}#&line=#{line}#&work_center=#{work_center}#",
				OUTPUTREPORT_DEVICE : baseUrl + "report/site/#{site}#/resource_help?resrce=#{resrce}#&lineArea=#{lineArea}#&workArea=#{workArea}#",
				OUTPUTREPORT_MODEL : baseUrl + "report/site/#{site}#/model_help?model=#{model}#",
				OUTPUTREPORT_PRD: baseUrl + "report/site/#{site}#/prd_help",
				OUTPUTREPORT_CODE : baseUrl + "report/site/#{site}#/item_help?model=#{model}#&item=#{item}#",
				PLC_OUTPUTREPORT_CODE : baseUrl + "report/site/#{site}#/item_help?model=#{model}#&item=#{item}#&plcIp=#{plcIp}#",

				OUTPUTREPORT_SELECT : baseUrlPrefix + "report/site/#{site}#/production_output_report",
				OUTPUTREPORT_PULL : baseUrlPrefix + 	"report/site/#{site}#/export_excel",


				////////////////////////////////////////////////////////////////////////
				////// OEE报表控制
				////////////////////////////////////////////////////////////////////////
				OEE_REPORT_LIST: baseUrl + "report/oee/site/#{site}#/oee_output_report?resourceBo=#{resourceBo}#&startDateTime=#{startDateTime}#&endDateTime=#{endDateTime}#&byTimeType=#{byTimeType}#",
				OEE_REASON_CODE_REPORT_INSIDE : baseUrl + "report/oee/site/#{site}#/oee_reason_code_group_report?resourceBo=#{resourceBo}#&startDateTime=#{startDateTime}#&endDateTime=#{endDateTime}#&byTimeType=#{byTimeType}#",
				OEE_REASON_CODE_REPORT_OUTSIDE : baseUrl + "report/oee/site/#{site}#/oee_reason_code_report?resourceBo=#{resourceBo}#&startDateTime=#{startDateTime}#&endDateTime=#{endDateTime}#&byTimeType=#{byTimeType}#",
				OEE_REPORT_ALARM_LINE:baseUrl +"report/oee/site/#{site}#/oee_alarmLine",
				OEE_EXPORT_EXCEL_FILE : baseUrl + "report/oee/site/#{site}#/export_excel?resourceBo=#{resourceBo}#&startDateTime=#{startDateTime}#&endDateTime=#{endDateTime}#&byTimeType=#{byTimeType}#",
				OEE_SELECT_DEVICE_CODE :baseUrl + 'report/site/#{site}#/oee_report_resource_help?resrce=#{resrce}#',
				////////////////////////////////////////////////////////////////////////
				////// ppm报表控制  
				////////////////////////////////////////////////////////////////////////
				PPMREPORT_SELECT : baseUrlPrefix + "report/ppm/site/#{site}#/ppm_output_report",
				
				PPMREPORT_PULL : baseUrlPrefix + 	 "report/ppm/site/#{site}#/export_excel",
				// 设备状态明细 
				RESOURCE_STATUS_DETAIL_WITH_ALARM_COMMENT_CODE :baseUrl + "report/site/#{site}#/resource_status_detail_report?page_size=#{page_size}#&curr_page=#{curr_page}#&work_area=#{work_area}#&line_area=#{line_area}#&start_date=#{start_date}#&end_date=#{end_date}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&shift=#{shift}#&type=#{type}#&commentCode=#{comment_code}#",
				RESOURCE_STATUS_DETAIL :baseUrl + "report/site/#{site}#/resource_status_detail_report?page_size=#{page_size}#&curr_page=#{curr_page}#&work_area=#{work_area}#&line_area=#{line_area}#&start_date=#{start_date}#&end_date=#{end_date}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&shift=#{shift}#&type=#{type}#",
				RESOURCE_STATUS_DETAIL_EXCEL :baseUrl + "report/site/#{site}#/resource_status_detail/export_excel?work_area=#{work_area}#&line_area=#{line_area}#&start_date=#{start_date}#&end_date=#{end_date}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&shift=#{shift}#&type=#{type}#",
				RESOURCE_STATUS_DETAIL_COUNT :baseUrl +"report/site/#{site}#/resource_status_detail_count?work_area=#{work_area}#&line_area=#{line_area}#&start_date=#{start_date}#&end_date=#{end_date}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&shift=#{shift}#",
				RESOURCE_STATUS_DETAIL_TOTAL :baseUrl +"report/site/#{site}#/resource_status_detail_total?type=#{type}#&work_area=#{work_area}#&line_area=#{line_area}#&start_date=#{start_date}#&end_date=#{end_date}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&shift=#{shift}#",
				RESOURCE_STATUS_DETAIL_TOTAL_WITH_ALARM_COMMENT_CODE :baseUrl +"report/site/#{site}#/resource_status_detail_total?type=#{type}#&work_area=#{work_area}#&line_area=#{line_area}#&start_date=#{start_date}#&end_date=#{end_date}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&shift=#{shift}#&commentCode=#{comment_code}#",

				// device realtime query
				DEVICE_REALTIME_QUERY :baseUrl +"report/site/#{site}#/realtime_device_status_report?work_center=#{work_center}#&line=#{line}#&device_type=#{device_type}#&device_code=#{device_code}#",
			//设备编码   拉线多选
				XLZ_RESOURCE_CODES_BYLINES: baseUrl + "plant/site/#{SITE}#/resource_condition_opt_with_line?resource_type=#{RESOURCE_TYPE}#&line=#{LINE}#&work_center=#{WORK_CENTER}#",
				PLC_XLZ_RESOURCE_CODES_BYLINES: baseUrl + "plant/site/#{SITE}#/resource_condition_opt_with_line?resource_type=#{RESOURCE_TYPE}#&line=#{LINE}#&work_center=#{WORK_CENTER}#&plcIp=#{plcIp}#",

				//设备编码   拉线多选  需要拉线描述 停机计划用
				XLZ_RESOURCE_CODES_BYLINES_DESC: baseUrl + "plant/site/#{SITE}#/resource_condition_opt_with_line_add_linearea?resource_type=#{RESOURCE_TYPE}#&line=#{LINE}#&work_center=#{WORK_CENTER}#",


				OEE_DETAIL_EXCEL:baseUrl + "report/oeedetail/site/#{site}#/export_excel?work_area=#{work_area}#&line_area=#{line_area}#&start_date=#{start_date}#&end_date=#{end_date}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&shift=#{shift}#",
				
				OEE_DETAIL_EXCEL_COUNT:baseUrl + "report/oeedetailnum/site/#{site}#/export_excel?work_area=#{work_area}#&line_area=#{line_area}#&start_date=#{start_date}#&end_date=#{end_date}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&shift=#{shift}#",
				
				RS_DETAIL_EXCEL:baseUrl + "report/site/#{site}#/resource_status_detail_collect/export_excel?work_area=#{work_area}#&line_area=#{line_area}#&start_date=#{start_date}#&end_date=#{end_date}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&shift=#{shift}#&resource_state=#{resource_state}#",
				
				RS_DETAIL_EXCEL_COUNT:baseUrl + "report/site/#{site}#/resource_status_detail_collect_count?work_area=#{work_area}#&line_area=#{line_area}#&start_date=#{start_date}#&end_date=#{end_date}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&shift=#{shift}#&resource_state=#{resource_state}#",
				
				//PPM数据导出报表接口地址
				PPM_DATA_DOWNLOAD:baseUrl+"report/ppm/site/#{site}#/export_excel_all",
				
				//PPM数据导出报表记录数接口地址
				PPM_DATA_DOWNLOAD_NUM:baseUrl+"report/ppm/site/#{site}#/export_excel_all_num",
				
				//停机原因柏拉图报表
				SHUTDOWN_REASON_REPORT :baseUrl+"report/site/#{site}#/shutdown_reason_report?data_type=#{data_type}#&work_center=#{work_center}#&line_in=#{line_in}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&start_date=#{start_date}#&end_date=#{end_date}#&by_timetype=#{by_timetype}#&by_type=#{by_type}#&shift=#{shift}#",
				SHUTDOWN_REASON_EXPORT :baseUrl+"report/site/#{site}#/shutdown_reason/export_excel?data_type=#{data_type}#&work_center=#{work_center}#&line_in=#{line_in}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&start_date=#{start_date}#&end_date=#{end_date}#&by_timetype=#{by_timetype}#&by_type=#{by_type}#&shift=#{shift}#",
				
				
				//生产微计划报表
				PRODUCE_MOCRO_PLAN_REPORT:baseUrl+"report/site/#{site}#/production_realtime_report?work_center=#{work_center}#&line_in=#{line_in}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&start_date=#{start_date}#&end_date=#{end_date}#&model=#{model}#&item=#{item}#&colNum=#{colNum}#",
				PRODUCE_MOCRO_PLAN_EXPORT:baseUrl+"report/site/#{site}#/production_realtime/export_excel?work_center=#{work_center}#&line_in=#{line_in}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&start_date=#{start_date}#&end_date=#{end_date}#&model=#{model}#&item=#{item}#&colNum=#{colNum}#",

				//OEE趋势分析报表
				OEE_TREND_ANALYSIS_REPORT:baseUrl+"report/oeetrend/site/#{site}#/report?byResrceType=#{byResrceType}#&work_area=#{work_area}#&line_area=#{line_area}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&start_date=#{start_date}#&end_date=#{end_date}#&byTimeType=#{byTimeType}#&model=#{model}#&pro=#{pro}#&shift=#{shift}#",
				OEE_TREND_ANALYSIS_EXPORT:baseUrl+"report/oeetrend/site/#{site}#/export_excel?byResrceType=#{byResrceType}#&work_area=#{work_area}#&line_area=#{line_area}#&resource_type=#{resource_type}#&resource_code=#{resource_code}#&start_date=#{start_date}#&end_date=#{end_date}#&byTimeType=#{byTimeType}#&model=#{model}#&pro=#{pro}#&shift=#{shift}#",

				//设备宏观状态列表
				DEVICE_GLOBAL_STATUS_REPORT:baseUrl+"report/realtime/main?dimension=#{dimension}#&site_list=#{site_list}#&work_area_list=#{work_area_list}#&line_area_list=#{line_area_list}#&source_type_list=#{source_type_list}#",

				//报警备注列表
				ALERT_COMMENT_LIST:baseUrl+"alert/comment/site/#{SITE}#/list?resource_type=#{RESOURCE_TYPE}#&enabled=#{ENABLED}#",
				ALERT_COMMENT_USED:baseUrl+"alert/comment/site/#{SITE}#/userd?comment_code=#{COMMENT_CODE}#",
				CHANGE_ALERT_COMMENT:baseUrl+"alert/comment/site/#{SITE}#/change",

				//报警备注看板
				PLC_ALARM_REMARK_REPORT: baseUrl+"report/alert/site/#{SITE}#/comment_query?resrce=#{PLC_RESRCE}#",
				PLC_NEW_ALARM_REMARK_REPORT: baseUrl+"report/alert/site/#{SITE}#/comment_query?resrce=#{PLC_RESRCE}#&plcIp=#{plcIp}#",

				GET_IP:baseUrl+"report/get_request_ip",

				//报警备注看板获取设备号report/alert/site/{site}/get_resource_by_ip
				PLC_ALARM_REMARK_REPORT_STATEMENT: baseUrl+"report/alert/site/#{SITE}#/get_resource_by_ip?ip=#{ALARM_IP}#",
				PLC_NEW_ALARM_REMARK_REPORT_STATEMENT: baseUrl+"report/alert/site/#{SITE}#/get_resource_by_ip?ip=#{ALARM_IP}#&plcIp=#{plcIp}#",

				//报警备注看板 --修改备注
				PLC_ALARM_REMARK_UPDATE: baseUrl+"report/alert/site/#{SITE}#/update_alert?resrce=#{PLC_RESRCE}#&date_time=#{PLC_DATE_TIME}#&description=#{PLC_DESCRIPTION}#&alert_comment=#{PLC_ALERT_COMMENT}#&comment_code=#{PLC_COMMENT_CODE}#",
				PLC_NEW_ALARM_REMARK_UPDATE: baseUrl+"report/alert/site/#{SITE}#/update_alert?resrce=#{PLC_RESRCE}#&date_time=#{PLC_DATE_TIME}#&description=#{PLC_DESCRIPTION}#&alert_comment=#{PLC_ALERT_COMMENT}#&comment_code=#{PLC_COMMENT_CODE}#&plcIp=#{plcIp}#",

				//设备报警明细报表 --根据备注描述查 备注report/alert/site/{site}/remark/selectlike
				PLC_ALARM_REMARK_QUERY_DESCRIPTION: baseUrl+"report/alert/site/#{SITE}#/remark/selectlike?description=#{DESCRIPTION}#",

				//设备宏观状态各种颜色灯列表
				DEVICE_GLOBAL_LIST_QUERY_BY_COLOR: baseUrl+"report/realtime/detail?dimension=#{dimension}#&dimensionCode=#{dimensionCode}#&color=#{color}#&site_list=#{site_list}#&work_area_list=#{work_area_list}#&line_area_list=#{line_area_list}#&source_type_list=#{source_type_list}#",

				//客户端界面
				CLIENT_WORK_ORDER_DATA:baseUrl+"ws/erp/site/#{site}#/worklistselect",
				PLC_CLIENT_WORK_ORDER_DATA:baseUrl+"ws/erp/site/#{site}#/worklistselect",
				CLIENT_GET_RESOURCE_INFO:baseUrl+"client/resource_operator_info?ip=#{ip}#&plcIp=#{plcIp}#",
				PLC_CLIENT_GET_RESOURCE_INFO:baseUrl+"client/resource_operator_info?ip=#{ip}#&plcIp=#{plcIp}#",
				CLIENT_CHANGE_ITEM:baseUrl+"client/site/#{site}#/change_item",
				CLIENT_ME_RECEIVE_ORDER:baseUrl+"ws/erp/site/#{site}#/mecreatelist",
				CLIENT_CHANGE_WORK_ORDER_STATU:baseUrl+"ws/erp/site/#{site}#/changestatus?IAUFNR=#{IAUFNR}#&IANDONUSER=#{IANDONUSER}#&ISTATUS=#{ISTATUS}#",
				PLC_CLIENT_CHANGE_WORK_ORDER_STATU:baseUrl+"ws/erp/site/#{site}#/changestatus?IAUFNR=#{IAUFNR}#&IANDONUSER=#{IANDONUSER}#&ISTATUS=#{ISTATUS}#&plcIp=#{plcIp}#",

				CLIENT_MAINTENANCE_RESULT_CONFIRMATION:baseUrl+"ws/erp/site/#{site}#/maintain",
				CLIENT_EDIT_WORK_ORDER:baseUrl+"ws/erp/site/#{site}#/editworklist?IUSER=#{IUSER}#&MODE=#{MODE}#&AUFNR=#{AUFNR}#",
				PLC_CLIENT_EDIT_WORK_ORDER:baseUrl+"ws/erp/site/#{site}#/editworklist?IUSER=#{IUSER}#&MODE=#{MODE}#&AUFNR=#{AUFNR}#&plcIp=#{plcIp}#",

				CLIENT_CREATE_FAULT_NOTICE:baseUrl+"ws/erp/site/#{site}#/faultlistcreate?QMART=#{QMART}#&QMTXT=#{QMTXT}#&EQUNR=#{EQUNR}#&QMNAM=#{QMNAM}#&QMDAT=#{QMDAT}#&MZEIT=#{MZEIT}#&ZSTATE=#{ZSTATE}#",
				PLC_CLIENT_CREATE_FAULT_NOTICE:baseUrl+"ws/erp/site/#{site}#/faultlistcreate?QMART=#{QMART}#&QMTXT=#{QMTXT}#&EQUNR=#{EQUNR}#&QMNAM=#{QMNAM}#&QMDAT=#{QMDAT}#&MZEIT=#{MZEIT}#&ZSTATE=#{ZSTATE}#&plcIp=#{plcIp}#",

				CALL_EAP_INTERFACE:baseUrl+'ws/erp/site/#{site}#/limitresource?Resource=#{Resource}#&limitCode=#{limitCode}#&plcIp=#{plcIp}#',
				DOWN_EXCEL_TEMPLATE_WORKORDER:baseUrlPrefix+'download/gzgsgd/template/site/{site}/excel',
				CHECK_EXCEL_DATA_WORKORDER:baseUrl+'ws/erp/site/#{site}#/excelcheck',
				UPLOAD_EXCEL_DATA_WORKORDER:baseUrl+'ws/erp/site/#{site}#/gzlistcreate',
				CLIENT_OPERATOR_LOGOUT:baseUrl+"client/operator_logout",
				CLIENT_ME_USER_LOGOUT:baseUrl+"client/me_user_logout",
				CLIENT_CHANGE_OPERATOR:baseUrl+"client/change_operator_or_me_user",
				CLIENT_CHANGE_LOCKED:baseUrl+"client/change_locked",
				CLIENT_CALL_RESOURCE_REASON_LOG:baseUrl+"client/resource_reason_log",

				//记录登入登出日志
				PLC_USER_LOG_INFO:baseUrl+"client/post_client_login_log",
			},

			getDescByExceptionCode: __getDescByExceptionCode,
			reloadPage: __reloadPage,
			getResponseStatusDesc : getResponseStatusDesc,
			reloadPage : __reloadPage,
 		};

 		/**
 		 * 
 		 * [__getSite 获取当前工厂]
 		 * @return {[type]} [description]
 		 * 
 		 */
 		function __getSite(){
 			return ROOTCONFIG.AndonConfig.SITE;
 		}

 		/**
 		 * 
 		 * [__handleCommenUrl 给传入的url添加 permactivity 和 permsite 请求参数]
 		 * @param  {[string]} url [需要处理的url]
 		 * @return {[string]}     [处理后的url]
 		 * 
 		 */
 		function __handleCommenUrl(url){
 			if(UtilsService.isEmptyString(url)){
 				return url;
 			}
 			var activity = ROOTCONFIG.AndonConfig.CURRENTACTIVITY;
 			if(!UtilsService.isEmptyString(activity) && url.indexOf("permactivity") < 0){
 				if(url.indexOf("?") > 0 && url.indexOf("?") != url.length-1){
 					url = url + "&permactivity="+activity;
 				}else{
 					url = url + "?permactivity="+activity;
 				}
 			}
 			if(url.indexOf("permsite=") < 0){
	        	if(url.indexOf("?") <= 0){
	        		url = url + "?permsite="+__getSite();
	        	}else{
	        		url = url + "&permsite="+__getSite();
	        	}
	        }
 			//console.log("url:"+url );
	        return url;
 		}

			/**
 		 * 
 		 * [get 公共get请求出口]
 		 * @param  {[object]} opt [请求参数对象]
 		 *                        {
 		 *                        	url: ''
 		 *                        }
 		 * @return {[promise]}    [promise]
 		 * 
 		 */
 		function get(opt){
 			var activity = ROOTCONFIG.AndonConfig.CURRENTACTIVITY;
 			var config = {
 				//Authorization: 'Basic YnssdjJlsdsdsdfsfsdfI',
                timeout: 300000
            };	
            var startTime = new Date().getTime();
 			var isTimeout = false; 
 			var isSessionOut = false;
	        var delay = $q.defer(); 
	        var url = opt.url;
			//UtilsService.log("url:"+url);
	        url = url.replace(/undefined/g,"").replace(/null/g,"");
	        url = __handleCommenUrl(url);
			UtilsService.log("url:"+url);
	        $http
	        	.get(url, config)
	        	.success(function (data, status, headers, config){
	        		// console.log("success  url: "+url);
	        		var endtime = new Date().getTime(); 
	        		isTimeout = (endtime - startTime) > config.timeout ? true : false; 
	        		if (status == 200) { 
		            	var returnDatas = {
		                	myHttpConfig: {
		                		isTimeout: isTimeout,
		                		isSessionOut: isSessionOut,
		                		status: status
		                	},	
		                    headers: headers,
		                    config: config,
		                    response: data 
		            	};
		            	// console.log(returnDatas); 
		                delay.resolve(returnDatas); 
		            }else if(status == 413){
		            	__reloadPage("当前用户权限已被更改,请重新登录!");
		            }else if(status == 401|| status == 403){
		            	__reloadPage("登录已过期,请重新登录!");
		            	// __reloadPage("当前 session 已超时,请重新登录!");
		            }else {
		            	var errorDatas = {
		            		myHttpConfig: {
		            			isTimeout: isTimeout,
		                		isSessionOut: isSessionOut,
		                		status: status,
		                		statusDesc: getResponseStatusDesc(status)
		            		},
		                    headers: headers,
		                    config: config,
		                    response: data
		                };
		                delay.reject(errorDatas);
		            }
	        	})
	        	.error(function (data, status, headers, config){
	        		if(status == 401 || status == 403){
	        			__reloadPage("登录已过期,请重新登录!");
	        			// __reloadPage("当前 session 已超时,请重新登录!");
	        		}else if(status == 413){
		            	__reloadPage("当前用户权限已被更改,请重新登录!");
		            }
	        		// if(config.response.indexOf("")){

	        		// }
					var errorMessage = headers("error-message");
					if(errorMessage){
						errorMessage = errorMessages[getResponseErrorCode(headers("error-message")).code];
					};
	        		var endtime = new Date().getTime();
	        		isTimeout = (endtime - startTime) > config.timeout ? true : false;
	        		var errorDatas = {
		        		myHttpConfig: {
		        			isTimeout: isTimeout,
		                	isSessionOut: isSessionOut,
		                	status: status,
		                	statusDesc: errorMessage?errorMessage:getResponseStatusDesc(status)
		        		},
		                headers: headers,
		                config: config,
		                response: data
		            };
		            delay.reject(errorDatas);
	        	});
	        return delay.promise;
 		}

 		var isRealoading = false;
 		/**
 		 * 
 		 * [__reloadPage alert提示用户信息并强制刷新界面]
 		 * @param  {[string]} msg [提示信息]
 		 * 
 		 */
 		function __reloadPage(msg){
 			if(isRealoading){
 				return;
 			}
 			isRealoading = true;
 			var promise = UtilsService.alert(msg);
 			promise.then(function(){
        		window.location.reload();
 			});
 		}

 		/**
 		 * 
 		 * [post 公共post请求出口]
 		 * @param  {[object]} opt [description]
 		 *                        {
 		 *                        	url: '',
 		 *                        	params: {}
 		 *                        }
 		 * @return {[promise]}    [promise]
 		 * 
 		 */
 		function post(opt) {
 			var activity = ROOTCONFIG.AndonConfig.CURRENTACTIVITY;
 			/*{ 
				url: '', 
				paras: { } 
 			}*/
 			var config = {
                timeout: 300000
            };
            var startTime = new Date().getTime();
 			var isTimeout = false;
 			var isSessionOut = false;
	        var delay = $q.defer();
	        //"_request_data=" + encodeURIComponent( $filter('json')({parameter: opt.para}) )
	        var url = opt.url;
	        url = __handleCommenUrl(url);
	        $http.post(
	            url,
	            opt.paras,
	            config
	        )
	        .success(function (data, status, headers, config) {
	        	var endtime = new Date().getTime();
	        	isTimeout = (endtime - startTime) > config.timeout ? true : false;
	            if (status == 200) { 
	            	var returnDatas = {
	                	myHttpConfig: {
	                		isTimeout: isTimeout,
	                		isSessionOut: isSessionOut,
	                		status: status
	                	},
	                    headers: headers,
	                    config: config,
	                    response: data 
	            	}; 
	            	// console.log(returnDatas); 
	                delay.resolve(returnDatas); 
	            }else if(status == 401 || status == 403){
	            	// __reloadPage("当前 session 已超时,请重新登录!");
	            	__reloadPage("登录已过期,请重新登录!");
	            }else if(status == 413){
	            	__reloadPage("当前用户权限已被更改,请重新登录!");
	            }else {
	            	var errorDatas = {
	            		myHttpConfig: {
	            			isTimeout: isTimeout,
	                		isSessionOut: isSessionOut,
	                		status: status,
		                	statusDesc: getResponseStatusDesc(status)
	            		},
	                    headers: headers,
	                    config: config,
	                    response: data
	                };
	                delay.reject(errorDatas);
	            } 
	        })
	        .error(function (data, status, headers, config) {
	        	if(status == 401 || status == 403){
	        		__reloadPage("登录已过期,请重新登录!");
        			// __reloadPage("当前 session 已超时,请重新登录!");
        		}else if(status == 413){
	            	__reloadPage("当前用户权限已被更改,请重新登录!");
	            }
				var errorMessage = headers("error-message");
				if(errorMessage){
					errorMessage = 	errorMessages[getResponseErrorCode(headers("error-message")).code];
				};
	        	var endtime = new Date().getTime();
	        	isTimeout = (endtime - startTime) > config.timeout ? true : false;
	        	var errorDatas = {
	        		myHttpConfig: {
	        			isTimeout: isTimeout,
	                	isSessionOut: isSessionOut,
	                	status: status,
		                statusDesc: errorMessage?errorMessage:getResponseStatusDesc(status),
						errorMessage:errorMessage?errorMessage:getResponseStatusDesc(status),
	        		},
	                headers: headers,
	                config: config,
	                response: data
	            };
	            delay.reject(errorDatas);
	        });
	        return delay.promise;
	    }

	    function sessionOutRefreshUrl(){
	    	var url = window.location.href.substring(0, window.location.href.indexOf("#"));
	    	window.location.reload();
	    }

	    /**
	     * 
	     * [getResponseErrorCode json字符串转为json对象]
	     * @param  {[type]} statusCode [description]
	     * @return {[type]}            [description]
	     * 
	     */
		function getResponseErrorCode(statusCode){
			UtilsService.log("错误信息："+statusCode);
			return eval("("+statusCode+")");
		};

		/**
		 * 
		 * [__getDescByExceptionCode 依据 error-code 来获取对应的错误描述]
		 * @param  {[int]} code 	[错误代码 error-code]
		 * @return {[string]}       [对应的错误描述]
		 * 
		 */
		function __getDescByExceptionCode(code){
			var desc = code;
			for(var key in errorMessages){
				if(key == code){
					return errorMessages[key];
				}
			}	
	    	return desc;
		}		

	    function getResponseStatusDesc(statusCode){
	    	var desc = "未知错误响应码: "+statusCode;
	    	for(var i = 0; i < responseStatus.length; i++){
	    		if(statusCode == responseStatus[i].code){
	    			return responseStatus[i].desc;
	    		}
	    	}
	    	return desc;
	    }



 	}]);


