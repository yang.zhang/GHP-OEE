plannedStopModule.service('plannedStopService', [
    'HttpAppService',
    function (HttpAppService) {
        return{
            planStopReasonCode : reasonCode,
            planStopQueryData : queryData,
            planStopQueryDataCount : queryDataCount,
            planStopVerificationData : verificationData,
            planStopKeepData :keepData,
            requestDataResourceCodesByLine : requestDataResourceCodesByLine
        }
        function reasonCode(){
            var url = HttpAppService.URLS.STOP_PLAN_REASON_LIST
                    .replace("#{SITE}#", HttpAppService.getSite())
                ;
            return HttpAppService.get({
                url: url
            });
        };
        function queryData(resrce,time,work_center,line,index,count,resource_type){
            var url = HttpAppService.URLS.STOP_PLAN_QUERY_LIST
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{resrce}#", resrce)
                    .replace("#{time}#", time)
                    .replace("#{work_center}#", work_center)
                    .replace("#{line}#", line)
                    .replace("#{index}#", index)
                    .replace("#{count}#", count)
                    .replace("#{resource_type}#", resource_type)
                    ;
            return HttpAppService.get({
                url: url
            });
        };
        function queryDataCount(resrce,time,work_center,line,resource_type){
            var url = HttpAppService.URLS.STOP_PLAN_QUERY_LIST_COUNT
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{resrce}#", resrce)
                    .replace("#{time}#", time)
                    .replace("#{work_center}#", work_center)
                    .replace("#{line}#", line)
                    .replace("#{resource_type}#", resource_type)
                ;
            return HttpAppService.get({
                url: url
            });
        };
        function verificationData(resrce,start_date_time,end_date_time,handle){
            var url = HttpAppService.URLS.STOP_PLAN_VERIFICATION_DATA
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{resrce}#", resrce)
                    .replace("#{start_date_time}#", start_date_time)
                    .replace("#{end_date_time}#", end_date_time)
                    .replace("#{handle}#", handle)

                ;
            return HttpAppService.get({
                url: url
            });
        };
        function keepData(paramActions){
            var url = HttpAppService.URLS.STOP_PLAN_KEEP_DATA;
            return HttpAppService.post({
                url: url,
                paras: paramActions
            });
        };
        function requestDataResourceCodesByLine(workCenter, line, resourceType){
            var url = HttpAppService.URLS.XLZ_RESOURCE_CODES_BYLINES_DESC
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{WORK_CENTER}#", workCenter)
                    .replace("#{LINE}#", line)
                    .replace("#{RESOURCE_TYPE}#", resourceType)
                ;
            return HttpAppService.get({
                url: url
            });
        }
    }]);

plannedStopModule
    .run([
        '$templateCache',
        function ($templateCache){
            $templateCache.put('andon-ui-grid-tpls/planStopStart',
                "<div ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><form name=\"inputForm\"><input type=\"datetime-local\" ng-class=\"'colt' + col.uid\" ui-grid-edit-dropdown ng-model=\"row.entity.startDateTimeIns\"></form></div>"
            );
            $templateCache.put('andon-ui-grid-tpls/planStopEnd',
                "<div ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><form name=\"inputForm\"><input type=\"datetime-local\" ng-class=\"'colt' + col.uid\" ui-grid-edit-dropdown ng-model=\"row.entity.endDateTimeIns\" ></form></div>"
            );
            $templateCache.put('andon-ui-grid-tpls/planStopReason',
                "<div ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><form name=\"inputForm\"><select ng-class=\"'colt' + col.uid\" ui-grid-edit-dropdown ng-model=\"row.entity.reasonCode\" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] CUSTOM_FILTERS for field in editDropdownOptionsArray\"></select></form></div>"
            );
        }]);










    