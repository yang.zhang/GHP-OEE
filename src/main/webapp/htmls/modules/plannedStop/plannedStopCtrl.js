plannedStopModule.controller('plannedStopCtrl',['$scope', '$http',
    'HttpAppService', '$timeout',
    'uiGridConstants','$uibModal', 'plcService','UtilsService', 'gridUtil', 'i18nService', 'uiGridValidateService',
    'uiGridCellNavConstants','plannedStopService','$q','$filter','$state','plannedStopService',
    function ($scope, $http, HttpAppService, $timeout,
              uiGridConstants,$uibModal, plcService,UtilsService, gridUtil, i18nService, uiGridValidateService,
              uiGridCellNavConstants,plannedStopService,$q,$filter,$state,plannedStopService) {

        $scope.config = { 
            gridApi: null, 

            $workCenterSelect: null,
            $lineSelect: null,
            $deviceTypeSelect: null,
            $deviceNumSelect: null,

            btnDisabledQuery: true,
            btnDisabledDelete: true,
            btnDisabledAdd: true,
            btnDisabledSave: true,
            btnDisabledEdit : true,

            defaultSelectItem: { descriptionNew:null, description:null, workCenter: null ,resourceType:null},
            workCenters: [],
            currentWorkCenter: null,
            lines: [],
            currentLine: [],
            deviceTypes: [],
            deviceTypesCopy : [],
            currentDeviceType: null,
            deviceNums: [],
            currentDeviceNums: [],

            queryManDate : UtilsService.serverFommateDateShow(new Date()),
            reasoncode : [],
            currentReasonCode : null,
            queryEditDateStart : UtilsService.serverFommateDateTimeShow(new Date()),
            queryEditDateEnd : UtilsService.serverFommateDateTimeShow(new Date()),

            deletedRows : [],
            editRW : true,
            // canEdit: false,
            showDeviceCode : true,
            showSelectLine : true
        };

        $scope.config.editRW = $scope.modulesRWFlag("#"+$state.$current.url.source);
        // $scope.config.canEdit = $scope.modulesRWFlag("#"+$state.$current.url.source);

        //拉线多选帮助
        $scope.toWireLineHelp = function () {
            if($scope.config.currentWorkCenter == null){
                $scope.addAlert('','请先选择生产区域');
                return;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/lineSelectModal.html',
                controller: 'LineSelectMoreCtrl',
                size: 'lg',
                backdrop: 'false',
                scope: $scope,
                openedClass: 'flex-center-parent',
                resolve: {
                    selectLineModal: function () {
                        return {workLine: $scope.config.lines};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                console.log(selectedItem);
                $scope.config.currentLine = selectedItem.lineDatas;
                $scope.config.currentLineDesc = selectedItem.lineName;
                if($scope.config.currentLine.length >0){
                    $scope.config.showSelectLine = false;
                }
                $scope.lineChanged();
            }, function () {
            });
        };
        $scope.deleteAll = function(item){
            if(item == 'line'){
                $scope.config.currentLine = [];
                $scope.config.currentLineDesc = [];
                $scope.config.showSelectLine = true;
                $scope.config.deviceNums = [];
                $scope.config.currentDeviceNums = [];
            }
            if(item == 'deviceCode'){
                $scope.config.currentDeviceNumsShow = '';
                $scope.config.currentDeviceNums = [];
                $scope.config.showDeviceCode = true;
            }
        }
        //设备帮助
        $scope.toDeviceHelp = function(){
            if($scope.config.currentWorkCenter == null){
                $scope.addAlert('','请先选择生产区域');
                return;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/noReportModalResourceHelp.html',
                controller: 'noReportDeviceMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectMoreDevice: function () {
                        return {deviceCode : $scope.config.deviceNums};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.currentDeviceNumsShow = selectedItem.area.join(",");
                $scope.config.currentDeviceNums = selectedItem.selectdDatas;
                if($scope.config.currentDeviceNumsShow.length >0){
                    $scope.config.showDeviceCode = false;
                }
            }, function () {
            });
        };
        $scope.plannedStopTitle = '计划停机维护';
        //表格的配置
        $scope.gridPlannedStop = {
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes, //[5, 10, 15, 20, 50, 100],
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize, //10,
            useExternalPagination: true,
            paginationCurrentPage: 1,
            totalItems: 0,
            showGridFooter: false,
            modifierKeysToMultiSelectCells: false,
            columnDefs:[ {
                field: 'handle', name: 'handle', visible: false
            },
                {
                    field: 'isSelected', name: 'isSelected', displayName: '选择', minWidth: 60, maxWidth: 60,
                    enableCellEdit: true, type: 'boolean', visible: false,
                    enableColumnMenu: false, enableColumnMenus: false, enableSorting: false
                },
                {
                    field: 'workCenterDes', name: 'workCenterDes',
                    displayName: '生产区域', minWidth: 80,
                    enableCellEdit: true, enableCellEditOnFocus: false,
                    validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'lineDes', name: 'lineDes', displayName: '拉线', minWidth: 125,
                    enableCellEdit: true, enableCellEditOnFocus: false,
                    validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'resrce', name: 'resrce', displayName: '设备编码', minWidth: 90,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'asset', name: 'asset', displayName: '资产号', minWidth: 70,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'resrceDes', name: 'resrceDes', displayName: '设备名称', minWidth: 100,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'startDateTimeIn', name: 'startDateTimeIn', displayName: '计划开始时间', minWidth: 150,
                    enableCellEdit: $scope.config.editRW, enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    editableCellTemplate: 'andon-ui-grid-tpls/planStopStart',
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row,
                            col = arguments[0].col;
                        var nowDate = new Date().getTime();
                        row.entity.startDateTimeIns = new Date(row.entity.startDateTimeIn);
                        row.entity.endDateTimeIns  = new Date(row.entity.endDateTimeIn);

                        if((new Date(row.entity.startDateTimeIn)).getTime() > nowDate ){
                            return true;
                        }else{
                            return false;
                        }
                    }
                },{
                    field: 'endDateTimeIn', name: 'endDateTimeIn', displayName: '计划结束时间', minWidth: 150,
                    enableCellEdit: $scope.config.editRW, enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    editableCellTemplate: 'andon-ui-grid-tpls/planStopEnd',
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row,
                            col = arguments[0].col;
                        row.entity.startDateTimeIns = new Date(row.entity.startDateTimeIn);
                        row.entity.endDateTimeIns  = new Date(row.entity.endDateTimeIn);
                        var nowDate = (new Date()).getTime();
                        console.log((new Date(row.entity.startDateTimeIn)).getTime());
                        console.log(nowDate);
                        if((new Date(row.entity.startDateTimeIn)).getTime() > nowDate ){
                            return true;
                        }else{
                            return false;
                        }
                    }
                },{
                    field: 'reasonCodeDes', name: 'reasonCodeDes', displayName: '原因代码', minWidth: 80,
                    enableCellEdit: $scope.config.editRW, enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    // editableCellTemplate: 'andon-ui-grid-tpls/planStopReason',
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row,
                            col = arguments[0].col;
                        row.entity.startDateTimeIns = new Date(row.entity.startDateTimeIn);
                        row.entity.endDateTimeIns  = new Date(row.entity.endDateTimeIn);
                        var nowDate = (new Date()).getTime();
                        if((new Date(row.entity.startDateTimeIn)).getTime() > nowDate ){
                            return true;
                        }else{
                            return false;
                        }
                    },
                    editDropdownIdLabel: 'reasonCodeDes', editDropdownValueLabel: 'descriptionNew',
                    editDropdownOptionsFunction: function(rowEntity, colDef){
                        var results = [];
                        for(var i = 0; i < $scope.config.reasoncode.length; i++){
                            var item = $scope.config.reasoncode[i];
                            results.push({
                                reasonCode: item.reasonCode,
                                reasonCodeDes: item.description,
                                descriptionNew: "代码:" + item.reasonCode + "  描述:" + item.description
                            });
                        }
                        return results;
                    }
                },
                {
                    field: 'isDeleted', name: 'isDeleted', displayName: '删除', width: 60,
                    enableCellEdit: false, enableCellEditOnFocus: false,visible: $scope.config.editRW,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return "删除该行"; },
                    cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col"><span class="ion-trash-a"></span></div>',
                    headerTemplate: '<div class="xbr-delete-col-header">删除</div>'
                }],
            data : [],
            onRegisterApi: __onRegisterApi
        };
        //表格的方法
        function __onRegisterApi(gridApi){
            $scope.config.gridApi = gridApi;
            // 分页相关
            $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                // console.log("刷新界面 第 "+currentPage+" 页,每页 "+pageSize+"个元素!");
                $scope.bodyConfig.overallWatchChanged = false;
                __requestTableDatas(currentPage, pageSize);
                return true;
            }, $scope);
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
                if(newRowCol.col.field == "isDeleted" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                    __checkCanDeleteData(newRowCol);
                }
            });
            gridApi.cellNav.on.viewPortKeyDown($scope, function(row, col){
                console.log("viewPortKeyDown");
            });
            gridApi.cellNav.on.viewPortKeyPress($scope, function(row, col){
                console.log("viewPortKeyPress");
            });
            $scope.config.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
                $scope.config.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
            });
            $scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                if(new Date(rowEntity.startDateTimeIn) > rowEntity.startDateTimeIns || new Date(rowEntity.startDateTimeIn) < rowEntity.startDateTimeIns
                || new Date(rowEntity.endDateTimeIn) > rowEntity.endDateTimeIns || new Date(rowEntity.endDateTimeIn) < rowEntity.endDateTimeIns){
                    rowEntity.hasChanged = true;
                }


                $scope.bodyConfig.overallWatchChanged = true;
                var promise = $q.defer();
                if($scope.config.gridApi.rowEdit){
                    $scope.config.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
                }
                //if(oldValue == newValue){
                //    promise.resolve();
                //    console.log("1212")
                //    return;
                //}
                if(colDef.field === "reasonCodeDes" || colDef.name === "reasonCodeDes"){
                    rowEntity.reasonCode = __getReasonCodeByDesc(newValue);
                }

                if(colDef.field === "startDateTimeIn" || colDef.name === "startDateTimeIn"){
                    promise.reject();
                    var nowDate = new Date().getTime();
                    var startTime = new Date(rowEntity.startDateTimeIns).getTime();
                    var endTime = new Date(rowEntity.endDateTimeIn).getTime();
                    if(startTime >= endTime){
                        $scope.addAlert("","计划结束时间早于等于计划开始时间，不允许修改");
                        uiGridValidateService.setInvalid(rowEntity, colDef);
                        rowEntity.plcObjectIsValid = false;
                        return;
                    }
                    if(startTime <= nowDate){
                        $scope.addAlert("","不能修改计划开始时间早于等于当天日期的停机记录");
                        uiGridValidateService.setInvalid(rowEntity, colDef);
                        rowEntity.plcObjectIsValid = false;
                        return;
                    }
                    for(var i=0;i<$scope.gridPlannedStop.data.length;i++){
                        if($scope.gridPlannedStop.data[i].workCenter == rowEntity.workCenter && $scope.gridPlannedStop.data[i].resrce == rowEntity.resrce && $scope.gridPlannedStop.data[i].$$hashKey != rowEntity.$$hashKey){
                            if(new Date($scope.gridPlannedStop.data[i].startDateTimeIn).getTime() >= endTime || new Date($scope.gridPlannedStop.data[i].endDateTime).getTime() <= startTime){
                            }else{
                                $scope.addAlert("",$scope.gridPlannedStop.data[i].resrce+"存在重叠的计划期间，不允许修改");
                                uiGridValidateService.setInvalid(rowEntity, colDef);
                                rowEntity.plcObjectIsValid = false;
                                return;
                            }
                        }
                    }
                    rowEntity.startDateTimeIn = UtilsService.serverFommateDateTimeShow(rowEntity.startDateTimeIns);
                    rowEntity.endDateTimeIn = UtilsService.serverFommateDateTimeShow(rowEntity.endDateTimeIns);
                    if(rowEntity.changeStart == 'old'){
                        rowEntity.changeStart = "D";
                    }
                }
                if( colDef.field === "endDateTimeIn" || colDef.name === "endDateTimeIn"){
                    promise.reject();
                    var nowDate = new Date().getTime();
                    var startTime = new Date(rowEntity.startDateTimeIn).getTime();
                    var endTime = new Date(rowEntity.endDateTimeIns).getTime();
                    if(startTime >= endTime){
                        $scope.addAlert("","计划结束时间早于等于计划开始时间，不允许修改");
                        uiGridValidateService.setInvalid(rowEntity, colDef);
                        rowEntity.plcObjectIsValid = false;
                        return;
                    }
                    if(startTime <= nowDate){
                        $scope.addAlert("","不能修改计划开始时间早于等于当天日期的停机记录");
                        uiGridValidateService.setInvalid(rowEntity, colDef);
                        rowEntity.plcObjectIsValid = false;
                        return;
                    }
                    for(var i=0;i<$scope.gridPlannedStop.data.length;i++){
                        if($scope.gridPlannedStop.data[i].workCenter == rowEntity.workCenter && $scope.gridPlannedStop.data[i].resrce == rowEntity.resrce && $scope.gridPlannedStop.data[i].$$hashKey != rowEntity.$$hashKey){
                            if(new Date($scope.gridPlannedStop.data[i].startDateTimeIn).getTime() >= endTime || new Date($scope.gridPlannedStop.data[i].endDateTime).getTime() <= startTime){
                            }else{
                                $scope.addAlert("", $scope.gridPlannedStop.data[i].resrce+"存在重叠的计划期间，不允许修改");
                                uiGridValidateService.setInvalid(rowEntity, colDef);
                                rowEntity.plcObjectIsValid = false;
                                return;
                            }
                        }
                    }
                    rowEntity.startDateTimeIn = UtilsService.serverFommateDateTimeShow(rowEntity.startDateTimeIns);
                    rowEntity.endDateTimeIn = UtilsService.serverFommateDateTimeShow(rowEntity.endDateTimeIns);
                }
                var resrce = rowEntity.reasonCode,
                    start_date_time = UtilsService.serverFommateDateTime(new Date(rowEntity.startDateTimeIn)),
                    end_date_time = UtilsService.serverFommateDateTime(new Date(rowEntity.endDateTimeIn)),
                    handle = rowEntity.handle;
                plannedStopService.planStopVerificationData(resrce,start_date_time,end_date_time,handle).then(function (resultDatas){
                    console.log(resultDatas);
                    if(resultDatas.response){
                        $scope.addAlert('danger', '存在冲突,不可以使用!');
                        uiGridValidateService.setInvalid(rowEntity, colDef);
                        rowEntity.plcObjectIsValid = false;
                        promise.reject();
                    }else{
                        console.log("不存在冲突，可以使用");
                        promise.resolve();
                        rowEntity.hasChanged = true;
                        rowEntity.plcObjectIsValid = true;
                        uiGridValidateService.setValid(rowEntity, colDef);
                    }
                }, function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });

            });
            //选择相关
            $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                gridRow.entity.isSelected = gridRow.isSelected;
            });
            $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                for(var i = 0; i < gridRows.length; i++){
                    gridRows[i].entity.isSelected = gridRows[i].isSelected;
                }
            });
        };
        //原因代码
        function __getReasonCodeByDesc(desc){
            if(UtilsService.isEmptyString(desc)){
                return '';
            }
            for(var i = 0; i < $scope.config.reasoncode.length; i++){
                if(desc == $scope.config.reasoncode[i].description){
                    return $scope.config.reasoncode[i].reasonCode;
                }
            }
        };
        //删除
        function __checkCanDeleteData(newRowCol){
            var entity = newRowCol.row.entity;
            var handle = entity.handle;
            var hashKey = entity.$$hashKey;
            if(angular.isUndefined(handle) || handle==null || handle==""){ // 处理新增行
                __deleteRowDataByHashKey(hashKey);
                return;
            }
            // 处理历史行，需要检验是否已经被引用
            if(new Date(entity.startDateTimeIn).getTime() < new Date().getTime()){ // 不允许删除
                entity.canDeleted = false;
                $scope.addAlert('danger', "停机计划已发生,不允许删除!");
            }else{
                entity.canDeleted = true;
                __deleteRowDataByHashKey(hashKey);
            }
        };

        function __deleteRowDataByHashKey(hashKey){
            $scope.bodyConfig.overallWatchChanged = true;
            for(var i = 0; i < $scope.gridPlannedStop.data.length; i++){
                if(hashKey == $scope.gridPlannedStop.data[i].$$hashKey){
                    $scope.config.deletedRows.push($scope.gridPlannedStop.data[i]);
                    $scope.gridPlannedStop.data = UtilsService.removeArrayByIndex($scope.gridPlannedStop.data, i);
                    break;
                }
            }
        };
        //摸态框
        $scope.modalPlannedStop = function(name){
            var deviceNums = [];
            $scope.bodyConfig.overallWatchChanged = true;
            if($scope.config.currentDeviceNums.length > 0){
                for(var i = 0; i < $scope.config.deviceNums.length; i++){
                    for(var j=0;j<$scope.config.currentDeviceNums.length;j++){
                        if($scope.config.currentDeviceNums[j].resrce == $scope.config.deviceNums[i].resrce ){
                            deviceNums.push($scope.config.deviceNums[i]);
                        }
                    }
                }
            }else{
                deviceNums = $scope.config.deviceNums;
            }

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'ModalPlannedStop.html',
                controller: 'ModalPlannedStopCtrl',
                size: 'lg',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope : $scope,
                resolve: {
                    planStopdata: function () {
                        return {
                            deviceNums:  deviceNums,
                            reasoncode:  $scope.config.reasoncode,
                            name : name,
                            allData : $scope.gridPlannedStop.data,
                            workCenter : $scope.config.currentWorkCenter.workCenter
                        }
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                for(var i=0;i<selectedItem.length;i++){
                    $scope.gridPlannedStop.data.unshift({
                        hasChanged: true,
                        endDateTimeIn : selectedItem[i].endDate,
                        line : selectedItem[i].deviceCode.lineArea,
                        lineDes : selectedItem[i].deviceCode.lineAreaDes,
                        modifiedDateTime : "",
                        reasonCode : selectedItem[i].reasonCode.reasonCode,
                        resourceBo : "ResourceBO:" + HttpAppService.getSite()+"," + selectedItem[i].deviceCode.resrce,
                        resrce : selectedItem[i].deviceCode.resrce,
                        resrceDes : selectedItem[i].deviceCode.description,
                        site : HttpAppService.getSite(),
                        startDateTimeIn : selectedItem[i].startDate,
                        workCenter : $scope.config.currentWorkCenter.workCenter,
                        workCenterDes : $scope.config.currentWorkCenter.description,
                        asset : selectedItem[i].deviceCode.asset,
                        reasonCodeDes : selectedItem[i].reasonCode.description
                    });
                }
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.modalSupplement = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'ModalSupplement.html',
                controller: 'ModalSupplementCtrl',
                size: 'lg',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope : $scope,
                planStopdata: {
                    deviceNums: function () {
                        return {
                            deviceNums:  $scope.config.deviceNums,
                            reasoncode:  $scope.config.reasoncode
                        }
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                console.log(selectedItem);
                $scope.gridPlannedStop.data.push(selectedItem);
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };
//初始化
        $scope.init = function(){
            __initDropdowns('init');
            __requestQueryReasonCode();
            __requestDropdownWorkCenters();
            __requestDropdownDeviceTypes();
        };

        $scope.init();

        function __initDropdowns(type){
            // $scope.config.workCenters = [];
            // $scope.config.workCenters.push($scope.config.defaultSelectItem);
            // $scope.config.currentWorkCenter = $scope.config.workCenters[0];

            if(type == 'init'){
                $scope.config.workCenters = [];
                $scope.config.workCenters.push($scope.config.defaultSelectItem);
                $scope.config.deviceTypes = [];
                $scope.config.deviceTypes.push($scope.config.defaultSelectItem);
            }

            if(type == 'init' || type == 'workCenterChanged'){
                $scope.config.lines = [];
                $scope.config.currentLine = [];
                //$scope.config.lines.push($scope.config.defaultSelectItem);
                //console.log($scope.config.lines);
                //$scope.config.currentLine = $scope.config.lines[0];

                $scope.config.deviceNums = [];
                $scope.config.currentDeviceNums = [];
                $scope.deleteAll("line");
                $scope.deleteAll("deviceCode");
            }

            if(type == 'lineChanged'){
                $scope.config.deviceNums = [];
                $scope.config.currentDeviceNums = [];
                $scope.deleteAll("deviceCode");
            }

            if(type == 'deviceTypeChanged'){
                $scope.config.deviceNums = [];
                $scope.config.currentDeviceNums = [];
                $scope.deleteAll("deviceCode");
            }
        };

        $scope.tipSelectClick = function(type){
            if(type == 'line'){
                $scope.config.$workCenterSelect.toggle();
            }else if(type == 'deviceNum'){
                $scope.config.$workCenterSelect.toggle();
            }
        };

        /////   生成区域、级联选择
        $scope.workCenterChanged = function(){
            //if($scope.config.currentWorkCenter == null || $scope.config.currentWorkCenter.workCenter==null){
            //    __initDropdowns('workCenterChanged');
            //}else{
            //
            //
            //}
            __initDropdowns('workCenterChanged');
            var workCenter = $scope.config.currentWorkCenter.workCenter;
            __requestDropdownLines(workCenter);
            var line = [];
            for(var i=0;i<$scope.config.currentLine.length;i++){
                line.push($scope.config.currentLine[i].workCenter);
            }
            var resourceType = $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:'';
            __requestDropdownDeviceCodes(workCenter, line, resourceType);
            __setBtnsDisable();
        };

        $scope.queryManDateChange = function(){
            __setBtnsDisable();
        };
//按钮控制
        function __setBtnsDisable(){

            if(!$scope.config.currentWorkCenter){
                $scope.config.btnDisabledSave = true;
                return;
            }else{
                $scope.config.btnDisabledSave = false;
            }
            if(!$scope.config.currentWorkCenter
                || !$scope.config.currentDeviceType
                || $scope.config.queryManDate == null
            ){
                $scope.config.btnDisabledQuery = true;
                $scope.config.btnDisabledDelete = true;
                $scope.config.btnDisabledAdd = true;
                return;
            }else{
                $scope.config.btnDisabledQuery = false;
                $scope.config.btnDisabledDelete = false;
                $scope.config.btnDisabledAdd = false;
            }
        };
//拉线
        $scope.lineChanged = function(){
            //if($scope.config.currentLine == null || $scope.config.currentLine.workCenter==null){
            //    __initDropdowns('lineChanged');
            //}else{
            //
            //}
            __initDropdowns('lineChanged');
            var workCenter = $scope.config.currentWorkCenter.workCenter;
            var line = [];
            for(var i=0;i<$scope.config.currentLine.length;i++){
                line.push($scope.config.currentLine[i].workCenter);
            }
            var resourceType = null;
            if($scope.config.currentDeviceType){
                resourceType = $scope.config.currentDeviceType.resourceType;
            }


            __requestDropdownDeviceCodes(workCenter, line, resourceType);
            //__setBtnsDisable();
        };
//设备类型
        $scope.deviceTypeChanged = function(){
            //$scope.config.deviceTypes = angular.copy($scope.config.deviceTypesCopy);
            //if($scope.config.currentDeviceType == null || $scope.config.currentDeviceType.resourceType==null){
            //    __initDropdowns('deviceTypeChanged');
            //}else{
            //
            //}

            __initDropdowns('deviceTypeChanged');
            var workCenter = $scope.config.currentWorkCenter.workCenter;
            var line = [];
            for(var i=0;i<$scope.config.currentLine.length;i++){
                line.push($scope.config.currentLine[i].workCenter);
            }
            var resourceType = $scope.config.currentDeviceType.resourceType;

            __requestDropdownDeviceCodes(workCenter, line, resourceType);
            __setBtnsDisable();
        };

        function __requestDropdownWorkCenters(){
            plcService
                    .dataWorkCenters()
                    .then(function (resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            $scope.config.workCenters = [];
                            // $scope.config.workCenters.push($scope.config.defaultSelectItem);
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].workCenter+"   描述:"+resultDatas.response[i].description;
                                $scope.config.workCenters.push(resultDatas.response[i]);
                            }
                            // $scope.config.workCenters = $scope.config.workCenters.concat(resultDatas.response);
                            return;
                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert("danger",resultDatas.myHttpConfig.statusDesc);
                    });
        };

        function __requestDropdownLines(workCenter){
            plcService
                    .dataLines(workCenter)
                    .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].workCenter+"   描述:"+resultDatas.response[i].description;
                                $scope.config.lines.push(resultDatas.response[i]);
                            }
                            //$scope.config.$lineSelect.toggle();
                            // $scope.config.lines = $scope.config.lines.concat(resultDatas.response);
                            return;
                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    });
        };

        function __requestDropdownDeviceTypes(){
            //console.log($scope.config.deviceTypes);
            plcService
                    .dataResourceTypes()
                    .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].resourceType+"   描述:"+resultDatas.response[i].description;
                                $scope.config.deviceTypes.push(resultDatas.response[i]);
                            }
                        //$scope.config.deviceTypesCopy = angular.copy($scope.config.deviceTypes);
                            // $scope.config.deviceTypes = $scope.config.deviceTypes.concat(resultDatas.response);
                        //console.log($scope.config.deviceTypes);

                        return;
                        }
                    },function (resultDatas){ //TODO 检验失败
                    if(resultDatas.myHttpConfig.isSessionOut == true || resultDatas.myHttpConfig.isTimeout == true || resultDatas.myHttpConfig.status == 1 || resultDatas.myHttpConfig.status == -1){
                    }else{
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    }
                });
        };

        function __requestDropdownDeviceCodes(workCenter, line, resourceType){
            plannedStopService
                    .requestDataResourceCodesByLine(workCenter, line, resourceType)
                    .then(function (resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i=0;i<resultDatas.response.length;i++){
                            resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].resrce+"   描述:"+resultDatas.response[i].description;
                            $scope.config.deviceNums.push(resultDatas.response[i]);
                            $scope.config.btnDisabledAdd = false;}
                            // $scope.config.deviceNums = $scope.config.deviceNums.concat(resultDatas.response);
                            return;
                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    });
        };
//查询数据
        function __requestQueryDataCount(){
            // var resource = $scope.config.currentDeviceNum.resrce;
            var resrce = [];
            for(var i = 0; i < $scope.config.currentDeviceNums.length; i++){
                resrce.push($scope.config.currentDeviceNums[i].resrce);
            }
            var work_center = $scope.config.currentWorkCenter.workCenter;
            var line = [];
            for(var i=0;i<$scope.config.currentLine.length;i++){
                line.push($scope.config.currentLine[i].workCenter);
            }
            var index = 1;
            var count = $scope.gridPlannedStop.paginationPageSize;
            var time = UtilsService.serverFommateDate(new Date($scope.config.queryManDate));
            var deviceTypes = $scope.config.currentDeviceType.resourceType;
            $scope.gridPlannedStop.data = [];
            plannedStopService
                .planStopQueryData(resrce,time,work_center,line,index,count,deviceTypes)
                .then(function (resultDatas){
                    //console.log(resultDatas);
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.gridPlannedStop.data = resultDatas.response;
                        for(var i=0;i<$scope.gridPlannedStop.data.length;i++){
                            $scope.gridPlannedStop.data[i].startDateTimeIn = UtilsService.serverStringFormatDateTime($scope.gridPlannedStop.data[i].startDateTimeIn);
                            $scope.gridPlannedStop.data[i].endDateTimeIn = UtilsService.serverStringFormatDateTime($scope.gridPlannedStop.data[i].endDateTimeIn);
                            $scope.gridPlannedStop.data[i].changeStart = "old";
                        }
                        // $scope.config.deviceTypes = angular.copy($scope.config.deviceTypesCopy);
                        return;
                    }else{
                        $scope.addAlert("","暂无数据");
                    }
                }, function (errorResultDatas){
                    $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
                });
        };
        //原因代码
        function __requestQueryReasonCode(){
            plannedStopService.planStopReasonCode().then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.config.reasoncode = [];
                    for(var i = 0; i < resultDatas.response.length; i++){
                        resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].reasonCode+"   描述:"+resultDatas.response[i].description;
                        $scope.config.reasoncode.push(resultDatas.response[i]);
                    }
                    return;
                }
            }, function (resultDatas){
                if(resultDatas.myHttpConfig.isSessionOut == true || resultDatas.myHttpConfig.isTimeout == true || resultDatas.myHttpConfig.status == 1 || resultDatas.myHttpConfig.status == -1){
                }else{
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                }
            });
        };
        //删除
        $scope.checkAndDeleteSelectedRows = function(){
            var deleteArrY = [];
            var deleteArrN = [];
            var deleteIndexArrN = [];
            for(var i=0;i<$scope.gridPlannedStop.data.length;i++){
                if($scope.gridPlannedStop.data[i].isSelected && new Date($scope.gridPlannedStop.data[i].startDateTimeIn) <= new Date()){
                    deleteArrN.push($scope.gridPlannedStop.data[i]);
                    deleteIndexArrN.push(i+1);
                    $scope.gridPlannedStop.data[i].canDeleted = false;
                }else if($scope.gridPlannedStop.data[i].isSelected && new Date($scope.gridPlannedStop.data[i].startDateTimeIn) >= new Date()){
                    deleteArrY.push($scope.gridPlannedStop.data[i]);
                    $scope.gridPlannedStop.data[i].canDeleted = true;
                }else{

                }
            }

            if(deleteIndexArrN.length > 0){
                //for(var i=0;i<deleteArrY.length;i++){
                //    $scope.addAlert("","停机计划已发生,不允许删除");
                // return;
                for(var x = 0; x < deleteIndexArrN.length; x++){
                    $scope.addAlert("danger","第"+deleteIndexArrN[x]+"行: 停机计划已发生,不允许删除");
                }
                return;
                //}
            }
            __chekForDeleteRows();
        };

        function __chekForDeleteRows(){
            var canDeletes = true;
            var deletedRowIndexs = [];
            var rows = $scope.gridPlannedStop.data;
            for(var i = 0; i < rows.length; i++){
                var row = rows[i];
                if(row.isSelected){
                    if(row.canDeleted !== true){
                        canDeletes = false;
                    }
                }
            }
            if(canDeletes){
                for (var i = rows.length - 1; i >= 0; i--) {
                    var row = rows[i];
                    if(row.isSelected){
                        row.isDeleted = true;
                        deletedRowIndexs.push(i);
                        $scope.config.deletedRows.push(angular.copy(row));
                    }
                };
            }else{
                return;
            };
            var length = deletedRowIndexs.length;
            for(var i = 0; i < length; i++){
                $scope.gridPlannedStop.data.splice(deletedRowIndexs[i],1);
                // $scope.virtualGridOptions.data = UtilsService.removeArrayByIndex($scope.virtualGridOptions.data, deletedRowIndexs[0]);
            }
            $scope.bodyConfig.overallWatchChanged = true;
            //$scope.config.btnDisabledDelete = true;
        };
//保存操作
        $scope.saveAllAcitons = function(){
            $scope.showBodyModel("正在保存");
            if(!__checkCanSaveDatas()){
                $scope.hideBodyModel();
                return;
            };
            var dataCopy = angular.copy($scope.gridPlannedStop.data);
            console.log($scope.gridPlannedStop.data);
            var addArr = [];
            for(var i = 0; i < $scope.gridPlannedStop.data.length; i++) {
                var row = $scope.gridPlannedStop.data[i];
                if(row.changeStart == "D"){
                    addArr.push(row);
                    $scope.config.deletedRows.push(row);
                    $scope.gridPlannedStop.data = UtilsService.removeArrayByIndex($scope.gridPlannedStop.data, i);
                }
            }
            for(var i=0;i<addArr.length;i++){
                var row = addArr[i];
                $scope.gridPlannedStop.data.push({
                    //"handle" : row.handle,
                    "site" : HttpAppService.getSite(),
                    "resrce": row.resrce,
                    "resourceBo": row.resourceBo,
                    //"strt": UtilsService.serverFommateDateTime(new Date(row.startDateTimeIn)),
                    "resrceDes": row.resrceDes,
                    "workCenter": row.workCenter,
                    "workCenterDes": row.workCenterDes,
                    "line": row.line,
                    "lineDes": row.lineDes,
                    "startDateTimeIn":  row.startDateTimeIn,
                    "endDateTimeIn": row.endDateTimeIn,
                    "reasonCode": row.reasonCode,
                    //"modifiedDateTime": row.modifiedDateTime,
                    "viewActionCode" : 'C',
                    "hasChanged" : true
                })
            }
            var paramActions = [];
            for(var i = 0; i < $scope.gridPlannedStop.data.length; i++){
                console.log("--");
                var row = $scope.gridPlannedStop.data[i];
                if(row.hasChanged){
                    // var viewActionCode = angular.isUndefined(row.handle) ? 'C' : 'U';
                    if(angular.isUndefined(row.handle)){ //创建新数据 ，若是删除中存在该条数据，则直接忽略该行,并在后面将删除操作也忽略
                        if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
                            paramActions.push({
                                "site": HttpAppService.getSite(),
                                "resrce": row.resrce,
                                "resourceBo": row.resourceBo,
                                //"strt": UtilsService.serverFommateDateTime(new Date(row.startDateTimeIn)),
                                "resrceDes": row.resrceDes,
                                "workCenter": row.workCenter,
                                "workCenterDes": row.workCenterDes,
                                "line": row.line,
                                "lineDes": row.lineDes,
                                "startDateTimeIn": UtilsService.serverFommateDateTime(new Date(row.startDateTimeIn)),
                                "endDateTimeIn": UtilsService.serverFommateDateTime(new Date(row.endDateTimeIn)),
                                "reasonCode": row.reasonCode,
                                "viewActionCode" : 'C'
                            });
                        }else{}
                    }else{ //修改已存于服务器的行数据 ，若删除列表中有该行，则直接忽略该修改提交，并在后面将删除操作添加进来
                        if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
                            paramActions.push({
                                "handle" : row.handle,
                                "site" : HttpAppService.getSite(),
                                "resrce": row.resrce,
                                "resourceBo": row.resourceBo,
                                "strt": row.strt,
                                "resrceDes": row.resrceDes,
                                "workCenter": row.workCenter,
                                "workCenterDes": row.workCenterDes,
                                "line": row.line,
                                "lineDes": row.lineDes,
                                "startDateTimeIn":  UtilsService.serverFommateDateTime(new Date(row.startDateTimeIn)),
                                "endDateTimeIn": UtilsService.serverFommateDateTime(new Date(row.endDateTimeIn)),
                                "reasonCode": row.reasonCode,
                                "modifiedDateTime": row.modifiedDateTime,
                                "viewActionCode" : 'U'
                            });
                        }else{}
                    }
                }
            }
            for(var i = 0; i < $scope.config.deletedRows.length; i++){
                var deleteTempRow = $scope.config.deletedRows[i];
                if(!angular.isUndefined(deleteTempRow.handle)){// 服务器端数据的删除操作,直接添加进去即可
                    paramActions.push({
                        "handle" : deleteTempRow.handle,
                        "site" : HttpAppService.getSite(),
                        "resrce": deleteTempRow.resrce,
                        "resourceBo": deleteTempRow.resourceBo,
                        "strt": deleteTempRow.strt,
                        "resrceDes": deleteTempRow.resrceDes,
                        "workCenter": deleteTempRow.workCenter,
                        "workCenterDes": deleteTempRow.workCenterDes,
                        "line": deleteTempRow.line,
                        "lineDes": deleteTempRow.lineDes,
                        "startDateTimeIn": UtilsService.serverFommateDateTime(new Date(deleteTempRow.startDateTimeIn)),
                        "endDateTimeIn":UtilsService.serverFommateDateTime(new Date( deleteTempRow.endDateTimeIn)),
                        "reasonCode": deleteTempRow.reasonCode,
                        "modifiedDateTime": deleteTempRow.modifiedDateTime,
                        "viewActionCode" : 'D'
                    });
                }else{ }
            }
            console.log(paramActions);
            if(paramActions.length > 0){
                plannedStopService
                    .planStopKeepData(paramActions)
                    .then(function (resultDatas){
                        //$scope.config.btnDisabledSave = true;
                        if(!$scope.$$phase){
                            $scope.$apply();
                        }
                        $scope.hideBodyModel();
                        $scope.bodyConfig.overallWatchChanged = false;
                        $scope.addAlert('success', '保存成功!');
                        $scope.config.deletedRows = [];
                        $scope.queryDatas();
                    },function (resultDatas){
                        $scope.hideBodyModel();
                        $scope.gridPlannedStop.data = dataCopy;
                        $scope.addAlert('danger', '保存失败! '+resultDatas.myHttpConfig.statusDesc);
                    });
            }else{
                $scope.hideBodyModel();
                $scope.addAlert('没有修改,无需保存!');
            }
        };

        function __deletedRowsExitThisRowByHashKey(rowHashKey){
            for(var n = 0; n < $scope.config.deletedRows; n++){
                if(rowHashKey == $scope.config.deletedRows[n].$$hashKey){
                    return true;
                }
            }
            return false;
        };
//验证
        function __checkCanSaveDatas(){
            for(var i = 0; i < $scope.gridPlannedStop.data.length; i++){
                var item = $scope.gridPlannedStop.data[i];
                if(item.hasChanged){  // TODO
                    var rules = [
                        { field: 'startDateTimeIn', emptyDesc: '第'+(i+1)+'行: 计划开始时间不能为空!' },
                        { field: 'endDateTimeIn', emptyDesc: '第'+(i+1)+'行: 计划结束时间不能为空!' },
                        { field: 'reasonCode', emptyDesc: '第'+(i+1)+'行: 原因代码不能为空!' }
                    ];
                    for(var ruleIndex in rules){
                        var rule = rules[ruleIndex];
                        if(angular.isUndefined(item[rule.field]) || item[rule.field] == null || item[rule.field] == ""){
                            $scope.addAlert('', rule.emptyDesc);
                            return false;
                        }
                    }
                }
            }
            return true;
        };

        //查询
        $scope.queryDatas = function(){
            $scope.gridPlannedStop.paginationCurrentPage = 1;
            if($scope.bodyConfig.overallWatchChanged){
                var promise = UtilsService.confirm('重新查询会刷新现有数据', '提示', ['确定', '取消']);
            		promise
                        .then(function(){ // ok
                            $scope.bodyConfig.overallWatchChanged = false;
                            __requestTableDatas();
                            //__requestQueryDataCount();
                            __selectedDeviceTypeForDataCount();
                }, function(){   // cancel

                });
            }else{
                 __requestQueryDataCount();
                __selectedDeviceTypeForDataCount();
             }

        };
        //fenye
        function __requestTableDatas(pageIndex, pageCount){
            var pageIndexX = 1;
            var pageCountX = $scope.gridPlannedStop.paginationPageSize;
            if(angular.isUndefined(pageIndex) || pageIndex == null || pageIndex<=1){
                pageIndexX = 1;
            }else{
                pageIndexX = pageIndex;
            }
            if(angular.isUndefined(pageCount) || pageCount == null || pageCount<1){
                pageCountX = $scope.gridPlannedStop.paginationPageSize;
            }else{
                pageCountX = pageCount;
            }
            var resrce = [];
            for(var i = 0; i < $scope.config.currentDeviceNums.length; i++){
                resrce.push($scope.config.currentDeviceNums[i].resrce);
            }
            var work_center = $scope.config.currentWorkCenter.workCenter;
            var line = [];
            for(var i=0;i<$scope.config.currentLine.length;i++){
                line.push($scope.config.currentLine[i].workCenter);
            }
            console.log(line);
            console.log($scope.config.currentLine);
            //var index = 1;
            //var count = $scope.gridPlannedStop.paginationPageSize;
            var time = UtilsService.serverFommateDate(new Date($scope.config.queryManDate));
            var deviceTypes = null;
            if($scope.config.currentDeviceType){
                deviceTypes = $scope.config.currentDeviceType.resourceType;
            }
            $scope.gridPlannedStop.data = [];
            plannedStopService
                .planStopQueryData(resrce,time,work_center,line,pageIndexX,pageCountX,deviceTypes)
                .then(function (resultDatas){
                    //console.log(resultDatas);
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.gridPlannedStop.data = resultDatas.response;
                        for(var i=0;i<$scope.gridPlannedStop.data.length;i++){
                            $scope.gridPlannedStop.data[i].startDateTimeIn = UtilsService.serverStringFormatDateTime($scope.gridPlannedStop.data[i].startDateTimeIn);
                            $scope.gridPlannedStop.data[i].endDateTimeIn = UtilsService.serverStringFormatDateTime($scope.gridPlannedStop.data[i].endDateTimeIn);
                            $scope.gridPlannedStop.data[i].changeStart = "old";
                        }
                        return;
                    }
                }, function (errorResultDatas){
                    $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
                });







            //var pageIndexX = 1;
            //var pageCountX = $scope.gridPlannedStop.paginationPageSize;
            //if(angular.isUndefined(pageIndex) || pageIndex == null || pageIndex<=1){
            //    pageIndexX = 1;
            //}else{
            //    pageIndexX = pageIndex;
            //}
            //if(angular.isUndefined(pageCount) || pageCount == null || pageCount<1){
            //    pageCountX = $scope.gridPlannedStop.paginationPageSize;
            //}else{
            //    pageCountX = pageCount;
            //}
            //$scope.gridPlannedStop.data = [];
            //plannedStopService
            //    .planStopQueryData($scope.config.currentWorkCenter,$scope.config.queryManDate,$scope.config.resrce,$scope.config.currentLine,pageIndexX,pageCountX)
            //    .then(function (resultDatas){
            //        if(resultDatas.response && resultDatas.response.length > 0){
            //            $scope.gridPlannedStop.data = resultDatas.response;
            //            return;
            //        }
            //    }, function (errorResultDatas){
            //        $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
            //    });
        };

        function __selectedDeviceTypeForDataCount(){
            var resrce = [];
            for(var i = 0; i < $scope.config.currentDeviceNums.length; i++){
                resrce.push($scope.config.currentDeviceNums[i].resrce);
            }
            var work_center = $scope.config.currentWorkCenter.workCenter;
            var line = [];
            for(var i=0;i<$scope.config.currentLine.length;i++){
                line.push($scope.config.currentLine[i].workCenter);
            }
            var time = UtilsService.serverFommateDate(new Date($scope.config.queryManDate));
            var deviceTypes = null;
            if($scope.config.currentDeviceType){
                deviceTypes = $scope.config.currentDeviceType.resourceType;
            };
            plannedStopService
                .planStopQueryDataCount(resrce,time,work_center,line,deviceTypes)
                .then(function (resultDatas){
                    if(resultDatas.myHttpConfig.status == 200){
                        $scope.gridPlannedStop.totalItems = resultDatas.response;
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        };

        $scope.editDataBtn = function(){
            if($scope.config.currentReasonCode == null){
                $scope.config.btnDisabledEdit = true;
            }else{
                $scope.config.btnDisabledEdit = false;
            }
        };
        //一键编辑 
        $scope.editData = function(){ 
            console.log($scope.config.queryEditDateStart);
            console.log(UtilsService.serverFommateDateTimeShow($scope.config.queryEditDateStart));
            if($scope.config.queryEditDateStart >= $scope.config.queryEditDateEnd){
                $scope.addAlert("","结束时间早于等于开始时间，不允许修改");
                return;
            }

            if(new Date($scope.config.queryEditDateStart) <= new Date()){
                $scope.addAlert("","开始时间早于等于当前时间，不允许修改");
                return;
            }
            var rows = $scope.gridPlannedStop.data;
            var selected = []
            var num = 0;
            for(var i=0;i<rows.length;i++){
                if(rows[i].isSelected){
                    selected.push(rows[i]);
                }
            }
            var errorInfos = null;
            var resrce = [];
            for(var i=0;i<rows.length;i++){
                for(var j=0;j<selected.length;j++){
                    if(selected[j].$$hashKey != rows[i].$$hashKey  && selected[j].workCenter == rows[i].workCenter && selected[j].resrce == rows[i].resrce ){
                        if( (new Date(rows[i].startDateTimeIn) >= new Date(selected[j].startDateTimeIn) &&
                            new Date(rows[i].startDateTimeIn) <= new Date(selected[j].endDateTimeIn)) ||
                            (new Date(rows[i].endDateTimeIn) >= new Date(selected[j].startDateTimeIn) &&
                            new Date(rows[i].endDateTimeIn) <= new Date(selected[j].endDateTimeIn))){
                            errorInfos = (i+1) + " ";
                            resrce.push(selected[j].resrce);
                        }

                    }
                }
            }
            if (errorInfos != null){
                $scope.addAlert(resrce.join(",")+"存在重叠的计划期间，不允许修改");
                return;
            }
            $scope.numFlag = 0;
            $scope.stringFlag = [];
            for(var k=0;k<selected.length;k++){
                console.log();
                if(new Date(selected[k].startDateTimeIn) <= new Date()){
                    $scope.addAlert("",selected[k].resrce+"的计划开始时间早于等于当天日期的停机记录");
                    return;
                }
                var resrce = selected[k].reasonCode,
                    start_date_time = UtilsService.serverFommateDateTime(new Date(selected[k].startDateTimeIn)),
                    end_date_time = UtilsService.serverFommateDateTime(new Date(selected[k].endDateTimeIn)),
                    handle = selected[k].handle;

                if(handle == null || handle == "" || handle ==undefined){

                }else{
                    plannedStopService.planStopVerificationData(resrce,start_date_time,end_date_time,handle).then(function (resultDatas){
                        console.log(resultDatas);
                        $scope.numFlag++;
                        $scope.stringFlag[$scope.numFlag] = resultDatas.response;
                        if($scope.numFlag == selected.length){
                            for(var l=0;l<selected.length;l++){
                                if($scope.stringFlag[l] == true){
                                    $scope.addAlert("","不允许编辑此条停机维护");
                                    return;
                                }
                            }
                            for(var i=0;i<selected.length;i++){
                                if(selected[i].isSelected) {
                                    selected[i].reasonCode = $scope.config.currentReasonCode.reasonCode;
                                    selected[i].reasonCodeDes = $scope.config.currentReasonCode.description;
                                    selected[i].startDateTimeIn = UtilsService.serverFommateDateTimeShow($scope.config.queryEditDateStart);
                                    selected[i].endDateTimeIn = UtilsService.serverFommateDateTimeShow($scope.config.queryEditDateEnd);
                                    selected[i].hasChanged = true;
                                }
                                if(selected[i].changeStart == "old"){
                                    selected[i].changeStart = "D";
                                }
                            }
                            console.log(selected);
                        }
                    }, function (resultDatas){
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
                }
            } 
        };

        $scope.$on("$stateChangeSuccess", function (event2, toState, toParams, fromState, fromParam){
            console.log(event2);
        });
    }]);

plannedStopModule.controller('ModalPlannedStopCtrl',['$scope','$uibModalInstance','planStopdata','uiGridConstants','plannedStopService','UtilsService','$filter', function ($scope, $uibModalInstance,planStopdata,uiGridConstants,plannedStopService,UtilsService,$filter) {
    $scope.config = {
        modalStartDate : null,
        modalEndDate : null,
        disabledConfirmBtn : true,
        reasoncode : [],
        currentReasonCode : null,
        name : planStopdata.name,
        allData : planStopdata.allData,
        workCenter : planStopdata.workCenter
    }
    if($scope.config.name == "新增停机计划"){
        var date = new Date((new Date().getTime() + 24*60*60*1000));
        $scope.config.modalStartDate = UtilsService.serverFommateDateTimeShow(date);
        $scope.config.modalEndDate = UtilsService.serverFommateDateTimeShow(date);
    }else{
        $scope.config.modalStartDate = UtilsService.serverFommateDateTimeShow(new Date());
        $scope.config.modalEndDate = UtilsService.serverFommateDateTimeShow(new Date());
    }
    //var a = new Date("2016/08/06 00:00:00");
    //$scope.config.modalStartDate = a;
    $scope.gridPlannedStop = {
        //paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes, //[5, 10, 15, 20, 50, 100],
        //paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize, //10,
        //useExternalPagination: true,

        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
            {
                field: 'handle', name: 'handle', visible: false
            },
            {
                field: 'isSelected', name: 'isSelected', displayName: '选择', minWidth: 60, maxWidth: 60,
                enableCellEdit: true, type: 'boolean', visible: false,
                enableColumnMenu: false, enableColumnMenus: false, enableSorting: false
            },
            {
                field: 'resrce', name: 'resrce', displayName: '设备编码', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellEditableCondition: function($scope){
                    return false;
                }
            },
            {
                field: 'asset', name: 'asset', displayName: '资产号', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                // cellTemplate: 'andon-ui-grid-tpls/dataCategory-categoryCode',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellEditableCondition: function($scope){
                    return false;
                }
            },
            {
                field: 'description', name: 'description', displayName: '设备名称', minWidth: 200,
                enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellEditableCondition: function($scope){
                    return false;
                }
            }
        ],
        data : [],
        onRegisterApi: __onRegisterApi
    };

    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
            if(newRowCol.col.field == "isDeleted" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                __checkCanDeleteData(newRowCol);
            }
        });
        gridApi.cellNav.on.viewPortKeyDown($scope, function(row, col){
            console.log("viewPortKeyDown");
        });
        gridApi.cellNav.on.viewPortKeyPress($scope, function(row, col){
            console.log("viewPortKeyPress");
        });
        $scope.config.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {

            $scope.config.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
        });
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            __disabledBtnChange();
            gridRow.entity.isSelected = gridRow.isSelected;
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                __disabledBtnChange();
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
        });
    };

    $scope.gridPlannedStop.data = planStopdata.deviceNums;

    for(var i=0;i<$scope.gridPlannedStop.data.length;i++){
        $scope.gridPlannedStop.data[i].isSelected = false;
    }
    $scope.config.reasoncode = planStopdata.reasoncode;

    //console.log($scope.gridPlannedStop.data);
    $scope.reasoncodeChanged = function(){
        __disabledBtnChange();
    };
    $scope.dataChange = function(){
        __disabledBtnChange();
    };
    //按钮控制
    function __disabledBtnChange(){
        var selectRows = $scope.config.gridApi.selection.getSelectedRows();
        if($scope.config.modalStartDate == null
        || $scope.config.modalEndDate == null
        || $scope.config.currentReasonCode == null
        || selectRows.length ==0 ){
            $scope.config.disabledConfirmBtn = true;
        }else{
            $scope.config.disabledConfirmBtn = false;
        }
    }
    //确定操作
    $scope.ok = function () {
        var startDate = new Date($scope.config.modalStartDate);
        var endDate = new Date($scope.config.modalEndDate)
        var rows = $scope.gridPlannedStop.data;
        if($scope.config.name == "新增停机计划" && startDate.getTime() > endDate.getTime()){
            $scope.addAlert("","结束时间早于等于开始时间，不允许新增");
            return;
        }
        if($scope.config.name == "事后补录停机计划" && startDate.getTime() > endDate.getTime()){
            $scope.addAlert("","结束时间早于等于开始时间，不允许补录");
            return;
        }
        //console.log($scope.config.modalStartDate.getTime());
        //console.log(new Date("2016/08/04 10:10:10").getTime());
        //
        //console.log(new Date().getTime());
        //
        //console.log(new Date().getTime()-24*60*60*100);

        if($scope.config.name == "新增停机计划" && new Date().getTime() >= startDate.getTime()){
            $scope.addAlert("","不能新增计划开始时间早于等于当天日期的停机记录");
            return;
        }
        if($scope.config.name == "事后补录停机计划" && new Date().getTime()-24*60*60*1000 >= startDate.getTime()){
            $scope.addAlert("","不能补录计划开始时间在24小时之前的停机记录");
            return;
        }

        if($scope.config.name == "事后补录停机计划" && new Date().getTime() <= startDate.getTime()){
            $scope.addAlert("","不能补录没有发生的的停机记录");
            return;
        }
        if($scope.config.name == "事后补录停机计划" && new Date().getTime() <= endDate.getTime()){
            $scope.addAlert("","不能补录没有发生的的停机记录");
            return;
        }
        if($scope.config.name == "事后补录停机计划"){
            $scope.titleName = "补录"
        }else{
            $scope.titleName = "新增"
        }
        for(var i=0;i<$scope.config.allData.length;i++){
            if($scope.config.allData[i].workCenter == $scope.config.workCenter && $scope.config.allData[i].resrce == $scope.config.resrce){
                if(new Date($scope.config.allData[i].startDateTimeIn) >= new Date($scope.config.modalEndDate) || new Date($scope.gridPlannedStop.data[i].endDateTime) <= $scope.config.modalStartDate){
                }else{
                    if($scope.config.name == "事后补录停机计划"){
                        $scope.addAlert("",$scope.config.allData[i].resrce+"存在重叠的计划期间，不允许补录");
                    }else{
                        $scope.addAlert("",$scope.config.allData[i].resrce+"存在重叠的计划期间，不允许新增");
                    }
                    return;
                }
            }
        }
        $scope.selectedSupplements = [];
        console.log($scope.selectedSupplements);
        for(var i=0;i<rows.length;i++){
            var row = rows[i];
            if(row.isSelected){
                $scope.selectedSupplements.push({
                    deviceCode : row,
                    reasonCode : $scope.config.currentReasonCode,
                    startDate  : UtilsService.serverFommateDateTimeShow(new Date($scope.config.modalStartDate)),
                    endDate  : UtilsService.serverFommateDateTimeShow(new Date($scope.config.modalEndDate)),
                    flag : ""
                });
            }
        }
        console.log($scope.selectedSupplements);
        for(var i=0;i<$scope.selectedSupplements.length;i++){
            var resrce = $scope.selectedSupplements[i].deviceCode.resrce,
                start_date_time = UtilsService.serverFommateDateTime(new Date($scope.config.modalStartDate)),
                end_date_time = UtilsService.serverFommateDateTime(new Date($scope.config.modalEndDate));
                handle = '';
            $scope.num = 0;
            plannedStopService.planStopVerificationData(resrce,start_date_time,end_date_time,handle).then(function (resultDatas){
                if(resultDatas.response){
                    $scope.selectedSupplements[$scope.num].flag = true;
                }else{
                    $scope.selectedSupplements[$scope.num].flag = false;
                }
                $scope.num++;
                console.log($scope.num);
                console.log($scope.selectedSupplements);
                if($scope.num == $scope.selectedSupplements.length){
                    console.log($scope.selectedSupplements);
                    $scope.infos = "";
                    for(var j=0;j<$scope.selectedSupplements.length;j++){
                        if($scope.selectedSupplements[j].flag == true){
                            $scope.infos = $scope.selectedSupplements[j].deviceCode.resrce + " ";
                        }
                    }
                    console.log($scope.infos);
                    if($scope.infos != ""){
                        $scope.addAlert("设备编码"+$scope.infos +"不可以" +$scope.titleName);
                    }else{
                        $uibModalInstance.close($scope.selectedSupplements);
                    }
                }
            }, function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        if($scope.selectedSupplements.length < 1){
            $scope.selectedSupplements.push({
                deviceCode : '',
                reasonCode : $scope.config.currentReasonCode,
                startDate  : UtilsService.serverFommateDateTimeShow(new Date($scope.config.modalStartDate)),
                endDate  : UtilsService.serverFommateDateTimeShow(new Date($scope.config.modalEndDate))
            });
            var resrce = '',
                start_date_time = UtilsService.serverFommateDateTime(new Date($scope.config.modalStartDate)),
                end_date_time = UtilsService.serverFommateDateTime(new Date($scope.config.modalEndDate));
                //handle = '';
            plannedStopService.planStopVerificationData(resrce,start_date_time,end_date_time,handle).then(function (resultDatas){
                console.log(resultDatas);
                if(resultDatas.response){
                    $scope.addAlert("","已被占用,不可以使用");
                    return;
                }else{
                    $uibModalInstance.close($scope.selectedSupplements);
                }
            }, function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
plannedStopModule.controller('ModalSupplementCtrl',["$scope","$uibModalInstance","planStopdata","uiGridConstants", function ($scope, $uibModalInstance,planStopdata,uiGridConstants) {
    $scope.config = {
        modalSupplementStartDate: null,
        modalSupplementEndDate : null,

        disabledConfirmBtn : true,
        reasoncode : [],
        currentReasonCode : null
    }
    $scope.gridSupplement = {
        paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes, //[5, 10, 15, 20, 50, 100],
        paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize, //10,
        useExternalPagination: true,

        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
            {
                field: 'handle', name: 'handle', visible: false
            },
            {
                field: 'isSelected', name: 'isSelected', displayName: '选择', minWidth: 60, maxWidth: 60,
                enableCellEdit: true, type: 'boolean', visible: false,
                enableColumnMenu: false, enableColumnMenus: false, enableSorting: false
            },
            {
                field: 'resrce', name: 'resrce', displayName: '设备编码', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellEditableCondition: function($scope){
                    return false;
                }
            },
            {
                field: 'deviceNumber', name: 'deviceNumber', displayName: '资产号', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                // cellTemplate: 'andon-ui-grid-tpls/dataCategory-categoryCode',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellEditableCondition: function($scope){
                    return false;
                }
            },
            {
                field: 'description', name: 'description', displayName: '设备名称', minWidth: 200,
                enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellEditableCondition: function($scope){
                    return false;
                }
            }
        ],
        data : [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
            if(newRowCol.col.field == "isDeleted" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                __checkCanDeleteData(newRowCol);
            }
        });
        gridApi.cellNav.on.viewPortKeyDown($scope, function(row, col){
            console.log("viewPortKeyDown");
        });
        gridApi.cellNav.on.viewPortKeyPress($scope, function(row, col){
            console.log("viewPortKeyPress");
        });
        $scope.config.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
            $scope.config.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
        });
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
        });
    }
    $scope.gridSupplement.data = deviceNums;
    $scope.reasoncodeChanged = function(){
        __disabledBtnChange();
    };
    $scope.dataChange = function(){
        __disabledBtnChange();
    };
    function __disabledBtnChange(){
        //var selectRows = $scope.config.gridApi.selection.getSelectedRows();
        if($scope.config.modalSupplementStartDate ==null
           ||  $scope.config.modalSupplementEndDate==null
            ||$scope.config.currentReasonCode==null){
            $scope.config.disabledConfirmBtn = true;
        }else{
            $scope.config.disabledConfirmBtn = false;
        }
    }
    $scope.ok = function () {
        var rows = $scope.gridSupplement.data;
        $scope.selectedSupplements = [];
        for(var i=0;i<rows.length;i++){
            var row = rows[i];
            if(row.isSelected){
                $scope.selectedSupplements.push(row);
                $uibModalInstance.close($scope.selectedMaterials);
            }
        }
        if($scope.selectedSupplements.length < 1){
            $scope.addAlert("","请选择设备编码");
        }else{
            $uibModalInstance.close($scope.selectedSupplements);
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);