reasonCodeModule.controller('reasonCodeMaintainCtrl',[
    '$scope', '$http', '$q','UtilsService',
    'HttpAppService', '$timeout', 'reasonCodeService',
    'uiGridConstants', 'uiGridValidateService','$state',
    function ($scope, $http, $q, UtilsService,
              HttpAppService, $timeout, reasonCodeService,
              uiGridConstants, uiGridValidateService,$state) {

        $scope.config= { 
            // 新增行数
            addRowsNum: 1,
            gridApi: null,

            reasonCodeGroups: [],
            currentReasonCodeGroup: null, 
            enabled: false,

            btnDisabledQuery: true,
            btnDisabledAdd: true,
            btnDisabledSave: true,
            inputDisabledNum: true,

            paginationCurrentPage: 0,   // 用来保存点击翻页之前，所在页数
            ignoreThisPageChange: false  // 是否忽略当前翻页时的修改判断，针对代码中修改页码的情况
        };

        //表格处理
        $scope.gridReasonCodeMaintain = {
            //是否分页
            enablePagination: true,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,

            useExternalPagination: true,
            //现在页数
            paginationCurrentPage: 1,
            //总页数
            totalItems: 0,

            showGridFooter: false, 
            modifierKeysToMultiSelectCells: false,
            data: [],
            onRegisterApi: __onRegisterApi,
            columnDefs: [
                { 
                    field: 'handle', name: 'handle', visible: false, minWidth: 0, maxWidth: 0
                },
                {
                    field: 'hasChanged', name: 'hasChanged', visible: false, minWidth: 0, maxWidth: 0
                },
                {
                    field: 'site', name: 'site', visible: false, minWidth: 0, maxWidth: 0
                },
                {
                    field: 'reasonCodeGroupPO.description',
                    name: 'reasonCodeGroupPO.description', displayName: '原因代码组描述',
                    minWidth: 100,
                    enableCellEdit: $scope.reasonCodeConfig.canEdit, enableCellEditOnFocus: false,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }, 
                    cellEditableCondition: function($scope){
                        return false;
                    }
                }, 
                { 
                    field: 'reasonCode', name: 'reasonCode', displayName: '原因代码', minWidth: 150, 
                    validators: { required: true }, cellClass:'grid-no-editable ad-uppercase',
                    enableCellEdit: $scope.reasonCodeConfig.canEdit, enableCellEditOnFocus: false, 
                    cellTooltip: function(row, col){  return row.entity[col.colDef.name];  }, 
                    cellTemplate: 'andon-ui-grid-tpls/reasonCode-reasonCode',
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row;
                        var col = arguments[0].col;
                        // 非新增不可编辑 、新增行可编辑 
                        if(!angular.isUndefined(row.entity) && row.entity!=null && !angular.isUndefined(row.entity.handle) && row.entity.handle!=null && row.entity.handle!=""){
                            return false;
                        }else{
                            return true;
                        }
                    }
                },  
                {   
                    field: 'description', name: 'description', displayName: '原因代码描述', minWidth: 200,
                    enableCellEdit: $scope.reasonCodeConfig.canEdit, enableCellEditOnFocus: false, validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellTemplate: 'andon-ui-grid-tpls/dataCategory-categoryDesc'
                }, 
                {
                    field:"enabled",name:"enabled",displayName:'启用',width: 50,
                    cellTooltip: function(row, col){ return "启用"; },
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTemplate:'andon-ui-grid-rscg/reasonCodeGroup-checkbox'
                }
            ]
        };

        $scope.init = function(){
            __requestReasonCodeGroups();
            $scope.config.currentReasonCodeGroup = $scope.config.reasonCodeGroups[0];
        };

        //选择原因代码组后,所有按钮可点击
        $scope.reasonCodeGroup = function($select){
            var group = $scope.config.currentReasonCodeGroup;
            if(angular.isUndefined(group) || group == null || UtilsService.isEmptyString(group.handle)){
                $scope.config.btnDisabledQuery = true;
            }else{
                $scope.config.btnDisabledQuery = false;
                $scope.config.btnDisabledSave = false;
                $scope.config.btnDisabledAdd = false;
                $scope.config.inputDisabledNum = false;
            }
        };

        //是否显示未被启用的数据
        $scope.enabledChanged = function(){
            __enabledChanged();
        };

        //新增行数
        $scope.addReasonCodeRows = function(){
            if(angular.isUndefined($scope.gridReasonCodeMaintain.data) || $scope.gridReasonCodeMaintain.data == null){
                $scope.gridReasonCodeMaintain.data = [];
            }
            for(var i = 0; i < $scope.config.addRowsNum; i++){
                $scope.gridReasonCodeMaintain.data.unshift({
                    hasChanged:true,
                    reasonCodeGroupPO: {
                        description:$scope.config.currentReasonCodeGroup.description
                    },
                    reasonCodeGroup:$scope.config.currentReasonCodeGroup.reasonCodeGroup,
                });
            }
            //正在修改的状态
            $scope.bodyConfig.overallWatchChanged = true;
        };

        //查询数据，如果还有修改未保存，提示
        $scope.queryDatas = function(){
            if($scope.bodyConfig.overallWatchChanged){
                var promise =UtilsService.confirm('还有修改未保存，是否继续', '提示', ['确定', '取消']);
                promise.then(function(){
                    $scope.bodyConfig.overallWatchChanged = false;
                    $scope.gridReasonCodeMaintain.paginationCurrentPage = 1;
                    $scope.config.paginationCurrentPage = 1;
                    __requestTableDatasCount();
                    __requestTableDatas();
                }, function(){
                    //取消停留在原页面
                    $scope.bodyConfig.overallWatchChanged = true;
                });
            }else{
                $scope.bodyConfig.overallWatchChanged = false;
                $scope.gridReasonCodeMaintain.paginationCurrentPage = 1;
                $scope.config.paginationCurrentPage = 1;
                //查询总数据数量
                __requestTableDatasCount();
                //查询分页数据
                __requestTableDatas();

            }

        };

        //保存数据
        $scope.saveAllAcitons = function(){
            //不满足条件直接返回
            if(!__checkCanSaveDatas()){
                return;
            };
            var paramActions = [];
            for(var i = 0; i < $scope.gridReasonCodeMaintain.data.length; i++){
                var row = $scope.gridReasonCodeMaintain.data[i]; 
                if(row.hasChanged){
                    //创建新数据 ，若是删除中存在该条数据，则直接忽略该行,并在后面将删除操作也忽略
                    if(angular.isUndefined(row.handle)){
                        paramActions.push({
                            "site": HttpAppService.getSite(),
                            "reasonCode": row.reasonCode,
                            "description": row.description,
                            "reasonCodeGroup": row.reasonCodeGroup,
                            "enabled": row.enabled ? row.enabled:false,
                            "viewActionCode": 'C'
                        });

                        //修改已存于服务器的行数据 ，若删除列表中有该行，则直接忽略该修改提交，并在后面将删除操作添加进来
                    }else{
                        paramActions.push({
                            "handle": row.handle,
                            "site": row.site,
                            "reasonCode": row.reasonCode,
                            "description": row.description,
                            "reasonCodeGroup": row.reasonCodeGroup,
                            "enabled": row.enabled ? row.enabled:false,
                            "createdDateTime":row.createdDateTime,
                            "modifiedDateTime":row.modifiedDateTime,
                            "modifiedUser":row.modifiedUser,
                            "viewActionCode": 'U',
                            "reasonCodeGroupPO": row.reasonCodeGroupPO
                        });
                    }
                }
            }
            if(paramActions.length > 0){
                //打开遮罩
                $scope.showBodyModel("正在保存");
                reasonCodeService
                    .reasonCodeSaveX(paramActions)
                    .then(function (resultDatas){
                        $scope.addAlert('success', '保存成功!');
                        //隐藏遮罩、将状态调整为没有修改，此时可以跳转到任何界面不出现提示、页面回到第一页
                        $scope.hideBodyModel();
                        $scope.bodyConfig.overallWatchChanged = false;
                        $scope.gridReasonCodeMaintain.paginationCurrentPage = 1;
                        $scope.config.paginationCurrentPage = 1;
                        __requestTableDatas();
                    },function (resultDatas){
                        $scope.addAlert('danger', '保存失败!');
                    });
            }else{
                $scope.addAlert('没有修改,无需保存!');
            }
        };

        //是否显示未被启用的数据
        function __enabledChanged(){
            $scope.gridReasonCodeMaintain.paginationCurrentPage = 1;
            $scope.config.paginationCurrentPage = 1;
            __requestTableDatasCount();
             __requestTableDatas();
        };

        function __checkCanSaveDatas(){
            for(var i = 0; i < $scope.gridReasonCodeMaintain.data.length; i++){
                var item = $scope.gridReasonCodeMaintain.data[i];
                if(item.hasChanged){
                    var rules = [
                        { field: 'reasonCode', emptyDesc: '第'+(i+1)+'行: 原因代码不能为空!' },
                        { field: 'description', emptyDesc: '第'+(i+1)+'行: 原因代码描述不能为空!' }
                    ];
                    for(var ruleIndex in rules){
                        var rule = rules[ruleIndex];
                        if(angular.isUndefined(item[rule.field]) || item[rule.field] == null || item[rule.field] == ""){
                            $scope.addAlert('danger', rule.emptyDesc);
                            return false;
                        }
                    }
                    if(item.reasonCodeZhcnIsValid === false){
                        $scope.addAlert('', '第'+(i+1)+'行: 原因代码不能为汉字!');
                        return false;
                    }
                    if(item.reasonCodeCountIsValid === false){
                        $scope.addAlert('', '第'+(i+1)+'行: 原因代码必须是4位!');
                        return false;
                    }
                    

                    if(item.reasonCodeIsValid === false){
                        $scope.addAlert('danger', '第'+(i+1)+'行: 该原因代码已经存在!');
                        return false;
                    }
                }
            }
            return true;
        };

        $scope.init();

        function __onRegisterApi(gridApi){
            $scope.config.gridApi = gridApi; 
            $scope.config.gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
                if(newRowCol.col.field == "enable" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                    newRowCol.row.entity.enabled = !newRowCol.entity.enabled;
                    console.log(newRowCol.entity.enabled);
                }   
            }); 
            // 分页相关 
            $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                // console.log(currentPage); 
                // if($scope.config.ignoreThisPageChange){ 
                //     $scope.config.ignoreThisPageChange = false; 
                //     return; 
                // } 
                // if($scope.bodyConfig.overallWatchChanged){
                //     $scope.config.ignoreThisPageChange = false;
                //     var promise =UtilsService.confirm('放弃当前页修改?', '提示', ['确定', '取消']);
                //     promise.then(function(){
                //         $scope.config.ignoreThisPageChange = true;
                //         $scope.config.paginationCurrentPage = $scope.gridReasonCodeMaintain.paginationCurrentPage = currentPage;
                //         __requestTableDatas(currentPage, pageSize);
                //     }, function(){ //取消停留在原页面
                //         $scope.config.ignoreThisPageChange = true;
                //         $scope.config.paginationCurrentPage = $scope.gridReasonCodeMaintain.paginationCurrentPage = $scope.config.paginationCurrentPage;
                //         //__requestTableDatas(currentPage, pageSize);
                //     });
                // }else{
                //     $scope.config.ignoreThisPageChange = false;
                //     $scope.bodyConfig.overallWatchChanged = false;
                //     __requestTableDatas(currentPage, pageSize);
                // }
                $scope.bodyConfig.overallWatchChanged = false;
                __requestTableDatas(currentPage, pageSize);
                return true;
            }, $scope);

            // 编辑相关 
            $scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                    var promise = $q.defer();
                    if($scope.config.gridApi.rowEdit){
                        $scope.config.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
                    }

                    if(oldValue == newValue || colDef.field == 'isSelected'){
                        promise.resolve();
                        return;
                    }
                    $scope.bodyConfig.overallWatchChanged = true;
                    // 原因代码必须大写 
                    if(colDef.field == "reasonCode" || colDef.name=="reasonCode"){
                        if(UtilsService.isEmptyString(rowEntity.reasonCode)){
                            return;
                        };
                        rowEntity.reasonCode = rowEntity.reasonCode.toUpperCase();
                        if(UtilsService.isContainZhcn(rowEntity.reasonCode)){
                            promise.reject();
                            $scope.addAlert('', '原因代码不能为汉字!');
                            uiGridValidateService.setInvalid(rowEntity, colDef);
                            rowEntity.reasonCodeZhcnIsValid = false;
                            return;
                        }   
                        if(rowEntity.reasonCode.length != 4){
                            promise.reject();
                            $scope.addAlert('', '原因代码必须是4位!');
                            uiGridValidateService.setInvalid(rowEntity, colDef);
                            rowEntity.reasonCodeCountIsValid = false;
                            return;
                        }   
                        rowEntity.reasonCodeCountIsValid = true;
                        rowEntity.reasonCodeZhcnIsValid = true;
                    };
                    rowEntity.hasChanged = true;
                    if(colDef.field === "reasonCode"){
                        reasonCodeService
                            .reasonCodeExistX(newValue)
                            .then(function (resultDatas){
                                if(resultDatas.response){ //已经存在:true
                                    $scope.addAlert('danger', '该原因代码已经存在!');
                                    promise.reject();
                                    uiGridValidateService.setInvalid(rowEntity, colDef);
                                    rowEntity.reasonCodeIsValid = false;
                                }else{  //不存在冲突，可以使用 
                                    console.log("不存在冲突，可以使用");
                                    //gridApi.validate.setValid(rowEntity, colDef);
                                    // uiGridValidateService.setValid(rowEntity, colDef);
                                    promise.resolve();
                                    rowEntity.hasChanged = true;
                                    rowEntity.reasonCodeIsValid = true;
                                    uiGridValidateService.setValid(rowEntity, colDef);
                                }
                            },function (resultDatas){ //TODO 检验失败
                                $scope.addAlert('danger', '该原因代码已经存在!');
                                // gridApi.validate.setInvalid(rowEntity, colDef);
                                // uiGridValidateService.setInvalid(rowEntity, colDef);
                                promise.reject();
                                uiGridValidateService.setInvalid(rowEntity, colDef);
                                rowEntity.reasonCodeIsValid = false;
                                // $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                            }); 
                    }else{
                        promise.resolve();
                    }
            }); 
            $scope.config.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
                $scope.config.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
            });
        };

        //查询总数据数量
        function __requestTableDatasCount(){

            var reasonCodeGroup = $scope.config.currentReasonCodeGroup.reasonCodeGroup;
            var enabled = null;
            if(!$scope.config.enabled){
                enabled = true;
            }
            reasonCodeService
                .reasonCodeCountX(reasonCodeGroup, enabled)
                .then(function (resultDatas){
                    $scope.gridReasonCodeMaintain.totalItems = resultDatas.response;
                }, function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };

        //查询总数据
        function __requestTableDatas(currentPage, pageSize){
            $scope.gridReasonCodeMaintain.data = [];
            var reasonCodeGroup = $scope.config.currentReasonCodeGroup.reasonCodeGroup;
            var enabled = null;
            var pageIndex = 1;
            var pageCount = 1;
            if(angular.isUndefined(currentPage) || currentPage == null | currentPage<=1){
                pageIndex = 1;
            }else{
                pageIndex = currentPage;
            }
            if(angular.isUndefined(pageSize) || pageSize == null | pageSize<1){
                pageCount = $scope.gridReasonCodeMaintain.paginationPageSize;
            }else{
                pageCount = pageSize;
            }
            if(!$scope.config.enabled){
                enabled = true;
            }
            reasonCodeService
                .reasonCodeListX(reasonCodeGroup, enabled,pageIndex,pageCount)
                .then(function (resultDatas){ 
                    $scope.config.ignoreThisPageChange = false;
                    $scope.config.paginationCurrentPage = 1;
                    $scope.bodyConfig.overallWatchChanged = false;
                    if(resultDatas.response && resultDatas.response.length > 0){ 
                        for(var i = 0; i < resultDatas.response.length; i++){ 
                            resultDatas.response[i].hasChanged = false;
                            if(resultDatas.response[i].enabled == "true"){
                                resultDatas.response[i].enabled = true;
                            }else{
                                resultDatas.response[i].enabled = false;
                            }
                        }
                        $scope.gridReasonCodeMaintain.data = resultDatas.response;
                        return;
                    }
                }, function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };

        //获得原因代码组数据
        function __requestReasonCodeGroups(){
            reasonCodeService
                .reasonCodeGroup()
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].reasonCodeGroup+"  描述:"+resultDatas.response[i].description;
                        }
                        if(angular.isUndefined($scope.config.reasonCodeGroups) || $scope.config.reasonCodeGroups==null){
                            $scope.config.reasonCodeGroups = [{
                                handle: null,
                                description: '',
                                descriptionNew: ''
                            },{
                                handle: null,
                                description: '--- 请选择 ---',
                                descriptionNew: '--- 请选择 ---'
                            }];
                        }
                        $scope.config.reasonCodeGroups = $scope.config.reasonCodeGroups.concat(resultDatas.response);
                        // $scope.config.reasonCodeGroups = resultDatas.response;
                        return;
                    }else{
                        $scope.config.reasonCodeGroups = [];
                    }
                }, function (resultDatas){
                    $scope.config.reasonCodeGroups = [];
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };


    }]);








