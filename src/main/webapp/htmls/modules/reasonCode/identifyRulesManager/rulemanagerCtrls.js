/**
 * 
 * [原因代码规则管理界面默认页所对应的 Controller: reasonCodeRulesManagerCtrl]
 * 
 */
reasonCodeModule.controller('reasonCodeRulesManagerCtrl',['$scope','$q', '$http','$interval', '$state', 
    'HttpAppService', '$timeout','$rootScope', 'UtilsService', 'gridUtil', 'i18nService', '$uibModal',
    'uiGridConstants','reasonCodeService','uiGridValidateService','uiGridValidateService', '$filter',
    function ($scope,$q,$http,$interval, $state, HttpAppService, $timeout,$rootScope, 
              UtilsService,gridUtil,i18nService, $uibModal, uiGridConstants,reasonCodeService,
              uiGridValidateService,uiGridValidateService, $filter) {
        $scope.config = { 
            gridApi: null,

            btnDisabledQuery: true,       // 是否禁用<查询>按钮
            btnDisabledDelete: true,      // 是否禁用<删除>按钮
            btnDisabledShowDetail: true,  // 是否禁用<详细信息>按钮
            btnDisabledCopy: true,        // 是否禁用<复制>按钮
            btnDisabledAddNew: false,     // 是否禁用<新增>按钮

            deletedRows: [],              // 缓存删除且未提交的表格行数据

            // currentPriority: null,
            currentPriorities: [],      // 当前所选优先级对象数组
            priorities: [               // 优先级下拉列表数组
                {id: 'A', value: 'A'},
                {id: 'B', value: 'B'},
                {id: 'C', value: 'C'},
                {id: 'D', value: 'D'}
            ],  
            
            currentQueryTarget: null,
            queryHistoryChange: true,
            queryHistoryStatus: false,

            queryData: UtilsService.serverFommateDateShow(new Date()), // 查询日期
            currentReasonCode: null,   // 当前所选原因代码对象
            currentReasonCodeS: [],    // 原因代码下拉列表数组

            canEdit: false      // 当前用户是否对该activity有编辑权限
        };
        $scope.config.canEdit = $scope.modulesRWFlag("#"+$state.$current.url.source);

        $scope.init = function(){
        };
        $scope.init();  
        $scope.queryHistoryStatusChange = function(){
            $scope.config.queryHistoryChange = !$scope.config.queryHistoryStatus;
            // console.log($scope.config.queryHistoryChange);
        };  
        $scope.queryHistoryChangeChange = function(){
            $scope.config.queryHistoryStatus = !$scope.config.queryHistoryChange;
            // console.log($scope.config.queryHistoryStatus);
        };  
        $scope.priorityChanged = function($select){
            __setBtns();
        };  
        /**
         * 
         * [clearReasonCodes 清除所选的原因代码数据]
         * 
         */
        $scope.clearReasonCodes = function(){
            $scope.config.currentReasonCodeS = [];
            if(!angular.isUndefined($scope.config.currentReasonCode) && $scope.config.currentReasonCode!= null){
                $scope.config.currentReasonCode.reasonCode = "";
            }
            __setBtns();
        };


        $scope.rulesInfoGrid = {
            columnDefs: [
                {
                    field: 'handle', name: 'handle', visible: false
                },
                {
                    field: 'isSelected', name: 'isSelected', displayName: '选择', minWidth: 60, maxWidth: 60,
                    enableCellEdit: false, type: 'boolean', visible: false,
                    enableColumnMenu: false, enableColumnMenus: false, enableSorting: false
                },
                {
                    field: 'showDetail', name: 'showDetail', displayName: '详情', minWidth: 60, maxWidth: 60,
                    enableCellEdit: false, type: 'boolean', visible: true, cellClass:'grid-no-editable',
                    cellTemplate: 'andon-ui-grid-tpls/reasonCodeRule-viewDetail',
                    enableColumnMenu: false, enableColumnMenus: false, enableSorting: false
                },
                {
                    field: 'reasonCodePO.reasonCode', name: 'reasonCodePO.reasonCode', displayName: '原因代码编号', minWidth: 115,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'reasonCodePO.reasonCodeDesc', name: 'description', displayName: '原因代码描述', minWidth: 115,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'reasonCodePO.priority', name: 'plcObject', displayName: '优先级', minWidth: 75,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'reasonCodePO.subPriority', name: 'plcObjectDescription', displayName: '子优先级', minWidth: 85,
                    enableCellEdit: false, enableCellEditOnFocus: false,cellClass:'text-align-right',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'reasonCodePO.effDateTimeParsed', name: 'startData', displayName: '生效日期', minWidth: 150,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'reasonCodePO.effEndDateTimeParsed', name: 'effEndDateTime', displayName: '失效日期', minWidth: 150,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'reasonCodePO.used', name: 'isUsed', displayName: '是否已使用', minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTemplate: 'andon-ui-grid-tpls/reasonCodeRule-used',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'activityRPO.activityR', name: 'workIdentify', displayName: '识别方式作业', minWidth: 115,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'activityRPO.activityRDesc', name: 'workIdentifyDescription', displayName: '识别方式作业描述', minWidth: 150,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'activityFPO.activityF', name: 'workFDValid', displayName: '防呆验证作业', minWidth: 115,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'activityFPO.activityFDesc', name: 'workFDValidDescription', displayName: '防呆验证作业描述', minWidth: 150,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'modifiedPO.modifiedUser', name: 'lastPerson', displayName: '最后修改人', minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'modifiedPO.modifiedDateTimeDesc', name: 'modifiedDateTime', displayName: '最后维护日期', minWidth: 150,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'isDeleted', name: 'isDeleted', displayName: '删除', width: 40,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    visible: $scope.config.canEdit,
                    cellTooltip: function(row, col){ return "删除该行"; },
                    cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col"><span class="ion-trash-a"></span></div>',
                    headerTemplate: '<div class="xbr-delete-col-header">删除</div>'
                }
            ],
            data: [],
            onRegisterApi: __onRegisterApi
        };

        $scope.ruleQueryDateChanged = function(){
            var d = $scope.config.queryData;
            if(new Date(d) == "Invalid Date"){
                $scope.config.queryData = UtilsService.serverFommateDateShow(new Date());
            }
        };

        /**
         * 
         * [openReasonCodeModal 打开原因代码选择模态框]
         * 
         */
        $scope.openReasonCodeModal = function(){
            var reasonCode = angular.isUndefined($scope.config.currentReasonCode) || $scope.config.currentReasonCode == null ? "" : $scope.config.currentReasonCode.reasonCode;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/reasonCode/identifyRulesManager/model/m_select_reason_code.html',
                controller: 'ModelReasonCodeCtrl',
                size: 'lg',
                backdrop: 'false',
                openedClass: 'flex-center-parent',
                scope : $scope,
                resolve: {
                   modelReasonCodeData: function (){
                       return {
                            reasonCode: reasonCode
                       };
                   }
                }
            }); 
            modalInstance.result.then(function (closeDataObj){
                if(angular.isUndefined(closeDataObj) || closeDataObj == null){

                }else{
                    // var reasonCode = closeDataObj.reasonCode;
                    // $scope.config.currentReasonCode = reasonCode;
                    var reasonCodeS = closeDataObj.reasonCodes;
                    $scope.config.currentReasonCodeS = reasonCodeS;
                    var sStr = "";
                    for(var i = 0; i < reasonCodeS.length; i++){
                        if(i != 0){
                            sStr += ","
                        }
                        sStr += reasonCodeS[i].reasonCode;
                    }
                    // $scope.config.currentReasonCode.reasonCode = sStr;
                    $scope.config.reasonCodeSStr = sStr;
                }
                __setBtns();
                // console.log($scope.config.currentReasonCode);
            }, function(){ });
        };
        /**
         * 
         * [deleteSelectedRows 点击删除按钮触发该函数]
         * 
         */
        $scope.deleteSelectedRows = function(){
            var rows = $scope.config.gridApi.selection.getSelectedGridRows();
            var rowArray = [];
            for(var i = 0; i < rows.length; i++){
                var row = rows[i];
                rowArray.push({
                    handle: row.entity.reasonCodePO.reasonCodePriorityBo//+"abcdefg"
                });
            }
            if(rowArray.length <= 0){
                return;
            }
            $scope.showBodyModel("正在删除 "+rowArray.length+" 条记录!");
            reasonCodeService
                .reasonRuleDeleteRecognition(rowArray)
                .then(function (resultDatas){
                    // if(UtilsService.isEmptyString(resultDatas.response)){ // 保存成功
                        __deleteRowByHashCode(row.$$hashCode);
                        $scope.hideBodyModel();
                        $scope.addAlert('success', "删除成功! 共删除 "+rowArray.length+" 个"); 
                        $scope.queryDatas();
                        return;
                    // }
                }, function(resultDatas) {
                    $scope.hideBodyModel();
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                }); 
        }; 
        $scope.copySelectedRows = function(){
        };
        /**
         * 
         * [addNewRow 点击新增按钮触发该函数]
         * 
         */
        $scope.addNewRow = function(){
            __showNewModel();
        };
        /**
         * 
         * [showDetailInfo 点击详细信息触发该函数]
         * 
         */
        $scope.showDetailInfo = function(){ 
            var rows = $scope.config.gridApi.selection.getSelectedGridRows(); 
            __showDetailModel(rows[0].entity); 
        }; 

        function __showDetailModel(entity){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/reasonCode/identifyRulesManager/model/m_detail_info_rule.html',
                controller: 'ModelReasonCodeRuleDetailCtrl',
                size: 'lg',
                backdrop: 'false',
                openedClass: 'flex-center-parent',
                scope : $scope,
                resolve: {
                    modelRuleDetailData: function (){
                        return {
                            ruleRowEntity: entity,
                            canEdit: $scope.config.canEdit
                        };
                    }
                }   
            });     
            modalInstance.result.then(function (closeDataObj){
                if(angular.isUndefined(closeDataObj) || closeDataObj == null){
                    __setBtns();
                }else{ 
                    var reasonCode = closeDataObj.reasonCode; 
                    $scope.config.currentReasonCode = reasonCode; 
                }   
            }, function(){ 

            }); 
        };  
        function __showNewModel(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/reasonCode/identifyRulesManager/model/create/m_rule_create_model.html',
                controller: 'ModelRuleCreateCtrl',
                size: 'lg',
                backdrop: 'false',
                openedClass: 'flex-center-parent',
                scope : $scope,
                resolve: {
                   modelRuleCreateData: function (){
                       return {
                            
                       };
                   }
                }
            });
            modalInstance.result.then(function (closeDataObj){

            }, function(){

            });
        }
        function __showDeleteModel(entity){
            var modalInstance = $uibModal.open({
                animation: true, 
                templateUrl: 'modules/reasonCode/identifyRulesManager/model/m_delete_rule_row.html', 
                controller: 'ModelRuleDeleteCtrl', 
                size: 'lg', 
                backdrop: 'false', 
                openedClass: 'flex-center-parent', 
                scope : $scope, 
                resolve: { 
                   modelRuleDeleteData: function (){
                        return {
                            entity: entity
                        };
                   }
                }
            });
            modalInstance.result.then(function (closeDataObj){ 
                if(UtilsService.isEmptyString(closeDataObj)){ 
                    return; 
                } 
                var row = closeDataObj.entity; 
                $scope.config.deletedRows.push(angular.copy(row)); 
                $scope.showBodyModel("正在删除 1 条记录!");
                reasonCodeService 
                    .reasonRuleDeleteRecognition([{
                        handle: row.reasonCodePO.reasonCodePriorityBo//+"abcdefg"
                    }]) 
                    .then(function (resultDatas){ 
                        // if(UtilsService.isEmptyString(resultDatas.response)){ // 保存成功 
                            __deleteRowByHashCode(row.$$hashCode);
                            $scope.hideBodyModel(); 
                            $scope.addAlert('success', "删除成功! 共删除1个"); 
                            $scope.queryDatas();
                            return; 
                        // } 
                    }, function(resultDatas) { 
                        $scope.hideBodyModel();
                        $scope.addAlert('danger', "删除失败: "+resultDatas.myHttpConfig.statusDesc); 
                    }); 
            }, function(){ 

            });
        }
        function __deleteRowByHashCode(hashCode){
            for(var i = 0; i < $scope.rulesInfoGrid.data.length; i++){
                var data =  $scope.rulesInfoGrid.data[i];
                if(data.$$hashCode == hashCode){
                    $scope.rulesInfoGrid.data.splice(i, 1);
                    return;
                }
            }
        }

        /**
         * 
         * [reasonCodeChanged 所选原因代码改变时触发该方法]
         * 
         */
        $scope.reasonCodeChanged = function(){ 
            if(!angular.isUndefined($scope.config.currentReasonCode) 
                && $scope.config.currentReasonCode != null 
                && !angular.isUndefined($scope.config.currentReasonCode.reasonCode)
                && $scope.config.currentReasonCode.reasonCode !=null
                && $scope.config.currentReasonCode.reasonCode != ""){
                $scope.config.currentReasonCode.reasonCode = $scope.config.currentReasonCode.reasonCode.toUpperCase();
            }
            __setBtns(); 
        }; 
        function __setBtns(){
            // if($scope.config.currentPriority==null && $scope.config.currentReasonCode == null){
            if(
                (angular.isUndefined($scope.config.currentPriorities) || $scope.config.currentPriorities == null || $scope.config.currentPriorities.length <= 0) 
                && 
                (angular.isUndefined($scope.config.currentReasonCodeS) || $scope.config.currentReasonCodeS == null || $scope.config.currentReasonCodeS.length <= 0)
            ){
                $scope.config.btnDisabledQuery = true;
            }else{
                if($scope.config.queryData ==null){
                    $scope.config.btnDisabledQuery = true;
                }else{
                    $scope.config.btnDisabledQuery = false;
                }
            }
        };

        function __onRegisterApi(gridApi){ 
            $scope.config.gridApi = gridApi;
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){ 
                if(!angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                    if(newRowCol.col.field == "showDetail"){
                        __showDetailModel(newRowCol.row.entity);
                    }else if(newRowCol.col.field == "isDeleted"){
                        __showDeleteModel(newRowCol.row.entity);
                    }
                }
            }); 
            
            //选择相关 
            $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
                var count = $scope.config.gridApi.selection.getSelectedCount();
                $scope.config.btnDisabledShowDetail = count == 1 ? false : true;
                $scope.config.btnDisabledCopy = count == 1 ? false : true;
                $scope.config.btnDisabledDelete = count > 0 ? false : true;
                if(!$scope.$$phase){
                    $scope.$apply();
                }
            }); 
            $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                var count = $scope.config.gridApi.selection.getSelectedCount();
                $scope.config.btnDisabledShowDetail = count == 1 ? false : true;
                $scope.config.btnDisabledCopy = count == 1 ? false : true;
                $scope.config.btnDisabledDelete = count > 0 ? false : true;
                if(!$scope.$$phase){
                    $scope.$apply();
                }
            });  
        };

        $scope.queryDatas = function(){
            __requestTableDatas();
        };
        
        function __requestTableDatas(){
            $scope.config.gridApi.selection.clearSelectedRows();
            $scope.config.btnDisabledShowDetail = true;
            $scope.config.btnDisabledDelete = true;
            var priorityStr = ""; 
            for(var i = 0; i < $scope.config.currentPriorities.length; i++){
                if(i == 0){
                    priorityStr += $scope.config.currentPriorities[i].id;
                }else{
                    priorityStr += ","+$scope.config.currentPriorities[i].id;
                }
            }
            // var reasonCode = $scope.config.currentReasonCode;
            var reasonCode = "";
            for (var i = 0; i < $scope.config.currentReasonCodeS.length; i++) {
                if(i != 0){
                    reasonCode += ",";
                }
                reasonCode += $scope.config.currentReasonCodeS[i].reasonCode;
            };
            var params = {
                // reasonCode: angular.isUndefined(reasonCode) || reasonCode == null ? '' : reasonCode.reasonCode,
                reasonCode: reasonCode, 
                priority: priorityStr,
                dateTime: UtilsService.serverFommateDate(new Date($scope.config.queryData))
            };
            if($scope.config.queryHistoryChange){
                reasonCodeService
                    .reasonRuleHisChangeLog(params)
                    .then(function (resultDatas){
                        $scope.bodyConfig.overallWatchChanged = false
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].reasonCodePO.effDateTimeParsed = $filter('date')(new Date(resultDatas.response[i].reasonCodePO.effDateTime), 'yyyy-MM-dd HH:mm:ss');
                                resultDatas.response[i].reasonCodePO.effEndDateTimeParsed = $filter('date')(new Date(resultDatas.response[i].reasonCodePO.effEndDateTime), 'yyyy-MM-dd HH:mm:ss');
                                resultDatas.response[i].modifiedPO.modifiedDateTimeDesc = $filter('date')(new Date(resultDatas.response[i].modifiedPO.modifiedDateTime), 'yyyy-MM-dd HH:mm:ss');
                            }
                            $scope.rulesInfoGrid.data = resultDatas.response;
                            return;
                        }else{
                            $scope.rulesInfoGrid.data = [];
                            $scope.addAlert('', '暂无数据!');
                        }
                    }, function (errorResultDatas){
                        $scope.rulesInfoGrid.data = [];
                        $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
                    });
            }
            if($scope.config.queryHistoryStatus){
                reasonCodeService
                    .reasonRuleHisStatusLog(params)
                    .then(function (resultDatas){ 
                        $scope.bodyConfig.overallWatchChanged = false;
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].reasonCodePO.effDateTimeParsed = $filter('date')(new Date(resultDatas.response[i].reasonCodePO.effDateTime), 'yyyy-MM-dd HH:mm:ss');
                                resultDatas.response[i].reasonCodePO.effEndDateTimeParsed = $filter('date')(new Date(resultDatas.response[i].reasonCodePO.effEndDateTime), 'yyyy-MM-dd HH:mm:ss');
                                resultDatas.response[i].modifiedPO.modifiedDateTimeDesc = $filter('date')(new Date(resultDatas.response[i].modifiedPO.modifiedDateTime), 'yyyy-MM-dd HH:mm:ss');
                            }
                            $scope.rulesInfoGrid.data = resultDatas.response;
                            return;
                        }else{
                            $scope.rulesInfoGrid.data = [];
                            $scope.addAlert('', '暂无数据!');
                        }
                    }, function (errorResultDatas){
                        $scope.rulesInfoGrid.data = [];
                        $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
                    });
            }
        }; 
}]);

/**
 * 
 * [原因代码规则管理界面--原因代码选择Model界面 所对应 Controller: ModelReasonCodeCtrl]
 */
reasonCodeModule.controller('ModelReasonCodeCtrl', [ 
    '$scope', '$uibModalInstance', 'modelReasonCodeData',
    'reasonCodeService','$timeout',
    function ($scope, $uibModalInstance, modelReasonCodeData,
        reasonCodeService,$timeout){

    $scope.config = {
        gridApi: null,
        isFirstSelect:true,
        btnDisabledCancel: false,        // 是否禁用<取消>按钮
        btnDisabledOk: true              // 是否禁用<确定>按钮
    };

    $scope.selectReasonCodeOptions = {
        onRegisterApi: __onRegisterApi, 
        // data: [{reasonCode:'code one'}], 
        data: [],
        multiSelect: true, 
        columnDefs: [ 
            { 
                field: 'handle', name: 'handle', visible: false
            },
            {
                field: 'isSelected', name: 'isSelected', displayName: '选择', minWidth: 60, maxWidth: 60,
                enableCellEdit: false, type: 'boolean', visible: false,
                enableColumnMenu: false, enableColumnMenus: false, enableSorting: false 
            },
            {
                field: 'reasonCode', name: 'reasonCode', displayName: '原因代码', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'reasonCodeDesc', name: 'reasonCodeDesc', displayName: '原因代码描述', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            { 
                field: 'reasonCodeEnabled', name: 'reasonCodeEnabled', displayName: '原因代码是否已启用', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTemplate: 'andon-ui-grid-tpls/reasonCodeSelectModel-Enabled',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'reasonCodeGroup', name: 'reasonCodeGroup', displayName: '原因代码组', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'reasonCodeGroupDesc', name: 'reasonCodeGroupDesc', displayName: '原因代码组描述', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'reasonCodeGroupEnabled', name: 'reasonCodeGroupEnabled', displayName: '原因代码组是否已启用', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false, 
                cellTemplate: 'andon-ui-grid-tpls/reasonCodeSelectModel-groupEnabled',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            } 
        ]
    };  

    $scope.init = function(){
        var multi = modelReasonCodeData.multiSelect;
        if( !angular.isUndefined(multi) && multi != null ){     
            $scope.selectReasonCodeOptions.multiSelect = multi; 
        }   
        var reasonCode = modelReasonCodeData.reasonCode;
        reasonCodeService
            .reasonRuleDistinguish(reasonCode)
            .then(function (resultDatas){ 
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.selectReasonCodeOptions.data = resultDatas.response;
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return; 
                } 
                $scope.selectReasonCodeOptions.data = [];
            }, function (resultDatas){
                $scope.selectReasonCodeOptions.data = []; 
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            }); 
    };
    $scope.init();

    /**
     * 
     * [ok 点击确定按钮时触发该函数]
     * 
     */
    $scope.ok = function(){
        var reasonCodeS = [];
        var rows = $scope.config.gridApi.selection.getSelectedGridRows();
        for(var i = 0; i < rows.length; i++){
            var entity = rows[i].entity;
            reasonCodeS.push(entity);
        }
        $uibModalInstance.close({
            // reasonCode: angular.copy(gridRow.entity)
            reasonCodes: reasonCodeS
        });
    };

    /**
     * 
     * [cancel 点击取消按钮时触发该函数]
     * 
     */
    $scope.cancel = function(){
        $uibModalInstance.close();
    };

    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
         //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
            var count = $scope.config.gridApi.selection.getSelectedCount();
            $scope.config.btnDisabledOk = count > 0 ? false : true;
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            var count = $scope.config.gridApi.selection.getSelectedCount();
            $scope.config.btnDisabledOk = count > 0 ? false : true;
        }); 
    };  
}]);

/**
 * 
 * [cancel 点击取消按钮时触发该函数]
 * 
 */ 
reasonCodeModule.controller('ModelReasonCodeRuleDetailCtrl', [ 
    '$scope', '$uibModalInstance', 'modelRuleDetailData', '$uibModal',
    'reasonCodeService', '$filter', 'UtilsService', 'HttpAppService',
    function ($scope, $uibModalInstance, modelRuleDetailData, $uibModal,
            reasonCodeService, $filter, UtilsService, HttpAppService){

    $scope.config = { 
        gridApi: null, 

        detailCanEdit: false,  // 规则明细是否可以编辑

        gridApiParams: null,            // 识别方式作业参数表格对象
        gridApiPreventStay: null,       // 防呆验证作业参数表格对象

        btnDisabledEditParams: false,   // 识别方式作业是否可以编辑
        btnDisabledSaveParams: true,    // 识别方式标签页保存按钮是否可用
        btnDisabledCancelParams: false, // 识别方式标签页取消按钮是否可用

        btnDisabledEditStay: false,
        btnDisabledSaveStay: true,
        btnDisabledCancelStay: false,

        btnDisabledEditStayParams: false,       // 防呆验证是否可以编辑
        btnDisabledSaveStayParams: true,        // 防呆验证标签页保存按钮是否可用
        btnDisabledCancelStayParams: false,     // 防呆验证标签页取消按钮是否可用

        currentIndentify: null,                 // 当前所选识别方式作业
        currentPreventStay: null,               // 当前所选防呆验证作业

        currentAction: '',                      // 防呆验证-参数值-By设备类型时，所选操作
        hasSelectedRow: true, 
        currentAction2: '',                     // 识别方式-参数值-By设备类型时，所选操作
        hasSelectedRow2: true,

        detailDataBack: null,
        detailData: null,

        preventStayActivityHasChanged: false,

        canEdit: false              // 当前用户是否对该activity有编辑权限
        // { config.detailData
        //     reasonCode: 
        //     reasonCodeDesc:
        //     priority: 
        //     subPriority:
        //     effDateTime:
        //     used: 
        //     modifiedUser:

        // } 
    };

    /**
     * 
     * [actionFChanged2 识别方式标签页 - By设备类型 - 点击使用默认值、设定值时触发]
     * 
     */
    $scope.actionFChanged2 = function(type){ 
        if(UtilsService.isEmptyString(type)){
            return;
        }   
        $scope.config.currentAction2 = type;

        var rows = $scope.config.gridApiParams.selection.getSelectedGridRows();
        for(var i = 0; i < rows.length; i++){ 
            rows[i].entity.actionType = $scope.config.currentAction2;
            rows[i].entity.hasChanged = true;
            rows[i].entity.resourceTypeDesc = '无';
            rows[i].entity.modifiedDateTimeStr = "";
            rows[i].entity.modifiedUser = "";
            if($scope.config.currentAction2 == "default"){ 
                rows[i].entity.paramValue = rows[i].entity.paramSetValue = rows[i].entity.paramDefaultValue;
            }else if($scope.config.currentAction2 == "set"){ 
                rows[i].entity.paramValue = rows[i].entity.paramSetValue = ""; 
            }else if($scope.config.currentAction2 == "notUse"){ 
                rows[i].entity.paramValue = rows[i].entity.paramSetValue = "";
            }   
        }
    };
     /**
     * 
     * [actionFChanged 防呆验证标签页 - By设备类型 - 点击使用默认值、设定值时触发]
     * 
     */
    $scope.actionFChanged = function(type){
        if(UtilsService.isEmptyString(type)){
            return;
        }   
        $scope.config.currentAction = type;

        var rows = $scope.config.gridApiPreventStay.selection.getSelectedGridRows();
        for(var i = 0; i < rows.length; i++){ 
            rows[i].entity.actionType = $scope.config.currentAction;
            rows[i].entity.hasChanged = true;
            rows[i].entity.resourceTypeDesc = '无';
            rows[i].entity.modifiedDateTimeStr = "";
            rows[i].entity.modifiedUser = "";
            if($scope.config.currentAction == "default"){ 
                rows[i].entity.paramValue = rows[i].entity.paramSetValue = rows[i].entity.paramDefaultValue;
            }else if($scope.config.currentAction == "set"){ 
                rows[i].entity.paramValue = rows[i].entity.paramSetValue = ""; 
            }else if($scope.config.currentAction == "notUse"){ 
                rows[i].entity.paramValue = rows[i].entity.paramSetValue = "";
            }   
        }
    };  
    /**
     * 
     * [changeUppercase 转大写]
     * 
     */
    $scope.changeUppercase = function(){
        if(!angular.isUndefined($scope.config.detailData.activityR) && $scope.config.detailData.activityR != null){
            $scope.config.detailData.activityR = $scope.config.detailData.activityR.toUpperCase();
        }   
        if(!angular.isUndefined($scope.config.detailData.activityF) && $scope.config.detailData.activityF != null){
            $scope.config.detailData.activityF = $scope.config.detailData.activityF.toUpperCase();
        }   
    };  
    
    $scope.ruleDetailInfoOptions = {
        onRegisterApi: __onRegisterApi,
        data: [{reasonCode:'code one'}],
        columnDefs: [ 
            {   
                field: 'handle', name: 'handle', visible: false
            }, 
            {   
                field: 'isSelected', name: 'isSelected', displayName: '选择', minWidth: 60, maxWidth: 60,
                enableCellEdit: false, type: 'boolean', visible: false,
                enableColumnMenu: false, enableColumnMenus: false, enableSorting: false 
            },
            {   
                field: 'reasonCode', name: 'reasonCode', displayName: '原因代码', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false, 
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {   
                field: 'description', name: 'description', displayName: '原因代码描述', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {   
                field: 'plcObject', name: 'plcObject', displayName: '原因代码是否已启用', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'plcObjectDescription', name: 'plcObjectDescription', displayName: '原因代码组', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'startData', name: 'startData', displayName: '原因代码组描述', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'isUsed', name: 'isUsed', displayName: '原因代码组是否已使用', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false, 
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            }
        ]
    };
    
    /**
     * 
     * [paramsGridOptions 识别方式作业参数表格元数据定义]
     * 
     */
    $scope.paramsGridOptions = {    
        onRegisterApi: __onRegisterApiParams,
        data: [],
        columnDefs: [ 
            {   
                field: 'handle', name: 'handle', visible: false
            },  
            {   
                field: 'paramId', name: 'sequenceId', displayName: '参数序号', minWidth: 90,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },  
            {   
                field: 'paramDescription', name: 'description', displayName: '参数描述', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },  
            {   
                field: 'paramDefaultValue', name: 'classOrProgram', displayName: '参数默认值', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'paramValue', name: 'paramValue', displayName: '参数设定值', minWidth: 200,
                enableCellEdit: true, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellTemplate: 'andon-ui-grid-tpls/reasonCodeRule-params-paramValue',
                cellEditableCondition: function(scope){
                    if(!$scope.config.btnDisabledEditParams){ 
                        return false;
                    }else{
                        var row = arguments[0].row; 
                        var col = arguments[0].col; 
                        var entity = row.entity;
                        if(entity.actionType == 'default' || entity.actionType == 'notUse'){ 
                            return false;
                        }
                        if(entity.actionType == 'set'){
                            return true;
                        }
                    }
                }
            },
            {   
                field: 'modifiedDateTimeStr', name: 'modifiedDateTimeStr', displayName: '最后修改日期', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'modifiedUser', name: 'modifiedUser', displayName: '最后修改人', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false,  cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            }
        ]
    };  
    function __onRegisterApiParams(gridApi){
        $scope.config.gridApiParams = gridApi;
        gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
            if(newRowCol.col.field == "paramValue" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                // $scope.openPreventStayParamsModel(newRowCol);
                // if(newRowCol.row.entity.resourceTypeDesc == "无"){
                // }else{
                //     $scope.openPreventStayParamsModel(newRowCol);
                // }
            }
            if(newRowCol.col.field == "paramValue" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                // $scope.openPreventStayParamsModel(newRowCol);
            }   
        }); 
        gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
            if(colDef.name == "paramValue"  && !UtilsService.isEmptyString(newValue) && newValue!= oldValue){
                rowEntity.paramSetValue = rowEntity.paramValue;
                rowEntity.hasChanged = true; 
            }
            if(colDef.name == "resourceTypeDesc" && newValue!=oldValue){
                rowEntity.paramSetValue = "";
                rowEntity.paramValue = "";
            }
        }); 
        //选择相关 
        gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
            var count = gridApi.selection.getSelectedCount();
            $scope.config.hasSelectedRow2 = count > 0 ? false : true;
        });
        gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            var count = gridApi.selection.getSelectedCount();
            $scope.config.hasSelectedRow2 = count > 0 ? false : true;
        });
    }
    
    $scope.init = function(){
        $scope.config.canEdit = modelRuleDetailData.canEdit;
        __requestDetailDatas();
    };  
    $scope.init();  
    
    function __requestDetailDatas(needReloadFParams){
        reasonCodeService
            .reasonRuleDetailAll(modelRuleDetailData.ruleRowEntity.reasonCodePO.reasonCodePriorityBo)
            .then(function(resultDatas){
                resultDatas.response.modifiedDateTimeStr = $filter('date')(new Date(resultDatas.response.modifiedDateTime), 'yyyy-MM-dd HH:mm:ss');
                resultDatas.response.effDateTimeStr = $filter('date')(new Date(resultDatas.response.effDateTime), 'yyyy-MM-dd HH:mm:ss');
                if(!angular.isUndefined(resultDatas.response.effEndDateTime)){
                    resultDatas.response.effEndDateTimeStr = $filter('date')(new Date(resultDatas.response.effEndDateTime), 'yyyy-MM-dd HH:mm:ss');
                }   
                if(!angular.isUndefined(resultDatas.response.modifiedDateTimeR)){
                    resultDatas.response.modifiedDateTimeRStr = $filter('date')(new Date(resultDatas.response.modifiedDateTimeR), 'yyyy-MM-dd HH:mm:ss');
                }   
                if(!angular.isUndefined(resultDatas.response.modifiedDateTimeF)){
                    resultDatas.response.modifiedDateTimeFStr = $filter('date')(new Date(resultDatas.response.modifiedDateTimeF), 'yyyy-MM-dd HH:mm:ss');
                }   
                
                // resultDatas.response. = resultDatas.response.resourceType;
                $scope.config.detailData = resultDatas.response; 
                $scope.config.detailDataBack = angular.copy($scope.config.detailData); 
                if(!UtilsService.isEmptyString(resultDatas.response.used) && resultDatas.response.used == 'true'){
                    $scope.config.detailCanEdit = false;
                    $scope.config.btnDisabledEditStayParams = false;
                    $scope.config.btnDisabledEditParams = false;
                    $scope.config.btnDisabledSaveStayActivity = true;
                    $scope.config.btnDisabledSaveStayParams = true;
                }else{
                    $scope.config.detailCanEdit = true;
                }
                var activity = $scope.config.detailData.activityBoF; 
                var paramsF = $scope.config.detailData.activityFParam;
                // if(angular.isUndefined(paramsF) || paramsF == null){
                //     __requestPreventStayTableDatas(activity); 
                // }   
                // __reasonRuleCheckInputHelp(activity, 'R'); 
                $scope.paramsGridOptions.data = UtilsService.isEmptyArray(resultDatas.response.activityRParam) ? [] : resultDatas.response.activityRParam;
                $scope.PreventStayGridOptions.data = UtilsService.isEmptyArray(resultDatas.response.activityFParam) ? [] : resultDatas.response.activityFParam;
                for(var i = 0; i < $scope.paramsGridOptions.data.length; i++){
                    var type = $scope.paramsGridOptions.data[i].resourceType;
                    $scope.paramsGridOptions.data[i].resourceTypeDesc = UtilsService.isEmptyString(type) || type=="*" ? "无" : 'By设备类型';
                    var modifiedDateTime = $scope.paramsGridOptions.data[i].modifiedDateTime;
                    if(!UtilsService.isEmptyString(modifiedDateTime)){
                        $scope.paramsGridOptions.data[i].modifiedDateTimeStr = $filter('date')(new Date(modifiedDateTime), 'yyyy-MM-dd HH:mm:ss');
                    }
                }
                for(var i = 0; i < $scope.PreventStayGridOptions.data.length; i++){
                    var type = $scope.PreventStayGridOptions.data[i].resourceType;
                    $scope.PreventStayGridOptions.data[i].resourceTypeDesc = UtilsService.isEmptyString(type) || type=="*" ? "无" : 'By设备类型';
                    var modifiedDateTime = $scope.PreventStayGridOptions.data[i].modifiedDateTime;
                    if(!UtilsService.isEmptyString(modifiedDateTime)){
                        $scope.PreventStayGridOptions.data[i].modifiedDateTimeStr = $filter('date')(new Date(modifiedDateTime), 'yyyy-MM-dd HH:mm:ss');
                    }
                }
                if(!angular.isUndefined(needReloadFParams) && needReloadFParams != null && needReloadFParams == true){
                    __requestPreventStayTableDatas(activity); 
                }
            }, function(errorResultDatas){
                $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc); 
            }); 
    }   
    function __requestPreventStayTableDatas(activity, needRequestFParams){ 
        if(UtilsService.isEmptyString(activity)){
            return;
        }
        var executionType = "F";
        reasonCodeService 
            // .reasonRuleCheckInputHelp(activity)
            .reasonRuleActivityParam(activity, executionType) 
            .then(function(resultDatas){ 
                // resultDatas.response.modifiedDateTimeStr = $filter('date')(new Date(resultDatas.response.modifiedDateTime), 'yyyy-MM-dd');
                // resultDatas.response.effDateTimeStr = $filter('date')(new Date(resultDatas.response.effDateTime), 'yyyy-MM-dd');
                for(var i = 0; i < resultDatas.response.length; i++){
                    if(!UtilsService.isEmptyString(resultDatas.response[i].modifiedDateTime)){
                        resultDatas.response[i].modifiedDateTimeStr = $filter('date')(new Date(resultDatas.response[i].modifiedDateTime), 'yyyy-MM-dd HH:mm:ss');
                    }
                    var eType = resultDatas.response[i].executionType;
                    resultDatas.response[i].resourceTypeDesc = angular.isUndefined(eType) || eType == null || eType == "" || eType == "*" ? '无' : 'By设备类型';
                }
                $scope.PreventStayGridOptions.data = resultDatas.response;
            }, function(errorResultDatas){
                $scope.PreventStayGridOptions.data = [];
                $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
            });
    }
    function __reasonRuleCheckInputHelp(activity){
        var executionType = "R";
        reasonCodeService
            .reasonRulePatternDetail(activity, executionType)
            .then(function(resultDatas){
                // resultDatas.response.modifiedDateTimeStr = $filter('date')(new Date(resultDatas.response.modifiedDateTime), 'yyyy-MM-dd');
                // resultDatas.response.effDateTimeStr = $filter('date')(new Date(resultDatas.response.effDateTime), 'yyyy-MM-dd');
                $scope.paramsGridOptions.data = resultDatas.response;
            }, function(errorResultDatas){
                $scope.paramsGridOptions.data = [];
                $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
            });
    }
    /**
     * 
     * [editParams 识别方式 - 点击编辑按钮时触发该函数]
     * 
     */
    $scope.editParams = function(){
        $scope.config.btnDisabledEditParams = true;
        $scope.config.btnDisabledSaveParams = false;
    };  
    function __identifyParamsHasChanged(){
        if($scope.config.detailData.activityR != $scope.config.detailDataBack.activityR){
            return true;
        }
        for(var i = 0; i < $scope.paramsGridOptions.data.length; i++){
            var data = $scope.paramsGridOptions.data[i];
            if(data.hasChanged){
                return true;
            }
        }
        return false;
    }
    function __getActivityCodeR(){ 
        if($scope.config.detailData.activityBoR == $scope.config.detailDataBack.activityBoR){ 
            return 'CHANGE_PARAMETER';
        }
        return 'CHANGE_ACTIVITY';
    }   
    function __getActivityCodeF(){
        if($scope.config.detailData.activityBoF == $scope.config.detailDataBack.activityBoF){ 
            return 'CHANGE_PARAMETER';
        }
        return 'CHANGE_ACTIVITY';
    }   
    /**
     * 
     * [saveParams 识别方式 - 点击保存按钮时触发该函数]
     * 
     */
    $scope.saveParams = function(){
        if(!__identifyParamsHasChanged()){
            $scope.addAlert('', "没有修改,无需保存!");
            return;
        }   
        for(var i = 0; i < $scope.paramsGridOptions.data.length; i++){
            $scope.paramsGridOptions.data[i].paramSetValue = $scope.paramsGridOptions.data[i].paramValue;
        }   
        var params = {
            reasonCodePriorityBO: $scope.config.detailData.reasonCodePriorityBo,
            site: HttpAppService.getSite(),  // activityBoR
            activity: $scope.config.detailData.activityR, //$scope.config.detailData.activityBoR, //$scope.config.currentIndentify.handle,
            // activityCode: $scope.config.currentIndentify.activity,
            activityCode: __getActivityCodeR(),
            ruleParams: $scope.paramsGridOptions.data
        };  
        reasonCodeService
            .reasonRulePatternUpdate(params)
            .then(function(resultDatas){
                $scope.addAlert('success', "保存成功!");
                $scope.config.btnDisabledSaveParams = true;
                $scope.config.btnDisabledEditParams = false;
                __requestDetailDatas();
            }, function(resultDatas){  
                $scope.addAlert('', "保存失败! "+resultDatas.myHttpConfig.statusDesc); 
            }); 
    };  
    /**
     * 
     * [cancelParams 识别方式 - 点击取消按钮时触发该函数]
     * 
     */
    $scope.cancelParams = function(){
        $uibModalInstance.close();
    };  
    /**
     * 
     * [openIndentifySelectModel 打开识别方式作业选择Model界面]
     * 
     */
    $scope.openIndentifySelectModel = function(){ 
        if($scope.config.btnDisabledSaveParams){ 
            return true; 
        }   
        var modalInstance = $uibModal.open({ 
            animation: true,
            templateUrl: 'modules/reasonCode/identifyRulesManager/model/m_identify_select.html',
            controller: 'IndentifySelectCtrl',
            size: 'lg',
            backdrop: 'false',
            openedClass: 'flex-center-parent',
            scope : $scope,
            resolve: {
               modelReasonCodeData: function (){
                    return {
                        activity: $scope.config.detailData.activityR
                    };
               }    
            }       
        });         
        modalInstance.result.then(function (closeDataObj){ 
            var indentify = closeDataObj.indentify; 
            $scope.config.currentIndentify = indentify; 
            $scope.config.detailData.activityR = indentify.activity; 
            $scope.config.detailData.activityBoR = indentify.handle; 
            $scope.config.detailData.activityRDesc = indentify.description; 
            
            // $scope.config.detailData.modifiedDateTimeStr = ""; 
            // $scope.config.detailData.modifiedDateTime = ""; 
            // $scope.config.detailData.modifiedUser = ""; 
            $scope.config.detailData.modifiedDateTimeRStr = ""; 
            // $scope.config.detailData.modifiedDateRTime = ""; 
            $scope.config.detailData.modifiedUserR = ""; 
            
            // $scope.config.btnDisabledEditParams = false; 
            // $scope.config.btnDisabledSaveParams = true; 
            
            __reasonRuleCheckInputHelp(indentify.handle); 
        }, function(){ }); 
    };  
    
    /**
     * 
     * [PreventStayGridOptions 防呆验证作业参数表格元数据]
     * 
     */
    $scope.PreventStayGridOptions = { 
        onRegisterApi: __onRegisterApiPreventStay,  
        data: [],   
        columnDefs: [ 
            {   
                field: 'handle', name: 'handle', visible: false
            },
            {   
                field: 'paramId', name: 'paramId', displayName: '参数序号', minWidth: 90,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {   
                field: 'paramDescription', name: 'paramDescription', displayName: '参数描述', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {   
                field: 'paramDefaultValue', name: 'paramDefaultValue', displayName: '参数默认值', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            // { 
            //     field: 'actions', name: 'actions', displayName: '操作', minWidth: 280,
            //     enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable grid-xbr-actions',
            //     cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
            //     cellTemplate: 'andon-ui-grid-tpls/reasonCodeRule-PreventStay-param-actions'
            // },
            {
                field: 'resourceTypeDesc2', name: 'resourceTypeDesc2', visible: false 
            },
            {   
                field: 'resourceTypeDesc', name: 'resourceTypeDesc', displayName: '参数设置方式', minWidth: 200,
                enableCellEdit: true, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                editableCellTemplate: 'ui-grid/dropdownEditor',
                // cellTemplate: 'andon-ui-grid-tpls/reasonCode-recognition-rule-param-f-resourceTypeDesc',
                editDropdownIdLabel: 'resourceTypeDesc', editDropdownValueLabel: 'resourceTypeDesc',
                // $scope.config.plcProtocols   
                editDropdownOptionsFunction: function(rowEntity, colDef){
                    return [
                        { resourceTypeDesc: 'W', resourceTypeDesc: 'By设备类型' },
                        { resourceTypeDesc: '*', resourceTypeDesc: '无' }
                    ];
                },  
                cellEditableCondition: function(scope){
                    return $scope.config.btnDisabledEditStayParams;
                }
            },
            {   
                field: 'paramValue', name: 'paramValue', displayName: '参数设定值', minWidth: 200,
                enableCellEdit: true, enableCellEditOnFocus: false,
                cellTemplate: 'andon-ui-grid-tpls/reasonCodeRule-PreventStay-paramValue',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellEditableCondition: function(scope){
                    if(!$scope.config.btnDisabledEditStayParams){ 
                        return false;
                    }else{
                        var row = arguments[0].row; 
                        var col = arguments[0].col; 
                        var entity = row.entity;    
                        // row.entity.=='无' && row.=='default'  
                        if(entity.resourceTypeDesc == 'By设备类型'){ 
                            return false; 
                        } 
                        if(entity.actionType == 'default' || entity.actionType == 'notUse'){ 
                            return false;
                        }
                        if(entity.actionType == 'set'){
                            return true;
                        }
                    }
                }
            },
            {   
                field: 'modifiedDateTimeStr', name: 'modifiedDateTimeStr', displayName: '最后修改日期', minWidth: 150, 
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable', 
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; } 
            },  
            {   
                field: 'modifiedUser', name: 'modifiedUser', displayName: '最后修改人', minWidth: 150, 
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable', 
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; } 
            }
        ]
    };  
    
    /**
     * 
     * [editPreventStay 防呆验证 - 点击编辑按钮时触发该函数]
     * 
     */
    $scope.editPreventStay = function(){
        $scope.config.btnDisabledEditStayParams = true; 
        $scope.config.btnDisabledSaveStayParams = false; 
    };  
    
    /**
     * 
     * [savePreventStayActivity 点击防呆验证上方保存按钮时触发该函数]
     * 
     */
    $scope.savePreventStayActivity = function(){
        var activityBo = $scope.config.detailData.activityBoF; 
        var executionType = "F"; 
        var stay = $scope.config.currentPreventStay;
        var stayHandle;
        if(angular.isUndefined(stay) || stay == null ){
            // stayHandle
        }else{
            stayHandle = stay.handle;
        }
        var params = { 
            actionCode: 'CHANGE_ACTIVITY', 
            reasonCodePriorityBo: $scope.config.detailData.reasonCodePriorityBo, 
            // activityCode: 'CHANGE_ACTIVITY', //__getActivityCodeF(),
            activity: $scope.config.detailData.activityF, 
            activityBo: stayHandle,  // $scope.config.detailData.activityBoF, //
            modifiedDateTime: $scope.config.detailData.modifiedDateTimeF, 
            ruleParam: $scope.PreventStayGridOptions.data 
        };  
        reasonCodeService 
            .reasonRuleParamFChange(params)
            .then(function (){
                $scope.addAlert('success', '保存成功!');
                $scope.config.currentAction = "";
                $scope.config.hasSelectedRow = true;
                $scope.config.preventStayActivityHasChanged = false;
                __requestDetailDatas(true);
                //$scope.config.detailData.modifiedDateTimeF = 
            }, function(resultDatas){
                $scope.addAlert('success', '保存失败!  '+resultDatas.myHttpConfig.statusDesc);
            });
    };  
    /**
     * 
     * [savePreventStayParams 保存防呆验证下方保存按钮时触发该函数]
     * 
     */
    $scope.savePreventStayParams = function(){
        if($scope.config.preventStayActivityHasChanged){
            $scope.addAlert('', '请先保存防呆验证作业!');
            return;
        }
        var activityBo = $scope.config.detailData.activityBoF;
        var executionType = "F";
        // var ruleParams = [];
        for( var i = 0; i < $scope.PreventStayGridOptions.data.length; i++){ 
            $scope.PreventStayGridOptions.data[i].handle = $scope.PreventStayGridOptions.data[i].reasonCodeRuleParamBo;
            // ruleParams.push();
        }
        var ruleParams = [];
        // ruleParams = $scope.PreventStayGridOptions.data;
        for(var i = 0; i < $scope.PreventStayGridOptions.data.length; i++){
            var dx = $scope.PreventStayGridOptions.data[i];
            dx.handle = dx.reasonCodeRuleParamBo;
            if(dx.resourceTypeDesc == 'By设备类型'){
                dx.resourceType = 'By设备类型';
            }else{
                dx.resourceTypeDesc = '无';
                dx.resourceType = '*';
            }
            // if(!angular.isUndefined(dx.paramSetValue) && dx.paramSetValue!= null && dx.paramSetValue!=""){
                ruleParams.push(dx);
            // }
        }
        if(ruleParams.length == 0){
            $scope.addAlert('', '暂无修改，无须保存!');
            return;
        }
        var params = {  
            actionCode: 'CHANGE_PARAMETER',
            reasonCodePriorityBo: $scope.config.detailData.reasonCodePriorityBo,
            activity: $scope.config.detailData.activityF,
            handle: $scope.config.detailData.reasonCodeRuleBoF,
            reasonCodeRuleBo: $scope.config.detailData.reasonCodeRuleBoF,
            // activityCode: '', //__getActivityCodeF(),
            activityBo: $scope.config.detailData.activityBoF, //$scope.config.currentIndentify.handle,
            modifiedDateTime: $scope.config.detailData.modifiedDateTimeF,
            ruleParam: ruleParams
        }; 
        reasonCodeService 
            .reasonRuleParamFChange(params) 
            .then(function (resultDatas){ 
                //$scope.config.detailData.modifiedDateTimeF = 
                $scope.addAlert('success', '保存成功!'); 
                $scope.PreventStayGridOptions.data = [];
                $scope.config.currentAction = "";
                $scope.config.hasSelectedRow = true;
                __requestDetailDatas(); 
                $scope.config.btnDisabledEditStayParams = false; 
                $scope.config.btnDisabledSaveStayParams = true; 
            }, function(resultDatas){ 
                $scope.addAlert('success', '保存失败!  '+resultDatas.myHttpConfig.statusDesc); 
            });
    };   
    /**
     * 
     * [cancelPreventStay 防呆验证-点击取消按钮触发该函数]
     * 
     */   
    $scope.cancelPreventStay = function(){
        $uibModalInstance.close(); 
    };      
    
    /**
     * 
     * [openPreventStaySelectModel 打开防呆验证选择Model界面]
     * 
     */
    $scope.openPreventStaySelectModel = function(){
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modules/reasonCode/identifyRulesManager/model/m_PreventStay_select.html',
            controller: 'PreventStaySelectCtrl',
            size: 'lg',
            backdrop: 'false',
            openedClass: 'flex-center-parent',
            scope : $scope,
            resolve: {
                modelReasonCodeData: function (){
                    return {
                        activity: $scope.config.detailData.activityF
                    };
                }
            }
        });     
        modalInstance.result.then(function (closeDataObj){
            $scope.config.preventStayActivityHasChanged = true;
            $scope.PreventStayGridOptions.data = [];
            $scope.config.currentAction = "";
            $scope.config.hasSelectedRow = true;

            var PreventStay = closeDataObj.PreventStay; 
            $scope.config.currentPreventStay = PreventStay; 
            $scope.config.detailData.activityF = PreventStay.activity; 
            $scope.config.detailData.activityFHandle = PreventStay.handle; 
            $scope.config.detailData.activityFDesc = PreventStay.description; 

            // $scope.config.detailData.modifiedDateTimeStr = ""; 
            // $scope.config.detailData.modifiedDateTime = ""; 
            // $scope.config.detailData.modifiedUser = ""; 
            $scope.config.detailData.modifiedDateTimeFStr = ""; 
            // $scope.config.detailData.modifiedDateTimeF = ""; 
            $scope.config.detailData.modifiedUserF = ""; 

            // __requestPreventStayTableDatas(PreventStay.handle); 
            // __requestPreventStayTableDatas($scope.config.detailData.reasonCodeRuleBoR); 
        }, function(){ }); 
    };      
    function __onRegisterApiPreventStay(gridApi){ 
        $scope.config.gridApiPreventStay = gridApi;
        gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
            if(newRowCol.col.field == "paramValue" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                if(newRowCol.row.entity.resourceTypeDesc == "无"){
                }else{
                    $scope.openPreventStayParamsModel(newRowCol);
                }   
                // else if($scope.config.btnDisabledEditStayParams){
                //     $scope.openPreventStayParamsModel(newRowCol);
                // }
            }   
            if(newRowCol.col.field == "paramValue" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                // $scope.openPreventStayParamsModel(newRowCol);
            }   
        }); 
        gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
            if(colDef.name == "paramValue"  && !UtilsService.isEmptyString(newValue) && newValue!= oldValue){
                rowEntity.paramSetValue = rowEntity.paramValue;
                rowEntity.hasChanged = true; 
            }
            if(colDef.name == "resourceTypeDesc"){
                rowEntity.paramSetValue = "";
                rowEntity.paramValue = "";
            }
        }); 
        //选择相关 
        gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
            var count = gridApi.selection.getSelectedCount();
            $scope.config.hasSelectedRow = count > 0 ? false : true;
        });
        gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            var count = gridApi.selection.getSelectedCount();
            $scope.config.hasSelectedRow = count > 0 ? false : true;
        });
    }   
    
    /**
     * 
     * [cancelPreventStayParams 防呆验证取消按钮触发该函数]
     * 
     */
    $scope.cancelPreventStayParams = function(){
        $uibModalInstance.close();
    };  
    
    /**
     * 
     * [openPreventStayParamsModel 打开防呆验证参数表格Model设置界面]
     * 
     */
    $scope.openPreventStayParamsModel = function(newRowCol){
        var entity = newRowCol.row.entity;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modules/reasonCode/identifyRulesManager/model/m_PreventStay_params.html',
            controller: 'PreventStayParamsCtrl',
            size: 'lg',
            backdrop: 'static',
            openedClass: 'flex-center-parent',
            scope : $scope,
            resolve: {
               preventStayParamsDatas: function (){
                   return {
                        preventStayEntity: entity,
                        detailData: $scope.config.detailData
                   };
               }
            }
        }); 
        modalInstance.result.then(function (closeDataObj){
            __requestDetailDatas();
        }, function(){ });
    };      
    
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
            console.log(gridRow);
            $uibModalInstance.close({
                reasonCode: angular.copy(gridRow.entity)
            });
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            // TODO 不允许多选
        }); 
    };      
    
    /**
     * 
     * [editPreventStayParams 防呆验证标签页-点击编辑按钮时触发该函数]
     * 
     */
    $scope.editPreventStayParams = function(){
        $scope.config.btnDisabledEditStayParams = true;
        $scope.config.btnDisabledSaveStayParams = false;
    };       

}]);

/**
 * 
 * [原因代码规则管理界面--规则详细信息Model界面-识别方式作业选择Model界面 所对应 Controller: IndentifySelectCtrl]
 */
reasonCodeModule.controller('IndentifySelectCtrl', [ 
    '$scope', '$uibModalInstance', 'modelReasonCodeData', 'reasonCodeService','$timeout',
    function ($scope, $uibModalInstance, modelReasonCodeData, reasonCodeService,$timeout) {

    $scope.config = {
        gridApi: null,
        isFirstSelect:true
    };

    $scope.init = function(){
        var activity = modelReasonCodeData.activity;
        __requestTableDatas(activity);
    };

    $scope.init();
    function __requestTableDatas(activity){
        reasonCodeService
            .reasonRuleDistinguishInputHelp(activity)
            .then(function(resultDatas){ 
                $scope.indentifyOptions.data = resultDatas.response;
                if($scope.config.isFirstSelect)
                {
                    $timeout(function(){
                        $scope.config.isFirstSelect=false;
                        /* To hide the blank gap when use selecting and grouping */
                        $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                        $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                    },0);
                }
            }, function(errorResultDatas){ 
                $scope.indentifyOptions.data = []; 
                $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc); 
            }); 
    }

    $scope.indentifyOptions = {
        onRegisterApi: __onRegisterApi,
        multiSelect: false,
        data: [],
        columnDefs: [ 
            { 
                field: 'handle', name: 'handle', visible: false
            }, 
            {
                field: 'activity', name: 'activity', displayName: '识别方式作业', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'description', name: 'description', displayName: '作业描述', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            }
        ]
    };

    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
            $uibModalInstance.close({
                indentify: angular.copy(gridRow.entity)
            });
        });
    }; 
}]);

/**
 * 
 * [原因代码规则管理界面--规则详细信息Model界面-防呆验证作业选择Model界面 所对应 Controller: PreventStaySelectCtrl]
 */
reasonCodeModule.controller('PreventStaySelectCtrl', [ 
    '$scope', '$uibModalInstance', 'modelReasonCodeData', 'reasonCodeService',
    function ($scope, $uibModalInstance, modelReasonCodeData, reasonCodeService) { 

    $scope.config = {
        gridApi: null 
    };

    $scope.PreventStayOptions = { 
        onRegisterApi: __onRegisterApi,
        multiSelect: false,
        data: [],
        columnDefs: [  
            { 
                field: 'handle', name: 'handle', visible: false
            }, 
            {
                field: 'activity', name: 'activity', displayName: '防呆验证作业', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'description', name: 'description', displayName: '作业描述', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            }
        ] 
    };  

    $scope.init = function(){
        var activity = modelReasonCodeData.activity;
        __requestTableDatas(activity);
    };

    $scope.init(); 

    function __requestTableDatas(activity){
        reasonCodeService
            .reasonRuleCheckInputHelp(activity)
            .then(function(resultDatas){ 
                $scope.PreventStayOptions.data = resultDatas.response; 
            }, function(errorResultDatas){ 
                $scope.PreventStayOptions.data = []; 
                $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc); 
            }); 
    }

    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
            $uibModalInstance.close({
                PreventStay: angular.copy(gridRow.entity)
            });
        });
    }; 
}]);

/**
 * 
 * [原因代码规则管理界面--规则详细信息Model界面-防呆验证作业参数表格-参数值修改Model界面 所对应 Controller: PreventStayParamsCtrl]
 */
reasonCodeModule.controller('PreventStayParamsCtrl', [
    '$scope', '$uibModalInstance', 'preventStayParamsDatas',
    'reasonCodeService', '$filter', 'UtilsService',
    function ($scope, $uibModalInstance, preventStayParamsDatas,
        reasonCodeService, $filter, UtilsService) {

    $scope.config = {
        gridApi: null,

        btnDisabledEditParams: false,
        btnDisabledSaveParams: true,
        btnDisabledCancelParams: false,

        entity: null,

        hasSelectedRow: false,
        currentAction: ''
    };

    $scope.actionFChanged = function(type){
        if(UtilsService.isEmptyString(type)){
            return;
        }
        $scope.config.currentAction = type;

        var rows = $scope.config.gridApi.selection.getSelectedGridRows();
        for(var i = 0; i < rows.length; i++){ 
            rows[i].entity.actionType = $scope.config.currentAction;
            rows[i].entity.hasChanged = true;
            rows[i].entity.resourceTypeDesc = '无';
            rows[i].entity.modifiedDateTimeStr = "";
            rows[i].entity.modifiedUser = "";
            if($scope.config.currentAction == "default"){ 
                rows[i].entity.activityFParamValue = rows[i].entity.activityFParamDefaultValue;
            }else if($scope.config.currentAction == "set"){ 
                rows[i].entity.activityFParamValue = rows[i].entity.paramSetValue = ""; 
            }else if($scope.config.currentAction == "notUse"){ 
                rows[i].entity.activityFParamValue = rows[i].entity.paramSetValue = "";
            }   
        }
    };

    $scope.init = function(){
        var entity = preventStayParamsDatas.preventStayEntity;
        $scope.config.entity = entity;
        var reasonCodeRuleBoF = preventStayParamsDatas.detailData.reasonCodeRuleBoF; // TODO 
        $scope.config.reasonCodeRuleBoF = reasonCodeRuleBoF;
        var activityFParamId = entity.paramId;
        $scope.config.paramId = activityFParamId;
        __requestPreventStayParams(reasonCodeRuleBoF, activityFParamId);
    };
    $scope.init();

    function __requestPreventStayParams(reasonCodeRuleBoF, activityFParamId){
        reasonCodeService
            .reasonRuleDetailStayparams(reasonCodeRuleBoF, activityFParamId)
            .then(function (resultDatas){
                // for(var i = 0; i < resultDatas.response.length; i++){
                //     resultDatas.response[i].activityFParamModifiedDateTimeStr = $filter('date')(new Date(resultDatas.response[i].activityFParamModifiedDateTime), 'yyyy-MM-dd HH:mm:ss');
                // }   
                $scope.PreventStayParamsOptions.data = resultDatas.response.params;
                for(var i = 0; i < $scope.PreventStayParamsOptions.data.length; i++){
                    // activityFParamModifiedDateTime
                    if(!UtilsService.isEmptyString($scope.PreventStayParamsOptions.data[i].activityFParamModifiedDateTime)){
                        $scope.PreventStayParamsOptions.data[i].activityFParamModifiedDateTimeStr = $filter('date')(new Date($scope.PreventStayParamsOptions.data[i].activityFParamModifiedDateTime), 'yyyy-MM-dd HH:mm:ss');
                    }
                    $scope.PreventStayParamsOptions.data[i].activityFParamDefaultValue = $scope.config.entity.paramDefaultValue;
                }
            }, function(resultDatas){ 
                $scope.PreventStayParamsOptions.data = [];
                // TODO
            });
    }

    $scope.PreventStayParamsOptions = {
        onRegisterApi: __onRegisterApi,
        // multiSelect: false,
        data: [],
        columnDefs: [ 
            { 
                field: 'handle', name: 'handle', visible: false
            }, 
            {
                field: 'activityFParamResourceType', name: 'activityFParamResourceType', displayName: '设备类型', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'activityFParamResourceTypeDesc', name: 'activityFParamResourceTypeDesc', displayName: '设备类型描述', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'activityFParamDefaultValue', name: 'activityFParamDefaultValue', displayName: '默认值', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'activityFParamValue', name: 'activityFParamValue', displayName: '设定值', minWidth: 150,
                enableCellEdit: true, enableCellEditOnFocus: false, 
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellEditableCondition: function(scope){
                    var entity = scope.row.entity;
                    return $scope.config.btnDisabledEditParams && entity.actionType != 'default';
                }
            },
            {
                field: 'activityFParamModifiedDateTimeStr', name: 'activityFParamModifiedDateTimeStr', displayName: '最后修改日期', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            { 
                field: 'activityFParamModifiedUser', name: 'activityFParamModifiedUser', displayName: '最后修改人', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            } 
        ]
    };

    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关 
        gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
            var count = gridApi.selection.getSelectedCount();
            $scope.config.hasSelectedRow = count > 0 ? true : false;
        }); 
        gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            var count = gridApi.selection.getSelectedCount();
            $scope.config.hasSelectedRow = count > 0 ? true : false;
        }); 
    };
    
    $scope.editPreventStayParams = function(){
        $scope.config.btnDisabledSaveParams = false;
        $scope.config.btnDisabledEditParams = true;
    };
    $scope.savePreventStayParams = function(){
        if($scope.PreventStayParamsOptions.data.length <= 0){
            return;
        }
        var params = [];
        for(var i = 0; i < $scope.PreventStayParamsOptions.data.length; i++){
            var data = $scope.PreventStayParamsOptions.data[i];
            var param = {
                reasonCodeRuleParamBo: data.reasonCodeRuleParamBo,
                reasonCodeRuleParamModifiedDateTime: data.activityFParamModifiedDateTime,
                // reasonCodeRuleBo: data.reasonCodeRuleBoF,
                resourceType: data.activityFParamResourceType, 
                activityFParamValue: data.activityFParamValue//,
                // viewActionCode: "U"
            };
            if(UtilsService.isEmptyString(param.activityFParamValue) && UtilsService.isEmptyString(param.reasonCodeRuleParamBo)){
            }else{
                params.push(param);
            }
        }
        var postData = {
            resourceTypeAndActivityParamValue: params,
            reasonCodeRuleBo: $scope.config.reasonCodeRuleBoF, //$scope.PreventStayParamsOptions.data[0].reasonCodeRuleBoF,
            // resourceTypeBo: data[0].
            paramId: $scope.config.paramId //$scope.PreventStayParamsOptions.data[0].activityFParamId
        };
        reasonCodeService
            .reasonRuleTypeParamSave(postData)
            .then(function (resultDatas){
                $scope.config.btnDisabledSaveParams = true;
                $scope.config.btnDisabledEditParams = false;
                $scope.addAlert('success', "保存成功!");
                __requestPreventStayParams($scope.config.reasonCodeRuleBoF, $scope.config.paramId);
            }, function (resultDatas){
                $scope.addAlert('danger', "保存失败! "+resultDatas.myHttpConfig.statusDesc);
            });
    };
    $scope.cancelPreventStayParams = function(){
        $uibModalInstance.close();
    };

}]);

/**
 * 
 * [原因代码规则管理界面--规则创建Model界面  所对应 Controller: ModelRuleCreateCtrl]
 *     变量名含义及方法含义请参考 ModelReasonCodeRuleDetailCtrl
 * 
 */
reasonCodeModule.controller('ModelRuleCreateCtrl', [ 
    '$scope', '$uibModalInstance', 'modelRuleCreateData', '$uibModal',
    'UtilsService', 'reasonCodeService', '$filter', 
    function ($scope, $uibModalInstance, modelRuleCreateData, $uibModal,
        UtilsService, reasonCodeService, $filter){

    $scope.config = { 
        gridApi: null,

        savedOkDatas: {
        },

        gridApiParams: null,
        gridApiStay: null,
        btnDisabledEditParams: false,
        btnDisabledSaveParams: true,
        btnDisabledCancelParams: false,

        currentIndentify: null,
        currentPreventStay: null,
        
        currentReasonCode: null,
        currentReasonCodeBack: null,
        priorities: [ 
            {id: 'A', value: 'A'},
            {id: 'B', value: 'B'},
            {id: 'C', value: 'C'},
            {id: 'D', value: 'D'}
        ], 
        subPriorities: [ 
            {id: '1', value: '1'}, 
            {id: '2', value: '2'}, 
            {id: '3', value: '3'} 
        ], 

        hasSavedTabOneOk: false,
        hasSavedTabThreeActivityOk: false,

        oneTabDatas: null, 
        oneTabChanged: true, 

        towTabDatas: null, 
        towTabChanged: false, 
        btnDisabledSaveTow: true,
        btnDisabledCancelTow: false,

        threeTabActivityNotInit: true,
        threeTabDatas: null, 
        threeTabChanged: false, 
        btnDisabledSaveThree: true,
        btnDisabledCancelThree: false,

        currentAction: '',
        hasSelectedRow: false,
        currentAction2: '',
        hasSelectedRow2: false,

        btnDisabledEditParams: false,
        btnDisabledEditStayParams: false,

        preventStayActivityHasChanged: false,

        oneTabDate: new Date(), 
        currentEffectDate: null
    };
        $scope.config.currentEffectDate = UtilsService.serverFommateDateTimeShow(new Date((new Date().getTime() + 60*60*1000)));

    $scope.actionFChanged2 = function(type){
        if(UtilsService.isEmptyString(type)){
            return;
        }   
        $scope.config.currentAction2 = type;

        var rows = $scope.config.gridApiParams.selection.getSelectedGridRows();
        for(var i = 0; i < rows.length; i++){ 
            rows[i].entity.actionType = $scope.config.currentAction2;
            rows[i].entity.hasChanged = true;
            rows[i].entity.resourceTypeDesc = '无';
            rows[i].entity.resourceType = '*';
            rows[i].entity.modifiedDateTimeStr = "";
            rows[i].entity.modifiedUser = "";
            if($scope.config.currentAction2 == "default"){ 
                rows[i].entity.paramValue = rows[i].entity.paramSetValue = rows[i].entity.paramDefaultValue;
            }else if($scope.config.currentAction2 == "set"){ 
                rows[i].entity.paramValue = rows[i].entity.paramSetValue = ""; 
            }else if($scope.config.currentAction2 == "notUse"){ 
                rows[i].entity.paramValue = rows[i].entity.paramSetValue = "";
            }   
        }
    };
    $scope.actionFChanged = function(type){
        if(UtilsService.isEmptyString(type)){
            return;
        }   
        $scope.config.currentAction = type;

        var rows = $scope.config.gridApiStay.selection.getSelectedGridRows();
        for(var i = 0; i < rows.length; i++){ 
            rows[i].entity.actionType = $scope.config.currentAction;
            rows[i].entity.hasChanged = true;
            rows[i].entity.resourceTypeDesc = '*';
            rows[i].entity.resourceTypeDesc = '无';
            rows[i].entity.modifiedDateTimeStr = "";
            rows[i].entity.modifiedUser = "";
            // if(rows[i].entity.resourceTypeDesc == "By设备类型"){
            //     continue;
            // }
            if($scope.config.currentAction == "default"){ 
                rows[i].entity.paramValue = rows[i].entity.paramSetValue = rows[i].entity.paramDefaultValue;
            }else if($scope.config.currentAction == "set"){ 
                rows[i].entity.paramValue = rows[i].entity.paramSetValue = ""; 
            }else if($scope.config.currentAction == "notUse"){ 
                rows[i].entity.paramValue = rows[i].entity.paramSetValue = "";
            }   
        }
    };
    $scope.changeUppercase = function(){
        if(!angular.isUndefined($scope.config.currentIndentify) && $scope.config.currentIndentify != null && !UtilsService.isEmptyString($scope.config.currentIndentify.activity)){
            $scope.config.currentIndentify.activity = $scope.config.currentIndentify.activity.toUpperCase();
        }   
        if(!angular.isUndefined($scope.config.currentPreventStay) && $scope.config.currentPreventStay != null && !UtilsService.isEmptyString($scope.config.currentPreventStay.activity)){
            $scope.config.currentPreventStay.activity = $scope.config.currentPreventStay.activity.toUpperCase();
        }   
        if(!angular.isUndefined($scope.config.currentReasonCode) && $scope.config.currentReasonCode != null && !UtilsService.isEmptyString($scope.config.currentReasonCode.reasonCode)){
            $scope.config.currentReasonCode.reasonCode = $scope.config.currentReasonCode.reasonCode.toUpperCase();
        }  
        setBtns(1);
    };      
    
    $scope.goTab = function(toIndex){
        if($scope.innerTabOne && $scope.config.oneTabChanged){ 
            return; 
        }   
        // if($scope.innerTabTwo && $scope.config.towTabChanged){
        //     return;
        // }
        // if($scope.innerTabThree && $scope.config.threeTabChanged){
        //     return;
        // } 
        $scope.innerTabOne = toIndex == 1 ? true : false; 
        $scope.innerTabTwo = toIndex == 2 ? true : false; 
        $scope.innerTabThree = toIndex == 3 ? true : false; 
    };
    $scope.init = function(){
    };
    $scope.init();

    $scope.editParams = function(){
    };
    // 识别优先级相关
    $scope.effDateTimeChanged = function(){
        setBtns(1);
    };
    $scope.subPriorityChanged = function(){
        setBtns(1);
    };
    $scope.priorityChanged = function(){
        setBtns(1);
    }; 
    $scope.saveParams = function(){ // 保存识别优先级基本信息 
        $scope.config.btnDisabledCancelParams = true;
        var params = { 
            reasonCodeBo: $scope.config.currentReasonCode.reasonCodeBo, 
            priority: $scope.config.priority.id, 
            subPriority: $scope.config.subPriority.id, 
            inStartDateTime: UtilsService.serverFommateDateTime(new Date($scope.config.currentEffectDate))
        };  
        reasonCodeService
            .reasonRulePriorityChange(params)
            .then(function (resultDatas){    
                if(angular.isUndefined(resultDatas.response) || resultDatas.response == null || resultDatas.response ==""){
                    $scope.config.btnDisabledCancelParams = false;
                    $scope.addAlert('danger',"保存失败! "+resultDatas.myHttpConfig.statusDesc);
                }else{
                    $scope.config.btnDisabledCancelParams = false;
                    $scope.config.oneTabSaved = true;
                    $scope.config.oneTabChanged = false;
                    $scope.config.oneTabDatas = params;
                    $scope.config.savedOkDatas.handle = resultDatas.response.handle;
                    __requestDetailDatas(resultDatas.response.handle);
                    $scope.addAlert('success',"保存成功! ");
                    $scope.config.hasSavedTabOneOk = true;
                }
                // $scope.config.btnDisabledCancelParams = false;
            }, function (resultDatas){ 
                $scope.config.btnDisabledCancelParams = false;
                $scope.addAlert('danger',"保存失败! "+resultDatas.myHttpConfig.statusDesc);
                // $scope.config.btnDisabledCancelParams = false;
                // $scope.config.oneTabSaved = true;
                // $scope.config.btnDisabledSaveParams = true;
                // $scope.config.oneTabChanged = false;
                // $scope.config.oneTabDatas = params;
            });  
    };  
    function __requestDetailDatas(reasonCodePriorityBo){
        reasonCodeService
            .reasonRuleDetailAll(reasonCodePriorityBo)
            .then(function(resultDatas){
                console.log(resultDatas);
                resultDatas.response.modifiedDateTimeStr = $filter('date')(new Date(resultDatas.response.modifiedDateTime), 'yyyy-MM-dd HH:mm:ss');
                resultDatas.response.effDateTimeStr = $filter('date')(new Date(resultDatas.response.effDateTime), 'yyyy-MM-dd HH:mm:ss');
                $scope.config.detailData = resultDatas.response; 
                $scope.config.detailDataBack = angular.copy($scope.config.detailData); 
                var activity = $scope.config.detailData.activityBoF; 
                var activityFParam = $scope.config.detailData.activityFParam;
                if(resultDatas.response.activityFParam == undefined){

                }else{
                    var activityFParam = resultDatas.response.activityFParam;
                    var data = $scope.preventStayGridOptions.data;
                    for(var i=0;i<activityFParam.length;i++){
                        for(var j=0;j<data.length;j++) {
                            if(activityFParam[i].paramId == data[j].paramId){
                                data[j].reasonCodeRuleParamBo = activityFParam[i].reasonCodeRuleParamBo;
                                data[j].modifiedDateTime = activityFParam[i].modifiedDateTime;
                                data[j].resourceType = activityFParam[i].resourceType;
                                data[j].paramValue = activityFParam[i].paramValue;
                            }
                        }
                    }
                }
                console.log($scope.preventStayGridOptions.data);
                for(var i = 0; i < $scope.preventStayGridOptions.data.length; i++){
                    var type = $scope.preventStayGridOptions.data[i].resourceType;
                    $scope.preventStayGridOptions.data[i].resourceTypeDesc = UtilsService.isEmptyString(type) || type=="*" ? "无" : 'By设备类型';
                    var modifiedDateTime = $scope.preventStayGridOptions.data[i].modifiedDateTime;
                    if(!UtilsService.isEmptyString(modifiedDateTime)){
                        $scope.preventStayGridOptions.data[i].modifiedDateTimeStr = $filter('date')(new Date(modifiedDateTime), 'yyyy-MM-dd HH:mm:ss');
                    }
                }
                console.log($scope.preventStayGridOptions.data);
            }, function(errorResultDatas){
                $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc); 
            }); 
    }       
    $scope.cancelParams = function(){ 
        $uibModalInstance.close(); 
    };  
    $scope.cancelParamsTwoTab = function(){
        $uibModalInstance.close(); 
    };
    $scope.cancelParamsThreeTab = function(){
        $uibModalInstance.close(); 
    };
    function setBtns(tabIndex){ 
        if(tabIndex == 1){ 
            if( angular.isUndefined($scope.config.currentReasonCode) || $scope.config.currentReasonCode == null || UtilsService.isEmptyString($scope.config.currentReasonCode.handle)
                || angular.isUndefined($scope.config.priority) || $scope.config.priority ==null || UtilsService.isEmptyString($scope.config.priority.id) 
                || angular.isUndefined($scope.config.subPriority) || $scope.config.subPriority == null || UtilsService.isEmptyString($scope.config.subPriority.id) 
                || UtilsService.isEmptyString($scope.config.currentReasonCode.reasonCode)
                || UtilsService.isEmptyString($scope.config.currentReasonCodeBack.reasonCode)
                || $scope.config.currentReasonCodeBack.reasonCode != $scope.config.currentReasonCode.reasonCode
            ){
                $scope.config.btnDisabledSaveParams = true;
            }else{
                $scope.config.btnDisabledSaveParams = false;
            }
            var params = $scope.config.oneTabDatas;
            if( params == null 
                || params.reasonCodeBo != $scope.config.currentReasonCode.handle
                || params.priority != $scope.config.priority.id
                || params.subPriority != $scope.config.subPriority.id
                || params.startDateTime != UtilsService.serverFommateDateTime($scope.config.currentEffectDate)){
                $scope.config.oneTabChanged = true; 
            }else{
                $scope.config.oneTabChanged = false;
            } 
        }else if(tabIndex == 2){
            if( angular.isUndefined($scope.config.currentIndentify) || angular.isUndefined($scope.config.currentIndentify == null 
                || UtilsService.isEmptyString($scope.config.currentIndentify.activity)
                )){

            }
        }else if(tabIndex == 3){

        } 
    }; 
    
    $scope.saveParamsTwoTab = function(){ 
        var params = $scope.paramsGridOptions.data;
        for(var i = 0; i < params.length; i++){
            params[i].paramSetValue = params[i].paramValue
        }
        var getParams = { 
            activity: $scope.config.currentIndentify.activity,
            // $scope.config.currentIndentify.handle
            reasonCodePriorityBO: $scope.config.savedOkDatas.handle
        };
        reasonCodeService
            .reasonRuleCreatePattern(params, getParams)
            .then(function (resultDatas){
                $scope.addAlert('success',"保存成功!");
                __requestDetailDatas($scope.config.detailData.reasonCodePriorityBo);
            }, function(resultDatas){
                $scope.addAlert('danger',"保存失败! "+resultDatas.myHttpConfig.statusDesc);
            });  
    };
    // 新建-防呆验证：保存相关接口调用: 
    $scope.savePreventStayThreeTab = function(isUpdate){ 
        if($scope.config.hasSavedTabThreeActivityOk){
            __updateParamsThreeTab();
            return;
        }   
        var datas = [];
        for(var i = 0; i < $scope.preventStayGridOptions.data.length; i++){
            if(!UtilsService.isEmptyString($scope.preventStayGridOptions.data[i].paramSetValue)){
                var desc = $scope.preventStayGridOptions.data[i].resourceTypeDesc;
                var type = "*";
                if(desc == "By设备类型"){
                    type = "By设备类型";
                }else{
                    desc = "无";
                    type = "*";
                }
                datas.push({
                    activityFParamId: $scope.preventStayGridOptions.data[i].paramId,
                    activityFParamValue: $scope.preventStayGridOptions.data[i].paramValue,
                    resourceTypeDesc: desc,
                    resourceType: type 
                });
            }   
        }       
        var postData = { 
            reasonCodePriorityBo: $scope.config.detailData.reasonCodePriorityBo,
            activityF: $scope.config.currentPreventStay.activity,
            activityParam: datas,
            // activityBo: $scope.config.currentPreventStay.handle,
            viewActionCode: 'C'
        };  
        reasonCodeService 
            .reasonRuleFilterActivitySave(postData)  
            .then(function (resultDatas){ 
                $scope.config.threeTabActivityNotInit = false;
                $scope.config.preventStayActivityHasChanged = false;
                $scope.config.hasSavedTabThreeActivityOk = true;
                __requestDetailDatas($scope.config.detailData.reasonCodePriorityBo);
                __requestPreventStayTableDatas($scope.config.currentPreventStay.handle); 
                $scope.addAlert('success', "保存成功!");
                $scope.config.detailData.modifiedDateTimeF = resultDatas.modifiedDateTime;
            }, function (resultDatas){
                $scope.config.hasSavedTabThreeActivityOk = false;
                $scope.addAlert('danger', "保存失败!");
            });     
    };
    function __updateParamsThreeTab(isSaveParams){ 
        if(isSaveParams && $scope.config.preventStayActivityHasChanged){
            $scope.addAlert('', "请先保存防呆验证作业!");
            return;
        }
        var datas = [];
        console.log($scope.preventStayGridOptions.data);
        for(var i = 0; i < $scope.preventStayGridOptions.data.length; i++){
            // if(!UtilsService.isEmptyString($scope.preventStayGridOptions.data[i].paramSetValue)){
                var desc = $scope.preventStayGridOptions.data[i].resourceTypeDesc;
                var type = "*";
                if(desc == "By设备类型"){
                    type = "By设备类型";
                }else{
                    desc = "无";
                    type = "*";
                }
                datas.push({
                    activityFParamId: $scope.preventStayGridOptions.data[i].paramId,
                    activityFParamValue: $scope.preventStayGridOptions.data[i].paramValue,
                    resourceTypeDesc: desc,
                    resourceType: type,
                    
                    paramId: $scope.preventStayGridOptions.data[i].paramId,
                    paramValue: $scope.preventStayGridOptions.data[i].paramValue,
                    paramSetValue: $scope.preventStayGridOptions.data[i].paramValue,
                    reasonCodeRuleParamBo : $scope.preventStayGridOptions.data[i].reasonCodeRuleParamBo
                });
            // }
        }
        var executionType = "F";
        var params = {
            // actionCode: 'CHANGE_ACTIVITY',
            actionCode: 'CHANGE_PARAMETER',
            reasonCodeRuleBo: $scope.config.detailData.reasonCodeRuleBoF,
            reasonCodePriorityBo: $scope.config.detailData.reasonCodePriorityBo,
            activity: $scope.config.currentPreventStay.activity, //$scope.config.detailData.activityF, 
            activityBo: $scope.config.currentPreventStay.handle,  // $scope.config.detailData.activityBoF, //
            modifiedDateTime: $scope.config.detailData.modifiedDateTimeF, 
            ruleParam: datas //$scope.preventStayGridOptions.data 
        };
        reasonCodeService
            .reasonRuleParamFChange(params)
            .then(function (){
                $scope.addAlert('success', '保存成功!');
                $scope.config.currentAction = "";
                $scope.config.hasSelectedRow = true;
                __requestDetailDatas($scope.config.detailData.reasonCodePriorityBo);
                //$scope.config.detailData.modifiedDateTimeF =
            }, function(resultDatas){
                $scope.addAlert('success', '保存失败!  '+resultDatas.myHttpConfig.statusDesc);
            });
    };
    $scope.saveParamsThreeTab = function(){
        __updateParamsThreeTab(true);
    };

    $scope.openIdentifySelectModel = function(){ 
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modules/reasonCode/identifyRulesManager/model/m_identify_select.html',
            controller: 'IndentifySelectCtrl',
            size: 'lg',
            backdrop: 'false',
            openedClass: 'flex-center-parent',
            scope : $scope,
            resolve: {
                modelReasonCodeData: function (){
                    return {
                        activity: $scope.config.currentIndentify == null || UtilsService.isEmptyString($scope.config.currentIndentify.activity) ? '' : $scope.config.currentIndentify.activity
                    };
                }
            }
        });
        modalInstance.result.then(function (closeDataObj){
            var indentify = closeDataObj.indentify;
            $scope.config.currentIndentify = indentify;
            __reasonRuleCheckInputHelp(indentify.handle);
        }, function(){ }); 
    };  
    function __onRegisterApiParams(gridApi){ 
        $scope.config.gridApiParams = gridApi; 
        //选择相关 
        gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
            var count = gridApi.selection.getSelectedCount();
            $scope.config.hasSelectedRow2 = count > 0 ? true : false;
        });
        gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            var count = gridApi.selection.getSelectedCount();
            $scope.config.hasSelectedRow2 = count > 0 ? true : false;
        });
    }   
    function __onRegisterApiParams3(gridApi){
        $scope.config.gridApiStay = gridApi; 
        gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
            if(newRowCol.col.field == "paramValue" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                if(newRowCol.row.entity.resourceTypeDesc == "无"){
                }else{
                    $scope.editPreventStay(newRowCol);
                    // $scope.openPreventStayParamsModel(newRowCol);
                }   
            }
        }); 
        gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
            if(colDef.name == "paramValue"  && !UtilsService.isEmptyString(newValue) && newValue!= oldValue){
                rowEntity.paramSetValue = rowEntity.paramValue;
                rowEntity.hasChanged = true; 
            }
            if(colDef.name == "resourceTypeDesc" && newValue!=oldValue){
                rowEntity.paramSetValue = "";
                rowEntity.paramValue = "";
            }
        });
        //选择相关 
        gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
            var count = gridApi.selection.getSelectedCount();
            $scope.config.hasSelectedRow = count > 0 ? true : false;
        }); 
        gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            var count = gridApi.selection.getSelectedCount();
            $scope.config.hasSelectedRow = count > 0 ? true : false;
        }); 
    }   

    $scope.openReasonCodeModal = function(){ 
        var reasonCode = angular.isUndefined($scope.config.currentReasonCode) || $scope.config.currentReasonCode == null ? "" : $scope.config.currentReasonCode.reasonCode;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modules/reasonCode/identifyRulesManager/model/m_select_reason_code.html',
            controller: 'ModelReasonCodeCtrl',
            size: 'lg',
            backdrop: 'false',
            openedClass: 'flex-center-parent',
            scope : $scope,
            resolve: {
               modelReasonCodeData: function (){
                   return {
                        reasonCode: reasonCode,
                        multiSelect: false
                   };
               }
            }
        }); 
        modalInstance.result.then(function (closeDataObj){
            // var reasonCode = closeDataObj.reasonCode;
            if(angular.isUndefined(closeDataObj) || closeDataObj==null || angular.isUndefined(closeDataObj.reasonCodes) || closeDataObj.reasonCodes == null || closeDataObj.reasonCodes.length ==0){
                return;
            }
            var reasonCode = closeDataObj.reasonCodes[0];
            $scope.config.currentReasonCode = reasonCode;
            $scope.config.currentReasonCodeBack = angular.copy($scope.config.currentReasonCode);
            $scope.config.currentReasonCode.handle = reasonCode.reasonCodeBo;
            $scope.config.currentReasonCode.description = reasonCode.reasonCodeDesc;
            setBtns(1);
        }, function(){ });   
    }; 

    $scope.paramsGridOptions = { 
        onRegisterApi: __onRegisterApiParams,
        data: [],
        columnDefs: [ 
            { 
                field: 'handle', name: 'handle', visible: false
            },
            {
                field: 'paramId', name: 'paramId', displayName: '参数序号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'paramDescription', name: 'paramDescription', displayName: '参数描述', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {   
                field: 'paramDefaultValue', name: 'paramDefaultValue', displayName: '参数默认值', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'paramValue', name: 'paramValue', displayName: '参数设定值', minWidth: 200,
                enableCellEdit: true, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellEditableCondition: function(scope){
                    var row = arguments[0].row;
                    var col = arguments[0].col;
                    var entity = row.entity;
                    var type = entity.actionType;
                    if(UtilsService.isEmptyString(type) || type == 'default' || type == 'notUse'){
                        return false;
                    }   
                    return true;
                }
            },
            {   
                field: 'modifiedDateTimeStr', name: 'modifiedDateTimeStr', displayName: '最后修改日期', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                visible: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'modifiedUser', name: 'modifiedUser', displayName: '最后修改人', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false,  cellClass:'grid-no-editable',
                visible: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            }
        ]
    };
    
    $scope.preventStayGridOptions = {
        onRegisterApi: __onRegisterApiParams3,
        data: [],
        columnDefs: [
            {   
                field: 'handle', name: 'handle', visible: false
            },  
            {   
                field: 'paramId', name: 'paramId', displayName: '参数序号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },  
            {   
                field: 'paramDescription', name: 'paramDescription', displayName: '参数描述', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },  
            { 
                field: 'paramDefaultValue', name: 'paramDefaultValue', displayName: '参数默认值', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }  
            },  
            {
                field: 'resourceTypeDesc', name: 'resourceTypeDesc', displayName: '参数设置方式', minWidth: 200,
                enableCellEdit: true, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                editableCellTemplate: 'ui-grid/dropdownEditor',
                editDropdownIdLabel: 'resourceTypeDesc', editDropdownValueLabel: 'resourceTypeDesc',
                editDropdownOptionsFunction: function(rowEntity, colDef){
                    return [
                        { resourceTypeDesc: 'W', resourceTypeDesc: 'By设备类型' },
                        { resourceTypeDesc: '*', resourceTypeDesc: '无' }
                    ];
                },  
                cellEditableCondition: function(scope){
                    return true;
                }   
            },  
            {   
                field: 'paramValue', name: 'paramValue', displayName: '参数设定值', minWidth: 200,
                enableCellEdit: true, enableCellEditOnFocus: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellTemplate: 'andon-ui-grid-tpls/reasonCodeRule-PreventStay-paramValue',
                cellEditableCondition: function(scope){
                    var row = arguments[0].row;
                    var col = arguments[0].col;
                    var entity = row.entity;
                    // var type = entity.actionType;
                    // if(UtilsService.isEmptyString(type) || type == 'default' || type == 'notUse'){
                    //     return false;
                    // }   
                    // return true;
                    if(entity.resourceTypeDesc == 'By设备类型'){
                        return false;
                    }
                    if(entity.actionType == 'default' || entity.actionType == 'notUse'){
                        return false;
                    }
                    if(entity.actionType == 'set'){
                        return true;
                    }
                }   
            },      
            {       
                field: 'modifiedDateTimeStr', name: 'modifiedDateTimeStr', displayName: '最后修改日期', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false, cellClass:'grid-no-editable',
                visible: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'modifiedUser', name: 'modifiedUser', displayName: '最后修改人', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false,  cellClass:'grid-no-editable',
                visible: false,
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            }
        ]
    };

    function __requestPreventStayTableDatas(activity){
        var type ="F"; 
        reasonCodeService
            // .reasonRuleCheckInputHelp(activity)
            .reasonRuleActivityParam(activity, type)
            .then(function(resultDatas){
                if(!UtilsService.isEmptyString(resultDatas.response.modifiedDateTime)){
                    resultDatas.response.modifiedDateTimeStr = $filter('date')(new Date(resultDatas.response.modifiedDateTime), 'yyyy-MM-dd');    
                }
                if(!UtilsService.isEmptyString(resultDatas.response.effDateTime)){
                    resultDatas.response.effDateTimeStr = $filter('date')(new Date(resultDatas.response.effDateTime), 'yyyy-MM-dd');    
                }
                resultDatas.response.modifiedDateTimeStr = UtilsService.isEmptyString(resultDatas.response.modifiedDateTimeStr) ? '' : resultDatas.response.modifiedDateTimeStr;
                // resultDatas.response.modifiedDateTimeStr = $filter('date')(new Date(resultDatas.response.modifiedDateTime), 'yyyy-MM-dd');
                // resultDatas.response.effDateTimeStr = $filter('date')(new Date(resultDatas.response.effDateTime), 'yyyy-MM-dd');
                for(var i = 0; i < resultDatas.response.length; i++){
                    // resultDatas.response[i].modifiedDateTimeStr = $filter('date')(new Date(resultDatas.response[i].modifiedDateTime), 'yyyy-MM-dd HH:mm:ss');
                    if(!UtilsService.isEmptyString(resultDatas.response.modifiedDateTime)){
                        resultDatas.response.modifiedDateTimeStr = $filter('date')(new Date(resultDatas.response.modifiedDateTime), 'yyyy-MM-dd');    
                    }
                    if(!UtilsService.isEmptyString(resultDatas.response.effDateTime)){
                        resultDatas.response.effDateTimeStr = $filter('date')(new Date(resultDatas.response.effDateTime), 'yyyy-MM-dd');    
                    }
                    resultDatas.response.modifiedDateTimeStr = UtilsService.isEmptyString(resultDatas.response.modifiedDateTimeStr) ? '' : resultDatas.response.modifiedDateTimeStr;
                    
                    var type = resultDatas.response[i].resourceType;
                    if(UtilsService.isEmptyString(type) || type == "*"){
                        resultDatas.response[i].resourceType = "*";
                        resultDatas.response[i].resourceTypeDesc = "无";
                    }else{
                        resultDatas.response[i].resourceType = "By设备类型";
                        resultDatas.response[i].resourceTypeDesc = "By设备类型";
                    }
                }
                $scope.preventStayGridOptions.data = resultDatas.response;
                console.log($scope.preventStayGridOptions.data);

            }, function(errorResultDatas){
                // $scope.PreventStayGridOptions.data = [];
                $scope.preventStayGridOptions.data = [];
                $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
            });
    }
    function __reasonRuleCheckInputHelp(activity){
        // var type = "R";
        // reasonCodeService
        //     .reasonRulePatternUpdate(activity, type)
        //     .then(function(resultDatas){
        //         // resultDatas.response.modifiedDateTimeStr = $filter('date')(new Date(resultDatas.response.modifiedDateTime), 'yyyy-MM-dd');
        //         // resultDatas.response.effDateTimeStr = $filter('date')(new Date(resultDatas.response.effDateTime), 'yyyy-MM-dd');
        //         $scope.paramsGridOptions.data = resultDatas.response;
        //     }, function(errorResultDatas){
        //         $scope.paramsGridOptions.data = [];
        //         $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
        //     });
        var executionType = "R";
        reasonCodeService
            .reasonRulePatternDetail(activity, executionType)
            .then(function(resultDatas){ 
                if(!UtilsService.isEmptyString(resultDatas.response.modifiedDateTime)){
                    resultDatas.response.modifiedDateTimeStr = $filter('date')(new Date(resultDatas.response.modifiedDateTime), 'yyyy-MM-dd');    
                }
                if(!UtilsService.isEmptyString(resultDatas.response.effDateTime)){
                    resultDatas.response.effDateTimeStr = $filter('date')(new Date(resultDatas.response.effDateTime), 'yyyy-MM-dd');    
                }
                resultDatas.response.modifiedDateTimeStr = UtilsService.isEmptyString(resultDatas.response.modifiedDateTimeStr) ? '' : resultDatas.response.modifiedDateTimeStr;
                $scope.paramsGridOptions.data = resultDatas.response;
            }, function(errorResultDatas){
                $scope.paramsGridOptions.data = [];
                $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
            });
    }
    
    $scope.editPreventStay = function(newRowCol){
        var entity = newRowCol.row.entity;
        $scope.openPreventStayParamsModel(entity);
    };  
    $scope.cancelPreventStay = function(){
        $uibModalInstance.close();
    };  
    $scope.openPreventStaySelectModel = function(){
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modules/reasonCode/identifyRulesManager/model/m_PreventStay_select.html',
            controller: 'PreventStaySelectCtrl',
            size: 'lg',
            backdrop: 'false',
            openedClass: 'flex-center-parent',
            scope : $scope,
            resolve: {
                modelReasonCodeData: function (){
                    return {
                        activity: $scope.config.currentPreventStay == null || UtilsService.isEmptyString($scope.config.currentPreventStay.activity) ? '' : $scope.config.currentPreventStay.activity
                    };
                }    
            }   
        }); 
        modalInstance.result.then(function (closeDataObj){
            var PreventStay = closeDataObj.PreventStay;
            $scope.config.preventStayActivityHasChanged = true;
            $scope.config.currentPreventStay = PreventStay;
            $scope.config.towTabDatas = {
                activity: PreventStay.activity
            }, 
            $scope.config.towTabChanged = true;
            // __requestPreventStayTableDatas(PreventStay.handle); 
        }, function(){

        });
    };  
    $scope.openPreventStayParamsModel = function(entity){
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modules/reasonCode/identifyRulesManager/model/m_PreventStay_params.html',
            controller: 'PreventStayParamsCtrl',
            size: 'lg',
            backdrop: 'static',
            openedClass: 'flex-center-parent',
            scope : $scope,
            resolve: {
               // modelReasonCodeData: function (){
               //     return {};
               // },
               preventStayParamsDatas: function (){
                   return {
                        preventStayEntity: entity,
                        detailData: $scope.config.detailData
                   };
                }   
            }
        });
        modalInstance.result.then(function (closeDataObj){
            __requestDetailDatas($scope.config.detailData.reasonCodePriorityBo);
        }, function(){ });
    };

    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
            console.log(gridRow);
            $uibModalInstance.close({
                reasonCode: angular.copy(gridRow.entity)
            });
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            // TODO 不允许多选
        }); 
    };

}]);

/**
 * 
 * [原因代码规则管理界面--规则删除Model界面  所对应 Controller: ModelRuleDeleteCtrl]
 */
reasonCodeModule.controller('ModelRuleDeleteCtrl', [
    '$scope', 'modelRuleDeleteData', '$uibModalInstance',
    function ($scope, modelRuleDeleteData, $uibModalInstance) {

    $scope.config = {
        entity: null
    };
    
    $scope.init = function(){
        $scope.config.entity = modelRuleDeleteData.entity;
    };

    $scope.init();

    $scope.confirmDelete = function(){
        $uibModalInstance.close({
            entity: $scope.config.entity
        });
    };

    $scope.cancel = function(){
        $uibModalInstance.close();
    };

}]);
