reasonCodeModule.controller('reasonCodeAndDeviceStatusCtrl',[
    '$scope', '$http','$q', 'UtilsService','HttpAppService', '$timeout',
    'uiGridConstants','reasonCodeService', 'uiGridValidateService',
    '$uibModal','$log','$filter','$state',
    function ($scope, $http ,$q, UtilsService, HttpAppService, $timeout,
              uiGridConstants,reasonCodeService, uiGridValidateService,
              $uibModal,$log,$filter,$state) { 

        $scope.config = {

            reasonCodeAndDeviceDate: UtilsService.serverFommateDateShow(new Date()),
            enabled: true,

            reasonCodeGroupsDefaults: [
                {reasonCode: null, description:"",descriptionNew:"" },
                {reasonCode: null, description:"--- 请选择 ---",descriptionNew:"--- 请选择 ---" }
            ],
            reasonCodeGroups: null, // []
            currentReasonCodeGroup: null, 

            reasonCodesDefaults:[
                {reasonCode: null, description:"",descriptionNew:"" },
                {reasonCode: null, description:"--- 请选择 ---",descriptionNew:"--- 请选择 ---" }
            ],
            reasonCodes:[
                {reasonCode: null, description:"",descriptionNew:"" },
                {reasonCode: null, description:"--- 请选择 ---",descriptionNew:"--- 请选择 ---" }
            ],
            currentReasonCode:null,

            isNecessary:{isReasonCodeGroups:false,isDate:false},

            addItems:[],
            reasonCodeResStateAdd:[],
            //defaultSelectItem: { descriptionNew:"--- 请选择 ---", description:"--- 请选择 ---", reasonCodeGroup: null },

            btnDisabledQuery: true,
            btnDisabledAdd: true,
            btnDisabledSave: true,
            prePage:1,
            preSize:1
        };

        $scope.init = function(){
            __initDropdowns('init');
            __requestReasonCodeGroups();
            $scope.config.reasonCodeGroups = angular.copy($scope.config.reasonCodeGroupsDefaults);
            $scope.config.currentReasonCodeGroup = $scope.config.reasonCodeGroups[0];
            $scope.config.currentReasonCode = $scope.config.reasonCodes[0];

        };

        $scope.init();

        $scope.gridReasonCodeAndDeviceStatus = {
            //是否分页
            enablePagination: true,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,

            useExternalPagination: true,
            paginationCurrentPage: 1,
            totalItems: 0,

            showGridFooter: false,
            modifierKeysToMultiSelectCells: false,
            data: [],
            columnDefs:[
                {
                    field: 'handle', name: 'handle', visible: false
                },
                {
                    field: 'isSelected', name: 'isSelected', displayName: '选择', minWidth: 60, maxWidth: 60,
                    enableCellEdit: false, type: 'boolean', visible: false,
                    enableColumnMenu: false, enableColumnMenus: false, enableSorting: false
                },
                {
                    field: 'reasonCodeGroupDes',name:"reasonCodeGroupDes",displayName:'原因代码组描述',
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellClass:'grid-no-editable',
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field:'reasonCodeDes',name:"reasonCodeDes",displayName:'原因代码描述',
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellClass:'grid-no-editable',
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field:'stateDes',name:"stateDes",displayName:'对应的设备状态',
                    enableCellEdit: false, enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellClass:'grid-no-editable',
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field:'startDateTimeIn',name:"startDateTime",displayName:'有效开始日期',
                    enableCellEdit: false,enableCellEditOnFocus: false,
                    cellClass:'grid-no-editable',
                },
                {
                    field:'endDateTimeIn',name:"endDateTime",displayName:'有效结束日期',
                    enableCellEdit: false,enableCellEditOnFocus: false,
                    cellClass:'grid-no-editable',
                },
                {
                    field: 'isDeleted', name: 'isDeleted',visible:false
                }
            ],
            onRegisterApi: __onRegisterApi,
        };

        $scope.gridReasonCodeAndDeviceStatus.data = [];

        function __onRegisterApi(gridApi){
            $scope.gridApi = gridApi;

            // 分页相关
            $scope.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                $scope.bodyConfig.overallWatchChanged = true;
                __requestTableDatas(currentPage, pageSize);
                return true;
            }, $scope);
        };

        function __initDropdowns(type){

            if(type == 'init'){
                $scope.config.reasonCodeGroups = [];
                $scope.config.currentReasonCodeGroup = null;
            }

            if(type == 'init' || type == 'reasonCodeGroupChanged'){
                $scope.config.reasonCodes = angular.copy($scope.config.reasonCodesDefaults);;
                $scope.config.currentReasonCodeGroup = null;

            }
        };

        //查询所有数据
        $scope.queryDatas = function(){

            if($scope.bodyConfig.overallWatchChanged){
                var promise =UtilsService.confirm('还有修改未保存，是否继续', '提示', ['确定', '取消']);
                promise.then(function(){
                    $scope.bodyConfig.overallWatchChanged = false;
                    $scope.gridReasonCodeAndDeviceStatus.paginationCurrentPage = 1;
                    $scope.gridReasonCodeAndDeviceStatus.totalItems = 0;
                    __requestReasonCodeCount();
                    __requestTableDatas();
                }, function(){
                    //取消停留在原页面
                    $scope.bodyConfig.overallWatchChanged = true;
                });
            }else{
                $scope.bodyConfig.overallWatchChanged = false;
                $scope.gridReasonCodeAndDeviceStatus.paginationCurrentPage = 1;
                $scope.gridReasonCodeAndDeviceStatus.totalItems = 0;
                __requestReasonCodeCount();
                __requestTableDatas();

            }
        };

        //保存新增数据
        $scope.saveDeviceStatus = function(){
            $scope.bodyConfig.overallWatchChanged = true;
            var addItems = $scope.config.addItems;
            console.log(addItems);
            var paramsArray = [];
            for(var i = 0 ; i < addItems.length ; i++){
                var paramsObj = {};
                paramsObj.site = HttpAppService.getSite();
                paramsObj.reasonCodeBo = "ReasonCodeBO:"
                    +HttpAppService.getSite()+","
                    +addItems[i].reasonCode;
                paramsObj.resourceStateBo =  "ResourceStateBO:"
                    +HttpAppService.getSite()+","
                    +addItems[i].state;
                paramsObj.startDateTimeIn = UtilsService.serverFommateDateTime(new Date(addItems[i].startDateTimeIn));
                paramsObj.str = UtilsService.serverFommateDateTime(new Date(addItems[i].startDateTimeIn));
                paramsObj.modifiedDateTime =addItems[i].modifiedDateTime;
                paramsObj.viewActionCode = "C";

                paramsArray.push(paramsObj);
            }
            if(addItems.length > 0){
                reasonCodeService
                    .deviceStatusSave(paramsArray)
                    .then(function (resultDatas){
                        $scope.bodyConfig.overallWatchChanged = false;
                        $scope.addAlert('success', '保存成功!');
                        $scope.queryDatas();
                    },function (resultDatas){
                        $scope.addAlert('danger', '保存失败!');
                    });
            }else{
                $scope.addAlert('没有修改,无需保存!');
            }

        };

        //原因代码组改变时触发事件 
        $scope.reasonCodeGroupChanged = function($select){
            var group = $scope.config.currentReasonCodeGroup;
            $scope.config.currentReasonCode = $scope.config.currentReasonCode?$scope.config.reasonCodes[0]:'';
            if(angular.isUndefined(group) || group == null || UtilsService.isEmptyString(group.reasonCodeGroup)){
                $scope.config.btnDisabledQuery = true;
                $scope.config.isNecessary.isReasonCodeGroups = true;
            }else{
                $scope.config.isNecessary.isReasonCodeGroups = false;
                $scope.config.reasonCodes = angular.copy($scope.config.reasonCodesDefaults);
                __requestReasonCode();
                $scope.config.btnDisabledQuery = false;

                $scope.config.btnDisabledAdd = false;
            } 
        };

        //时间输入框改变时触发
        $scope.dateChanged = function($select){
            var date =UtilsService.serverFommateDateTime($scope.config.reasonCodeAndDeviceDate);
            if(angular.isUndefined(date) || date == null || date == ''){
                $scope.config.isNecessary.isDate = true;
            }else{
                $scope.config.isNecessary.isDate = false;
            }
        };

        //原因代码有数据时触发
        $scope.reasonCodeChanged = function($select){
            var code = $scope.config.currentReasonCode;
            if(angular.isUndefined(code) || code == null){
                $scope.config.btnDisabledQuery = true;
            }else{
                $scope.config.btnDisabledQuery = false;
                $scope.config.btnDisabledAdd = false;
            }
        };

        //获取选择的行数
        $scope.checkSelectedRows = function(){
            var rows = $scope.config.gridApi.selection.getSelectedRows();
            return rows
        };

        //打开新增模态框
        $scope.ModalReasonCodeDevice = function(){
        //校验主页必填字段
            if(__checkReasonCodeGroupNecessary()){
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'ModalReasonCodeAndDeviceStatus.html',
                    controller: 'ModalReasonCodeAndDeviceStatusCtrl',
                    size: 'lg',
                    backdrop:'false',
                    openedClass:'flex-center-parent',
                    scope:$scope,
                    resolve: {
                        items: function () {
                            var res = {
                                "reasonCodeGroup":$scope.config.currentReasonCodeGroup.reasonCodeGroup,
                                "reasonCodeAndDeviceDate":$scope.config.reasonCodeAndDeviceDate
                            };
                            if($scope.config.currentReasonCode){
                                res.reasonCode = $scope.config.currentReasonCode.reasonCode;
                            }
                            return res;
                        }()
                    }
                });
                modalInstance.result.then(function (deviceValidateParams) {
                    $scope.bodyConfig.overallWatchChanged = true;

                    $scope.config.btnDisabledSave = false;
                    var add = $scope.config.addItems = deviceValidateParams;
                    for(var i = 0; i < add.length; i++){
                        $scope.gridReasonCodeAndDeviceStatus.data.unshift({
                            isSelected : false,
                            isDeleted : false,
                            reasonCodeGroupDes : add[i].reasonCodeGroupDes,
                            reasonCodeDes : add[i].reasonCodeDes,
                            stateDes:add[i].stateDes,
                            startDateTimeIn:add[i].startDateTimeIn,
                            endDateTimeIn: add[i].endDateTimeIn
                        });
                    }
                    //滚动到新增那一行
                    $scope.gridApi.cellNav.scrollToFocus($scope.gridReasonCodeAndDeviceStatus.data.length-add.length,0);
                    //$scope.gridApi.cellNav.scrollTo(datas[datas.length-1],0);
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            }

        };

        //新增时校验原因代码组不能为空
        function __checkReasonCodeGroupNecessary(){
            var group = $scope.config.currentReasonCodeGroup;
            if(angular.isUndefined(group) || group == null){
                $scope.config.isNecessary.isReasonCodeGroups = true;
                $scope.addAlert('danger', '请先选择原因代码组!');
                return false;
            }else{
                return true;
            }
        };

        //验证主页必填字段
        function __checkMainPageNecessary(){
            var group = $scope.config.currentReasonCodeGroup;
            var time = $scope.config.reasonCodeAndDeviceDate;
            if(angular.isUndefined(group) || group == null){
                $scope.config.isNecessary.isReasonCodeGroups = true;
                $scope.addAlert('danger', '请先选择原因代码组!');
                return false;
            }else if(angular.isUndefined(time) || time == null || time == ''){
                $scope.config.isNecessary.isDate = true;
                $scope.addAlert('danger', '请选择有效日期!');
                return false;
            }else{
                return true;
            }
        };

        //获取根据条件查询数据
        function __requestTableDatas(currentPage, pageSize){

            $scope.gridReasonCodeAndDeviceStatus.data = [];
            var reasonCodeGroup = $scope.config.currentReasonCodeGroup.reasonCodeGroup;
            var reasonCodeAndDeviceDate = UtilsService.serverFommateDate(new Date($scope.config.reasonCodeAndDeviceDate));

            //var reasonCodeAndDeviceDate = $scope.config.reasonCodeAndDeviceDate;
            var pageIndex = 1;
            var pageCount = 1;
            if(angular.isUndefined(currentPage) || currentPage == null | currentPage<=1){
                pageIndex = 1;
            }else{
                pageIndex = currentPage;
            }
            if(angular.isUndefined(pageSize) || pageSize == null | pageSize<1){
                pageCount = $scope.gridReasonCodeAndDeviceStatus.paginationPageSize;
            }else{
                pageCount = pageSize;
            }
            var deviceParams = {
                "site" : HttpAppService.getSite(),
                "reasonCodeGroup" : reasonCodeGroup,
                "dateTimeIn" : reasonCodeAndDeviceDate,
                "pageIndex" : pageIndex,
                "pageCount" : pageCount
            };
            if($scope.config.currentReasonCode){
                deviceParams.reaonCode = $scope.config.currentReasonCode.reasonCode;
            }
            reasonCodeService
                .reasonCodeAndDeviceStatus(deviceParams)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].isSelected = false;
                            resultDatas.response[i].isDeleted = false;
                            resultDatas.response[i].startDateTimeIn =UtilsService.serverStringFormatDateTime(resultDatas.response[i].startDateTimeIn);
                            resultDatas.response[i].endDateTimeIn =UtilsService.serverStringFormatDateTime(resultDatas.response[i].endDateTimeIn);
                        }
                        $scope.gridReasonCodeAndDeviceStatus.data = resultDatas.response;
                        $scope.config.btnDisabledSave = false;
                        return;
                    }else{
                        $scope.gridReasonCodeAndDeviceStatus.data = [];
                    }
                }, function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });


        };

        //获取总数
        function __requestReasonCodeCount(){
            var deviceParams = {};
            deviceParams.reasonCodeGroup = $scope.config.currentReasonCodeGroup.reasonCodeGroup;
            deviceParams.dateTime = UtilsService.serverFommateDate(new Date($scope.config.reasonCodeAndDeviceDate));
            if($scope.config.currentReasonCode){
                deviceParams.reaonCode = $scope.config.currentReasonCode.reasonCode;
            }
            reasonCodeService
                .reasonCodeAndDeviceStatusCount(deviceParams)
                .then(function (resultDatas){
                    $scope.gridReasonCodeAndDeviceStatus.totalItems = resultDatas.response;
                }, function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };

        //获取原因代码
        function __requestReasonCode(){
            var reasonCodeGroup = $scope.config.currentReasonCodeGroup.reasonCodeGroup;
            var enabled = true;
            reasonCodeService
                .reasonCodeListAll(reasonCodeGroup,enabled)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = "原因代码:"+resultDatas.response[i].reasonCode+"   描述:"+resultDatas.response[i].description;
                            $scope.config.reasonCodes.push(resultDatas.response[i]);
                        }
                        return;
                    }else{
                        $scope.config.reasonCodes = angular.copy($scope.config.reasonCodesDefaults);
                        $scope.config.currentReasonCode = $scope.config.reasonCodes[0];
                    }
                }, function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };

        //获取原因代码组
        function __requestReasonCodeGroups(){
            reasonCodeService
                .reasonCodeGroup()
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = "原因代码组代码:"+resultDatas.response[i].reasonCodeGroup+"  描述:"+resultDatas.response[i].description;
                        }
                        // $scope.config.reasonCodeGroups = resultDatas.response;
                        $scope.config.reasonCodeGroups = $scope.config.reasonCodeGroups.concat(resultDatas.response);
                        return;
                    }else{
                        $scope.config.reasonCodeGroups = angular.copy($scope.config.reasonCodeGroupsDefaults);
                    }
                }, function (resultDatas){
                    $scope.config.reasonCodeGroups = angular.copy($scope.config.reasonCodeGroupsDefaults);
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };

    }]);

reasonCodeModule.controller('ModalReasonCodeAndDeviceStatusCtrl',[
    '$scope', '$http','$q', 'UtilsService','HttpAppService', '$timeout',
    'uiGridConstants','reasonCodeService', 'uiGridValidateService',
    '$uibModal','$log','$filter','$uibModalInstance','items',
    function ($scope, $http, $q,UtilsService,HttpAppService,$timeout,
              uiGridConstants,reasonCodeService,uiGridValidateService,
              $uibModal,$log,$filter,$uibModalInstance,items) {

        $scope.config = {

            modalDeviceDate : UtilsService.serverFommateDate(new Date()),
                //UtilsService.serverFommateDate(new Date()),
            //modalDeviceDateTest:new Date(),
            enabled:true,
            deviceStatus:[],//所有的设备状态
            currentDeviceStatus:null,//选择的设备状态

            selectedReasonCodeGroup:[],//选择的原因代码
            successDatas:[],//保存前成功的数据
            errorDatas:[],//保存前失败的数据

            isInvalidToSave:true,//保存按钮可点击
            timeInValid:false,
            deviceTitle:"原因代码不能为空",

            index: -1,

        };

        $scope.gridModalReasonCodeAndDeviceStatus = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
            {
                field: 'handle', name: 'handle', visible: false
            },
            {
                field: 'isSelected', name: 'isSelected', visible: false
            },
            {
              field:'startTime',name:  'startTime',visible:false
            },
            {
                field: 'isSelected', name: 'isSelected', displayName: '选择',
                enableCellEdit: true, type: 'boolean', visible: false,
                enableColumnMenu: false, enableColumnMenus: false, enableSorting: false
            },
            {
                field:'reasonCodeGroupPO',name:'reasonCodeGroupPO',visible: false
            },
            {
                field: 'reasonCodeGroupPO.description', name: 'reasonCodeGroupPO.description',
                displayName: '原因代码组描述',
                enableCellEdit: true,enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
            },
                {
                    field: 'reasonCode', name: 'reasonCode', displayName: '原因代码',
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    // cellTemplate: 'andon-ui-grid-tpls/dataCategory-categoryCode',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                },
                {
                    field: 'description', name: 'description', displayName: '原因代码描述',
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row;
                        var col = arguments[0].col;
                        if(!angular.isUndefined(row.entity) && row.entity!=null && !angular.isUndefined(row.entity.handle) && row.entity.handle!=null && row.entity.handle!=""){
                            return false;
                        }else{
                            return true;
                        }
                    }
                }
            ],
            data: [],
            onRegisterApi: __onRegisterApi
        };

        function init(){
            __reasonCodeResStateAdd();
            __requestDeviceStatus();
        };

        init();

        $scope.changeDeviceStatus = function(){
            __changeDeviceStatus();
        };

        $scope.changeDeviceDate = function(){
            __changeDeviceDate();
        };

        function __onRegisterApi(gridApi){
                $scope.config.gridApi = gridApi;
                $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                    gridRow.entity.isSelected = true;
                    $scope.config.selectedReasonCodeGroup = gridRow.entity;
                    if(__checkValidateBeforeOK()){
                        $scope.config.deviceTitle = null;
                        $scope.config.isInvalidToSave = false;
                    };
                });
                $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                    if(__checkValidateBeforeOK()){
                        $scope.config.deviceTitle = null;
                        $scope.config.isInvalidToSave = false;
                    };
                    for(var i = 0; i < gridRows.length; i++){
                        gridRows[i].entity.isSelected = true;
                    }
                    $scope.config.selectedReasonCodeGroup = gridRows.entity;
                });
            };

        //新增时获取数据
        function __reasonCodeResStateAdd(){
            var reasonCodeGroup = items.reasonCodeGroup;
            var reasonCodeAndDeviceDate = UtilsService.serverFommateDateTime(new Date($scope.config.modalDeviceDate));
            var deviceParams = {
                "site" : HttpAppService.getSite(),
                "reasonCodeGroup" : reasonCodeGroup,
                "dateTime" : reasonCodeAndDeviceDate
            };
            if(items.reasonCode){
                deviceParams.reaonCode = items.reasonCode;
            }
            reasonCodeService
                .reasonCodeResStateAdd(deviceParams)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.gridModalReasonCodeAndDeviceStatus.data = resultDatas.response;
                        return;
                    }else{
                        $scope.gridModalReasonCodeAndDeviceStatus.data = [];
                    }
                }, function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
            };

        //获取关系设备
        function __requestDeviceStatus (){
            var enabled = $scope.config.enabled;
            reasonCodeService
                .deviceStatusList(enabled)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = "设备代码:"+resultDatas.response[i].state+"   设备状态:"+resultDatas.response[i].description;
                        }
                        $scope.config.deviceStatus = resultDatas.response;
                        return;
                    }else{
                        $scope.config.deviceStatus = [];
                    }
                }, function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };

        function __checkSelectedReasonCodeGroup(){
            var selected = $scope.config.selectedReasonCodeGroup = $scope.config.gridApi.selection.getSelectedRows();
            if(angular.isUndefined(selected) || selected == null || selected.length == 0){
                return false;
            }else{
                return true;
            }
        };

        function __changeDeviceStatus(){
            if(__checkValidateBeforeOK()){
                $scope.config.deviceTitle = null;
                $scope.config.isInvalidToSave = false;
            };
        };

        function __changeDeviceDate(){
            if(__checkValidateBeforeOK()){
                $scope.config.deviceTitle = null;
                $scope.config.isInvalidToSave = false;
            };
        };

        function __checkSelectedDevice(){
            var device = $scope.config.currentDeviceStatus;
            if(angular.isUndefined(device) || device == null || device.length == 0){
                return false;
            }else{
                return true;
            }
        };

        function __checkDeviceDate(){
            var deviceDate = $scope.config.modalDeviceDate;
            if(angular.isUndefined(deviceDate) || deviceDate == null || deviceDate.length == 0){
                return false;
            }else{
                return true;
            }
        };

        //校验输入字段是否为空，是否符合输入要求
        function __checkValidateBeforeOK(){
            if(!__checkSelectedReasonCodeGroup()){
                $scope.config.deviceTitle = "原因代码不能为空";
                $scope.config.isInvalidToSave = true;
                return false;
            }else if(!__checkSelectedDevice()){
                $scope.config.deviceTitle = "设备状态不能为空";
                $scope.config.isInvalidToSave = true;
                return false;
            }else if(!__checkDeviceDate()){
                $scope.config.deviceTitle = "时间不能为空";
                $scope.config.isInvalidToSave = true;
                return false;
            }else{
                return true;
            };
        };

        //点击确定
        $scope.addDevice = function () {
            if(__checkValidateBeforeOK()){//检查原因代码和关系设备是否都已选
                if(__checkDeviceDate()){//检查日期是否已选
                    $scope.config.index = -1;
                    var selectedGroupArr = $scope.config.selectedReasonCodeGroup;
                    var selectedDevice = $scope.config.currentDeviceStatus;
                    for(var i=0;i<selectedGroupArr.length;i++){//判断已选的数据是否已被引用
                        var deviceParams = {};
                        deviceParams.reasonCode = selectedGroupArr[i].reasonCode;
                        deviceParams.dateTime = UtilsService.serverFommateDateTime(new Date($scope.config.modalDeviceDate));
                        deviceParams.state = selectedDevice.state;
                        reasonCodeService
                            .reasonCodeResStateValidateBeforeAdd(deviceParams)
                            .then(function (resultDatas){
                                $scope.config.index = $scope.config.index + 1;
                                if(resultDatas.response){//如果为true，可以使用，将成功数据存入successDatas中
                                    $scope.config.successDatas.push({
                                        "reasonCodeGroupDes":$scope.config.selectedReasonCodeGroup[$scope.config.index].reasonCodeGroupPO.description,
                                        "reasonCode": selectedGroupArr[$scope.config.index].reasonCode,
                                        "reasonCodeDes":selectedGroupArr[$scope.config.index].description,
                                        "state":selectedDevice.state,
                                        "stateDes":selectedDevice.description,
                                        "startDateTimeIn":UtilsService.serverFommateDateTimeFrontShow(new Date($scope.config.modalDeviceDate)),
                                        "endDateTimeIn":'9999-12-31 23:59:59',
                                        "modifiedDateTime":selectedGroupArr[$scope.config.index].modifiedDateTime
                                    });
                                }else{//如果为false，不可以使用，将失败数据存入errorDatas中
                                    $scope.config.errorDatas.push({
                                        "reasonCodeGroupDes":$scope.config.selectedReasonCodeGroup[$scope.config.index].reasonCodeGroupPO.description,
                                        "reasonCode": selectedGroupArr[$scope.config.index].reasonCode,
                                        "reasonCodeDes":selectedGroupArr[$scope.config.index].description,
                                        "state":selectedDevice.state,
                                        "stateDes":selectedDevice.description,
                                        "startDateTimeIn":$scope.config.modalDeviceDate,
                                        "endDateTimeIn":'9999-12-31 23:59:59',
                                        //"modifiedDateTime":selectedGroupArr[$scope.config.index].modifiedDateTime
                                    });
                                }
                                //如果所有数据都检验了，判定是否有错误信息，如果有，则有失败的数据，操作回滚，不允许新增
                                if($scope.config.index == selectedGroupArr.length -1 ){
                                    if($scope.config.errorDatas.length > 0){
                                        console.log($scope.config.errorDatas);

                                        $scope.addAlert('danger',$scope.config.errorDatas[0].reasonCodeDes+'和'+$scope.config.errorDatas[0].stateDes+'的关联已存在晚于所选择有效开始时间的记录，不允许新增');

                                        $scope.config.successDatas = [];
                                        $scope.config.errorDatas = [];
                                        $scope.config.isInvalidToSave = true;//保存按钮不可点击
                                        //保存的信息有误
                                        $scope.config.deviceTitle = "已存在";

                                    }else{//如果没有错误信息，关闭模态框，新增数据
                                        $uibModalInstance.close($scope.config.successDatas);
                                    };
                                };
                            }, function (resultDatas){
                                return false;
                            });



                    };
                }else{
                    $scope.config.deviceTitle = "时间不能为空";
                    $scope.config.isInvalidToSave = true;
                };

            };
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
}

]);