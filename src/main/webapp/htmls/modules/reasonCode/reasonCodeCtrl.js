reasonCodeModule.controller('reasonCodeCtrl',['$scope', '$http',
    'HttpAppService', '$timeout',
    'uiGridConstants',
    function ($scope, $http, HttpAppService, $timeout,
              uiGridConstants) {
        $scope.config = {
            reasonCodeMenuList:[{
                title:'原因代码组维护',
                distState: '#/andon/ReasonCode/ReasonCodeGroup',
                distStateName:'andon.ReasonCode.ReasonCodeGroup',
                isActive:true,beIcon:'search'
            },{
                title:'原因代码维护',
                distState: '#/andon/ReasonCode/ReasonCodeMaintain',
                distStateName:'andon.ReasonCode.ReasonCodeMaintain',
                isActive:false,beIcon:'edit'
            }
            // ,{
            //     title:'原因代码与设备状态关系维护',
            //     distState: '#/andon/ReasonCode/ReasonCodeAndDeviceStatus',
            //     distStateName:'andon.ReasonCode.ReasonCodeAndDeviceStatus',
            //     isActive:false,
            //     beIcon:'desktop'
            // }
            ]
        };  
        
        $scope.reasonCodeConfig = {
            canEdit: false
        };
        
        $scope.reasonCodeConfig.canEdit = $scope.modulesRWFlag("#/andon/ReasonCode/ReasonCodeGroup");
        // $scope.reasonCodeConfig.editRw = $scope.modulesRWFlag("#/andon/ReasonCode/ReasonCodeGroup");
        // var rw = $scope.reasonCodeConfig.editRw;
        // if(angular.isUndefined(rw) || rw == null){
        //     $scope.reasonCodeConfig.canEdit = rw.write;
        // }

        $scope.$on("$stateChangeSuccess", function (event2, toState, toParams, fromState, fromParam){
            for(var i=0;i<$scope.config.reasonCodeMenuList.length;i++){
                var item = $scope.config.reasonCodeMenuList
                //$scope.config.reasonCodeMenuList[i].distState.slice(2).replace('/','.').replace('/','.')
                if(fromState && toState.name == item[i].distStateName){
                    item[i].isActive = true
                }else{
                    item[i].isActive = false;
                }
            }
        });
    }])