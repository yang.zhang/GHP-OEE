reasonCodeModule.controller('reasonCodeGroupCtrl',['$scope','$q', '$http','$interval',
    'HttpAppService', '$timeout','$rootScope', 'UtilsService', 'gridUtil', 'i18nService',
    'uiGridConstants','reasonCodeService','uiGridValidateService','uiGridValidateService',
    '$state',
    function ($scope,$q,$http,$interval, HttpAppService, $timeout,$rootScope,
              UtilsService,gridUtil,i18nService,uiGridConstants,reasonCodeService,
              uiGridValidateService,uiGridValidateService,$state) {

        $scope.config = {
            //新增行数
            reasonCodeGroupRows:1,
            //是否显示未被启用的数据
            enabled : false,
            //新增数据
            newAddReasonCodeGroup:[],
            //原始数据
            copyReasonCodeGroupDatas:[]
        };

        //页面加载时获取表格数据
        $scope.init = function(){
            __requestTableDatas();
        };

        $scope.init();

        //点击新增按钮
        $scope.reasonCodeAdd = function(){
            //如果表格没有数据，清空
            if(angular.isUndefined($scope.gridReasonCode.data) || $scope.gridReasonCode.data == null){
                $scope.gridReasonCode.data = [];
            }
            //新增reasonCodeGroupRows条
            for(var i = 0; i < $scope.config.reasonCodeGroupRows; i++){
                $scope.gridReasonCode.data.unshift({
                    site: HttpAppService.getSite(),
                    isDelete:false,
                    hasChanged:true
                });
            }
            //新增时处于正在修改状态，离开页面时有提示是否继续
            $scope.bodyConfig.overallWatchChanged = true;
        };

        //表格处理
        $scope.gridReasonCode = {
            modifierKeysToMultiSelectCells: false,
            columnDefs:[
                {
                    field:"handle",name:"handle",visible: false
                },
                {
                    field:"site",name:"site",visible: false
                },
                {
                    field:"reasonCodeGroup",name:"reasonCodeGroup",displayName:'原因代码组编码',
                    minWidth: 200,
                    //不可编辑时背景颜色为灰色，编码需大写
                    cellClass:'grid-no-editable ad-uppercase',enableCellEdit: $scope.reasonCodeConfig.canEdit,
                    enableCellEditOnFocus: false,validators: { required: true },
                    cellTooltip: function(row, col){return row.entity[col.colDef.name];},
                    cellTemplate: 'andon-ui-grid-rscg/reasonCodeGroup-site',
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row;
                        var col = arguments[0].col;
                        // 非新增不可编辑 、新增行可编辑
                        if(!angular.isUndefined(row.entity) && row.entity!=null && !angular.isUndefined(row.entity.handle) && row.entity.handle!=null && row.entity.handle!=""){
                            return false;
                        }else{
                            return true;
                        }
                    }},
                {
                    field:"description",name:"description",displayName:'原因代码组描述',
                    minWidth: 200,
                    enableCellEdit: $scope.reasonCodeConfig.canEdit, enableCellEditOnFocus: false,validators: { required: true },
                    cellTemplate:'andon-ui-grid-rscg/reasonCodeGroup-description',
                    cellTooltip: function(row, col){  return row.entity[col.colDef.name];  },
                },
                {
                    field:"isDelete",name:"isDelete",visible: false},
                {
                    field:"enabled",name:"enabled",displayName:'启用',width: 50,
                    cellTooltip: function(row, col){ return "启用"; },
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTemplate:'andon-ui-grid-rscg/reasonCodeGroup-checkbox'
                },
                {
                    field:"hasChanged",name:"hasChanged",visible: false,type:"boolean"
                },
                {
                    field:'modifiedDateTime',name:'modifiedDateTime',visible: false
                }],
            data: [],
            onRegisterApi: __onRegisterApi
        };

        $scope.gridReasonCode.data= [];

        $scope.saveReasonCodeGroup = function(){
            //加遮罩，接口正在请求时不允许任何操作
            $scope.showBodyModel("正在保存");
            if(!__checkCanSaveDatas()){
                $scope.hideBodyModel();
                return;
            }

            var reasonCodeParams = [];
            for(var i = 0; i < $scope.gridReasonCode.data.length; i++){
                var row = $scope.gridReasonCode.data[i];
                if(row.hasChanged){
                    //新增数据
                    if(angular.isUndefined(row.handle)){
                        reasonCodeParams.push({
                            "site": row.site,
                            "reasonCodeGroup": row.reasonCodeGroup,
                            "description": row.description,
                            enabled:row.enabled,
                            "modifiedDateTime":row.modifiedDateTime,
                            "viewActionCode": 'C'
                        });
                    }else{
                        //更新数据
                        reasonCodeParams.push({
                            "handle": row.handle,
                            "site": row.site,
                            "reasonCodeGroup": row.reasonCodeGroup,
                            "description": row.description,
                            enabled:row.enabled,
                            "modifiedDateTime":row.modifiedDateTime,
                            "viewActionCode": 'U'
                        });
                    }
                }
            }
            if(reasonCodeParams.length > 0){
                reasonCodeService
                    .reasonCodeSave(reasonCodeParams)
                    .then(function (resultDatas){
                        //保存成功后，消除遮罩
                        $scope.hideBodyModel();
                        $scope.addAlert('success', '保存成功!');
                        //将状态调整为没有修改，此时可以跳转到任何界面不出现提示
                        $scope.bodyConfig.overallWatchChanged = false;
                        //再获取一次表格数据
                        __requestTableDatas();
                    },function (resultDatas){
                        //保存失败后消除遮罩
                        $scope.hideBodyModel();
                        //撤销所有操作，数据回到操作之前
                        $scope.gridReasonCode.data = $scope.config.copyReasonCodeGroupDatas;
                        $scope.addAlert('danger', '保存失败!');
                    });
            }else{
                $scope.hideBodyModel();
                $scope.addAlert('没有修改,无需保存!');
            }
        };

        //是否显示未被启用的数据
        $scope.isShowInActiveDatas = function(){
            __requestTableDatas();
        };

        function __onRegisterApi(gridApi){
            $scope.gridApi = gridApi;
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
                if(newRowCol.col.field == "enabled" || newRowCol.col.name == "enabled"){
                    var datas = $scope.gridReasonCode.data;
                    for(var i=0;i<datas.length;i++){
                        if(datas[i].hasChanged){
                            $scope.bodyConfig.overallWatchChanged = true;
                            return;
                        };
                    };
                };

            });
            // 编辑相关
            $scope.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                $scope.bodyConfig.overallWatchChanged = true;
                var promise = $q.defer();
                if($scope.gridApi.rowEdit){
                    $scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
                }
                if(oldValue == newValue){
                    promise.resolve();
                    return;
                }
                rowEntity.hasChanged = true;
                //原因代码组必须大写
                if(colDef.field == "reasonCodeGroup" || colDef.name=="reasonCodeGroup"){
                    if(UtilsService.isEmptyString(rowEntity.reasonCodeGroup)){
                        return;
                    };
                    rowEntity.reasonCodeGroup = rowEntity.reasonCodeGroup.toUpperCase();
                    
                    if(UtilsService.isContainZhcn(rowEntity.reasonCodeGroup)){
                        promise.reject();
                        $scope.addAlert('', '原因代码不能为汉字!');
                        uiGridValidateService.setInvalid(rowEntity, colDef);
                        rowEntity.reasonCodeZhcnIsValid = false;
                        return;
                    }
                    if(rowEntity.reasonCodeGroup.length != 4){
                        promise.reject();
                        $scope.addAlert('', '原因代码必须是4位!');
                        uiGridValidateService.setInvalid(rowEntity, colDef);
                        rowEntity.reasonCodeCountIsValid = false;
                        return;
                    }
                    rowEntity.reasonCodeCountIsValid = true;
                    rowEntity.reasonCodeZhcnIsValid = true;
                };
                if(colDef.field === "reasonCodeGroup"){
                    if(newValue == '' || newValue == null){
                        $scope.addAlert('danger', '该原因代码组编码不可为空!');
                        promise.resolve();
                    }else{
                        if(!__isUniqueInNewAdd(newValue)){
                            $scope.addAlert('danger', '该原因代码组编码已新增!');
                            promise.resolve();
                            uiGridValidateService.setInvalid(rowEntity, colDef);
                            rowEntity.dataTypeIsValid = false;
                        }else{
                            reasonCodeService
                                .reasonCodeGroupExist(newValue)
                                .then(function (resultDatas){
                                    if(resultDatas.response){ //已经存在:true
                                        console.log("已经存在");
                                        $scope.addAlert('danger', '该原因代码组编码已经存在!');
                                        promise.reject();
                                        uiGridValidateService.setInvalid(rowEntity, colDef);
                                        rowEntity.dataTypeIsValid = false;
                                    }else{	//不存在冲突，可以使用
                                        promise.resolve();

                                        rowEntity.hasChanged = true;
                                        rowEntity.dataTypeIsValid = true;
                                        uiGridValidateService.setValid(rowEntity, colDef);
                                    }
                                },function (resultDatas){ //TODO 检验失败
                                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                                });
                        }
                    }

                }else if(colDef.field === "description"){
                    if(!newValue){
                        $scope.addAlert('danger', '该原因代码组描述不可为空!');
                        promise.resolve();
                    }else{
                        promise.resolve();
                    }
                }else{
                    promise.resolve();
                };

            });

        };

        //判断新增的行数在所有新增的数据里是否唯一
        function __isUniqueInNewAdd(newValue){
            var newAddReasonCodeGroup = $scope.config.newAddReasonCodeGroup;
            console.log(newAddReasonCodeGroup);
            if(angular.isUndefined(newAddReasonCodeGroup) || newAddReasonCodeGroup.length == 0){
                $scope.config.newAddReasonCodeGroup.push(newValue);
                return true;
            }else{
                for(var i = 0 ; i < newAddReasonCodeGroup.length ; i++){
                    if(newAddReasonCodeGroup[i] == newValue){
                        return false;
                    }else{
                        $scope.config.newAddReasonCodeGroup.push(newValue);
                        return true;
                    };
                }
            }
        };

        //保存之前的验证
        function __checkCanSaveDatas(){
            for(var i = 0; i < $scope.gridReasonCode.data.length; i++){
                var item = $scope.gridReasonCode.data[i];
                if(item.hasChanged){
                    var rules = [
                        { field: 'reasonCodeGroup',  emptyDesc: '第'+(i+1)+'行: 编码不能为空!' },
                        { field: 'description',  emptyDesc: '第'+(i+1)+'行: 原因代码组描述不可为空!' }
                    ];
                    for(var ruleIndex in rules){
                        var rule = rules[ruleIndex];
                        if(angular.isUndefined(item[rule.field]) || item[rule.field] == null || item[rule.field] == ""){
                            $scope.addAlert('', rule.emptyDesc);
                            return false;
                        }
                    }   
                    if(item.reasonCodeZhcnIsValid === false){
                        $scope.addAlert('', '第'+(i+1)+'行: 原因代码不能为汉字!');
                        return false;
                    }   
                    if(item.reasonCodeCountIsValid === false){
                        $scope.addAlert('', '第'+(i+1)+'行: 原因代码必须是4位!');
                        return false;
                    }   
                    // if(!angular.isUndefined(item.dataTypeIsValid) && item.dataTypeIsValid==false ){ // 修改后未通过校验
                    if(item.dataTypeIsValid === false){
                        $scope.addAlert('', '第'+(i+1)+'行: 该原因代码组已经存在!');
                        return false;
                    }
                }
            }
            return true;
        };

        //需要加是否显示未被启用的数据  参数config.enabled
        function __requestTableDatas(){
            //$scope
            var enabled = null;
            if(!$scope.config.enabled){
                enabled = true;
            }
            reasonCodeService
                .reasonCodeGroup(enabled)
                .then(function (resultDatas){
                    $scope.bodyConfig.overallWatchChanged = false;
                    $scope.gridReasonCode.data = [];
                // console.log("Success: isTimeout: "+JSON.stringify(resultDatas));
                if(resultDatas.response && resultDatas.response.length > 0){
                    //// $scope.config.plcDataTypeGridOptions.data = resultDatas.response;
                    for(var i = 0; i < resultDatas.response.length; i++){
                        resultDatas.response[i].hasChanged = false;
                        resultDatas.response[i].editRw = $scope.reasonCodeConfig.canEdit;//$scope.config.editRw;
                        if(resultDatas.response[i].enabled == "true"){
                            resultDatas.response[i].enabled = true;
                        }else{
                            resultDatas.response[i].enabled = false;
                        };
                        resultDatas.response[i].threeLight = 'R';
                    }
                    $scope.gridReasonCode.data = resultDatas.response;
                    $scope.config.copyReasonCodeGroupDatas = angular.copy(resultDatas.response);

                    return;
                }else{
                    $scope.addAlert("","暂无数据");
                }
            }, function (resultDatas){
                    $scope.gridReasonCode.data = [];
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        };

    }])