reasonCodeModule
    .service('reasonCodeService', [
    'HttpAppService',
    function (HttpAppService) { 
        return{    
            // 原因代码组维护
            reasonCodeGroup: reasonCodeGroup,
            //判断主键原因代码组编码是否存在
            reasonCodeGroupExist:reasonCodeGroupExist,
            //保存原因代码组
            reasonCodeSave:reasonCodeSave,

            reasonCodeListX: requestResponseCodeList,
            reasonCodeExistX: requestResponCodeExist,
            reasonCodeSaveX: requestResponseCodeSave,
            reasonCodeCountX: requestResponseCodeCount,

            //查询出所有的原因代码
            reasonCodeListAll:requestReasonCodeListAll,
            //原因代码与设备状态关系维护查询
            reasonCodeAndDeviceStatus:requestReasonCodeAndDeviceStatus,
            //查询总数量
            reasonCodeAndDeviceStatusCount:requestReasonCodeAndDeviceStatusCount,
            //查询所有的设备状态
            deviceStatusList:requestdeviceStatusList,
            //新增界面初始化接口 POST
            reasonCodeResStateAdd:requetReasonCodeResStateAdd,
            //新增前的校验接口 POST
            reasonCodeResStateValidateBeforeAdd:requetReasonCodeResStateValidateBeforeAdd,
            //保存新增的设备状态
            deviceStatusSave:requestDeviceStatusSave,

            /////////// 原因代码规则管理 //////////////// 
            // 查询-“原因代码” 输入帮助
            reasonRuleDistinguish: requestReasonRuleDistinguish,
            // 查询-历史变更记录 原因代码、优先级、规则生效日期大于:
            reasonRuleHisChangeLog: requestReasonRuleHisChangeLog,
            // 查询-历史状态记录
            reasonRuleHisStatusLog: requestReasonRuleHisStatusLog,
            // 识别方式作业 输入帮助
            reasonRuleDistinguishInputHelp: requestReasonRuleDistinguishInputHelp,
            // 防呆验证作业 输入帮助
            reasonRuleCheckInputHelp: requestReasonRuleCheckInputHelp,
            // 识别优先级增删改接口:
            reasonRulePriorityChange: requestReasonRulePriorityChange,
            
            // 原因代码详情 
            reasonRuleDetailAll: requestReasonRuleDetailAll, 
            // “详细信息”-“防呆验证”参数值按钮 
            reasonRuleDetailStayparams: requestReasonRuleDetailStayParams, 

            // 识别优先级详情
            reasonRulePriorityDetail: requestReasonRulePriorityDetail,


            // 识别方式详情 “详细信息”-“识别方式作业”修改 
            reasonRulePatternDetail: requestReasonRulePatternDetail,
            reasonRulePatternUpdate: requestReasonRulePatternUpdate, 

            
            // “详细信息”-“防呆验证作业”修改 
            reasonRuleActivityParam: requestReasonRuleActivityParam,
            
            // “新建”-“防呆验证”参数值按钮 
            reasonRuleNewFilterParam: requestReasonRuleNewFilterParam,
            
            

            // 删除   POST 
            reasonRuleDeleteRecognition: requestReasonRuleDeleteRecognition,            
            

            // “详细信息”- “防呆验证”编辑保存  ！！！POST 
            reasonRuleParamFChange: requestReasonRuleParamFChange,
            
            // “新建”- “防呆验证”By设备类型参数值保存   POST
            reasonRuleTypeParamSave: requestReasonRuleTypeParamSave,
            
            // “新建”-“防呆验证作业”保存   POST
            reasonRuleFilterActivitySave: requestReasonRuleFilterActivitySave,
            
            // 识别方式新增接口    POST
            reasonRuleCreatePattern: requestReasonRuleCreatePattern,
            
            // 识别方式修改接口 POST
            reasonRuleUpdatePattern: requestReasonRuleUpdatePattern,
            

            
            




        };
        function reasonCodeGroup(params){
            var url = HttpAppService.URLS.REASON_CODE_GROUP
                .replace("#{SITE}#", HttpAppService.getSite());
            if(params){
                url = url + "?enabled="+ params
            }
            return HttpAppService.get({
                url: url
            });
        };
        function reasonCodeGroupExist(newValue){
            var url = HttpAppService.URLS.REASON_CODE_GROUP_EXIST
                .replace("#{SITE}#", HttpAppService.getSite())
                .replace("#{REASON_CODE_GROUP}#",newValue);
            return HttpAppService.get({
                url: url
            });
        };
        function reasonCodeSave(reasonCodeParams){
            var url = HttpAppService.URLS.REASON_CODE_GROUP_CHANGE;
            return HttpAppService.post({
                url: url,
                paras : reasonCodeParams
            });
        };

        function requestResponseCodeList(reasonCodeGroup, enabled,pageIndex,pageCount){
            var url = HttpAppService.URLS.REASON_CODE_LIST
                .replace("#{SITE}#", HttpAppService.getSite())
                .replace("#{REASON_CODE_GROUP}#", reasonCodeGroup)
                .replace("#{INDEX}#", pageIndex)
                .replace("#{COUNT}#", pageCount)
                ;
            if(enabled){
                url = url + "&enabled=" + enabled;
            };
            return HttpAppService.get({
                url: url
            });
        };
        function requestResponCodeExist(reasonCode){
            var url = HttpAppService.URLS.REASON_CODE_EXIST
                .replace("#{SITE}#", HttpAppService.getSite())
                .replace("#{REASON_CODE}#", reasonCode)
                ;
            return HttpAppService.get({
                url: url
            });
        };
        function requestResponseCodeSave(reasonCodeParams){
            var url = HttpAppService.URLS.REASON_CODE_SAVE;
            return HttpAppService.post({
                url: url,
                paras : reasonCodeParams
            });
        };
        function requestResponseCodeCount(reasonCodeGroup,enabled){
            var url = HttpAppService.URLS.REASON_CODE_COUNT
                .replace("#{SITE}#", HttpAppService.getSite())
                .replace("#{REASON_CODE_GROUP}#", reasonCodeGroup);
            if(enabled){
                url = url + "&enabled=" + enabled;
            };
            return HttpAppService.get({
                url: url
            });
        };
        
        //原因代码与设备状态关系维护查询
        // var deviceParams = {
        //     "site" : HttpAppService.getSite(),
        //     "reasonCodeGroup" : reasonCodeGroup,
        //     "reasonCode": reasonCode,
        //     "dateTime" : reasonCodeAndDeviceDate,
        //     "pageIndex" : pageIndex,
        //     "pageCount" : pageCount
        // };

        function requestReasonCodeListAll(reasonCodeGroup,enabled){
            var url = HttpAppService.URLS.REASON_CODE_LIST_ALL
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{REASON_CODE_GROUP}#", reasonCodeGroup)
                .replace("#{ENABLED}#", enabled);
            return HttpAppService.get({
                url: url
            });
        };
        function requestReasonCodeAndDeviceStatus(deviceParams){
            var url = HttpAppService.URLS.REASON_CODE_AND_DEVICE_STATUS
                                    .replace("#{SITE}#", HttpAppService.getSite())
                                    .replace("#{REASON_CODE_RES_STATE}#", deviceParams.reasonCodeGroup)
                                    .replace("#{DATE_TIME}#", deviceParams.dateTimeIn)
                                    .replace("#{INDEX}#", deviceParams.pageIndex)
                                    .replace("#{COUNT}#", deviceParams.pageCount)
                                    ;
            if(deviceParams.reaonCode){
                url = url + "&reaon_code="+deviceParams.reaonCode
            };
            return HttpAppService.get({ 
                url: url
            });
        };
        //查询总数
        function requestReasonCodeAndDeviceStatusCount(deviceParams){
            var url = HttpAppService.URLS.REASON_CODE_AND_DEVICE_STATUS_COUNT
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{REASON_CODE_RES_STATE}#", deviceParams.reasonCodeGroup)
                    .replace("#{DATE_TIME}#", deviceParams.dateTime)
                ;
            if(deviceParams.reaonCode){
                url = url + "&reaon_code="+deviceParams.reaonCode
            };
            return HttpAppService.get({
                url: url
            });
        };
        //新增界面初始化接口 POST
        function requetReasonCodeResStateAdd(reasonCodeGroupParams){
            var url = HttpAppService.URLS.REASON_CODE_RES_STATE_ADD
                .replace("#{SITE}#", HttpAppService.getSite())
                .replace("#{REASON_CODE_GROUP}#",reasonCodeGroupParams.reasonCodeGroup);
            if(reasonCodeGroupParams.reaonCode){
                url = url + "&reaon_code="+reasonCodeGroupParams.reaonCode
            };
            return HttpAppService.get({
                url: url
            });
        };
        
        //查询所有的设备状态
        function requestdeviceStatusList(enabled){
            var url = HttpAppService.URLS.DEVICE_STATUS_LIST
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{ENABLED}#", enabled);
            return HttpAppService.get({
                url: url
            });
        };
        //新增前的校验GET
        function requetReasonCodeResStateValidateBeforeAdd(deviceValidateParams){
            var url = HttpAppService.URLS.REASON_CODE_RES_STATUS_VALIDATE_BEFORE_ADD
                .replace("#{SITE}#",HttpAppService.getSite())
                .replace("#{REASON_CODE}#",deviceValidateParams.reasonCode)
                .replace("#{DATE_TIME}#",deviceValidateParams.dateTime)
                .replace("#{STATE}#",deviceValidateParams.state);
            return HttpAppService.get({
                url: url
            });
        };
        //保存所有新增的设备状态
        function requestDeviceStatusSave(deviceParams){
            var url = HttpAppService.URLS.DEVICE_STATUS_SAVE;
            return HttpAppService.post({
                url: url,
                paras : deviceParams
            });
        };


        ////////// 原因代码规则管理 ////////////
        // REASON_RULE_DISTINGUISH: baseUrl + "plant/reason_code/distinguish/site/#{site}#/reason_code?reasonCode=#{REASON_CODE}#",    
        function requestReasonRuleDistinguish(reasonCode){
            var url = HttpAppService.URLS.REASON_RULE_DISTINGUISH
                .replace("#{SITE}#",HttpAppService.getSite())
                .replace("#{REASON_CODE}#",reasonCode)
                ;
            return HttpAppService.get({
                url: url
            });
        }
        // REASON_RULE_HIS_CHANGE_LOG: baseUrl + "plant/reason_code/distinguish/site/#{site}#/history_change_log?reasonCode=#{REASON_CODE}#&priority=#{PRIORITY}#&dateTime=#{DATE_TIME}#",
        function requestReasonRuleHisChangeLog(params){
            var url = HttpAppService.URLS.REASON_RULE_HIS_CHANGE_LOG
                .replace("#{SITE}#",HttpAppService.getSite())
                .replace("#{REASON_CODE}#",params.reasonCode)
                .replace("#{PRIORITY}#",params.priority)
                .replace("#{DATE_TIME}#",params.dateTime)
                ;
            return HttpAppService.get({
                url: url
            });
        }

        // REASON_RULE_HIS_STATUS_LOG: baseUrl + "plant/reason_code/distinguish/site/#{site}#/history_status_log?reasonCode=#{REASON_CODE}#&priority=#{PRIORITY}#&dateTime=#{DATE_TIME}#",
        function requestReasonRuleHisStatusLog(params){ 
            var url = HttpAppService.URLS.REASON_RULE_HIS_STATUS_LOG
                .replace("#{SITE}#",HttpAppService.getSite())
                .replace("#{REASON_CODE}#",params.reasonCode)
                .replace("#{PRIORITY}#",params.priority)
                .replace("#{DATE_TIME}#",params.dateTime)
                ;
            return HttpAppService.get({
                url: url
            });
        }
        // REASON_RULE_DISTINGUISH_INPUT_HELP: baseUrl + "plant/reason_code/distinguish/distinguish_input_help?activity=#{ACTIVITY}#",
        function requestReasonRuleDistinguishInputHelp(activity){
            var url = HttpAppService.URLS.REASON_RULE_DISTINGUISH_INPUT_HELP
                .replace("#{SITE}#",HttpAppService.getSite())
                .replace("#{ACTIVITY}#",activity)
                ;
            return HttpAppService.get({
                url: url
            });
        }
        // REASON_RULE_CHECK_INPUT_HELP: baseUrl + "plant/reason_code/distinguish/check_input_help?activity=#{ACTIVITY}#",
        function requestReasonRuleCheckInputHelp(activity){
            var url = HttpAppService.URLS.REASON_RULE_CHECK_INPUT_HELP
                .replace("#{SITE}#",HttpAppService.getSite())
                .replace("#{ACTIVITY}#",activity)
                ;
            return HttpAppService.get({
                url: url
            });
        }
        // REASON_RULE_PRIORITY_CHANGE: baseUrl + "plant/reason_code/distinguish/changeReasonCodePriority",
        function requestReasonRulePriorityChange(paramActions){
            var url = HttpAppService.URLS.REASON_RULE_PRIORITY_CHANGE;
            return HttpAppService.post({
                url: url,
                paras: paramActions
            });
        }
        // 
        function requestReasonRuleDetailAll(reasonCodePriorityBo){
            var url = HttpAppService.URLS.REASON_RULE_DETAIL_ALL
                        .replace("#{REASON_CODE_PRIORITY_BO}#", reasonCodePriorityBo)
                        ;
            return HttpAppService.get({
                url: url
            });
        }
        
        
        
        
        
        function requestReasonRuleDetailStayParams(reasonCodeRuleBoF, activityFParamId){
            var url = HttpAppService.URLS.REASON_RULE_DETAIL_STAY_PARAMS
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{REASON_CODE_RULE_BO_F}#", reasonCodeRuleBoF)
                    .replace("#{ACTIVITY_F_PARAM_ID}#", activityFParamId)
                    ;
            return HttpAppService.get({
                url: url
            });
        }

        function requestReasonRulePriorityDetail(reasonCodePriorityBO){
            var url = HttpAppService.URLS.REASON_RULE_PRIORITY_DETAIL
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{REASON_CODE_PRIORITY_BO}#", reasonCodePriorityBO)
                    ;
            return HttpAppService.get({
                url: url
            });
        }
        function requestReasonRulePatternDetail(activityBo, executionType){
            var url = HttpAppService.URLS.REASON_RULE_PATTERN_DETAIL
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{ACTIVITY_BO}#", activityBo)
                    .replace("#{EXECUTION_TYPE}#", executionType)
                    ;
            return HttpAppService.get({
                url: url
            });
        }
        // {
        //     easonCodePriorityBO: 
        //     site: 
        //     activity: 
        //     activityCode: 
        //     ruleParams: 
        // }   
        function requestReasonRulePatternUpdate(params){
            var url = HttpAppService.URLS.REASON_RULE_PATTERN_UPDATE
                            .replace("#{SITE}#", HttpAppService.getSite())
                            ;
            return HttpAppService.post({
                url: url,
                paras: params
            });
        }

        function requestReasonRuleActivityParam(activityBo, executionType){
            var url = HttpAppService.URLS.REASON_RULE_ACTIVITY_PARAM
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{ACTIVITY_BO}#", activityBo)
                    .replace("#{EXECUTION_TYPE}#", executionType)
                    ;
            return HttpAppService.get({
                url: url
            });
        }
        function requestReasonRuleNewFilterParam(activityBo, activityFParamId){
            var url = HttpAppService.URLS.REASON_RULE_NEW_FILTER_PARAM
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{ACTIVITY_BO}#", activityBo)
                    .replace("#{ACTIVITY_F_PARAM_ID}#", activityFParamId)
                    ;
            return HttpAppService.get({
                url: url
            });
        }
        function requestReasonRuleDeleteRecognition(reasonCodePriorityBOArray){
            var url = HttpAppService.URLS.REASON_RULE_NEW_D_RECOGNITION
                            .replace("#{SITE}#", HttpAppService.getSite())
                            // .replace("#{REASON_CODE_PRIORITY_BO}#", reasonCodePriorityBOArray)
                            ;    
            return HttpAppService.post({
                url: url,
                paras: reasonCodePriorityBOArray
            });
        }
        function requestReasonRuleParamFChange(paramActions){
            var url = HttpAppService.URLS.REASON_RULE_RULE_PARAM_F_CHANGE
                            .replace("#{SITE}#", HttpAppService.getSite());    
            return HttpAppService.post({
                url: url,
                paras: paramActions
            });
        }
        function requestReasonRuleTypeParamSave(paramActions){
            var url = HttpAppService.URLS.REASON_RULE_NEW_TYPE_PARAM_SAVE
                            .replace("#{SITE}#", HttpAppService.getSite());    
            return HttpAppService.post({
                url: url,
                paras: paramActions
            });
        }
        function requestReasonRuleFilterActivitySave(paramActions){
            var url = HttpAppService.URLS.REASON_RULE_NEW_FILTER_ACTIVITY_SAVE
                            .replace("#{SITE}#", HttpAppService.getSite());
            return HttpAppService.post({
                url: url,
                paras: paramActions
            });
        }
        function requestReasonRuleCreatePattern(paramActions, params){
            // REASON_RULE_NEW_C_PATTERN: baseUrl + "planttern?Activity=&   =  &site=#{SITE}#",
            var url = HttpAppService.URLS.REASON_RULE_NEW_C_PATTERN
                            .replace("#{SITE}#", HttpAppService.getSite())
                            .replace("#{ACTIVITY}#", params.activity)
                            .replace("#{REASONCODE_PRIORIY_BO}#", params.reasonCodePriorityBO)
                            ;
            return HttpAppService.post({
                url: url,
                paras: paramActions
            });
        }
        function requestReasonRuleUpdatePattern(paramActions){
            var url = HttpAppService.URLS.REASON_RULE_NEW_U_PATTERN
                            .replace("#{SITE}#", HttpAppService.getSite());
            return HttpAppService.post({
                url: url,
                paras: paramActions
            });
        } 
        

    }]);

reasonCodeModule
    .run([ 
        '$templateCache',
        function ($templateCache){
            $templateCache.put('andon-ui-grid-rscg/reasonCodeGroup-site',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.reasonCodeGroup\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
            );
            $templateCache.put('andon-ui-grid-rscg/reasonCodeGroup-description',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.description\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
            );
            $templateCache.put('andon-ui-grid-rscg/reasonCodeGroup-checkbox',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents flex-center\"  ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><input type=\"checkbox\" class=\"checkbox-column\"  ng-model=\"row.entity.enabled\" ng-change=\"row.entity.hasChanged = (row.entity.hasChanged)?row.entity.hasChanged:!row.entity.hasChanged \" /></div>"
            );
            $templateCache.put('andon-ui-grid-tpls/reasonCode-reasonCode',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\"  ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><span ng-bind=\"row.entity.reasonCode\"></span></div>"
            );
            
            $templateCache.put('andon-ui-grid-tpls/reasonCodeRule-viewDetail',
                "<div class=\"ui-grid-cell-contents\"  ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><span style=\"text-align:center;\">&#x8BE6;&#x60C5;</span></div>"
            );
            
            
            $templateCache.put('andon-ui-grid-tpls/reasonCodeRule-PreventStay-paramValue',          
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\"> <span ng-if=\"(row.entity.actionType==undefined || row.entity.actionType == null || row.entity.actionType=='') && row.entity.resourceTypeDesc!='By设备类型'\" ng-bind=\"row.entity.paramValue\"></span>  <span ng-if=\"row.entity.resourceTypeDesc=='By设备类型'\">参数值</span>      <span ng-if=\"row.entity.resourceTypeDesc=='无' && row.entity.actionType=='default'\" ng-bind=\"row.entity.paramSetValue\"></span>   <span ng-if=\"row.entity.resourceTypeDesc=='无' && row.entity.actionType=='notUse'\"></span>   <div ng-if=\"row.entity.resourceTypeDesc=='无' && row.entity.actionType=='set'\" ng-bind=\"row.entity.paramSetValue\"></div>   </div>"
            );
            $templateCache.put('andon-ui-grid-tpls/reasonCodeRule-params-paramValue',          
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\"> <span ng-if=\"(row.entity.actionType==undefined || row.entity.actionType == null || row.entity.actionType=='')\" ng-bind=\"row.entity.paramValue\"></span>  <span ng-if=\"row.entity.actionType=='default'\" ng-bind=\"row.entity.paramSetValue\"></span>   <span ng-if=\"row.entity.actionType=='notUse'\"></span>   <div ng-if=\"row.entity.actionType=='set'\" ng-bind=\"row.entity.paramSetValue\"></div>   </div>"
            );
            // $templateCache.put('andon-ui-grid-tpls/reasonCodeRule-PreventStay-setValue-edit',
            //     "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\"><span ng-if=\"resourceTypeDesc=='By设备类型'\">参数值</span>   <span ng-if=\"row.entity.resourceTypeDesc=='无' && row.entity.actionType=='default'\" ng-bind=\"row.entity.paramSetValue\"></span>   <span ng-if=\"row.entity.resourceTypeDesc=='无' && row.entity.actionType=='notUse'\">*</span>   <input type=\"text\" ng-if=\"row.entity.resourceTypeDesc=='无' && row.entity.actionType=='set'\" ng-model=\"row.entity.paramSetValue\"/>   </div>"
            // );
            
            
            
            $templateCache.put('andon-ui-grid-tpls/reasonCodeRule-PreventStay-param-actions',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\"><span> <div class=\"action-item\"><input type=\"radio\"  name=\"adRadioParamAction\" value=\"1\"><span>使用默认值</span></div>     <div class=\"action-item\"><input type=\"radio\" name=\"adRadioParamAction\" value=\"1\"><span>使用设定值</span></div>  <div class=\"action-item\"><input type=\"radio\" name=\"adRadioParamAction\" value=\"1\"><span>不使用</span></div> </span></div>"
            );

            $templateCache.put('andon-ui-grid-tpls/reasonCode-recognition-rule-param-f-resourceTypeDesc',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\"><span> <div class=\"action-item\"><input type=\"radio\"  name=\"adRadioParamAction\" value=\"1\"><span>使用默认值</span></div>     <div class=\"action-item\"><input type=\"radio\" name=\"adRadioParamAction\" value=\"1\"><span>使用设定值</span></div>  <div class=\"action-item\"><input type=\"radio\" name=\"adRadioParamAction\" value=\"1\"><span>不使用</span></div> </span></div>"
            );
            $templateCache.put('andon-ui-grid-tpls/reasonCodeRule-used',
                // "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents flex-center\"  ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><input type=\"checkbox\" class=\"checkbox-column\"  ng-model=\"row.entity.enabled\" ng-change=\"row.entity.hasChanged = (row.entity.hasChanged)?row.entity.hasChanged:!row.entity.hasChanged \" /></div>"
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents flex-center\"   ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><input type=\"checkbox\" ng-true-value=\"'true'\" class=\"checkbox-column\" disabled=\"disabled\"  ng-model=\"row.entity.reasonCodePO.used\" /></div>"
            );

            $templateCache.put('andon-ui-grid-tpls/reasonCodeSelectModel-Enabled',
                // "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents flex-center\"  ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><input type=\"checkbox\" class=\"checkbox-column\"  ng-model=\"row.entity.enabled\" ng-change=\"row.entity.hasChanged = (row.entity.hasChanged)?row.entity.hasChanged:!row.entity.hasChanged \" /></div>"
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents flex-center\" ><input type=\"checkbox\" ng-true-value=\"'true'\" class=\"checkbox-column\" disabled=\"disabled\"  ng-model=\"row.entity.reasonCodeEnabled\" /></div>"
            );
             $templateCache.put('andon-ui-grid-tpls/reasonCodeSelectModel-groupEnabled',
                // "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents flex-center\"  ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><input type=\"checkbox\" class=\"checkbox-column\"  ng-model=\"row.entity.enabled\" ng-change=\"row.entity.hasChanged = (row.entity.hasChanged)?row.entity.hasChanged:!row.entity.hasChanged \" /></div>"
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents flex-center\" ><input type=\"checkbox\" ng-true-value=\"'true'\" class=\"checkbox-column\" disabled=\"disabled\"  ng-model=\"row.entity.reasonCodeGroupEnabled\" /></div>"
            );

    }]);















