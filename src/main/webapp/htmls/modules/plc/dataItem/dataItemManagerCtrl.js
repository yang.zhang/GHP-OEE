/**
 * 
 * [数据项管理 Controller: dataItemManagerCtrl]
 * 
 */
plcModule.controller('dataItemManagerCtrl',[
	'$scope', '$http', '$q', 'UtilsService', 'HttpAppService',
    'HttpAppService', '$timeout', 'plcService', 'UtilsService',
    'uiGridConstants', 'uiGridValidateService', '$state',
    function ($scope, $http, $q, UtilsService, HttpAppService,
    		HttpAppService, $timeout, plcService, UtilsService,
            uiGridConstants, uiGridValidateService, $state) {
        $scope.menus = ['PLC数据项管理'];
        
        $scope.init = function(){ 
        	__requestSelectListDatas();
        	//$scope.config.selectedPlcCategory = $scope.config.plcCategories[0];
        };	
        
        $scope.config = {
        	gridApi: null,
        	needAddedRowsNum: 1,  // 需要新增行数
        	deletedRows: [],      // 缓存删除且未提交的表格行数据
        	plcCategories: [],
        	selectedPlcCategory: null,   // 选中的数据类型
        	
        	btnDisabledQuery: true,  // 是否禁用<查询>按钮
            btnDisabledAdd: true,    // 是否禁用<新增>按钮
            btnDisabledDelete: true, // 是否禁用<删除>按钮
            btnDisabledSave: true,   // 是否禁用<保存>按钮
            inputDisabledNum: true,  // 是否禁用<新增行数>输入框
            
            canEdit: false, 			// 当前用户是否对该activity有编辑权限

			isFirstAddClass:true
        };
        $scope.config.canEdit = $scope.modulesRWFlag("#"+$state.$current.url.source);

        // $scope.config.canEditObj = $scope.modulesRWFlag("#"+$state.$current.url.source);
        // if(!angular.isUndefined($scope.config.canEditObj) || $scope.config.canEditObj != null){
        // 	$scope.config.canEdit = $scope.config.canEditObj.write;
        // }	

        // $timeout(function(){
        // 	$scope.config.canEdit = true;
        // 	console.log(" config.canEdit ");
        // }, 20000);

        $scope.plcDataItemGridOptions = {
        	// 分页相关设置
        	enablePagination: true,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,
            paginationCurrentPage: 1,
            totalItems: 0,
            useExternalPagination: true,
		    

		    showGridFooter: false, 
		    modifierKeysToMultiSelectCells: false, 
		    columnDefs: [
		    	{ 
		    		field: 'handle', name: 'handle', visible: false
		    	}, 
		    	{   
		    		field: 'isSelected', name: 'isSelected', displayName: '选择', minWidth: 60, maxWidth: 60,
		    		enableCellEdit: $scope.config.canEdit, type: 'boolean', visible: false,
		    		enableColumnMenu: false, enableColumnMenus: false, enableSorting: false 
		    	},
		    	{
		    		field: 'site', name: 'site', displayName: '工厂', minWidth: 100,
		    		enableCellEdit: $scope.config.canEdit, enableCellEditOnFocus: false, visible: false,
		    		validators: { required: true }, 
		    		cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
		    		cellEditableCondition: function($scope){
		    			return false;
		    			// var row = arguments[0].row;
		    			// var col = arguments[0].col;
		    			// // 非新增不可编辑 、新增行可编辑 
		    			// if(!angular.isUndefined(row.entity) && row.entity!=null && !angular.isUndefined(row.entity.handle) && row.entity.handle!=null && row.entity.handle!=""){
		    			// 	return false;
		    			// }else{
		    			// 	return true;
		    			// }
		    		}
				    // 		editableCellTemplate: 'ui-grid/dropdownEditor',
				    // 		editDropdownValueLabel: 'site',
				    // 		editDropdownOptionsArray: [
				    // 			{ id: 1000, site: '1000' },
								// { id: 2000, site: '2000' },
								// { id: 3000, site: '3000' }
				    // 		]
		    	},
		    	{
		    		field: 'plcCategoryDescription', name: 'plcCategoryDescription', displayName: 'PLC数据类型描述', minWidth: 150,
		    		enableCellEdit: $scope.config.canEdit, enableCellEditOnFocus: false, validators: { required: true }, cellClass:'grid-no-editable',
		    		// cellTemplate: 'andon-ui-grid-tpls/dataCategory-categoryCode',
		    		cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
		    		cellEditableCondition: function($scope){
		    			return false;
		    			// var row = arguments[0].row;
		    			// var col = arguments[0].col;
		    			// // 非新增不可编辑 、新增行可编辑 
		    			// if(!angular.isUndefined(row.entity) && row.entity!=null && !angular.isUndefined(row.entity.handle) && row.entity.handle!=null && row.entity.handle!=""){
		    			// 	return false;
		    			// }else{
		    			// 	return true;
		    			// }
		    		}
		    	},
		    	{ 
		    		field: 'plcObject', name: 'plcObject', displayName: 'PLC数据项代码', minWidth: 200,
		    		enableCellEdit: $scope.config.canEdit, enableCellEditOnFocus: false, validators: { required: true },
		    		cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
		    		cellTemplate: 'andon-ui-grid-tpls/dataItem-plcObject', cellClass:'grid-no-editable ad-uppercase',
		    		cellEditableCondition: function($scope){ 
		    			var row = arguments[0].row; 
		    			var col = arguments[0].col; 
		    			// 非新增不可编辑 、新增行可编辑
		    			if(!angular.isUndefined(row.entity) && row.entity!=null && !angular.isUndefined(row.entity.handle) && row.entity.handle!=null && row.entity.handle!=""){
		    				return false;
		    			}else{
		    				return true;
		    			}
		    		}
		    	},
		    	{
		    		field: 'plcObjectDescription', name: 'plcObjectDescription', displayName: 'PLC数据项描述', minWidth: 200,
		    		enableCellEdit: $scope.config.canEdit, enableCellEditOnFocus: false, validators: {  required: true },
		    		// cellTemplate: 'andon-ui-grid-tpls/dataCategory-categoryDesc' 
		    		cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
		    	},
		    	{
		    		field: 'isDeleted', name: 'isDeleted', displayName: '删除', width: 40,
		    		enableCellEdit: false, enableCellEditOnFocus: false,
		    		enableColumnMenu: false, enableColumnMenus: false,
		    		visible: $scope.config.canEdit,
		    		cellTooltip: function(row, col){ return "删除该行"; }, 
		    		cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col"><span class="ion-trash-a"></span></div>',
		    		headerTemplate: '<div class="xbr-delete-col-header">删除</div>'
		    	}
		    ], 
		    data: [],
		    onRegisterApi: __onRegisterApi 
        };

        /**
         * 
         * [dataCategoryChange 选择的数据类型更改后触发]
         * 
         */
        $scope.dataCategoryChange = function(){
        	var sc = $scope.config.selectedPlcCategory;
        	// scope.config.selectedPlcCategory.handle
        	if(sc.handle == null){
        		$scope.config.btnDisabledQuery = true;
        		$scope.config.btnDisabledAdd = true;
        		$scope.config.btnDisabledDelete = true;
        		$scope.config.btnDisabledSave = true;
        		$scope.config.inputDisabledNum = true;
        	}else{
        		$scope.config.btnDisabledQuery = false;
        		$scope.config.btnDisabledAdd = false;
        		// $scope.config.btnDisabledDelete = false;
        		// $scope.config.btnDisabledSave = false;
        		$scope.config.inputDisabledNum = false;
        	}
        	if(!$scope.$$phase){
            	$scope.$apply();
            }
        };
        /**
         * 
         * [queryDatas 点击查询按钮时触发]
         * 
         */
        $scope.queryDatas = function(){
			if($scope.bodyConfig.overallWatchChanged){
				var promise = UtilsService.confirm('重新查询会刷新现有数据', '提示', ['确定', '取消']);
				promise
					.then(function(){ // ok
						$scope.bodyConfig.overallWatchChanged = false;
						__requestTableDatasCount();
						__requestTableDatas();
					}, function(){   // cancel

					});
			}else{
				__requestTableDatasCount();
				__requestTableDatas();
			}

        };
        /**
         * 
         * [addRows 点击新增按钮时触发]
         * 
         */
        $scope.addRows = function(){
			if(angular.isUndefined($scope.plcDataItemGridOptions.data) || $scope.plcDataItemGridOptions.data == null){
				$scope.plcDataItemGridOptions.data = [];
			}
			
			for(var i = 0; i < $scope.config.needAddedRowsNum; i++){
				$scope.config.btnDisabledSave = false;
				$scope.bodyConfig.overallWatchChanged = true;
				$scope.plcDataItemGridOptions.data.unshift({
					site: HttpAppService.getSite(),
					category: $scope.config.selectedPlcCategory.category,
					plcCategoryDescription: $scope.config.selectedPlcCategory.description,
					hasChanged: true
				});
			}
		};
		/**
         * 
         * [checkAndDeleteSelectedRows 点击删除按钮时触发]
         * 
         */
		$scope.checkAndDeleteSelectedRows = function(){ 
			var deletedRowIndexs = [];
			var rows = $scope.config.gridApi.selection.getSelectedRows();
			for(var i = 0; i < rows.length; i++){
				var row = rows[i];
				if( !angular.isUndefined(row.handle) && row.handle!=null && row.handle!=""){
					plcService
						.plcDataItemIsUsed(row.plcObject)
						.then(function (resultDatas){ 
							var endIndex = resultDatas.config.url.indexOf('&');
							var plcObject = resultDatas.config.url.substring(resultDatas.config.url.lastIndexOf("plc_object=")+11, endIndex);
							if(resultDatas.response === true){ // 不允许删除
								for(var i = 0; i < $scope.plcDataItemGridOptions.data.length; i++){
									if($scope.plcDataItemGridOptions.data[i].plcObject == plcObject){
										$scope.plcDataItemGridOptions.data[i].canDeleted = false;   
										break;
									}
								}
								$scope.addAlert('danger', plcObject+" 已被引用,不允许删除!");
							}else{
								// row.canDeleted = true;
								for(var i = 0; i < $scope.plcDataItemGridOptions.data.length; i++){
									if($scope.plcDataItemGridOptions.data[i].plcObject == plcObject){
										$scope.plcDataItemGridOptions.data[i].canDeleted = true;
										$scope.plcDataItemGridOptions.data[i].isSelected = true;
										break;
									}
								}
								__chekForDeleteRows();
							}
						}, function (errorResultDatas){
							$scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
						});
				}else{
					row.canDeleted = true;
					row.isSelected = true;
				}
			}
			__chekForDeleteRows();
		};
		/**
         * 
         * [__chekForDeleteRows 删除前准备工作]
         * 
         */
		function __chekForDeleteRows(){
			var canDeletes = true;
			var deletedRowIndexs = [];
			var rows = $scope.plcDataItemGridOptions.data;
			for(var i = 0; i < rows.length; i++){
				var row = rows[i];
				if(row.isSelected){
					if(row.canDeleted !== true){
						canDeletes = false;
					}
				}
			}
			if(canDeletes){
				for (var i = rows.length - 1; i >= 0; i--) {
					var row = rows[i];
					if(row.isSelected){
						row.isDeleted = true;
				        deletedRowIndexs.push(i);
				        $scope.config.deletedRows.push(angular.copy(row));
					}
				};
			}else{
				return;	
			}
			var length = deletedRowIndexs.length;
			for(var i = 0; i < length; i++){
				$scope.bodyConfig.overallWatchChanged = true;
				$scope.plcDataItemGridOptions.data.splice(deletedRowIndexs[i], 1);
				// $scope.plcDataItemGridOptions.data = UtilsService.removeArrayByIndex($scope.plcDataItemGridOptions.data, deletedRowIndexs.pop());
			}
		};

		/**
         * 
         * [saveAllAcitons 点击保存按钮时触发]
         * 
         */
		$scope.saveAllAcitons = function(){
			if(!__checkCanSaveDatas()){
				return;
			};
			var paramActions = []; 
			for(var i = 0; i < $scope.plcDataItemGridOptions.data.length; i++){
				var row = $scope.plcDataItemGridOptions.data[i]; 
				if(row.hasChanged){
					// var viewActionCode = angular.isUndefined(row.handle) ? 'C' : 'U';
					if(angular.isUndefined(row.handle)){ //创建新数据 ，若是删除中存在该条数据，则直接忽略该行,并在后面将删除操作也忽略
						if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
							paramActions.push({
								"handle": row.handle, 
								//"site": HttpAppService.getSite(),
								"site": row.site, 
								"category": row.category,  
								"plcObjectDescription": row.plcObjectDescription,
								"plcObject": row.plcObject,
								"viewActionCode": 'C'  
							});  
						}else{}
					}else{ //修改已存于服务器的行数据 ，若删除列表中有该行，则直接忽略该修改提交，并在后面将删除操作添加进来
						if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
							paramActions.push({
								"handle": row.handle, 
								//"site": HttpAppService.getSite(),
								"site": row.site, 
								"category": row.category,  
								"plcObjectDescription": row.plcObjectDescription,  
								"plcObject": row.plcObject,
								"viewActionCode": 'U',
								"modifiedDateTime": row.modifiedDateTime
							});
						}else{}
					}
				}
			}
			for(var i = 0; i < $scope.config.deletedRows.length; i++){
				var deleteTempRow = $scope.config.deletedRows[i];
				if(!angular.isUndefined(deleteTempRow.handle)){// 服务器端数据的删除操作,直接添加进去即可
					paramActions.push({
						"handle": deleteTempRow.handle,
						"site": deleteTempRow.site,
						"category": deleteTempRow.category, 
						"plcObjectDescription": deleteTempRow.plcObjectDescription, 
						"plcObject": deleteTempRow.plcObject,
						"viewActionCode": 'D',
						"modifiedDateTime": deleteTempRow.modifiedDateTime
					});
				}else{ }
			}	
			if(paramActions.length > 0){
				$scope.showBodyModel("正在保存");
				plcService 
					.plcDataItemChange(paramActions)
					.then(function (resultDatas){ 
						$scope.hideBodyModel();
						$scope.config.deletedRows = [];
						$scope.addAlert('success', '保存成功!');
						$scope.bodyConfig.overallWatchChanged = false;
						__requestTableDatas();
					},function (resultDatas){ 
						$scope.hideBodyModel();
						$scope.addAlert('danger', '保存失败!');
						$scope.bodyConfig.overallWatchChanged = false;
					});
			}else{ 
				$scope.addAlert('没有修改,无需保存!');
			}
		};
		/**
         * 
         * [__checkCanSaveDatas 保存前的数据检验]
         * 
         */
		function __checkCanSaveDatas(){
			for(var i = 0; i < $scope.plcDataItemGridOptions.data.length; i++){
				var item = $scope.plcDataItemGridOptions.data[i];
				if(item.hasChanged){
					var rules = [
						{ field: 'site',  emptyDesc: '第'+(i+1)+'行: 工厂不能为空!' },
						{ field: 'plcCategoryDescription', emptyDesc: '第'+(i+1)+'行: 数据类型不能为空!' },
						{ field: 'plcObject', emptyDesc: '第'+(i+1)+'行: 数据项类型不能为空!' },
						{ field: 'plcObjectDescription', emptyDesc: '第'+(i+1)+'行: 数据项类型描述不能为空!' },
					];
					for(var ruleIndex in rules){
						var rule = rules[ruleIndex];
						if(angular.isUndefined(item[rule.field]) || item[rule.field] == null || item[rule.field] == ""){
							$scope.addAlert('', rule.emptyDesc);
							return false;
						}
					}
				}
				if(item.plcObjectIsValid === false){
					$scope.addAlert('danger', '第'+(i+1)+'行: 该数据项代码已经存在!');
					return false;
				}	
			}	
			
			return true;
		}		

		function __deletedRowsExitThisRowByHashKey(rowHashKey){
			for(var n = 0; n < $scope.config.deletedRows; n++){
				if(rowHashKey == $scope.config.deletedRows[n].$$hashKey){
					return true;
				}
			}
			return false;
		}

        function __onRegisterApi(gridApi){ 
			$scope.config.gridApi = gridApi; 

			// 分页相关 
            $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                console.log("刷新界面 第 "+currentPage+" 页,每页 "+pageSize+"个元素!");
                __requestTableDatas(currentPage, pageSize);
                return true;
            }, $scope); 
			
			gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
				if(newRowCol.col.field == "isDeleted" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
					__checkCanDeleteData(newRowCol);
				}
			});

			$scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
					var promise = $q.defer();
					if($scope.config.gridApi.rowEdit){
						$scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
					}
					
					if(oldValue == newValue || colDef.field == 'isSelected'){
						promise.resolve();
						return;
					}
					rowEntity.hasChanged = true; 
					$scope.bodyConfig.overallWatchChanged = true;	      		 
					if(colDef.field === "plcObject" || colDef.name === "plcObject"){
						if(UtilsService.isEmptyString(rowEntity.plcObject)){
							return;
						}
						rowEntity.plcObject = rowEntity.plcObject.toUpperCase();
						if(UtilsService.isContainZhcn(rowEntity.plcObject)){
							promise.reject();
							$scope.addAlert('', '数据项代码不能为汉字!');
							uiGridValidateService.setInvalid(rowEntity, colDef);
							rowEntity.plcObjectZhcnIsValid = false;
							return;
						} 
						if(rowEntity.plcObject.length != 4){
							promise.reject();
							$scope.addAlert('', '数据项代码必须是4位!');
							uiGridValidateService.setInvalid(rowEntity, colDef);
							rowEntity.plcObjectCountIsValid = false;
							return;
						}
						rowEntity.plcObjectZhcnIsValid = true;
                    	rowEntity.plcObjectCountIsValid = true;
						plcService 
				  			.plcDataItemExist(rowEntity.plcObject) 
				  			.then(function (resultDatas){
								if(resultDatas.response){ //已经存在:true
									$scope.addAlert('danger', '该数据项代码已经存在!');
									uiGridValidateService.setInvalid(rowEntity, colDef);
									rowEntity.plcObjectIsValid = false;
									promise.reject();
								}else{	//不存在冲突，可以使用
									console.log("不存在冲突，可以使用");
									promise.resolve();
									rowEntity.hasChanged = true;
									rowEntity.plcObjectIsValid = true;
									uiGridValidateService.setValid(rowEntity, colDef);
								}
							},function (resultDatas){ 
								$scope.addAlert(resultDatas.myHttpConfig.statusDesc);
							});
					}else{
						promise.resolve();
					}
			});
			$scope.config.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
				$scope.config.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
			});
			//选择相关
			$scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
				gridRow.entity.isSelected = gridRow.isSelected;
				var count = $scope.config.gridApi.selection.getSelectedCount();
				$scope.config.btnDisabledDelete = count > 0 ? false : true;
			});
			$scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
				var count = $scope.config.gridApi.selection.getSelectedCount();
				$scope.config.btnDisabledDelete = count > 0 ? false : true;
				for(var i = 0; i < gridRows.length; i++){
					gridRows[i].entity.isSelected = gridRows[i].isSelected;
				}
			});
		}

		function __requestTableDatas(pageIndex, pageCount){
			var pageIndexX = 1;
            var pageCountX = $scope.plcDataItemGridOptions.paginationPageSize;
            if(angular.isUndefined(pageIndex) || pageIndex == null | pageIndex<=1){
                pageIndexX = 1;
            }else{
                pageIndexX = pageIndex;
            }
            if(angular.isUndefined(pageCount) || pageCount == null | pageCount<1){
                pageCountX = $scope.plcDataItemGridOptions.paginationPageSize;
            }else{
                pageCountX = pageCount;
            }

			var ct = $scope.config.selectedPlcCategory;
			if(angular.isUndefined(ct) || ct == null || angular.isUndefined(ct.category) || ct.category==null){
				$scope.addAlert("请先选择 PLC 数据类型!");
			 	return;
			}
			var plc_category;
			plc_category = ct.category;
			plcService
				.plcDataItemList(plc_category, pageIndexX, pageCountX)
				.then(function (resultDatas){
					$scope.plcDataItemGridOptions.paginationCurrentPage = pageIndexX;
					$scope.config.btnDisabledSave = false;
					if(resultDatas.response && resultDatas.response.length > 0){
						for(var i = 0; i < resultDatas.response.length; i++){
							resultDatas.response[i].isSelected = false;
							resultDatas.response[i].plcCategoryDescription = $scope.config.selectedPlcCategory.description;
						}
						$scope.plcDataItemGridOptions.data = resultDatas.response;
						return;
					}else{
						$scope.plcDataItemGridOptions.data = [];
						$scope.addAlert('', '暂无数据!');
					}
				}, function (resultDatas){
					$scope.plcDataItemGridOptions.paginationCurrentPage = pageIndexX;
					$scope.config.btnDisabledSave = true;
					$scope.plcDataItemGridOptions.data = [];
					$scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
				});
		}
		function __requestTableDatasCount(){
			var ct = $scope.config.selectedPlcCategory; 
			if(angular.isUndefined(ct) || ct == null || angular.isUndefined(ct.category) || ct.category==null){
				$scope.addAlert("请先选择 PLC 数据类型!"); 
			 	return; 
			}
			var plc_category;
			plc_category = ct.category;
			plcService
				.plcDataItemCount(plc_category)
				.then(function (resultDatas){					
					$scope.bodyConfig.overallWatchChanged = false;
					$scope.plcDataItemGridOptions.totalItems = resultDatas.response;
				}, function(resultDatas){
					$scope.plcDataItemGridOptions.totalItems = [];
					$scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
				});
		}

		/**
		 * 
		 * [__requestSelectListDatas 查询数据类型下拉列表数据]
		 * 
		 */
		function __requestSelectListDatas(){ 
			plcService
				.requestPlcCategoryList()
				.then(function (resultDatas){
					if(resultDatas.response && resultDatas.response.length > 0){
						$scope.config.plcCategories = resultDatas.response
						for(var i = 0; i < $scope.config.plcCategories.length; i++){
							$scope.config.plcCategories[i].descriptionNew = "代码:"+$scope.config.plcCategories[i].category+" 描述:"+$scope.config.plcCategories[i].description;
						}
						//if($scope.config.isFirstAddClass)
						//{
						//	$timeout(function(){
						//		/* To hide the blank gap when use selecting and grouping */
						//		$('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
						//		$('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
						//		$scope.config.isFirstAddClass=false;
						//	},0);
						//}
						return;
					}
				}, function (resultDatas){
					$scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
				});
		}

		$scope.init();

		/**
		 * 
		 * [__checkCanDeleteData 检验该行是否可以被删除]
		 * @param  {[string]} newRowCol [表格行对象]
		 * @return {[boolean]}          [该行是否可以被删除]
		 * 
		 */
		function __checkCanDeleteData(newRowCol){
			var entity = newRowCol.row.entity;
			var handle = entity.handle;
			var hashKey = entity.$$hashKey;
			if(angular.isUndefined(handle) || handle==null || handle==""){ // 处理新增行 
				__deleteRowDataByHashKey(hashKey);
				return;
			}
			// 处理历史行，需要检验是否已经被引用
			plcService
				.plcDataItemIsUsed(entity.plcObject)
				.then(function (resultDatas){
					if(resultDatas.response === true){ // 不允许删除
						entity.canDeleted = false;
						$scope.addAlert('danger', entity.plcObject+" 已被引用,不允许删除!");
					}else{
						entity.canDeleted = true;
						// row.canDeleted = true;
						// __chekForDeleteRows();
						__deleteRowDataByHashKey(hashKey);
					}
					
				}, function (errorResultDatas){
					$scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
				});
		}

		/**
		 * 
		 * [__deleteRowDataByHashKey 依据hashkey来删除对应的表格行数据]
		 * @param  {[string]} hashKey [hashKey]
		 * 
		 */
		function __deleteRowDataByHashKey(hashKey){
			for(var i = 0; i < $scope.plcDataItemGridOptions.data.length; i++){
				if(hashKey == $scope.plcDataItemGridOptions.data[i].$$hashKey){
					$scope.bodyConfig.overallWatchChanged = true;
					$scope.config.deletedRows.push($scope.plcDataItemGridOptions.data[i]);
					$scope.plcDataItemGridOptions.data = UtilsService.removeArrayByIndex($scope.plcDataItemGridOptions.data, i);
					break; 
				}
			}
		}

    }]);