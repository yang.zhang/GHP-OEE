/**
 * 
 * [PLC模块 设备上位机网络信息维护界面对应Controller: networkCtrl]
 * 
 */
homeModule.controller('networkCtrl',['$scope', '$http',
    'HttpAppService', '$timeout', 'plcService', '$state',
    'uiGridConstants', '$q', 'UtilsService', 'uiGridValidateService','$uibModal',
    function ($scope, $http, HttpAppService, $timeout, plcService, $state,
              uiGridConstants, $q, UtilsService, uiGridValidateService,$uibModal) {
        
        $scope.config = {
            gridApi: null,
            deletedRows: [],
            // $scope.config.$workCenterSelect
            // $scope.config.$lineSelect
            $workCenterSelect: null,
            $lineSelect: null,
            $deviceTypeSelect: null,
            $deviceNumSelect: null,

            btnDisabledQuery: true,
            btnDisabledDelete: true,
            btnDisabledAdd: true,
            btnDisabledSave: true,
            
            defaultSelectItems: [
                { descriptionNew:"", description:"", workCenter: null,  resourceType: null, },
                { descriptionNew:"--- 请选择 ---", description:"--- 请选择 ---", workCenter: null,  resourceType: null}
            ],
            defaultSelectItem: { descriptionNew:"--- 请选择 ---", description:"--- 请选择 ---", workCenter: null },
            workCenters: [],
            currentWorkCenter: null,
            lines: [],
            currentLine: null,
            deviceTypes: [],
            currentDeviceType: null,
            deviceNums: [],
            currentDeviceNums: [],
            currentDeviceNumsShow : null,

            plcProtocols: [],
            showDeviceCode : true
        };
        //设备帮助
        $scope.toDeviceHelp = function(){
            if($scope.config.currentWorkCenter.workCenter == null){
                $scope.addAlert('','请先选择生产区域');
                return;
            }
            if($scope.config.currentLine.workCenter == null){
                $scope.addAlert('','请先选择拉线');
                return;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/noReportModalResourceHelp.html',
                controller: 'noReportDeviceMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectMoreDevice: function () {
                        return {deviceCode : $scope.config.deviceNums};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.currentDeviceNumsShow = selectedItem.area.join(",");
                $scope.config.currentDeviceNums = selectedItem.selectdDatas;
                if($scope.config.currentDeviceNumsShow.length >0){
                    $scope.config.showDeviceCode = false;
                }
            }, function () {
            });
        };
        $scope.deleteAll = function(item){
            if(item == 'deviceCode'){
                $scope.config.currentDeviceNumsShow = '';
                $scope.config.currentDeviceNums = [];
                $scope.config.showDeviceCode = true;
                return;
            }
        }
        $scope.upModel = function(){
            $scope.config.currentDeviceNumsShow = angular.uppercase($scope.config.currentDeviceNumsShow);
        }
        $scope.netInfoGridOptions = { 
            enablePagination: true,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,
            paginationCurrentPage: 1,
            // paginationTemplate: 'ui-grid/pagination',
            totalItems: 0, 

            useExternalPagination: true,

            showGridFooter: false,
            modifierKeysToMultiSelectCells: false,
            data: [],
            columnDefs: [
                {
                    field: 'handle', name: 'handle', visible: false
                },
                { 
                    field: 'resourcePO.resrce', name: 'resourcePOResrce', displayName: '设备编码', visible: true, minWidth: 100,
                    enableCellEdit: false,
                    enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }, 
                    //cellEditableCondition: function($scope){
                    //    var row = arguments[0].row;
                    //    var col = arguments[0].col;
                    //    // 非新增不可编辑 、新增行可编辑
                    //    if(!angular.isUndefined(row.entity) && row.entity!=null && !angular.isUndefined(row.entity.handle) && row.entity.handle!=null && row.entity.handle!=""){
                    //     return false;
                    //    }else{
                    //     return true;
                    //    }
                    //}
                },
                {
                    field: 'resourcePO.description', name: 'resourcePODescription', displayName: '设备名称', visible: true, minWidth: 150,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }, 
                    //cellEditableCondition: function($scope){
                    //    var row = arguments[0].row;
                    //    var col = arguments[0].col;
                    //    // 非新增不可编辑 、新增行可编辑
                    //    if(!angular.isUndefined(row.entity) && row.entity!=null && !angular.isUndefined(row.entity.handle) && row.entity.handle!=null && row.entity.handle!=""){
                    //     return false;
                    //    }else{
                    //     return true;
                    //    }
                    //}
                },
                {
                    field: 'plcIp', name: 'plcIp', displayName: 'PLC IP地址', visible: true, minWidth: 100,
                    enableCellEdit: $scope.networkConfig.canEdit, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },
                    cellTemplate: 'andon-ui-grid-tpls/network-plcIp',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'plcPort', name: 'plcPort', displayName: '端口号', visible: true, minWidth: 100,
                    enableCellEdit: $scope.networkConfig.canEdit, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },cellClass:'text-align-right',
                    cellTemplate: 'andon-ui-grid-tpls/network-plcPort',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'plcAccessTypeDescription', name: 'plcAccessTypeDescription', displayName: '设备PLC协议类型', visible: true, minWidth: 150,
                    enableCellEdit: $scope.networkConfig.canEdit, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownIdLabel: 'description', editDropdownValueLabel: 'plcAccessTypeDesc',
                    // $scope.config.plcProtocols
                    editDropdownOptionsFunction: function(rowEntity, colDef){
                        return $scope.config.plcProtocols;
                    }
                },
                {
                    field: 'pcIp', name: 'pcIp', displayName: '上位机IP地址', visible: true, minWidth: 100,
                    enableCellEdit: $scope.networkConfig.canEdit, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },
                    cellTemplate: 'andon-ui-grid-tpls/network-pcIp',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {   
                    field: 'isDeleted', name: 'isDeleted', displayName: '删除',
                    visible:true, width: 60,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    visible: $scope.networkConfig.canEdit,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return "删除该行"; }, 
                    cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col"><span class="ion-trash-a"></span></div>',
                    headerTemplate: '<div class="xbr-delete-col-header">删除</div>'
                }
            ],
            onRegisterApi: __onRegisterApi
        };

        function __getAccessTypeByDesc(desc){
            if(UtilsService.isEmptyString(desc)){
                return null;
            }
            for(var i = 0; i < $scope.config.plcProtocols.length; i++){
                var obj = $scope.config.plcProtocols[i];
                if(obj.description == desc){
                    return obj.plcAccessType;
                }
            }
        }

        $scope.addRows = function(){ 
            if(angular.isUndefined($scope.netInfoGridOptions.data) || $scope.netInfoGridOptions.data == null){
                $scope.netInfoGridOptions.data = [];
            }   
            $scope.bodyConfig.overallWatchChanged = true;
            $scope.netInfoGridOptions.data.push({
                hasChanged: true
            }); 
        };  
        $scope.queryDatas = function(){
            if($scope.bodyConfig.overallWatchChanged){
                var promise =UtilsService.confirm('还有修改未保存，是否继续', '提示', ['确定', '取消']);
                promise.then(function(){
                    $scope.netInfoGridOptions.paginationCurrentPage = 1;
                    $scope.netInfoGridOptions.totalItems = 0;
                    __requestAddressNetworkCount();
                    __requestPlcProtocols();
                    __requestTableDatas();
                }, function(){
                    //取消停留在原页面
                    $scope.bodyConfig.overallWatchChanged = true;
                });
            }else{
                $scope.netInfoGridOptions.paginationCurrentPage = 1;
                $scope.netInfoGridOptions.totalItems = 0;
                __requestAddressNetworkCount();
                __requestPlcProtocols();
                __requestTableDatas();
            }

        }; 
        $scope.checkAndDeleteSelectedRows = function(){ 
            var deletedRowIndexs = [];
            var rows = $scope.config.gridApi.selection.getSelectedRows();
            for(var i = 0; i < rows.length; i++){
                var row = rows[i];
                if( !angular.isUndefined(row.handle) && row.handle!=null && row.handle!=""){
                    row.canDeleted = true;
                    row.isSelected = true;
                }else{
                    row.canDeleted = true;
                    row.isSelected = true;
                    // $scope.netInfoGridOptions.data[i].canDeleted = true;
                    // $scope.netInfoGridOptions.data[i].isSelected = true;
                }
            }
            __chekForDeleteRows(); 
        };  
        function __chekForDeleteRows(){ 
            var canDeletes = true;
            var deletedRowIndexs = [];
            var rows = $scope.netInfoGridOptions.data;
            for(var i = 0; i < rows.length; i++){
                var row = rows[i];
                if(row.isSelected){
                    if(row.canDeleted !== true){
                        canDeletes = false;
                    }
                }
            }
            if(canDeletes){
                for (var i = rows.length - 1; i >= 0; i--) {
                    var row = rows[i];
                    if(row.isSelected){
                        row.isDeleted = true;
                        deletedRowIndexs.push(i);
                        $scope.config.deletedRows.push(angular.copy(row));
                    }
                };
            }else{
                return;
            }
            var length = deletedRowIndexs.length;
            for(var i = 0; i < length; i++){
                $scope.bodyConfig.overallWatchChanged = true;
                $scope.netInfoGridOptions.data.splice(deletedRowIndexs[i], 1);
                // $scope.netInfoGridOptions.data = UtilsService.removeArrayByIndex($scope.netInfoGridOptions.data, deletedRowIndexs.pop());
            } 
        }; 

        $scope.saveAllAcitons = function(){ 
            if(!__checkCanSaveDatas()){
                return;
            };  
            var paramActions = []; 
            for(var i = 0; i < $scope.netInfoGridOptions.data.length; i++){
                var row = $scope.netInfoGridOptions.data[i]; 
                if(row.hasChanged){
                    // var viewActionCode = angular.isUndefined(row.handle) ? 'C' : 'U';
                    if(angular.isUndefined(row.handle)){ //创建新数据 ，若是删除中存在该条数据，则直接忽略该行,并在后面将删除操作也忽略
                        if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
                            var paramAction = angular.copy(row);
                            paramAction.viewActionCode = 'C';   
                            paramAction.site = HttpAppService.getSite();
                            paramAction.resourceBo = paramAction.resourcePO.handle;
                            paramAction.resourceType = row.accessType;
                            paramActions.push(paramAction);
                        }else{}  
                    }else{ //修改已存于服务器的行数据 ，若删除列表中有该行，则直接忽略该修改提交，并在后面将删除操作添加进来
                        if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
                            var paramAction = angular.copy(row);
                            paramAction.viewActionCode = 'U';
                            paramActions.push(paramAction);
                        }else{}
                    }   
                }   
            }   
            for(var i = 0; i < $scope.config.deletedRows.length; i++){ 
                var deleteTempRow = $scope.config.deletedRows[i];
                if(!angular.isUndefined(deleteTempRow.handle)){ // 服务器端数据的删除操作,直接添加进去即可
                    // var paramAction = angular.copy(row);
                    // paramAction.viewActionCode = 'D';
                    // paramActions.push(paramAction);
                    paramActions.push({
                        // "handle": deleteTempRow.resourcePO.handle,
                        "handle": deleteTempRow.handle,
                        "resrce": deleteTempRow.resrce,
                        "resourceName": deleteTempRow.resourceName, 
                        "ipAddress": deleteTempRow.ipAddress,  
                        "port": deleteTempRow.port,
                        "accessType": deleteTempRow.accessType,
                        "viewActionCode": 'D',
                        "modifiedDateTime": deleteTempRow.modifiedDateTime
                    });
                }else{
                    paramActions.push({
                        "handle": deleteTempRow.resourcePO.handle,
                        // "handle": deleteTempRow.handle,
                        "resrce": deleteTempRow.resrce,
                        "resourceName": deleteTempRow.resourceName, 
                        "ipAddress": deleteTempRow.ipAddress,  
                        "port": deleteTempRow.port,
                        "accessType": deleteTempRow.accessType,
                        "viewActionCode": 'D',
                        "modifiedDateTime": deleteTempRow.modifiedDateTime
                    });
                } 
            }   
            if(paramActions.length > 0){
                $scope.showBodyModel("正在保存");
                plcService 
                    .plcNetInfoChange(paramActions)
                    .then(function (resultDatas){ 
                        $scope.bodyConfig.overallWatchChanged = false;
                        $scope.config.deletedRows = [];
                        $scope.hideBodyModel();
                        $scope.addAlert('success', '保存成功!');
                        $scope.queryDatas();
                    },function (resultDatas){ 
                        $scope.bodyConfig.overallWatchChanged = false;
                        $scope.hideBodyModel();
                        $scope.addAlert('danger', '保存失败!');
                    });
            }else{ 
                $scope.addAlert('','没有修改,无需保存!');
            }
        };
        function __deletedRowsExitThisRowByHashKey(rowHashKey){
            for(var n = 0; n < $scope.config.deletedRows; n++){
                if(rowHashKey == $scope.config.deletedRows[n].$$hashKey){
                    return true;
                }
            }
            return false;
        }

        function __onRegisterApi(gridApi){
            $scope.config.gridApi = gridApi;

            // 分页相关
            $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                // console.log("刷新界面 第 "+currentPage+" 页,每页 "+pageSize+"个元素!");
                $scope.bodyConfig.overallWatchChanged = true;
                __requestTableDatas(currentPage, pageSize);
                return true;
            }, $scope);

            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
                if(newRowCol.col.field == "isDeleted" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                    __checkCanDeleteData(newRowCol);
                }
            });

            $scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                    var promise = $q.defer();
                    if($scope.config.gridApi.rowEdit){
                        $scope.config.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
                    }   
                    if(oldValue == newValue){
                        promise.resolve();
                        return;
                    }   
                    rowEntity.hasChanged = true;
                    $scope.bodyConfig.overallWatchChanged = true;
                    if(colDef.field === "plcIp"){
                        if(__checkIP(newValue)){
                            rowEntity.PlcIpIsInValid = false;

                        }else{
                            rowEntity.PlcIpIsInValid = true;
                        };
                        return;
                    };
                    if(colDef.field==="pcIp"){
                        if(__checkIP(newValue)){
                            rowEntity.PcIpIsInValid = false;
                        }else{
                            rowEntity.PcIpIsInValid = true;
                            return;
                        };

                        if(!UtilsService.isEmptyString(newValue)){
                            plcService
                                .plcNetInfoExistPcIp(newValue)
                                .then(function(resultDatas){
                                    if(resultDatas.response){
                                        $scope.addAlert('danger','上位机IP地址：'+newValue+"已存在");
                                        rowEntity.PcIpIsExist = true;
                                        uiGridValidateService.setInvalid(rowEntity, colDef);
                                    }else{
                                        rowEntity.PcIpIsExist = false;
                                        uiGridValidateService.setValid(rowEntity, colDef);
                                    }
                                },function (resultDatas){ //TODO 检验失败
                                    $scope.addAlert('danger',resultDatas.myHttpConfig.statusDesc);
                                });
                        }else{
                            rowEntity.PcIpIsInValid = false;
                            rowEntity.PcIpIsExist = false;
                        }


                    };
                    if(colDef.field === "plcPort"){
                        var rePort = new RegExp(/^[0-9]{1,10}$/);
                        if(UtilsService.isEmptyString(newValue) || rePort.test(newValue)){
                            rowEntity.PlcPortIsInValid = false;
                            uiGridValidateService.setValid(rowEntity, colDef);
                        }else{
                            rowEntity.PlcPortIsInValid = true;
                            uiGridValidateService.setInvalid(rowEntity, colDef);
                        }
                        return;
                    }
                    if(colDef.field === "plcAccessTypeDescription"){
                        rowEntity.plcAccessType = __getAccessTypeByDesc(newValue);
                    }
                    return;

                    if(colDef.field === "resrce" || colDef.name === "resrce"){
                        promise.reject();
                    }else{
                        promise.resolve();
                    }
            });     
            $scope.config.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
                $scope.config.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
            }); 
            //选择相关 
            $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
                var count = $scope.config.gridApi.selection.getSelectedCount();
                $scope.config.btnDisabledDelete = count > 0 ? false : true;
                gridRow.entity.isSelected = gridRow.isSelected;
            }); 
            $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                var count = $scope.config.gridApi.selection.getSelectedCount();
                $scope.config.btnDisabledDelete = count > 0 ? false : true;
                for(var i = 0; i < gridRows.length; i++){
                    gridRows[i].entity.isSelected = gridRows[i].isSelected;
                }
            }); 
        };
        function __checkIP(IP){
            var ipRegexp = /(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)\.(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)\.(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)\.(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)/g;

            var matches = IP.match(ipRegexp);
            if(UtilsService.isEmptyString(IP) || !angular.isUndefined(matches) && matches!=null && matches.length==1 && matches[0]==IP){
                return true;
            }else{
                return false;
            }
        };
        function __checkCanDeleteData(newRowCol){
            $scope.bodyConfig.overallWatchChanged = true;
            var entity = newRowCol.row.entity;
            var handle = entity.handle;
            var hashKey = entity.$$hashKey;
            if(angular.isUndefined(handle) || handle==null || handle==""){ // 处理新增行 
                __deleteRowDataByHashKey(hashKey);
                return;
            }
             __deleteRowDataByHashKey(hashKey);
        }


        $scope.init = function(){
            __initDropdowns('init');
            __requestDropdownWorkCenters();
            __requestDropdownDeviceTypes();
        };
        $scope.init();

        function __initDropdowns(type){
            // $scope.config.workCenters = [];
            // $scope.config.workCenters.push($scope.config.defaultSelectItem);
            // $scope.config.currentWorkCenter = $scope.config.workCenters[0];
            if(type == 'init'){
                $scope.config.workCenters = [];
                $scope.config.workCenters.push($scope.config.defaultSelectItem);
                $scope.config.currentWorkCenter = $scope.config.workCenters[0];

                //$scope.config.deviceTypes = angular.copy($scope.config.defaultSelectItems);
                // $scope.config.deviceTypes.push($scope.config.defaultSelectItem);
                // $scope.config.deviceTypes.push(angular.copy($scope.config.defaultSelectItem));
                $scope.config.deviceTypes = [];
                $scope.config.currentDeviceType = null;
                //$scope.config.currentDeviceType = $scope.config.deviceTypes[0];
            }

            if(type == 'init' || type == 'workCenterChanged'){
                $scope.config.lines = [];
                $scope.config.lines.push($scope.config.defaultSelectItem);
                $scope.config.currentLine = $scope.config.lines[0];

                $scope.config.deviceNums = [];//$scope.config.defaultSelectItems;;
                // $scope.config.deviceNums.push($scope.config.defaultSelectItem);
                $scope.config.currentDeviceNum = $scope.config.deviceNums[0]; 
                $scope.config.currentDeviceNums = [];
                $scope.config.currentDeviceNumsShow = [];
                $scope.config.showDeviceCode = true;
            }

            if(type == 'lineChanged'){
                $scope.config.deviceNums = [];//$scope.config.defaultSelectItems;;
                // $scope.config.deviceNums.push($scope.config.defaultSelectItem);
                $scope.config.currentDeviceNum = $scope.config.deviceNums[0]; 
                $scope.config.currentDeviceNums = [];
                $scope.config.currentDeviceNumsShow = [];
                $scope.config.showDeviceCode = true;
            }

            if(type == 'deviceTypeChanged'){
                $scope.config.deviceNums = [];//$scope.config.defaultSelectItems;;
                // $scope.config.deviceNums.push($scope.config.defaultSelectItem);
                $scope.config.currentDeviceNum = $scope.config.deviceNums[0]; 
                $scope.config.currentDeviceNums = [];
                $scope.config.currentDeviceNumsShow = [];
                $scope.config.showDeviceCode = true;
            }

        }

        function __requestTableDatas(pageIndex, pageCount){
            var pageIndexX = 1;
            var pageCountX = $scope.netInfoGridOptions.paginationPageSize;
            if(angular.isUndefined(pageIndex) || pageIndex == null | pageIndex<=1){
                pageIndexX = 1;
            }else{
                pageIndexX = pageIndex;
            }
            if(angular.isUndefined(pageCount) || pageCount == null | pageCount<1){
                pageCountX = $scope.netInfoGridOptions.paginationPageSize;
            }else{
                pageCountX = pageCount;
            }

            var resourceS = [];
            for(var i = 0; i < $scope.config.currentDeviceNums.length; i++){
                resourceS.push($scope.config.currentDeviceNums[i].resrce);
            }
            
            var params = {
                "site" : HttpAppService.getSite(),
                "pageIndex" : pageIndexX,
                "pageCount" : pageCountX,
                "resource" : resourceS,
                "resourceType" : $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:'',
                "workCenter" : $scope.config.currentWorkCenter.workCenter,
                "line" : $scope.config.currentLine.workCenter,
            };
            
            plcService
                    .plcNetInfoList(params)  
                    .then(function (resultDatas){ 
                        $scope.config.btnDisabledSave = false;
                        $scope.bodyConfig.overallWatchChanged = false;
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0; i < resultDatas.response.length; i++){
                                var po = resultDatas.response[i].plcAccessTypePO;
                                if(!angular.isUndefined(po) && po!=null){
                                    resultDatas.response[i].plcAccessTypeDescription = po.description;
                                }
                            }
                            $scope.netInfoGridOptions.data = resultDatas.response;
                            return;
                        }else{
                            $scope.netInfoGridOptions.data = [];
                            // $scope.bodyConfig.overallWatchChanged = false;
                            $scope.addAlert('', '暂无数据!');
                        }
                    }, function (errorResultDatas){
                        $scope.config.btnDisabledSave = true;
                        $scope.netInfoGridOptions.data = [];
                        $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
                    });
        }
        
        function __requestPlcProtocols(){
            plcService
                .plcProtocolTypeList()
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].plcAccessTypeDesc = "代码: "+resultDatas.response[i].plcAccessType+"  描述: "+resultDatas.response[i].description;
                        }
                        $scope.config.plcProtocols = resultDatas.response;
                        return;
                    }
                }, function (resultDatas){
                    $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
                });
        }

        function __requestAddressNetworkCount(){
            var params = {};
            // var resource = $scope.config.currentDeviceNum.resrce;
            var resourceS = [];
            for(var i = 0; i < $scope.config.currentDeviceNums.length; i++){
                resourceS.push($scope.config.currentDeviceNums[i].resrce);
            }
            var resourceType = $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:'';
            var workCenter = $scope.config.currentWorkCenter.workCenter;
            var line = $scope.config.currentLine.workCenter;
            params.workCenter = workCenter;
            params.line = line;
            if(resourceS.length>0){
                params.resourceS = resourceS;
            }
            if(resourceType){
                params.resourceType = resourceType;
            }

            plcService
                .plcNetInfoCount(params)
                .then(function (resultDatas){
                    $scope.netInfoGridOptions.totalItems = resultDatas.response;
                    console.log(resultDatas.response);
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc); 
                }); 
        }

        function __checkCanSaveDatas(){ 
            for(var i = 0; i < $scope.netInfoGridOptions.data.length; i++){ 
                var item = $scope.netInfoGridOptions.data[i]; 
                if(item.hasChanged){ 
                    if(angular.isUndefined(item['resourcePO']) || angular.isUndefined(item['resourcePO']['resrce']) || item['resourcePO']['resrce'] == null || item['resourcePO']['resrce'] == ""){ 
                        $scope.addAlert('', '第'+(i+1)+'行: 设备编码不能为空!'); 
                        return false; 
                    }   
                    if(angular.isUndefined(item['resourcePO']) || angular.isUndefined(item['resourcePO']['description']) || item['resourcePO']['description'] == null || item['resourcePO']['description'] == ""){ 
                        $scope.addAlert('', '第'+(i+1)+'行: 设备名称不能为空!'); 
                        return false; 
                    }   
                    if(!angular.isUndefined(item.PlcIpIsInValid) && item.PlcIpIsInValid==true){
                        $scope.addAlert('', '第'+(i+1)+'行: PLC IP地址不合法');
                        return false;
                    }
                    if(!angular.isUndefined(item.PcIpIsInValid) && item.PcIpIsInValid==true){
                        $scope.addAlert('', '第'+(i+1)+'行: 上位机IP地址不合法!');
                        return false;
                    }
                    if(!angular.isUndefined(item.PcIpIsExist) && item.PcIpIsExist==true){
                        $scope.addAlert('', '第'+(i+1)+'行: 上位机IP地址已存在!');
                        return false;
                    }
                    if(!angular.isUndefined(item.PlcPortIsInValid) && item.PlcPortIsInValid==true){
                        $scope.addAlert('', '第'+(i+1)+'行: 端口号不合法!');   
                        return false;
                    }
                } 
            } 
            return true; 
        } 

        function __deleteRowDataByHashKey(hashKey){
            for(var i = 0; i < $scope.netInfoGridOptions.data.length; i++){
                if(hashKey == $scope.netInfoGridOptions.data[i].$$hashKey){
                    $scope.config.deletedRows.push($scope.netInfoGridOptions.data[i]);
                    $scope.netInfoGridOptions.data = UtilsService.removeArrayByIndex($scope.netInfoGridOptions.data, i);
                    break; 
                }
            }
        }

        $scope.tipSelectClick = function(type){ 
            if(type == 'line'){
                $scope.config.$workCenterSelect.toggle();
            }else if(type == 'deviceNum'){
                $scope.config.$workCenterSelect.toggle();
            }
        }

        /////   生成区域、级联选择
        $scope.workCenterChanged = function(){
            if($scope.config.currentWorkCenter == null || $scope.config.currentWorkCenter.workCenter==null){  
                __initDropdowns('workCenterChanged'); 
            }else{
                __initDropdowns('workCenterChanged');
                var workCenter = $scope.config.currentWorkCenter.workCenter;
                __requestDropdownLines(workCenter);
                var line = $scope.config.currentLine.workCenter;
                if($scope.config.currentLine != null && $scope.config.currentLine.workCenter!=null){  
                    var resourceType = $scope.config.currentDeviceType.resourceType;
                    __requestDropdownDeviceCodes(workCenter, line, resourceType);
                } 
            } 
            __setBtnsDisable(); 
        };
        function __setBtnsDisable(){ 
            if($scope.config.currentWorkCenter.workCenter == null
                // || $scope.config.currentDeviceType.resourceType == null
                || $scope.config.currentLine.workCenter == null
                // || $scope.config.currentDeviceNum.resrce == null
                ){
                $scope.config.btnDisabledAdd = true;
                $scope.config.btnDisabledQuery = true;
                $scope.config.btnDisabledDelete = true;
                return;
            }
            $scope.config.btnDisabledAdd = false;
            $scope.config.btnDisabledQuery = false;
            // $scope.config.btnDisabledDelete = false;
        } 
        $scope.lineChanged = function(){
            if($scope.config.currentLine == null || $scope.config.currentLine.workCenter==null){
                __initDropdowns('lineChanged');
            }else{
                __initDropdowns('lineChanged');
                var workCenter = $scope.config.currentWorkCenter.workCenter;
                var line = $scope.config.currentLine.workCenter;
                var resourceType = $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:'';

                __requestDropdownDeviceCodes(workCenter, line, resourceType);
                // if($scope.config.currentDeviceType != null && $scope.config.currentDeviceType.resourceType!=null){
                //     $scope.config.btnDisabledAdd = false;
                //     $scope.config.btnDisabledQuery = false;
                // }
            }
            __setBtnsDisable();
        }; 
        $scope.deviceTypeChanged = function(){
            if($scope.config.currentDeviceType == null || $scope.config.currentDeviceType.resourceType==null){
                __initDropdowns('deviceTypeChanged');
                var workCenter = $scope.config.currentWorkCenter.workCenter;
                var line = $scope.config.currentLine.workCenter;
                __requestDropdownDeviceCodes(workCenter, line, null);
            }else{
                __initDropdowns('deviceTypeChanged');
                var workCenter = $scope.config.currentWorkCenter.workCenter;
                var line = $scope.config.currentLine.workCenter;
                var resourceType = $scope.config.currentDeviceType.resourceType;

                __requestDropdownDeviceCodes(workCenter, line, resourceType);
                // if($scope.config.currentLine != null && $scope.config.currentLine.workCenter!=null){
                //     $scope.config.btnDisabledQuery = false;
                // }
            } 
            __setBtnsDisable();
        }; 
        $scope.deviceNumChanged = function(){  
            // if($scope.config.currentDeviceType == null || $scope.config.currentDeviceType.resourceType==null){
            //     $scope.config.btnDisabledAdd = true;
            // }else{
            //     $scope.config.btnDisabledAdd = false;
            // }
            __setBtnsDisable();
        }; 
        function __requestDropdownWorkCenters(){
            plcService
                    .dataWorkCenters() 
                    .then(function (resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            $scope.config.workCenters = [];
                            $scope.config.workCenters.push($scope.config.defaultSelectItem);
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].workCenter+"   描述:"+resultDatas.response[i].description;
                                $scope.config.workCenters.push(resultDatas.response[i]);
                            }
                            // $scope.config.workCenters = $scope.config.workCenters.concat(resultDatas.response);
                            return; 
                        } 
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    }); 
        }; 
        function __requestDropdownLines(workCenter){
            plcService
                    .dataLines(workCenter) 
                    .then(function (resultDatas){ 
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].workCenter+"   描述:"+resultDatas.response[i].description;
                                $scope.config.lines.push(resultDatas.response[i]);
                            }
                            $scope.config.$lineSelect.toggle();
                            // $scope.config.lines = $scope.config.lines.concat(resultDatas.response);
                            return; 
                        } 
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    }); 
        }; 
        function __requestDropdownDeviceTypes(){ 
            plcService
                    .dataResourceTypes() 
                    .then(function (resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].resourceType+"   描述:"+resultDatas.response[i].description;
                                $scope.config.deviceTypes.push(resultDatas.response[i]);
                            }
                            // $scope.config.deviceTypes = $scope.config.deviceTypes.concat(resultDatas.response);
                            return; 
                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    }); 
        }; 
        function __requestDropdownDeviceCodes(workCenter, line, resourceType){ 
            plcService 
                    .dataPlcResourceCodes(workCenter, line, resourceType) 
                    .then(function (resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].resrce+"   描述:"+resultDatas.response[i].description;
                                $scope.config.deviceNums.push(resultDatas.response[i]);
                            }
                            // $scope.config.deviceNums = $scope.config.deviceNums.concat(resultDatas.response);
                            return; 
                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    }); 
        };


    }])
    .filter('treatyFilter', function() {
        var treatyHash = {
            1: 'http',
            2: 'https'
        };

        return function(input) {
            if (!input){
                return '';
            } else {
                return treatyHash[input];
            }
        };
    })