/**
 * 
 * [PLC模块: 设备PLC地址信息批量维护界面Controller: addressInfoCtrl]
 * 
 */
plcModule.controller('addressInfoCtrl',['$scope', '$http', '$sce',
    'HttpAppService', '$timeout', 'UtilsService', 'uiGridValidateService',
    'uiGridConstants', 'Upload', 
    function ($scope, $http, $sce, HttpAppService, $timeout, UtilsService, uiGridValidateService,
              uiGridConstants, Upload) {

        $scope.addressInfoGrid = {
            paginationPageSizes: [10, 25, 50, 75],
            paginationPageSize:10,
            data: [],
            onRegisterApi: __onRegisterApi,
            columnDefs:[  
                { 
                    field: 'resrce', name:"resrce",displayName:"设备编码",enableCellEdit: false,
                    cellTemplate: 'andon-ui-grid-tpls/addInfo-DeviceNumber', 
                    cellClass:'grid-no-editable',
                    cellTooltip: __cellToolTip
                },
                {   
                    field: 'plcAddress', name:"plcAddress", displayName:"PLC地址", 
                    enableCellEdit: false, cellClass:'grid-no-editable',
                    cellTemplate: 'andon-ui-grid-tpls/addInfo-PLCAddress',
                    cellTooltip: __cellToolTip 
                },
                {   
                    field: 'category', name:"category",displayName:"PLC设备类型",enableCellEdit: false, cellClass:'grid-no-editable',
                    cellTemplate: 'andon-ui-grid-tpls/addInfo-category',
                    cellTooltip: __cellToolTip 
                },
                { 
                    field: 'plcValue', name:"plcValue",displayName:"寄存器值",
                    enableCellEdit: false, cellClass:'grid-no-editable',
                    cellTemplate: 'andon-ui-grid-tpls/addInfo-Regis',
                    cellTooltip: __cellToolTip 
                },
                {   
                    field: 'plcObject', name:"plcObject",displayName:"数据项代码",enableCellEdit: false, cellClass:'grid-no-editable',
                    cellTemplate: 'andon-ui-grid-tpls/addInfo-Object',
                    cellTooltip: __cellToolTip 
                },  
                {   
                    field: 'description', name:"description",
                    displayName:"数据项描述",
                    enableCellEdit: false, cellClass:'grid-no-editable',
                    cellTemplate: 'andon-ui-grid-tpls/addInfo-Description',
                    cellTooltip: __cellToolTip 
                },  
                {   
                    field:'logPath', name:"logPath",displayName:"log路径",
                    enableCellEdit: false, cellClass:'grid-no-editable',
                    cellTemplate: 'andon-ui-grid-tpls/addInfo-LogPath',
                    cellTooltip: __cellToolTip 
                }]
        };
        $scope.addressInfoGrid.onRegisterApi = __onRegisterApi;
        // $scope.addressInfoGridDump.onRegisterApi = __onRegisterApiDump;

        /**
         * 
         * [__cellToolTip 表格提示信息,当前界面共用]
         * 
         */
        function __cellToolTip(row, col){ 
            var str = '';// row.entity[col.colDef.name]; 
            var errors = row.entity[col.colDef.name+"Exceptions"];
            if(!angular.isUndefined(errors) && errors != null && errors.length > 0){
                for(var i = 0; i < errors.length; i++){
                    if(i > 0){
                        str += "<br/>";
                    }
                    str += errors[i];
                }
            }
            return str; 
        }

        $scope.config = { 
            gridApi: null,
            isUploading: false,             // 是否正在上传中
            uploadError: false,             // 是否上传失败
            uploadOk: false,                // 是否上传成功
            showErrorDucpGridDatas: true,   // 是否显示出错表格:用于展示当数据有重复时的重复数据

            selectedFile: null,             // 选中的文件对象

            parseFileOkNum: 0,              // 文件上传并保存成功的条数
            parseFileOk: false,             // 文件是否解析成功

            uploadProgressStr: '',          // 文件上传进度的提示字符串
            progressPercentage: 0           // 文件上传进度百分比
        };  

        function __onRegisterApi(gridApi){
            $scope.config.gridApi = gridApi;
        };
        // function __onRegisterApiDump(gridApi){
        //     $scope.config.gridApiDump = gridApi;  
        // }

        $scope.$watch('config.selectedFile', function(newVal, oldVal){
            if(newVal==null){
                $scope.config.uploadProgressStr = '';
                $scope.config.progressPercentage = 0;
            }else{
                $scope.bodyConfig.overallWatchChanged = true;
            }
        });
        
        // upload on file select or drop
        $scope.upload = function () { 
            if($scope.config.uploadOk){ //重新上传:初始化相关变量
                $scope.repareForUpload();
                return;
            }
            var file = $scope.config.selectedFile;
            $scope.config.isUploading = true;
            $scope.bodyConfig.overallWatchChanged = true;
            $scope.config.uploadOk = false;
            $scope.config.uploadError = false;
            $scope.config.progressPercentage = 0;
            $scope.config.showErrorDucpGridDatas = false;
            var url = HttpAppService.URLS.PLC_FILE_UPLOAD
                            .replace('#{SITE}#', HttpAppService.getSite());
            url = HttpAppService.handleCommenUrl(url);
            Upload.upload({ 
                // url: 'mhp-oee/web/rest/plant/site/1000/plc_category',
                url: url,
                data: {file: file}
            }).then(function (resp) { 
                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                $scope.config.isUploading = false;
                $scope.bodyConfig.overallWatchChanged = false;
                $scope.config.uploadOk = true;
                $scope.config.uploadError = false;
                $scope.config.uploadProgressStr = ''; 
                $scope.config.progressPercentage = 0; 
                // var errors = resp.data;
                // __handleErrors(errors);
                var errors = resp.data.exceptionRowArray;
                if(angular.isUndefined(errors) || errors == null || errors.length == 0){
                    // 文件没有解析错误信息
                    errors = [];
                    __handleErrors(errors);
                    $scope.config.parseFileOk = true;
                    $scope.config.parseFileOkNum = resp.data.successUpload.commitNumber;
                }else{ 
                    $scope.config.parseFileOk = false;
                    __handleErrors(errors);
                }   
            }, function (resp) {    
                console.log('Error status: ' + resp.status);
                console.log(resp);
                $scope.config.showErrorDucpGridDatas = false;
                $scope.config.isUploading = false;
                $scope.bodyConfig.overallWatchChanged = false;
                $scope.config.uploadOk = false;
                $scope.config.uploadError = true;
                $scope.config.uploadProgressStr = '';
                $scope.config.progressPercentage = 0;
                if(resp.status == 400){
                    var msg = resp.headers('error-message');
                    if(!UtilsService.isEmptyString(msg)){
                        var msgObj = JSON.parse(msg);
                        var errorJson = msgObj.errorJson;
                        if(!UtilsService.isEmptyString(errorJson)){
                            var errorJsonObj = JSON.parse(errorJson);
                            // errorJsonObj.description = UtilsService.covertUTF8ToZhcn(errorJsonObj.description);
                            errorJsonObj.description = UtilsService.covertUTF8ToZhcn(errorJsonObj.descriptionUnicode);
                            // errorJsonObj.logPath = UtilsService.covertUTF8ToZhcn(errorJsonObj.logPath);
                            errorJsonObj.logPath = UtilsService.covertUTF8ToZhcn(errorJsonObj.logPathUnicode);
                            $scope.addressInfoGrid.data = [errorJsonObj];
                            $scope.config.showErrorDucpGridDatas = true;
                        }   
                    }       
                }else{      
                    $scope.addAlert(resp.statusText);   
                }           
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                $scope.config.uploadProgressStr = progressPercentage + "%";
                $scope.config.progressPercentage = progressPercentage;
            }); 
        }; 

        $scope.drap = function(){
            console.log(arguments);
            $scope.config.isUploading = false;
            $scope.bodyConfig.overallWatchChanged = true;
            $scope.config.uploadError = false;
            $scope.config.uploadOk = false;
        };
        $scope.select = function($file){
            $scope.config.selectedFile = $file;
            $scope.config.isUploading = false;
            $scope.bodyConfig.overallWatchChanged = true;
            $scope.config.uploadError = false;
            $scope.config.uploadOk = false;
        }
        $scope.repareForUpload = function(){ 
            $scope.config.selectedFile = null;
            $scope.config.isUploading = false;
            $scope.bodyConfig.overallWatchChanged = false;
            $scope.config.uploadError = false;
            $scope.config.uploadOk = false;
            $scope.config.uploadProgressStr = '';
            $scope.config.progressPercentage = 0;

            $scope.addressInfoGrid.data = [];
        };  

        function __corvetToChinaese(codeStr){
            if(UtilsService.isEmptyString(codeStr)){
                return codeStr;
            }
            var code = codeStr.substring(codeStr.indexOf("[")+1, codeStr.indexOf("]"));
            var codeDesc = HttpAppService.getDescByExceptionCode(code);
            return codeDesc;
        }

        function __handleErrors(errors){
            var resultDatas = [];
            for(var i = 0; i < errors.length; i++){
                var err = errors[i];
                var data = err.originDataMap; 
                var list = err.exceptionList; 
                var rowNum = err.rowNumber;
                for(var j = 0; j < list.length; j++){ 
                    var exe = list[j]; 
                    for(key in exe){ 
                        data[key+"Exceptions"] = exe[key]; 
                        // 转换 exception 为中文描述
                        for(var xx = 0; xx < data[key+"Exceptions"].length; xx++){
                            data[key+"Exceptions"][xx] = "文件第"+rowNum+"行: "+__corvetToChinaese(data[key+"Exceptions"][xx]);
                        }
                    } 
                } 
                resultDatas.push(data);
            }
            console.log(resultDatas);
            if(resultDatas.length > 0){
                $scope.addressInfoGrid.data = resultDatas;
                $timeout(function(){
                    __setInValidCells();
                }, 500);
            }
        }
        // uiGridValidateService.setValid(rowEntity, colDef);
        // var colDefNamesNeedValid = ['DeviceNumber','Description','Category','Object','PLC Address', 'Regis', 'Log Path'];
        var colDefNamesNeedValid = ['resrce','description','category','plcObject','plcAddress', 'plcValue', 'logPath'];
        function __setInValidCells(){
            var datas = $scope.addressInfoGrid.data;
            for(var i = 0; i < datas.length; i++){
                var rowEntity = datas[i];
                for(var colName in colDefNamesNeedValid){
                    var colNameError = colDefNamesNeedValid[colName]+"Exceptions";
                    if(!angular.isUndefined(rowEntity[colNameError]) && rowEntity[colNameError] != null && rowEntity[colNameError].length > 0){
                        uiGridValidateService.setInvalid(rowEntity,$scope.config.gridApi.grid.getColDef(colDefNamesNeedValid[colName]));
                    }
                }
            }
        }


    }]);













