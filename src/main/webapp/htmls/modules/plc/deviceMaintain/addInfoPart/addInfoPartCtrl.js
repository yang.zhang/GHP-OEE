plcModule.controller('addInfoPartCtrl',['$scope', '$http',
    'HttpAppService', '$timeout', 'plcService', '$state',
    'uiGridConstants', '$q', 'UtilsService', 'uiGridValidateService','$uibModal',
    function ($scope, $http, HttpAppService, $timeout, plcService, $state,
              uiGridConstants, $q, UtilsService, uiGridValidateService,$uibModal) {

        $scope.config = {
            gridApi: null,
            addRowsNum: 1,
            deletedRows: [],

            btnDisabledQuery: true,
            btnDisabledDelete: true,
            btnDisabledAdd: true,
            btnDisabledSave: true,
            inputDisabledNum: true,

            totalCountNum: 0,

            plcDataItems: [],


            tableDatasCache: [],

            defaultSelectItems: [
                { descriptionNew:"", description:"", workCenter: null,  resourceType: null, },
                { descriptionNew:"--- 请选择 ---", description:"--- 请选择 ---", workCenter: null,  resourceType: null}
            ],
            defaultSelectItem: { descriptionNew:"--- 请选择 ---",resrce:"--- 请选择 ---", description:"--- 请选择 ---", workCenter: null ,category:null},
            workCenters: [], 
            currentWorkCenter: null,
            lines: [],
            currentLine: null,
            deviceTypes: [],
            currentDeviceType: null,
            deviceNums: [],
            currentDeviceNum: null,

            plcCategories: [],
            currentPlcCategory: null,
            /*
             * C01 设备状态 C02 原因代码 C03 产量数据 C04 设备是否允许开机
             * C05 报警信息 C06 关键配件寿命 C07 实时PPM C08
             * 三色灯
             */
            plcCategotiesArray:[
                {
                    category:'C01',description:'设备状态'
                },
                {
                    category:'C02',description:'原因代码'
                },
                {
                    category:'C03',description:'产量数据'
                },
                {
                    category:'C04',description:'设备是否允许开机'
                },
                {
                    category:'C05',description:'报警信息'
                },
                {
                    category:'C06',description:'关键配件寿命'
                },
                {
                    category:'C07',description:'实时PPM'
                },
                {
                    category:'C08',description:'三色灯'
                }
            ]
        }; 
        $scope.addressInfoPartGrid = { 
            enablePagination: true, 
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes, 
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize, 
            paginationCurrentPage: 1, 
            // paginationTemplate: 'ui-grid/pagination', 
            totalItems: 0, 

            useExternalPagination: true, 
            showGridFooter: false, 
            modifierKeysToMultiSelectCells: false,
            data: [], 
            //只支持选择单行，不支持选择多行 
            //multiSelect: false,
            onRegisterApi: __onRegisterApi, 
            columnDefs: [
                {   
                    field: 'handle', name: 'handle', visible: false
                },  
                {   
                    field: 'resourcePO.resrce', name: 'resrce', displayName: '设备编码',
                    visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },  
                {   
                    field: 'resourcePO.description', name: 'resrceDes', displayName: '设备名称',
                    visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },  
                {   
                    field: 'categoryDes', name: 'categoryDes', displayName: 'PLC数据类型',
                    visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownIdLabel: 'plcCategoryDes', editDropdownValueLabel: 'plcCategoryDesNew',
                    editDropdownOptionsFunction: function(rowEntity, colDef){
                        var results = [];
                        for(var i = 0; i < $scope.config.plcCategories.length; i++){
                            var item = $scope.config.plcCategories[i];
                            results.push({
                                plcCategory: item.category,
                                plcCategoryDes: item.description,
                                plcCategoryDesNew: "代码:" + item.category + "  描述:" + item.description
                            });

                        }
                        return results;

                    } 
                }, 
                {   
                    field: 'plcObjectDes', name: 'plcObjectDes', displayName: '数据项', visible: true, minWidth: 200,
                    enableCellEdit: $scope.networkConfig.canEdit, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },
                    cellTemplate: 'andon-ui-grid-tpls/addinfopart-plcObject',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownIdLabel: 'plcObjectDes', editDropdownValueLabel: 'plcObjectDesNew',
                    editDropdownOptionsFunction: function(rowEntity, colDef){
                        UtilsService.log("调用成功");
                        var results = [];
                        var item = $scope.config.plcDataItems;
                        UtilsService.log("修改前"+angular.toJson(item,true));
                        if(!angular.isUndefined(rowEntity.isLoading) ||  !angular.isUndefined(rowEntity.changeDropDown)){
                            item = rowEntity.changeDropDown;
                        }
                        UtilsService.log("修改后"+angular.toJson(item,true));
                        for(var i = 0; i < item.length; i++){
                            //var item = $scope.config.plcDataItems[i];
                            if(!angular.isUndefined(item[i].handle)){
                                results.push({
                                    plcObject: item[i].plcObject,
                                    plcObjectDes: item[i].plcObjectDescription,
                                    plcObjectDesNew: "代码:" + item[i].plcObject + "  描述:" + item[i].plcObjectDescription
                                });
                            }else{
                                results.push({
                                    plcObjectDes: item[i].plcObjectDes,
                                    plcObjectDesNew: item[i].plcObjectDescription
                                });
                            }
                        }
                        if($scope.config.currentPlcCategory.category == "C05"){
                            results = [{
                                plcObject: "",
                                plcObjectDes: rowEntity.plcObjectDes,
                                plcObjectDesNew: rowEntity.plcObjectDes
                            },{
                                plcObject: "",
                                plcObjectDes: rowEntity.plcObjectDes+" ",
                                plcObjectDesNew: "更新报警信息描述"
                            }];
                            if(rowEntity.plcObjectDes == undefined){
                                results = [{
                                    plcObject: "",
                                    plcObjectDes: " ",
                                    plcObjectDesNew: "更新报警信息描述"
                                }];
                            }
                        }else if($scope.config.currentPlcCategory.category == "C06"){
                            results = [{
                                plcObject: "",
                                plcObjectDes: rowEntity.plcObjectDes,
                                plcObjectDesNew: rowEntity.plcObjectDes
                            },{
                                plcObject: "",
                                plcObjectDes: rowEntity.plcObjectDes+" ",
                                plcObjectDesNew: "更新关键配件寿命描述"
                            }];
                            if(rowEntity.plcObjectDes == undefined){
                                results = [{
                                    plcObject: "",
                                    plcObjectDes: " ",
                                    plcObjectDesNew: "更新关键配件寿命描述"
                                }];
                            }
                        }
                        UtilsService.log(angular.toJson(results,true));
                        return results;
                    } 
                },  
                { 
                    field: 'plcAddress', name: 'plcAddress', displayName: 'PLC地址', visible: true, minWidth: 150,
                    enableCellEdit: $scope.networkConfig.canEdit, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },
                    cellTemplate: 'andon-ui-grid-tpls/addinfopart-plcAddress',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'plcValue', name: 'plcValue', displayName: '寄存器值', visible: true, minWidth: 150,
                    enableCellEdit: $scope.networkConfig.canEdit, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true }, cellClass:'ad-uppercase text-align-right',
                    cellTemplate: 'andon-ui-grid-tpls/addinfopart-plcValue',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                { 
                    field: 'logPath', name: 'logPath', displayName: 'Log路径',
                    visible: true, minWidth: 150,
                    enableCellEdit: $scope.networkConfig.canEdit, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: false },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'isDeleted', name: 'isDeleted', displayName: '删除',
                    visible:true, width: 60,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    visible: $scope.networkConfig.canEdit,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return "删除该行"; }, 
                    cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col" ng-class="{\'cannot-delete\':(row.entity.plcAddress.length<=0)}"><span class="ion-trash-a"></span></div>',
                    headerTemplate: '<div class="xbr-delete-col-header" >删除</div>'
                },
            ],  
            data: [] 
        };  
        
        

        $scope.init = function(){
            __initDropdowns('init');
            $scope.config.plcCategories = [];
            //$scope.config.plcCategories.push($scope.config.defaultSelectItem);
            //$scope.config.currentPlcCategory = $scope.config.plcCategories[0];
            //console.log($scope.config.plcCategories);
            
            __requestDropdownWorkCenters();
            __requestDropdownDeviceTypes();
            __requestPlcCategories();
        };  
          
        function __initDropdowns(type){ 
            // $scope.config.workCenters = [];
            // $scope.config.workCenters.push($scope.config.defaultSelectItem);
            // $scope.config.currentWorkCenter = $scope.config.workCenters[0];
            
            if(type == 'init'){ 
                $scope.config.workCenters = [];
                $scope.config.workCenters.push($scope.config.defaultSelectItem);
                $scope.config.currentWorkCenter = $scope.config.workCenters[0];

                $scope.config.deviceTypes = [];
                $scope.config.currentDeviceType = null;
                //$scope.config.deviceTypes = angular.copy($scope.config.defaultSelectItems);// [];
                // $scope.config.deviceTypes.push($scope.config.defaultSelectItem);
                //$scope.config.currentDeviceType = $scope.config.deviceTypes[0];
            }

            if(type == 'init' || type == 'workCenterChanged'){
                $scope.config.lines = [];
                $scope.config.lines.push($scope.config.defaultSelectItem);
                $scope.config.currentLine = $scope.config.lines[0];

                $scope.config.deviceNums = [];
                $scope.config.deviceNums.push($scope.config.defaultSelectItem);
                $scope.config.currentDeviceNum = $scope.config.deviceNums[0]; 
            }

            if(type == 'lineChanged'){
                $scope.config.deviceNums = [];
                $scope.config.deviceNums.push($scope.config.defaultSelectItem);
                $scope.config.currentDeviceNum = $scope.config.deviceNums[0]; 
            }

            if(type == 'deviceTypeChanged'){
                $scope.config.deviceNums = [];
                $scope.config.deviceNums.push($scope.config.defaultSelectItem);
                $scope.config.currentDeviceNum = $scope.config.deviceNums[0]; 
            }
        };

        $scope.addRows = function(){
            $scope.bodyConfig.overallWatchChanged = true;
            $scope.config.btnDisabledSave = false;
            if(angular.isUndefined($scope.addressInfoPartGrid.data) || $scope.addressInfoPartGrid.data == null){
                $scope.addressInfoPartGrid.data = [];
            }
            for(var i = 0; i < $scope.config.addRowsNum; i++){
                $scope.bodyConfig.overallWatchChanged = true;
                $scope.addressInfoPartGrid.data.unshift({
                    hasChanged: true,
                    resrce: $scope.config.currentDeviceNum.resrce,
                    resrceDes: $scope.config.currentDeviceNum.description,
                    categoryDes: $scope.config.currentPlcCategory.description,

                    resourceBo: $scope.config.currentDeviceNum.handle,
                    //resourcePO: {
                    //    resrce: $scope.config.currentDeviceNum.resrce,
                    //    description: $scope.config.currentDeviceNum.description
                    //}
                    resourcePO: $scope.config.currentDeviceNum
                }); 
            }
        };

        $scope.queryDatas = function(){
            if($scope.bodyConfig.overallWatchChanged){
                var promise =UtilsService.confirm('还有修改未保存，是否继续', '提示', ['确定', '取消']);
                promise.then(function(){
                    $scope.config.plcDataItems = [];
                    $scope.config.deletedRows = [];
                    $scope.config.btnDisabledAdd = false;
                    $scope.config.btnDisabledDelete = true;
                    $scope.addressInfoPartGrid.paginationCurrentPage = 1;
                    __requestAddressInfoPartsCount();
                    __requestTableDatas();
                    __requestQueryDataItems();
                }, function(){
                    //取消停留在原页面
                    $scope.bodyConfig.overallWatchChanged = true;
                });
            }else{
                $scope.config.deletedRows = [];
                $scope.config.btnDisabledAdd = false;
                $scope.config.btnDisabledDelete = true;
                $scope.addressInfoPartGrid.paginationCurrentPage = 1;
                __requestAddressInfoPartsCount();
                __requestTableDatas();
                __requestQueryDataItems();
            };

        };

        $scope.checkAndDeleteSelectedRows = function(){
            //console.log($scope.addressInfoPartGrid.data);
            //var deletedRowIndexs = [];
            //var rows = $scope.config.gridApi.selection.getSelectedRows();
            //for(var i = 0; i < rows.length; i++){
            //    var row = rows[i];
            //    if( !angular.isUndefined(row.handle) && row.handle!=null && row.handle!="" ){
            //        $scope.addressInfoPartGrid.data[i].canDeleted = true;
            //        $scope.addressInfoPartGrid.data[i].isSelected = true;
            //    }else{
            //        row.canDeleted = true;
            //        row.isSelected = true;
            //    }
            //}
            $scope.bodyConfig.overallWatchChanged = true;
            __chekForDeleteRows();
        };

        function __chekForDeleteRows(){
            $scope.config.deletedRows = [];
            var rows = $scope.config.gridApi.selection.getSelectedRows();
            if(rows.length > 0){
                for(var i =0;i<rows.length;i++){
                    $scope.config.deletedRows.push(rows[i]);
                    __deleteRowDataByHashKey(rows[i].$$hashKey);
                };
            }else{
                $scope.addAlert("info","先选择要删除的行");
            }
        };  

        $scope.saveAllAcitons = function(){ 
            if(!__checkCanSaveDatas()){   
                return;   
            };  
            var paramActions = [];
            console.log($scope.addressInfoPartGrid.data);
            for(var i = 0; i < $scope.addressInfoPartGrid.data.length; i++){
                var row = $scope.addressInfoPartGrid.data[i];

                if(row.hasChanged){ 
                    // var viewActionCode = angular.isUndefined(row.handle) ? 'C' : 'U';
                    if(angular.isUndefined(row.handle)){ //创建新数据 ，若是删除中存在该条数据，则直接忽略该行,并在后面将删除操作也忽略
                        if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){ 
                            // var paraAction = angular.copy(row);
                            // paraAction.viewActionCode = 'C';
                            // paramActions.push(paraAction);
                            var plcObject = null;
                            for(var j=0;j<$scope.config.plcDataItems.length;j++){
                                if($scope.config.plcDataItems[j].plcObjectDescription==row.plcObjectDes){
                                    plcObject = $scope.config.plcDataItems[j].plcObject;
                                };
                            };
                            paramActions.push({
                                site: HttpAppService.getSite(),
                                resourceBo: $scope.config.currentDeviceNum.handle,
                                resourcePO: $scope.config.currentDeviceNum,
                                plcAddress: row.plcAddress,
                                plcValue: row.plcValue,
                                category: row.category?row.category:getCategoryByDescription(row.categoryDes),
                                plcObject: row.plcObject,
                                description: row.description,
                                logPath: row.logPath,
                                viewActionCode : "C",
                                plcObjectDes : row.plcObjectDes
                            }); 
                            // paramActions.push({  // TODO
                                //     "handle": row.handle, 
                                //     "resrce": row.resrce,
                                //     "resourceName": row.resourceName, 
                                //     "ipAddress": row.ipAddress,  
                                //     "port": row.port,
                                //     "accessType": row.accessType,
                                //     "viewActionCode": 'C'  
                            // });  
                        }else{}
                    }else{ //修改已存于服务器的行数据 ，若删除列表中有该行，则直接忽略该修改提交，并在后面将删除操作添加进来
                        if(__needChaiFei(row)){
                            var paraAction = __getRowCached(row);
                            paraAction.viewActionCode = 'D';
                            paramActions.push(paraAction);
                            var paraAction = angular.copy(row);
                            paraAction.viewActionCode = 'C';
                            paramActions.push(paraAction);
                        }else{
                            if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
                                var paraAction = angular.copy(row);
                                paraAction.viewActionCode = 'U';
                                paramActions.push(paraAction);
                            }else{}
                        }
                    }
                }
            };

            for(var i = 0; i < $scope.config.deletedRows.length; i++){
                var deleteTempRow = $scope.config.deletedRows[i];
                if(!angular.isUndefined(deleteTempRow.handle)){     // 服务器端数据的删除操作,直接添加进去即可
                    var paraAction = angular.copy(deleteTempRow);
                    paraAction.viewActionCode = 'D';
                    paramActions.push(paraAction);
                }else{ }
            }
            console.log(paramActions);
            if($scope.config.currentPlcCategory.category == "C05" || $scope.config.currentPlcCategory.category == "C06"){
                for(var i=0;i<paramActions.length;i++){
                    paramActions[i].description = paramActions[i].plcObjectDes;
                    paramActions[i].plcObjectDes = "";
                }
            }
            console.log(paramActions);
            if(paramActions.length > 0){
                $scope.showBodyModel("正在保存");
                plcService
                    .plcAddressChange(paramActions)
                    .then(function (resultDatas){
                        $scope.config.btnDisabledSave = true;
                        if(!$scope.$$phase){
                            $scope.$apply();
                        }
                        $scope.bodyConfig.overallWatchChanged = false;
                        $scope.hideBodyModel();
                        $scope.config.deletedRows = [];
                        $scope.addAlert('success', '保存成功!');
                        $scope.queryDatas();
                    },function (resultDatas){
                        $scope.bodyConfig.overallWatchChanged = false;
                        $scope.hideBodyModel();
                        $scope.addAlert('danger', resultDatas.myHttpConfig.errorMessage);
                        //$scope.addAlert('danger', '保存失败!');
                    });
            }else{ 
                $scope.addAlert('没有修改,无需保存!');
            }
        };

        function __checkCanSaveDatas(){
            //return true;
            var category = ['C03','C06','C07'];
            for(var i = 0; i < $scope.addressInfoPartGrid.data.length; i++){
                var item = $scope.addressInfoPartGrid.data[i];
                if(item.hasChanged){  // TODO
                    var rules = [
                        { field: 'categoryDes', emptyDesc: '第'+(i+1)+'行: PLC设备类型不能为空!' },
                        { field: 'plcObjectDes', emptyDesc: '第'+(i+1)+'行: 数据项不能为空!' },
                        { field: 'plcAddress', emptyDesc: '第'+(i+1)+'行: PLC地址不能为空!' },
                        { field: 'plcValue', emptyDesc: '第'+(i+1)+'行: 寄存器值不能为空!' }
                    ];
                    for(var ruleIndex in rules){
                        var rule = rules[ruleIndex];
                        if(angular.isUndefined(item[rule.field]) || item[rule.field] == null || item[rule.field] == ""){
                            if(category.indexOf(getCategoryByDescription(item.categoryDes))<0){
                                $scope.addAlert('danger', rule.emptyDesc);
                                return false;
                            }

                        }
                    }
                }
            }
            return true;
        };

        function __deletedRowsExitThisRowByHashKey(rowHashKey){
            for(var n = 0; n < $scope.config.deletedRows; n++){
                if(rowHashKey == $scope.config.deletedRows[n].$$hashKey){
                    return true;
                }
            }
            return false;
        };

        function __needChaiFei(row){
            for(var i = 0; i < $scope.config.tableDatasCache.length; i++){
                var cached = $scope.config.tableDatasCache[i];
                if(cached.handle == row.handle){
                    // plcAddress plcValue
                    // 若用户修改了一条记录的PLC地址或寄存器值，需要前端页面逻辑把这步操作解析为对原记录的删除和新记录的新增
                    if(cached.plcAddress == row.plcAddress && cached.plcValue == row.plcValue){
                        return false;
                    }else{
                        return true;
                    }
                }
            }
        };

        function __getRowCached(row){
            for(var i = 0; i < $scope.config.tableDatasCache.length; i++){
                var cached = $scope.config.tableDatasCache[i];
                if(cached.handle == row.handle){
                   return angular.copy(cached);
                }
            }
        };

        function getCategoryByDescription(description){
            var category = $scope.config.plcCategories;
            for(var i=0;i<category.length;i++){
                if(category[i].description == description){
                    return category[i].category;
                }
            }
        };

        function __onRegisterApi(gridApi){  
            $scope.config.gridApi = gridApi;
            // 分页相关 
            $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                console.log("刷新界面 第 "+currentPage+" 页,每页 "+pageSize+"个元素!");
                __requestTableDatas(currentPage, pageSize);
                return true;
            }, $scope); 

            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
                if(newRowCol.col.field == "isDeleted" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                    __deleteRowDataByHashKey(newRowCol.row.entity.$$hashKey);
                    var plcAddress = newRowCol.row.entity['plcAddress'];
                }
            }); 

            $scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                var promise = $q.defer();
                if($scope.config.gridApi.rowEdit){
                    $scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
                }
                if(oldValue == newValue){
                    promise.resolve();
                    return;
                }
                rowEntity.hasChanged = true;
                $scope.bodyConfig.overallWatchChanged = true;
                    
                //if(colDef.field === "plcAddress"){
                //    //if(UtilsService.isEmptyString(rowEntity.plcAddress)){
                //    //    return;
                //    //}
                //    rowEntity.plcAddress = rowEntity.plcAddress.toUpperCase();
                //}
                 if(colDef.field === "plcValue"){
                    //if(UtilsService.isEmptyString(rowEntity.plcValue)){
                    //    return;
                    //}
                    rowEntity.plcValue = rowEntity.plcValue.toUpperCase();
                }
                //PLC数据类型
                if(colDef.field === "categoryDes"){
                    var category = getCategoryByDescription(rowEntity.categoryDes);
                    rowEntity.isLoading = true;
                    plcService
                        .plcDataItemListAll(category)
                        .then(function (resultDatas){
                            rowEntity.isLoading = false;
                            rowEntity.category = category;
                            if(resultDatas.response.length>0){
                                rowEntity.plcObjectDes =resultDatas.response[0].plcObjectDescription;
                                rowEntity.plcObject = resultDatas.response[0].plcObject
                            }else{
                                rowEntity.plcObjectDes = null;
                            }
                            rowEntity.changeDropDown = resultDatas.response;
                        },function (resultDatas){
                            rowEntity.isLoading = false;
                            $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                        });
                };
                //数据项
                if(colDef.field === "plcObjectDes"){
                    var plcDataItem = $scope.config.plcDataItems;
                    if(!angular.isUndefined(rowEntity.changeDropDown)){
                        plcDataItem = rowEntity.changeDropDown;
                    };
                    for(var i = 0; i < plcDataItem.length; i++){
                        //var plcDataItem = $scope.config.plcDataItems[i];
                        if(rowEntity.plcObjectDes === plcDataItem[i].plcObjectDescription){
                            rowEntity.plcObject = plcDataItem[i].plcObject;
                            // rowEntity.handle = plcDataItem.handle;
                            // rowEntity.site = plcDataItem.site;
                            // rowEntity.category = plcDataItem.category;
                            rowEntity.plcObjectDescription = plcDataItem[i].plcObjectDescription;
                            // rowEntity.modifiedDateTime = plcDataItem.modifiedDateTime;
                            break;
                        }
                    }

                };
                // updateValid
                // 修改时: 检验
                if(!angular.isUndefined(rowEntity.handle)){
                    //修改时，PLC地址、寄存器值变为先删除再新增

                    if(colDef.name == "plcAddress" || colDef.name == "plcValue"){
                        //如果修改值跟最初值一致，不做校验
                        var compare = $scope.config.tableDatasCache;
                        for(var i=0;i<compare.length;i++){
                            if(compare[i].handle == rowEntity.handle){
                                if(newValue == compare[i][colDef.name]){
                                    return;
                                };
                            }

                        };
                        if(UtilsService.isEmptyString(newValue)){
                            $scope.addAlert('danger', colDef.displayName+'不可为空!');
                            uiGridValidateService.setValid(rowEntity, colDef);
                            return;
                        }else{
                            //PLC地址、寄存器值变为新增时的校验
                            //校验设备号+PLC地址+寄存器值是否在表RESOURCE_PLC中已经存在，
                            plcService
                                .plcAddressExistResourcePlc({
                                    resourceBo: rowEntity.resourceBo,
                                    plcAddress:rowEntity.plcAddress,
                                    plcValue: rowEntity.plcValue
                                })
                                .then(function (resultDatas){
                                    if(resultDatas.response){
                                        // 若存在，则提示“PLC地址已存在，不允许新增”，新增失败；
                                        $scope.addAlert('danger', 'PLC地址已存在，不允许修改!');
                                        uiGridValidateService.setInvalid(rowEntity, colDef);
                                        rowEntity.newPlcObjectValid = false;
                                        promise.reject();
                                    }else{
                                        //若不存在，则继续
                                        promise.resolve();
                                        rowEntity.hasChanged = true;
                                        rowEntity.newPlcObjectValid = true;
                                        uiGridValidateService.setValid(rowEntity, colDef);
                                    };
                                },function (resultDatas){
                                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                                });
                        };

                        //校验PLC数据类型
                    }
                    else if(colDef.field === "categoryDes"){

                        var category = getCategoryByDescription(rowEntity.categoryDes);
                        var checkPlcValue = ['C01',' C02',' C04',' C05',' C08'];
                        var checkDataItemCode = ['C01',' C02',' C03',' C04','C07',' C08'];
                        var checkDataItemDes = ['C01',' C02',' C03',' C04','C07',' C08'];
                        //若数据类型是设备状态、原因代码、设备启动防呆、报警信息、三色灯，
                        // 则检查寄存器值是否填写，若无值，则将错误记录显示，并在消息列提示“寄存器值没有维护”
                        if(checkPlcValue.indexOf(category) >= 0 ){
                            if(rowEntity.plcValue ==''|| rowEntity.plcValue == null){
                                $scope.addAlert('danger','寄存器值没有维护');
                                return;
                            }
                        }
                        //若数据类型是设备状态、原因代码、设备启动防呆、优率数据、实时PPM、三色灯，
                        // 则检查数据项代码是否填写，若无值，则将错误记录显示，并在消息列提示“数据项代码没有维护”
                        else if(checkDataItemCode.indexOf(category) >= 0 ){
                            if(rowEntity.plcObjectDes ==''|| rowEntity.plcObjectDes == null){
                                $scope.addAlert('danger','数据项代码没有维护');
                                return;
                            }
                        }
                        //若数据类型是报警信息和关键配件寿命，
                        // 则检查描述是否填写，若无值，则将错误记录显示，并在消息列提示“数据项描述没有维护”
                        else if(checkDataItemDes.indexOf(category) >= 0 ){
                            if(rowEntity.plcObjectDes ==''|| rowEntity.plcObjectDes == null){
                                $scope.addAlert('danger','数据项描述没有维护');
                                return;
                            }
                        };

                        //rowEntity.isLoading = true;
                        //plcService
                        //    .plcDataItemListAll(category)
                        //    .then(function (resultDatas){
                        //        rowEntity.changeDropDown = resultDatas.response;
                        //        rowEntity.isLoading = false;
                        //        rowEntity.category = category;
                        //        if(resultDatas.response.length>0){
                        //            rowEntity.plcObjectDes =resultDatas.response[0].plcObjectDescription
                        //            rowEntity.plcObject = resultDatas.response[0].plcObject
                        //        }else{
                        //            rowEntity.plcObjectDes = null;
                        //        }
                        //    },function (resultDatas){
                        //        rowEntity.isLoading = false;
                        //        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                        //    });
                    }else{
                        if(colDef.field === "plcObjectDes"){
                            if($scope.config.currentPlcCategory.category == "C05" || $scope.config.currentPlcCategory.category == "C06"){
                                var modalInstance = $uibModal.open({
                                    animation: true,
                                    templateUrl: 'modules/plc/deviceMaintain/addInfoPart/addInfoPartModel.html',
                                    controller: 'addInfoPartModalCtrl',
                                    size: '',
                                    backdrop:'false',
                                    openedClass:'flex-center-parent',
                                    scope : $scope,
                                    resolve: {
                                        addInfoPartModal:  rowEntity
                                    }
                                });
                                modalInstance.result.then(function (item) {
                                    rowEntity.plcObjectDes = item;
                                }, function () {
                                    //$log.info('Modal dismissed at: ' + new Date());
                                });
                            }else{
                                plcService
                                    .plcAddressExistPlcObject({
                                        resourceBo: rowEntity.resourceBo,
                                        plcAddress: rowEntity.plcAddress,
                                        plcValue: rowEntity.plcValue,
                                        plcObject: rowEntity.plcObject
                                    })
                                    .then(function (resultDatas){
                                        if(resultDatas.response){ //已经存在:true
                                            $scope.addAlert('danger', '数据项已存在PLC地址对应，不允许修改!');
                                            uiGridValidateService.setInvalid(rowEntity, colDef);
                                            rowEntity.updateValid = false;
                                            promise.reject();
                                        }else{  //不存在冲突，可以使用
                                            console.log("不存在冲突，可以使用");
                                            promise.resolve();
                                            rowEntity.hasChanged = true;
                                            rowEntity.updateValid = true;
                                            uiGridValidateService.setValid(rowEntity, colDef);
                                        }
                                    },function (resultDatas){
                                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                                    });
                            }
                        }

                    };
                }else{ //新增时的校验 ：2个
                    if(colDef.name == "plcObjectDes"){
                        // 校验设备号+数据项是否在表RESOURCE_PLC中已经存在
                        // tip: PLC地址已存在，不允许新增
                        if($scope.config.currentPlcCategory.category == "C05" || $scope.config.currentPlcCategory.category == "C06"){
                            var modalInstance = $uibModal.open({
                                animation: true,
                                templateUrl: 'modules/plc/deviceMaintain/addInfoPart/addInfoPartModel.html',
                                controller: 'addInfoPartModalCtrl',
                                size: '',
                                backdrop:'false',
                                openedClass:'flex-center-parent',
                                scope : $scope,
                                resolve: {
                                    addInfoPartModal:  rowEntity
                                }
                            });
                            modalInstance.result.then(function (item) {
                                rowEntity.plcObjectDes = item;
                                rowEntity.hasChanged = true;
                                rowEntity.newPlcObjectValid = true;
                            }, function () {
                                //$log.info('Modal dismissed at: ' + new Date());
                            });
                        }else{
                            plcService
                                .plcAddressExistResourcePlcObj({
                                    resourceBo: rowEntity.resourceBo,
                                    plcObject: rowEntity.plcObject
                                })
                                .then(function (resultDatas){
                                    if(resultDatas.response){ //已经存在:true
                                        $scope.addAlert('danger', 'PLC地址已存在，不允许新增!');
                                        uiGridValidateService.setInvalid(rowEntity, colDef);
                                        rowEntity.newPlcObjectValid = false;
                                        promise.reject();
                                    }else{  //不存在冲突，可以使用
                                        console.log("不存在冲突，可以使用");
                                        promise.resolve();
                                        rowEntity.hasChanged = true;
                                        rowEntity.newPlcObjectValid = true;
                                        uiGridValidateService.setValid(rowEntity, colDef);
                                    }
                                },function (resultDatas){
                                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                                });
                        }
                    }else if(colDef.name == "plcAddress" || colDef.name == "plcValue"){
                        // 校验设备号+PLC地址+寄存器值是否在表RESOURCE_PLC中已经存在
                        // tip: 数据项已存在PLC地址对应，不允许新增
                        if(UtilsService.isEmptyString(newValue)){
                            $scope.addAlert('danger', colDef.displayName+'不可为空!');
                            uiGridValidateService.setInvalid(rowEntity, colDef);
                            return;
                        }else{
                            plcService
                                .plcAddressExistResourcePlc({
                                    resourceBo: rowEntity.resourceBo,
                                    plcAddress: rowEntity.plcAddress,
                                    plcValue: rowEntity.plcValue
                                })
                                .then(function (resultDatas){
                                    if(resultDatas.response){ //已经存在:true
                                        $scope.addAlert('danger', 'PLC地址已存在，不允许新增!');
                                        uiGridValidateService.setInvalid(rowEntity, colDef);
                                        rowEntity.newPlcObjectValueValid = false;
                                        promise.reject();
                                    }else{  //不存在冲突，可以使用
                                        promise.resolve();
                                        rowEntity.hasChanged = true;
                                        rowEntity.newPlcObjectValueValid = true;
                                        uiGridValidateService.setValid(rowEntity, colDef);
                                    }
                                },function (resultDatas){
                                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                                });
                        };

                    }else{
                        promise.resolve();
                    }
                }
            }); 
            $scope.config.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
                $scope.config.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
            }); 
            //选择相关
            $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                gridRow.entity.isSelected = gridRow.isSelected;
                gridRow.entity.canDeleted= true;
                var count = $scope.config.gridApi.selection.getSelectedCount();
                $scope.config.btnDisabledDelete = count > 0 ? false : true;
            });
            $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                var count = $scope.config.gridApi.selection.getSelectedCount();
                $scope.config.btnDisabledDelete = count > 0 ? false : true;
                for(var i = 0; i < gridRows.length; i++){
                    gridRows[i].entity.isSelected = gridRows[i].isSelected;
                }
            }); 
        };

        function __deleteRowDataByHashKey(hashKey){ 
            for(var i = 0; i < $scope.addressInfoPartGrid.data.length; i++){
                if(hashKey == $scope.addressInfoPartGrid.data[i].$$hashKey){
                    $scope.bodyConfig.overallWatchChanged = true;   
                    $scope.config.deletedRows.push($scope.addressInfoPartGrid.data[i]);
                    $scope.addressInfoPartGrid.data = UtilsService.removeArrayByIndex($scope.addressInfoPartGrid.data, i);
                    break; 
                }
            } 
        };
        
        function __requestTableDatas(pageIndex, pageCount){
            $scope.addressInfoPartGrid.data = [];
            var resource = $scope.config.currentDeviceNum.resrce;
            var plcCategory = $scope.config.currentPlcCategory.category;
            var pageIndexX = 1;
            var pageCountX = $scope.addressInfoPartGrid.paginationPageSize;
            if(angular.isUndefined(pageIndex) || pageIndex == null | pageIndex<=1){
                pageIndexX = 1;
            }else{ 
                pageIndexX = pageIndex;
            }
            if(angular.isUndefined(pageCount) || pageCount == null | pageCount<1){
                pageCountX = $scope.addressInfoPartGrid.paginationPageSize;
            }else{
                pageCountX = pageCount;
            }
            plcService 
                .plcAddressList(resource, plcCategory, pageIndexX, pageCountX)
                .then(function (resultDatas){
                    $scope.config.btnDisabledAdd = false;
                    $scope.config.btnDisabledSave = false;
                    $scope.bodyConfig.overallWatchChanged = false;

                    if(resultDatas.response && resultDatas.response.length > 0){
                        // $scope.bodyConfig.overallWatchChanged = false;
                        $scope.config.tableDatasCache = angular.copy(resultDatas.response);
                        $scope.addressInfoPartGrid.data = resultDatas.response;
                        // $scope.config.btnDisabledAdd = false;
                        // $scope.config.btnDisabledSave = false;
                        return;
                    }else{
                        $scope.addressInfoPartGrid.data = $scope.config.tableDatasCache = [];
                        $scope.addAlert('', '暂无数据!');
                    }
                }, function (errorResultDatas){
                    $scope.addressInfoPartGrid.data = $scope.config.tableDatasCache = [];
                    $scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
                });
        };

        $scope.init();

        function __requestPlcCategories(){
            plcService
                .requestPlcCategoryList()
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.plcCategories = [];
                        //$scope.config.plcCategories.push($scope.config.defaultSelectItem);
                        //$scope.config.currentPlcCategory = $scope.config.plcCategories[0];
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].category+"   描述:"+resultDatas.response[i].description;
                            $scope.config.plcCategories.push(resultDatas.response[i]);
                        }
                        //console.log($scope.config.plcCategories);
                        // $scope.config.plcCategories = $scope.config.plcCategories.concat(resultDatas.response);
                        return;
                    }   
                }, function (resultDatas){
                    $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
                }); 
        };

        function __requestQueryDataItems(){ 
            var plcCategory = $scope.config.currentPlcCategory.category;
            plcService
                .plcDataItemListAll(plcCategory)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){ 
                        $scope.config.plcDataItems = [];
                        //$scope.config.plcDataItems.push({
                        //    plcObjectDescription: " ---   请选择   --- ",
                        //    descriptionNew: " ---   请选择   --- "
                        //});
                        for(var i = 0; i < resultDatas.response.length; i++){ 
                            resultDatas.response[i].descriptionNew = "代码: "+resultDatas.response[i].plcObject+"   描述: "+resultDatas.response[i].plcObjectDescription; 
                            $scope.config.plcDataItems.push(resultDatas.response[i]); 
                        }   
                        return;
                    }       
                },function (resultDatas){ //TODO 检验失败 
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc); 
                });  
        };

        function __requestAddressInfoPartsCount(){  
            var resource = $scope.config.currentDeviceNum.resrce; 
            var plcCategory = $scope.config.currentPlcCategory.category; 
            plcService      
                .plcAddressCount(resource, plcCategory) 
                .then(function (resultDatas){ 
                    $scope.addressInfoPartGrid.totalItems = resultDatas.response; 
                    // $scope.config.totalCountNum = resultDatas.response;
                },function (resultDatas){ //TODO 检验失败 
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);  
                }); 
        };

        function __setBtnsDisable(){
            if($scope.config.currentWorkCenter.workCenter == null
                || $scope.config.currentDeviceNum.resrce == null){
                $scope.config.btnDisabledAdd = true;
                $scope.config.btnDisabledQuery = true;
                $scope.config.btnDisabledDelete = true;
                $scope.config.btnDisabledSave = true;
                return;
            }
            if(!$scope.config.currentPlcCategory){
                $scope.config.btnDisabledAdd = true;
                $scope.config.btnDisabledQuery = true;
                $scope.config.btnDisabledDelete = true;
                $scope.config.btnDisabledSave = true;
                return;
            }
            // $scope.config.btnDisabledAdd = false;
            $scope.config.btnDisabledQuery = false;
            // $scope.config.btnDisabledDelete = false;
            if(!$scope.$$phase){
                $scope.$apply();
            }
        };

        $scope.plcCategoryChanged = function(){
            __setBtnsDisable();
            var category = $scope.config.currentPlcCategory;
            __requestQueryDataItems();
            if($scope.addressInfoPartGrid.data.length>0&&($scope.addressInfoPartGrid.data[0].categoryDes!=$scope.config.currentPlcCategory.description)){
            	$scope.config.btnDisabledAdd=true;
            }else{
            	$scope.config.btnDisabledAdd=false;
            }
        };

        /////   生成区域、级联选择 
        $scope.workCenterChanged = function(){
            if($scope.config.currentWorkCenter == null || $scope.config.currentWorkCenter.workCenter==null){
                __initDropdowns('workCenterChanged');
            }else{
                __initDropdowns('workCenterChanged');
                var workCenter = $scope.config.currentWorkCenter.workCenter;
                __requestDropdownLines(workCenter);
                var line = $scope.config.currentLine.workCenter;
                if($scope.config.currentLine != null && $scope.config.currentLine.workCenter!=null){
                    var resourceType = $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:'';
                    __requestDropdownDeviceCodes(workCenter, line, resourceType);
                }
            }
            __setBtnsDisable(); 
        };

        $scope.lineChanged = function(){
            if($scope.config.currentLine == null || $scope.config.currentLine.workCenter==null){
                __initDropdowns('lineChanged');
            }else{
                __initDropdowns('lineChanged');
                var workCenter = $scope.config.currentWorkCenter.workCenter;
                var line = $scope.config.currentLine.workCenter;
                var resourceType = $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:'';
                __requestDropdownDeviceCodes(workCenter, line, resourceType);
                // if($scope.config.currentDeviceType != null && $scope.config.currentDeviceType.resourceType!=null){
                //     $scope.config.btnDisabledAdd = false;
                //     $scope.config.btnDisabledQuery = false;
                // }
            }
            __setBtnsDisable(); 
        };

        $scope.deviceTypeChanged = function(){
            //if($scope.config.currentDeviceType == null || $scope.config.currentDeviceType.resourceType==null){
            //    __initDropdowns('deviceTypeChanged');
            //    var workCenter = $scope.config.currentWorkCenter.workCenter;
            //    var line = $scope.config.currentLine.workCenter;
            //    __requestDropdownDeviceCodes(workCenter, line, null);
            //}else{
            //
            //    // if($scope.config.currentLine != null && $scope.config.currentLine.workCenter!=null){
            //    //     __requestDropdownDeviceCodes(workCenter, line, resourceType);
            //    //     $scope.config.btnDisabledQuery = false;
            //    // }
            //}

            __initDropdowns('deviceTypeChanged');
            var workCenter = $scope.config.currentWorkCenter.workCenter;
            var line = $scope.config.currentLine.workCenter;
            var resourceType = $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:'';
            __requestDropdownDeviceCodes(workCenter, line, resourceType);
            __setBtnsDisable();
        };

        $scope.deviceNumChanged = function(){ 
            // if($scope.config.currentDeviceType == null || $scope.config.currentDeviceType.resourceType==null){
            //     $scope.config.btnDisabledAdd = true;
            // }else{
            //     $scope.config.btnDisabledAdd = false;
            // } 
            __setBtnsDisable();
        };

        function __requestDropdownWorkCenters(){
            plcService
                    .dataWorkCenters() 
                    .then(function (resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].workCenter+"   描述:"+resultDatas.response[i].description;
                                $scope.config.workCenters.push(resultDatas.response[i]);
                            }
                            // $scope.config.workCenters = $scope.config.workCenters.concat(resultDatas.response);
                            return; 
                        } 
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    }); 
        };

        function __requestDropdownLines(workCenter){
            plcService
                    .dataLines(workCenter) 
                    .then(function (resultDatas){ 
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].workCenter+"   描述:"+resultDatas.response[i].description;
                                $scope.config.lines.push(resultDatas.response[i]);
                            }
                            // $scope.config.lines = $scope.config.lines.concat(resultDatas.response);
                            return; 
                        } 
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    });  
        };

        function __requestDropdownDeviceTypes(){  
            plcService
                    .dataResourceTypes() 
                    .then(function (resultDatas){ 
                        if(resultDatas.response && resultDatas.response.length > 0){
                            $scope.config.deviceTypes = [];
                            $scope.config.deviceTypes.push($scope.config.defaultSelectItem);
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].resourceType+"   描述:"+resultDatas.response[i].description;
                                $scope.config.deviceTypes.push(resultDatas.response[i]);
                            }
                            // $scope.config.deviceTypes = $scope.config.deviceTypes.concat(resultDatas.response);
                            return; 
                        } 
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    }); 
        };

        function __requestDropdownDeviceCodes(workCenter, line, resourceType){
            plcService
                    .dataPlcResourceCodes(workCenter, line, resourceType) 
                    .then(function (resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].resrce+"   描述:"+resultDatas.response[i].description;
                                $scope.config.deviceNums.push(resultDatas.response[i]);
                            }
                            //console.log($scope.config.deviceNums);
                            // $scope.config.deviceNums = $scope.config.deviceNums.concat(resultDatas.response);
                            return; 
                        } 
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    }); 
        };


    }]);
plcModule.controller('addInfoPartModalCtrl',['$scope', '$http',
    'HttpAppService', '$timeout', 'plcService', '$state',
    'uiGridConstants', '$q', 'UtilsService', 'uiGridValidateService','$uibModal','addInfoPartModal','$uibModalInstance',
    function ($scope, $http, HttpAppService, $timeout, plcService, $state,
              uiGridConstants, $q, UtilsService, uiGridValidateService,$uibModal,addInfoPartModal,$uibModalInstance) {
        console.log(addInfoPartModal);
        $scope.config = {
            descInfo : addInfoPartModal.plcObjectDes,
            confirmBtn : true,
            nameModify : null,
            nameLable : null
        }
        if(addInfoPartModal.category == "C05"){
            $scope.config.nameModify = "更新报警信息描述";
            $scope.config.nameLable = "报警信息描述";
        }else{
            $scope.config.nameModify = "更新关键配件寿命描述";
            $scope.config.nameLable = "关键配件寿命描述";
        }

        $scope.modelChange = function(){
            if($scope.config.descInfo == null || $scope.config.descInfo == ""){
                $scope.config.confirmBtn = true;
            }else{
                $scope.config.confirmBtn = false;
            }
        }
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        //确定以及验证
        $scope.ok = function () {
            $uibModalInstance.close($scope.config.descInfo);
        };
    }]);