plcModule.controller('networkInfoCtrl',['$scope', '$http',
    'HttpAppService', '$timeout', '$state',
    'uiGridConstants',
    function ($scope, $http, HttpAppService, $timeout, $state,
              uiGridConstants) {

        $scope.networkConfig = {
            canEdit: false
        };
        $scope.networkConfig.canEdit = $scope.modulesRWFlag("#/andon/NetworkInfo/network");

        $scope.$on("$stateChangeSuccess", function (event2, toState, toParams, fromState, fromParam){
            if(fromState && toState.name == 'andon.NetworkInfo.network' || fromState.name == 'andon.NetworkInfo.AddressInfo' && toState && toState.name == "andon.NetworkInfo.network" || fromState.name == 'andon.NetworkInfo.AddInfoPart' && toState && toState.name == "andon.NetworkInfo.network"){
                //if(window.event && window.event.type == "load"){
                //    console.log("first should be true")
                //    $scope.first = true;
                //    $scope.second = false;
                //}   
                $scope.first = true;
                $scope.second = false;
                $scope.third = false;
            }else if(fromState && toState.name == 'andon.NetworkInfo.AddressInfo'|| fromState.name == 'andon.NetworkInfo.network' && toState && toState.name == "andon.NetworkInfo.AddressInfo" || fromState.name == 'andon.NetworkInfo.AddInfoPart' && toState && toState.name == "andon.NetworkInfo.AddressInfo"){
                $scope.second = true;
                $scope.first = false;
                $scope.third = false;
            }else if(fromState && toState.name == 'andon.NetworkInfo.AddInfoPart'|| fromState.name == 'andon.NetworkInfo.network' && toState && toState.name == "andon.NetworkInfo.AddInfoPart" || fromState.name == 'andon.NetworkInfo.AddressInfo' && toState && toState.name == "andon.NetworkInfo.AddInfoPart"){
                $scope.second = false;
                $scope.first = false;
                $scope.third = true;
            }
        });
    }])