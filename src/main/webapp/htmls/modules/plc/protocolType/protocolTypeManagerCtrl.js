/**
 * 
 * [协议类型界面Controller: protocolTypeManagerCtrl]
 * 
 */
plcModule.controller('protocolTypeManagerCtrl',[
	'$scope', '$http', '$q', 'UtilsService', '$state',
    'HttpAppService', '$timeout', 'plcService',
    'uiGridConstants', 'uiGridValidateService',
    function ($scope, $http, $q, UtilsService, $state,
    		HttpAppService, $timeout, plcService,
            uiGridConstants, uiGridValidateService) { 


        $scope.init = function(){
        	__requestTableDatas(); 
        }; 

        $scope.config = {
        	gridApi: null,
        	addRowsNum: 1,
			deletedRows: [],  // 缓存删除且未提交的表格行数据

			btnDisabledDelete: true,  // 是否禁用<删除>按钮
			btnDisabledSave: true,    // 是否禁用<保存>按钮

			canEdit: false,			  // 当前用户是否对该activity有编辑权限

			isFirstAddClass:true      //用于去除grid左下角空白区域
        };	
        $scope.config.canEdit = $scope.modulesRWFlag("#"+$state.$current.url.source);

        $scope.plcProtocolTypeGridOptions = {
        	enablePagination: false,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,
            paginationCurrentPage: 1,
            // paginationTemplate: 'ui-grid/pagination',
            totalItems: 0,
            useExternalPagination: true,


		    showGridFooter: false, 
		    modifierKeysToMultiSelectCells: false, 
		    columnDefs: [
		    	{ 
		    		field: 'handle', name: 'handle', visible: false
		    	}, 
		    	{
		    		name: 'site', displayName: '工厂', minWidth: 100, visible: false,
		    		enableCellEdit: false, enableCellEditOnFocus: false,
		    		validators: { required: true }, cellClass:'grid-no-editable',
		    		cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
		    		editableCellTemplate: 'ui-grid/dropdownEditor',
		    		editDropdownValueLabel: 'site',
		    		editDropdownOptionsArray: [
		    			{ id: 1000, site: '1000' },
						{ id: 2000, site: '2000' },
						{ id: 3000, site: '3000' }
		    		]
		    	}, 
		    	{ 
		    		field: 'plcAccessType', name: 'plcAccessType', displayName: '协议类型代码', minWidth: 150, 
		    		enableCellEdit: $scope.config.canEdit, enableCellEditOnFocus: false, validators: { required: true },
		    		cellTooltip: function(row, col){  return row.entity[col.colDef.name];  }, 
		    		cellTemplate: 'andon-ui-grid-tpls/accessType-typeCode', cellClass:'grid-no-editable ad-uppercase',
		    		cellEditableCondition: function($scope){
		    			var row = arguments[0].row;
		    			var col = arguments[0].col;
		    			// 非新增不可编辑 、新增行可编辑 
		    			if(!angular.isUndefined(row.entity) && row.entity!=null && !angular.isUndefined(row.entity.handle) && row.entity.handle!=null && row.entity.handle!=""){
		    				return false;
		    			}else{
		    				return true;
		    			}
		    		}
		    	}, 
		    	{
		    		field: 'description', name: 'description', displayName: '协议类型描述', minWidth: 200,
		    		enableCellEdit: $scope.config.canEdit, enableCellEditOnFocus: false, validators: {  required: true },
		    		cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
		    		cellTemplate: 'andon-ui-grid-tpls/dataCategory-categoryDesc' 
		    	},
		    	{
		    	 	field: 'isDeleted', name: 'isDeleted', displayName: '删除', width: 40, 
		    		enableCellEdit: false, enableCellEditOnFocus: false, 
		    		visible: $scope.config.canEdit,
		    		enableColumnMenu: false, enableColumnMenus: false,
		    		cellTooltip: function(row, col){ return "删除该行"; }, 
		    		cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col"><span class="ion-trash-a"></span></div>',
		    		headerTemplate: '<div class="xbr-delete-col-header">删除</div>'
		    	}
		    ],
		    data: [],
		    onRegisterApi: __onRegisterApi
        };

        /**
         * 
         * [addRows 点击新增按钮时触发]
         * 
         */
        $scope.addRows = function(){
			if(angular.isUndefined($scope.plcProtocolTypeGridOptions.data) || $scope.plcProtocolTypeGridOptions.data == null){
				$scope.plcProtocolTypeGridOptions.data = [];
			}
			for(var i = 0; i < $scope.config.addRowsNum; i++){
				$scope.bodyConfig.overallWatchChanged = true;
				$scope.plcProtocolTypeGridOptions.data.unshift({
					site: HttpAppService.getSite(),
					hasChanged: true
				});
			}
		};

		/**
         * 
         * [checkAndDeleteSelectedRows 点击删除按钮时触发]
         * 
         */
		$scope.checkAndDeleteSelectedRows = function(){
			var deletedRowIndexs = [];
			var rows = $scope.config.gridApi.selection.getSelectedRows();
			for(var i = 0; i < rows.length; i++){
				var row = rows[i];
				if( !angular.isUndefined(row.handle) && row.handle!=null && row.handle!=""){
					plcService
						.plcProtocolTypeIsUsed(row.plcAccessType)
						.then(function (resultDatas){ 
							if(resultDatas.response === true){ // 不允许删除
								var endIndex = resultDatas.config.url.indexOf('&');
								var plcAccessType = resultDatas.config.url.substring(resultDatas.config.url.lastIndexOf("plc_access_type=")+16, endIndex);
								// row.canDeleted = false;
								for(var i = 0; i < $scope.plcProtocolTypeGridOptions.data.length; i++){
									if($scope.plcProtocolTypeGridOptions.data[i].plcAccessType == plcAccessType){
										$scope.plcProtocolTypeGridOptions.data[i].canDeleted = false;
										break;
									}
								}
								$scope.addAlert('danger', plcAccessType+" 已被引用,不允许删除!");
							}else{
								// var plcAccessType = resultDatas.config.url.substring(resultDatas.config.url.lastIndexOf("plc_access_type=")+16);
								var endIndex = resultDatas.config.url.indexOf('&');
								var plcAccessType = resultDatas.config.url.substring(resultDatas.config.url.lastIndexOf("plc_access_type=")+16, endIndex);
								// row.canDeleted = false;
								for(var i = 0; i < $scope.plcProtocolTypeGridOptions.data.length; i++){
									if($scope.plcProtocolTypeGridOptions.data[i].plcAccessType == plcAccessType){
										$scope.plcProtocolTypeGridOptions.data[i].canDeleted = true;
										break;
									}
								}
								__chekForDeleteRows();
							}
						}, function (errorResultDatas){
							$scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
						});
				}else{
					row.canDeleted = true;
					row.isSelected = true;
				}
			}
			__chekForDeleteRows();
		};
		function __chekForDeleteRows(){
			var canDeletes = true;
			var deletedRowIndexs = [];
			var rows = $scope.plcProtocolTypeGridOptions.data;
			for(var i = 0; i < rows.length; i++){
				var row = rows[i];
				if(row.isSelected){
					if(row.canDeleted !== true){
						canDeletes = false;
					}
				}
			}
			if(canDeletes){
				for (var i = rows.length - 1; i >= 0; i--) {
					var row = rows[i];
					if(row.isSelected){
						$scope.bodyConfig.overallWatchChanged = true;
						row.isDeleted = true;
				        deletedRowIndexs.push(i);
				        $scope.config.deletedRows.push(angular.copy(row));
					}
				};
			}else{
				return;
			}
			var length = deletedRowIndexs.length;
			for(var i = 0; i < deletedRowIndexs.length; i++){
				var index = deletedRowIndexs[i];
				$scope.plcProtocolTypeGridOptions.data.splice(index,1);
				// $scope.plcProtocolTypeGridOptions.data = UtilsService.removeArrayByIndex($scope.plcProtocolTypeGridOptions.data, deletedRowIndexs.pop());
			}
		};

		/**
         * 
         * [saveAllAcitons 点击保存按钮时触发]
         * 
         */
		$scope.saveAllAcitons = function(){ 
			if(!__checkCanSaveDatas()){ return; };
			var paramActions = []; 
			for(var i = 0; i < $scope.plcProtocolTypeGridOptions.data.length; i++){
				var row = $scope.plcProtocolTypeGridOptions.data[i]; 
				if(row.hasChanged){
					if(angular.isUndefined(row.handle)){ //创建新数据 ，若是删除中存在该条数据，则直接忽略该行,并在后面将删除操作也忽略
						if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
							paramActions.push({ 
								"handle": row.handle, 
								"site": row.site, 
								"plcAccessType": row.plcAccessType, 
								"description": row.description,
								"viewActionCode": 'C'  
							});  
						}else{}
					}else{ //修改已存于服务器的行数据 ，若删除列表中有该行，则直接忽略该修改提交，并在后面将删除操作添加进来
						if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
							paramActions.push({
								"handle": row.handle,
								"site": row.site, 
								"plcAccessType": row.plcAccessType, 
								"description": row.description,
								"viewActionCode": 'U',
								"modifiedDateTime": row.modifiedDateTime
							});
						}else{}
					}
				}
			}	
			for(var i = 0; i < $scope.config.deletedRows.length; i++){
				var deleteTempRow = $scope.config.deletedRows[i];
				if(!angular.isUndefined(deleteTempRow.handle)){// 服务器端数据的删除操作,直接添加进去即可
					paramActions.push({
						"handle": deleteTempRow.handle,
						"site": deleteTempRow.site,
						"plcAccessType": deleteTempRow.plcAccessType, 
						"description": deleteTempRow.description,
						"viewActionCode": 'D',
						"modifiedDateTime": deleteTempRow.modifiedDateTime
					});
				}else{ }
			}	
			if(paramActions.length > 0){
				$scope.showBodyModel("正在保存");
				plcService 
					.plcProtocolTypeUpdate(paramActions)
					.then(function (resultDatas){ 
						$scope.hideBodyModel();
						$scope.bodyConfig.overallWatchChanged = false;
						$scope.config.deletedRows = [];
						$scope.addAlert('success', '保存成功!');
						__requestTableDatas();
					},function (resultDatas){ 
						$scope.hideBodyModel();
						$scope.bodyConfig.overallWatchChanged = false;
						$scope.addAlert('danger', '保存失败!');
					});
			}else{ 
				$scope.addAlert('没有修改,无需保存!');
			}	
		}; 	
		/**
         * 
         * [__checkCanSaveDatas 保存前的数据检验]
         * 
         */	
		function __checkCanSaveDatas(){
			for(var i = 0; i < $scope.plcProtocolTypeGridOptions.data.length; i++){
				var item = $scope.plcProtocolTypeGridOptions.data[i];
				if(item.hasChanged){
					var rules = [
						{ field: 'site',  emptyDesc: '第'+(i+1)+'行: 工厂不能为空!' },
						{ field: 'plcAccessType', emptyDesc: '第'+(i+1)+'行: 协议类型不能为空!' },
						{ field: 'description', emptyDesc: '第'+(i+1)+'行: 协议类型描述不能为空!' }
					];
					for(var ruleIndex in rules){
						var rule = rules[ruleIndex];
						if(angular.isUndefined(item[rule.field]) || item[rule.field] == null || item[rule.field] == ""){
							$scope.addAlert('', rule.emptyDesc);
							return false;
						}
					}
					if(item.plcAccessTypeCountIsValid === false){
						$scope.addAlert('', '第'+(i+1)+'行: 协议类型代码必须是6位!');
						return false;
					}
					if(item.plcAccessTypeZhcnIsValid === false){
						$scope.addAlert('', '第'+(i+1)+'行: 协议类型代码不能为汉字!');
						return false;
					}
					if(!angular.isUndefined(item.plcAccessTypeIsValid) && item.plcAccessTypeIsValid!=null && item.plcAccessTypeIsValid == false){
						$scope.addAlert('', '第'+(i+1)+'行: 协议类型已经存在!');
						return false;
					}
				}
			}
			return true;
		}	
		function __deletedRowsExitThisRowByHashKey(rowHashKey){
			for(var n = 0; n < $scope.config.deletedRows; n++){
				if(rowHashKey == $scope.config.deletedRows[n].$$hashKey){
					return true;
				}
			}
			return false;
		}	


        function __onRegisterApi(gridApi){
			$scope.config.gridApi = gridApi;

			gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
				if(newRowCol.col.field == "isDeleted" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
					__checkCanDeleteData(newRowCol);
				}
			});

			$scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
					var promise = $q.defer(); 
					if($scope.config.gridApi.rowEdit){
						$scope.config.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
					}

					if(oldValue == newValue || colDef.field == 'isSelected'){
						promise.resolve();
						return;
					}
					rowEntity.hasChanged = true;      		 
					if(colDef.field === "plcAccessType" || colDef.name === "plcAccessType"){ 
						if(UtilsService.isEmptyString(rowEntity.plcAccessType)){
							return;
						}	
						rowEntity.plcAccessType = rowEntity.plcAccessType.toUpperCase();
						if(UtilsService.isContainZhcn(rowEntity.plcAccessType)){
							promise.reject();
							$scope.addAlert('', '协议类型代码不能为汉字!');
							uiGridValidateService.setInvalid(rowEntity, colDef);
							rowEntity.plcAccessTypeZhcnIsValid = false;
							return;
						}	
						if(rowEntity.plcAccessType.length != 6){
							promise.reject();
							$scope.addAlert('', '协议类型代码必须是6位!');
							uiGridValidateService.setInvalid(rowEntity, colDef);
							rowEntity.plcAccessTypeCountIsValid = false;
							return;
						}   
						rowEntity.plcAccessTypeZhcnIsValid = true;
                    	rowEntity.plcAccessTypeCountIsValid = true;
						plcService 
				  			.plcProtocolTypeExit(newValue) 
				  			.then(function (resultDatas){ 
								if(resultDatas.response){ //已经存在:true
									$scope.addAlert('danger', '该协议类型已经存在!');
									uiGridValidateService.setInvalid(rowEntity, colDef);
									rowEntity.plcAccessTypeIsValid = false;
									promise.reject();
								}else{	//不存在冲突，可以使用
									promise.resolve();
									rowEntity.hasChanged = true;
									rowEntity.plcAccessTypeIsValid = true;
									uiGridValidateService.setValid(rowEntity, colDef);
								}
							},function (resultDatas){ 
								$scope.addAlert(resultDatas.myHttpConfig.statusDesc);
							}); 
					}else{
						promise.resolve();
					}
			}); 

			$scope.config.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
				$scope.config.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
			});

			//选择相关
			$scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
				gridRow.entity.isSelected = gridRow.isSelected;
				var count = $scope.config.gridApi.selection.getSelectedCount();
				$scope.config.btnDisabledDelete = count > 0 ? false : true;
			});
			$scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
				var count = $scope.config.gridApi.selection.getSelectedCount();
				$scope.config.btnDisabledDelete = count > 0 ? false : true;
				for(var i = 0; i < gridRows.length; i++){
					gridRows[i].entity.isSelected = gridRows[i].isSelected;
				}
			});
		}

		function __requestTableDatas(){
			plcService
				.plcProtocolTypeList()
				.then(function (resultDatas){
					$scope.bodyConfig.overallWatchChanged = false;
					$scope.config.btnDisabledSave = false;
					if(resultDatas.response && resultDatas.response.length > 0){
						for(var i = 0; i < resultDatas.response.length; i++){
							resultDatas.response[i].isSelected = false;
						}
						$scope.plcProtocolTypeGridOptions.data = resultDatas.response;

						//if($scope.config.isFirstAddClass)
						//{
						//	$timeout(function(){
						//		/* To hide the blank gap when use selecting and grouping */
						//		$('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
						//		$('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
						//		$scope.config.isFirstAddClass=false;
						//	},0);
						//}
						return;
					}else{
						$scope.plcProtocolTypeGridOptions.data = [];
						$scope.addAlert('', '暂无数据!');
					}
				}, function (resultDatas){
					$scope.config.btnDisabledSave = true;
					$scope.plcProtocolTypeGridOptions.data = [];
					$scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
				}); 
		} 


		$scope.init();

		/**
		 * 
		 * [__deleteRowDataByHashKey 依据hashkey来删除对应的表格行数据]
		 * @param  {[string]} hashKey [hashKey]
		 * 
		 */
		function __deleteRowDataByHashKey(hashKey){
			for(var i = 0; i < $scope.plcProtocolTypeGridOptions.data.length; i++){
				if(hashKey == $scope.plcProtocolTypeGridOptions.data[i].$$hashKey){
					$scope.bodyConfig.overallWatchChanged = true;
					var obj = angular.copy($scope.plcProtocolTypeGridOptions.data[i]);
					$scope.config.deletedRows.push(obj);
					$scope.plcProtocolTypeGridOptions.data = UtilsService.removeArrayByIndex($scope.plcProtocolTypeGridOptions.data, i);
					break; 
				}
			}
		}

		function __checkCanDeleteData(newRowCol){
			var entity = newRowCol.row.entity;
			var handle = entity.handle;
			var hashKey = entity.$$hashKey;
			if(angular.isUndefined(handle) || handle==null || handle==""){ // 处理新增行 
				__deleteRowDataByHashKey(hashKey);
				return;
			}
			// 处理历史行，需要检验是否已经被引用
			plcService
				.plcProtocolTypeIsUsed(entity.plcAccessType)
				.then(function (resultDatas){
					if(resultDatas.response){
						entity.canDeleted = false;
						$scope.addAlert('', '该协议类型已被引用,不可删除!');
					}else{
						entity.canDeleted = true;
						__deleteRowDataByHashKey(hashKey);	
					}					
				}, function (errorResultDatas){
					$scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
				});
		}

    }]);