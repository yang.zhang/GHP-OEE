/**
 * 
 * [PLC 数据类型界面Controller: PlcDataTypeCtrl]
 * 
 */
plcModule.controller('PlcDataTypeCtrl', [ 
	'$scope', '$q', '$interval', '$timeout', '$rootScope',
	'HttpAppService', 'plcService', 'UtilsService', '$state',
	'uiGridConstants', 'gridUtil', 'i18nService', 'uiGridValidateService',
	'uiGridCellNavConstants',
	function ($scope, $q, $interval, $timeout, $rootScope,
		HttpAppService, plcService, UtilsService, $state,
		uiGridConstants, gridUtil, i18nService, uiGridValidateService,
		uiGridCellNavConstants) { 
	
	$scope.config = {
		addRowsNum: 1,
		deletedRows: [],  // 缓存删除且未提交的表格行数据
		btnDisabledDelete: true, // 是否禁用<删除>按钮
		btnDisabledSave: true,   // 是否禁用<保存>按钮

		canEdit: false 			// 当前用户是否对该activity有编辑权限
		
	};  
	$scope.config.canEdit = $scope.modulesRWFlag("#"+$state.$current.url.source);
	
	$scope.init = function(){
		__requestTableDatas();
		// console.log(uiGridConstants.filter);
	};	
	$scope.virtualGridOptions = { 
		paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes, //[5, 10, 15, 20, 50, 100],
		paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize, //10, 
	    useExternalPagination: true, 

	    // enableSorting: true, 
	    showGridFooter: false, 
	    // enableFiltering: true, 
	    modifierKeysToMultiSelectCells: false,
	    columnDefs: [ 
	    	{ 
	    		field: 'handle', name: 'handle', visible: false, minWidth: 0, maxWidth: 0
	    	}, 
	    	{	
	    		field: 'isSelected', name: 'isSelected', displayName: '选择', minWidth: 0, maxWidth: 0,
	    		enableCellEdit:  $scope.config.canEdit, type: 'boolean', visible: false, 
	    		cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex){ return 'xbr-test'; }, 
	    		enableColumnMenu: false, enableColumnMenus: false, enableSorting: false 
	    	}, 
	    	{
	    		name: 'site', displayName: '工厂', minWidth: 100,
	    		enableCellEdit:  $scope.config.canEdit, enableCellEditOnFocus: false, visible: false,
	    		validators: { required: true }, 
	    		cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }, 
	    		cellEditableCondition: function($scope){
	    			return false;
	    			// var row = arguments[0].row;
	    			// var col = arguments[0].col;
	    			// // 非新增不可编辑 、新增行可编辑
	    			// if(!angular.isUndefined(row.entity) && row.entity!=null && !angular.isUndefined(row.entity.handle) && row.entity.handle!=null && row.entity.handle!=""){
	    			// 	return false;
	    			// }else{
	    			// 	return true;
	    			// }
	    		},
	    		editableCellTemplate: 'ui-grid/dropdownEditor',
	    		// cellFilter: 'mapGender',
	    		editDropdownValueLabel: 'site',
	    		editDropdownOptionsArray: [
	    			{ id: 1000, site: '1000' },
					{ id: 2000, site: '2000' },
					{ id: 3000, site: '3000' }
	    		]
	    		// editableCellTemplate: 'andon-ui-grid-tpls/edit-site-select'
	    	}, 
	    	{ 
	    		field: 'category', name: 'category', displayName: 'PLC数据类型代码', minWidth: 150, 
	    		validators: { required: true },  cellClass:'grid-no-editable ad-uppercase',
	    		enableCellEdit:  $scope.config.canEdit, enableCellEditOnFocus: false, 
	    		cellTooltip: function(row, col){  return row.entity[col.colDef.name];  }, 
	    		cellTemplate: 'andon-ui-grid-tpls/dataCategory-categoryCode',
	    		cellEditableCondition: function($scope){
	    			var row = arguments[0].row;
	    			var col = arguments[0].col;
	    			// 非新增不可编辑 、新增行可编辑 
	    			if(!angular.isUndefined(row.entity) && row.entity!=null && !angular.isUndefined(row.entity.handle) && row.entity.handle!=null && row.entity.handle!=""){
	    				return false;
	    			}else{
	    				return true;
	    			}
	    		}
	    	}, 
	    	{
	    		field: 'description', name: 'description', displayName: 'PLC数据类型描述', minWidth: 200,
	    		enableCellEdit:  $scope.config.canEdit, enableCellEditOnFocus: false, validators: {  required: true },
	    		cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
	    		cellTemplate: 'andon-ui-grid-tpls/dataCategory-categoryDesc'
	    	}, 
	    	{ 
	    	 	field: 'isDeleted', name: 'isDeleted', displayName: '删除', width: 60, 
	    		enableCellEdit: false, enableCellEditOnFocus: false, 
	    		visible:  $scope.config.canEdit,
	    		enableColumnMenu: false, enableColumnMenus: false,
	    		cellTooltip: function(row, col){ return "删除该行"; }, 
	    		cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col"><span class="ion-trash-a"></span></div>',
	    		headerTemplate: '<div class="xbr-delete-col-header">删除</div>'
	    	}
	    ],
	    data: [],
	    onRegisterApi: __onRegisterApi
	};

	function __onRegisterApi(gridApi){
		$scope.gridApi = gridApi; 
		gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
			if(newRowCol.col.field == "isDeleted" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
				__checkCanDeleteData(newRowCol);
			}
		});
		// 分页相关 
			// $scope.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){ 
			//   	console.log("刷新界面 第 "+currentPage+" 页,每页 "+pageSize+"个元素!"); 
			//   	// $scope.virtualGridOptions.data.push({
			//   	// 	"name": "自动添加  "+currentPage+"  "+pageSize,
			//       //   "gender": "female", 
			//       //   "company": "汉得"
			//   	// });
			//   	// window.xbrGridUtil = gridUtil;
			//   	return true; 
		// }, $scope);
		gridApi.cellNav.on.viewPortKeyDown($scope, function(row, col){
			console.log("viewPortKeyDown");
		});
		gridApi.cellNav.on.viewPortKeyPress($scope, function(row, col){
			console.log("viewPortKeyPress");
		});
		// 编辑相关 
		$scope.gridApi.edit.on.beginCellEdit($scope,function(rowEntity, colDef){
				// console.log("开始编辑 ..."); 
		});	
		$scope.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
				// console.log("完成编辑 ...");

				var promise = $q.defer();
				if($scope.gridApi.rowEdit){
					$scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
				}
				
				if(oldValue == newValue || colDef.field == 'isSelected'){
					promise.resolve();
					return;
				}
				rowEntity.hasChanged = true;
				$scope.bodyConfig.overallWatchChanged = true;
				if(colDef.field === "category"){
					if(UtilsService.isEmptyString(rowEntity.category)){
						return;
					}
					rowEntity.category = rowEntity.category.toUpperCase();
					// console.log(rowEntity.category);
					if(UtilsService.isContainZhcn(rowEntity.category)){
						promise.reject();
						$scope.addAlert('', '数据类型代码不能包含中文!');
						uiGridValidateService.setInvalid(rowEntity, colDef);
						rowEntity.dataTypeZhcnIsValid = false;
						return;
					}
					if(rowEntity.category.length != 3){
						promise.reject();
						$scope.addAlert('', '数据类型代码必须是3位!');
						uiGridValidateService.setInvalid(rowEntity, colDef);
						rowEntity.dataTypeCountIsValid = false;
						return;
					}
					rowEntity.dataTypeZhcnIsValid = true;
                    rowEntity.dataTypeCountIsValid = true;
                    
					plcService
		  			.requestPlcTypeRowCheck(rowEntity.category) 
		  			.then(function (resultDatas){
						if(resultDatas.response){ //已经存在:true
							console.log("已经存在");
							$scope.addAlert('danger', '该数据类型已经存在!');
							// gridApi.validate.setInvalid(rowEntity, colDef);
							// uiGridValidateService.setInvalid(rowEntity, colDef);
							promise.reject();
							uiGridValidateService.setInvalid(rowEntity, colDef);
							rowEntity.dataTypeIsValid = false;
						}else{	//不存在冲突，可以使用 
							console.log("不存在冲突，可以使用");
							//gridApi.validate.setValid(rowEntity, colDef);
							// uiGridValidateService.setValid(rowEntity, colDef);
							promise.resolve();
							rowEntity.hasChanged = true;
							rowEntity.dataTypeIsValid = true;
							uiGridValidateService.setValid(rowEntity, colDef);
						}
					},function (resultDatas){ //TODO 检验失败
						$scope.addAlert(resultDatas.myHttpConfig.statusDesc);
					}); 
				}else{
					promise.resolve();
				}
		});  
		$scope.gridApi.edit.on.cancelCellEdit($scope,function(rowEntity, colDef){
				console.log("取消编辑 ..."); 
			 	$scope.absHomeConfig.editRowData = null; 
		}); 
		$scope.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
			$scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
		});
		//选择相关
        $scope.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){ 
            gridRow.entity.isSelected = gridRow.isSelected;
            var count = $scope.gridApi.selection.getSelectedCount();
            $scope.config.btnDisabledDelete = count > 0 ? false : true;
        });
        $scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            var count = $scope.gridApi.selection.getSelectedCount();
            $scope.config.btnDisabledDelete = count > 0 ? false : true;
            for(var i = 0; i < gridRows.length; i++){
				gridRows[i].entity.isSelected = gridRows[i].isSelected;
			}
        });
	}

	$scope.init();

	/**
	 * 
	 * [addRows 点击新增时触发的方法]
	 * 
	 */
	$scope.addRows = function(){
		if(angular.isUndefined($scope.virtualGridOptions.data) || $scope.virtualGridOptions.data == null){
			$scope.virtualGridOptions.data = [];
		}
		for(var i = 0; i < $scope.config.addRowsNum; i++){
			$scope.bodyConfig.overallWatchChanged = true;
			$scope.virtualGridOptions.data.unshift({
				site: HttpAppService.getSite(),
				isSelected: false,
				hasChanged: true
			});
		}
	};
	
	/**
	 * 
	 * [checkAndDeleteSelectedRows 点击删除时触发的方法]
	 * 
	 */
	$scope.checkAndDeleteSelectedRows = function(){ 
		var deletedRowIndexs = [];
		//var rows = $scope.virtualGridOptions.data;
		var rows = $scope.gridApi.selection.getSelectedRows();
		for(var i = rows.length-1; i >= 0; i--){
		// for(var i = 0; i < rows.length; i++){
			var row = rows[i];
			// row.isSelected &&handle用来表示唯一性
			if( !angular.isUndefined(row.handle) && row.handle!=null && row.handle!=""){
				plcService
					.requestPlcCategoryIsUsed(row.category)
					.then(function (resultDatas){
						if(resultDatas.response === true){ // 不允许删除
							var endIndex = resultDatas.config.url.indexOf('?');
							endIndex = endIndex< 0 ? resultDatas.config.url.length : endIndex;
							var category = resultDatas.config.url.substring(resultDatas.config.url.lastIndexOf("/")+1, endIndex);
							// row.canDeleted = false;
							for(var i = 0; i < $scope.virtualGridOptions.data.length; i++){
								if($scope.virtualGridOptions.data[i].category == category){
									$scope.virtualGridOptions.data[i].canDeleted = false;
									break;
								}
							}
							$scope.addAlert('danger', category+" 已被引用,不允许删除!");
						}else{
							// var category = resultDatas.config.url.substring(resultDatas.config.url.lastIndexOf("/")+1);
							var endIndex = resultDatas.config.url.indexOf('?');
							endIndex = endIndex< 0 ? resultDatas.config.url.length : endIndex;
							var category = resultDatas.config.url.substring(resultDatas.config.url.lastIndexOf("/")+1, endIndex);
							for(var i = 0; i < $scope.virtualGridOptions.data.length; i++){
								if($scope.virtualGridOptions.data[i].category == category){
									$scope.virtualGridOptions.data[i].canDeleted = true;
									break;
								}
							}
							// row.canDeleted = true;
							__chekForDeleteRows();
						}
					}, function (errorResultDatas){
						$scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
					});
			}else{
				row.canDeleted = true;
				// $scope.virtualGridOptions.data.splice(i, 1);
			}
		}
		__chekForDeleteRows();
	};

	/**
	 * 
	 * [__chekForDeleteRows 删除前准备工作]
	 * 
	 */
	function __chekForDeleteRows(){
		var canDeletes = true;
		var deletedRowIndexs = [];
		var rows = $scope.virtualGridOptions.data;
		for(var i = 0; i < rows.length; i++){
			var row = rows[i];
			if(row.isSelected){
				if(row.canDeleted !== true){
					canDeletes = false;
				}
			}
		} 
		if(canDeletes){
			for (var i = rows.length - 1; i >= 0; i--) {
				var row = rows[i];
				if(row.isSelected){
					row.isDeleted = true;
			        deletedRowIndexs.push(i);
			        $scope.config.deletedRows.push(angular.copy(row));
				}
			};
		}else{ 
			return; 
		} 
		var length = deletedRowIndexs.length;
		for(var i = 0; i < length; i++){
			$scope.virtualGridOptions.data.splice(deletedRowIndexs[i],1);
			// $scope.virtualGridOptions.data = UtilsService.removeArrayByIndex($scope.virtualGridOptions.data, deletedRowIndexs[0]);
		}
		$scope.config.btnDisabledDelete = true;
	};

	/**
	 * 
	 * [__deletedRowsExitThisRowByHashKey 当前表格数据中是否存在该rowHashKey所对应行数据]
	 * @param  {[type]} rowHashKey [row hash key]
	 * 
	 */
	function __deletedRowsExitThisRowByHashKey(rowHashKey){
		for(var n = 0; n < $scope.config.deletedRows; n++){
			if(rowHashKey == $scope.config.deletedRows[n].$$hashKey){
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * [saveAllAcitons 点击保存按钮时触发该函数]
	 * 
	 */
	$scope.saveAllAcitons = function(){
		if(!__checkCanSaveDatas()){
			return;
		};
		var paramActions = []; 
		for(var i = 0; i < $scope.virtualGridOptions.data.length; i++){
			var row = $scope.virtualGridOptions.data[i]; 
			if(row.hasChanged){
				// var viewActionCode = angular.isUndefined(row.handle) ? 'C' : 'U';
				if(angular.isUndefined(row.handle)){ //创建新数据 ，若是删除中存在该条数据，则直接忽略该行,并在后面将删除操作也忽略
					if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
						paramActions.push({
							"handle": row.handle, 
							//"site": HttpAppService.getSite(),
							"site": row.site, 
							"category": row.category,  
							"description": row.description,  
							"viewActionCode": 'C'  
						});  
					}else{}
				}else{ //修改已存于服务器的行数据 ，若删除列表中有该行，则直接忽略该修改提交，并在后面将删除操作添加进来
					if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
						paramActions.push({
							"handle": row.handle, 
							//"site": HttpAppService.getSite(),
							"site": row.site, 
							"category": row.category,  
							"description": row.description,  
							"viewActionCode": 'U',
							"modifiedDateTime": row.modifiedDateTime
						});
					}else{}
				}
			}
		}
		for(var i = 0; i < $scope.config.deletedRows.length; i++){
			var deleteTempRow = $scope.config.deletedRows[i];
			if(!angular.isUndefined(deleteTempRow.handle)){// 服务器端数据的删除操作,直接添加进去即可
				paramActions.push({
					"handle": deleteTempRow.handle,
					"site": deleteTempRow.site,
					"category": deleteTempRow.category, 
					"description": deleteTempRow.description, 
					"viewActionCode": 'D',
					"modifiedDateTime": deleteTempRow.modifiedDateTime
				});
			}else{ }
		} 
		if(paramActions.length > 0){
			$scope.showBodyModel("正在保存");
			plcService 
				.requestPlcCategoriesChange(paramActions)
				.then(function (resultDatas){ 
					$scope.bodyConfig.overallWatchChanged = false;
					$scope.hideBodyModel();
					$scope.addAlert('success', '保存成功!');
					$scope.config.deletedRows = [];
					__requestTableDatas();
				},function (resultDatas){
					$scope.bodyConfig.overallWatchChanged = false;
					$scope.hideBodyModel();
					$scope.addAlert('danger', '保存失败!');
				});
		}else{ 
			$scope.addAlert('没有修改,无需保存!');
		}
	};
	/**
	 * 
	 * [__checkCanSaveDatas 保存前的数据检验]
	 * 
	 */
	function __checkCanSaveDatas(){
		for(var i = 0; i < $scope.virtualGridOptions.data.length; i++){
			var item = $scope.virtualGridOptions.data[i];
			if(item.hasChanged){
				var rules = [
					{ field: 'site',  emptyDesc: '第'+(i+1)+'行: 工厂不能为空!' },
					{ field: 'category', emptyDesc: '第'+(i+1)+'行: 数据类型代码不能为空!' },
					{ field: 'description', emptyDesc: '第'+(i+1)+'行: 数据类型描述不能为空!' }
				];
				for(var ruleIndex in rules){
					var rule = rules[ruleIndex];
					if(angular.isUndefined(item[rule.field]) || item[rule.field] == null || item[rule.field] == ""){
						$scope.addAlert('danger', rule.emptyDesc);
						return false;
					} 
				} 
				// if(!angular.isUndefined(item.dataTypeIsValid) && item.dataTypeIsValid==false ){ // 修改后未通过校验
				if(item.dataTypeIsValid === false){
					$scope.addAlert('danger', '第'+(i+1)+'行: 该数据类型代码已经存在!');
					return false;
				}
				if(item.dataTypeZhcnIsValid === false){
					$scope.addAlert('danger', '第'+(i+1)+'行: 数据类型代码不能包含中文!');
					return false;
				}
				if(item.dataTypeCountIsValid === false){
					$scope.addAlert('danger', '第'+(i+1)+'行: 数据类型代码必须是3位!');
					return false;
				}
			}
		}
		return true;
	}

	function __checkCanDeleteData(newRowCol){
		var entity = newRowCol.row.entity;
		var handle = entity.handle;
		var hashKey = entity.$$hashKey;
		if(angular.isUndefined(handle) || handle==null || handle==""){ // 处理新增行 
			__deleteRowDataByHashKey(hashKey);
			return;
		} 
		// 处理历史行，需要检验是否已经被引用
		plcService
			.requestPlcCategoryIsUsed(entity.category)
			.then(function (resultDatas){
				if(resultDatas.response === true){ // 不允许删除
					entity.canDeleted = false;
					$scope.addAlert('danger', entity.category+" 已被引用,不允许删除!");
				}else{
					entity.canDeleted = true;
					// __chekForDeleteRows();
					__deleteRowDataByHashKey(hashKey);
				}
			}, function (errorResultDatas){
				$scope.addAlert('', errorResultDatas.myHttpConfig.statusDesc);
			});
	}

	function __deleteRowDataByHashKey(hashKey){
		for(var i = 0; i < $scope.virtualGridOptions.data.length; i++){
			if(hashKey == $scope.virtualGridOptions.data[i].$$hashKey){
				$scope.bodyConfig.overallWatchChanged = true;
				$scope.config.deletedRows.push($scope.virtualGridOptions.data[i]);
				$scope.virtualGridOptions.data = UtilsService.removeArrayByIndex($scope.virtualGridOptions.data, i);
				break; 
			}
		}
	}

	function __requestTableDatas(){
		var plc_category = "plc_category"; 
		var url = HttpAppService.URLS.PLC_CATEGORY_LIST
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{PLC_CATEGORY}#", plc_category); 
		HttpAppService.get({
			url: url 
		}).then(function (resultDatas){  
			$scope.config.btnDisabledSave = false;
			$scope.bodyConfig.overallWatchChanged = false;
			// console.log("Success: isTimeout: "+JSON.stringify(resultDatas));
			if(resultDatas.response && resultDatas.response.length > 0){
				// $scope.config.plcDataTypeGridOptions.data = resultDatas.response;
				for(var i = 0; i < resultDatas.response.length; i++){ 
					resultDatas.response[i].isSelected = false; 
				}
				$scope.virtualGridOptions.data = resultDatas.response;
				return;
			}else{
                $scope.addAlert('', '暂无数据!');
			}
			$scope.virtualGridOptions.data = [];
		}, function (resultDatas){ 
			$scope.config.btnDisabledSave = true;
			$scope.virtualGridOptions.data = [];
			$scope.addAlert('', resultDatas.myHttpConfig.statusDesc);
		});
	} 

}]);