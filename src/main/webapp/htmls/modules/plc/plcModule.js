plcModule.service('plcService', [ 
	'HttpAppService','UtilsService',
	function (HttpAppService,UtilsService) {

	return { 
		// 下拉值接口：生产区域、接线、设备类型、设备编码、报警备注
		dataWorkCenters: requestDataWorkCenters,
		dataLines: requestDataLines,
		dataResourceTypes: requestDataResourceTypes,
		newDataResourceTypes: requestNewDataResourceTypes,
		dataResourceCodes: requestDataResourceCodes,
		dataPlcResourceCodes:requestDataPlcResourceCodes,
		dataAlarmRemarkWithDescription:dataAlarmRemarkWithDescription,

		// plc数据类型管理接口   
		requestPlcTypeRowCheck: requestPlcTypeRowCheck, 
		requestPlcCategoryList: requestPlcCategoryList, 
		requestPlcCategoriesChange: requestPlcCategoriesChange, 
		requestPlcCategoryIsUsed: requestPlcCategoryIsUsed, 
		// plc数据项管理接口    
		plcDataItemList: requestPlcDataItemList,  
		plcDataItemListAll: requestPlcDataItemListAll,
		plcDataItemCount: requestPlcDataItemCount,
		plcDataItemIsUsed: requestPlcDataItemIsUsed,  
		plcDataItemExist: requestPlcDataItemExist,  
		plcDataItemChange: requestPlcDataItemChange, 
		// PLC 协议类型描述   
		plcProtocolTypeList: requestPlcProtocolTypeList, 
		plcProtocolTypeIsUsed: requestPlcProtocolTypeIsUsed, 
		plcProtocolTypeExit: requestPlcProtocolTypeExit, 
		plcProtocolTypeUpdate: requestPlcProtocolTypeUpdate, 

		// PLC 地址信息维护接口 
		// plcAddressInit: requestPlcAddressInit,
		plcAddressList: requestPlcAddressList,
		plcAddressCount: requestPlcAddressCount,
		// plcAddressExistResource: requestPlcAddressExistResource,
		// plcAddressExistObject: requestPlcAddressExistObject,
		plcAddressExistPlcObject: requestPlcAddressExistPlcObject,
		plcAddressExistResourcePlc: requestPlcAddressExistResourcePlc,
		plcAddressExistResourcePlcObj: requestPlcAddressExistResourcePlcObj,
		plcAddressChange: requestPlcAddressChange,
		// PLC 上位机网络维护  
		plcNetInfoList: requestPlcNetInfoList,
		plcNetInfoCount: requestPlcNetInfoCount,
		plcNetInfoChange: requestPlcNetInfoChange,
		plcNetInfoIsUsed: requestPlcNetInfoIsUsed,
		plcNetInfoExistPcIp:requestPlcNetInfoExistPcIp,
		requestDataPlcAlarmRemark:requestDataPlcAlarmRemark,
		plcRequestDataPlcAlarmRemark:plcRequestDataPlcAlarmRemark,
		updateDataPlcAlarmRemark:updateDataPlcAlarmRemark,
		plcUpdateDataPlcAlarmRemark:plcUpdateDataPlcAlarmRemark,
		requestStatementAlarmRemark:requestStatementAlarmRemark,
		plcRequestStatementAlarmRemark:plcRequestStatementAlarmRemark,
		getIp:getIp
	};
	// plc 地址信息维护接口： 
	function requestPlcAddressList(resource, plcCategory, pageIndex, pageCount){ 
		var url = HttpAppService.URLS.PLC_ADDRESS_LIST
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{RESOURCE}#", resource)
					.replace("#{PLC_CATEGORY}#", plcCategory)
					.replace("#{PAGEINDEX}#", pageIndex)
					.replace("#{PAGECOUNT}#", pageCount)
					;
		return HttpAppService.get({
			url: url
		});
	}; 
	function requestPlcAddressCount(resource, plcCategory){
		var url = HttpAppService.URLS.PLC_ADDRESS_COUNT
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{RESOURCE}#", resource)
					.replace("#{PLC_CATEGORY}#", plcCategory)
					;
		return HttpAppService.get({
			url: url
		});
	}; 
	function getIp(){
		var url = HttpAppService.URLS.GET_IP;
		return HttpAppService.get({
			url: url
		});
	}; 

	// 修改前验证: 
	// {
	// 	plcAddress:
	// 	plcValue:
	// 	resourceBo:
	// 	plcObject:
	// }
	function requestPlcAddressExistPlcObject(params){	
		var url = HttpAppService.URLS.PLC_ADDRESS_EXIST_PLC_OBJECT
						.replace("#{SITE}#", HttpAppService.getSite())
						.replace("#{PLC_ADDRESS}#", params.plcAddress)
						.replace("#{PLC_VALUE}#", params.plcValue)
						.replace("#{RESOURCE_BO}#", params.resourceBo)
						.replace("#{PLC_OBJECT}#", params.plcObject)
						;
		return HttpAppService.get({
			url: url
		});
	}; 	
	// 新增前验证:2个
	// {
	// 	plcAddress:
	// 	plcValue:
	// 	resourceBo:
	// }
	function requestPlcAddressExistResourcePlc(params){ 
		var url = HttpAppService.URLS.PLC_ADDRESS_EXIST_RESOURCE_PLC
						.replace("#{SITE}#", HttpAppService.getSite())
						.replace("#{PLC_ADDRESS}#", params.plcAddress)
						.replace("#{PLC_VALUE}#", params.plcValue)
						.replace("#{RESOURCE_BO}#", params.resourceBo)
						// .replace("#{PLC_OBJECT}#", params.plcObject);
						;
		return HttpAppService.get({
			url: url
		});
	}; 
	// {
	// 	plcAddress: 
	// 	resourceBo:
	// }
	function requestPlcAddressExistResourcePlcObj(params){
		var url = HttpAppService.URLS.PLC_ADDRESS_EXIST_RESOURCE_PLC_OBJ
						.replace("#{SITE}#", HttpAppService.getSite())
						.replace("#{PLC_OBJECT}#", params.plcObject)
						.replace("#{RESOURCE_BO}#", params.resourceBo);
		return HttpAppService.get({
			url: url
		});
	};
	function requestPlcAddressChange(paramActions){ 
		var url = HttpAppService.URLS.PLC_ADDRESS_CHANGE;
		return HttpAppService.post({
			url: url,
			paras: paramActions
		});
	}; 
	
	


	// 下拉值接口
	function requestDataWorkCenters(){
		var url = HttpAppService.URLS.XLZ_WORK_CENTERS
					.replace("#{SITE}#", HttpAppService.getSite());
		return HttpAppService.get({
			url: url
		});
	}
	function requestDataLines(workCenter){
		var url = HttpAppService.URLS.XLZ_LINES
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{WORK_CENTER}#", workCenter);
		return HttpAppService.get({
			url: url
		});
	} 
	function requestDataResourceTypes(){
		var url = HttpAppService.URLS.XLZ_RESOURCE_TYPES 
					.replace("#{SITE}#", HttpAppService.getSite()); 
		return HttpAppService.get({ 
			url: url 
		}); 
	}
	//requestNewDataResourceTypes with choose工厂
	function requestNewDataResourceTypes(sites){
		var url = HttpAppService.URLS.XLZ_RESOURCE_TYPES_NEW
			.replace("#{SITE}#", sites ? sites:HttpAppService.getSite());
		return HttpAppService.get({
			url: url
		});
	}

	function requestDataResourceCodes(workCenter, line, resourceType){ 
		// var resourceTypeX = angular.isUndefined(resourceType) || resourceType==null ? "" : resourceType; 
		var url = HttpAppService.URLS.XLZ_RESOURCE_CODES 
					.replace("#{SITE}#", HttpAppService.getSite()) 
					.replace("#{WORK_CENTER}#", workCenter) 
					.replace("#{LINE}#", line) 
					.replace("#{RESOURCE_TYPE}#", resourceType) 
					; 
		// var params = { 
		// 	site: HttpAppService.getSite(),
		// 	workCenter: workCenter, 
		// 	line: line, 
		// 	resourceType: resourceType 
		// }; 
		return HttpAppService.get({ 
			url: url
		}); 
	} 

	function requestDataPlcResourceCodes(workCenter, line, resourceType){ 
		var url = HttpAppService.URLS.XLZ_PLC_RESOURCE_CODES
					.replace("#{SITE}#", HttpAppService.getSite()) 
					.replace("#{WORK_CENTER}#", workCenter) 
					.replace("#{LINE}#", line) 
					.replace("#{RESOURCE_TYPE}#", resourceType) 
					; 
		return HttpAppService.get({ 
			url: url
		}); 
	}

	//设备报警明细报表 modal层调用
	function dataAlarmRemarkWithDescription(description) {
		var url = HttpAppService.URLS.PLC_ALARM_REMARK_QUERY_DESCRIPTION
				.replace("#{SITE}#", HttpAppService.getSite())
				.replace("#{DESCRIPTION}#", description)
			;
		return HttpAppService.get({
			url: url
		});
	}

	// PLC 上位机网络维护
	function requestPlcNetInfoList(params){
		/**
		 * {
				"site" : "1000",
				"pageIndex" : 1,
				"pageCount" : 1,
				"resource" : [],
				"resourceType" : "RESOURCE_TYPE_T1",
				"workCenter" : "WORK_CENTER_T1",
				"line" : "WORK_CENTER_T5"
			}
		*/ 
		// var site = HttpAppService.getSite();
		// var defaults = {
			// "site" : site,
			// "pageIndex" : 1,
			// "pageCount" : 10,
			// "resource" : [],
			// "resourceType" : "RESOURCE_TYPE_T1",
			// "workCenter" : "WORK_CENTER_T1",
			// "line" : "WORK_CENTER_T5"
		// }; 
		// var finalParams = angular.copy(defaults);
		// angular.extend(finalParams, paramActions);
		var url = HttpAppService.URLS.PLC_NETINFO_LIST
						.replace("#{SITE}#", HttpAppService.getSite())
						.replace("#{PAGE_INDEX}#", params.pageIndex)
						.replace("#{PAGE_COUNT}#", params.pageCount)
						.replace("#{RESOURCE_S}#", params.resource)
						.replace("#{RESOURCE_TYPE}#", params.resourceType)
						.replace("#{WORK_CENTER}#", params.workCenter)
						.replace("#{LINE}#", params.line)
						;
		return HttpAppService.get({
			url: url
		});
	}

	function requestPlcNetInfoCount(params){
		var resourceSStr = "";
		if(angular.isUndefined(params.resourceS) || params.resourceS == null || params.resourceS.length ==0){
			resourceSStr = "";
		}else{
			for(var i = 0; i < params.resourceS.length; i++){
				if(i == 0){
					resourceSStr += params.resourceS[i];
				}else{
					resourceSStr += ","+params.resourceS[i];
				}
			}
		}
		var url = HttpAppService.URLS.PLC_NETINFO_COUNT
						.replace("#{SITE}#", HttpAppService.getSite())
						.replace("#{RESOURCE_S}#", resourceSStr)
						.replace("#{RESOURCE_TYPE}#", params.resourceType)
						.replace("#{WORK_CENTER}#", params.workCenter)
						.replace("#{LINE}#", params.line)
						;
		return HttpAppService.get({
			url: url
		});
	}
	function requestPlcNetInfoChange(paramActions){
		var url = HttpAppService.URLS.PLC_NETINFO_CHANGE;
		return HttpAppService.post({
			url: url,
			paras : paramActions
		});
	} 
	function requestPlcNetInfoIsUsed(plcAccessType){
		var url = HttpAppService.URLS.PLC_NETINFO_ISUSED
					.replace("#{SITE}#", HttpAppService.getSite());
		return HttpAppService.get({
			url: url
		});
	};
	function requestPlcNetInfoExistPcIp(param){
		var url = HttpAppService.URLS.PLC_NETINFO_EXIST_PCIP
			.replace("#{SITE}#", HttpAppService.getSite())
			.replace("#{PC_IP}#",param);
		return HttpAppService.get({
			url: url
		});
	};
	

	// plc数据类型管理接口 
	function requestPlcTypeRowCheck(plc_category){
		// var plc_category = "plc_category";
		// if(!UtilsService.isEmptyString(plcCategory)){
		// 	plcCategory = plcCategory.toUpperCase();
		// }
		var url = HttpAppService.URLS.PLC_CATEGORY_EXIT
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{PLC_CATEGORY}#", plc_category);
		return HttpAppService.get({
			url: url
		});
	};

	function requestPlcCategoryList(){ 
		var plc_category = "plc_category";
		var url = HttpAppService.URLS.PLC_CATEGORY_LIST
					.replace("#{SITE}#", HttpAppService.getSite());
		return HttpAppService.get({
			url: url
		});
	};

	function requestPlcCategoriesChange(paramActions){
		var plc_category = "plc_category";
		var url = HttpAppService.URLS.PLC_CATEGORIES_CHANGE
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{PLC_CATEGORY}#", plc_category);
		return HttpAppService.post({
			url: url,
			paras : paramActions
		});
	};
	
	function requestPlcCategoryIsUsed(plc_category){
		// var plc_category = "plc_category";
		var url = HttpAppService.URLS.PLC_CATEGORY_ISUSED
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{PLC_CATEGORY}#", plc_category);
		return HttpAppService.get({
			url: url
		});
	};
	
	// plc数据项管理接口 
	function requestPlcDataItemList(plc_category, pageIndex, pageCount){
		// var plc_category = "plc_category";  
		var url = HttpAppService.URLS.PLC_DATAITEM_LIST
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{PLC_CATEGORY}#", plc_category)
					.replace("#{INDEX}#", pageIndex)
					.replace("#{COUNT}#", pageCount)
					; 
		return HttpAppService.get({
			url: url
		}); 
	};
	function requestPlcDataItemListAll(plc_category){
		// var plc_category = "plc_category";  
		var url = HttpAppService.URLS.PLC_DATAITEM_LIST_ALL
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{PLC_CATEGORY}#", plc_category)
					; 
		return HttpAppService.get({
			url: url
		}); 
	};  
	function requestPlcDataItemCount(plc_category){
		var url = HttpAppService.URLS.PLC_DATAITEM_COUNT 
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{PLC_CATEGORY}#", plc_category)
					;
		return HttpAppService.get({
			url: url
		}); 
	}
	function requestPlcDataItemIsUsed(plc_object){
		// var plc_object = "plc_object";
		var url = HttpAppService.URLS.PLC_DATAITEM_ISUSED
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{PLC_OBJECT}#", plc_object);
		return HttpAppService.get({
			url: url
		});
	};
	function requestPlcDataItemExist(plc_object){
		var url = HttpAppService.URLS.PLC_DATAITEM_EXIST
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{PLC_OBJECT}#", plc_object);
		return HttpAppService.get({
			url: url
		});
	};
	function requestPlcDataItemChange(paramActions){
		// var plc_category = "plc_category";
		var url = HttpAppService.URLS.PLC_DATAITEM_CHANGE;
					// .replace("#{SITE}#", HttpAppService.getSite())
					// .replace("#{PLC_CATEGORY}#", plc_category);
		return HttpAppService.post({
			url: url,
			paras : paramActions
		});
	};

	// PLC 协议类型描述 
	function requestPlcProtocolTypeList(){
		var url = HttpAppService.URLS.PLC_PROTOCOLTYPE_LIST
					.replace("#{SITE}#", HttpAppService.getSite())
					// .replace("#{PLC_CATEGORY}#", plc_category)
					;
		return HttpAppService.get({
			url: url
		}); 
	};
	function requestPlcProtocolTypeIsUsed(accesTypeCode){
		var url = HttpAppService.URLS.PLC_PROTOCOLTYPE_ISUSED
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{PLC_ACCESS_TYPE}#", accesTypeCode)
					;
		return HttpAppService.get({
			url: url
		});
	};
	function requestPlcProtocolTypeExit(protocolType){
		var url = HttpAppService.URLS.PLC_PROTOCOLTYPE_EXIT
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{PLC_ACCESS_TYPE}#", protocolType);
		return HttpAppService.get({
			url: url
		});
	};
	function requestPlcProtocolTypeUpdate(paramActions){
		var url = HttpAppService.URLS.PLC_PROTOCOLTYPE_CHANGE
					.replace("#{SITE}#", HttpAppService.getSite())
					;
		return HttpAppService.post({
			url: url,
			paras : paramActions
		});
	};
	//报警备注看板列表
	function requestDataPlcAlarmRemark(resrce){
		var url = HttpAppService.URLS.PLC_ALARM_REMARK_REPORT
			.replace("#{SITE}#", HttpAppService.getSite())
			.replace("#{PLC_RESRCE}#", resrce);
//			.replace("#{PLC_CURR_PAGE}#", curr_page)
//			.replace("#{PLC_PAGE_SIZE}#", page_size);
		UtilsService.log("url----"+url);
		return HttpAppService.get({
			url: url
		});
	};

	//报警备注看板列表
	function plcRequestDataPlcAlarmRemark(resrce){
		var url = HttpAppService.URLS.PLC_NEW_ALARM_REMARK_REPORT
			.replace("#{SITE}#", HttpAppService.getSite())
			.replace("#{plcIp}#",window.localStorage.plcIp)
			.replace("#{PLC_RESRCE}#", resrce);
//			.replace("#{PLC_CURR_PAGE}#", curr_page)
//			.replace("#{PLC_PAGE_SIZE}#", page_size);
		UtilsService.log("url----"+url);
		return HttpAppService.get({
			url: url
		});
	};

	//报警备注看板获取设备号
	function requestStatementAlarmRemark(ip){
		var url = HttpAppService.URLS.PLC_ALARM_REMARK_REPORT_STATEMENT
			.replace("#{SITE}#", HttpAppService.getSite())
			.replace("#{ALARM_IP}#",ip);
		UtilsService.log("url----"+url+"ip----------"+ip);
		return HttpAppService.get({
			url: url
		});
	};

	//报警备注看板获取设备号
	function plcRequestStatementAlarmRemark(ip){
		var url = HttpAppService.URLS.PLC_NEW_ALARM_REMARK_REPORT_STATEMENT
			.replace("#{SITE}#", HttpAppService.getSite())
			.replace("#{plcIp}#",window.localStorage.plcIp)
			.replace("#{ALARM_IP}#",ip);
		UtilsService.log("url----"+url+"ip----------"+ip);
		return HttpAppService.get({
			url: url
		});
	};

	//报警备注看板列表－－－修改备注
	function updateDataPlcAlarmRemark(resrce,date_time,description,alert_comment,comment_code){
		var url = HttpAppService.URLS.PLC_ALARM_REMARK_UPDATE
			.replace("#{SITE}#", HttpAppService.getSite())
			.replace("#{PLC_RESRCE}#", resrce)
			.replace("#{PLC_DATE_TIME}#", date_time)
			.replace("#{PLC_DESCRIPTION}#", description)
			.replace("#{PLC_ALERT_COMMENT}#", alert_comment)
			.replace("#{PLC_COMMENT_CODE}#", comment_code);
		UtilsService.log("url----"+url);
		return HttpAppService.get({
			url: url
		});
	};

	function plcUpdateDataPlcAlarmRemark(resrce,date_time,description,alert_comment,comment_code){
		var url = HttpAppService.URLS.PLC_NEW_ALARM_REMARK_UPDATE
			.replace("#{SITE}#", HttpAppService.getSite())
			.replace("#{PLC_RESRCE}#", resrce)
			.replace("#{PLC_DATE_TIME}#", date_time)
			.replace("#{PLC_DESCRIPTION}#", description)
			.replace("#{PLC_ALERT_COMMENT}#", alert_comment)
			.replace("#{plcIp}#",window.localStorage.plcIp)
			.replace("#{PLC_COMMENT_CODE}#", comment_code);
		UtilsService.log("url----"+url);
		return HttpAppService.get({
			url: url
		});
	};

}]);

plcModule
	.run([
		'$templateCache',
		function ($templateCache){ 

		$templateCache.put('andon-ui-grid-tpls/dataCategory-site', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.site\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		); 
		$templateCache.put('andon-ui-grid-tpls/dataCategory-categoryCode', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.category\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		); 
		$templateCache.put('andon-ui-grid-tpls/dataCategory-categoryDesc', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.description\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		); 
		// $templateCache.put('andon-ui-grid-tpls/edit-site-select', 
		// 	""
		// );
		 $templateCache.put('andon-ui-grid-tpls/dropdownEditor', 
		    "<div><form name=\"inputForm\"><select ng-class=\"'colt' + col.uid\" ui-grid-edit-dropdown ng-model=\"MODEL_COL_FIELD\" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] CUSTOM_FILTERS for field in editDropdownOptionsArray\"></select></form></div>"
		 );

		$templateCache.put('andon-ui-grid-tpls/accessType-typeCode',
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.plcAccessType\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);


		$templateCache.put('andon-ui-grid-tpls/dataItem-plcObject', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.plcObject\"></div>"
		); 

		$templateCache.put('andon-ui-grid-tpls/network-plcIp', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.plcIp\" ng-class=\"{'grid-invalid':(row.entity.PlcIpIsInValid)}\"></div>"
		);
			$templateCache.put('andon-ui-grid-tpls/network-pcIp',
				"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.pcIp\" ng-class=\"{'grid-invalid':(row.entity.PcIpIsInValid || row.entity.PcIpIsExist)}\"></div>"
			);
		$templateCache.put('andon-ui-grid-tpls/network-plcPort', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.plcPort\" ng-class=\"{'grid-invalid':(row.entity.PlcPortIsInValid)}\"></div>"
		); 
		



		
		$templateCache.put('andon-ui-grid-tpls/addInfo-category', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.category\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);
		$templateCache.put('andon-ui-grid-tpls/addInfo-Description', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.description\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);
		$templateCache.put('andon-ui-grid-tpls/addInfo-DeviceNumber', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.resrce\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);
		$templateCache.put('andon-ui-grid-tpls/addInfo-LogPath', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity['logPath']\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);
		$templateCache.put('andon-ui-grid-tpls/addInfo-PLCAddress', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity['plcAddress']\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		); 
		$templateCache.put('andon-ui-grid-tpls/addInfo-Object', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity['plcObject']\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);  
		$templateCache.put('andon-ui-grid-tpls/addInfo-Regis', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity['plcValue']\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);
		// $templateCache.put('ui-grid/addinfopart-pagination',
		// 	"<div role=\"contentinfo\" class=\"ui-grid-pager-panel\" ui-grid-pager ng-show=\"grid.options.enablePaginationControls\"><div role=\"navigation\" class=\"ui-grid-pager-container\"><div role=\"menubar\" class=\"ui-grid-pager-control\"><button type=\"button\" role=\"menuitem\" class=\"ui-grid-pager-first\" ui-grid-one-bind-title=\"aria.pageToFirst\" ui-grid-one-bind-aria-label=\"aria.pageToFirst\" ng-click=\"pageFirstPageClick()\" ng-disabled=\"cantPageBackward()\"><div ng-class=\"grid.isRTL() ? 'last-triangle' : 'first-triangle'\"><div ng-class=\"grid.isRTL() ? 'last-bar-rtl' : 'first-bar'\"></div></div></button> <button type=\"button\" role=\"menuitem\" class=\"ui-grid-pager-previous\" ui-grid-one-bind-title=\"aria.pageBack\" ui-grid-one-bind-aria-label=\"aria.pageBack\" ng-click=\"pagePreviousPageClick()\" ng-disabled=\"cantPageBackward()\"><div ng-class=\"grid.isRTL() ? 'last-triangle prev-triangle' : 'first-triangle prev-triangle'\"></div></button> <input type=\"number\" ui-grid-one-bind-title=\"aria.pageSelected\" ui-grid-one-bind-aria-label=\"aria.pageSelected\" class=\"ui-grid-pager-control-input\" ng-model=\"grid.options.paginationCurrentPage\" min=\"1\" max=\"{{ paginationApi.getTotalPages() }}\" required> <span class=\"ui-grid-pager-max-pages-number\" ng-show=\"paginationApi.getTotalPages() > 0\"><abbr ui-grid-one-bind-title=\"paginationOf\">/</abbr> {{ paginationApi.getTotalPages() }}</span> <button type=\"button\" role=\"menuitem\" class=\"ui-grid-pager-next\" ui-grid-one-bind-title=\"aria.pageForward\" ui-grid-one-bind-aria-label=\"aria.pageForward\" ng-click=\"pageNextPageClick()\" ng-disabled=\"cantPageForward()\"><div ng-class=\"grid.isRTL() ? 'first-triangle next-triangle' : 'last-triangle next-triangle'\"></div></button> <button type=\"button\" role=\"menuitem\" class=\"ui-grid-pager-last\" ui-grid-one-bind-title=\"aria.pageToLast\" ui-grid-one-bind-aria-label=\"aria.pageToLast\" ng-click=\"pageLastPageClick()\" ng-disabled=\"cantPageToLast()\"><div ng-class=\"grid.isRTL() ? 'first-triangle' : 'last-triangle'\"><div ng-class=\"grid.isRTL() ? 'first-bar-rtl' : 'last-bar'\"></div></div></button></div><div class=\"ui-grid-pager-row-count-picker\" ng-if=\"grid.options.paginationPageSizes.length > 1\"><select ui-grid-one-bind-aria-labelledby-grid=\"'items-per-page-label'\" ng-model=\"grid.options.paginationPageSize\" ng-options=\"o as o for o in grid.options.paginationPageSizes\"></select><span ui-grid-one-bind-id-grid=\"'items-per-page-label'\" class=\"ui-grid-pager-row-count-label\">&nbsp;{{sizesLabel}}</span></div><span ng-if=\"grid.options.paginationPageSizes.length <= 1\" class=\"ui-grid-pager-row-count-label\">{{grid.options.paginationPageSize}}&nbsp;{{sizesLabel}}</span></div><div class=\"ui-grid-pager-count-container\"><div class=\"ui-grid-pager-count\"><span ng-show=\"grid.options.totalItems > 0\">{{showingLow}} <abbr ui-grid-one-bind-title=\"paginationThrough\">-</abbr> {{showingHigh}} {{paginationOf}} {{grid.options.totalItems}} {{totalItemsLabel}}</span></div></div></div>"
		// );
		
		$templateCache.put('andon-ui-grid-tpls/addinfopart-plcAddress', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.plcAddress\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		); 
		$templateCache.put('andon-ui-grid-tpls/addinfopart-plcValue', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.plcValue\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		); 
		$templateCache.put('andon-ui-grid-tpls/addinfopart-plcObject', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.plcObjectDes\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);
		$templateCache.put('andon-ui-grid-tpls/addinfopart-plcRemark',
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.alertComment\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);
		$templateCache.put('andon-ui-grid-tpls/addinfopart-plcRealTime',
			"<div><div  title=\"{{col.cellTooltip(row,col)}}\" style='white-space:nowrap;overflow:hidden;text-overflow:ellipsis;'>xugegegegegeggegeg</div><div ng-bind=\"row.entity.intAlertCount\"></div></div>"
		);
		$templateCache.put('andon-ui-grid-tpls/addinfopart-plcShowPen',
			"<div class=\"ui-grid-cell-contents ad-delete-col\" ng-if='row.entity.QMART == \"Z6\"'><span class=\"glyphicon glyphicon-pencil\"></span></div>"
		);


}]);














































