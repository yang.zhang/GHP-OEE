theoryPPMModule.controller('queryManagerCtrl',[
    '$scope', '$http', 'HttpAppService', '$timeout', 'plcService',
    'uiGridConstants','$uibModal', 'theoryPPMService', 'UtilsService',
    '$filter', '$q','$state','yieldMaintainService',
    function ($scope, $http, HttpAppService, $timeout, plcService,
              uiGridConstants,$uibModal, theoryPPMService, UtilsService,
              $filter, $q,$state,yieldMaintainService) {
        $scope.config = {
            // editRw:false, 
            queryManDate: UtilsService.serverFommateDateShow(new Date()),

            deletedRows: [],

            gridApi: null,
            $workCenterSelect: null,
            $lineSelect: null,
            $deviceTypeSelect: null,
            $deviceNumSelect: null, 

            btnDisabledQuery: true,
            btnDisabledDelete: true,
            btnDisabledAdd: true,
            btnDisabledEdit: true,
            btnDisabledSave: true,
            btnOkDisabled:true,

            ppmTheory: 1,
            defaultSelectItems: [
                { descriptionNew:"", description:"", workCenter: null,  resourceType: null, },
                { descriptionNew:"--- 请选择 ---", description:"--- 请选择 ---", workCenter: null,  resourceType: null}
            ],
            defaultSelectItem: { descriptionNew:"--- 请选择 ---", description:"--- 请选择 ---", workCenter: null },
            workCenters: [], 
            currentWorkCenter: null,
            lines: [],
            currentLine: [],
            currentLineDesc : '',
            deviceTypes: [],
            currentDeviceType: null,
            deviceNums: [],
            currentDeviceNum: '',

            currentMaterialCode: null,
            showSelectLine : true,
            checkAllStatus:false,
            ppmNum:""

        };

        //$scope.config.editRw = $scope.modulesRWFlag("#"+$state.$current.url.source);

        //$scope.modulesRWFlag("#/andon/TheoryPPM/QueryManager")
        //    .then(function(item){
        //        console.log(item);
        //        $scope.config.editRw = item.write;
        //    }, function(){
        //        //console.log();
        //    });
        //拉线多选帮助
        $scope.toWireLineHelp = function () {
            if($scope.config.currentWorkCenter == null){
                $scope.addAlert('','请先选择生产区域');
                return;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/lineSelectModal.html',
                controller: 'LineSelectMoreCtrl',
                size: 'lg',
                backdrop: 'false',
                scope: $scope,
                openedClass: 'flex-center-parent',
                resolve: {
                    selectLineModal: function () {
                        return {workLine: $scope.config.lines};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                console.log(selectedItem);
                $scope.config.currentLine = selectedItem.lineDatas;
                $scope.config.currentLineDesc = selectedItem.lineName;
                if($scope.config.currentLine.length >0){
                    $scope.config.showSelectLine = false;
                }
                $scope.lineChanged();
            }, function () {
            });
        };
        $scope.deleteAll = function(item){
            if(item == 'line'){
                $scope.config.currentLine = [];
                $scope.config.currentLineDesc = [];
                $scope.config.currentDeviceNums = [];
                $scope.config.currentDeviceNum = null;
                $scope.config.showSelectLine = true;
            }
        }
        $scope.gridPPMQueryManager = {
            enablePagination: true,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,
            paginationCurrentPage: 1,
            // paginationTemplate: 'ui-grid/pagination',
            totalItems: 0,

            useExternalPagination: true,

            showGridFooter: false,
            modifierKeysToMultiSelectCells: false,
            data: [],
            onRegisterApi: __onRegisterApi,

            columnDefs: [
                {
                    field: 'handle', name: 'handle', visible: false
                },
                {
                    field: 'resrce', name: 'resrce', displayName: '设备编码', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'asset', name: 'asset', displayName: '资产号', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'resrceDes', name: 'resrceDes', displayName: '设备名称', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    // cellTemplate: 'andon-ui-grid-tpls/network-plcIp',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'model', name: 'model', displayName: 'Model', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    // cellTemplate: 'andon-ui-grid-tpls/network-plcPort',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {   
                    field: 'item', name: 'item', displayName: '物料编码', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true }, cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'ppmTheory', name: 'ppmTheory', displayName: '理论PPM', visible: true, minWidth: 100,
                    enableCellEdit: $scope.ppmConfig.canEdit, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    validators: { required: true },cellClass:'text-align-right', // grid-no-editable
                    cellTemplate: 'andon-ui-grid-tpls/ppm-PPM', type:'number',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row,
                            col = arguments[0].col;
                        var endDate = new Date(row.entity.endDate);
                        var nowDate = new Date();
                        if(endDate <= nowDate){
                            UtilsService.log("返回false");
                            return false;
                        }
                        // if(angular.isUndefined(row.entity.targetDate) || row.entity.targetDate == null || row.entity.targetDate == "" || row.entity.targetDate=="*"){
                        //     return false;
                        // }
                        return true;
                    }
                },
                {
                    field: 'startDateParsed', name: 'startDateParsed', displayName: '有效开始日期', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }, cellClass:'grid-no-editable',
                },
                {
                    field: 'endDateParsed', name: 'endDateParsed', displayName: '有效结束日期', visible: true, minWidth: 100,
                    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }, cellClass:'grid-no-editable',
                },
                {
                    field: 'isDeleted', name: 'isDeleted', displayName: '删除', width: 40,
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    visible: $scope.config.canEdit,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return "删除该行"; },
                    cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col"><span class="ion-trash-a" ng-if="row.entity.isCreate"></span></div>',
                    headerTemplate: '<div class="xbr-delete-col-header">删除</div>'
                }
            ]
        };

        $scope.modalQueryManager = function(){

            var modelNeedDatas = __initNeedNewResourceDatas();
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'ModalQueryManager.html',
                controller: 'ModalQueryManagerCtrl',
                size: 'lg',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope : $scope,
                resolve: {
                    PPMModelData: function(){
                        return modelNeedDatas;
                    }
                }
            });
            modalInstance.result.then(function (datas) {
                $scope.bodyConfig.overallWatchChanged = true;

                var rows = datas.gridSelectionRows;
                for(var i = 0; i < rows.length; i++){
                    $scope.gridPPMQueryManager.data.unshift({
                        isCreate: true,
                        ppmTheory: datas.obj.ppm,
                        startDate: datas.obj.stateTime,
                        startDateParsed: $filter('date')(new Date(datas.obj.stateTime), 'yyyy-MM-dd'),
                        endDate: '9999-12-31',
                        endDateParsed: '9999-12-31',
                        hasChanged: true,
                        // resrce: $scope.config.currentDeviceNum.resrce,
                        resrce: datas.gridSelectionRows[i].resourceResrce,
                        resourceType: datas.gridSelectionRows[i].resourceType,
                        asset: datas.gridSelectionRows[i].resourceAsset,
                        resrceDes: datas.gridSelectionRows[i].resourceDescription,
                        //asset: $scope.config.currentDeviceNum.asset,
                        //resrceDes: $scope.config.currentDeviceNum.description,
                        model: $scope.config.currentMaterialCode.model,
                        item: $scope.config.currentMaterialCode.item
                    });
                }
                $scope.config.btnDisabledSave = false;
                $scope.config.btnDisabledEdit = false;
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

        function __initNeedNewResourceDatas(){
            var resultDatas = [];
            for(var i = 0; i< $scope.config.deviceNums.length; i++){
                if($scope.config.deviceNums[i].resrce == null){
                    continue;
                }
                resultDatas.push({
                    resourceDescription: $scope.config.deviceNums[i].description,
                    resourceAsset: $scope.config.deviceNums[i].asset,
                    resourceResrce: $scope.config.deviceNums[i].resrce,
                    resourceType: $scope.config.currentDeviceType.resourceType
                });
            }
            //if($scope.config.currentDeviceType.resourceType == null){
            //    for(var i = 0; i < $scope.config.deviceTypes.length; i++){
            //        if($scope.config.deviceTypes[i].resourceType == null){ continue; }
            //        for(var j = 0; j < $scope.config.deviceNums.length; j++){
            //            if($scope.config.deviceNums[j].resrce == null){ continue; }
            //            resultDatas.push({
            //                resourceDescription: $scope.config.deviceNums[j].description,
            //                resourceAsset: $scope.config.deviceNums[j].asset,
            //                resourceResrce: $scope.config.deviceNums[j].resrce,
            //                resourceType: $scope.config.deviceTypes[i].resourceType
            //                // materialCode: $scope.config.currentMaterialCode.materialCode
            //            });
            //        }
            //    }
            //}else{
            //    for(var j = 0; j < $scope.config.deviceNums.length; j++){
            //        if($scope.config.deviceNums[j].resrce == null){ continue; }
            //        resultDatas.push({
            //            resourceDescription: $scope.config.deviceNums[j].description,
            //            resourceAsset: $scope.config.deviceNums[j].asset,
            //            resourceResrce: $scope.config.deviceNums[j].resrce,
            //            resourceType: $scope.config.currentDeviceType.resourceType
            //            // materialCode: $scope.config.currentMaterialCode.materialCode
            //        });
            //    }
            //}

            var returnObj = {
                queryData: {
                    materialCode: $scope.config.currentMaterialCode,
                    currentDeviceNum:$scope.config.currentDeviceNum
                },
                gridRowDatas: resultDatas
            };
            return returnObj;
        };

        function __deleteLocalCreatedRow(newRowCol){
            var entity = newRowCol.row.entity;
            for(var i = 0; i < $scope.gridPPMQueryManager.data.length; i++){
                var rowData = $scope.gridPPMQueryManager.data[i];
                if(rowData.isCreate && rowData.$$hashKey == entity.$$hashKey){
                    $scope.gridPPMQueryManager.data.splice(i,1);
                    return;
                }
            }
        }

        //全选
        $scope.checkAllChange=function(){
            if($scope.config.checkAllStatus)
            {
                $scope.timeoutCheckAll=$timeout(function () {
                        for(var i=0;i<$scope.gridPPMQueryManager.data.length;i++) {
                            $scope.config.gridApi.selection.selectRow($scope.gridPPMQueryManager.data[i]);
                        }
                    }, 100);
            }
            else{
                $scope.timeouterWithoutCheckAll=$timeout(function () {
                    for(var i=0;i<$scope.gridPPMQueryManager.data.length;i++) {
                        $scope.config.gridApi.selection.unSelectRow($scope.gridPPMQueryManager.data[i]);
                    }
                    //$scope.config.gridApi.selection.clearSelectedRows();
                    $scope.config.btnDisabledEdit = true;
                }, 100);
            }
        };

        function __onRegisterApi(gridApi){
            $scope.config.gridApi = gridApi;
                gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
                    if(newRowCol.row.entity.isCreate && newRowCol.col.field == "isDeleted" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
                        __deleteLocalCreatedRow(newRowCol);
                    }
                });
                // 分页相关
                $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                    // console.log("刷新界面 第 "+currentPage+" 页,每页 "+pageSize+"个元素!");
                    $scope.bodyConfig.overallWatchChanged = false;
                    if(!$scope.config.ppmNum){
                        $scope.config.gridApi.selection.clearSelectedRows();
                    }
                    __requestTableDatas(currentPage, pageSize);
                    return true;
                }, $scope);

                $scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                    $scope.bodyConfig.overallWatchChanged = true;
                    if(colDef.field === "ppmTheory" || colDef.name === "ppmTheory") {
                        var length = rowEntity.ppmTheory.toString().length;
                        var index  = rowEntity.ppmTheory.toString().indexOf('.');
                        if(isNaN(parseInt(rowEntity.ppmTheory))
                            || parseInt(rowEntity.ppmTheory)<=0){
                            $scope.addAlert('info','理论PPM必须大于0');
                            $scope.config.btnDisabledSave = true;
                        }else if((length - index) > 2 && index > 0){
                            $scope.addAlert('info','理论PPM最多一位小数');
                            $scope.config.btnDisabledSave = true;
                        }else{
                            $scope.config.btnDisabledSave = false;
                        }
                    }
                    var promise = $q.defer();
                        if($scope.config.gridApi.rowEdit){
                            $scope.config.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
                        }

                        if(oldValue == newValue){
                            promise.resolve();
                            return;
                        }
                        rowEntity.hasChanged = true;
                        promise.resolve();
                        // if(colDef.name === "ppmTheory"){
                        //     promise.reject();
                        // }else{
                        //     promise.resolve();
                        // }
                });
                $scope.config.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
                    $scope.config.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
                });
                //选择相关
                $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                    var count = $scope.config.gridApi.selection.getSelectedCount();
                    $scope.config.btnDisabledDelete = count > 0 ? false : true;
                    $scope.config.btnDisabledEdit = count > 0 ? false : true;
                    gridRow.entity.isSelected = gridRow.isSelected;

                    if($scope.config.checkAllStatus&&!gridRow.isSelected)
                    {
                        $scope.config.checkAllStatus=false;
                        $scope.checkAllChange();
                    }
                });
                $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                    var count = $scope.config.gridApi.selection.getSelectedCount();
                    $scope.config.btnDisabledDelete = count > 0 ? false : true;
                    $scope.config.btnDisabledEdit = count > 0 ? false : true;
                    var allStatus=true;
                    for(var i = 0; i < gridRows.length; i++){
                        gridRows[i].entity.isSelected = gridRows[i].isSelected;

                        if(!gridRows[i].isSelected)
                        {
                            allStatus=false;
                        }

                    }
                    if($scope.config.checkAllStatus&&!allStatus)
                    {
                        $scope.config.checkAllStatus=false;
                        $scope.checkAllChange();
                    }
                });
        }

        $scope.saveAllAcitons = function(){
            if(!__checkCanSaveDatas()){
                return;
            };
            var paramActions = [];
            for(var i = 0; i < $scope.gridPPMQueryManager.data.length; i++){
                var row = $scope.gridPPMQueryManager.data[i];
                UtilsService.log("第"+i+"行"+row.hasChanged);
                if(row.hasChanged){
                    // var viewActionCode = angular.isUndefined(row.handle) ? 'C' : 'U';
                    if(angular.isUndefined(row.handle)){ //创建新数据 ，若是删除中存在该条数据，则直接忽略该行,并在后面将删除操作也忽略
                        var paramAction = angular.copy(row);
                        paramAction.viewActionCode = 'C';
                        paramAction.startDateDec = UtilsService.serverFommateDate(new Date(paramAction.startDate));
                        paramAction.endDateDec = UtilsService.serverFommateDate(new Date(paramAction.endDate));
                        delete paramAction.startDate;
                        delete paramAction.endDate;
                        paramActions.push(paramAction);
                    }else{ //修改已存于服务器的行数据 ，若删除列表中有该行，则直接忽略该修改提交，并在后面将删除操作添加进来
                        var paramAction = angular.copy(row);
                        paramAction.viewActionCode = 'U';
                        paramAction.startDateDec = UtilsService.serverFommateDate(new Date(paramAction.startDate));
                        delete paramAction.startDate;
                        if(angular.isUndefined(paramAction.endDate) || paramAction.endDate ==null || paramAction.endDate == ""){
                        }else{
                            paramAction.endDateDec = UtilsService.serverFommateDate(new Date(paramAction.endDate));
                            delete paramAction.endDate;
                        }
                        paramActions.push(paramAction);
                    }
                }
            }
            UtilsService.log("所修改的行数："+angular.toJson(paramActions,true));
            if(paramActions.length > 0){
                //在原来保存基础上添加一键编辑功能
                var params = {
                    editList:paramActions,
                    checkAll:$scope.config.ppmNum ? "1":"0",
                    ppm:$scope.config.ppmNum,
                    item: $scope.config.currentMaterialCode ? $scope.config.currentMaterialCode.item:"",
                    resrce: $scope.config.currentDeviceNum == null || $scope.config.currentDeviceNum == '' ? '' :$scope.config.currentDeviceNum.resrce,
                    date: UtilsService.serverFommateDate(new Date($scope.config.queryManDate)),
                    model: $scope.config.currentMaterialCode ? $scope.config.currentMaterialCode.model:"",
                    resourceType: $scope.config.currentDeviceType.resourceType,
                    line: __arrayToString($scope.config.currentLine),
                    workCenter: $scope.config.currentWorkCenter ? $scope.config.currentWorkCenter.workCenter:""
                };
                $scope.showBodyModel("正在保存数据,请稍后...");
                theoryPPMService
                    .PPMSave(params)
                    .then(function (resultDatas){
                        $scope.hideBodyModel();
                        $scope.bodyConfig.overallWatchChanged = false;
                        $scope.addAlert('success', '保存成功!');
                        //保存的时候 重新赋值所有页面ppm值
                        $scope.config.ppmNum="";
                        $scope.config.btnDisabledSave = true;
                        $scope.config.btnDisabledEdit = true;
                        $scope.config.gridApi.selection.clearSelectedRows();
                        $scope.queryDatas();
                    },function (resultDatas){
                        $scope.hideBodyModel();
                        $scope.addAlert('danger', resultDatas.myHttpConfig.errorMessage);
                        //$scope.queryDatas();
                    });
            }else{
                $scope.addAlert('没有修改,无需保存!');
            }
        };

        function __checkCanSaveDatas(){
            for(var i = 0; i < $scope.gridPPMQueryManager.data.length; i++){
                    var item = $scope.gridPPMQueryManager.data[i];
                    if(item.hasChanged){
                        var ppm = item['ppmTheory'];
                        if(angular.isUndefined(ppm) || ppm == null || ppm == ''){
                            $scope.addAlert('', '第'+(i+1)+'行: 设备编码不能为空!');
                            return false;
                        }
                    }
                }
                return true;
        };

        $scope.modalQueryMaterialsCode = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/yieldMaintain/model/ModalQueryMaterialsCode.html',
                controller: 'ModelQueryMaterialsCodeCtr',
                size: 'lg',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope : $scope,
                resolve: {
                   modalMaterialsCodeOptions: function () {
                       return {
                            multiSelect: false
                       };
                   }
                }
            });
            modalInstance.result.then(function (selectedMaterials) {
                if(!angular.isUndefined(selectedMaterials) && !angular.isUndefined(selectedMaterials.Materials) && selectedMaterials.Materials!=null && selectedMaterials.Materials.length >0 ){
                    $scope.config.currentMaterialCode = selectedMaterials.Materials[0];
                    $scope.config.currentMaterialCode.itemBack = selectedMaterials.Materials[0].item;
                    //__setBtnsDisable();
                }
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        }

        $scope.editHandle = function(){
            if(isNaN(parseInt($scope.config.ppmTheory))
                || parseInt($scope.config.ppmTheory)<=0){
                $scope.addAlert('info','理论PPM必须大于0');
                return;
            }
            var length = $scope.config.ppmTheory.toString().length;
            var index  = $scope.config.ppmTheory.toString().indexOf('.');
            if((length - index) > 2 && index > 0){
                $scope.addAlert('info','理论PPM最多一位小数');
                return;
            }
            $scope.bodyConfig.overallWatchChanged = true;

            var selectedRows = $scope.config.gridApi.selection.getSelectedGridRows();
            var datas = $scope.gridPPMQueryManager.data;
            if(selectedRows.length > 0){
                for (var i = datas.length - 1; i >= 0; i--) {
                    if(datas[i].isSelected){
                        datas[i].ppmTheory = $scope.config.ppmTheory;
                        datas[i].hasChanged = true;
                    };
                };
            }else{
                for (var i = $scope.gridPPMQueryManager.data.length - 1; i >= 0; i--) {
                    $scope.gridPPMQueryManager.data[i].ppmTheory = $scope.config.ppmTheory;
                    $scope.gridPPMQueryManager.data[i].hasChanged = true;
                };
            };

            //如果勾选了全选  那么每页ppm都相同
            if($scope.config.checkAllStatus){
                $scope.config.ppmNum=$scope.config.ppmTheory;
            }

            $scope.config.btnDisabledSave = false;
            $scope.config.btnDisabledEdit = false;
        };

        $scope.queryDatas = function(){
            if($scope.bodyConfig.overallWatchChanged){
                var promise =UtilsService.confirm('还有修改未保存，是否继续', '提示', ['确定', '取消']);
                promise.then(function(){
                    $scope.gridPPMQueryManager.paginationCurrentPage = 1;
                    $scope.gridPPMQueryManager.totalItems = 0;
                    __requestTableDatasCount();
                    __requestTableDatas();
                }, function(){
                    //取消停留在原页面
                    $scope.bodyConfig.overallWatchChanged = true;
                });
            }else{

                $scope.gridPPMQueryManager.paginationCurrentPage = 1;
                $scope.gridPPMQueryManager.totalItems = 0;
                __requestTableDatasCount();
                __requestTableDatas();
            };
        };

        function __requestTableDatas(pageIndex, pageCount){
            var pageIndexX = pageIndex,
                pageCountX = pageCount;
            if(angular.isUndefined(pageIndexX) || pageIndexX == null || pageIndexX <= 0){
                pageIndexX = 1;
            }
            if(angular.isUndefined(pageCountX) || pageCountX == null || pageCountX <= 0){
                pageCountX = $scope.gridPPMQueryManager.paginationPageSize;
            }

            var params = {
                item: $scope.config.currentMaterialCode ? $scope.config.currentMaterialCode.item:"",
                resrce: $scope.config.currentDeviceNum == null || $scope.config.currentDeviceNum == '' ? '' :$scope.config.currentDeviceNum.resrce,
                date: UtilsService.serverFommateDate(new Date($scope.config.queryManDate)),
                index: pageIndexX,
                count: pageCountX,
                model: $scope.config.currentMaterialCode ? $scope.config.currentMaterialCode.model:"",
                resourceType: $scope.config.currentDeviceType.resourceType,
                line: __arrayToString($scope.config.currentLine),
                workCenter: $scope.config.currentWorkCenter ? $scope.config.currentWorkCenter.workCenter:""
            };
            theoryPPMService
                .PPMList(params)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].isSelected = false;
                            resultDatas.response[i].startDateParsed = $filter('date')(new Date(resultDatas.response[i].startDate), 'yyyy-MM-dd');
                            resultDatas.response[i].endDateParsed = $filter('date')(new Date(resultDatas.response[i].endDate), 'yyyy-MM-dd');
                        }
                        $scope.bodyConfig.overallWatchChanged = false;
                        //判断是否全选需要赋值
                        if($scope.config.ppmNum){
                            for(var i = 0; i < resultDatas.response.length; i++) {
                                resultDatas.response[i].ppmTheory=$scope.config.ppmNum;
                            }
                            $scope.config.btnDisabledSave = false;
                            $scope.config.btnDisabledEdit = false;
                        }
                        else{
                            $scope.config.btnDisabledSave = true;
                            $scope.config.btnDisabledEdit = true;
                        }
                        $scope.gridPPMQueryManager.data = resultDatas.response;
                        //$scope.config.btnDisabledSave = true;
                        //$scope.config.btnDisabledEdit = true;

                        $scope.checkAllChange();
                        return;
                    }else{
                        $scope.gridPPMQueryManager.data = [];
                    }
                }, function (resultDatas){
                    $scope.gridPPMQueryManager.data = [];
                    $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
                });
        };

        function __requestTableDatasCount(){
            var params = {
                item: $scope.config.currentMaterialCode ? $scope.config.currentMaterialCode.item:"",
                resrce: $scope.config.currentDeviceNum == null || $scope.config.currentDeviceNum == '' ? '' :$scope.config.currentDeviceNum.resrce,
                date: UtilsService.serverFommateDate(new Date($scope.config.queryManDate)),
                model:$scope.config.currentMaterialCode ? $scope.config.currentMaterialCode.model:"",
                resourceType: $scope.config.currentDeviceType.resourceType,
                line: __arrayToString($scope.config.currentLine),
                workCenter: $scope.config.currentWorkCenter ? $scope.config.currentWorkCenter.workCenter:""
            };

            theoryPPMService
                .PPMListCount(params)
                .then(function (resultDatas){
                    if(resultDatas.response){
                        $scope.gridPPMQueryManager.totalItems = resultDatas.response;

                    }
                    if(resultDatas.response === 0){
                        $scope.addAlert("","暂无数据");
                    }
                }, function (resultDatas){
                    $scope.gridPPMQueryManager.data = [];
                    $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
                });
        };

        $scope.init = function(){
            __initDropdowns('init');
            __requestDropdownWorkCenters();
            __requestDropdownDeviceTypes();
        };

        $scope.init();

        $scope.tipSelectClick = function(type){
            if(type == 'line'){
                $scope.config.$workCenterSelect.toggle();
            }else if(type == 'deviceNum'){
                $scope.config.$workCenterSelect.toggle();
            }
        };

        function __initDropdowns(type){

            if(type == 'init'){
                $scope.config.workCenters = [];
                $scope.config.currentWorkCenter = null;

                $scope.config.deviceTypes = [];
                $scope.config.currentDeviceType = null;
            }
            if(type == 'init' || type == 'workCenterChanged'){
                $scope.config.lines = [];
                $scope.config.currentLine = [];
                $scope.config.currentLineDesc = '';
            }
            $scope.config.currentDeviceNums = [];
            $scope.config.currentDeviceNum = null;
        };
        /**
         * 将数组转为String，以逗号分隔
         * */
        function __arrayToString (array){
            var lineString = [];
            for(var i=0;i<array.length;i++){
                lineString[i] = array[i].workCenter;
            }
            return lineString.toString();
        };

        /////   生成区域、级联选择
        $scope.workCenterChanged = function(){
            __initDropdowns('workCenterChanged');
            var workCenter = $scope.config.currentWorkCenter.workCenter;
            var line = __arrayToString($scope.config.currentLine);
            var resourceType = $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:null;
            __requestDropdownLines(workCenter);
            __requestDropdownDeviceCodes(workCenter,line,resourceType);
            //__setBtnsDisable();
        };

        function __setBtnsDisable(){
            UtilsService.log("设备类型："+$scope.config.currentDeviceType);
            if(//!$scope.config.currentWorkCenter
                !$scope.config.currentDeviceType
                //|| !$scope.config.currentMaterialCode
                //|| UtilsService.isEmptyString($scope.config.currentMaterialCode.item)
                //|| $scope.config.currentMaterialCode.item != $scope.config.currentMaterialCode.itemBack
                ){
                $scope.config.btnDisabledAdd = true;
                $scope.config.btnDisabledQuery = true;
                $scope.config.btnDisabledDelete = true;
                $scope.config.btnDisabledEdit = true;
                return;
            }
            $scope.config.btnDisabledAdd = false;
            $scope.config.btnDisabledQuery = false;
        };

        $scope.lineChanged = function(){
            //if($scope.config.currentLine == null || $scope.config.currentLine.workCenter==null){
            //    __initDropdowns('lineChanged');
            //}else{
            //    __initDropdowns('lineChanged');
            //    var workCenter = $scope.config.currentWorkCenter.workCenter;
            //    var line = $scope.config.currentLine.workCenter;
            //    var resourceType = $scope.config.currentDeviceType.resourceType;
            //
            //    __requestDropdownDeviceCodes(workCenter, line, resourceType);
            //    // if($scope.config.currentDeviceType != null && $scope.config.currentDeviceType.resourceType!=null){
            //    //     $scope.config.btnDisabledAdd = false;
            //    //     $scope.config.btnDisabledQuery = false;
            //    // }
            //}


            __initDropdowns('lineChanged');
            var workCenter = $scope.config.currentWorkCenter.workCenter;
            var line = __arrayToString($scope.config.currentLine);
            var resourceType = $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:null;

            __requestDropdownDeviceCodes(workCenter, line, resourceType);
            __setBtnsDisable();
        };

        $scope.deviceTypeChanged = function(){
            __initDropdowns('deviceTypeChanged');
            var workCenter="";
            if($scope.config.currentWorkCenter)
            {
                if($scope.config.currentWorkCenter.workCenter)
                {
                    workCenter = $scope.config.currentWorkCenter.workCenter;
                }
            }
            //var workCenter = $scope.config.currentWorkCenter.workCenter;
            var line = __arrayToString($scope.config.currentLine);
            var resourceType = $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:null;

            __requestDropdownDeviceCodes(workCenter, line, resourceType);
            __setBtnsDisable();
        };

        $scope.deviceNumChanged = function(){
        };

        $scope.materialCodeChanged = function(){
            if(angular.isUndefined($scope.config.currentMaterialCode) || $scope.config.currentMaterialCode == null || UtilsService.isEmptyString($scope.config.currentMaterialCode.item)){
            }else{
                $scope.config.currentMaterialCode.item = $scope.config.currentMaterialCode.item.toUpperCase();
            }
            //__setBtnsDisable();
        };

        function __requestDropdownWorkCenters(){
            plcService
                    .dataWorkCenters()
                    .then(function (resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            $scope.config.workCenters = [];
                            //$scope.config.workCenters.push($scope.config.defaultSelectItem);
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].workCenter+"   描述:"+resultDatas.response[i].description;
                                $scope.config.workCenters.push(resultDatas.response[i]);
                            }
                            // $scope.config.workCenters = $scope.config.workCenters.concat(resultDatas.response);
                            return;
                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    });
        };

        function __requestDropdownLines(workCenter){
            plcService
                    .dataLines(workCenter)
                    .then(function (resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            $scope.config.lines = [];
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].workCenter+"   描述:"+resultDatas.response[i].description;
                                $scope.config.lines.push(resultDatas.response[i]);
                            }
                            // $scope.config.$lineSelect.toggle();
                            // $scope.config.lines = $scope.config.lines.concat(resultDatas.response);
                            return;
                        }
                    },function (resultDatas){ //TODO 检验失败
                    if(resultDatas.myHttpConfig.isSessionOut == true || resultDatas.myHttpConfig.isTimeout == true || resultDatas.myHttpConfig.status == 1 || resultDatas.myHttpConfig.status == -1){
                    }else{
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    }
                });
        };

        function __requestDropdownDeviceTypes(){
            plcService
                    .dataResourceTypes()
                    .then(function (resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].resourceType+"   描述:"+resultDatas.response[i].description;
                                $scope.config.deviceTypes.push(resultDatas.response[i]);
                            }
                            // $scope.config.deviceTypes = $scope.config.deviceTypes.concat(resultDatas.response);
                            return;
                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
        };

        function __requestDropdownDeviceCodes(workCenter, line, resourceType){
            yieldMaintainService.requestDataResourceCodesByLine(workCenter, line, resourceType)
                    .then(function (resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            $scope.config.deviceNums = [];
                            $scope.config.deviceNums.push($scope.config.defaultSelectItem);
                            for(var i = 0; i < resultDatas.response.length; i++){
                                resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].resrce+"   描述:"+resultDatas.response[i].description;
                                $scope.config.deviceNums.push(resultDatas.response[i]);
                            }
                            // $scope.config.deviceNums = $scope.config.deviceNums.concat(resultDatas.response);
                            return;
                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
        };

        $scope.$on('$destroy',function(){
            $timeout.cancel($scope.timeoutCheckAll);
            $timeout.cancel($scope.timeouterWithoutCheckAll);
        });

}]);



theoryPPMModule.controller('ModalQueryManagerCtrl', [
    '$scope', '$uibModalInstance',
    'PPMModelData','UtilsService', 'theoryPPMService',
    function ($scope, $uibModalInstance, PPMModelData,UtilsService, theoryPPMService) {
    
    $scope.config = {
        gridApi: null,

        needValidRemoteCount: 0,

        btnDisabledSave: true,

        queryManagerDate :  UtilsService.serverFommateDateShow(new Date()),
        materialCode: null, // 物料编码
        shift: null,
        targetOutput: '', // 理论PPM
        btnOkQueryManagerDisabled:true,
    };



    $scope.gridModalQueryManager = {
        enablePagination: false,
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        data: [],
        columnDefs: [
            { 
                field: 'handle', name: 'handle', visible: false
            },
            //{
            //    field: 'resourceType', name: 'resourceType', displayName: '设备类型', visible: true, minWidth: 100,
            //    enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
            //    validators: { required: true },
            //    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            //},
            { 
                field: 'resourceResrce', name: 'resourceResrce', displayName: '设备编码', visible: true, minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'resourceAsset', name: 'resourceAsset', displayName: '资产号', visible: true, minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            },
            {
                field: 'resourceDescription', name: 'resourceDescription', displayName: '设备名称', visible: true, minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false, enableColumnMenu: false, enableColumnMenus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
            }
        ],
        onRegisterApi: __onRegisterApi
    };

    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            __checkIsValidBeforeOk();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            __checkIsValidBeforeOk();
        });
        __checkIsValidBeforeOk();
    };

    $scope.init = function(){
        $scope.gridModalQueryManager.data = [];
        var data = PPMModelData.gridRowDatas;
        if(PPMModelData.queryData.currentDeviceNum == null || PPMModelData.queryData.currentDeviceNum.resrce == null){
            $scope.gridModalQueryManager.data = data;
        }else{
            for(var i=0;i<data.length;i++){
                if(data[i].resourceResrce == PPMModelData.queryData.currentDeviceNum.resrce){
                    $scope.gridModalQueryManager.data.push(data[i]);
                }
            }
        }
        console.log($scope.gridModalQueryManager.data);
        $scope.config.materialCode = PPMModelData.queryData.materialCode;
    };

    $scope.isEmpty = function(){
        __isEmpty();
    }

    $scope.init();

    function __checkIsValidBeforeOk(){
        var selectedRows = $scope.config.gridApi.selection.getSelectedGridRows();
        var ppm = $scope.config.targetOutput;
        var queryManagerDate = $scope.config.queryManagerDate;
        if(selectedRows.length == 0 || ppm == '' || ppm==null || angular.isUndefined(ppm) || queryManagerDate == '' || queryManagerDate==null || angular.isUndefined(queryManagerDate) || ppm < 0){
            $scope.config.btnOkQueryManagerDisabled = true;
            return;
        };
        $scope.config.btnOkQueryManagerDisabled = false;
    };

    function __isEmpty(){
        __checkIsValidBeforeOk();
    };

    $scope.okQueryManager = function () {
        if($scope.config.targetOutput){
            var length = $scope.config.targetOutput.toString().length;
            var index  = $scope.config.targetOutput.toString().indexOf('.');
            if(isNaN(parseInt($scope.config.targetOutput))
                || parseInt($scope.config.targetOutput)<=0){
                $scope.addAlert('info','理论PPM必须大于0');
            }else if((length - index) > 2 && index > 0){
                $scope.addAlert('info','理论PPM最多一位小数');
            }
            // else if(new Date($scope.config.queryManagerDate) <= new Date()){
            //     $scope.addAlert('info','开始日期早于或等于已存在的日期')
            // }
            else {
                __validForCreate();
            }
        };
    };
    function __validForCreate(){
        var selectedRowsNew = $scope.config.gridApi.selection.getSelectedGridRows();
        $scope.config.needValidRemoteCount = selectedRowsNew.length;
        for(var i = 0; i < selectedRowsNew.length; i++){
            var row = selectedRowsNew[i];
            var validObj = {
                item: PPMModelData.queryData.materialCode.item,
                resrce: row.entity.resourceResrce,
                dateDec: UtilsService.serverFommateDate(new Date($scope.config.queryManagerDate))
            };
            theoryPPMService
                .PPMValidForCreate(validObj)
                .then(function(resultDatas){
                    if(resultDatas.response.existing){ // 已经存在，不允许新增
                        var alertStr = resultDatas.config.data.resrce +"对应理论PPM维护记录已经存在,不允许新增!";
                        $scope.addAlert('warning', alertStr);
                    }else{
                        $scope.config.needValidRemoteCount--;
                        __realCreateRows();
                    }
                }, function(resultDatas){
                    $scope.addAlert('info', resultDatas.myHttpConfig.statusDesc);
                });
        }
    }
    function __realCreateRows(){
        if($scope.config.needValidRemoteCount> 0){
            return;
        }
        $uibModalInstance.close({
            gridSelectionRows: $scope.config.gridApi.selection.getSelectedRows(),
            obj: {
                ppm: $scope.config.targetOutput,
                stateTime: new Date($scope.config.queryManagerDate)
            }
        });
    }

    $scope.cancelQueryManager = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);