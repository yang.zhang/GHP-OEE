theoryPPMModule.service('theoryPPMService', [
    'HttpAppService',
    function (HttpAppService) {
	
    return {
    	PPMList: requestPPMList,
    	PPMListCount: requestPPMListCount,
    	PPMSave: requestPPMSave,
    	PPMValidForCreate: requestPPMValidForCreate
    };


	//   {	
	//   	item: '',
	//   	resrce: '',
	//   	date: '',
		// index: '',
		// count: '',
		// model: ''
		// resourceType: '',
		// line: '',
		// workCenter: ''
	//   } 
    function requestPPMList(params){ 
		var url = HttpAppService.URLS.PPM_LIST
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{ITEM}#", params.item)
					.replace("#{RESRCE}#", params.resrce)
					.replace("#{DATE}#", params.date)
					.replace("#{MODEL}#", params.model)
					.replace("#{INDEX}#", params.index)
					.replace("#{COUNT}#", params.count)
					.replace("#{RESOURCE_TYPE}#", params.resourceType)
					.replace("#{LINE}#", params.line)
					.replace("#{WORK_CENTER}#", params.workCenter)
					;
		return HttpAppService.get({ 
			url: url
		});
	};
	function requestPPMListCount(params){ 
		var url = HttpAppService.URLS.PPM_LIST_COUNT
					.replace("#{SITE}#", HttpAppService.getSite())
					.replace("#{ITEM}#", params.item)
					.replace("#{RESRCE}#", params.resrce)
					.replace("#{DATE}#", params.date)
					.replace("#{MODEL}#", params.model)
					.replace("#{RESOURCE_TYPE}#", params.resourceType)
					.replace("#{LINE}#", params.line)
					.replace("#{WORK_CENTER}#", params.workCenter)
					;
		return HttpAppService.get({ 
			url: url
		});
	};

	function requestPPMSave(paramActions){
		var url = HttpAppService.URLS.PPM_SAVE
					.replace("#{SITE}#", HttpAppService.getSite())
					;
		return HttpAppService.post({ 
			url: url,
			paras: paramActions
		});
	}

	/**
	 * 
	 * [requestPPMValidForCreate PPM新增前检验接口调用]
	 * @param  {[object]} paramActions [接口所需参数]
	 *                               {
	 *                               	item: '',
	 *                               	resrce: '',
	 *                               	dateDec: ''
	 *                               }
	 * @return {[promise]}           [promise]
	 * 
	 */
	function requestPPMValidForCreate(paramAction){
		var url = HttpAppService.URLS.PPM_VALID_FOR_CREATE
					.replace("#{SITE}#", HttpAppService.getSite())
					;
		return HttpAppService.post({ 
			url: url,
			paras: paramAction
		});
	}
 	


}]);

theoryPPMModule
	.run([ 
		'$templateCache',
		function ($templateCache){ 




		$templateCache.put('andon-ui-grid-tpls/ppm-DeviceNumber', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.resrce\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		); 

		$templateCache.put('andon-ui-grid-tpls/ppm-Model', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.model\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		); 

		<!-- row.entity.ppmTheory -->
		$templateCache.put('andon-ui-grid-tpls/ppm-PPM', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.ppmTheory\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		); 

		$templateCache.put('andon-ui-grid-tpls/ppm-StartDate', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.startDate\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		); 

}]);









