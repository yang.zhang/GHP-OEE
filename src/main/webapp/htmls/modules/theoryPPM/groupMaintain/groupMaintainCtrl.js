theoryPPMModule.controller('groupMaintainCtrl',['$scope', '$http',
    'HttpAppService', '$timeout', 'UtilsService', '$filter',
    'uiGridConstants', 'Upload', 'uiGridValidateService',
    function ($scope, $http, HttpAppService, $timeout, UtilsService, $filter,
              uiGridConstants, Upload, uiGridValidateService) {



    // $scope.config = { 
    //     isUploading: false,
    //     uploadError: false,
    //     uploadOk: false,

    //     uploadProgressStr: ''
    // }; 

    //  // upload on file select or drop
        // $scope.upload = function (file) { 
        //     $scope.config.isUploading = true;
        //     $scope.config.uploadOk = false;
        //     $scope.config.uploadError = false;
        //     var url = HttpAppService.URLS.PPM_UPLOAD_FILE
        //                     .replace("#{SITE}#", HttpAppService.getSite());
        //     Upload.upload({
        //         // url: 'mhp-oee/web/rest/std/cycletime/management/1000/upload_file',
        //         url: url,
        //         data: {file: file}
        //     }).then(function (resp) {
        //         console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        //         $scope.config.isUploading = false;
        //         $scope.config.uploadOk = true;
        //         $scope.config.uploadError = false;
        //     }, function (resp) {
        //         console.log('Error status: ' + resp.status);
        //         $scope.config.isUploading = false;
        //         $scope.config.uploadOk = false;
        //         $scope.config.uploadError = true;
        //     }, function (evt) {
        //         var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        //         console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        //         $scope.config.uploadProgressStr = progressPercentage + "%";
        //     });
        // };



        // $scope.gridPPMGroupMaintain = {
        //     paginationPageSizes: [10, 25, 50, 75],
        //     paginationPageSize:10,
        //     columnDefs:[{name:"column1",displayName:'原因代码组编码'},
        //         {name:"column2",displayName:'原因代码组维护'}, 
        //         {name:"column3",displayName:'原因代码选择'},{name:"column4"},{name:"column5"},
        //         {name:"column6"}]
    // }
    
    
    
    $scope.gridPPMGroupMaintain = {
        paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
        paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,
        data: [],
        onRegisterApi: __onRegisterApi,
        columnDefs:[ 
            {   
                // field: "DeviceNumber",
                field: 'resrce',
                name:"resrce",displayName:"设备编码",enableCellEdit: false,
                cellTemplate: 'andon-ui-grid-tpls/ppm-DeviceNumber',
                cellTooltip: __cellToolTip
            },  
            {   
                // field: "Model",
                field: 'model',
                name:"model",
                displayName:"Model",
                enableCellEdit: false,
                cellTemplate: 'andon-ui-grid-tpls/ppm-Model',
                cellTooltip: __cellToolTip 
            },  
            {   
                // field: "PPM",
                field: 'ppmTheory',
                name:"ppmTheory",
                displayName:"理论PPM",
                enableCellEdit: false,
                cellTemplate: 'andon-ui-grid-tpls/ppm-PPM',
                cellTooltip: __cellToolTip 
            },  
            {   
                // field: "StartDate",
                field: 'startDate', 
                name:"startDate",
                displayName:"有效开始日期",
                enableCellEdit: false,
                cellTemplate: 'andon-ui-grid-tpls/ppm-StartDate',
                cellTooltip: __cellToolTip 
            }]  
    };  
    
    function __cellToolTip(row, col){ 
        var str = '';// row.entity[col.colDef.name]; 
        var errors = row.entity[col.colDef.name+"Exceptions"];
        if(!angular.isUndefined(errors) && errors != null && errors.length > 0){
            for(var i = 0; i < errors.length; i++){
                if(i > 0){
                    str += "<br/>";
                }
                str += errors[i];
            }
        }
        return str; 
    }

    $scope.config = { 
        gridApi: null,
        isUploading: false,
        uploadError: false,
        uploadOk: false,

        selectedFile: null,

        parseFileOk: false,
        parseFileOkNum: 0,

        uploadProgressStr: '',
        progressPercentage: 0,

        showErrorDucpGridDatas: false
    };  

    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
    };

    $scope.$watch('config.selectedFile', function(newVal, oldVal){
        if(newVal==null){
            $scope.config.uploadProgressStr = '';
            $scope.config.progressPercentage = 0;
            $scope.bodyConfig.overallWatchChanged = false;
        }else{
            $scope.bodyConfig.overallWatchChanged = true;
        }
    });

    // upload on file select or drop
    $scope.upload = function () { 
        if($scope.config.uploadOk){ //重新上传:初始化相关变量
            $scope.repareForUpload();
            return;
        }   
        var file = $scope.config.selectedFile;
        $scope.config.isUploading = true;
        $scope.bodyConfig.overallWatchChanged = true;
        $scope.config.uploadOk = false;
        $scope.config.uploadError = false;
        $scope.config.progressPercentage = 0;
        var url = HttpAppService.URLS.PPM_UPLOAD_FILE
                        .replace('#{SITE}#', HttpAppService.getSite());
        // url = url + "?permactivity="+ROOTCONFIG.AndonConfig.CURRENTACTIVITY;
        // url = url + "&permsite="+HttpAppService.getSite();
        url = HttpAppService.handleCommenUrl(url);
        Upload.upload({
            // url: 'mhp-oee/web/rest/plant/site/1000/plc_category',
            url: url,
            timeout:60*60*1000,   //一个小时
            data: {file: file}
        }).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            $scope.config.isUploading = false;
            $scope.bodyConfig.overallWatchChanged = false;
            $scope.config.uploadOk = true;
            $scope.config.uploadError = false;
            $scope.config.uploadProgressStr = '';
            $scope.config.progressPercentage = 0;
            var errors = resp.data.exceptionRowArray;
            if(angular.isUndefined(errors) || errors == null || errors.length == 0){
                // 文件没有解析错误信息
                errors = [];
                __handleErrors(errors);
                $scope.config.parseFileOk = true;
                $scope.config.parseFileOkNum = resp.data.successUpload.commitNumber;
            }else{
                __handleErrors(errors);
            }
        }, function (resp) {
            console.log('Error status: ' + resp.status);
            console.log(resp);
            $scope.config.isUploading = false;
            $scope.bodyConfig.overallWatchChanged = false;
            $scope.config.uploadOk = false;
            $scope.config.uploadError = true;
            $scope.config.uploadProgressStr = '';
            $scope.config.progressPercentage = 0;
            $scope.config.parseFileOk = false;
            if(resp.status == 400){
                var msg = resp.headers('error-message');
                if(!UtilsService.isEmptyString(msg)){
                    var msgObj = JSON.parse(msg);
                    var errorJson = msgObj.errorJson;
                    if(!UtilsService.isEmptyString(errorJson)){
                        var errorJsonObj = JSON.parse(errorJson);
                        $scope.gridPPMGroupMaintain.data = [errorJsonObj];
                        $scope.config.showErrorDucpGridDatas = true;
                    }   
                }       
            }else{      
                $scope.addAlert(resp.statusText);   
            }    
        }, function (evt) { 
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            $scope.config.uploadProgressStr = progressPercentage + "%";
            $scope.config.progressPercentage = progressPercentage;
        }); 
    }; 

    $scope.drap = function(){
        console.log(arguments);
        $scope.config.isUploading = false;
        $scope.bodyConfig.overallWatchChanged = true;
        $scope.config.uploadError = false;
        $scope.config.uploadOk = false;
    };
    $scope.select = function($file){
        $scope.config.selectedFile = $file;
        $scope.config.isUploading = false;
        $scope.bodyConfig.overallWatchChanged = true;
        $scope.config.uploadError = false;
        $scope.config.uploadOk = false;
    }
    $scope.repareForUpload = function(){
        $scope.config.selectedFile = null;
        $scope.config.isUploading = false;
        $scope.bodyConfig.overallWatchChanged = false;
        $scope.config.uploadError = false;
        $scope.config.uploadOk = false;
        $scope.config.parseFileOk = false;
        $scope.config.parseFileOkNum = 0;
        $scope.config.uploadProgressStr = '';
        $scope.config.progressPercentage = 0;
    };

    function __handleErrors(errors){
        var resultDatas = [];
        for(var i = 0; i < errors.length; i++){
            var err = errors[i];
            // err.originDataMap.StartDate = $filter('date')(new Date(err.originDataMap.startDate), 'yyyy-MM-dd');
            var data = err.originDataMap; 
            var list = err.exceptionList; 
            var rowNum = err.rowNumber;
            for(var j = 0; j < list.length; j++){ 
                var exe = list[j]; 
                for(key in exe){ 
                    data[key+"Exceptions"] = exe[key]; 
                    // 转换 exception 为中文描述 
                    for(var xx = 0; xx < data[key+"Exceptions"].length; xx++){
                        data[key+"Exceptions"][xx] = "文件第"+rowNum+"行: "+__corvetToChinaese(data[key+"Exceptions"][xx]);
                    }
                } 
            }   
            resultDatas.push(data);
        }
        console.log(resultDatas);
        if(resultDatas.length > 0){
            $scope.gridPPMGroupMaintain.data = resultDatas;
            $timeout(function(){
                __setInValidCells();
            }, 500);
        }
    }
    // uiGridValidateService.setValid(rowEntity, colDef);
    var colDefNamesNeedValid = ['resrce','model','ppmTheory','startDate'];
    function __setInValidCells(){
        var datas = $scope.gridPPMGroupMaintain.data;
        for(var i = 0; i < datas.length; i++){
            var rowEntity = datas[i];
            for(var colName in colDefNamesNeedValid){
                var colNameError = colDefNamesNeedValid[colName]+"Exceptions";
                if(!angular.isUndefined(rowEntity[colNameError]) && rowEntity[colNameError] != null && rowEntity[colNameError].length > 0){
                    uiGridValidateService.setInvalid(rowEntity,$scope.config.gridApi.grid.getColDef(colDefNamesNeedValid[colName]));
                }
            }
        }
    }

    function __corvetToChinaese(codeStr){
        if(UtilsService.isEmptyString(codeStr)){
            return codeStr;
        }
        var code = codeStr.substring(codeStr.indexOf("[")+1, codeStr.indexOf("]"));
        var codeDesc = HttpAppService.getDescByExceptionCode(code);
        return codeDesc;
    }


}]);