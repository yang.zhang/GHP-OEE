theoryPPMModule.controller('theoryPPMCtrl',['$scope', '$http',
    'HttpAppService', '$timeout',
    'uiGridConstants','$state',
    function ($scope, $http, HttpAppService, $timeout,
              uiGridConstants,$state) {
        $scope.config = {
            // editRw:false,
            tab1: {
                title:'理论PPM批量维护',
                distState: '#/andon/TheoryPPM/GroupMaintain',
                distStateName:'andon.TheoryPPM.GroupMaintain',
                isActive:true
            },  
            tab2: {
                title:'理论PPM查询管理',
                distState: '#/andon/TheoryPPM/QueryManager',
                distStateName:'andon.TheoryPPM.QueryManager',
                isActive:false
            },  
            theoryPPMMenuList: []
            // theoryPPMMenuList:[{
            //         title:'理论PPM查询管理',
            //         distState: '#/andon/TheoryPPM/QueryManager',
            //         distStateName:'andon.TheoryPPM.QueryManager',
            //         isActive:false
            // },{
            //         title:'理论PPM批量维护',
            //         distState: '#/andon/TheoryPPM/GroupMaintain',
            //         distStateName:'andon.TheoryPPM.GroupMaintain',
            //         isActive:true
            // }]
        };
        
        $scope.ppmConfig = {
            canEdit: false
        };
        $scope.ppmConfig.canEdit = $scope.modulesRWFlag("#/andon/TheoryPPM/GroupMaintain");
        // $scope.ppmConfig.canEdit = true;
        if($scope.ppmConfig.canEdit){
            $scope.config.theoryPPMMenuList = [$scope.config.tab2, $scope.config.tab1];
        }else{
            $scope.config.theoryPPMMenuList = [$scope.config.tab2];
        }


        // $scope.config.theoryPPMMenuList[0].editRW = $scope.config.editRw;

        //$scope.modulesRWFlag("#/andon/TheoryPPM/GroupMaintain")
        //    .then(function(item){
        //        console.log(item);
        //        $scope.config.editRw = item.write;
        //        $scope.config.theoryPPMMenuList[0].editRW = item.write
        //    }, function(){
        //        //console.log();
        //    });
        $scope.$on("$stateChangeSuccess", function (event2, toState, toParams, fromState, fromParam){
            for(var i=0;i<$scope.config.theoryPPMMenuList.length;i++){
                var item = $scope.config.theoryPPMMenuList
                //$scope.config.reasonCodeMenuList[i].distState.slice(2).replace('/','.').replace('/','.')
                if(fromState && toState.name == item[i].distStateName){
                    item[i].isActive = true
                }else{
                    item[i].isActive = false;
                }
            }
        });





    }])