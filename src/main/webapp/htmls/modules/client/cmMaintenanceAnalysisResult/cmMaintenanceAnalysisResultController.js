clientModule.controller('cmMaintenanceAnalysisResultController', [
    '$scope', '$http', '$q',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants','$uibModal', 'gridUtil','UtilsService','clientService','$state','uiGridCellNavConstants',
    function ($scope, $http, $q, HttpAppService, $timeout, $interval,
              uiGridConstants,$uibModal,gridUtil,UtilsService,clientService,$state,uiGridCellNavConstants){
        $scope.config = {
           title:'CM维修分析结果录入',
           resourceMeStaff:'',                      //ME工程师工号
           meStaffName:'',						    //ME工程师姓名
           meStaffLocked:'',                        //ME工程师是否锁定
           resource:'',                             //设备编码
           resourceName:'',                         //设备信息
           resourceCurrentData:{},
        };
        $scope.init = function(){
        	//界面打开，则加载数据
        	initResourceInfo();
        };
        $scope.init();
        $scope.getHeight=function(){
        	if ($scope.workOrderGrid.data.length > 0) {
                var rowHeight = 31;
                var headerHeight = 50;
                return {
                    height: (15 * rowHeight + headerHeight) + "px"
                };
            }
        }
        //刷新
        $scope.refresh=function(){
        	initData($scope.config.resource);
        }
        $scope.changeMeStaffPage = function(){
        	var modalInstance = $uibModal.open({
				animation : true,
				templateUrl : 'modules/client/modal/operatorLogin.html',
				controller : 'operatorLoginContrller',
				size : 'md',
				backdrop : 'false',
				scope : $scope,
				openedClass : 'flex-center-parent',
				resolve : {
					userModel : function() {
						if(UtilsService.isNull($scope.config.resourceMeStaff)){
							return {
								modalTitle:'维护ME工程师',
							};
						}else{
							return {
								modalTitle:'更换ME工程师',
							};
						}
					}
				}
			});
			modalInstance.result.then(function(data) {
				var params=$scope.config.resourceCurrentData;
				params.userName=data.userName;
				params.passWord=data.passWord;
				params.type="meUser";
				changeMeStaff(params);
			}, function() {
			});
        }
        //锁定
        $scope.locked=function(){
        	var params=$scope.config.resourceCurrentData;
        	params.type="meUser";
        	params.meStaffLocked="true";
        	clientService.__changeLocked(params).then(function (data){
        		if(data.myHttpConfig.status == 200&&data.response){
        			if(data.response.code==0){
        				$scope.addAlert("success","锁定成功");
        				initResourceInfo();
        			}else if(data.response.code==1){
        				$scope.addAlert('danger', "锁定失败");
        			}
                }
            },function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        //注销
        $scope.logout=function(){
        	var params=$scope.config.resourceCurrentData;
        	clientService.__plcMeUserLogout(params).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200){
        			$scope.addAlert("success","注销成功");
        			//initResourceInfo();
                    writeUserLogInfo(params,0);
                }
            },function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        //打开解锁页面
        $scope.unlockedPage = function(){
        	var modalInstance = $uibModal.open({
				animation : true,
				templateUrl : 'modules/client/modal/operatorLogin.html',
				controller : 'operatorLoginContrller',
				size : 'md',
				backdrop : 'false',
				scope : $scope,
				openedClass : 'flex-center-parent',
				resolve : {
					userModel : function() {
						return {
							modalTitle:'解锁ME工程师',
							resourceMeStaff:$scope.config.resourceMeStaff
						};
					}
				}
			});
			modalInstance.result.then(function(data) {
				var params=$scope.config.resourceCurrentData;
				params.userName=data.userName;
				params.passWord=data.passWord;
				unlocked(params);
			}, function() {
			});
        }
        //变更工单状态
        $scope.changeWorkOrderStatu=function(){
        	clientService.__plcChangeWorkOrderStatu().then(function (resultDatas){
        		if(resultDatas.response){
        			$scope.addAlert("success",resultDatas.response.emsg);
                    return;
                }
        	},function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        //维修结果确认弹出框
        $scope.maintenanceResultConfirmation = function(){
			var promise = UtilsService.confirm('您确定调用维修结果确认接口吗?', '结果确认', ['确定', '取消']);
			promise.then(function(){
				maintenanceRC();
			}, function(){
				maintenanceRC();
			});
		};
		//me接单
        $scope.meReceiveOrder=function(){
        	var params={
        			IANDONUSER:'测试一',
        			IZSPOT:'0',
        			items:[{"AUFNR":"000010000102"}]
            	}
            	clientService.__plcMeReceiveOrder(params).then(function (resultDatas){
            		if(resultDatas.myHttpConfig.status == 200&&resultDatas.response){
            			$scope.addAlert("success","接单成功");
                    }
                },function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        }
        //$scope.workOrderGrid={
        //    	data:[],
        //        enableCellEdit:false,
        //        columnDefs:[
			//		{
			//		    name:"handle",visible:false
			//		},
        //            {
        //                name:"QMNUM",
        //                displayName:'通知单号',
        //                minWidth:100,
        //                enableCellEdit : false,
        //                cellClass: function (row, col) {
        //                	if(col.entity.QMART=='Z1'){
        //                		return 'high-light-red';
        //                	}
        //                },
        //                enableCellEditOnFocus : false,
        //            },
        //            {
        //                name:"QMTXT",
        //                displayName:'通知单描述',
        //                minWidth:150,
        //                enableCellEdit : false,
        //                cellClass: function (row, col) {
        //                	if(col.entity.QMART=='Z1'){
        //                		return 'high-light-red';
        //                	}
        //                },
        //                enableCellEditOnFocus : false,
        //            },
        //            {
        //                name:"QMART",
        //                displayName:'通知单类型',
        //                minWidth:100,
        //                enableCellEdit : false,
        //                cellClass: function (row, col) {
        //                	if(col.entity.QMART=='Z1'){
        //                		return 'high-light-red';
        //                	}
        //                },
        //                enableCellEditOnFocus : false,
        //            },
        //            {
        //                name:"QMARTX",
        //                displayName:'通知单类型描述',
        //                minWidth:100,
        //                enableCellEdit : false,
        //                cellClass: function (row, col) {
        //                	if(col.entity.QMART=='Z1'){
        //                		return 'high-light-red';
        //                	}
        //                },
        //                enableCellEditOnFocus : false,
        //            },
        //            {
        //                name:"GSTRP",
        //                displayName:'计划日期',
        //                minWidth:100,
        //                enableCellEdit : false,
        //                cellClass: function (row, col) {
        //                	if(col.entity.QMART=='Z1'){
        //                		return 'high-light-red';
        //                	}
        //                },
        //                enableCellEditOnFocus : false,
        //            },
        //            {
        //                name:"AUFNR",
        //                displayName:'工单号',
        //                minWidth:100,
        //                enableCellEdit : false,
        //                cellClass: function (row, col) {
        //                	if(col.entity.QMART=='Z1'){
        //                		return 'high-light-red';
        //                	}
        //                },
        //                enableCellEditOnFocus : false,
        //            },
        //            {
        //                name:"KTEXT",
        //                displayName:'工单描述',
        //                minWidth:100,
        //                enableCellEdit : false,
        //                cellClass: function (row, col) {
        //                	if(col.entity.QMART=='Z1'){
        //                		return 'high-light-red';
        //                	}
        //                },
        //                enableCellEditOnFocus : false,
        //            },
        //            {
        //                name:"AUFART",
        //                displayName:'工单类型',
        //                minWidth:100,
        //                enableCellEdit : false,
        //                enableCellEditOnFocus : false,
        //                cellClass: function (row, col) {
        //                	if(col.entity.QMART=='Z1'){
        //                		return 'high-light-red';
        //                	}
        //                },
        //            },
        //            {
        //                name:"AUARTTEXT",
        //                displayName:'工单类型描述',
        //                minWidth:100,
        //                enableCellEdit : false,
        //                enableCellEditOnFocus : false,
        //                cellClass: function (row, col) {
        //                	if(col.entity.QMART=='Z1'){
        //                		return 'high-light-red';
        //                	}
        //                },
        //            },
        //            {
        //                name:"TXT04",
        //                displayName:'工单状态',
        //                minWidth:140,
        //                enableCellEdit : false,
        //                enableCellEditOnFocus : false,
        //                cellClass: function (row, col) {
        //                	if(col.entity.QMART=='Z1'){
        //                		return 'high-light-red';
        //                	}
        //                },
        //            },
        //            {
        //                name:"TXT30",
        //                displayName:'工单状态描述',
        //                minWidth:140,
        //                enableCellEdit : false,
        //                enableCellEditOnFocus : false,
        //                cellClass: function (row, col) {
        //                	if(col.entity.QMART=='Z1'){
        //                		return 'high-light-red';
        //                	}
        //                },
        //            },
        //            {
    		//    		field: 'edit',
    		//    		name: 'edit',
    		//    		displayName: '工单编辑',
    		//    		minWidth: 40,
    		//    		enableCellEdit: false,
    		//    		enableCellEditOnFocus: false,
    		//    		cellClass: function (row, col) {
        //                	if(col.entity.QMART=='Z1'){
        //                		return 'high-light-red';
        //                	}
        //                },
    		//    		enableColumnMenu: false,
    		//    		enableColumnMenus: false,
    		//    		visible: $scope.config.canEdit,
    		//    		cellTooltip: function(row, col){ return "编辑该行"; },
    		//    		cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col" ng-dblclick="fun()"><span class="glyphicon glyphicon-pencil"></span></div>',
    		//    		headerTemplate: '<div class="xbr-delete-col-header">编辑</div>'
    		//    	}
        //        ],
        //        onRegisterApi: __onRegisterApi
        //    };
        $scope.workOrderGrid={
            data:[],
            enableCellEdit:false,
            columnDefs:[
                {
                    name:"QMNUM",
                    displayName:'通知单号',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass: function (row, col) {
                        //if(col.entity.QMART=='Z1'){
                        //    return 'high-light-red';
                        //}
                    },
                    cellTooltip : function(row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"QMTXT",
                    displayName:'通知单描述',
                    minWidth:150,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass: function (row, col) {
                        //if(col.entity.QMART=='Z1'){
                        //    return 'high-light-red';
                        //}
                    },
                    cellTooltip : function(row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"QMART",
                    displayName:'通知单类型',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass: function (row, col) {
                        //if(col.entity.QMART=='Z1'){
                        //    return 'high-light-red';
                        //}
                    },
                    cellTooltip : function(row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"QMARTX",
                    displayName:'通知单类型描述',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass: function (row, col) {
                        //if(col.entity.QMART=='Z1'){
                        //    return 'high-light-red';
                        //}
                    },
                    cellTooltip : function(row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"GSTRP",
                    displayName:'计划日期',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass: function (row, col) {
                        //if(col.entity.QMART=='Z1'){
                        //    return 'high-light-red';
                        //}
                    },
                    cellTooltip : function(row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"AUFNR",
                    displayName:'工单号',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass: function (row, col) {
                        //if(col.entity.QMART=='Z1'){
                        //    return 'high-light-red';
                        //}
                    },
                    cellTooltip : function(row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"KTEXT",
                    displayName:'工单描述',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass: function (row, col) {
                        //if(col.entity.QMART=='Z1'){
                        //    return 'high-light-red';
                        //}
                    },
                    cellTooltip : function(row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"AUFART",
                    displayName:'工单类型',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass: function (row, col) {
                        //if(col.entity.QMART=='Z1'){
                        //    return 'high-light-red';
                        //}
                    },
                    cellTooltip : function(row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"AUARTTEXT",
                    displayName:'工单类型描述',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass: function (row, col) {
                        //if(col.entity.QMART=='Z1'){
                        //    return 'high-light-red';
                        //}
                    },
                    cellTooltip : function(row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"TXT04",
                    displayName:'工单状态',
                    minWidth:140,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass: function (row, col) {
                        //if(col.entity.QMART=='Z1'){
                        //    return 'high-light-red';
                        //}
                    },
                    cellTooltip : function(row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"TXT30",
                    displayName:'工单状态描述',
                    minWidth:140,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass: function (row, col) {
                        //if(col.entity.QMART=='Z1'){
                        //    return 'high-light-red';
                        //}
                    },
                    cellTooltip : function(row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name: 'edit',
                    displayName: '工单编辑',
                    minWidth: 40,
                    enableCellEdit: false,
                    enableCellEditOnFocus: false,
                    cellClass: function (row, col) {
                        //if(col.entity.QMART=='Z1'){
                        //    return 'high-light-red';
                        //}
                    },
                    enableColumnMenu: false,
                    enableColumnMenus: false,
                    cellTemplate: function(){
                        return '<div class="ui-grid-cell-contents ad-delete-col"><span class="glyphicon glyphicon-pencil"></span></div>';
                    },
                }
            ],
            onRegisterApi: __onRegisterApi
        };
        function __onRegisterApi(gridApi){
        	$scope.config.gridApi = gridApi;
        	
            //gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
    		//	if(newRowCol.col.field == "edit" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
    		//		edit(newRowCol.row.entity.AUFNR);
    		//	}
    		//});
            $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                gridRow.entity.isSelected = gridRow.isSelected;
                console.info(gridRow);
            });
            $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                for(var i = 0; i < gridRows.length; i++){
                    gridRows[i].entity.isSelected = gridRows[i].isSelected;
                }
                console.info(22222);
            });
        }
        function edit(QMNUM){
            var mode="2";
            if(!$scope.config.resourceMeStaff || $scope.config.meStaffLocked == "true"){
                mode="0";
            }
            var params={
                IUSER:$scope.config.meStaffName,
                MODE:mode,
                AUFNR:QMNUM,
            }
        	clientService.__plcEditWorkOrder(params).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200){
        			window.open(resultDatas.response.url);   
                    return;
                }
        	},function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        function changeMeStaff(params){
        	clientService.__plcChangeOperator(params,1).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200){
        			console.info(resultDatas.response);
        			if(resultDatas.response.code==0){
        				$scope.addAlert("success","更新成功");
        				//initResourceInfo();
                        writeUserLogInfo(params,1);
        			}else{
                        $scope.addAlert('danger',resultDatas.response.msg);
                    }
                }
            },function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }

        //记录用户登入登出日志  第二个参数代表 1 更换或者维护  0 注销
        function writeUserLogInfo(params,code){
            if($scope.config.resourceMeStaff)
            {
                var paramLoginOut={
                    "site":ROOTCONFIG.AndonConfig.SITE,
                    "user":$scope.config.resourceMeStaff,
                    "resrce":$scope.config.resource,
                    "actionCode":"LOGOUT",
                    "loginMode":"ONSITE",
                    "userDepartment":"ME"
                };

                clientService._plcUserLogInfo(paramLoginOut,0).then(function (resultDatas){
                    if(resultDatas.myHttpConfig.status == 200){
                        if(code){
                            UtilsService.log("第二次执行");
                            paramLoginOut.actionCode="LOGIN";
                            paramLoginOut.user=params.userName;
                            clientService._plcUserLogInfo(paramLoginOut,1).then(function (resultDatas){
                                if(resultDatas.myHttpConfig.status == 200){
                                    initResourceInfo();
                                }
                            },function (resultDatas){
                                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                            });
                        }else {
                            initResourceInfo();
                        }
                    }
                },function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
            }else{
                var paramLoginOut={
                    "site":ROOTCONFIG.AndonConfig.SITE,
                    "user":params.userName,
                    "resrce":$scope.config.resource,
                    "actionCode":"LOGIN",
                    "loginMode":"ONSITE",
                    "userDepartment":"ME"
                };
                clientService._plcUserLogInfo(paramLoginOut,1).then(function (resultDatas){
                    if(resultDatas.myHttpConfig.status == 200){
                        initResourceInfo();
                    }
                },function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
            }

        }
        //解锁
        function unlocked(params){
        	params.type="meUser";
        	params.meStaffLocked="false";
        	clientService.__changeLocked(params).then(function (data){
        		if(data.myHttpConfig.status == 200&&data.response){
        			if(data.response.code==0){
        				$scope.addAlert("success","解锁成功");
        				initResourceInfo();
        			}else if(data.response.code==1){
        				$scope.addAlert('danger', "用户名或密码错误");
        			}
                }
            },function (data){
                $scope.addAlert('danger', data.myHttpConfig.statusDesc);
            });
        }
        //维修结果确认接口
        function maintenanceRC(){
        	var params={
        		IANDONUSR:'',
        		IRESULT:'',
        		items:[{"AUFNR":""}],
        	}
        	clientService.__maintenanceResultConfirmation(params).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200&&resultDatas.response.length>0){
        			$scope.addAlert("success","接口调用success");
                }
            },function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        function initResourceInfo(){
        	clientService._plcInitResourceInfo().then(function (resultDatas){
        		if(resultDatas.response){
        			$scope.config.resource=resultDatas.response.resource;
                    $scope.config.resourceName=resultDatas.response.resource+resultDatas.response.resourceName;
                    $scope.config.resourceMeStaff=resultDatas.response.resourceMeStaff;
                    $scope.config.meStaffLocked=resultDatas.response.meStaffLocked;
                    $scope.config.resourceCurrentData=resultDatas.response;
                    getUserInfo($scope.config.resourceMeStaff);
                    initData($scope.config.resource);
                    return;
                }
        	},function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        function getUserInfo(userId){
        	clientService.__getUserInfo(userId).then(function (resultDatas){
                 if(resultDatas.response){
                	 var lastName="";
                	 var firstName="";
                	 if(!UtilsService.isNull(resultDatas.response.serverUserDetailsVO.lastName)){
                		 lastName=resultDatas.response.serverUserDetailsVO.lastName
                	 }
                	 if(!UtilsService.isNull(resultDatas.response.serverUserDetailsVO.firstName)){
                		 firstName=resultDatas.response.serverUserDetailsVO.firstName;
                	 }
                	 $scope.config.meStaffName=lastName+firstName;
                     return;
                 }
             },function (resultDatas){
                 $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
             });
        }
        function initData(resource){
        	$scope.showBodyModel("正在加载数据,请稍后......");
        	var params={
        		ZSTATE:'E',
        		items:[{"EQUNR":resource}],
        	}
        	clientService.__plcInitData(params).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200&&resultDatas.response){
        			if(resultDatas.response.length==1&&resultDatas.response[0].MSG=='没有符合条件的数据'){
    					$scope.addAlert('', '没有待处理的通知单、工单！');
    					$scope.workOrderGrid.data=[];
    				}else{
    					$scope.workOrderGrid.data=resultDatas.response;
    				}
        			$scope.hideBodyModel();
                }
            },function (resultDatas){
            	$scope.hideBodyModel();
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }

        $scope.$on(uiGridCellNavConstants.CELL_NAV_EVENT, function (evt, rowCol, modifierDown) {
            UtilsService.log("进入监听事件多次");
            if(rowCol)
            {
                if(rowCol.col.field == "edit" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'&&!UtilsService.isNull(rowCol.row.entity.AUFNR)){
                    edit(rowCol.row.entity.AUFNR);
                }
            }
        });
    }]);