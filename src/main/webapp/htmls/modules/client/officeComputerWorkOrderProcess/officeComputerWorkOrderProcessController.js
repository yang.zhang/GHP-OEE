clientModule.controller('officeComputerWorkOrderProcessController', [
    '$scope', '$http', '$q',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants','$uibModal', 'gridUtil','UtilsService',
    'clientService','$state','uiGridCellNavConstants','statementService','yieldMaintainService',
    function ($scope, $http, $q, HttpAppService, $timeout, $interval,
              uiGridConstants,$uibModal,gridUtil,UtilsService,clientService,$state,uiGridCellNavConstants,statementService,yieldMaintainService){

        $scope.config = {
           user:'',
           userName:'',
           resource:'',                            //设备编码
           isShwoMeReceiveOrderBtn:false,          //是否显示接单按钮
           //meReceiveOrderBtn:true,				//是否禁用接单按钮	
           meReceiveOrderData:[],
           resourceSeacrhIcon:true,
           queryBtn:true,                           //是否禁用查询按钮
           type:'F',
           modeCode:"",
            workCenters:[],
            currentWorkCenter:[],
            currentWorkCenterDesc:[],
            //resourceSeacrhIcon:true,
            lines:[],
            currentLine:[],
            currentLineDesc:[],

            deviceTypes:[],
            currentDeviceType:[],
            currentDeviceTypeDesc:[],

            workCenter: null,
            line: null,
            showSelectLine: true,
            deviceCode : true,
            showSelectArea : true,
            deviceType : true,
            showLineDesc : false,
            showDeviceDesc : false,
            deviceTypeDesc : null,
            $workCenterSelect: null,
            $lineSelect: null,
            $deviceTypeSelect: null,
            $deviceNumSelect: null,
            //
            //btnDisabledQuery: false,
            //设备状态明细查询按钮
            rsdQueryBtn:true,

            localResource:[],

        };
        $scope.statementConfig={
            localResourceType:[],
        }

        $scope.disableBtn=function (){
        	if(UtilsService.isNull($scope.config.resource)){
        		$scope.config.queryBtn=true;
        	}else{
        		$scope.config.queryBtn=false;
        	}
        };
        //生产区域帮助
        $scope.toAreaHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/areaSelectMoreModal.html',
                controller: 'areaSelectMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectAreaModal: function () {
                        return {workArea : $scope.config.workCenters};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.currentWorkCenter = selectedItem.area.join(",");
                $scope.config.currentWorkCenterDesc = selectedItem.areaName.join(",");
                $scope.config.showSelectArea = false;
                $scope.config.queryBtn=false;
                $scope.deleteAll('line');
                $scope.deleteAll('deviceCode');
            }, function () {
            });
        };
        //搜索设备
        $scope.searchResource = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/client/modal/modalWorkOrderResourceSearch.html',
                controller: 'WorkOrderResourceSearchController',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectMoreDevice: function () {
                        return {
                            workArea: $scope.config.currentWorkCenter,
                            workLine: $scope.config.currentLine,
                            deviceCode : $scope.config.currentDeviceType
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.resource = selectedItem.area;
                if($scope.config.resource.length >0){
                    $scope.config.resourceSeacrhIcon = false;
                }
                $scope.config.queryBtn=false;
            }, function () {
            });
        };
        //拉线多选帮助
        $scope.toWireLineHelp = function () {
            /*if($scope.statementConfig.currentWorkCenter == ''){
             $scope.addAlert('','请先选择生产区域');
             return;
             }*/
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/lineSelectModal.html',
                controller: 'LineSelectMoreCtrl',
                size: 'lg',
                backdrop: 'false',
                scope: $scope,
                openedClass: 'flex-center-parent',
                resolve: {
                    selectLineModal: function () {
                        return {
                            workArea: $scope.config.currentWorkCenter,
                            workLine: $scope.config.lines
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.currentLine = selectedItem.line;
                $scope.config.currentLineDesc = selectedItem.lineName;
                if($scope.config.currentLine.length >0){
                    $scope.config.showSelectLine = false;
                }
                $scope.deleteAll('deviceCode');
                $scope.config.queryBtn=false;
                //$scope.lineChanged('T');
            }, function () {
            });
        };
        //设备类型帮助
        $scope.toDeviceTypeHelp = function(){
            /*if($scope.statementConfig.currentWorkCenter == ''){
             $scope.addAlert('','请先选择生产区域');
             return;
             }*/
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/deviceTypeSelectMoreModal.html',
                controller: 'deviceTypeMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectTypeMoreDevice: function () {
                        return {
                            deviceCode : $scope.config.deviceTypes
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.currentDeviceType = selectedItem.resourceType;
                $scope.config.currentDeviceTypeDesc = selectedItem.resourceTypeDesc;
                if($scope.config.currentDeviceType.length >0){
                    $scope.config.deviceType = false;
                }
                $scope.deleteAll('deviceCode');
                $scope.config.queryBtn=false;
                $scope.deviceTypeChanged('T');
            }, function () {
            });
        };

        //$scope.deleteResource=function(){
        //	$scope.config.resource='';
        //	$scope.config.resourceSeacrhIcon=true;
        //}
        $scope.selectedType=function(type){
        	if(type=='F'){
        		$scope.config.type='F';
        	}else if(type=='E'){
        		$scope.config.type='E';
        	}
        }
        $scope.deleteAll = function(item){
            if(item == 'area'){
                $scope.config.currentWorkCenter = [];
                $scope.config.currentWorkCenterDesc = [];
                $scope.config.currentLine = [];
                $scope.config.currentLineDesc = [];
                $scope.config.deviceNums = [];
                $scope.config.currentDeviceNum = null;
                $scope.config.resource = [];
                $scope.config.showSelectArea = true;
                $scope.config.showSelectLine = true;
                $scope.config.deviceCode = true;
                $scope.config.resourceSeacrhIcon=true;
                //$scope.config.btnDisabledQuery = true;
            }
            if(item == 'line'){
                $scope.config.currentLine = [];
                $scope.config.currentLineDesc = [];
                $scope.config.deviceNums = [];
                $scope.config.currentDeviceNum = null;
                $scope.config.resource = [];
                $scope.config.showSelectLine = true;
                $scope.config.deviceCode = true;
                $scope.lineChanged('T');
                $scope.config.resourceSeacrhIcon=true;

            }
            if(item == 'deviceType'){
                $scope.config.currentDeviceType = [];
                $scope.config.currentDeviceTypeDesc = [];
                $scope.config.resource = [];
                $scope.config.deviceType = true;
                $scope.config.deviceCode = true;
                $scope.deviceTypeChanged('T');
                $scope.config.resourceSeacrhIcon=true;

            }
            if(item == 'deviceCode'){
                $scope.config.resource = [];
                $scope.config.deviceCode = true;
                $scope.config.resourceSeacrhIcon=true;

            }
            if($scope.config.currentWorkCenter == ''&&$scope.config.currentLine== ''&&$scope.config.currentDeviceType== ''&&$scope.config.resource==''){
                $scope.config.queryBtn=true;
            }

        }
        $scope.init = function(){
        	//界面打开，则加载数据
        	//initResourceInfo();
        	getLogonUser();
        };
        $scope.init();
        $scope.getHeight=function(){
        	if ($scope.workOrderGrid.data.length > 0) {
                var rowHeight = 31;
                var headerHeight = 50;
                return {
                    height: (15 * rowHeight + headerHeight) + "px"
                };
            }
        }
		//me接单
        $scope.meReceiveOrder=function(){
        	var items=[];
        	for(var i=0;i<$scope.workOrderGrid.data.length;i++){
        		if($scope.workOrderGrid.data[i].isSelected == true){
        			if((!UtilsService.isNull($scope.workOrderGrid.data[i].QMNUM)&&UtilsService.isNull($scope.workOrderGrid.data[i].AUFNR))
	        				  ||($scope.workOrderGrid.data[i].TXT04=='待接单')){
        				var obj={};
            			obj.AUFNR=$scope.workOrderGrid.data[i].QMNUM;
            			items.push(obj);
        			}else{
        				$scope.addAlert("danger","禁止重复接单");
        				return;
        			}
        		}
        	}
        	if(items.length==0){
        		$scope.addAlert("danger","选择单号不能为空");
        		return;
        	}
    		var params={
    			IANDONUSER:$scope.config.userName,
    			IZSPOT:'1',                               //0代表现场接单，1代表远程接单
    			items:items,                               //UtilsService.removeDul(items)
    			IRELEASE:'X'
        	}
        	clientService.__meReceiveOrder(params).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200&&resultDatas.response){
        			if(resultDatas.response.e_SUBRC==0){
        				$scope.addAlert("success",resultDatas.response.e_MSG);
        				initData();
        			}else if(resultDatas.response.e_SUBRC==1){
        				$scope.addAlert("danger",resultDatas.response.e_MSG);
        			}
                }
            },function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        $scope.queryData=function (){
            $timeout.cancel(window.recTime);
        	initData();
        }
        $scope.workOrderGrid={
            	data:[],
                enableCellEdit:false,
                columnDefs:[
					{
					    name:"handle",visible:false
					},
					{
                        name:"EQUNR",
                        displayName:'设备编码',
                        minWidth:100,
                        enableCellEdit : false,
                        cellClass: function (row, col) {
                        	//if(col.entity.QMART=='Z1'){
                        	//	return 'high-light-red';
                        	//}
                        },
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"QMNUM",
                        displayName:'通知单号',
                        minWidth:100,
                        enableCellEdit : false,
                        cellClass: function (row, col) {
                        	//if(col.entity.QMART=='Z1'){
                        	//	return 'high-light-red';
                        	//}
                        },
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"QMTXT",
                        displayName:'通知单描述',
                        minWidth:150,
                        enableCellEdit : false,
                        cellClass: function (row, col) {
                        	//if(col.entity.QMART=='Z1'){
                        	//	return 'high-light-red';
                        	//}
                        },
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"QMART",
                        displayName:'通知单类型',
                        minWidth:100,
                        enableCellEdit : false,
                        cellClass: function (row, col) {
                        	//if(col.entity.QMART=='Z1'){
                        	//	return 'high-light-red';
                        	//}
                        },
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"QMARTX",
                        displayName:'通知单类型描述',
                        minWidth:100,
                        enableCellEdit : false,
                        cellClass: function (row, col) {
                        	//if(col.entity.QMART=='Z1'){
                        	//	return 'high-light-red';
                        	//}
                        },
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"GSTRP",
                        displayName:'计划日期',
                        minWidth:100,
                        enableCellEdit : false,
                        cellClass: function (row, col) {
                        	//if(col.entity.QMART=='Z1'){
                        	//	return 'high-light-red';
                        	//}
                        },
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"AUFNR",
                        displayName:'工单号',
                        minWidth:100,
                        enableCellEdit : false,
                        cellClass: function (row, col) {
                        	//if(col.entity.QMART=='Z1'){
                        	//	return 'high-light-red';
                        	//}
                        },
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"KTEXT",
                        displayName:'工单描述',
                        minWidth:100,
                        enableCellEdit : false,
                        cellClass: function (row, col) {
                        	//if(col.entity.QMART=='Z1'){
                        	//	return 'high-light-red';
                        	//}
                        },
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"AUFART",
                        displayName:'工单类型',
                        minWidth:100,
                        enableCellEdit : false,
                        cellClass: function (row, col) {
                        	//if(col.entity.QMART=='Z1'){
                        	//	return 'high-light-red';
                        	//}
                        },
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"AUARTTEXT",
                        displayName:'工单类型描述',
                        minWidth:100,
                        enableCellEdit : false,
                        cellClass: function (row, col) {
                        	//if(col.entity.QMART=='Z1'){
                        	//	return 'high-light-red';
                        	//}
                        },
                        enableCellEditOnFocus : false,
                        cellClass:"text-align-right",
                    },
                    {
                        name:"TXT04",
                        displayName:'工单状态',
                        minWidth:140,
                        enableCellEdit : false,
                        cellClass: function (row, col) {
                        	//if(col.entity.QMART=='Z1'){
                        	//	return 'high-light-red';
                        	//}
                        },
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"TXT30",
                        displayName:'工单状态描述',
                        minWidth:140,
                        cellClass: function (row, col) {
                        	//if(col.entity.QMART=='Z1'){
                        	//	return 'high-light-red';
                        	//}
                        },
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
    		    		field: 'edit', 
    		    		name: 'edit', 
    		    		displayName: '工单编辑',
    		    		minWidth: 40,
    		    		enableCellEdit: false, 
    		    		cellClass: function (row, col) {
                        	//if(col.entity.QMART=='Z1'){
                        	//	return 'high-light-red';
                        	//}
                        },
    		    		enableCellEditOnFocus: false,
    		    		enableColumnMenu: false, 
    		    		enableColumnMenus: false,
    		    		visible: $scope.config.canEdit,
    		    		cellTooltip: function(row, col){ return "编辑该行"; }, 
    		    		cellTemplate: '<div class="ui-grid-cell-contents ad-delete-col" ng-dblclick="fun()"><span class="glyphicon glyphicon-pencil"></span></div>',
    		    		headerTemplate: '<div class="xbr-delete-col-header">编辑</div>'
    		    	}
                ],
                onRegisterApi: __onRegisterApi
            };
        function __onRegisterApi(gridApi){
        	$scope.config.gridApi = gridApi;
        	
            //gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
    		//	if(newRowCol.col.field == "edit" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'){
    		//		edit(newRowCol.row.entity.AUFNR);
    		//	}
    		//});
            $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                gridRow.entity.isSelected = gridRow.isSelected;
                if(gridRow.isSelected==true){
                	$scope.config.meReceiveOrderData.push(gridRow.entity);
                }
            });
            $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                for(var i = 0; i < gridRows.length; i++){
                    gridRows[i].entity.isSelected = gridRows[i].isSelected;
                }
            });
        }
        function getLogonUser(){
        	clientService.__getLogonUser().then(function (resultDatas){
                 if(resultDatas.response){
                	 $scope.config.user=resultDatas.response.user;
                	 $scope.config.userName=resultDatas.response.lastName+resultDatas.response.firstName;
                     return;
                 }
             },function (resultDatas){
                 $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
             });
        }
        function edit(qmnum){
        	var params={
        			IUSER:$scope.config.userName,
        			MODE:$scope.config.modeCode,
        			AUFNR:qmnum,
        	}
        	clientService.__editWorkOrder(params).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200){
        			window.open(resultDatas.response.url);   
                    return;
                }
        	},function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        function initData(){
            //$scope.showBodyModel("正在加载数据,请稍后...");
            if($scope.config.currentWorkCenter == ''&&$scope.config.currentLine== ''&&$scope.config.currentDeviceType== ''&&$scope.config.resource==''){
                //$scope.addAlert('','查询条件不可以为空');
                $scope.config.queryBtn=true;
            }else{
                $scope.config.queryBtn=false;
            }

            var workCenter = $scope.config.currentWorkCenter;
            if(workCenter == '' || workCenter == null){
                workCenter = [];
            }else{
                workCenter = workCenter.split(',');
            }

            var line = [];
            for (var i = 0; i < $scope.config.currentLine.length; i++) {
                line.push($scope.config.currentLine[i]);
            }
            console.log($scope.config.currentLine);
            var deviceType = $scope.config.currentDeviceType == null ? "" : $scope.config.currentDeviceType;
            if($scope.config.currentDeviceType == '' || $scope.config.currentDeviceType == null){
                $scope.config.showDeviceDesc = false;
            }else{
                $scope.config.showDeviceDesc = true;
            }
            $scope.config.deviceTypeDesc = angular.copy($scope.config.currentDeviceTypeDesc);
            if($scope.config.deviceTypeDesc.length>1){
                $scope.config.deviceTypeDesc = $scope.config.deviceTypeDesc[0]+'...';
            }else{
                $scope.config.deviceTypeDesc = $scope.config.deviceTypeDesc[0];
            }
            var deviceCode =$scope.config.resource;

            var desString = [];
            for (var i = 0; i < $scope.config.currentLine.length; i++) {
                desString[i] = $scope.config.currentLine[i];
            }
            $scope.config.workCenter = null;
            $scope.config.line = null;

            /*$scope.config.workCenter = $scope.statementConfig.currentWorkCenterDesc.split(',');
             if($scope.config.workCenter.length>1){
             $scope.config.workCenter = $scope.config.workCenter[0]+'...';
             }else{
             $scope.config.workCenter = $scope.config.workCenter[0];
             }*/

            if($scope.config.currentLineDesc.length>0){
                $scope.config.showLineDesc = true;
            }else{
                $scope.config.showLineDesc = false;
            }
            $scope.config.line = $scope.config.currentLineDesc;
            if($scope.config.line.length>1){
                $scope.config.line = $scope.config.line[0]+'...';
            }else{
                $scope.config.line = $scope.config.line[0];
            }
            //当直接在输入框中输入字符串，需要帮字符串转化成数组
            var array=$scope.config.resource;

            var items=[];

            if(array)
            {
                if(array.length>0)
                {
                    if(Object.prototype.toString.call(array) === '[object Array]')
                    {
                        for(var i=0;i<array.length;i++){
                            var obj={};
                            obj.EQUNR=array[i];
                            items.push(obj);
                        }
                    }
                    else{
                        var newRes=array.toUpperCase().replace(/，/ig,',');
                        var newResArray=newRes.split(",");
                        for(var i=0;i<newResArray.length;i++){
                            var obj={};
                            obj.EQUNR=newResArray[i];
                            items.push(obj);
                        }
                    }
                }
            }

        	var params={
        		ZSTATE:$scope.config.type,
        		items:items,
                work_center:workCenter,
                line:line,
                deviceType:deviceType
        	}
        	clientService.__initData(params).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200&&resultDatas.response){
                    if($scope.config.type == 'F'){
                        $scope.config.modeCode="1";
                    }
                    else if($scope.config.type == 'E'){
                        $scope.config.modeCode="2";
                    }
        			$scope.workOrderGrid.data=resultDatas.response;
        			$scope.config.isShwoMeReceiveOrderBtn=false;
        			for(var i=0;i<$scope.workOrderGrid.data.length;i++){
        				if(resultDatas.response.length==1&&resultDatas.response[0].MSG=='没有符合条件的数据'){
        					$scope.addAlert('', '没有待处理的通知单、工单！');
        					$scope.workOrderGrid.data=[];
        				}else{
        					if((!UtilsService.isNull($scope.workOrderGrid.data[i].QMNUM)&&UtilsService.isNull($scope.workOrderGrid.data[i].AUFNR))
	        				  ||($scope.workOrderGrid.data[i].TXT04=='待接单')){
        							$scope.config.isShwoMeReceiveOrderBtn=true;
	        				}
        				}
        			}
        			$scope.hideBodyModel();
                }
            },function (resultDatas){
            	$scope.hideBodyModel();
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        //设备状态明细报表S,设备实时看板T
        $scope.workCenterChanged = function(type){
            __initDropdowns('workCenterChanged');
            //拉线只有选择了某个生产区域后，拉线下拉选择列表才有相应的拉线数据可选；
            var workCenter = $scope.config.currentWorkCenter;
            __requestDropdownLines(workCenter);
            __requestDropdownShifts(workCenter);
            var line = [];
            for(var i=0;i<$scope.config.currentLine.length;i++){
                line.push($scope.config.currentLine[i]);
            }                var resourceType = null;
            if($scope.config.currentDeviceType){
                resourceType = $scope.config.currentDeviceType.resourceType;
            }
            __requestDropdownDeviceCodes(workCenter, line, resourceType);
            // }
            __setBtnsDisable(type);
        };
        $scope.arrayToString = function(array){
            var string = [];
            for(var i=0;i<array.length;i++){
                string[i] = array[i].workCenter;
            }
            return string.toString();
        };

        function __initDropdowns(type){
            if(type == 'init'){
                $scope.config.workCenters = [];
                $scope.config.currentWorkCenter = null;

                $scope.config.deviceTypes = [];
                $scope.config.currentDeviceType = null;
            }

            if(type == 'init' || type == 'workCenterChanged'){
                $scope.config.lines = [];
                $scope.config.currentLine = [];
                $scope.config.deviceNums = [];
                $scope.config.currentDeviceNum = null;
                $scope.config.resource = [];
                $scope.config.shifts = [];
                $scope.config.currentShift = null;
            }

            if(type == 'lineChanged'){
                $scope.config.deviceNums = [];
                $scope.config.currentDeviceNum = null;
                $scope.config.resource = [];
            }

            if(type == 'deviceTypeChanged'){
                $scope.config.deviceNums = [];
                $scope.config.currentDeviceNum = null;
                $scope.config.resource = [];
            }

        } ;
        function __requestDropdownWorkCenters(){
            plcService
                .dataWorkCenters()
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.workCenters = [];
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = resultDatas.response[i].description + "  代码:"+resultDatas.response[i].workCenter;

                        }
                        $scope.config.workCenters=resultDatas.response;
                        if($scope.config.currentWorkCenter == null || $scope.config.currentWorkCenter == ''){
                        }else{
                        }
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    if(resultDatas.myHttpConfig.isSessionOut == true || resultDatas.myHttpConfig.isTimeout == true || resultDatas.myHttpConfig.status == 1 || resultDatas.myHttpConfig.status == -1){
                    }else{
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    }
                });
        };

        function __requestDropdownLines(workCenter){
            statementService
                .outputReportLineHelp('',workCenter)
                .then(function (resultDatas){
                    $scope.config.lines = [];
                    if(resultDatas.response && resultDatas.response.length > 0){
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = resultDatas.response[i].description + "  代码:"+resultDatas.response[i].workCenter;
                            $scope.config.lines.push(resultDatas.response[i]);

                        }
                        return;
                    }
                    $scope.config.lines = [];
                },function (resultDatas){ //TODO 检验失败
                    $scope.config.lines = [];
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };

        function __requestDropdownDeviceTypes(){
            plcService
                .dataResourceTypes()
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.deviceTypes = [];
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = resultDatas.response[i].description + " 代码:"+resultDatas.response[i].resourceType;
                            $scope.config.deviceTypes.push(resultDatas.response[i]);
                        }
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    if(resultDatas.myHttpConfig.isSessionOut == true || resultDatas.myHttpConfig.isTimeout == true || resultDatas.myHttpConfig.status == 1 || resultDatas.myHttpConfig.status == -1){
                    }else{
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    }
                });
        };

        function __requestDropdownDeviceCodes(workCenter, line, resourceType){
            yieldMaintainService
                .requestDataResourceCodesByLine(workCenter, line, resourceType)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.deviceNums = [];
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = resultDatas.response[i].description + " 代码:"+resultDatas.response[i].resrce;
                            $scope.config.deviceNums.push(resultDatas.response[i]);
                        }
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };

        $scope.deviceTypeChanged = function(){
            __initDropdowns('deviceTypeChanged');
            var workCenter = $scope.config.currentWorkCenter;
            var line = [];
            for(var i=0;i<$scope.config.currentLine.length;i++){
                line.push($scope.config.currentLine[i]);
            }                var resourceType = null;
            if($scope.config.currentDeviceType){
                resourceType = $scope.config.currentDeviceType;
            }

            __requestDropdownDeviceCodes(workCenter, line, resourceType);
        };

        $scope.lineChanged = function(type){
            __initDropdowns('lineChanged');
            var workCenter = $scope.config.currentWorkCenter;
            var line = [];
            for(var i=0;i<$scope.config.currentLine.length;i++){
                line.push($scope.config.currentLine[i]);
            }
            var resourceType = null;
            if($scope.config.currentDeviceType){
                resourceType = $scope.config.currentDeviceType;
            }
            __requestDropdownDeviceCodes(workCenter, line, resourceType);
            __setBtnsDisable(type);
        };

        $scope.dateChanged = function(type){
            __setBtnsDisable(type);
        };

        //设备状态明细报表S,设备实时看板T
        function __setBtnsDisable(type){
            if(type == 'S'){
                if(!$scope.config.currentWorkCenter
                    || !$scope.config.startDate
                    || !$scope.config.endDate){
                    $scope.config.btnDisabledQuery = true;
                    return;
                }
            }else if(type == 'T'){
                if(!$scope.config.currentWorkCenter){
                    $scope.config.btnDisabledQuery = true;
                    return;
                }
            }

            $scope.config.btnDisabledQuery = false;
        };
        $scope.upModel = function(){
            $scope.config.currentDeviceNum = angular.uppercase($scope.config.currentDeviceNum);
            $scope.config.resource = angular.uppercase($scope.config.resource);
        }
        function __requestGridApi(gridApi){
            $scope.config.gridApi = gridApi;
        }
        $scope.$on(uiGridCellNavConstants.CELL_NAV_EVENT, function (evt, rowCol, modifierDown) {
            UtilsService.log("进入监听事件多次");
            if(rowCol)
            {
                if(rowCol.col.name == "edit" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'&&!UtilsService.isNull(rowCol.row.entity.AUFNR)){
                    edit(rowCol.row.entity.AUFNR);
                }
            }
        });
    }]);