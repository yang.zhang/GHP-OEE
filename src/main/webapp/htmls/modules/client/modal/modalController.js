//物料编码帮助
statementModule.controller('itemListController', ['$scope','$uibModalInstance','statementService','clientService','passItemList','UtilsService','$timeout',function ($scope, $uibModalInstance,statementService,clientService,passItemList,UtilsService,$timeout) {
    $scope.config = {
        confirmBtn : true,
        multiSelect: passItemList.multiSelect,
        codeHelp:null,
        localItem:[],
        linkIndexStatus:passItemList.linkIndexStatus,
        isFirstSelect:true
    };
    $scope.itemListGrid = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        multiSelect:$scope.config.multiSelect,
        columnDefs: [
            {
                field: 'item', name: 'item', displayName: '物料编码', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'description', name: 'description', displayName: '描述', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'model', name: 'model', displayName: 'Model', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field:"enabled",name:"enabled",displayName:'是否已启用',width: 150,
                cellTooltip: function(row, col){ return "是否已启用"; },
                enableCellEdit: false, enableCellEditOnFocus: false,
                enableColumnMenu: false, enableColumnMenus: false,
                cellTemplate:'andon-ui-grid-rscg/statement-checkbox'
            }

        ],data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    };
    function __selectAny(){
        for(var i=0;i<$scope.itemListGrid.data.length;i++){
            if($scope.itemListGrid.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.codeHelp = $scope.config.codeHelp?$scope.config.codeHelp.toUpperCase():'';

        if(keycode==13){
            $scope.filterItem();
        }
    };
    $scope.init = function(){
        $scope.itemListGrid.data = [];
        var model = passItemList.model?passItemList.model.split(","):'';
        var item = $scope.config.codeHelp?$scope.config.codeHelp:'';
        statementService
            .outputReportCodeHelp(model,item,$scope.config.linkIndexStatus)
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.itemListGrid.data = resultDatas.response;
                    $scope.config.localItem = resultDatas.response;
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }else{
                    $scope.itemListGrid.data = [];
                    $scope.addAlert('', '暂无数据!');
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
            });
    }
    $scope.init();
    $scope.filterItem=function(){
    	var item= $scope.config.codeHelp?$scope.config.codeHelp:'';
    	if(item==''){
    		$scope.itemListGrid.data=$scope.config.localItem;
    	}else{
    		var newRes=item.replace(/；/ig,';');
            var resourceArrary=newRes.split(";");
            var filterResourceArray=[];
            $scope.config.gridFilterArea=[];
            $scope.itemListGrid.data=$scope.config.localItem;
            for(var x=0;x<resourceArrary.length;x++){
            	if(resourceArrary[x]!=''){
            		filterResourceArray.push(resourceArrary[x]);
            	}
            }if(filterResourceArray.length==0){
            	$scope.itemListGrid.data=$scope.config.localItem;
            }else{
            	for(var i=0;i<$scope.itemListGrid.data.length;i++){
                	var desc=$scope.itemListGrid.data[i].item;
                	for(var j=0;j<filterResourceArray.length;j++){
                		if(desc.indexOf(filterResourceArray[j])!=-1){
                    		$scope.config.gridFilterArea.push($scope.itemListGrid.data[i]);
                    		break;
                    	}
                	}
                }
                $scope.itemListGrid.data=angular.copy($scope.config.gridFilterArea);
            }
    	}
    }
    $scope.ok = function () {
        var obj = {};
        var item=[];
        for(var i=0;i<$scope.itemListGrid.data.length;i++){
            if($scope.itemListGrid.data[i].isSelected == true){
            	item.push($scope.itemListGrid.data[i].item);
            	obj.item=$scope.itemListGrid.data[i].item;
            }
        }
        if(item.length>1){
        	$scope.addAlert("","只能选择一条数据!");
        	return;
        }
        $uibModalInstance.close(obj);
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
statementModule.controller('operatorLoginContrller', 
		['$scope','$uibModalInstance','statementService','userModel',
		 function ($scope, $uibModalInstance,statementService,userModel) {
			$scope.config = {
			        confirmBtn : true,
			        userName:'',
			        passWord:'',
			        title:userModel.modalTitle,
			        resourceOperator:userModel.resourceOperator,
			        resourceMeStaff:userModel.resourceMeStaff,
			    };
			 $scope.ok = function () {
				 var userName="";
				 if($scope.config.title=="解锁设备操作员" || $scope.config.title=="注销操作验证"){
					 userName=$scope.config.resourceOperator;
				 }else if($scope.config.title=="解锁ME工程师"){
					 userName=$scope.config.resourceMeStaff;
				 }else{
					 userName=$scope.config.userName;
				 }
				 var obj={
						 userName:userName,
					     passWord:$scope.config.passWord,
				 }
				 $uibModalInstance.close(obj);
			 };
		    $scope.cancel = function () {
		        $uibModalInstance.dismiss('cancel');
		    };
}]);
//alertWarnContrller
statementModule.controller('alertWarnController',
    ['$scope','$uibModalInstance','statementService','warnModel',
        function ($scope, $uibModalInstance,statementService,warnModel) {
            $scope.ok = function () {
                $uibModalInstance.close();
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }]);
statementModule.controller('createFaultNoticeController', 
		['$scope','$uibModalInstance','statementService','passFaultNotice','UtilsService',
		 function ($scope, $uibModalInstance,statementService,passFaultNotice,UtilsService) {
			$scope.config = {
					title:passFaultNotice.modalTitle,
			        passParams:passFaultNotice.resourceCurrentData,
			        operatorName:passFaultNotice.operatorName,
			        QMART:passFaultNotice.QMART,
			        QMART_DESC:passFaultNotice.QMART_DESC,
			        QMTXT:passFaultNotice.description ? passFaultNotice.description:"",
			    };
		    $scope.ok = function () {
		        var obj = $scope.config.passParams;
		        obj.EQUNR=obj.resource,             //设备编码
		        obj.QMART=$scope.config.QMART;                  //通知类型
		        obj.QMART_DESC=$scope.config.QMART_DESC;        //通知类型描述
		        obj.QMTXT=$scope.config.QMTXT;                  //通知单描述  
		        if(UtilsService.isNull(obj.QMTXT)){
		        	$scope.addAlert("danger","必输项不能为空");
		        	return;
		        }
		        $uibModalInstance.close(obj);
		    };
		    $scope.cancel = function () {
		        $uibModalInstance.dismiss('cancel');
		    };
}]);
//设备帮助
statementModule.controller('resourceController',['$scope','$uibModalInstance','statementService','yieldMaintainService','selectMoreDevice','$timeout', function ($scope, $uibModalInstance,statementService,yieldMaintainService,selectMoreDevice,$timeout) {
    $scope.config = {
        confirmBtn : true,
        deviceHelp:null,
        isFirstSelect:true
    };
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
             {
                 field: 'description', name: 'description', displayName: '描述', minWidth: 100,
                 enableCellEdit: false, enableCellEditOnFocus: false,
                 validators: { required: true },
                 cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

             },
			{
			    field: 'resrce', name: 'resrce', displayName: '设备编码', minWidth: 100,
			    enableCellEdit: false, enableCellEditOnFocus: false,
			    validators: { required: true },
			    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
			
			},
            {
                field: 'asset', name: 'asset', displayName: '资产号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'subAsset', name: 'subAsset', displayName: '子资产号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }
        ],
        data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    }
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.init=function(){
    	var workCenter=selectMoreDevice.workArea;
    	var line=selectMoreDevice.workLine;
    	var resourceType=selectMoreDevice.deviceCode;
        yieldMaintainService.requestDataResourceCodesByLine(workCenter, line, resourceType).then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                	$scope.gridModalAreaHelp.data=resultDatas.response;
                	$scope.localResource=resultDatas.response;
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
    }
    $scope.init();
    $scope.filterResource=function(){
    	var resource= $scope.config.resourceHelp?$scope.config.resourceHelp:'';
    	if(resource==''){
    		$scope.gridModalAreaHelp.data=$scope.localResource;
    	}else{
    		var newRes=resource.replace(/；/ig,';');
            var resourceArrary=newRes.split(";");
            var filterResourceArray=[];
            $scope.config.gridFilterArea=[];
            $scope.gridModalAreaHelp.data=$scope.localResource;
            for(var x=0;x<resourceArrary.length;x++){
            	if(resourceArrary[x]!=''){
            		filterResourceArray.push(resourceArrary[x]);
            	}
            }if(filterResourceArray.length==0){
            	$scope.gridModalAreaHelp.data=$scope.localResource;
            }else{
            	for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
                	var desc=$scope.gridModalAreaHelp.data[i].description;
                	for(var j=0;j<filterResourceArray.length;j++){
                		if(desc.indexOf(filterResourceArray[j])!=-1){
                    		$scope.config.gridFilterArea.push($scope.gridModalAreaHelp.data[i]);
                    		break;
                    	}
                	}
                }
                $scope.gridModalAreaHelp.data=angular.copy($scope.config.gridFilterArea);
            }
    	}
    }
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.resourceHelp = $scope.config.resourceHelp?$scope.config.resourceHelp.toUpperCase():'';
        if(keycode==13){
            $scope.filterResource();
        }
    };
    $scope.ok = function () {
        var selectdData = {};
        var item=[];
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                selectdData.resourceObj=$scope.gridModalAreaHelp.data[i];
                item.push(selectdData.resourceObj);
            }
        }
        if(item.length>1){
        	$scope.addAlert("","只能选择一条数据!");
        	return;
        }
        $uibModalInstance.close(selectdData);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//通知单工单处理---设备搜索
statementModule.controller('WorkOrderResourceSearchController',['$scope','$uibModalInstance','statementService','yieldMaintainService','selectMoreDevice','$timeout', function ($scope, $uibModalInstance,statementService,yieldMaintainService,selectMoreDevice,$timeout) {
    $scope.config = {
        confirmBtn : true,
        deviceHelp:null,
        isFirstSelect:true
    };
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
             {
                 field: 'description', name: 'description', displayName: '描述', minWidth: 100,
                 enableCellEdit: false, enableCellEditOnFocus: false,
                 validators: { required: true },
                 cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

             },
			{
			    field: 'resrce', name: 'resrce', displayName: '设备编码', minWidth: 100,
			    enableCellEdit: false, enableCellEditOnFocus: false,
			    validators: { required: true },
			    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
			
			},
            {
                field: 'asset', name: 'asset', displayName: '资产号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'subAsset', name: 'subAsset', displayName: '子资产号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }
        ],
        data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    }
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.init=function(){
    	var workCenter=selectMoreDevice.workArea;
    	var line=selectMoreDevice.workLine;
    	var resourceType=selectMoreDevice.deviceCode;
        yieldMaintainService.requestDataResourceCodesByLine(workCenter, line, resourceType).then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                	$scope.gridModalAreaHelp.data=resultDatas.response;
                	$scope.localResource=resultDatas.response;
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
    }
    $scope.init();
    $scope.filterResource=function(){
    	var resource= $scope.config.resourceHelp?$scope.config.resourceHelp:'';
    	if(resource==''){
    		$scope.gridModalAreaHelp.data=$scope.localResource;
    	}else{
    		var newRes=resource.replace(/；/ig,';');
            var resourceArrary=newRes.split(";");
            var filterResourceArray=[];
            $scope.config.gridFilterArea=[];
            $scope.gridModalAreaHelp.data=$scope.localResource;
            for(var x=0;x<resourceArrary.length;x++){
            	if(resourceArrary[x]!=''){
            		filterResourceArray.push(resourceArrary[x]);
            	}
            }if(filterResourceArray.length==0){
            	$scope.gridModalAreaHelp.data=$scope.localResource;
            }else{
            	for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
                	var desc=$scope.gridModalAreaHelp.data[i].description;
                	for(var j=0;j<filterResourceArray.length;j++){
                		if(desc.indexOf(filterResourceArray[j])!=-1){
                    		$scope.config.gridFilterArea.push($scope.gridModalAreaHelp.data[i]);
                    		break;
                    	}
                	}
                }
                $scope.gridModalAreaHelp.data=angular.copy($scope.config.gridFilterArea);
            }
    	}
    }
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.resourceHelp = $scope.config.resourceHelp?$scope.config.resourceHelp.toUpperCase():'';
        if(keycode==13){
            $scope.filterResource();
        }
    };
    $scope.ok = function () {
        var allDatas = {};
        var selectdDatas = [];
        var area = [];

        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                area.push($scope.gridModalAreaHelp.data[i].resrce);
                selectdDatas.push($scope.gridModalAreaHelp.data[i])
            }
        }
        allDatas.area = area;
        allDatas.selectdDatas = selectdDatas;

        $uibModalInstance.close(allDatas);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);