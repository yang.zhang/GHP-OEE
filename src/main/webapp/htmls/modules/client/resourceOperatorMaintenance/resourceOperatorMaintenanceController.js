clientModule.controller('resourceOperatorMaintenanceController', [
    '$scope', '$http', '$q',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants','$uibModal','gridUtil','UtilsService','clientService','$state','uiGridCellNavConstants',
    function ($scope, $http, $q, HttpAppService, $timeout, $interval,
              uiGridConstants,$uibModal,gridUtil,UtilsService,clientService,$state,uiGridCellNavConstants){
        $scope.config = {
           title:'设备操作员维护',
           resource:'',              //设备编码
           resourceBo:'',            //设备表handle
           resourceName:'',          //设备名称
           item:'',                  //物料编码
           itemName:'',              //物料名称
           resourceOperator:'',      //设备操作员工号
           operatorLocked:'',        //设备操作员是否锁定
           operatorName:'',          //姓名   
           
           resourceCurrentData:{},
		   disabledMeReceiverOrderBtn:false,
		   isShwoMeReceiveOrderBtn:false,   //是否显示接单按钮

		   repairForMeBtn:false,     //故障保修Me处理按钮
		   isShowMaintenanceResultOKBtn:false, //自主维修确认ok
		   cmcmMaintenanceResultOK:'',//工单编号
		   qmart:""
        };

		//定义ui-grid
		$scope.workOrderGrid={
			data:[],
			enableCellEdit:false,
			columnDefs:[
				{
					name:"QMNUM",
					displayName:'通知单号',
					minWidth:100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass: function (row, col) {
						if(col.entity.QMART=='Z6' && col.entity.TXT04 == "待处理"){
							return 'high-light-red';
						}
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				},
				{
					name:"QMTXT",
					displayName:'通知单描述',
					minWidth:150,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass: function (row, col) {
						if(col.entity.QMART=='Z6' && col.entity.TXT04 == "待处理"){
							return 'high-light-red';
						}
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				},
				{
					name:"QMART",
					displayName:'通知单类型',
					minWidth:100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass: function (row, col) {
						if(col.entity.QMART=='Z6' && col.entity.TXT04 == "待处理"){
							return 'high-light-red';
						}
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				},
				{
					name:"QMARTX",
					displayName:'通知单类型描述',
					minWidth:100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass: function (row, col) {
						if(col.entity.QMART=='Z6' && col.entity.TXT04 == "待处理"){
							return 'high-light-red';
						}
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				},
				{
					name:"GSTRP",
					displayName:'计划日期',
					minWidth:100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass: function (row, col) {
						if(col.entity.QMART=='Z6' && col.entity.TXT04 == "待处理"){
							return 'high-light-red';
						}
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				},
				{
					name:"AUFNR",
					displayName:'工单号',
					minWidth:100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass: function (row, col) {
						if(col.entity.QMART=='Z6' && col.entity.TXT04 == "待处理"){
							return 'high-light-red';
						}
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				},
				{
					name:"KTEXT",
					displayName:'工单描述',
					minWidth:100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass: function (row, col) {
						if(col.entity.QMART=='Z6' && col.entity.TXT04 == "待处理"){
							return 'high-light-red';
						}
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				},
				{
					name:"AUFART",
					displayName:'工单类型',
					minWidth:100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass: function (row, col) {
						if(col.entity.QMART=='Z6' && col.entity.TXT04 == "待处理"){
							return 'high-light-red';
						}
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				},
				{
					name:"AUARTTEXT",
					displayName:'工单类型描述',
					minWidth:100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass: function (row, col) {
						if(col.entity.QMART=='Z6' && col.entity.TXT04 == "待处理"){
							return 'high-light-red';
						}
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				},
				{
					name:"TXT04",
					displayName:'工单状态',
					minWidth:140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass: function (row, col) {
						if(col.entity.QMART=='Z6' && col.entity.TXT04 == "待处理"){
							return 'high-light-red';
						}
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				},
				{
					name:"TXT30",
					displayName:'工单状态描述',
					minWidth:140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass: function (row, col) {
						if(col.entity.QMART=='Z6' && col.entity.TXT04 == "待处理"){
							return 'high-light-red';
						}
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				},
				{
					name: 'edit',
					displayName: '工单编辑',
					minWidth: 40,
					enableCellEdit: false,
					enableCellEditOnFocus: false,
					cellClass: function (row, col) {
						if(col.entity.QMART=='Z6' && col.entity.TXT04 == "待处理"){
							return 'high-light-red';
						}
					},
					enableColumnMenu: false,
					enableColumnMenus: false,
					//cellTemplate:'andon-ui-grid-tpls/addinfopart-plcShowPen'
					cellTemplate: function(){
						return "<div class=\"ui-grid-cell-contents ad-delete-col\" ng-show='row.entity.QMART == \"Z6\"'><span class=\"glyphicon glyphicon-pencil\"></span></div>";
						//if($scope.config.qmart =='Z6')
						//{
						//	return "<div class=\"ui-grid-cell-contents ad-delete-col\"><span class=\"glyphicon glyphicon-pencil\"></span></div>";
						//}
						//else{
						//	return "<div class=\"ui-grid-cell-contents ad-delete-col\"></div>";
						//}
					},
				}
			],
			onRegisterApi: __onRegisterApi
		};
		function __onRegisterApi(gridApi){
			$scope.config.gridApi = gridApi;

			//gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
			//	if(newRowCol.col.name == "edit" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'&&!UtilsService.isNull(newRowCol.row.entity.AUFNR)){
			//		edit(newRowCol.row.entity.AUFNR);
			//	}
			//});
			$scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
				gridRow.entity.isSelected = gridRow.isSelected;
				if(gridRow.isSelected==true){
					$scope.config.meReceiveOrderData.push(gridRow.entity);
				}
			});
			$scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
				for(var i = 0; i < gridRows.length; i++){
					gridRows[i].entity.isSelected = gridRows[i].isSelected;
				}
			});
		}

		//初始化ui-grid  createAtuoOrderStatus代表创建自主维修单所用
		function initData(resource,createAtuoOrderStatus,status){
			$scope.showBodyModel("正在加载数据,请稍后......");
			var params={
				ZSTATE:'G',
				items:[{"EQUNR":resource}],
			}
			clientService.__plcInitData(params).then(function (resultDatas){
				if(resultDatas.myHttpConfig.status == 200&&resultDatas.response){
					if(resultDatas.response.length==1&&resultDatas.response[0].MSG=='没有符合条件的数据'){
						$scope.addAlert('', '没有待处理的通知单、工单！');
						$scope.workOrderGrid.data=[];
					}else{
						$scope.workOrderGrid.data=resultDatas.response;
					}
					$scope.config.isShwoMeReceiveOrderBtn=false;//不显示接单按钮
					//$scope.config.isShowWorkOrderStatuBtn=false;//不显示CM维修结束按钮
					$scope.config.isShowMaintenanceResultOKBtn=false;//不显示CM维修结果确认OK按钮
					for(var i=0;i<$scope.workOrderGrid.data.length;i++){
						if((!UtilsService.isNull($scope.workOrderGrid.data[i].QMNUM)&&UtilsService.isNull($scope.workOrderGrid.data[i].AUFNR))
							||($scope.workOrderGrid.data[i].TXT04=='待接单'||$scope.workOrderGrid.data[i].TXT04=='远程接单')){
							$scope.config.isShwoMeReceiveOrderBtn=true;//显示接单按钮
						}
						//if(($scope.workOrderGrid.data[i].AUFART=='ZP01')
						//	&&($scope.workOrderGrid.data[i].TXT04=='现场接单'||$scope.workOrderGrid.data[i].TXT04=='待料'||$scope.workOrderGrid.data[i].TXT04=='委外服务')){
						//	$scope.config.isShowWorkOrderStatuBtn=true;//显示CM维修结束按钮
						//	$scope.config.cmMaintenanceWorkOrder=$scope.workOrderGrid.data[i].AUFNR;
						//}

						//if(($scope.workOrderGrid.data[i].AUFART=='ZP01')&&($scope.workOrderGrid.data[i].TXT04=='待确认')){
						//	$scope.config.isShowMaintenanceResultOKBtn=true;//显示CM维修结果确认OK按钮
						//	$scope.config.cmcmMaintenanceResultOK=$scope.workOrderGrid.data[i].AUFNR;
						//}
					}

					//判断是否显示 自主维修确认ok
					var repairConfirmStatus=true;
					for(var i=0;i<resultDatas.response.length;i++)
					{
						if( (resultDatas.response[i].QMART == "Z6" && resultDatas.response[i].TXT04 != "完成"))
						{
							$scope.config.cmcmMaintenanceResultOK=resultDatas.response[i].AUFNR;
							$scope.config.isShowMaintenanceResultOKBtn=true;
							repairConfirmStatus=false;
							continue;
						}
					}

					if(repairConfirmStatus)
					{
						$scope.config.isShowMaintenanceResultOKBtn=false;
					}


					//是否显示 故障报修(报ME处理)按钮
					var repairStatus=true;
					for(var i=0;i<resultDatas.response.length;i++)
					{
						if( (resultDatas.response[i].QMART == "Z6" && resultDatas.response[i].TXT04 != "完成") || (resultDatas.response[i].QMART == "Z1" && resultDatas.response[i].TXT04 != "完成"))
						{
							$scope.config.repairForMeBtn=false;
							repairStatus=false;
							continue;
						}
					}

					if(repairStatus)
					{
						$scope.config.repairForMeBtn=true;
					}

					$scope.hideBodyModel();

					//以下操作全是在 创建自主维修单下进行
					if(createAtuoOrderStatus == 1)
					{
						for(var i=0;i<resultDatas.response.length;i++)
						{
							if( (resultDatas.response[i].QMART == "Z1" || resultDatas.response[i].QMART == "Z6") && resultDatas.response[i].TXT04 != "完成")
							{
								$scope.addAlert('', '存在未关闭的自主维修单、故障单，不允许创建！');
								return;
							}
						}

						//检查通过
						$scope.createAutoOrderPage();

					}else if(createAtuoOrderStatus == 2)
					{
						for(var i=0;i<resultDatas.response.length;i++)
						{
							if( (resultDatas.response[i].QMART == "Z6" && resultDatas.response[i].TXT04 == "待处理") || (resultDatas.response[i].QMART == "Z1" && resultDatas.response[i].TXT04 != "完成"))
							{
								$scope.addAlert('', '存在未完成的自主维修单或故障单，不允许创建！');
								return;
							}
						}

						//检查通过
						$scope.repairForMePage();

					}
				}
			},function (resultDatas){
				$scope.hideBodyModel();
				$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
			});
		}

        $scope.init = function(){
        	//界面打开，则加载设备信息数据
        	initResourceInfo();
        };
        $scope.init();

		//刷新
		$scope.refresh=function(){
			initData($scope.config.resource);
			$scope.config.meReceiveOrderData=[];       //清空
		}

        //打开解锁页面
        $scope.unlockedPage = function(){
        	var modalInstance = $uibModal.open({
				animation : true,
				templateUrl : 'modules/client/modal/operatorLogin.html',
				controller : 'operatorLoginContrller',
				size : 'md',
				backdrop : 'false',
				scope : $scope,
				openedClass : 'flex-center-parent',
				resolve : {
					userModel : function() {
							return {
								modalTitle:'解锁设备操作员',
								resourceOperator:$scope.config.resourceOperator,
							};
					}
				}
			});
			modalInstance.result.then(function(data) {
				var params=$scope.config.resourceCurrentData;
				params.userName=data.userName;
				params.passWord=data.passWord;
				unlocked(params);
			}, function() {
			});
        }
        //锁定
        $scope.locked=function(){
        	var params=$scope.config.resourceCurrentData;
        	params.type="operator";
        	params.operatorLocked="true";
        	clientService.__changeLocked(params).then(function (data){
        		if(data.myHttpConfig.status == 200&&data.response){
        			if(data.response.code==0){
        				$scope.addAlert("success","锁定成功");
						confirmWithBtn();
        				initResourceInfo(1);
        			}else if(data.response.code==1){
        				$scope.addAlert('danger', "锁定失败");
        			}
                }
            },function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        $scope.logout=function(){
			var modalInstance = $uibModal.open({
				animation : true,
				templateUrl : 'modules/client/modal/alertWarnModal.html',
				controller : 'alertWarnController',
				size : 'md',
				backdrop : 'false',
				scope : $scope,
				openedClass : 'flex-center-parent',
				resolve : {
					warnModel : function() {
						return {
							//modalTitle:'解锁设备操作员',
							//resourceOperator:$scope.config.resourceOperator,
						};
					}
				}
			});
			modalInstance.result.then(function(data) {
				var confirmModalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/client/modal/operatorLogin.html',
					controller : 'operatorLoginContrller',
					size : 'md',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						userModel : function() {
								return {
									modalTitle:'注销操作验证',
									resourceOperator:$scope.config.resourceOperator,
								};
						}
					}
				});
				confirmModalInstance.result.then(function(data) {
					var params=$scope.config.resourceCurrentData;
					params.userName=data.userName;
					params.passWord=data.passWord;
					logoutConfirm(params);
				}, function() {
				});
			}, function() {
			});
        }
        $scope.createFaultNoticePage=function(){
        	var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/client/modal/createFaultNotice.html',
                controller: 'createFaultNoticeController',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    passFaultNotice: function () {
                        return {
                        	modalTitle:'故障通知单',
                        	QMART:'Z1',
                        	QMART_DESC:'故障通知单',
                        	resourceCurrentData:$scope.config.resourceCurrentData,
                        	operatorName:$scope.config.operatorName
                        };
                    }
                }
            });
            modalInstance.result.then(function (data) {
            	createFaultNotice(data);
            }, function () {
            });
        }
        //创建隐患通知单
        $scope.createHiddenTroubleNoticePage=function(){
        	var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/client/modal/createFaultNotice.html',
                controller: 'createFaultNoticeController',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    passFaultNotice: function () {
                        return {
                        	modalTitle:'隐患通知单',
                        	QMART:'Z5',
                        	QMART_DESC:'隐患通知单',
                        	resourceCurrentData:$scope.config.resourceCurrentData,
                        	operatorName:$scope.config.operatorName
                        };
                    }
                }
            });
            modalInstance.result.then(function (data) {
            	createHiddenTroubleNotice(data);
	            }, function () {
	            });
        }
		//创建自主维修单
		$scope.createAutoOrderPage=function(){
			var nowDate=new Date();
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'modules/client/modal/createFaultNotice.html',
				controller: 'createFaultNoticeController',
				size: 'lg',
				backdrop:'false',
				scope : $scope,
				openedClass:'flex-center-parent',
				resolve: {
					passFaultNotice: function () {
						return {
							modalTitle:'自主维修单',
							QMART:'Z6',
							QMART_DESC:'生产部自主维修单',
							resourceCurrentData:$scope.config.resourceCurrentData,
							operatorName:$scope.config.operatorName,
							description:"上位机创建自主维修单："+$scope.config.resourceCurrentData.resource+" "+UtilsService.serverFommateDateTimeShow(nowDate)
						};
					}
				}
			});
			modalInstance.result.then(function (data) {
				createFaultNotice(data);
			}, function () {
			});
		}
		//故障保修(报ME处理)Modal
		$scope.repairForMePage=function(){
			var nowDate=new Date();
			UtilsService.log("格式化时间："+UtilsService.serverFommateDateTimeShow(nowDate));
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'modules/client/modal/createFaultNotice.html',
				controller: 'createFaultNoticeController',
				size: 'lg',
				backdrop:'false',
				scope : $scope,
				openedClass:'flex-center-parent',
				resolve: {
					passFaultNotice: function () {
						return {
							modalTitle:'故障通知单',
							QMART:'Z1',
							QMART_DESC:'故障通知单',
							resourceCurrentData:$scope.config.resourceCurrentData,
							operatorName:$scope.config.operatorName,
							description:"上位机故障报修："+$scope.config.resourceCurrentData.resource+" "+UtilsService.serverFommateDateTimeShow(nowDate)
						};
					}
				}
			});
			modalInstance.result.then(function (data) {
				createFaultNotice(data);
			}, function () {
			});
		}
        $scope.changeOperatorPage = function(){
        	var modalInstance = $uibModal.open({
				animation : true,
				templateUrl : 'modules/client/modal/operatorLogin.html',
				controller : 'operatorLoginContrller',
				size : 'md',
				backdrop : 'false',
				scope : $scope,
				openedClass : 'flex-center-parent',
				resolve : {
					userModel : function() {
						if(UtilsService.isNull($scope.config.resourceOperator)){
							return {
								modalTitle:'维护设备操作员',
							};
						}else{
							return {
								modalTitle:'更换设备操作员',
							};
						}
					}
				}
			});
			modalInstance.result.then(function(data) {
				var params=$scope.config.resourceCurrentData;
				params.userName=data.userName;
				params.passWord=data.passWord;
				changeOperator(params);
			}, function() {
			});
        }
        //物料编码
        $scope.changeItem = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/client/modal/itemList.html',
                controller: 'itemListController',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    passItemList: function () {
                        return {
                        	model : $scope.config.model,
                        	item:$scope.config.item,
                        	resourceCurrentData:$scope.config.resourceCurrentData,
							linkIndexStatus:true
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.item = selectedItem.item;
                var params=$scope.config.resourceCurrentData;
                params.item=$scope.config.item;
            	clientService.__changeItem(params).then(function (resultDatas){
            		if(resultDatas.myHttpConfig.status == 200){
            			$scope.addAlert("success","更换成功");
            			initResourceInfo(1);
                    }
                },function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                }); 
	            }, function () {
	            });
        }
        function initResourceInfo(code){
        	clientService.__initResourceInfo().then(function (resultDatas){
                 if(resultDatas.response){
                	 $scope.config.resource=resultDatas.response.resource;
                	 $scope.config.resourceName=resultDatas.response.resourceName;
                	 $scope.config.item=resultDatas.response.item;
                	 $scope.config.itemName=resultDatas.response.itemName;
                	 $scope.config.resourceOperator=resultDatas.response.resourceOperator;
                	 $scope.config.operatorLocked=resultDatas.response.operatorLocked;
					 confirmWithBtn();
                	 $scope.config.resourceCurrentData=resultDatas.response;
                	 getUserInfo($scope.config.resourceOperator);
					 if(!code)
					 {
						 initData($scope.config.resource);
					 }
                     return;
                 }
             },function (resultDatas){
                 $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
             });
        }
		function edit(QMNUM,status){
			//var mode="1";
			//if(!$scope.config.resourceOperator || $scope.config.operatorLocked == "true"){
			//	mode="0";
			//}
			var mode="1";
			if(status == '完成'){
				mode="2";
			}
			var params={
				IUSER:$scope.config.operatorName,
				MODE:mode,
				AUFNR:QMNUM,
			}
			clientService.__plcEditWorkOrder(params).then(function (resultDatas){
				if(resultDatas.myHttpConfig.status == 200){
					window.open(resultDatas.response.url);
					return;
				}
			},function (resultDatas){
				$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
			});
		}
        function changeOperator(params){
        	clientService.__plcChangeOperator(params,2).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200){
        			if(resultDatas.response.code==0){
        				$scope.addAlert("success","更新成功");
						confirmWithBtn();
						//initResourceInfo(1);
						writeUserLogInfo(params,1);
        			}else{
						$scope.addAlert('danger',resultDatas.response.msg);
        			}
                }
            },function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }

		//记录用户登入登出日志  第二个参数代表 1 更换或者维护  0 注销
		function writeUserLogInfo(params,code){
			if($scope.config.resourceOperator)
			{
				var paramLoginOut={
					"site":ROOTCONFIG.AndonConfig.SITE,
					"user":$scope.config.resourceOperator,
					"resrce":$scope.config.resource,
					"actionCode":"LOGOUT",
					"loginMode":"ONSITE",
					"userDepartment":"PRD"
				};

				clientService._plcUserLogInfo(paramLoginOut,0).then(function (resultDatas){
					if(resultDatas.myHttpConfig.status == 200){
						if(code){
							//更换
							UtilsService.log("第二次执行");
							paramLoginOut.actionCode="LOGIN";
							paramLoginOut.user=params.userName;
							clientService._plcUserLogInfo(paramLoginOut,1).then(function (resultDatas){
								if(resultDatas.myHttpConfig.status == 200){
									initResourceInfo(1);
								}
							},function (resultDatas){
								$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
							});
						}else {
							//注销
							initResourceInfo();
						}
					}
				},function (resultDatas){
					$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
				});
			}else{
				//维护
				var paramLoginOut={
					"site":ROOTCONFIG.AndonConfig.SITE,
					"user":params.userName,
					"resrce":$scope.config.resource,
					"actionCode":"LOGIN",
					"loginMode":"ONSITE",
					"userDepartment":"PRD"
				};
				clientService._plcUserLogInfo(paramLoginOut,1).then(function (resultDatas){
					if(resultDatas.myHttpConfig.status == 200){
						initResourceInfo(1);
					}
				},function (resultDatas){
					$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
				});
			}

		}

		//当确认注销的时候先进行身份验证
		function logoutConfirm(params){
			clientService.__plcChangeOperator(params,2).then(function (resultDatas){
				if(resultDatas.myHttpConfig.status == 200){
					if(resultDatas.response.code==0){
						var params=$scope.config.resourceCurrentData;
						clientService.__logout(params).then(function (resultDatas){
						if(resultDatas.myHttpConfig.status == 200){
							if(resultDatas.response.code==0){
								$scope.addAlert("success","注销成功");
								confirmWithBtn();
								//initResourceInfo();
								writeUserLogInfo(params,0);
							}else if(resultDatas.response.code==1){
								$scope.addAlert("danger",resultDatas.response.msg);
							}else if(resultDatas.response.code==3){
								$scope.addAlert("danger",resultDatas.response.msg);
							}else{
								$scope.addAlert("danger","未知错误");
							}
						 }
						},function (resultDatas){
						    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
						});
					}else if(resultDatas.response.code==1){
						$scope.addAlert('danger',"用户名或密码错误");
					}
					//initResourceInfo();
				}
			},function (resultDatas){
				$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
			});
		}
        function createFaultNotice(params){
        	if(params.QMTXT.length>40){
        		$scope.addAlert('danger', '通知单描述长度超过40');
    			return;
        	}
			$scope.showBodyModel("正在创建中,请稍后......");
        	clientService.__plcCreateFaultNotice(params).then(function (resultDatas){
				$scope.hideBodyModel();
                if(resultDatas.response){
                 if(resultDatas.response.esubrc==0){
                	 $scope.addAlert("success",resultDatas.response.emsg);
					 initData($scope.config.resource);
                	 //callEap();
                	 return;
                 }else if(resultDatas.response.esubrc==1){
                	 $scope.addAlert("danger",resultDatas.response.emsg);
                	 //callEap();
                	 return;
                 }
                }
            },function (resultDatas){
				$scope.hideBodyModel();
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        function callEap(){
        	var params={
        		resource:$scope.config.resource,
        		limitCode:'0'
        	}
        	clientService.__callEap(params).then(function (resultDatas){
                if(resultDatas.response){
	                 console.info(resultDatas.response);
	                 /*$scope.addAlert("success",resultDatas.response.emsg);
	                    return;*/
                }
            },function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        function createHiddenTroubleNotice(params){
        	clientService.__plcCreateFaultNotice(params).then(function (resultDatas){
                if(resultDatas.response){
                	if(resultDatas.response.esubrc==0){
	                   	 $scope.addAlert("success",resultDatas.response.emsg);
						initData($scope.config.resource);
	                   	 return;
                    }else if(resultDatas.response.esubrc==1){
	                   	 $scope.addAlert("danger",resultDatas.response.emsg);
	                   	 //callEap();
	                   	 return;
                    }
                }
            },function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        //解锁
        function unlocked(params){
        	params.type="operator";
        	params.operatorLocked="false";
        	clientService.__changeLocked(params).then(function (data){
        		if(data.myHttpConfig.status == 200&&data.response){
        			if(data.response.code==0){
        				$scope.addAlert("success","解锁成功");
						confirmWithBtn();
        				initResourceInfo(1);
        			}else if(data.response.code==1){
        				$scope.addAlert('danger', "用户名或密码错误");
        			}
                }
            },function (data){
                $scope.addAlert('danger', data.myHttpConfig.statusDesc);
            });
        }
        function getUserInfo(userId){
        	clientService.__getUserInfo(userId).then(function (resultDatas){
                 if(resultDatas.response){
                	 var lastName="";
                	 var firstName="";
                	 if(!UtilsService.isNull(resultDatas.response.serverUserDetailsVO.lastName)){
                		 lastName=resultDatas.response.serverUserDetailsVO.lastName
                	 }
                	 if(!UtilsService.isNull(resultDatas.response.serverUserDetailsVO.firstName)){
                		 firstName=resultDatas.response.serverUserDetailsVO.firstName;
                	 }
                	 $scope.config.operatorName=lastName+firstName;
                     return;
                 }
             },function (resultDatas){
                 $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
             });
        }

		//创建自主维修单 meReceiveOrder===1   故障保修(ME处理) ===2
		$scope.createAutoOrder=function(code,status){
			initData($scope.config.resource,code,status);
			$scope.config.meReceiveOrderData=[];       //清空
		};

		//变更通知单状态
		//变更工单状态
		$scope.changeWorkOrderStatu=function(){
			var params={
				IAUFNR:$scope.config.cmMaintenanceWorkOrder,
				IANDONUSER:$scope.config.meStaffName,
				ISTATUS:"D",
			}
			clientService.__plcChangeWorkOrderStatu(params).then(function (resultDatas){
				if(resultDatas.response){
					if(resultDatas.response.esubrc=='S'){
						$scope.addAlert("success",resultDatas.response.emsg);
						initResourceInfo();
					}else if(resultDatas.response.esubrc=='E'){
						$scope.addAlert("danger",resultDatas.response.emsg);
					}
				}
			},function (resultDatas){
				$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
			});
		}

		//维修结果确认弹出框
		$scope.maintenanceResultConfirmation = function(type){
			var params={
				IANDONUSR:$scope.config.resourceOperator,
				items:[{"AUFNR":$scope.config.cmcmMaintenanceResultOK}],
			}
			if(type=='OK'){
				params.IRESULT=0;
				maintenanceRC(params);
			}else if(type=='NG'){
				params.IRESULT=1;
				maintenanceRC(params);
			}
		};

		//维修结果确认接口
		function maintenanceRC(params){
			$scope.showBodyModel("正在加载中,请稍后......");
			clientService.__maintenanceResultConfirmation(params).then(function (resultDatas){
				$scope.hideBodyModel();
				if(resultDatas.myHttpConfig.status == 200&&resultDatas.response.length>0){
					if(resultDatas.response[0].SUBRC==0){
						$scope.addAlert("success",resultDatas.response[0].MSG);
					}else if(resultDatas.response[0].SUBRC==1){
						$scope.addAlert("danger",resultDatas.response[0].MSG);
					}
					initResourceInfo();
				}
			},function (resultDatas){
				$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
			});
		}

		//当“PRD操作员”不为空且状态不为锁定  设置false 否则设为true
		function confirmWithBtn(){
            if($scope.config.resourceOperator && $scope.config.operatorLocked !='true'){
				$scope.config.disabledMeReceiverOrderBtn=false;
			}
			else{
				$scope.config.disabledMeReceiverOrderBtn=true;
			}
		}

		$scope.$on(uiGridCellNavConstants.CELL_NAV_EVENT, function (evt, rowCol, modifierDown) {
			UtilsService.log("进入监听事件多次");
			if(rowCol)
			{
				if(rowCol.col.name == "edit" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'&&!UtilsService.isNull(rowCol.row.entity.AUFNR)){
					if(rowCol.row.entity.QMART=='Z6')
					{
						edit(rowCol.row.entity.AUFNR,rowCol.row.entity.TXT04);
					}
				}
			}
		});
    }]);