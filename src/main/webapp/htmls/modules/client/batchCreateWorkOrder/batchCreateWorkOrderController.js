theoryPPMModule.controller('batchCreateWorkOrderController',['$scope', '$http',
    'HttpAppService', '$timeout', 'UtilsService', '$filter',
    'uiGridConstants', 'clientService','Upload', 'uiGridValidateService',
    function ($scope, $http, HttpAppService, $timeout, UtilsService, $filter,
              uiGridConstants,clientService, Upload, uiGridValidateService) {
    UtilsService.log("success-------------enter");
    $scope.gridWorkOrderData = {
        paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
        paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,
        data: [],
        onRegisterApi: __onRegisterApi,
        columnDefs:[ 
            {   
                name:"aufnr",
                displayName:"外部工单编号",
                minWidth:100,
                enableCellEdit : false,
                enableCellEditOnFocus : false,
            },  
            {   
                name:"pmaufart",
                displayName:"工单类型",
                minWidth:100,
                enableCellEdit : false,
                enableCellEditOnFocus : false
            },  
            {   
                name:"ktext",
                displayName:"工单描述",
                minWidth:100,
                enableCellEdit : false,
                enableCellEditOnFocus : false,
            },  
            {   
                name:"equnr",
                displayName:"改造设备",
                minWidth:100,
                enableCellEdit : false,
                enableCellEditOnFocus : false,
            },
            {   
                name:"gstrp",
                displayName:"计划开始日期",
                minWidth:100,
                enableCellEdit : false,
                enableCellEditOnFocus : false,
            },
            {   
                name:"arbeit",
                displayName:"工作时间",
                minWidth:100,
                enableCellEdit : false,
                enableCellEditOnFocus : false,
            },
            {   
                name:"arbeite",
                displayName:"时间单位",
                minWidth:100,
                enableCellEdit : false,
                enableCellEditOnFocus : false,
            },
            {   
                name:"anzzl",
                displayName:"人数",
                minWidth:100,
                enableCellEdit : false,
                enableCellEditOnFocus : false,
            },
            {   
                name:"matnr",
                displayName:"配件标识码",
                minWidth:100,
                enableCellEdit : false,
                enableCellEditOnFocus : false,
            },
            {
                name:"menge",
                displayName:"数量",
                minWidth:100,
                enableCellEdit : false,
                enableCellEditOnFocus : false,
            }
            ]  
    };  
    
    function __cellToolTip(row, col){ 
        var str = '';// row.entity[col.colDef.name]; 
        var errors = row.entity[col.colDef.name+"Exceptions"];
        if(!angular.isUndefined(errors) && errors != null && errors.length > 0){
            for(var i = 0; i < errors.length; i++){
                if(i > 0){
                    str += "<br/>";
                }
                str += errors[i];
            }
        }
        return str; 
    }

    $scope.config = { 
        gridApi: null,
        isUploading: false,
        uploadError: false,
        uploadOk: false,

        selectedFile: null,

        parseFileOk: false,
        parseFileOkNum: 0,

        uploadProgressStr: '',
        progressPercentage: 0,

        showErrorDucpGridDatas: false,
        loaclData:[],
    };  

    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
    };

    $scope.$watch('config.selectedFile', function(newVal, oldVal){
        if(newVal==null){
            $scope.config.uploadProgressStr = '';
            $scope.config.progressPercentage = 0;
            //$scope.bodyConfig.overallWatchChanged = false;
        }else{
            //判断
            $scope.bodyConfig.overallWatchChanged = true;
        }
    });
    $scope.checkExcel=function(){
    	if($scope.config.uploadOk){
            $scope.repareForUpload();
            return;
        }
        var file = $scope.config.selectedFile;
        var url = HttpAppService.URLS.CHECK_EXCEL_DATA_WORKORDER.replace('#{site}#', HttpAppService.getSite());
        url = HttpAppService.handleCommenUrl(url);
        Upload.upload({
            url: url,
            data: {file: file}
        }).then(function (resp) {
        	if(resp.data.checkResult==true){
        		if($scope.gridWorkOrderData.columnDefs[10]!=undefined){
        			$scope.gridWorkOrderData.columnDefs.splice(10,11);
        		}
        		var gridData=[];
                var data=resp.data.poList;
                $scope.config.loaclData=resp.data.poList;
                for(var i=0;i<data.length;i++){
             	   var itcomponents=data[i].itcomponents;
             	   var checkDescs=data[i].checkDescription;
             	   for(j=0;j<itcomponents.length;j++){
             			   var obj={};
                   		   obj.aufnr=data[i].ihead.AUFNR;
                   		   obj.pmaufart=data[i].ihead.PM_AUFART;
                   		   obj.ktext=data[i].ihead.KTEXT;
                   		   obj.equnr=data[i].ihead.EQUNR;
                   		   obj.gstrp=data[i].ihead.GSTRP;
                   		   obj.arbeit=data[i].itoperations[0].ARBEIT;
                   		   obj.arbeite=data[i].itoperations[0].ARBEITE;
                   		   obj.anzzl=data[i].itoperations[0].ANZZL;
                   		   obj.matnr=itcomponents[j].MATNR;
                   		   obj.menge=itcomponents[j].MENGE;
                   		   gridData.push(obj);
             	   }
                }
                $scope.gridWorkOrderData.data=gridData;
                $scope.addAlert("success","success");
        	}else if(resp.data.checkResult==false){
        		 var col={
        	                name:"checkDescription",
        	                displayName:"报错描述",
        	                minWidth:100,
        	                enableCellEdit : false,
        	                enableCellEditOnFocus : false,
        	            };
        		 if($scope.gridWorkOrderData.columnDefs[10]==undefined){
        			 $scope.gridWorkOrderData.columnDefs.push(col);
        		 }
        		 var gridData=[];
                 var data=resp.data.poList;
                 $scope.config.loaclData=resp.data.poList;
                 for(var i=0;i<data.length;i++){
              	   var itcomponents=data[i].itcomponents;
              	   var checkDescs=data[i].checkDescription;
              	   for(j=0;j<itcomponents.length;j++){
              		   for(var x=0;x<checkDescs.length;x++){
              			   if(j==x){
              				   var obj={};
                    		   obj.aufnr=data[i].ihead.AUFNR;
                    		   obj.pmaufart=data[i].ihead.PM_AUFART;
                    		   obj.ktext=data[i].ihead.KTEXT;
                    		   obj.equnr=data[i].ihead.EQUNR;
                    		   obj.gstrp=data[i].ihead.GSTRP;
                    		   obj.arbeit=data[i].itoperations[0].ARBEIT;
                    		   obj.arbeite=data[i].itoperations[0].ARBEITE;
                    		   obj.anzzl=data[i].itoperations[0].ANZZL;
                    		   obj.matnr=itcomponents[j].MATNR;
                    		   obj.menge=itcomponents[j].MENGE;
                    		   obj.checkDescription=checkDescs[x];
                    		   gridData.push(obj);
              			   }
              		   }
              	   }
                 }
                 $scope.gridWorkOrderData.data=gridData;
                 $scope.addAlert("danger","Excel数据校验出错，请看详细报错信息");
        	}
        }, function (resp) {
        	$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
        }, function (evt) {
        }); 
    }
    
    $scope.upload = function () {
        //先检查再上传
        if($scope.config.uploadOk){
            $scope.repareForUpload();
            return;
        }
        var file = $scope.config.selectedFile;
        $scope.bodyConfig.overallWatchChanged = true;
        var url = HttpAppService.URLS.CHECK_EXCEL_DATA_WORKORDER.replace('#{site}#', HttpAppService.getSite());
        url = HttpAppService.handleCommenUrl(url);
        Upload.upload({
            url: url,
            data: {file: file}
        }).then(function (resp) {
            $scope.bodyConfig.overallWatchChanged = false;
            if(resp.data.checkResult==true){
                if($scope.gridWorkOrderData.columnDefs[10]!=undefined){
                    $scope.gridWorkOrderData.columnDefs.splice(10,11);
                }
                var gridData=[];
                var data=resp.data.poList;
                $scope.config.loaclData=resp.data.poList;
                for(var i=0;i<data.length;i++){
                    var itcomponents=data[i].itcomponents;
                    var checkDescs=data[i].checkDescription;
                    for(j=0;j<itcomponents.length;j++){
                        var obj={};
                        obj.aufnr=data[i].ihead.AUFNR;
                        obj.pmaufart=data[i].ihead.PM_AUFART;
                        obj.ktext=data[i].ihead.KTEXT;
                        obj.equnr=data[i].ihead.EQUNR;
                        obj.gstrp=data[i].ihead.GSTRP;
                        obj.arbeit=data[i].itoperations[0].ARBEIT;
                        obj.arbeite=data[i].itoperations[0].ARBEITE;
                        obj.anzzl=data[i].itoperations[0].ANZZL;
                        obj.matnr=itcomponents[j].MATNR;
                        obj.menge=itcomponents[j].MENGE;
                        gridData.push(obj);
                    }
                }
                $scope.gridWorkOrderData.data=gridData;
               // $scope.addAlert("success","success");
               // 检查成功再上传
                var params=$scope.config.loaclData;
                clientService.__upload(params).then(function (resultDatas){
                    if(resultDatas.myHttpConfig.status == 200){
                        $scope.addAlert("success","创建成功");
                        return;
                    }
                },function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
            }else if(resp.data.checkResult==false){
                var col={
                    name:"checkDescription",
                    displayName:"报错描述",
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                };
                if($scope.gridWorkOrderData.columnDefs[10]==undefined){
                    $scope.gridWorkOrderData.columnDefs.push(col);
                }
                var gridData=[];
                var data=resp.data.poList;
                $scope.config.loaclData=resp.data.poList;
                for(var i=0;i<data.length;i++){
                    var itcomponents=data[i].itcomponents;
                    var checkDescs=data[i].checkDescription;
                    for(j=0;j<itcomponents.length;j++){
                        for(var x=0;x<checkDescs.length;x++){
                            if(j==x){
                                var obj={};
                                obj.aufnr=data[i].ihead.AUFNR;
                                obj.pmaufart=data[i].ihead.PM_AUFART;
                                obj.ktext=data[i].ihead.KTEXT;
                                obj.equnr=data[i].ihead.EQUNR;
                                obj.gstrp=data[i].ihead.GSTRP;
                                obj.arbeit=data[i].itoperations[0].ARBEIT;
                                obj.arbeite=data[i].itoperations[0].ARBEITE;
                                obj.anzzl=data[i].itoperations[0].ANZZL;
                                obj.matnr=itcomponents[j].MATNR;
                                obj.menge=itcomponents[j].MENGE;
                                obj.checkDescription=checkDescs[x];
                                gridData.push(obj);
                            }
                        }
                    }
                }
                $scope.gridWorkOrderData.data=gridData;
                $scope.addAlert("danger","Excel数据校验出错，请看详细报错信息");
            }
        }, function (resp) {
            $scope.bodyConfig.overallWatchChanged = false;
            $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
        }, function (evt) {
        });

    }; 

    $scope.drap = function(){
        console.log(arguments);
        $scope.config.isUploading = false;
        $scope.bodyConfig.overallWatchChanged = true;
        $scope.config.uploadError = false;
        $scope.config.uploadOk = false;
    };
    $scope.select = function($file){
        $scope.config.selectedFile = $file;
        $scope.config.isUploading = false;
        $scope.bodyConfig.overallWatchChanged = true;
        $scope.config.uploadError = false;
        $scope.config.uploadOk = false;
    }
    $scope.repareForUpload = function(){
        $scope.config.selectedFile = null;
        $scope.config.isUploading = false;
        $scope.bodyConfig.overallWatchChanged = false;
        $scope.config.uploadError = false;
        $scope.config.uploadOk = false;
        $scope.config.parseFileOk = false;
        $scope.config.parseFileOkNum = 0;
        $scope.config.uploadProgressStr = '';
        $scope.config.progressPercentage = 0;
    };

    function __handleErrors(errors){
        var resultDatas = [];
        for(var i = 0; i < errors.length; i++){
            var err = errors[i];
            // err.originDataMap.StartDate = $filter('date')(new Date(err.originDataMap.startDate), 'yyyy-MM-dd');
            var data = err.originDataMap; 
            var list = err.exceptionList; 
            var rowNum = err.rowNumber;
            for(var j = 0; j < list.length; j++){ 
                var exe = list[j]; 
                for(key in exe){ 
                    data[key+"Exceptions"] = exe[key]; 
                    // 转换 exception 为中文描述 
                    for(var xx = 0; xx < data[key+"Exceptions"].length; xx++){
                        data[key+"Exceptions"][xx] = "文件第"+rowNum+"行: "+__corvetToChinaese(data[key+"Exceptions"][xx]);
                    }
                } 
            }   
            resultDatas.push(data);
        }
        console.log(resultDatas);
        if(resultDatas.length > 0){
            $scope.gridPPMGroupMaintain.data = resultDatas;
            $timeout(function(){
                __setInValidCells();
            }, 500);
        }
    }
    var colDefNamesNeedValid = ['resrce','model','ppmTheory','startDate'];
    function __setInValidCells(){
        var datas = $scope.gridPPMGroupMaintain.data;
        for(var i = 0; i < datas.length; i++){
            var rowEntity = datas[i];
            for(var colName in colDefNamesNeedValid){
                var colNameError = colDefNamesNeedValid[colName]+"Exceptions";
                if(!angular.isUndefined(rowEntity[colNameError]) && rowEntity[colNameError] != null && rowEntity[colNameError].length > 0){
                    uiGridValidateService.setInvalid(rowEntity,$scope.config.gridApi.grid.getColDef(colDefNamesNeedValid[colName]));
                }
            }
        }
    }

    function __corvetToChinaese(codeStr){
        if(UtilsService.isEmptyString(codeStr)){
            return codeStr;
        }
        var code = codeStr.substring(codeStr.indexOf("[")+1, codeStr.indexOf("]"));
        var codeDesc = HttpAppService.getDescByExceptionCode(code);
        return codeDesc;
    }


}]);