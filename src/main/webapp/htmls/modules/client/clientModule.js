clientModule.service('clientService', [
    'HttpAppService','UtilsService',
    function (HttpAppService,UtilsService) {
        return{
        	__initData:initData,
			__plcInitData:plcInitData,
        	__initResourceInfo:initResourceInfo,
			_plcInitResourceInfo:plcInitResourceInfo,
        	__getUserInfo:getUserInfo,
        	__changeItem:changeItem,
        	__meReceiveOrder:meReceiveOrder,
			__plcMeReceiveOrder:plcMeReceiveOrder,
        	__changeWorkOrderStatu:changeWorkOrderStatu,
			__plcChangeWorkOrderStatu:plcChangeWorkOrderStatu,
        	__maintenanceResultConfirmation:maintenanceResultConfirmation,
        	__editWorkOrder:editWorkOrder,
			__plcEditWorkOrder:plcEditWorkOrder,
        	__createFaultNotice:createFaultNotice,
			__plcCreateFaultNotice:plcCreateFaultNotice,
        	__getLogonUser:getLogonUser,
        	__callEap:callEap,
        	__upload:upload,
        	__logout:logout,
        	__meUserLogout:meUserLogout,
			__plcMeUserLogout:plcMeUserLogout,
        	__changeOperator:changeOperator,
			__plcChangeOperator:plcChangeOperator,
			_plcUserLogInfo:plcUserLogInfo,
        	__changeLocked:changeLocked,
        	__callResourceReasonLog:callResourceReasonLog,
        }
        function callEap(params){
       	 var url = HttpAppService.URLS.CALL_EAP_INTERFACE
       	.replace("#{site}#", HttpAppService.getSite())
        .replace("#{Resource}#",params.resource)
	    .replace("#{plcIp}#",window.localStorage.plcIp)
        .replace("#{limitCode}#",params.limitCode);
		     return HttpAppService.get({
		         url: url
		     });
       }
        function initResourceInfo(){
        	 var url = HttpAppService.URLS.CLIENT_GET_RESOURCE_INFO
				 .replace("#{ip}#",window.localStorage.plcIp)
				 .replace("#{plcIp}#",window.localStorage.plcIp);
		     return HttpAppService.get({
		         url: url
		     });
        }
		function plcInitResourceInfo(){
			var url = HttpAppService.URLS.PLC_CLIENT_GET_RESOURCE_INFO
				.replace("#{ip}#",window.localStorage.plcIp)
				.replace("#{plcIp}#",window.localStorage.plcIp);
			return HttpAppService.get({
				url: url
			});
		}
        function initData(params){
        	 var url = HttpAppService.URLS.CLIENT_WORK_ORDER_DATA.replace("#{site}#", HttpAppService.getSite());
        	 return HttpAppService.post({
 				url: url,
 				paras : params
 			});
        }
		function plcInitData(params){
			params.plcIp=window.localStorage.plcIp;
			var url = HttpAppService.URLS.PLC_CLIENT_WORK_ORDER_DATA.replace("#{site}#", HttpAppService.getSite());
			return plcPost({
				url: url,
				params : params
			});
		}
        function meReceiveOrder(params){
       	 var url = HttpAppService.URLS.CLIENT_ME_RECEIVE_ORDER.replace("#{site}#", HttpAppService.getSite());
       	 return HttpAppService.post({
				url: url,
				paras : params
			});
        }
		function plcMeReceiveOrder(params){
			params.plcIp=window.localStorage.plcIp;
			var url = HttpAppService.URLS.CLIENT_ME_RECEIVE_ORDER.replace("#{site}#", HttpAppService.getSite());
			return plcPost({
				url: url,
				params : params
			});
		}
        function maintenanceResultConfirmation(params){
			params.plcIp=window.localStorage.plcIp;
        	var url = HttpAppService.URLS.CLIENT_MAINTENANCE_RESULT_CONFIRMATION.replace("#{site}#", HttpAppService.getSite());
			return plcPost({
				url: url,
				params : params
			});
        }
        function callResourceReasonLog(params){
			params.plcIp=window.localStorage.plcIp;
        	var url = HttpAppService.URLS.CLIENT_CALL_RESOURCE_REASON_LOG;
			return plcPost({
				url: url,
				params : params
			});
        }
        function editWorkOrder(params){
        	var url = HttpAppService.URLS.CLIENT_EDIT_WORK_ORDER
            .replace("#{site}#", HttpAppService.getSite())
            .replace("#{IUSER}#",params.IUSER)
            .replace("#{MODE}#",params.MODE)
            .replace("#{AUFNR}#",params.AUFNR);
		     return HttpAppService.get({
		         url: url
		     });
        }
		function plcEditWorkOrder(params){
			var url = HttpAppService.URLS.PLC_CLIENT_EDIT_WORK_ORDER
				.replace("#{site}#", HttpAppService.getSite())
				.replace("#{IUSER}#",params.IUSER)
				.replace("#{MODE}#",params.MODE)
				.replace("#{plcIp}#",window.localStorage.plcIp)
				.replace("#{AUFNR}#",params.AUFNR);
			return HttpAppService.get({
				url: url
			});
		}
        function changeWorkOrderStatu(params){
        	var url = HttpAppService.URLS.CLIENT_CHANGE_WORK_ORDER_STATU
            .replace("#{site}#", HttpAppService.getSite())
            .replace("#{IAUFNR}#",params.IAUFNR)
            .replace("#{IANDONUSER}#",params.IANDONUSER)
            .replace("#{ISTATUS}#",params.ISTATUS);
		     return HttpAppService.get({
		         url: url
		     });
        }
		function plcChangeWorkOrderStatu(params){
			var url = HttpAppService.URLS.PLC_CLIENT_CHANGE_WORK_ORDER_STATU
				.replace("#{site}#", HttpAppService.getSite())
				.replace("#{IAUFNR}#",params.IAUFNR)
				.replace("#{IANDONUSER}#",params.IANDONUSER)
				.replace("#{plcIp}#",window.localStorage.plcIp)
				.replace("#{ISTATUS}#",params.ISTATUS);
			return HttpAppService.get({
				url: url
			});
		}
        function createFaultNotice(params){
        	var url = HttpAppService.URLS.CLIENT_CREATE_FAULT_NOTICE
            .replace("#{site}#", HttpAppService.getSite())
            .replace("#{QMART}#",params.QMART)
            .replace("#{QMTXT}#",params.QMTXT)
            .replace("#{EQUNR}#",params.EQUNR)
            .replace("#{QMNAM}#",params.QMNAM)
            .replace("#{QMDAT}#","")
            .replace("#{MZEIT}#","")
            .replace("#{ZSTATE}#","");
		     return HttpAppService.get({
		         url: url
		     });
        }
		function plcCreateFaultNotice(params){
			var url = HttpAppService.URLS.PLC_CLIENT_CREATE_FAULT_NOTICE
				.replace("#{site}#", HttpAppService.getSite())
				.replace("#{QMART}#",params.QMART)
				.replace("#{QMTXT}#",params.QMTXT)
				.replace("#{EQUNR}#",params.EQUNR)
				.replace("#{QMNAM}#",params.QMNAM)
				.replace("#{plcIp}#",window.localStorage.plcIp)
				.replace("#{QMDAT}#","")
				.replace("#{MZEIT}#","")
				.replace("#{ZSTATE}#","");
			return HttpAppService.get({
				url: url
			});
		}
        function getUserInfo(userId){
        	var url = HttpAppService.URLS.USER_QUERY_DATAS
            .replace("#{site}#", HttpAppService.getSite())
            .replace("#{userId}#",userId)
			.replace("#{plcIp}#",window.localStorage.plcIp);
		     return HttpAppService.get({
		         url: url
		     });
        }
        function getLogonUser(){
        	var url = HttpAppService.URLS.USER_INFOS;
		     return HttpAppService.get({
		         url: url
		     });
        }
        function logout(params){
			params.plcIp=window.localStorage.plcIp;
        	var url = HttpAppService.URLS.CLIENT_OPERATOR_LOGOUT;
			return plcPost({
				url: url,
				params : params
			});
        }
        function meUserLogout(params){
        	var url = HttpAppService.URLS.CLIENT_ME_USER_LOGOUT;
          	 return HttpAppService.post({
   				url: url,
   				paras : params
   			});
        }
		function plcMeUserLogout(params){
			params.plcIp=window.localStorage.plcIp;
			UtilsService.log("------------------:"+angular.toJson(params,true));
			var url = HttpAppService.URLS.CLIENT_ME_USER_LOGOUT;
			return plcPost({
				url: url,
				params : params
			});
		}
        function changeOperator(params){
        	var url = HttpAppService.URLS.CLIENT_CHANGE_OPERATOR;
          	 return HttpAppService.post({
   				url: url,
   				paras : params
   			});
        }
		function plcChangeOperator(params,code){
			//在此判断是me/prd  1/2
			params.module=code;
			params.site=HttpAppService.getSite();
			params.plcIp=window.localStorage.plcIp;
			var url = HttpAppService.URLS.CLIENT_CHANGE_OPERATOR;
			return plcPost({
				url: url,
				params : params
			});
		}

		//记录日志
		function plcUserLogInfo(params,loginStatus){
			params.plcIp=window.localStorage.plcIp;
			var url = HttpAppService.URLS.PLC_USER_LOG_INFO;
			return plcPost({
				url: url,
				params : params
			});
		}

        function changeLocked(params){
			params.plcIp=window.localStorage.plcIp;
			UtilsService.log("更换："+angular.toJson(params,true));
        	var url = HttpAppService.URLS.CLIENT_CHANGE_LOCKED;
          	 return plcPost({
				 url: url,
				 params : params
			 });
        }
        function changeItem(params){
		 params.plcIp=window.localStorage.plcIp;
       	 var url = HttpAppService.URLS.CLIENT_CHANGE_ITEM.replace("#{site}#", HttpAppService.getSite());
		 return plcPost({
			url: url,
			params : params
		 });
       }
        function upload(params){
          	 var url = HttpAppService.URLS.UPLOAD_EXCEL_DATA_WORKORDER.replace("#{site}#", HttpAppService.getSite());
          	 return HttpAppService.post({
   				url: url,
   				paras : params
   			});
          }
		function plcPost(obj){
			return HttpAppService.post({
				url: obj.url+"?plcIp="+window.localStorage.plcIp,
				paras : obj.params
			})
		}
    }]);
clientModule
    .run([
        '$templateCache',
        function ($templateCache){
            $templateCache.put('andon-ui-grid-tpls/planStopStart',
                "<div ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><form name=\"inputForm\"><input type=\"datetime-local\" ng-class=\"'colt' + col.uid\" ui-grid-edit-dropdown ng-model=\"row.entity.startDateTimeIns\"></form></div>"
            );
            $templateCache.put('andon-ui-grid-tpls/planStopEnd',
                "<div ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><form name=\"inputForm\"><input type=\"datetime-local\" ng-class=\"'colt' + col.uid\" ui-grid-edit-dropdown ng-model=\"row.entity.endDateTimeIns\" ></form></div>"
            );
            $templateCache.put('andon-ui-grid-tpls/planStopReason',
                "<div ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><form name=\"inputForm\"><select ng-class=\"'colt' + col.uid\" ui-grid-edit-dropdown ng-model=\"row.entity.reasonCode\" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] CUSTOM_FILTERS for field in editDropdownOptionsArray\"></select></form></div>"
            );
        }]);










    