clientModule.controller('faultAndHiddenTroubleNoticeController', [
    '$scope', '$http', '$q',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants','$uibModal','gridUtil','UtilsService','clientService','$state',
    function ($scope, $http, $q, HttpAppService, $timeout, $interval,
              uiGridConstants,$uibModal,gridUtil,UtilsService,clientService,$state){
        $scope.config = {
           title:'故障隐患通知单创建',
           resource:'',              //设备编码
           resourceName:'',          //设备名称
           user:'',
           userName:'',
           resourceSeacrhIcon:true,
           localResource:{},
           disabledCreateBtn:true,
           QMTXT:'',
        };
        $scope.init = function(){
        	$scope.config.noticeTypes = [ {
				notice : '',
				description : '请选择',
				descriptionNew : '请选择'
			}, {
				notice : 'Z1',
				description : '故障通知',
				descriptionNew : 'Z1-故障通知'
			}, {
				notice : 'Z5',
				description : '隐患通知',
				descriptionNew : 'Z5-隐患通知'
			} ];
        	getLogonUser();
        };
        $scope.init();
        //搜索设备
        $scope.searchResource = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/client/modal/modalResource.html',
                controller: 'resourceController',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectMoreDevice: function () {
                        return {
                        	workArea: '',
                        	workLine: '',
                        	deviceCode : ''
                        };
                    }
                }
            });
            modalInstance.result.then(function (data) {
            	$scope.config.resource=data.resourceObj.resrce;
            	$scope.config.resourceName=data.resourceObj.description;
            	$scope.config.resourceSeacrhIcon=false;
            }, function () {
            });
        };
        $scope.deleteResource=function(){
        	$scope.config.resource="";
        	$scope.config.resourceName="";
        	$scope.config.resourceSeacrhIcon=true;
        }
        $scope.createNotice=function(){
        	if($scope.config.QMTXT.length>40){
    			$scope.addAlert('danger', '通知单描述长度超过40');
    			return;
    		}
        	if(UtilsService.isNull($scope.config.resource)||UtilsService.isNull($scope.config.QMTXT)||UtilsService.isNull($scope.config.currentNoticeType.notice)){
        		$scope.addAlert('danger', '请输入必填项');
        		return;
        	}
        	var obj={};
        	obj.QMTXT=$scope.config.QMTXT;
        	obj.EQUNR=$scope.config.resource;
        	obj.QMNAM=$scope.config.user;
        	obj.QMDAT='';  //日期
        	obj.MZEIT='';  //日期 
        	if($scope.config.currentNoticeType.notice=='Z1'){
        		obj.QMART='Z1';
        		createFaultNotice(obj);
        	}else if(($scope.config.currentNoticeType.notice=='Z5')){
        		obj.QMART='Z5';
        		createHiddenTroubleNotice(obj);
        	}
        }
        function createFaultNotice(params){
            $scope.showBodyModel("正在创建中,请稍后...");
        	clientService.__createFaultNotice(params).then(function (resultDatas){
                $scope.hideBodyModel();
                if(resultDatas.response){
                	if(resultDatas.response.esubrc==0){
	                   	 $scope.addAlert("success",resultDatas.response.emsg);
	                   	 return;
                    }else if(resultDatas.response.esubrc==1){
	                   	 $scope.addAlert("danger",resultDatas.response.emsg);
	                   	 return;
                    }
                }
            },function (resultDatas){
                $scope.hideBodyModel();
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        function createHiddenTroubleNotice(params){
            $scope.showBodyModel("正在创建中,请稍后...");
        	clientService.__createFaultNotice(params).then(function (resultDatas){
                $scope.hideBodyModel();
                if(resultDatas.response){
                	if(resultDatas.response.esubrc==0){
                      	$scope.addAlert("success",resultDatas.response.emsg);
                      	return;
                   }else if(resultDatas.response.esubrc==1){
	                  	$scope.addAlert("danger",resultDatas.response.emsg);
	                  	return;
                   }
                }
            },function (resultDatas){
                $scope.hideBodyModel();
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        function getLogonUser(){
        	clientService.__getLogonUser().then(function (resultDatas){
                 if(resultDatas.response){
                	 $scope.config.user=resultDatas.response.user;
                     if(resultDatas.response.firstName)
                     {
                         $scope.config.userName=resultDatas.response.lastName+resultDatas.response.firstName;
                     }else{
                         $scope.config.userName=resultDatas.response.lastName;
                     }

                     return;
                 }
             },function (resultDatas){
                 $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
             });
        }
    }]);