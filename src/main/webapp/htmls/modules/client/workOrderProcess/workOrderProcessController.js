clientModule.controller('workOrderProcessController', [
    '$scope', '$http', '$q',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants','$uibModal', 'gridUtil','UtilsService','clientService','$state','uiGridCellNavConstants',
    function ($scope, $http, $q, HttpAppService, $timeout, $interval,
              uiGridConstants,$uibModal,gridUtil,UtilsService,clientService,$state,uiGridCellNavConstants){
		//var loadConfigRequest = new XMLHttpRequest();
		//loadConfigRequest.open('GET', 'http://172.26.68.20:50100/mhp-oee/auth/logon_user?permsite=1030',false);
		//loadConfigRequest.send(null);
		//if (loadConfigRequest.status === 200 || loadConfigRequest.status === 0) {
        //
		//}
        $scope.config = {
           title:'通知单工单处理',
           resourceMeStaff:'',                      //ME工程师工号
           meStaffName:'',						    //ME工程师姓名
           meStaffLocked:'',                        //ME工程师是否锁定
           resource:'',                            //设备编码
           resourceName:'',                        //设备信息
           isShwoMeReceiveOrderBtn:false,          //是否显示接单按钮
           isShowWorkOrderStatuBtn:false,          //是否显示维修结束按钮
           isShowMaintenanceResultOKBtn:false,     //是否显示维修结果确认OK按钮
           //meReceiveOrderBtn:true,				//是否禁用接单按钮	
           meReceiveOrderData:[],
           cmMaintenanceWorkOrder:'',
           cmcmMaintenanceResultOK:'',
           resourceCurrentData:{},
           disabledMeReceiverOrderBtn:true,
           resourceOperator:'',
           operatorName:'',
           operatorLocked:'',
           disabledMaintenanceResultOKBtn:true,
           disabledWorkOrderStatuBtn:true,
        };
        $scope.init = function(){
        	//界面打开，则加载数据
        	initResourceInfo();
        };
        $scope.init();
        $scope.logout=function(){
        	var params=$scope.config.resourceCurrentData;
        	clientService.__plcMeUserLogout(params).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200){
        			if(resultDatas.response.code==0){
        				$scope.addAlert("success","注销成功");
        				//initResourceInfo();
						writeUserLogInfo(params,0);
        			}else if(resultDatas.response.code==1){
        				$scope.addAlert("danger",resultDatas.response.msg);
        			}else{
        				$scope.addAlert("dange","未知错误");
        			}
                }
            },function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        $scope.getHeight=function(){
        	if ($scope.workOrderGrid.data.length > 0) {
                var rowHeight = 31;
                var headerHeight = 50;
                return {
                    height: (15 * rowHeight + headerHeight) + "px"
                };
            }
        }
        //刷新
        $scope.refresh=function(){
        	initData($scope.config.resource);
        	$scope.config.meReceiveOrderData=[];       //清空
        }
        //锁定
        $scope.locked=function(){
        	var params=$scope.config.resourceCurrentData;
        	params.type="meUser";
        	params.meStaffLocked="true";
        	clientService.__changeLocked(params).then(function (data){
        		if(data.myHttpConfig.status == 200&&data.response){
        			if(data.response.code==0){
        				$scope.addAlert("success","锁定成功");
        				initResourceInfo();
        			}else if(data.response.code==1){
        				$scope.addAlert('danger', "锁定失败");
        			}
                }
            },function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        //打开解锁页面
        $scope.unlockedPage = function(){
        	var modalInstance = $uibModal.open({
				animation : true,
				templateUrl : 'modules/client/modal/operatorLogin.html',
				controller : 'operatorLoginContrller',
				size : 'md',
				backdrop : 'false',
				scope : $scope,
				openedClass : 'flex-center-parent',
				resolve : {
					userModel : function() {
						return {
							modalTitle:'解锁ME工程师',
							resourceMeStaff:$scope.config.resourceMeStaff,
						};
					}
				}
			});
			modalInstance.result.then(function(data) {
				var params=$scope.config.resourceCurrentData;
				params.userName=data.userName;
				params.passWord=data.passWord;
				unlocked(params);
			}, function() {
			});
        }
        //变更工单状态
        $scope.changeWorkOrderStatu=function(){
        /*	var array=$scope.config.meReceiveOrderData;
        	var item='';
        	for(var i=0;i<array.length;i++){
        		if((array[i].AUFART=='ZP01'&&!UtilsService.isNull($scope.config.meStaffName)&&$scope.config.meStaffLocked!=true)
    					&&(array[i].TXT04=='现场接单'||array[i].TXT04=='待料'||array[i].TXT04=='委外服务')){
        			item=array[i].AUFNR;
        		}
        	}*/
        	/*var params={
        			IAUFNR:item,
        			IANDONUSER:$scope.config.meStaffName,
        			ISTATUS:"D",
        	}*/
        	var params={
        			IAUFNR:$scope.config.cmMaintenanceWorkOrder,
        			IANDONUSER:$scope.config.meStaffName,
        			ISTATUS:"D",
        	}
        	clientService.__plcChangeWorkOrderStatu(params).then(function (resultDatas){
        		if(resultDatas.response){
        			if(resultDatas.response.esubrc=='S'){
        				$scope.addAlert("success",resultDatas.response.emsg);
        				initResourceInfo();
        			}else if(resultDatas.response.esubrc=='E'){
        				$scope.addAlert("danger",resultDatas.response.emsg);
        			}
                }
        	},function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        //维修结果确认弹出框
        $scope.maintenanceResultConfirmation = function(type){
        	var params={
        			IANDONUSR:$scope.config.resourceOperator,
        			items:[{"AUFNR":$scope.config.cmcmMaintenanceResultOK}],
        	}
        	if(type=='OK'){
        		params.IRESULT=0;
        		maintenanceRC(params);
        	}else if(type=='NG'){
        		params.IRESULT=1;
        		maintenanceRC(params);
        	}
		};
		//me接单
        $scope.meReceiveOrder=function(){
        	var items=[];
        	var array=$scope.workOrderGrid.data;
        	for(var i=0;i<array.length;i++){
        		if(array[i].isSelected == true){
        			if((!UtilsService.isNull(array[i].QMNUM)&&UtilsService.isNull(array[i].AUFNR))
	        				  ||(array[i].TXT04=='待接单')||(array[i].TXT04=='远程接单')){
        				var obj={};
            			obj.AUFNR=array[i].QMNUM;
            			items.push(obj);
        			}else{
        				$scope.addAlert("danger","禁止重复接单");
        				return;
        			}
        		}
        	}
        	if(items.length==0){
        		$scope.addAlert("danger","选择单号不能为空");
        		return;
        	}
        	var params={
        			IANDONUSER:$scope.config.meStaffName,
        			IZSPOT:'0',                               //0代表现场接单，1代表远程接单
        			items:items,                               //UtilsService.removeDul(items)
        			IRELEASE:'X'
            	}
            	clientService.__plcMeReceiveOrder(params).then(function (resultDatas){
            		if(resultDatas.myHttpConfig.status == 200&&resultDatas.response){
            			if(resultDatas.response.e_SUBRC==0){
            				$scope.addAlert("success",resultDatas.response.e_MSG);
            				//callResourceReasonLog();        //调用设备停机原因日志接收接口更新设备停机原因日志，业务还未确认清楚，先注释。
            				initResourceInfo();
            			}else if(resultDatas.response.e_SUBRC==1){
            				$scope.addAlert("danger",resultDatas.response.e_MSG);
            			}
                    }
                },function (resultDatas){
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        }
        $scope.changeMeStaffPage = function(){
        	var modalInstance = $uibModal.open({
				animation : true,
				templateUrl : 'modules/client/modal/operatorLogin.html',
				controller : 'operatorLoginContrller',
				size : 'md',
				backdrop : 'false',
				scope : $scope,
				openedClass : 'flex-center-parent',
				resolve : {
					userModel : function() {
						if(UtilsService.isNull($scope.config.resourceMeStaff)){
							return {
								modalTitle:'维护ME工程师',
							};
						}else{
							return {
								modalTitle:'更换ME工程师',
							};
						}
					}
				}
			});
			modalInstance.result.then(function(data) {
				var params=$scope.config.resourceCurrentData;
				params.userName=data.userName;
				params.passWord=data.passWord;
				params.type="meUser";
				changeMeStaff(params);
			}, function() {
			});
        }
        $scope.workOrderGrid={
            	data:[],
                enableCellEdit:false,
                columnDefs:[
                    {
                        name:"QMNUM",
                        displayName:'通知单号',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass: function (row, col) {
                        	if(col.entity.QMART=='Z1'){
                        		return 'high-light-red';
                        	}
                        },
                        cellTooltip : function(row, col) {
    						return row.entity[col.colDef.name];
    					}
                    },
                    {
                        name:"QMTXT",
                        displayName:'通知单描述',
                        minWidth:150,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass: function (row, col) {
                        	if(col.entity.QMART=='Z1'){
                        		return 'high-light-red';
                        	}
                        },
                        cellTooltip : function(row, col) {
    						return row.entity[col.colDef.name];
    					}
                    },
                    {
                        name:"QMART",
                        displayName:'通知单类型',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass: function (row, col) {
                        	if(col.entity.QMART=='Z1'){
                        		return 'high-light-red';
                        	}
                        },
                        cellTooltip : function(row, col) {
    						return row.entity[col.colDef.name];
    					}
                    },
                    {
                        name:"QMARTX",
                        displayName:'通知单类型描述',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass: function (row, col) {
                        	if(col.entity.QMART=='Z1'){
                        		return 'high-light-red';
                        	}
                        },
                        cellTooltip : function(row, col) {
    						return row.entity[col.colDef.name];
    					}
                    },
                    {
                        name:"GSTRP",
                        displayName:'计划日期',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass: function (row, col) {
                        	if(col.entity.QMART=='Z1'){
                        		return 'high-light-red';
                        	}
                        },
                        cellTooltip : function(row, col) {
    						return row.entity[col.colDef.name];
    					}
                    },
                    {
                        name:"AUFNR",
                        displayName:'工单号',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass: function (row, col) {
                        	if(col.entity.QMART=='Z1'){
                        		return 'high-light-red';
                        	}
                        },
                        cellTooltip : function(row, col) {
    						return row.entity[col.colDef.name];
    					}
                    },
                    {
                        name:"KTEXT",
                        displayName:'工单描述',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass: function (row, col) {
                        	if(col.entity.QMART=='Z1'){
                        		return 'high-light-red';
                        	}
                        },
                        cellTooltip : function(row, col) {
    						return row.entity[col.colDef.name];
    					}
                    },
                    {
                        name:"AUFART",
                        displayName:'工单类型',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass: function (row, col) {
                        	if(col.entity.QMART=='Z1'){
                        		return 'high-light-red';
                        	}
                        },
                        cellTooltip : function(row, col) {
    						return row.entity[col.colDef.name];
    					}
                    },
                    {
                        name:"AUARTTEXT",
                        displayName:'工单类型描述',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass: function (row, col) {
                        	if(col.entity.QMART=='Z1'){
                        		return 'high-light-red';
                        	}
                        },
                        cellTooltip : function(row, col) {
    						return row.entity[col.colDef.name];
    					}
                    },
                    {
                        name:"TXT04",
                        displayName:'工单状态',
                        minWidth:140,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass: function (row, col) {
                        	if(col.entity.QMART=='Z1'){
                        		return 'high-light-red';
                        	}
                        },
                        cellTooltip : function(row, col) {
    						return row.entity[col.colDef.name];
    					}
                    },
                    {
                        name:"TXT30",
                        displayName:'工单状态描述',
                        minWidth:140,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass: function (row, col) {
                        	if(col.entity.QMART=='Z1'){
                        		return 'high-light-red';
                        	}
                        },
                        cellTooltip : function(row, col) {
    						return row.entity[col.colDef.name];
    					}
                    },
                    {
    		    		name: 'edit',
    		    		displayName: '工单编辑',
    		    		minWidth: 40,
    		    		enableCellEdit: false,
    		    		enableCellEditOnFocus: false,
    		    		cellClass: function (row, col) {
                        	if(col.entity.QMART=='Z1'){
                        		return 'high-light-red';
                        	}
                        },
    		    		enableColumnMenu: false,
    		    		enableColumnMenus: false,
    		    		cellTemplate: function(){
    		    				return '<div class="ui-grid-cell-contents ad-delete-col"><span class="glyphicon glyphicon-pencil"></span></div>';
    		    		},
    		    	}
                ],
                onRegisterApi: __onRegisterApi
            };
        function __onRegisterApi(gridApi){
        	$scope.config.gridApi = gridApi;

            //gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
				//UtilsService.log("进入注册事件");
    		//	if(newRowCol.col.name == "edit" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'&&!UtilsService.isNull(newRowCol.row.entity.AUFNR)){
    		//		edit(newRowCol.row.entity.AUFNR);
    		//	}
    		//});
            $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                gridRow.entity.isSelected = gridRow.isSelected;
                if(gridRow.isSelected==true){
                	$scope.config.meReceiveOrderData.push(gridRow.entity);
                }
            });
            $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                for(var i = 0; i < gridRows.length; i++){
                    gridRows[i].entity.isSelected = gridRows[i].isSelected;
                }
            });
        }
        function edit(QMNUM){
			var mode="1";
			if(!$scope.config.resourceMeStaff || $scope.config.meStaffLocked == "true"){
				mode="0";
			}
        	var params={
        			IUSER:$scope.config.meStaffName,
        			MODE:mode,
        			AUFNR:QMNUM,
        	}
        	clientService.__plcEditWorkOrder(params).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200){
        			window.open(resultDatas.response.url);   
                    return;
                }
        	},function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        function changeMeStaff(params){
        	clientService.__plcChangeOperator(params,1).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200){
        			console.info(resultDatas.response);
        			if(resultDatas.response.code==0){
        				$scope.addAlert("success","更新成功");
        				//initResourceInfo();
						writeUserLogInfo(params,1);
        			}else{
						$scope.addAlert('danger',resultDatas.response.msg);
        			}
                }
            },function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }

		//记录用户登入登出日志  第二个参数代表 1 更换或者维护  0 注销
		function writeUserLogInfo(params,code){
			if($scope.config.resourceMeStaff)
			{
				var paramLoginOut={
					"site":ROOTCONFIG.AndonConfig.SITE,
					"user":$scope.config.resourceMeStaff,
					"resrce":$scope.config.resource,
					"actionCode":"LOGOUT",
					"loginMode":"ONSITE",
					"userDepartment":"ME"
				};

				clientService._plcUserLogInfo(paramLoginOut,0).then(function (resultDatas){
					if(resultDatas.myHttpConfig.status == 200){
						if(code){
							UtilsService.log("第二次执行");
							paramLoginOut.actionCode="LOGIN";
							paramLoginOut.user=params.userName;
							clientService._plcUserLogInfo(paramLoginOut,1).then(function (resultDatas){
								if(resultDatas.myHttpConfig.status == 200){
									initResourceInfo();
								}
							},function (resultDatas){
								$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
							});
						}else {
							initResourceInfo();
						}
					}
				},function (resultDatas){
					$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
				});
			}else{
				var paramLoginOut={
					"site":ROOTCONFIG.AndonConfig.SITE,
					"user":params.userName,
					"resrce":$scope.config.resource,
					"actionCode":"LOGIN",
					"loginMode":"ONSITE",
					"userDepartment":"ME"
				};
				clientService._plcUserLogInfo(paramLoginOut,1).then(function (resultDatas){
					if(resultDatas.myHttpConfig.status == 200){
						initResourceInfo();
					}
				},function (resultDatas){
					$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
				});
			}

		}
        //维修结果确认接口
        function maintenanceRC(params){
        	clientService.__maintenanceResultConfirmation(params).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200&&resultDatas.response.length>0){
        			if(resultDatas.response[0].SUBRC==0){
        				$scope.addAlert("success",resultDatas.response[0].MSG);
        			}else if(resultDatas.response[0].SUBRC==1){
        				$scope.addAlert("danger",resultDatas.response[0].MSG);
        			}
        			initResourceInfo();
                }
            },function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        function initResourceInfo(){
        	clientService.__initResourceInfo().then(function (resultDatas){
        		if(resultDatas.response){
        			$scope.config.resource=resultDatas.response.resource;
                    $scope.config.resourceName=resultDatas.response.resource+resultDatas.response.resourceName;
                    $scope.config.resourceMeStaff=resultDatas.response.resourceMeStaff;
                    $scope.config.meStaffLocked=resultDatas.response.meStaffLocked;
					UtilsService.log("$scope.config.meStaffLocked:"+$scope.config.meStaffLocked);
                    $scope.config.resourceCurrentData=resultDatas.response;
                    $scope.config.resourceOperator=resultDatas.response.resourceOperator;
               	    $scope.config.operatorLocked=resultDatas.response.operatorLocked;
                    getResourceMeStaffInfo($scope.config.resourceMeStaff);
                    getResourceOperatorInfo($scope.config.resourceOperator);
                    initData($scope.config.resource);
                    return;
                }
        	},function (resultDatas){
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }
        function getResourceMeStaffInfo(userId){
        	clientService.__getUserInfo(userId).then(function (resultDatas){
                 if(resultDatas.response){
                	 var lastName="";
                	 var firstName="";
                	 if(!UtilsService.isNull(resultDatas.response.serverUserDetailsVO.lastName)){
                		 lastName=resultDatas.response.serverUserDetailsVO.lastName
                	 }
                	 if(!UtilsService.isNull(resultDatas.response.serverUserDetailsVO.firstName)){
                		 firstName=resultDatas.response.serverUserDetailsVO.firstName;
                	 }
                	 $scope.config.meStaffName=lastName+firstName;
                	 if(!UtilsService.isNull($scope.config.meStaffName)&&$scope.config.meStaffLocked!="true"){
                		 $scope.config.disabledWorkOrderStatuBtn=false;
                		 $scope.config.disabledMeReceiverOrderBtn=false;
                	 }else{
                		 $scope.config.disabledMeReceiverOrderBtn=true;
                		 $scope.disabledWorkOrderStatuBtn=true;
                	 }
                     return;
                 }
             },function (resultDatas){
                 $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
             });
        }
        function getResourceOperatorInfo(userId){
        	clientService.__getUserInfo(userId).then(function (resultDatas){
                 if(resultDatas.response){
                	 var lastName="";
                	 var firstName="";
                	 if(!UtilsService.isNull(resultDatas.response.serverUserDetailsVO.lastName)){
                		 lastName=resultDatas.response.serverUserDetailsVO.lastName
                	 }
                	 if(!UtilsService.isNull(resultDatas.response.serverUserDetailsVO.firstName)){
                		 firstName=resultDatas.response.serverUserDetailsVO.firstName;
                	 }
                	 $scope.config.operatorName=lastName+firstName;
                	 if(!UtilsService.isNull($scope.config.operatorName)&&$scope.config.operatorLocked!="true"){
                		 $scope.config.disabledMaintenanceResultOKBtn=false;
                	 }else{
                		 $scope.config.disabledMaintenanceResultOKBtn=true;
                	 }
                     return;
                 }
             },function (resultDatas){
                 $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
             });
        }
        function unlocked(params){
        	params.type="meUser";
        	params.meStaffLocked="false";
        	clientService.__changeLocked(params).then(function (data){
        		if(data.myHttpConfig.status == 200&&data.response){
        			if(data.response.code==0){
        				$scope.addAlert("success","解锁成功");
        				initResourceInfo();
        			}else if(data.response.code==1){
        				$scope.addAlert('danger', "用户名或密码错误");
        			}
                }
            },function (data){
                $scope.addAlert('danger', data.myHttpConfig.statusDesc);
            });
        }
        function callResourceReasonLog(){
        	var params={
        		    "items": [
        		              {
        		                  "site":"2001",
        		                  "resrce":"WXXX0001",
        		                  "reasonCode":"NP11",
        		                  "actionCode":"AC-01",
        		                  "maintenanceNotification":"MN_0001",
        		                  "maintenanceOrder":"MO_0001",
        		                  "meUserId":"12345"
        		              },
        		              {
        		                  "site":"2001",
        		                  "resrce":"WXXX0002",
        		                  "reasonCode":"NP11",
        		                  "actionCode":"AC-02",
        		                  "maintenanceNotification":"MN_0002",
        		                  "maintenanceOrder":"MO_0002",
        		                  "meUserId":"12345"
        		              }
        		          ]
        		      };
        	clientService.__callResourceReasonLog(params).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200){
        			console.info(resultDatas.response);
                }
            },function (resultDatas){
            	console.info("11");
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });

        }
        function initData(resource){
        	$scope.showBodyModel("正在加载数据,请稍后......");
        	var params={
        		ZSTATE:'F',
        		items:[{"EQUNR":resource}],
        	}
        	clientService.__plcInitData(params).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200&&resultDatas.response){
        			if(resultDatas.response.length==1&&resultDatas.response[0].MSG=='没有符合条件的数据'){
    					$scope.addAlert('', '没有待处理的通知单、工单！');
    					$scope.workOrderGrid.data=[];
    				}else{
    					$scope.workOrderGrid.data=resultDatas.response;
    				}
        			$scope.config.isShwoMeReceiveOrderBtn=false;//不显示接单按钮
        			$scope.config.isShowWorkOrderStatuBtn=false;//不显示CM维修结束按钮
        			$scope.config.isShowMaintenanceResultOKBtn=false;//不显示CM维修结果确认OK按钮
        			for(var i=0;i<$scope.workOrderGrid.data.length;i++){
        				if((!UtilsService.isNull($scope.workOrderGrid.data[i].QMNUM)&&UtilsService.isNull($scope.workOrderGrid.data[i].AUFNR))
        				  ||($scope.workOrderGrid.data[i].TXT04=='待接单'||$scope.workOrderGrid.data[i].TXT04=='远程接单')){
        					$scope.config.isShwoMeReceiveOrderBtn=true;//显示接单按钮
        				}
        				if(($scope.workOrderGrid.data[i].AUFART=='ZP01')
        					&&($scope.workOrderGrid.data[i].TXT04=='现场接单'||$scope.workOrderGrid.data[i].TXT04=='待料'||$scope.workOrderGrid.data[i].TXT04=='委外服务')){
              					$scope.config.isShowWorkOrderStatuBtn=true;//显示CM维修结束按钮
              					$scope.config.cmMaintenanceWorkOrder=$scope.workOrderGrid.data[i].AUFNR;
              			}
        				if(($scope.workOrderGrid.data[i].AUFART=='ZP01')&&($scope.workOrderGrid.data[i].TXT04=='待确认')){
                  				$scope.config.isShowMaintenanceResultOKBtn=true;//显示CM维修结果确认OK按钮
                  				$scope.config.cmcmMaintenanceResultOK=$scope.workOrderGrid.data[i].AUFNR;
                  		}
        			}
        			$scope.hideBodyModel();
                }
            },function (resultDatas){
            	$scope.hideBodyModel();
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
        }

		$scope.$on(uiGridCellNavConstants.CELL_NAV_EVENT, function (evt, rowCol, modifierDown) {
			UtilsService.log("进入监听事件多次");
			if(rowCol)
			{
				if(rowCol.col.name == "edit" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'&&!UtilsService.isNull(rowCol.row.entity.AUFNR)){
					edit(rowCol.row.entity.AUFNR);
				}
			}
		});
    }]);