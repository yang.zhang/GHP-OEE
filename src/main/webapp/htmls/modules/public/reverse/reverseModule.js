reverseModule.directive('reverseTemplate',['UtilsService',function(UtilsService){
    return{
        restrict:'ACME',
        replace: true,
        //scope: {
        //    header:'=header',
        //    ngDisedit:'=ngDisedit',
        //    availableGroup:'=availableGroup',
        //    assigendGroup:'=assigendGroup',
        //    copyAssigendActivityGroup:'=copyAssigendActivityGroup',
        //    copyAvailableActivityGroup:'=copyAvailableActivityGroup',
        //    isChanged:'=isChanged',
        //},
        scope:true,
        controller:['$scope','$element',function($scope, $element){
            $scope.selectedAssigendActivityGroupList = function(items){
                __selectedAssigendActivityGroupList(items);
            };
            function __selectedAssigendActivityGroupList(items){
                var availableGroup = $scope.config.getAvailableActivityGroup;
                for(var i = 0 ; i < availableGroup.length ; i++){
                    if(availableGroup[i].isSelected){
                        availableGroup[i].isSelected = false;
                    }
                };
                items.isSelected = !items.isSelected;
            };

            $scope.selectedAvailableActivityGroupList = function(items){
                __selectedAvailableActivityGroupList(items);
            };

            function __selectedAvailableActivityGroupList(items){
                var assigendGroup = $scope.config.getAssigendActivityGroup;
                for(var i = 0;i<assigendGroup.length;i++){
                    if(assigendGroup[i].isSelected){
                        assigendGroup[i].isSelected = false;
                    }
                };
                items.isSelected = !items.isSelected;
            };

            $scope.reverseRight = function(){
                for(var i = $scope.config.getAvailableActivityGroup.length - 1; i >= 0; i--){
                    var item = $scope.config.getAvailableActivityGroup[i];
                    if(item.isSelected){
                        $scope.config.getAssigendActivityGroup.push(angular.copy(item));
                        $scope.config.getAvailableActivityGroup = UtilsService.removeArrayByIndex($scope.config.getAvailableActivityGroup, i);
                    }
                }

                __showTooltip();
            };

            $scope.reverseLeft = function(){
                for(var i = $scope.config.getAssigendActivityGroup.length - 1; i >= 0 ; i--){
                    var item = $scope.config.getAssigendActivityGroup[i];
                    if(item.isSelected){
                        $scope.config.getAvailableActivityGroup.push(angular.copy(item));
                        $scope.config.getAssigendActivityGroup = UtilsService.removeArrayByIndex($scope.config.getAssigendActivityGroup, i);
                    }
                }
                __showTooltip();
            };

            function __showTooltip(){
                if(__checkWorkGroupSave($scope.config.getAssigendActivityGroup,$scope.config.getAvailableActivityGroup)){
                    $scope.bodyConfig.overallWatchChanged = true;
                }else{
                    $scope.bodyConfig.overallWatchChanged = false;
                };
            };

            function __checkWorkGroupSave(getAssigendActivityGroup,getAvailableActivityGroup){
                var copyAssigendActivityGroup = $scope.config.copyAssigendActivityGroup;
                var copyAvailableActivityGroup = $scope.config.copyAvailableActivityGroup;

                if(copyAssigendActivityGroup.length != getAssigendActivityGroup.length){
                    return true;
                }else{
                    var assigend = [];
                    var copyAssigend = [];
                    for(var i=0;i<getAssigendActivityGroup.length;i++){
                        assigend.push(getAssigendActivityGroup[i].activity)
                    };
                    for(var i=0;i<copyAssigendActivityGroup.length;i++){
                        copyAssigend.push(copyAssigendActivityGroup[i].activity)
                    };
                    for(var i=0;i<assigend.length;i++){
                        if(copyAssigend.indexOf(assigend[i])<0){
                            return true;
                        }
                    }
                };

                if(copyAvailableActivityGroup.length != getAvailableActivityGroup.length){
                    return true;
                }else{
                    var available = [];
                    var copyAvailable = [];
                    for(var i=0;i<getAvailableActivityGroup.length;i++){
                        available.push(getAvailableActivityGroup[i].activity)
                    };
                    for(var i=0;i<copyAvailableActivityGroup.length;i++){
                        copyAvailable.push(copyAvailableActivityGroup[i].activity)
                    };
                    for(var i=0;i<available.length;i++){
                        if(copyAvailable.indexOf(available[i])<0){
                            return true;
                        }
                    }
                };
                return false;

            };
        }],
        templateUrl:'modules/public/reverse/reverse.html'
    }
}]);