dropdownModule.directive('andonSelect',['UtilsService',function(UtilsService){
    return{
        restrict:'E',
        transclude:true,
        require:'^ngModel',
        scope: {
            duplicate:'@',
            ngModel:'='
        },
        link: function(scope, iElement, iAttrs,ngModel) {

        },
        controller:['$scope','$element','$attrs','$transclude',function($scope, $element, $attrs, $transclude){
            $scope.config = {
                angle:true,
            };
            $scope.init = function(){
                if(angular.isUndefined($scope.duplicate) || $scope.duplicate == null || $scope.duplicate == ''){
                    $scope.duplicate = "false";
                }
                if($scope.duplicate == "false"){
                    //如果没有开启多功能模式，则无法在输入框输入值
                    $('.andon-select').on('click',function(){
                        console.log("*****");
                    });
                }else{
                    //如果开启了多功能模式，输入框即可输入值也可点击下拉输入
                }
            };

            $scope.init();

            $scope.iconSelected = function(){


                $scope.config.angle = !$scope.config.angle;
            };
        }],
        templateUrl:'modules/public/dropdown/andonSelect.html'
    }
}]);
dropdownModule.directive('andonSelectMatch',['UtilsService',function(UtilsService){
    return{
        restrict:'E',
        transclude:true,
        scope: {
            duplicate:'@duplicate',
            bindHtml:'=',
        },
        link: function(scope, iElement, iAttrs) {
            iElement.addClass('andon-select-match');
            $('.andon-select input').attr('id','andon-select');
            $('#andon-select').val(scope.bindHtml);
        },
        controller:['$scope','$element',function($scope, $element){

        }],

    }
}]);
dropdownModule.directive('andonSelectNoChoice',['UtilsService',function(UtilsService){
    return{
        restrict:'E',
        transclude:true,
        scope: {
            duplicate:'@duplicate',
        },

        controller:['$scope','$element',function($scope, $element){
            $scope.showDropDown = function(){};
        }],
        //templateUrl:'modules/public/dropdown/andonSelectNoChoice.html'
    }
}]);
dropdownModule.directive('andonSelectChoice',['UtilsService','AndonRepeatParser',
    function(UtilsService,AndonRepeatParser){
    return{
        restrict:'E',
        scope: {
            repeat:'@',
            bindHtml:'@',
        },
        link: function(scope, iElement, iAttrs) {
        },
        controller:['$scope','$element',function($scope, $element){
            $scope.config = {
                cycleList:null,
                bindHtml:null,
            };
            $scope.init = function(){
                $scope.config.cycleList = $scope.repeat;
                $scope.config.bindHtml = $scope.bindHtml;


                var parserResult = AndonRepeatParser.parse($scope.repeat);


                console.log(parserResult.repeatExpression({code:1001,description:'aaaa'}));

                //angular.forEach($scope.config.cycleList, function(data,index,array){
                //    console.log(data);
                //});
            };
            $scope.init();

        }],
        templateUrl:'modules/public/dropdown/andonSelectChoice.html',
    }
}]);

dropdownModule.service('AndonRepeatParser', ['$parse', function($parse) {
    var self = this;

    /**
     * Example:
     * expression = "address in addresses | filter: {street: $select.search} track by $index"
     * itemName = "address",
     * source = "addresses | filter: {street: $select.search}",
     * trackByExp = "$index",
     */
    self.parse = function(expression) {


        var match;
        //var isObjectCollection = /\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)/.test(expression);
        // If an array is used as collection

        // if (isObjectCollection){
        // 000000000000000000000000000000111111111000000000000000222222222222220033333333333333333333330000444444444444444444000000000000000055555555555000000000000000000000066666666600000000
        match = expression.match(/^\s*(?:([\s\S]+?)\s+as\s+)?(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+(\s*[\s\S]+?)?(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);

        // 1 Alias
        // 2 Item
        // 3 Key on (key,value)
        // 4 Value on (key,value)
        // 5 Source expression (including filters)
        // 6 Track by

        console.log(match);

        var source = match[5],
            filters = '';
        // When using (key,value) ui-select requires filters to be extracted, since the object
        // is converted to an array for $select.items
        // (in which case the filters need to be reapplied)
        if (match[3]) {
            // Remove any enclosing parenthesis
            source = match[5].replace(/(^\()|(\)$)/g, '');
            // match all after | but not after ||
            var filterMatch = match[5].match(/^\s*(?:[\s\S]+?)(?:[^\|]|\|\|)+([\s\S]*)\s*$/);
            if(filterMatch && filterMatch[1].trim()) {
                filters = filterMatch[1];
                source = source.replace(filters, '');
            }
        }

        return {
            itemName: match[4] || match[2], // (lhs) Left-hand side,
            keyName: match[3], //for (key, value) syntax
            source: $parse(source),
            filters: filters,
            trackByExp: match[6],
            modelMapper: $parse(match[1] || match[4] || match[2]),
            repeatExpression: function (grouped) {
                var expression = this.itemName + ' in ' + (grouped ? '$group.items' : '$select.items');
                if (this.trackByExp) {
                    expression += ' track by ' + this.trackByExp;
                }
                return expression;
            }
        };

    };

    self.getGroupNgRepeatExpression = function() {
        return '$group in $select.groups';
    };

}]);
