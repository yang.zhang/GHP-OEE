homeModule.service('homeService', ['HttpAppService',function (HttpAppService) {
    return{
        userpurview : userpurview,
        userinfos : userinfos,
        usersitelist : usersitelist,
        userLogout: userLogout
    };  
    function userpurview(){
        var url = HttpAppService.URLS.USER_PURVIEW
                .replace("#{SITE}#", HttpAppService.getSite())
            ;
        return HttpAppService.get({
            url: url
        });
    };
    function userinfos(){
        var url = HttpAppService.URLS.USER_INFOS
            ;
        return HttpAppService.get({
            url: url
        });
    };
    function usersitelist(userId){
        var url = HttpAppService.URLS.USER_SITELIST
                .replace("#{site}#", HttpAppService.getSite())
                .replace("#{userId}#", userId)
            ;
        return HttpAppService.get({
            url: url
        });
    };
    function userLogout(){
        var url = HttpAppService.URLS.USER_LOGOUT
            ;
        return HttpAppService.get({
            url: url
        });
    };
}]);



