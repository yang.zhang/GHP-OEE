homeModule.controller('absHomePageCtrl', [ 
	'$scope', '$http', '$q',
	'HttpAppService', '$timeout', '$interval',
	'uiGridConstants', 'gridUtil',
	function ($scope, $http, $q, HttpAppService, $timeout, $interval,
		uiGridConstants, gridUtil) {  
	
	$scope.initUI = function(){  
		$timeout(function(){
			initAdminLTEUI(); 
		}, 2000);
	}; 

	$scope.tabGlobalModalForContent = function(){

	};

	$scope.tabGlobalModalForBody = function(){
		
	};
	
	$scope.absHomeConfig = {  
		editRowData: null,
		editDataLog: [],
		switch: { 
			isOpenSideBar: 'nope',
			isActive: true,
			size: 'normal',
			animate: true,
			radioOff: true,
			handleWidth: "auto",
			labelWidth: "auto",
			inverse: true, 
			
			label: '测试',
			icon: '',
			onLabel: '打开',
			offLabel: '关闭',
			on: 'green',
			off: 'red', 
		},

		bodyModalOpen: false,
		contentModalOpen: false

	};

	$scope.maleSizeDropdownOptions = [
		{ id: 1, size: 'SM' },
		{ id: 2, size: 'M' },
		{ id: 3, size: 'L' },
		{ id: 4, size: 'XL' },
		{ id: 5, size: 'XXL' }
	];
	
	$scope.femaleSizeDropdownOptions = [
	    { id: 6, size: '8' },
	    { id: 7, size: '10' },
	    { id: 8, size: '12' },
	    { id: 9, size: '14' },
	    { id: 10, size: '16' }
  	];

	$scope.virtualGridOptions = {
		// paginationPageSizes: [10, 25, 50, 75],
		paginationPageSizes: [1,2,3,10],
	    paginationPageSize: 10,
	    useExternalPagination: true,

	    // enableSorting: true,
	    showGridFooter: false,
	    // enableFiltering: true,
	    modifierKeysToMultiSelectCells: true,
	    columnDefs: [
			{ 
				field: 'name', filterCellFiltered: 'false', 
				// cellTooltip:"这是提示!", 
				headerTooltip: 'headerTooltip提示',
				cellTooltip: function( row, col ) {
		          return 'Name: ' + row.entity.name + ' Company: ' + row.entity.company;
		        },
				enableColumnMenu: false, enableColumnMenus: false,
				enableCellEdit: true, enableCellEditOnFocus: true,
				editDropdownIdLabel: 'code', editDropdownValueLabel: 'name',
				editDropdownOptionsArray: [{code: 1, name: 'active'}, {code: 2, name: 'inactive'}],
				editDropdownOptionsFunction: function(rowEntity, colDef){
					console.log(rowEntity);
					console.log(colDef);
				},
				validators: {minLength: 3, maxLength: 9}
			}, 
			{ field: 'company', cellTooltip:"这是提示!", enableCellEdit: false,},
			{ 
				field: 'gender', cellTooltip: true, headerTooltip: true, 
				cellTooltip:"这是提示!", validators: {minLength: 3, maxLength: 9},
				editDropdownOptionsArray: [
			        { id: 1, gender: 'male' },
			        { id: 2, gender: 'female' }
			      ] 
			},
			{
				field: 'size', displayName:'大小', editDropdownRowEntityOptionsArrayPath: 'sizeOptions'
			},
			{ 	
				field: 'xbr', displayName: 'xbr', type:'boolean',
				cellTemplate: '<input type="checkbox" ng-model="col.xbr" >'
			} 
	    ],
	    onRegisterApi: function( gridApi ) { 
	      $scope.gridApi = gridApi;
	      // 分页相关 
	      $scope.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
		      	console.log("刷新界面 第 "+currentPage+" 页,每页 "+pageSize+"个元素!");
		      	$scope.virtualGridOptions.data.push({
		      		"name": "自动添加  "+currentPage+"  "+pageSize,
			        "gender": "female",
			        "company": "汉得"
		      	});
		      	window.xbrGridUtil = gridUtil;
		      	return true;
	      }, $scope); 
	      // 编辑相关 
	      $scope.gridApi.edit.on.beginCellEdit($scope,function(rowEntity, colDef){

	      		console.log("开始编辑 ..."); 
	      		$scope.absHomeConfig.editRowData = angular.copy(rowEntity);
	      });
	      $scope.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
	      		console.log("完成编辑 ..."); 
	      		var cols = ["name", "gender", "company"];
	      		var changed = false;
	      		for(var col in cols){
	      			if($scope.editRowData !=null && rowEntity[col] != $scope.editRowData[col]){
	      				changed = true;
	      				break;
	      			}
	      		} 
	      		if(changed){ // 先判断是否已经存在，若已经存在，则更新newValue、若不存在，则 push 上去
	      			$scope.absHomeConfig.editDataLog.push({
	      				key: $scope.absHomeConfig.editRowData.$$hashKey,
	      				newValue: angular.copy(rowEntity),
	      				oldValue: angular.copy($scope.absHomeConfig.editRowData)
	      			});
	      		} 
	      		$scope.absHomeConfig.editRowData = null;
	      });
	      $scope.gridApi.edit.on.cancelCellEdit($scope,function(rowEntity, colDef){
	      		console.log("取消编辑 ..."); 
	      		$scope.absHomeConfig.editRowData = null;
	      });
	      // 编辑保存相关
	      gridApi.rowEdit.on.saveRow($scope, function(rowEntity){
	      		var promise = $q.defer();
	      		$scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
	      		$interval(function (){
	      			if(rowEntity.gender == 'male' || rowEntity.gender == 'female'){
	      				promise.resolve();
	      			}else{
	      				promise.reject();
	      			}
	      		}, 10000, 1);
	      });

	      $scope.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
	        $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
	      })
	    },
	    data: [
	    		{
			        "name": "Rosa DyerRosa DyerRosa Dyer",
			        "gender": "female",
			        "company": "Netility",
			        "xbr": false,

			    },
			    {
			        "name": "Christine Compton",
			        "gender": "female",
			        "company": "Bleeko",
			        "xbr": true
			    },
			    {
			        "name": "Milagros Finch",
			        "gender": "female",
			        "company": "Handshake"
			    },
			    {
			        "name": "Ericka Alvarado",
			        "gender": "female",
			        "company": "Lyrichord"
			    },
			    {
			        "name": "Sylvia Sosa",
			        "gender": "female",
			        "company": "Circum"
			    },
			    {
			        "name": "Humphrey Curtis",
			        "gender": "male",
			        "company": "Corepan"
			    }
		] 	
	}; 	

	// $scope.gridOptions = { 
		//     enableFiltering: false,
		//     onRegisterApi: function(gridApi){
		//       // $scope.gridApi = gridApi;
		//       // $scope.gridApi.grid.registerRowsProcessor( $scope.singleFilter, 200 );
		//     }, 
		//     enableCellEditOnFocus: true,
		//     data: null,
		//     columnDefs: [ 
		//       { 
		//       	field: 'select', name: '选择' , 
		//       	width: 75, minWidth: 50, maxWidth: 100, 
		//       	visible: true, 
		//       	filters: null,
		//       	enableCellEdit: false,
		//       	sort: {
		// 			direction: uiGridConstants.ASC,
		// 			ignoreSort: true,
		// 			priority: 0
		//         },
		//         type: uiGridConstants.filter.SELECT,
		//         menuItems: [{
		//         	title: '测试',
	 	//          		action: function() {
		//             	alert('测试: ' + this.grid.id);
		//             }
		//         }]
		//       },
		//       { 
		//       	field: 'site', name: 'site', displayName: '工厂', width:100, 
		//       	enableColumnResizing: true, enableCellEditOnFocus: false
		//       },
		//       { field: 'category', name: 'category', displayName: 'PLC数据类型代码', width:145, enableColumnResizing: true}, //, cellFilter: 'mapGender'
		//       { field: 'description', name: 'description', displayName: 'PLC数据类型描述', width:200, enableColumnResizing: true}
		//     ]
		//   }; 

		  // $http.get('/json/500_complex.json')
		  //   .success(function(data) {
		  //     $scope.gridOptions.data = data;
		  //     $scope.gridOptions.data[0].age = -5;
		 	  
		  //     data.forEach( function addDates( row, index ){
		  //       row.mixedDate = new Date();
		  //       row.mixedDate.setDate(today.getDate() + ( index % 14 ) );
		  //       row.gender = row.gender==='male' ? '1' : '2';
		  //     });
	  //   });

	// __requestTableDatas();

	// function __requestTableDatas(){
	// 	var plc_category = "plc_category";
	// 	var url = HttpAppService.URLS.PLC_CATEGORY_LIST
	// 				.replace("#{SITE}#", HttpAppService.getSite())
	// 				.replace("#{PLC_CATEGORY}#", plc_category);
	// 	HttpAppService.get({
	// 		url: url
	// 	}).then(function (resultDatas){
	// 		console.log("Success: isTimeout: "+JSON.stringify(resultDatas));
	// 		if(resultDatas.response && resultDatas.response.length > 0){
	// 			$scope.gridOptions.data = resultDatas.response;
	// 			return;
	// 		}
	// 	}).then(function (resultDatas){
	// 		console.log("Success: isTimeout: "+JSON.stringify(resultDatas));
	// 	});
	// } 

	// $scope.filter = function() {
	//     $scope.gridApi.grid.refresh();
	// };
	    
	// $scope.singleFilter = function( renderableRows ){
	//     // var matcher = new RegExp($scope.filterValue);
	//     // renderableRows.forEach( function( row ) {
	//     //   var match = false;
	//     //   [ 'name', 'company', 'email' ].forEach(function( field ){
	//     //     if ( row.entity[field].match(matcher) ){
	//     //       match = true;
	//     //     }
	//     //   });
	//     //   if ( !match ){
	//     //     row.visible = false;
	//     //   }
	//     // });
	//     return renderableRows;
	// };

	// $scope.onFactoryInputChange = function(){
	// };

}]);

homeModule
.filter('mapGender', function() {
	var genderHash = {
		1: 'male',
		2: 'female'
	};
	return function(input) {
		if (!input){
		  return '';
		} else {
		  return genderHash[input];
		}
	};
}); 


homeModule.controller('homePageCtrl', ['$scope', '$interval', '$timeout', '$echarts', function ($scope, $interval, $timeout, $echarts) {

	$scope.changeTo = function(section){
		if(section == 1){
			$scope.config.selection = 1;
			$scope.config.tooltipHeader = "区域一";
			$scope.barExample.config.title.text = $scope.config.tooltipHeader+ "   "+$scope.config.title;
			$scope.barExample.config.series = [
				{ name:'良品数', type:'bar', data:[33, 89, 57, 99, 84, 58, 99, 138, 154, 188, 199, 221] },
				{ name:'坏品数', type:'bar', data:[53, 29, 47, 49, 84, 148, 119, 118, 134, 258, 29, 241] },
				{ name: '总产出数', type: 'bar', data:[33, 189, 57, 99, 84, 158, 199, 238, 254, 288, 299, 321] }
			];
		}else if(section == 2){
			$scope.config.selection = 2;
			$scope.config.tooltipHeader = "区域二";
			$scope.barExample.config.title.text = $scope.config.tooltipHeader+ "   "+$scope.config.title;
			$scope.barExample.config.series = [
				{ name:'良品数', type:'bar', data:[83, 29, 57, 199, 84, 128, 99, 238, 154, 88, 159, 121] },
				{ name:'坏品数', type:'bar', data:[53, 99, 87, 149, 284, 148, 319, 18, 634, 258, 29, 341] },
				{ name: '总产出数', type: 'bar', data:[33, 189, 157, 199, 184, 258, 299, 338, 454, 388, 199, 21] }
			];
		}else if(section == 3){
			$scope.config.selection = 3;
			$scope.config.tooltipHeader = "区域三";
			$scope.barExample.config.title.text = $scope.config.tooltipHeader+ "   "+$scope.config.title;
			// $scope.barExample.config.title.text
			$scope.barExample.config.series = [
				{ name:'良品数', type:'bar', data:[183, 19, 47, 159, 84, 328, 199, 138, 254, 188, 259, 221] },
				{ name:'坏品数', type:'bar', data:[153, 199, 187, 189, 384, 198, 219, 18, 634, 158, 129, 241] },
				{ name: '总产出数', type: 'bar', data:[133, 139, 197, 129, 124, 218, 219, 398, 354, 388, 199, 21] }
			];
		}
	};

	$scope.config = {
		selection: '',
		tooltipHeader: '',
		title: '安灯统计报表'
	};

	$scope.barExample1 = {
		identity: $echarts.generateInstanceIdentity(),
		dimension: '16:9',
		config: {
			baseOption: {
				title : {
				  show: true,
				  text: '----',
				  subtext: '产出信息统计报表',
				  left: 'left'
				},
				tooltip : {
				  trigger: 'axis'
				},
				legend: {
				  left: 'right', 
				  data:['良品数', '坏品数','总产出数']
				},
				yAxis : [
					{
					  type : 'value',
					  boundaryGap : [0, 0.0001],
					  min: 0,
					  max: 1000
					}
				],
				xAxis : [
					{
					  type : 'category',
					  data : ['一月','二月','三月','四月','五月','六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
					}
				],
				series : [
					{
						name:'良品数',
						type:'bar',
						data:[33, 189, 57, 99, 84, 158, 199, 238, 254, 288, 299, 321]
					},
					{
						name:'坏品数',
						type:'bar',
						data:[53, 89, 27, 49, 84, 128, 159, 218, 234, 298, 279, 341]
					},
					{
						name: '总产出数',
						type: 'bar',
						data:[133, 289, 157, 199, 184, 258, 299, 338, 354, 388, 399, 421]
					}
				],
				timeline: {
					data: ['2014','2015','2016']
				}
			},
			options: [
				{
					series:[
						{ name:'良品数', type:'bar', data:[33, 89, 57, 99, 84, 58, 99, 138, 154, 188, 199, 221] },
						{ name:'坏品数', type:'bar', data:[53, 29, 47, 49, 84, 148, 119, 118, 134, 258, 29, 241] },
						{ name: '总产出数', type: 'bar', data:[33, 189, 57, 99, 84, 158, 199, 238, 254, 288, 299, 321] }
					]
				},
				{
					series:[
						{ name:'良品数', type:'bar', data:[83, 29, 57, 199, 84, 128, 99, 238, 154, 88, 159, 121] },
						{ name:'坏品数', type:'bar', data:[53, 99, 87, 149, 284, 148, 319, 18, 634, 258, 29, 341] },
						{ name: '总产出数', type: 'bar', data:[33, 189, 157, 199, 184, 258, 299, 338, 454, 388, 199, 21] }
					]
				},
				{
					series: [
						{ name:'良品数', type:'bar', data:[183, 19, 47, 159, 84, 328, 199, 138, 254, 188, 259, 221] },
						{ name:'坏品数', type:'bar', data:[153, 199, 187, 189, 384, 198, 219, 18, 634, 158, 129, 241] },
						{ name: '总产出数', type: 'bar', data:[133, 139, 197, 129, 124, 218, 219, 398, 354, 388, 199, 21] }
					]
				}
			]
		} 
	};
	
	// var xAxis = [{
		//        type: 'category',
		//        data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
		//      }];

		//      var yAxis = [{
		//        type: 'value'
		//      }];

		//      var series = [
		//        {
		//          name: '邮件营销',
		//          type: 'bar',
		//          data: [120, 132, 101, 134, 90, 230, 210]
		//        },
		//        {
		//          name: '联盟广告',
		//          type: 'bar',
		//          data: [220, 182, 191, 234, 290, 330, 310]
		//        }
		//        ,
		//        {
		//          name: '视频广告',
		//          type: 'bar',
		//          data: [150, 232, 201, 154, 190, 330, 410]
		//        },
		//        {
		//          name: '直接访问',
		//          type: 'bar',
		//          data: [320, 332, 301, 334, 390, 330, 320]
		//        },
		//        {
		//          name: '搜索引擎',
		//          type: 'bar',
		//          data: [820, 932, 901, 934, 1290, 1330, 1320]
		//        }
		//      ];

		//      $scope.barExampleList = [0, 1, 2, 3].map(function (value) {
		//        return {
		//          identity: $echarts.generateInstanceIdentity(),
		//          dimension: '16:9',
		//          config: {
		//            xAxis: xAxis,
		//            yAxis: yAxis,
		//            series: [series[value]]
		//          }
		//        };
		//      });

		//      $scope.gauge = {
		//        identity: $echarts.generateInstanceIdentity(),
		//        dimension: '16:9',
		//        config: {
		//          tooltip: {
		//            trigger: 'item',
		//            formatter: "{a} <br/>{b} : {c}%"
		//          },
		//          series: [{
		//            name: '业务指标',
		//            type: 'gauge',
		//            detail: {formatter: '{value}%'},
		//            data: [{value: 55, name: '完成率'}]
		//          }]
		//        }
		//      };

		//      $scope.distribution = {
		//        identity: $echarts.generateInstanceIdentity(),
		//        dimension: '16:9',
		//        config: {
		//          tooltip : {
		//            trigger: 'item',
		//            formatter: "{a} <br/>{b} : {c} ({d}%)"
		//          },
		//          legend: {
		//            orient: 'vertical',
		//            left: 'left',
		//            data: ['直接访问','邮件营销','联盟广告','视频广告','搜索引擎']
		//          },
		//          series : [
		//            {
		//              name: '访问来源',
		//              type: 'pie',
		//              radius : '55%',
		//              center: ['50%', '60%'],
		//              label: {
		//                normal: {
		//                  show: true,
		//                  formatter: "{b} : {c} ({d}%)"
		//                }
		//              },
		//              data:[
		//                {value:335, name:'直接访问'},
		//                {value:310, name:'邮件营销'},
		//                {value:234, name:'联盟广告'},
		//                {value:135, name:'视频广告'},
		//                {value:1548, name:'搜索引擎'}
		//              ]
		//            }
		//          ]
		//        }
		//      };

		//      $timeout(function () {
		//        $scope.gauge.config.series[0].data = [{value: 75, name: '完成率'}];
		//        $echarts.updateEchartsInstance($scope.gauge.identity, $scope.gauge.config);
	//      }, 800);    color: #c23531;
	
    $scope.barExampleW = {
		identity: 'xbr-echarts-demo',//$echarts.generateInstanceIdentity(),
		dimension: '16:9',
		config: {
			title : {
			  show: true,
			  text: $scope.config.title,
			  subtext: '产出信息统计报表',
			  left: 'left'
			},
			grid: {
	            top: 90,
	            bottom: 14
	        },
			tooltip : {
			  trigger: 'axis',
			  // hideDelay: 100000,
			  formatter: function(params){
			  	var p1 = params[0];
			  	var p2 = params[1];
			  	var p3 = params[2];
			  	return p1.name+"  "+$scope.config.tooltipHeader+"<br/> <span class='xbr-tooltip-cls' style='background:"+p1.color+";'></span>"+p1.seriesName+":   "+p1.value+"<br/> <span class='xbr-tooltip-cls' style='background:"+p2.color+";'></span>"+p2.seriesName+":   "+p2.value+"<br/> <span class='xbr-tooltip-cls' style='background:"+p3.color+";'></span>"+p3.seriesName+":   "+p3.value;
			  	// console.log(arguments);
			  	// return "{a} <br/>{b} : {c}%";
			  }
			},
			legend: {
			  left: 'right', 
			  // padding: [0, 0,0,0],
			  data:['良品数', '坏品数','总产出数'],
			  tooltip: {
			  	show: true
			  },
			  // formatter: function (name) {
			  // 	return echarts.format.truncateText("这里面是 "+name+" 的说明信息!", 40, '14px Microsoft Yahei', '…');
			  // }
			},
			yAxis : [
				{
				  type : 'value',
				  boundaryGap : [0, 0.0001],
				  min: 0,
				  max: 1000
				}
			],
			xAxis : [
				{
				  type : 'category',
				  data : ['一月','二月','三月','四月','五月','六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
				}
			],
			series : [
				{
					name:'良品数',
					type:'bar',
					tooltip: {
					  show: true,
					  zlevel: 1
					},
					data:[33, 189, 57, 99, 84, 158, 199, 238, 254, 288, 299, 321]
				},
				{
					name:'坏品数',
					type:'bar',
					data:[53, 89, 27, 49, 84, 128, 159, 218, 234, 298, 279, 341]
				},
				{
					name: '总产出数',
					type: 'bar',
					data:[133, 289, 157, 199, 184, 258, 299, 338, 354, 388, 399, 421]
				}
			]
		} 
	};

	// $scope.changeTo(1);

}]);

// homeModule.controller('BarChartController', function ($scope) {

//         $scope.config = {
//             title: 'Bar Chart',
//             subtitle: 'Bar Chart Subtitle',
//             debug: true,
//             stack: true
//         };

//         $scope.data = [ pageload ];
//         $scope.multiple = [pageload, firstPaint ];

//     });