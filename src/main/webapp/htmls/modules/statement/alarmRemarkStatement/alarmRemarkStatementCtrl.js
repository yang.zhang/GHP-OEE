statementModule.controller('AlarmRemarkStatementCtrl',['$rootScope','$scope', '$http',
    'HttpAppService', '$timeout','UtilsService',
    'uiGridConstants','$uibModal','plcService','statementService','uiGridValidateService',
    function ($rootScope,$scope, $http,
              HttpAppService, $timeout,UtilsService,
              uiGridConstants,$uibModal,plcService,statementService,uiGridValidateService) {

        $scope.config = {
            statement : '近十分钟内报警信息',
            isCollapsed : false,
            gridApi: null,
            currentThemeGrid:null,
            themeGrid:[
                {
                    id:'S',
                    name:'设备状态明细报表'
                }],
            btnDisabledDownload:true,
            alertCount:null,
            itemCount:"",
            reasonCodeCount:null,
            outputCount:null,
            rtReasonCodeCount:null,
            totalHalt:null,
            totalTime:null,
            showSelectLine : true,
            deviceCode : true,
            showSelectArea : true,
            deviceType : true,
            commentList:[],
            isFirst:true,
            ip:"",
            queryStatus:true
        };
        $scope.getHeight=function(){
        	var defaultRowNum=20;
        	var rowHeight=31;
        	return {
        		height:(defaultRowNum*rowHeight)+"px"
        	};
        };

        //报警备注看板
        $scope.gridAlarmRemarkStatement = {
            enablePagination: false,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,
            modifierKeysToMultiSelectCells: false,
            useExternalPagination: false,
            totalItems: 0,
            enableCellEdit:false,
            showGridFooter: false,
            data:[],
            columnDefs:[
                {
                    field: 'handle', name: 'handle', visible: false
                },
                {
                    name:"dateTime",
                    displayName:'报警时间',
                    width:250,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"description",
                    displayName:'报警描述',
                    minWidth:150,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    field: 'alertComment', name: 'alertComment', displayName: '备注', visible: true, width: 250,
                    enableCellEdit: true, enableCellEditOnFocus: false,
                    validators: { required: true },
                    cellTemplate: 'andon-ui-grid-tpls/addinfopart-plcRemark',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownIdLabel: 'alertComment', editDropdownValueLabel: 'alertCommentNew',
                    editDropdownOptionsFunction: function(rowEntity, colDef){
                        UtilsService.log("调用成功");
                        //请求网络更新下拉框列表
                        var results = [
                            {
                                "alertComment":"添加备注",
                                "alertCommentNew":"",
                                "commentCode":""
                            }
                        ];
                        if($scope.gridAlarmRemarkStatement.data.length)
                        {
                            for(var i=0;i<$scope.config.commentList.length;i++){
                                results.push(
                                    {
                                        "alertComment":$scope.config.commentList[i].description,
                                        "alertCommentNew":$scope.config.commentList[i].description,
                                        "commentCode":$scope.config.commentList[i].commentCode
                                    }
                                );
                            }
                        }
                        return results;
                    }
                }
            ],
            onRegisterApi:requestAlarmRemarkStatementApi
        };

        //Test: Print the IP addresses into the console
        getIPs(function(ip){
            $scope.config.ip+=ip+",";
            //initGridInfo();
            UtilsService.log($scope.config.ip);
        });

        //init函数
        $scope.ipTimeout=$timeout(function(){
            if($scope.config.ip.lastIndexOf(",")>-1)
            {
                $scope.config.ip=$scope.config.ip.substring(0,$scope.config.ip.lastIndexOf(","));
            }
            initGridInfo();
        },1500);

        //赋值gridApi
        function requestAlarmRemarkStatementApi(gridApi){
            $scope.config.gridApi = gridApi;
            //$scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
            //    __requestChangeAlarmRemarkData();
            //    return true;
            //}, $scope);
            $scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue) {
                UtilsService.log("row:"+angular.toJson(rowEntity,true));
                UtilsService.log("col:"+angular.toJson(colDef,true));
                UtilsService.log("old value:"+newValue);
                UtilsService.log("new value:"+newValue);

                var comment_code="";
                for(var i=0;i<$scope.config.commentList.length;i++)
                {
                    if($scope.config.commentList[i].description == newValue){
                        comment_code=$scope.config.commentList[i].commentCode;
                    }
                }

                if(newValue == oldValue){
                    return;
                }
                if(oldValue != "添加备注")
                {
                    if(newValue == "添加备注")
                    {
                        var promise =UtilsService.confirm('是否确定删除备注', '提示', ['确定', '取消']);
                        promise.then(function(){
                            //点击确定请求接口
                            updateAlarmRemark(rowEntity.dateTime,rowEntity.description,"",comment_code,rowEntity,colDef,newValue);
                        }, function(){
                            //点击取消无操作
                            rowEntity.alertComment=oldValue;
                            uiGridValidateService.setValid(rowEntity, colDef);
                        });
                    }
                    else{
                        var promise =UtilsService.confirm('是否确定修改备注', '提示', ['确定', '取消']);
                        promise.then(function(){
                            //点击确定请求接口
                            updateAlarmRemark(rowEntity.dateTime,rowEntity.description,newValue,comment_code,rowEntity,colDef,newValue);
                        }, function(){
                            //点击取消无操作
                            rowEntity.alertComment=oldValue;
                            uiGridValidateService.setValid(rowEntity, colDef);
                        });
                    }
                }
                else{
                    var promise =UtilsService.confirm('是否确定添加备注', '提示', ['确定', '取消']);
                    promise.then(function(){
                        //点击确定请求接口
                        updateAlarmRemark(rowEntity.dateTime,rowEntity.description,newValue,comment_code,rowEntity,colDef,newValue);
                    }, function(){
                        //点击取消无操作
                        rowEntity.alertComment=oldValue;
                        uiGridValidateService.setValid(rowEntity, colDef);
                    });
                }

            });
        };

        function __arrayToString(array){
            var string = [];
            for(var i=0;i<array.length;i++){
                string[i] = array[i].workCenter;
            }
            return string.toString();
        }

        //get the IP addresses associated with an account
        function getIPs(callback){
            var ip_dups = {};
            //compatibility for firefox and chrome
            var RTCPeerConnection = window.RTCPeerConnection
                || window.mozRTCPeerConnection
                || window.webkitRTCPeerConnection;
            //bypass naive webrtc blocking
            if (!RTCPeerConnection) {
                var iframe = document.createElement('iframe');
                //invalidate content script
                iframe.sandbox = 'allow-same-origin';
                iframe.style.display = 'none';
                document.body.appendChild(iframe);
                var win = iframe.contentWindow;
                window.RTCPeerConnection = win.RTCPeerConnection;
                window.mozRTCPeerConnection = win.mozRTCPeerConnection;
                window.webkitRTCPeerConnection = win.webkitRTCPeerConnection;
                RTCPeerConnection = window.RTCPeerConnection
                    || window.mozRTCPeerConnection
                    || window.webkitRTCPeerConnection;
            }
            //minimal requirements for data connection
            var mediaConstraints = {
                optional: [{RtpDataChannels: true}]
            };
            //firefox already has a default stun server in about:config
            //    media.peerconnection.default_iceservers =
            //    [{"url": "stun:stun.services.mozilla.com"}]
            var servers = undefined;
            //add same stun server for chrome
            if(window.webkitRTCPeerConnection)
                servers = {iceServers: [{urls: "stun:stun.services.mozilla.com"}]};
            //construct a new RTCPeerConnection
            var pc = new RTCPeerConnection(servers, mediaConstraints);
            //listen for candidate events
            pc.onicecandidate = function(ice){
                //skip non-candidate events
                if(ice.candidate){
                    //match just the IP address
                    var ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3})/
                    var ip_addr = ip_regex.exec(ice.candidate.candidate)[1];
                    //remove duplicates
                    if(ip_dups[ip_addr] === undefined)
                        callback(ip_addr);
                    ip_dups[ip_addr] = true;
                }
            };
            //create a bogus data channel
            pc.createDataChannel("");
            //create an offer sdp
            pc.createOffer(function(result){
                //trigger the stun server request
                pc.setLocalDescription(result, function(){}, function(){});
            }, function(){});
        }

        //init请求接口
        function initGridInfo(){
            if($scope.config.isFirst)
            {
                $scope.showBodyModel("正在加载数据,请稍后...");
                plcService
                    .requestStatementAlarmRemark($scope.config.ip)
                    .then(function (resultData){
                        $scope.config.isFirst=false;
                        UtilsService.log("获取设备编码成功："+angular.toJson(resultData,true));
                        //console.log("获取设备编码成功："+angular.toJson(resultData,true));
                        $scope.statementConfig.currentDeviceNum=resultData.response.resrce;
                        $scope.config.queryStatus=false;
                        $scope.config.deviceCode = false;
                        
                        plcService
                            .requestDataPlcAlarmRemark($scope.statementConfig.currentDeviceNum)
                            .then(function (resultData){
                                $scope.hideBodyModel();
                                UtilsService.log(angular.toJson(resultData,true));
                                if(resultData.response){
                                    $scope.config.commentList=resultData.response.commentList;
                                    //$scope.gridAlarmRemarkStatement.totalItems=resultData.response.commentSize;
                                    // $scope.config.tableDatasCache = angular.copy(resultData.response);
                                    for(var i=0;i<resultData.response.pos.length;i++)
                                    {
                                        resultData.response.pos[i].dateTime=UtilsService.normalFormatDateTimeReport(resultData.response.pos[i].dateTime);
                                        if(!resultData.response.pos[i].commentCode){
                                            resultData.response.pos[i].alertComment="添加备注";
                                        }
                                    }
                                    $scope.gridAlarmRemarkStatement.data = resultData.response.pos;
                                }else{
                                    //$scope.config.tableDatasCache =[];
                                    $scope.gridAlarmRemarkStatement.data = [];
                                    $scope.addAlert('', '暂无数据!');
                                }
                            }, function (errorResultData){
                                $scope.hideBodyModel();
                                //$scope.config.tableDatasCache = [];
                                $scope.gridAlarmRemarkStatement.data = [];
                                $scope.addAlert('', errorResultData.myHttpConfig.statusDesc);
                            });
                    }, function (errorResultData){
                        $scope.config.isFirst=false;
                        UtilsService.log("获取设备编码错误："+angular.toJson(errorResultData,true));
                        $scope.hideBodyModel();
                        $scope.addAlert('', errorResultData.myHttpConfig.statusDesc);
                    });
            }
            else{
                UtilsService.log("进入成功");
                if($scope.statementConfig.currentDeviceNum) {
                    plcService
                        .requestDataPlcAlarmRemark($scope.statementConfig.currentDeviceNum)
                        .then(function (resultData) {
                            //$scope.hideBodyModel();
                            UtilsService.log(angular.toJson(resultData, true));
                            if (resultData.response) {
                                $scope.config.commentList = resultData.response.commentList;
                                //$scope.gridAlarmRemarkStatement.totalItems=resultData.response.commentSize;
                                // $scope.config.tableDatasCache = angular.copy(resultData.response);
                                for (var i = 0; i < resultData.response.pos.length; i++) {
                                    resultData.response.pos[i].dateTime = UtilsService.normalFormatDateTimeReport(resultData.response.pos[i].dateTime);
                                    if (!resultData.response.pos[i].commentCode) {
                                        resultData.response.pos[i].alertComment = "添加备注";
                                    }
                                }
                                $scope.gridAlarmRemarkStatement.data = resultData.response.pos;
                            } else {
                                //$scope.config.tableDatasCache =[];
                                $scope.gridAlarmRemarkStatement.data = [];
                                $scope.addAlert('', '暂无数据!');
                            }
                        }, function (errorResultData) {
                            //$scope.hideBodyModel();
                            //$scope.config.tableDatasCache = [];
                            $scope.gridAlarmRemarkStatement.data = [];
                            $scope.addAlert('', errorResultData.myHttpConfig.statusDesc);
                        });
                }
            }

            $scope.timeouter=$timeout(function () {
                initGridInfo();
            },15000);
            
            //plcService.getIp().then(function (resultDatas){
			//	if(resultDatas.response){
			//		$scope.config.ip=resultDatas.response.ip;
			//	}else{
			//		$scope.addAlert('', 'no');
			//	}
			//}, function (resultDatas){
			//	$scope.addAlert("danger", "failed");
			//});
        }


        function __requestChangeAlarmRemarkData(){
            $scope.gridAlarmRemarkStatement.data = [];
//            var pageIndexX = 1;
//            var pageCountX = $scope.gridAlarmRemarkStatement.paginationPageSize;
//            if(!pageIndex|| pageIndex<=1){
//                pageIndexX = 1;
//            }else{
//                pageIndexX = pageIndex;
//            }
//            if(!pageCount || pageCount<1){
//                pageCountX = $scope.gridAlarmRemarkStatement.paginationPageSize;
//            }else{
//                pageCountX = pageCount;
//            }
            plcService
                .requestDataPlcAlarmRemark($scope.statementConfig.currentDeviceNum)
                .then(function (resultData){
                    UtilsService.log(angular.toJson(resultData,true));
                    if(resultData.response){
                        $scope.config.commentList=resultData.response.commentList;
                        //$scope.gridAlarmRemarkStatement.totalItems=resultData.response.commentSize;
                        // $scope.config.tableDatasCache = angular.copy(resultData.response);
                        for(var i=0;i<resultData.response.pos.length;i++)
                        {
                            resultData.response.pos[i].dateTime=UtilsService.normalFormatDateTimeReport(resultData.response.pos[i].dateTime);
                            if(!resultData.response.pos[i].commentCode){
                                resultData.response.pos[i].alertComment="添加备注";
                            }
                        }
                        $scope.gridAlarmRemarkStatement.data = resultData.response.pos;
                    }else{
                        //$scope.config.tableDatasCache =[];
                        $scope.gridAlarmRemarkStatement.data = [];
                        $scope.addAlert('', '暂无数据!');
                    }
                }, function (errorResultData){
                    //$scope.config.tableDatasCache = [];
                    $scope.gridAlarmRemarkStatement.data = [];
                    $scope.addAlert('', errorResultData.myHttpConfig.statusDesc);
                });
        }

        //修改备注
        function updateAlarmRemark(date_time,description,alert_comment,comment_code,rowEntity,colDef,newValue) {
            plcService
                .updateDataPlcAlarmRemark($scope.statementConfig.currentDeviceNum,date_time,description,alert_comment,comment_code)
                .then(function (resultData){
                    UtilsService.log(angular.toJson(resultData,true));
                    $scope.addAlert('', '修改成功!');
                    rowEntity.alertComment=newValue;
                    uiGridValidateService.setValid(rowEntity, colDef);
                }, function (errorResultData){
                    $scope.addAlert('', errorResultData.myHttpConfig.statusDesc);
                });
        }

        $scope.queryData=function(){
            plcService
                .requestDataPlcAlarmRemark($scope.statementConfig.currentDeviceNum)
                .then(function (resultData) {
                    //$scope.hideBodyModel();
                    UtilsService.log(angular.toJson(resultData, true));
                    if (resultData.response) {
                        $scope.config.commentList = resultData.response.commentList;
                        //$scope.gridAlarmRemarkStatement.totalItems=resultData.response.commentSize;
                        // $scope.config.tableDatasCache = angular.copy(resultData.response);
                        for (var i = 0; i < resultData.response.pos.length; i++) {
                            resultData.response.pos[i].dateTime = UtilsService.normalFormatDateTimeReport(resultData.response.pos[i].dateTime);
                            if (!resultData.response.pos[i].commentCode) {
                                resultData.response.pos[i].alertComment = "添加备注";
                            }
                        }
                        $scope.gridAlarmRemarkStatement.data = resultData.response.pos;
                    } else {
                        //$scope.config.tableDatasCache =[];
                        $scope.gridAlarmRemarkStatement.data = [];
                        $scope.addAlert('', '暂无数据!');
                    }
                }, function (errorResultData) {
                    //$scope.hideBodyModel();
                    //$scope.config.tableDatasCache = [];
                    $scope.gridAlarmRemarkStatement.data = [];
                    $scope.addAlert('', errorResultData.myHttpConfig.statusDesc);
                });
        }

        $scope.controlQueryBtn=function(){
            if($scope.statementConfig.currentDeviceNum)
            {
               $scope.config.queryStatus=false;
            }else{
                $scope.config.queryStatus=true;
            }

        }

        //设备帮助
        $scope.toDeviceHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/modalOneDeviceHelp.html',
                controller: 'deviceOneCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectOneDevice: function () {
                        return {
                            //reportType:'Plato'
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentDeviceNum = selectedItem.area;
                if($scope.statementConfig.currentDeviceNum.length >0){
                    $scope.config.deviceCode = false;
                }
                $scope.controlQueryBtn();
            }, function () {
            });
        };

        //清除输入框
        $scope.deleteAll = function(item){
                $scope.statementConfig.currentDeviceNum = "";
                $scope.config.deviceCode = true;
                $scope.controlQueryBtn();
        }

        //控制显示隐藏列
        $scope.showHideCol=function () {
              for(var i=0;i<$scope.gridAlarmRemarkStatement.columnDefs.length;i++) {
                  if (!$scope.gridAlarmRemarkStatement.columnDefs[i].visible) {
                      $scope.gridAlarmRemarkStatement.columnDefs[i].visible = true;
                  }
              }
              $scope.config.gridApi.core.refresh();
        };

        //没过15秒刷新
        $scope.$on('$destroy',function(){
           $timeout.cancel( $scope.timeouter);
           $timeout.cancel($scope.ipTimeout);
        })
    }]);
