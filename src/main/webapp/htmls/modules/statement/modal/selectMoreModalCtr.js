/**
 * Created by daidongdong on 16/10/14.
 */
//生产区域帮助
statementModule.controller('areaSelectMoreCtrl', ['$scope','$uibModalInstance','selectAreaModal','statementService','UtilsService','$timeout', function ($scope, $uibModalInstance,selectAreaModal,statementService,UtilsService,$timeout) {
    $scope.config = {
        confirmBtn : true,
        areaHelp:null,
        isFirstSelect:true
    };
/*    $scope.init=function(){
        var area= $scope.config.areaHelp?$scope.config.areaHelp:'';
        var areaArrary=area.split(";");
        $scope.config.gridFilterArea=[];
        $scope.gridModalAreaHelp.data=selectAreaModal.workArea;
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
        	var desc=$scope.gridModalAreaHelp.data[i].description;
        	for(var j=0;j<areaArrary.length;j++){
        		if(desc.indexOf(areaArrary[j])!=-1){
            		$scope.config.gridFilterArea.push($scope.gridModalAreaHelp.data[i]);
            	}
        	}
        }
        $scope.gridModalAreaHelp.data=angular.copy($scope.config.gridFilterArea);
    }*/
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        columnDefs: [
            {
                field: 'description', 
                name: 'description', 
                displayName: '描述', 
                minWidth: 100,
                enableCellEdit: false, 
                enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){return row.entity[col.colDef.name]; },
            },
            {
                field: 'workCenter', name: 'workCenter', displayName: '生产区域', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field:"enabled",name:"enabled",displayName:'是否已启用',width: 150,
                cellTooltip: function(row, col){ return "是否已启用"; },
                enableCellEdit: false, enableCellEditOnFocus: false,
                enableColumnMenu: false, enableColumnMenus: false,
                cellTemplate:'andon-ui-grid-rscg/statement-checkbox'
            }
        ],
        data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    };
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.init = function(){
        $scope.gridModalAreaHelp.data = [];
        var area= $scope.config.areaHelp?$scope.config.areaHelp:'';
        if(selectAreaModal.withSiteStatus)
        {
            statementService.outputReportNewAreaHelp(area,selectAreaModal.sites).then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridModalAreaHelp.data = resultDatas.response;
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }else{
                    $scope.gridModalAreaHelp.data = [];
                    $scope.addAlert('', '暂无数据!');
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
            });
            UtilsService.log("执行带choose工厂的接口");
        }else{
            statementService.outputReportAreaHelp(area).then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridModalAreaHelp.data = resultDatas.response;
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }else{
                    $scope.gridModalAreaHelp.data = [];
                    $scope.addAlert('', '暂无数据!');
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
            });
            UtilsService.log("执行不带choose工厂的接口");
        }
    }
    $scope.init();
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.areaHelp = $scope.config.areaHelp?$scope.config.areaHelp.toUpperCase():'';

        if(keycode==13){
            $scope.init();
        }
    };

    $scope.ok = function () {
        var area = [];
        var areaName = [];
        var areaDate = [];
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                area.push($scope.gridModalAreaHelp.data[i].workCenter);
                areaName.push($scope.gridModalAreaHelp.data[i].description);
                areaDate.push($scope.gridModalAreaHelp.data[i]);
            }
        }
        var allArea = {
            area : area,
            areaName : areaName,
            areaDate : areaDate
        }
        if(selectAreaModal.reportType!='oeeDetail'&&selectAreaModal.reportType!='Plato'){
        	if(area.length>10){
                $scope.addAlert("","您选择的数据已超过十条!");
                return;
            }
        }
        
        $uibModalInstance.close(allArea);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//拉线帮助
statementModule.controller('LineSelectMoreCtrl', ['$scope','$uibModalInstance','selectLineModal','statementService','$timeout',function ($scope, $uibModalInstance,selectLineModal,statementService,$timeout) {

    $scope.config = {
        confirmBtn : true,
        lineHelp:null,
        isFirstSelect:true
    };
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
			{
			    field: 'description', name: 'description', displayName: '描述', minWidth: 100,
			    enableCellEdit: false, enableCellEditOnFocus: false,
			    validators: { required: true },
			    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
			
			},
            {
                field: 'workCenter', name: 'workCenter', displayName: '拉线', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field:"enabled",name:"enabled",displayName:'是否已启用',width: 150,
                cellTooltip: function(row, col){ return "是否已启用"; },
                enableCellEdit: false, enableCellEditOnFocus: false,
                enableColumnMenu: false, enableColumnMenus: false,
                cellTemplate:'andon-ui-grid-rscg/statement-checkbox'
            }

        ],
        data:[],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    }
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.init = function(){
        $scope.gridModalAreaHelp.data = [];
        var area = selectLineModal.workArea?selectLineModal.workArea:'';
        var workLine = $scope.config.lineHelp?$scope.config.lineHelp:'';
        if(selectLineModal.withSiteStatus) {
            statementService.outputReportNewLineHelp(workLine, area,selectLineModal.sites).then(function (resultDatas) {
                if (resultDatas.response && resultDatas.response.length > 0) {
                    $scope.gridModalAreaHelp.data = resultDatas.response;
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                } else {
                    $scope.gridModalAreaHelp.data = [];
                    $scope.addAlert('', '暂无数据!');
                }
            }, function (resultDatas) { //TODO 检验失败
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
            });
        }else{
            statementService.outputReportLineHelp(workLine, area).then(function (resultDatas) {
                if (resultDatas.response && resultDatas.response.length > 0) {
                    $scope.gridModalAreaHelp.data = resultDatas.response;
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                } else {
                    $scope.gridModalAreaHelp.data = [];
                    $scope.addAlert('', '暂无数据!');
                }
            }, function (resultDatas) { //TODO 检验失败
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
            });
        }
    }
    $scope.init();
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.lineHelp = $scope.config.lineHelp?$scope.config.lineHelp.toUpperCase():'';

        if(keycode==13){
            $scope.init();
        }
    };
    $scope.ok = function () {
        var line = [];
        var lineName = [];
        var lineDatas = [];
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                line.push($scope.gridModalAreaHelp.data[i].workCenter);
                lineName.push($scope.gridModalAreaHelp.data[i].description);
                lineDatas.push($scope.gridModalAreaHelp.data[i]);
            }
        }
        var allLine = {
            line : line,
            lineName : lineName,
            lineDatas : lineDatas
        }
        if(selectLineModal.reportType!='oeeDetail'&&selectLineModal.reportType!='Plato'){
        	if(line.length>10){
                $scope.addAlert("","您选择的数据已超过十条!");
                return;
            }
        }
        $uibModalInstance.close(allLine);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//设备类型帮助    
statementModule.controller('deviceTypeMoreCtrl',['$scope','$uibModalInstance','statementService','plcService','selectTypeMoreDevice','$timeout', function ($scope, $uibModalInstance,statementService,plcService,selectTypeMoreDevice,$timeout) {

    $scope.config = {
        confirmBtn : true,
        deviceHelp:null,
        isFirstSelect:true

    };
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        /*enableFullRowSelection:true,*/
        columnDefs: [
            {
                field: 'description', name: 'description', displayName: '描述', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'resourceType', name: 'resourceType', displayName: '设备类型', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }
        ],
        data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    }
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.init=function(){
        if(selectTypeMoreDevice.withSiteStatus) {
            plcService.newDataResourceTypes(selectTypeMoreDevice.sites).then(function (resultDatas) {
                if (resultDatas.response && resultDatas.response.length > 0) {
                    $scope.gridModalAreaHelp.data = resultDatas.response;
                    $scope.statementConfig.localResourceType = resultDatas.response;
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }
            }, function (resultDatas) { //TODO 检验失败
                if (resultDatas.myHttpConfig.isSessionOut == true || resultDatas.myHttpConfig.isTimeout == true || resultDatas.myHttpConfig.status == 1 || resultDatas.myHttpConfig.status == -1) {
                } else {
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                }
            });
        }else{
            plcService.dataResourceTypes().then(function (resultDatas) {
                if (resultDatas.response && resultDatas.response.length > 0) {
                    $scope.gridModalAreaHelp.data = resultDatas.response;
                    $scope.statementConfig.localResourceType = resultDatas.response;
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }
            }, function (resultDatas) { //TODO 检验失败
                if (resultDatas.myHttpConfig.isSessionOut == true || resultDatas.myHttpConfig.isTimeout == true || resultDatas.myHttpConfig.status == 1 || resultDatas.myHttpConfig.status == -1) {
                } else {
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                }
            });
        }
    }
    $scope.init();
    $scope.filterResourceType=function(){
        var resourceType= $scope.config.deviceHelp?$scope.config.deviceHelp:'';
        if(resourceType==''){
        	$scope.gridModalAreaHelp.data=$scope.statementConfig.localResourceType;
        }else{
        	var newResourceType=resourceType.replace(/；/ig,';');
        	var resourceTypeArrary=newResourceType.split(";");
            $scope.config.gridFilterArea=[];
            $scope.gridModalAreaHelp.data=$scope.statementConfig.localResourceType;
            var filterRTArrary=[];
            for(var x=0;x<resourceTypeArrary.length;x++){
            	if(resourceTypeArrary[x]!=''){
            		filterRTArrary.push(resourceTypeArrary[x]);
            	}
            }if(filterRTArrary.length==0){
            	$scope.gridModalAreaHelp.data=$scope.statementConfig.localResourceType;
            }else{
            	for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
                	var desc=$scope.gridModalAreaHelp.data[i].description;
                	for(var j=0;j<filterRTArrary.length;j++){
                		if(desc.indexOf(filterRTArrary[j])!=-1){
                    		$scope.config.gridFilterArea.push($scope.gridModalAreaHelp.data[i]);
                    		break;
                    	}
                	}
                }
                $scope.gridModalAreaHelp.data=angular.copy($scope.config.gridFilterArea);
            }
        }
    }
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.deviceHelp = $scope.config.deviceHelp?$scope.config.deviceHelp.toUpperCase():'';

        if(keycode==13){
            $scope.filterResourceType();
        }
    };
    $scope.ok = function () {
        var allDatas = {};
        var selectdDatas = [];
        var resourceType = [];
        var resourceTypeDesc = [];
        
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                resourceType.push($scope.gridModalAreaHelp.data[i].resourceType);
                resourceTypeDesc.push($scope.gridModalAreaHelp.data[i].description);
                selectdDatas.push($scope.gridModalAreaHelp.data[i])
            }
        }
        if(selectTypeMoreDevice.reportType!='oeeDetail'&&selectTypeMoreDevice.reportType!='Plato'){
        	if(resourceType.length>50){
                $scope.addAlert("","您选择的数据已超过五十条!");
                return;
            }
        }
        
        allDatas.resourceType = resourceType;
        allDatas.resourceTypeDesc = resourceTypeDesc;
        allDatas.selectdDatas = selectdDatas;

        $uibModalInstance.close(allDatas);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//报警备注管理******设备类型列表
statementModule.controller('resourceTypeController',['$scope','$uibModalInstance','statementService','plcService','selectTypeMoreDevice','$timeout', function ($scope, $uibModalInstance,statementService,plcService,selectTypeMoreDevice,$timeout) {

    $scope.config = {
        confirmBtn : true,
        deviceHelp:null,
        localResourceType:[],
        isFirstSelect:true

    };
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
            {
                field: 'description', name: 'description', displayName: '描述', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'resourceType', name: 'resourceType', displayName: '设备类型', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }
        ],
        data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    }
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.init=function(){
        plcService.dataResourceTypes().then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                	$scope.gridModalAreaHelp.data = resultDatas.response;
                	$scope.config.localResourceType=resultDatas.response;
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }
            },function (resultDatas){ //TODO 检验失败
                if(resultDatas.myHttpConfig.isSessionOut == true || resultDatas.myHttpConfig.isTimeout == true || resultDatas.myHttpConfig.status == 1 || resultDatas.myHttpConfig.status == -1){
                }else{
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                }
            });
    }
    $scope.init();
    $scope.filterResourceType=function(){
        var resourceType= $scope.config.deviceHelp?$scope.config.deviceHelp:'';
        if(resourceType==''){
        	$scope.gridModalAreaHelp.data=$scope.config.localResourceType;
        }else{
        	var newResourceType=resourceType.replace(/；/ig,';');
        	var resourceTypeArrary=newResourceType.split(";");
            $scope.config.gridFilterArea=[];
            $scope.gridModalAreaHelp.data=$scope.config.localResourceType;
            var filterRTArrary=[];
            for(var x=0;x<resourceTypeArrary.length;x++){
            	if(resourceTypeArrary[x]!=''){
            		filterRTArrary.push(resourceTypeArrary[x]);
            	}
            }if(filterRTArrary.length==0){
            	$scope.gridModalAreaHelp.data=$scope.config.localResourceType;
            }else{
            	for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
                	var desc=$scope.gridModalAreaHelp.data[i].description;
                	for(var j=0;j<filterRTArrary.length;j++){
                		if(desc.indexOf(filterRTArrary[j])!=-1){
                    		$scope.config.gridFilterArea.push($scope.gridModalAreaHelp.data[i]);
                    		break;
                    	}
                	}
                }
                $scope.gridModalAreaHelp.data=angular.copy($scope.config.gridFilterArea);
            }
        }
    }
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.deviceHelp = $scope.config.deviceHelp?$scope.config.deviceHelp.toUpperCase():'';

        if(keycode==13){
            $scope.filterResourceType();
        }
    };
    $scope.ok = function () {
        var allDatas = {};
        var selectdDatas = [];
        var resourceType = [];
        var resourceTypeDesc = [];
        
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                resourceType.push($scope.gridModalAreaHelp.data[i].resourceType);
                resourceTypeDesc.push($scope.gridModalAreaHelp.data[i].description);
                selectdDatas.push($scope.gridModalAreaHelp.data[i])
            }
        }
    	 if(selectTypeMoreDevice.type=='edit'){
         	if(resourceType.length>1){
                 $scope.addAlert("","只能选择一条记录");
                 return;
             }
         }
        
        allDatas.resourceType = resourceType;
        allDatas.resourceTypeDesc = resourceTypeDesc;
        allDatas.selectdDatas = selectdDatas;

        $uibModalInstance.close(allDatas);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//设备帮助
statementModule.controller('deviceMoreCtrl',['$scope','$uibModalInstance','statementService','yieldMaintainService','selectMoreDevice','$timeout', function ($scope, $uibModalInstance,statementService,yieldMaintainService,selectMoreDevice,$timeout) {

    $scope.config = {
        confirmBtn : true,
        deviceHelp:null,
        isFirstSelect:true

    };
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
             {
                 field: 'description', name: 'description', displayName: '描述', minWidth: 100,
                 enableCellEdit: false, enableCellEditOnFocus: false,
                 validators: { required: true },
                 cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

             },
			{
			    field: 'resrce', name: 'resrce', displayName: '设备编码', minWidth: 100,
			    enableCellEdit: false, enableCellEditOnFocus: false,
			    validators: { required: true },
			    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
			
			},
            {
                field: 'asset', name: 'asset', displayName: '资产号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'subAsset', name: 'subAsset', displayName: '子资产号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }
        ],
        data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    }
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.init=function(){
    	var workCenter=selectMoreDevice.workArea;
    	var line=selectMoreDevice.workLine;
    	var resourceType=selectMoreDevice.deviceCode;
        yieldMaintainService.requestDataResourceCodesByLine(workCenter, line, resourceType).then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                	$scope.gridModalAreaHelp.data=resultDatas.response;
                	$scope.statementConfig.localResource=resultDatas.response;
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
    }
    $scope.init();
    $scope.filterResource=function(){
    	var resource= $scope.config.resourceHelp?$scope.config.resourceHelp:'';
    	if(resource==''){
    		$scope.gridModalAreaHelp.data=$scope.statementConfig.localResource;
    	}else{
    		var newRes=resource.replace(/；/ig,';');
            var resourceArrary=newRes.split(";");
            var filterResourceArray=[];
            $scope.config.gridFilterArea=[];
            $scope.gridModalAreaHelp.data=$scope.statementConfig.localResource;
            for(var x=0;x<resourceArrary.length;x++){
            	if(resourceArrary[x]!=''){
            		filterResourceArray.push(resourceArrary[x]);
            	}
            }if(filterResourceArray.length==0){
            	$scope.gridModalAreaHelp.data=$scope.statementConfig.localResource;
            }else{
            	for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
                	var desc=$scope.gridModalAreaHelp.data[i].description;
                	for(var j=0;j<filterResourceArray.length;j++){
                		if(desc.indexOf(filterResourceArray[j])!=-1){
                    		$scope.config.gridFilterArea.push($scope.gridModalAreaHelp.data[i]);
                    		break;
                    	}
                	}
                }
                $scope.gridModalAreaHelp.data=angular.copy($scope.config.gridFilterArea);
            }
    	}
    }
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.resourceHelp = $scope.config.resourceHelp?$scope.config.resourceHelp.toUpperCase():'';
        if(keycode==13){
            $scope.filterResource();
        }
    };
    $scope.ok = function () {
        var allDatas = {};
        var selectdDatas = [];
        var area = [];

        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                area.push($scope.gridModalAreaHelp.data[i].resrce);
                selectdDatas.push($scope.gridModalAreaHelp.data[i])
            }
        }
        if(selectMoreDevice.reportType!='oeeDetail'&&selectMoreDevice.reportType!='Plato'){
        	if(area.length>50){
                $scope.addAlert("","您选择的数据已超过五十条!");
                return;
            }
        }
        allDatas.area = area;
        allDatas.selectdDatas = selectdDatas;

        $uibModalInstance.close(allDatas);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//设备帮助 单选
statementModule.controller('deviceOneCtrl',['$scope','$uibModalInstance','statementService','yieldMaintainService','selectOneDevice', 'UtilsService','$timeout',function ($scope, $uibModalInstance,statementService,yieldMaintainService,selectOneDevice,UtilsService,$timeout) {

    $scope.config = {
        confirmBtn : true,
        deviceHelp:null,
        plcChooseDeviceStatus:selectOneDevice.plcChooseDeviceStatus,
        isFirstSelect:true
    };
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
            {
                field: 'description', name: 'description', displayName: '描述', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'resrce', name: 'resrce', displayName: '设备编码', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'asset', name: 'asset', displayName: '资产号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'subAsset', name: 'subAsset', displayName: '子资产号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }
        ],
        data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            UtilsService.log("执行单选");
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            UtilsService.log("执行多选");
            __selectAny();
        });
    }
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.init=function(){
        yieldMaintainService.requestDataResourceCodesByLine("", "", "",selectOneDevice.plcChooseDeviceStatus).then(function (resultDatas){
            if(resultDatas.response && resultDatas.response.length > 0){
                $scope.gridModalAreaHelp.data=resultDatas.response;
                $scope.statementConfig.localResource=resultDatas.response;
                if($scope.config.isFirstSelect)
                {
                    $timeout(function(){
                        $scope.config.isFirstSelect=false;
                        /* To hide the blank gap when use selecting and grouping */
                        $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                        $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                    },0);
                }
                return;
            }
        },function (resultDatas){ //TODO 检验失败
            $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
        });
    }
    $scope.init();
    $scope.filterResource=function(){
        var resource= $scope.config.resourceHelp?$scope.config.resourceHelp:'';
        if(resource==''){
            $scope.gridModalAreaHelp.data=$scope.statementConfig.localResource;
        }else{
            var newRes=resource.replace(/；/ig,';');
            var resourceArrary=newRes.split(";");
            var filterResourceArray=[];
            $scope.config.gridFilterArea=[];
            $scope.gridModalAreaHelp.data=$scope.statementConfig.localResource;
            for(var x=0;x<resourceArrary.length;x++){
                if(resourceArrary[x]!=''){
                    filterResourceArray.push(resourceArrary[x]);
                }
            }if(filterResourceArray.length==0){
                $scope.gridModalAreaHelp.data=$scope.statementConfig.localResource;
            }else{
                for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
                    var desc=$scope.gridModalAreaHelp.data[i].description;
                    for(var j=0;j<filterResourceArray.length;j++){
                        if(desc.indexOf(filterResourceArray[j])!=-1){
                            $scope.config.gridFilterArea.push($scope.gridModalAreaHelp.data[i]);
                            break;
                        }
                    }
                }
                $scope.gridModalAreaHelp.data=angular.copy($scope.config.gridFilterArea);
            }
        }
    }
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.resourceHelp = $scope.config.resourceHelp?$scope.config.resourceHelp.toUpperCase():'';
        if(keycode==13){
            $scope.filterResource();
        }
    };
    $scope.ok = function () {
        var allDatas = {};
        var selectdDatas = [];
        var area = [];

        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                area.push($scope.gridModalAreaHelp.data[i].resrce);
                selectdDatas.push($scope.gridModalAreaHelp.data[i])
            }
        }
        if(area.length>1){
            $scope.addAlert("","设备只能单选,您选择的数据已超过一条!");
            return;
         }
        //if(selectOneDevice.reportType!='oeeDetail'&&selectOneDevice.reportType!='Plato'){
        //    if(area.length>50){
        //        $scope.addAlert("","您选择的数据已超过五十条!");
        //        return;
        //    }
        //}
        allDatas.area = area;
        allDatas.selectdDatas = selectdDatas;

        $uibModalInstance.close(allDatas);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//设备帮助
statementModule.controller('noReportDeviceMoreCtrl',['$scope','$uibModalInstance','statementService','yieldMaintainService','selectMoreDevice','$timeout', function ($scope, $uibModalInstance,statementService,yieldMaintainService,selectMoreDevice,$timeout) {

    $scope.config = {
        confirmBtn : true,
        deviceHelp:null,
        isFirstSelect:true

    };
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
             {
                 field: 'description', name: 'description', displayName: '描述', minWidth: 100,
                 enableCellEdit: false, enableCellEditOnFocus: false,
                 validators: { required: true },
                 cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

             },
			{
			    field: 'resrce', name: 'resrce', displayName: '设备编码', minWidth: 100,
			    enableCellEdit: false, enableCellEditOnFocus: false,
			    validators: { required: true },
			    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
			
			},
            {
                field: 'asset', name: 'asset', displayName: '资产号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'subAsset', name: 'subAsset', displayName: '子资产号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }
        ],
        data: angular.copy(selectMoreDevice.deviceCode),
        onRegisterApi: __onRegisterApi
    };

    //去除左下角空白区域
    if($scope.config.isFirstSelect)
    {
        $timeout(function(){
            $scope.config.isFirstSelect=false;
            /* To hide the blank gap when use selecting and grouping */
            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
        },0);
    }

    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    }
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.init=function(){
    	var workCenter=selectMoreDevice.workArea;
    	var line=selectMoreDevice.workLine;
    	var resourceType=selectMoreDevice.deviceCode;
        yieldMaintainService.requestDataResourceCodesByLine(workCenter, line, resourceType).then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                	$scope.gridModalAreaHelp.data=resultDatas.response;
                	$scope.statementConfig.localResource=resultDatas.response;
                    //$timeout(function(){
                    //    /* To hide the blank gap when use selecting and grouping */
                    //    $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                    //    $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                    //},0);
                    return;
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
    }
    //$scope.init();
    $scope.filterResource=function(){
    	var resource= $scope.config.resourceHelp?$scope.config.resourceHelp:'';
    	if(resource==''){
    		$scope.gridModalAreaHelp.data=$scope.statementConfig.localResource;
    	}else{
    		var newRes=resource.replace(/；/ig,';');
            var resourceArrary=newRes.split(";");
            var filterResourceArray=[];
            $scope.config.gridFilterArea=[];
            $scope.gridModalAreaHelp.data=$scope.statementConfig.localResource;
            for(var x=0;x<resourceArrary.length;x++){
            	if(resourceArrary[x]!=''){
            		filterResourceArray.push(resourceArrary[x]);
            	}
            }if(filterResourceArray.length==0){
            	$scope.gridModalAreaHelp.data=$scope.statementConfig.localResource;
            }else{
            	for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
                	var desc=$scope.gridModalAreaHelp.data[i].description;
                	for(var j=0;j<filterResourceArray.length;j++){
                		if(desc.indexOf(filterResourceArray[j])!=-1){
                    		$scope.config.gridFilterArea.push($scope.gridModalAreaHelp.data[i]);
                    		break;
                    	}
                	}
                }
                $scope.gridModalAreaHelp.data=angular.copy($scope.config.gridFilterArea);
            }
    	}
    }
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.resourceHelp = $scope.config.resourceHelp?$scope.config.resourceHelp.toUpperCase():'';
        if(keycode==13){
            $scope.filterResource();
        }
    };
    $scope.ok = function () {
        var allDatas = {};
        var selectdDatas = [];
        var area = [];

        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                area.push($scope.gridModalAreaHelp.data[i].resrce);
                selectdDatas.push($scope.gridModalAreaHelp.data[i])
            }
        }
    	if(area.length>50){
            $scope.addAlert("","您选择的数据已超过五十条!");
            return;
        }
        allDatas.area = area;
        allDatas.selectdDatas = selectdDatas;
        $uibModalInstance.close(allDatas);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
// 报警备注
statementModule.controller('chooseAlarmRemarkMoreCtrl',['$scope','$uibModalInstance','statementService','plcService','selectAlarmRemarkModal','UtilsService', function ($scope, $uibModalInstance,statementService,plcService,selectAlarmRemarkModal,UtilsService) {

    $scope.config = {
        confirmBtn : true,
        alarmRemark:null,
        gridApi:null,
        alarmRemarkDescription:""

    };
    $scope.gridModalAlarmRemark = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        /*enableFullRowSelection:true,*/
        columnDefs: [
            {
                field: 'description', name: 'description', displayName: '描述', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'commentCode', name: 'commentCode', displayName: '备注代码', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }
        ],
        data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    }
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAlarmRemark.data.length;i++){
            if($scope.gridModalAlarmRemark.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };

    $scope.queryData = function(){
        var alarmRemarkDescription=$scope.config.alarmRemarkDescription.replace(/；/ig,';');
        UtilsService.log("描述：alarmRemarkDescription"+alarmRemarkDescription);
        $scope.gridModalAlarmRemark.data = [];
        plcService.dataAlarmRemarkWithDescription(alarmRemarkDescription).then(function (resultData){
            if(resultData.response && resultData.response.length > 0){
                $scope.gridModalAlarmRemark.data = resultData.response;
                UtilsService.log(angular.toJson(resultData.response,true));
                return;
            }else{
                $scope.gridModalAlarmRemark.data = [];
                $scope.addAlert('', '暂无数据!');
            }
        },function (resultData){ //TODO 检验失败
            $scope.addAlert(resultData.myHttpConfig.statusDesc);
        });
    }

    //初始化数据
    $scope.queryData();

    $scope.enterKey = function(e){
        UtilsService.log("success enterKey");
        var keyCode = window.event ? e.keyCode : e.which;
        $scope.config.lineHelp = $scope.config.lineHelp?$scope.config.lineHelp.toUpperCase():'';

        if(keyCode==13){
            $scope.queryData();
        }
    };
    $scope.ok = function () {
        var allDatas = {};
        var selectdDatas = [];
        var commentCode = [];
        var commentDesc = [];

        for(var i=0;i<$scope.gridModalAlarmRemark.data.length;i++){
            if($scope.gridModalAlarmRemark.data[i].isSelected == true){
                commentCode.push($scope.gridModalAlarmRemark.data[i].commentCode);
                commentDesc.push($scope.gridModalAlarmRemark.data[i].description);
                selectdDatas.push($scope.gridModalAlarmRemark.data[i])
            }
        }
        if(selectAlarmRemarkModal.reportType!='oeeDetail'&&selectAlarmRemarkModal.reportType!='Plato'){
            if(commentCode.length>50){
                $scope.addAlert("","您选择的数据已超过五十条!");
                return;
            }
        }

        allDatas.commentCode = commentCode;
        allDatas.commentDesc = commentDesc;
        allDatas.selectdDatas = selectdDatas;

        $uibModalInstance.close(allDatas);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

// 设备不同颜色灯详情
statementModule.controller('deviceLampDetailModalCtrl',['$scope','$uibModalInstance','statementService','plcService','lampDetailData','UtilsService','HttpAppService', function ($scope, $uibModalInstance,statementService,plcService,lampDetailData,UtilsService,HttpAppService) {

    $scope.config = {
        confirmBtn : true,
        alarmRemark:null,
        gridApi:null,
        alarmRemarkDescription:"",
        lampColor:lampDetailData.lampColor,
        dimensionName:lampDetailData.dimensionName
    };

    if(lampDetailData.colorCode == 1)
    {
        $scope.config.dimensionName+="红灯设备明细";
    }
    else if(lampDetailData.colorCode == 2)
    {
        $scope.config.dimensionName+="黄灯设备明细";
    }
    else{
        $scope.config.dimensionName+="绿灯设备明细";
    }
    //红灯黄灯
    $scope.gridModalRYLamp = {
        enablePagination: false,
        totalItems: 0,
        useExternalPagination: false,
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        /*enableFullRowSelection:true,*/
        columnDefs: [
            {
                field: 'workCenter', name: 'workCenter', displayName: '生产区域', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'line', name: 'line', displayName: '拉线', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'resourceType', name: 'resourceType', displayName: '设备类型', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'resource', name: 'resource', displayName: '设备编码', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'elapsedTime', name: 'elapsedTime', displayName: '持续停机时长', minWidth: 80,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'reasonCode', name: 'reasonCode', displayName: '人工点选停机原因', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'productionCount', name: 'productionCount', displayName: '累计产量', minWidth: 70,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }

        ],
        data: [],
        onRegisterApi: __onRegisterRYApi
    };

    $scope.gridModalGLamp = {
        enablePagination: false,
        totalItems: 0,
        useExternalPagination: false,
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        /*enableFullRowSelection:true,*/
        columnDefs: [
            {
                field: 'workCenter', name: 'workCenter', displayName: '生产区域', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'line', name: 'line', displayName: '拉线', minWidth: 200,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'resourceType', name: 'resourceType', displayName: '设备类型', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'resource', name: 'resource', displayName: '设备编码', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'elapsedTime', name: 'elapsedTime', displayName: '生产持续时长(min)', minWidth: 80,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'productionType', name: 'productionType', displayName: '当前生产的品种', minWidth: 150,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'productionCount', name: 'productionCount', displayName: '累计产量', minWidth: 70,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }

        ],
        data: [],
        onRegisterApi: __onRegisterGApi
    };

    function __onRegisterRYApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
    }

    function __onRegisterGApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
    }

    function __selectAny(){
        for(var i=0;i<$scope.gridModalFactory.data.length;i++){
            if($scope.gridModalFactory.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };

    $scope.initData = function(){
        var params = {
            sites : lampDetailData.sites,
            workCenter : lampDetailData.workCenter,
            lineArea : lampDetailData.lineArea,
            resourceType : lampDetailData.resourceType,
            color : lampDetailData.colorCode,
            dimensionCode:lampDetailData.dimensionCode,
            dimension:lampDetailData.dimension
        };
        UtilsService.log("dimensionCode:"+lampDetailData.dimensionCode);
        statementService.dataGlobalListByColor(params).then(function (resultData){
            if(resultData.response && resultData.response.length > 0){
                if(lampDetailData.colorCode == 3){
                    //绿灯
                    $scope.gridModalGLamp.data = resultData.response;
                }else{
                    //红或黄灯
                    $scope.gridModalRYLamp.data = resultData.response;

                    for(var i=0;i<$scope.gridModalRYLamp.data.length;i++)
                    {
                        if(!$scope.gridModalRYLamp.data[i].reasonCode){
                            $scope.gridModalRYLamp.data[i].reasonCode="待PRD处理";
                        }
                    }
                }
                UtilsService.log(angular.toJson(resultData.response,true));
                return;
            }else{
                $scope.gridModalGLamp.data = [];
                $scope.addAlert('', '暂无数据!');
            }
        },function (resultData){ //TODO 检验失败
            $scope.addAlert(resultData.myHttpConfig.statusDesc);
        });
    }

    //初始化数据
    $scope.initData();

    $scope.ok = function () {
        var allDatas = {};
        var selectdDatas = [];
        var commentCode = [];
        var commentDesc = [];

        for(var i=0;i<$scope.gridModalFactory.data.length;i++){
            if($scope.gridModalFactory.data[i].isSelected == true){
                commentCode.push($scope.gridModalFactory.data[i].commentCode);
                commentDesc.push($scope.gridModalFactory.data[i].description);
                selectdDatas.push($scope.gridModalFactory.data[i])
            }
        }

        if(commentCode.length>50){
            $scope.addAlert("","您选择的数据已超过五十条!");
            return;
        }


        allDatas.commentCode = commentCode;
        allDatas.commentDesc = commentDesc;
        allDatas.selectdDatas = selectdDatas;

        $uibModalInstance.close(allDatas);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

// 选择工厂
statementModule.controller('factoryChooseCtrl',['$scope','$uibModalInstance','statementService','plcService','selectFactoryModal','UtilsService','$timeout','homeService', function ($scope, $uibModalInstance,statementService,plcService,selectFactoryModal,UtilsService,$timeout,homeService) {

    $scope.config = {
        confirmBtn : true,
        alarmRemark:null,
        gridApi:null,
        alarmRemarkDescription:"",
        factoryDesc:"",
        isFirstSelect:true

    };
    $scope.gridModalFactory = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        /*enableFullRowSelection:true,*/
        columnDefs: [
            {
                field: 'description', name: 'description', displayName: '工厂描述', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'site', name: 'site', displayName: '工厂代码', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }
        ],
        data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    }
    function __selectAny(){
        for(var i=0;i<$scope.gridModalFactory.data.length;i++){
            if($scope.gridModalFactory.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };

    $scope.queryData = function(){
        var alarmRemarkDescription=$scope.config.alarmRemarkDescription.replace(/；/ig,';');
        UtilsService.log("描述：alarmRemarkDescription"+alarmRemarkDescription);
        $scope.gridModalFactory.data = [];
        homeService.usersitelist($scope.bodyConfig.userCode).then(function (resultData){
            if(resultData.response && resultData.response.length > 0){
                //$scope.gridModalFactory.data = resultData.response;
                $scope.gridModalFactory.data=[{"handle":"SiteBO:2001","site":"2001","description":"ND-CATL 电芯工厂","createdDateTime":"Dec 19, 2016 7:43:58 PM","modifiedDateTime":"Dec 19, 2016 8:19:03 PM","modifiedUser":"00065870"}];
                $scope.statementConfig.localFactory=[{"handle":"SiteBO:2001","site":"2001","description":"ND-CATL 电芯工厂","createdDateTime":"Dec 19, 2016 7:43:58 PM","modifiedDateTime":"Dec 19, 2016 8:19:03 PM","modifiedUser":"00065870"}];
                if($scope.config.isFirstSelect)
                {
                    $timeout(function(){
                        $scope.config.isFirstSelect=false;
                        /* To hide the blank gap when use selecting and grouping */
                        $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                        $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                    },0);
                }
                UtilsService.log(angular.toJson(resultData.response,true));
                return;
            }else{
                $scope.gridModalFactory.data = [];
                $scope.addAlert('', '暂无数据!');
            }
        },function (resultData){ //TODO 检验失败
            $scope.addAlert(resultData.myHttpConfig.statusDesc);
        });
    }

    //初始化数据
    $scope.queryData();

    //点击差选按钮调用，直接在本地过滤
    $scope.filterResource=function(){
        if($scope.config.factoryDesc)
        {
            var newRes=$scope.config.factoryDesc.replace(/；/ig,';');
            var resourceArrary=newRes.split(";");
            var filterResourceArray=[];
            $scope.config.gridFilterArea=[];
            $scope.gridModalFactory.data=$scope.statementConfig.localFactory;
            for(var x=0;x<resourceArrary.length;x++){
                if(resourceArrary[x]!=''){
                    filterResourceArray.push(resourceArrary[x]);
                }
            }
            if(filterResourceArray.length==0){
                $scope.gridModalFactory.data=$scope.statementConfig.localFactory;
            }else {
                for (var i = 0; i < $scope.gridModalFactory.data.length; i++) {
                    var desc = $scope.gridModalFactory.data[i].description;
                    for (var j = 0; j < filterResourceArray.length; j++) {
                        if (desc.indexOf(filterResourceArray[j]) != -1) {
                            $scope.config.gridFilterArea.push($scope.gridModalFactory.data[i]);
                            break;
                        }
                    }
                }
            $scope.gridModalFactory.data = angular.copy($scope.config.gridFilterArea);
           }
        }else{
            $scope.gridModalFactory.data=$scope.statementConfig.localFactory;
        }
    };

    $scope.enterKey = function(e){
        UtilsService.log("success enterKey");
        var keyCode = window.event ? e.keyCode : e.which;
        $scope.config.lineHelp = $scope.config.lineHelp?$scope.config.lineHelp.toUpperCase():'';

        if(keyCode==13){
            $scope.queryData();
        }
    };
    $scope.ok = function () {
        var allDatas = {};
        var selectdDatas = [];
        var siteList = [];
        var descriptionList = [];

        for(var i=0;i<$scope.gridModalFactory.data.length;i++){
            if($scope.gridModalFactory.data[i].isSelected == true){
                siteList.push($scope.gridModalFactory.data[i].site);
                descriptionList.push($scope.gridModalFactory.data[i].description);
                selectdDatas.push($scope.gridModalFactory.data[i]);
            }
        }

        if(siteList.length>50){
            $scope.addAlert("","您选择的数据已超过五十条!");
            return;
        }


        allDatas.siteList = siteList;
        allDatas.descriptionList = descriptionList;
        allDatas.selectdDatas = selectdDatas;

        $uibModalInstance.close(allDatas);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//数据说明
statementModule.controller('DataDescriptionCtrl',['$scope','$uibModalInstance','statementService','yieldMaintainService','selectDescriptionData','$timeout', function ($scope, $uibModalInstance,statementService,yieldMaintainService,selectDescriptionData,$timeout) {

    $scope.config = {
        confirmBtn : true,
        deviceHelp:null,
        isFirstSelect:true

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
