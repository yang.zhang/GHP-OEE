statementModule.controller('OEEDetailCtrl',['$scope', '$http',
    'HttpAppService', '$timeout','UtilsService',
    'uiGridConstants','$uibModal','plcService','statementService',
    function ($scope, $http,
              HttpAppService, $timeout,UtilsService,
              uiGridConstants,$uibModal,plcService,statementService) {

        $scope.config = {
            isCollapsed : false,
            gridApi: null,
            currentThemeGrid:null,
            btnDisabledDownload:true,
            alertCount:null,
            itemCount:null,
            reasonCodeCount:null,
            outputCount:null,
            rtReasonCodeCount:null,
            totalHalt:null,
            totalTime:null,
            showSelectLine : true,
            deviceCode : true,
            showSelectArea : true,
            deviceType : true
        };
        //生产区域帮助
        $scope.toAreaHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/areaSelectMoreModal.html',
                controller: 'areaSelectMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectAreaModal: function () {
                        return {
                        	workArea : $scope.statementConfig.workCenters,
                        	reportType:'oeeDetail'
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentWorkCenter = selectedItem.area.join(",");
                $scope.statementConfig.currentWorkCenterDesc = selectedItem.areaName.join(",");
                $scope.config.showSelectArea = false;
                $scope.deleteAll('line');
                $scope.deleteAll('deviceCode');
            }, function () {
            });
        };
        //拉线多选帮助
        $scope.toWireLineHelp = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/lineSelectModal.html',
                controller: 'LineSelectMoreCtrl',
                size: 'lg',
                backdrop: 'false',
                scope: $scope,
                openedClass: 'flex-center-parent',
                resolve: {
                    selectLineModal: function () {
                        return {
                        	workArea: $scope.statementConfig.currentWorkCenter,
                        	workLine: $scope.statementConfig.lines,
                        	reportType:'oeeDetail'
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentLine = selectedItem.line;
                $scope.statementConfig.currentLineDesc = selectedItem.lineName;
                if($scope.statementConfig.currentLine.length >0){
                    $scope.config.showSelectLine = false;
                }
                $scope.deleteAll('deviceCode');
            }, function () {
            });
        };
        //设备类型帮助
        $scope.toDeviceTypeHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/deviceTypeSelectMoreModal.html',
                controller: 'deviceTypeMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectTypeMoreDevice: function () {
                        return {
                        	deviceCode : $scope.statementConfig.deviceTypes,
                        	reportType:'oeeDetail'
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentDeviceType = selectedItem.resourceType;
                $scope.statementConfig.currentDeviceTypeDesc = selectedItem.resourceTypeDesc;
                if($scope.statementConfig.currentDeviceType.length >0){
                    $scope.config.deviceType = false;
                }
                $scope.deleteAll('deviceCode');
            }, function () {
            });
        };	
        //设备帮助
        $scope.toDeviceHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/modalDeviceHelp.html',
                controller: 'deviceMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectMoreDevice: function () {
                        return {
                        	workArea: $scope.statementConfig.currentWorkCenter,
                        	workLine: $scope.statementConfig.currentLine,
                        	deviceCode : $scope.statementConfig.currentDeviceType,
                        	reportType:'oeeDetail'
                        	};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentDeviceNum = selectedItem.area;
                if($scope.statementConfig.currentDeviceNum.length >0){
                    $scope.config.deviceCode = false;
                }
            }, function () {
            });
        };
        $scope.deleteAll = function(item){
            if(item == 'area'){
                $scope.statementConfig.currentWorkCenter = [];
                $scope.statementConfig.currentWorkCenterDesc = [];
                $scope.statementConfig.currentLine = [];
                $scope.statementConfig.currentLineDesc = [];
                $scope.statementConfig.deviceNums = [];
                $scope.statementConfig.currentDeviceNum = null;
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.showSelectArea = true;
                $scope.config.showSelectLine = true;
                $scope.config.deviceCode = true;
            }
            if(item == 'line'){
                $scope.statementConfig.currentLine = [];
                $scope.statementConfig.currentLineDesc = [];
                $scope.statementConfig.deviceNums = [];
                $scope.statementConfig.currentDeviceNum = null;
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.showSelectLine = true;
                $scope.config.deviceCode = true;
            }
            if(item == 'deviceType'){
                $scope.statementConfig.currentDeviceType = [];
                $scope.statementConfig.currentDeviceTypeDesc = [];
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.deviceType = true;
                $scope.config.deviceCode = true;
            }
            if(item == 'deviceCode'){
                $scope.statementConfig.currentDeviceNum = [];
                $scope.config.deviceCode = true;
            }
        }
        $scope.init = function(){
            $scope.statementConfig.currentWorkCenter = [];
            $scope.statementConfig.currentWorkCenterDesc = [];
            $scope.statementConfig.currentLine = [];
            $scope.statementConfig.currentLineDesc = [];
            $scope.statementConfig.currentDeviceNum = null;
            $scope.statementConfig.multipleCurrentDeviceNum = [];
            $scope.statementConfig.currentDeviceType = [];
            $scope.statementConfig.currentDeviceTypeDesc = [];
            $scope.statementConfig.startDate = UtilsService.OEEStartFommateDateTimeShow(new Date());
            $scope.statementConfig.endDate = UtilsService.OEEEndFommateDateTimeShow(new Date());
            $scope.statementConfig.shiftsChange = [{shift : '',description : '请选择',descriptionNew : '请选择'},{shift : 'M',description : '早班',descriptionNew : '早班 代码:M'},
                {shift : 'E',description : '晚班',descriptionNew : '晚班 代码:E'}];
        };

        $scope.init();
        $scope.downLoad = function(){
            __downLoad();
        };

        function __arrayToString(array){
            var string = [];
            for(var i=0;i<array.length;i++){
                string[i] = array[i].workCenter;
            }
            return string.toString();
        };


        function __downLoad(){
        	if(($scope.statementConfig.currentWorkCenter == ''||$scope.statementConfig.currentWorkCenter ==null)
            		&&($scope.statementConfig.currentLine== ''||$scope.statementConfig.currentLine==null)
            		&&($scope.statementConfig.currentDeviceType== ''||$scope.statementConfig.currentDeviceType== null)
            		&&($scope.statementConfig.currentDeviceNum==''||$scope.statementConfig.currentDeviceNum==null)
            		&&($scope.statementConfig.currentShift==null||$scope.statementConfig.currentShift.description=='请选择')){
            		$scope.addAlert('danger','条件不可以为空');
                    return;
            	}
            var params = {
                workCenter: $scope.statementConfig.currentWorkCenter,
                line:$scope.statementConfig.currentLine,
                startDate: UtilsService.serverFommateDate(new Date($scope.statementConfig.startDate)),
                endDate: UtilsService.serverFommateDate(new Date($scope.statementConfig.endDate)),
            };
            if(new Date($scope.statementConfig.startDate)>new Date($scope.statementConfig.endDate)){
                $scope.addAlert('danger','开始时间不能大于结束时间');
                return;
            }
            if((new Date($scope.statementConfig.endDate)).getTime()-(new Date($scope.statementConfig.startDate)).getTime()>=92*24*60*60*1000){
                $scope.addAlert('danger','开始和结束时间跨度不超过92天');
                $scope.hideBodyModel();
                return;
            }
            //设备类型
            if($scope.statementConfig.currentDeviceType){
                params.resourceType = $scope.statementConfig.currentDeviceType;
            }
            //设备编码
            if($scope.statementConfig.currentDeviceNum){
                params.resrce = $scope.statementConfig.currentDeviceNum;
            }
            //班次
            if($scope.statementConfig.currentShift){
                params.shift = $scope.statementConfig.currentShift.shift;
            }
            $scope.showBodyModel("正在导出数据,请稍后...");
            var url = statementService.OEEDetailExport(params);
            url = url.replace(/undefined/g,"").replace(/null/g,"");
            url = HttpAppService.handleCommenUrl(url);
            $http({
                url: url,
                method: "GET",
                headers: {
                    'Content-type': 'application/json'
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                var objectUrl = URL.createObjectURL(blob);
                window.location.href=objectUrl;
                $scope.hideBodyModel();
            }).error(function (data, status, headers, config) {
            	$scope.hideBodyModel();
            	if(status == 401 || status == 403){
            		HttpAppService.reloadPage("登录已过期,请重新登录!");
        		}else if(status == 413){
        			HttpAppService.reloadPage("当前用户权限已被更改,请重新登录!");
	            }else{
	            	var msg = HttpAppService.getResponseStatusDesc(status);
	            	$scope.addAlert("danger", msg);
	            }
            });
            countOEED(params);
        };
        function countOEED(params){
            statementService.countOEED(params).then(function (resultDatas){
            	$scope.statementConfig.oeetotal="结果已导出到本地，总记录数"+resultDatas.response;
            	console.info(resultDatas.response);
            },function (resultDatas){
            	$scope.hideBodyModel();
            	$scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });
        };
    }]);