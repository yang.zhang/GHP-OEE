statementModule.controller('ppmExportStatementCtrl',['$scope', '$http',
    'HttpAppService', '$timeout',							
    'uiGridConstants','$uibModal','$echarts','$interval','statementService','UtilsService',
    function ($scope, $http,
              HttpAppService, $timeout,
              uiGridConstants,$uibModal,$echarts,$interval,statementService,UtilsService) {
        $scope.config ={
            outputStatement : 'PPM数据导出报表',
            workArea : "",
            workLine : "",
            workAreaName : "",
            workLineName : "",
            deviceCode : "",
            deviceCodeArray:[],
            model : "",
            itemCode : "",
            startDate : "",
            endDate : "",
            shift : "",
            type : 'DEV',
            selectType : true,
            
            showResource : true,
            showSelectArea : true,
            showSelectLine : true,
            showDevice : true,
            showModel : true,
            showItem : true,
            
            


            queryGridAreaShow : false,
            queryGridLineShow : false,
            queryGridDeviceShow : false,
            queryGridAreaCodeShow : false,
            addInfosShow : false,

            downloadBtn : true,

            //报表
            outPutStatementInstance : null,
            itemCodeSelected : [],
            itemCodeSelectedCopy : [],
            currentItemCode : null,
            dataNum : 0
        };
        $scope.init = function(){
            $scope.config.startDate = UtilsService.OEEStartFommateDateTimeShow(new Date());
            $scope.config.endDate = UtilsService.OEEEndFommateDateTimeShow(new Date());
            $scope.config.shiftsChange = [{shift : '',description : '请选择',descriptionNew : '请选择'},{shift : 'M',description : '早班',descriptionNew : '早班 代码:M'},
                {shift : 'E',description : '晚班',descriptionNew : '晚班 代码:E'}];
        };
        $scope.init();
        //导出数据
        $scope.exportExcel = function(){
            var work_area = $scope.config.workArea;
            var line_area = $scope.config.workLine;
            var resrce = $scope.config.deviceCode;
            var model = $scope.config.model;
            var item = $scope.config.itemCode;
            var time_type = $scope.config.dateType;
            var by_condition_type = $scope.config.type;
            var shift = $scope.config.shift;
            if($scope.config.currentShift!=null){
            	shift = $scope.config.currentShift.shift;
            }
            if((work_area== '' || work_area == null)
            		&&(line_area== '' || line_area == null)
            		&&(resrce== '' || resrce == null)
            		&&(model== '' || model == null)
            		&&(item== '' || item == null)
            		&&(shift== '' || shift == null)){
            	$scope.addAlert('danger','条件不可以为空');
                return;
            }
            if(model == '' || model == null){
                model = [];
            }else{
                model = model.split(',');
            }
            if(item == '' || item == null){
                item = [];
            }else{
                item = item.split(',');
            }
            if(resrce == '' || resrce == null){
                resrce =[];
            }else{
                resrce = resrce.split(',');
            }
            if(line_area == '' || line_area == null){
                line_area = [];
            }else{
                line_area = line_area.split(',');
            }
            if(work_area == '' || work_area == null){
                work_area = [];
            }else{
                work_area = work_area.split(',');
            }
            
            var start_date_time = UtilsService.serverFommateDate(new Date($scope.config.startDate));
            var end_date_time = UtilsService.serverFommateDate(new Date($scope.config.endDate));
            var paramActions = {
            	byConditonType:by_condition_type,
				workArea : work_area,
				lineArea : line_area,
				resource : resrce,
				model : model,	
				itemIn : item,
				startDate : start_date_time,
				endDate : end_date_time,
				shift : shift
            }
            if(new Date(paramActions.startDate)>new Date(paramActions.endDate)){
                $scope.addAlert('danger','开始时间不能大于结束时间');
                return;
            }
            if((new Date(paramActions.endDate)).getTime()-(new Date(paramActions.startDate)).getTime()>92*24*60*60*1000){
                $scope.addAlert('danger','开始和结束时间跨度不超过92天');
                $scope.hideBodyModel();
                return;
            }
            $scope.showBodyModel("正在导出数据,请稍后...");
            var url = statementService
                .PPMDateExport(paramActions);
            url = HttpAppService.handleCommenUrl(url);
            $http({
                url: url,
                method: "POST",
                data: paramActions, //this is your json data string
                headers: {
                    'Content-type': 'application/json'
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                var objectUrl = URL.createObjectURL(blob);
                window.location.href=objectUrl;
                $scope.hideBodyModel();
            }).error(function (data, status, headers, config) {
            	$scope.hideBodyModel();
            	if(status == 401 || status == 403){
            		HttpAppService.reloadPage("登录已过期,请重新登录!");
        		}else if(status == 413){
        			HttpAppService.reloadPage("当前用户权限已被更改,请重新登录!");
	            }else{
	            	var msg = HttpAppService.getResponseStatusDesc(status);
	            	$scope.addAlert("danger", msg);
	            }
            });
            countPPMExport(paramActions);
        }
        function countPPMExport(paramActions){
        	var url = statementService.PPMDateExportCount(paramActions);
        	url = HttpAppService.handleCommenUrl(url);
        	$http({
                url: url,
                method: "POST",
                data: paramActions, //this is your json data string
                headers: {
                    'Content-type': 'application/json'
                },
                responseType: 'text'
            }).success(function (resultDatas) {
            	$scope.config.ppmCount="结果已导出到本地，总记录数"+resultDatas;
            }).error(function (data, status, headers, config) {
            	if(status == 401 || status == 403){
            		HttpAppService.reloadPage("登录已过期,请重新登录!");
        		}else if(status == 413){
        			HttpAppService.reloadPage("当前用户权限已被更改,请重新登录!");
	            }else{
	            	var msg = HttpAppService.getResponseStatusDesc(status);
	            	$scope.addAlert("danger", msg);
	            }
            });
        };
        //转换大小写
        $scope.upModel = function(item){
            if(item == "workArea"){
                $scope.config.workArea = angular.uppercase($scope.config.workArea);
            } else if(item == "workLine"){
                $scope.config.workLine = angular.uppercase($scope.config.workLine);
            } else if(item == "deviceCode"){
                $scope.config.deviceCode = angular.uppercase($scope.config.deviceCode);
            } else if(item == "model"){
                $scope.config.model = angular.uppercase($scope.config.model);
            } else if(item == "itemCode"){
                $scope.config.itemCode = angular.uppercase($scope.config.itemCode);
            }
        }
        //按照区域 拉线 设备 型号查询
        $scope.changeSelect = function(type){
             if(type == "DEV"){
                $scope.config.type = "DEV";
                $scope.config.showDevice = true;
                $scope.config.showCode = false;
            }else if(type == "ITEM"){
                $scope.config.type = "ITEM";
                $scope.config.showCode = true;
                $scope.config.showDevice = false;
            }
            $scope.disableBtn();
        };
        //查询按钮的控制
        $scope.disableBtn = function(){
        	var shift = $scope.config.shift;
            if($scope.config.currentShift!=null){
            	shift = $scope.config.currentShift.shift;
            }
        	if($scope.config.showDevice == true){
        		if(($scope.config.workArea == null || $scope.config.workArea == "")
            			&&($scope.config.workArea == null || $scope.config.workArea == "")
            			&&($scope.config.workLine == null || $scope.config.workLine == "")
            			&&($scope.config.deviceCode == null || $scope.config.deviceCode == "")
            			&&($scope.config.model == null || $scope.config.model == "")
            			&&($scope.config.itemCode == null || $scope.config.itemCode == "")
            			&&(shift == null || shift == "")){
        			$scope.config.downloadBtn = true;
        		}else {
        			$scope.config.downloadBtn = false;
        		}
        	}else if($scope.config.showCode == true){
        		if(($scope.config.itemCode == null || $scope.config.itemCode == "")){
        			$scope.config.downloadBtn = true;
        		}else {
        			$scope.config.downloadBtn = false;
        		}
        	}
        }
        //更换物料
        $scope.itemCodeChanged = function($select){
            $scope.config.itemCodeSelected = angular.copy($scope.config.itemCodeSelectedCopy);
            $scope.echartsShow();
        }
        //清除输入框
        $scope.deleteAll = function(item){
            if(item == "area"){
                $scope.config.workAreaName ='';
                $scope.config.workArea ='';
                $scope.config.showSelectArea = true;
            }
            if(item == 'line'){
                $scope.config.workLineName ='';
                $scope.config.workLine ='';
                $scope.config.showSelectLine = true;
            }
            if(item == 'device'){
                $scope.config.deviceCode = '' ;
                $scope.config.showResource=true;
            }
            if(item == 'model'){
                $scope.config.model = '' ;
                $scope.config.showModel = true;
            }
            if(item == 'itemCode'){
                $scope.config.itemCode = '' ;
                $scope.config.showItem = true;
            }
            $scope.disableBtn();
        }
        //生产区域帮助
        $scope.toAreaHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalAreaHelp.html',
                controller: 'areaHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportArea: function () {
                        return {
                        	workArea : $scope.config.workArea,
                        	reportType:'ppmExport'
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.workArea = selectedItem.area.join(",");
                $scope.config.workAreaName = selectedItem.areaName.join(",");
                $scope.config.showSelectArea = false;
                $scope.disableBtn();
            }, function () {
            });
        };
        //拉线帮助
        $scope.toWireLineHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalLineHelp.html',
                controller: 'LineHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportLine: function () {
                        return {
                        	workLine : $scope.config.workLine,
                        	workArea : $scope.config.workArea,
                        	reportType:'ppmExport'
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.workLine = selectedItem.line.join(",");
                $scope.config.workLineName = selectedItem.lineName.join(",");
                $scope.config.showSelectLine = false;
                $scope.disableBtn();
            }, function () {
            });
        };
        //设备帮助
        $scope.toDeviceHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalDeviceHelp.html',
                controller: 'deviceHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportDevice: function () {
                        return {
                        	workLine : $scope.config.workLine,
                        	workArea : $scope.config.workArea,
                            deviceCode : $scope.config.deviceCode,
                            reportType:'ppmExport'
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.deviceCode = selectedItem.area.join(",");
                $scope.config.deviceCodeArray = selectedItem.selectdDatas;
                $scope.config.showResource=false;
                $scope.disableBtn();
            }, function () {
            });
        };
        //model帮助
        $scope.toModelHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalModelHelp.html',
                controller: 'modelHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportModel: function () {
                        return {
                        	model : $scope.config.model,
                        	reportType:'ppmExport'
                        		};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.model = selectedItem.join(",");
                $scope.config.showModel = false;
                $scope.disableBtn();
            }, function () {
            });
        }
        //物料编码帮助
        $scope.toCodeHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalCodeHelp.html',
                controller: 'codeHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportItemcode: function () {
                        return {
                        	model : $scope.config.model,
                        	itemCode:$scope.config.itemCode,
                        	reportType:'ppmExport'
                        		};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.itemCode = selectedItem.join(",");
                $scope.config.showItem = false;
                $scope.disableBtn();
            }, function () {
            });
        }
        function __unique1(arr){
            var tmpArr = [];
            for(var i=0; i<arr.length; i++){
                if(tmpArr.indexOf(arr[i]) == -1){
                    tmpArr.push(arr[i]);
                }
            }
            return tmpArr;
        }
    }])
;