statementModule.controller('oeeTrendAnalysisStatementCtrl', [
		'$scope',
		'$http',
		'HttpAppService',
		'$timeout',
		'UtilsService',
		'uiGridConstants',
		'$uibModal',
		'$echarts',
		'$interval',
		'statementService',
		function($scope, $http, HttpAppService, $timeout, UtilsService, uiGridConstants, $uibModal, $echarts, $interval, statementService) {
			$scope.config = {
				oeeTrendAnalysisStatement : 'OEE趋势分析报表',
				gridApi: null,
				byType : "W",
				workArea : "",
				workAreaName : "",
				workLineName : "",
				workLine : "",
				resourceType : "",
				resourceTypeDesc : "",
				startDate : UtilsService.serverFommateDateShow(new Date()),
				endDate : UtilsService.serverFommateDateShow(new Date()),
				startDateMouth : UtilsService.serverFormateDateMouth(new Date()),
				endDateMouth : UtilsService.serverFormateDateMouth(new Date()),

				selectType : true,
				type : 'RT',
				dateType : "Y",

				// 放大镜是否显示
				showSelectArea : true,
				showSelectLine : true,
				deviceType : true,
				deviceCode : true,
				modelIcon : true,
				showSelectPrd : true,

				// 是否必选,红色星号
				showWorkArea : true,
				showLineArea : false,
				showResourceType : false,
				showResource : false,
				showCount : false,

				selectYear : true,
				selectMouth : true,
				selectWeek : false,
				selectDay : false,
				selectShift : false,

				queryChartShow : false,

				queryGridAreaShow : false,
				queryGridLineShow : false,
				queryGridDeviceShow : false,
				queryGridAreaCodeShow : false,
				addInfosShow : false,

				// 是否禁用查询按钮
				queryBtn : true,
				downLoadBtn : true,
				byDateDisplayName : '',

				showOeeEChart : false,
				showAEChart : false,
				showPEChart : false,
				showQEChart : false,
				showEcharts : false,

				// 报表
				outPutStatementInstance : null,
				dataEcharts : null,
				dateTypeEcharts : null,

				selectName : "",
				currentSelected : "",
				dataSelected : "",
				selectedType : "",
				itemCodeSelectedCopy : [],
				dataNum : "",

			};
			$scope.init = function() {
				$scope.statementConfig.currentWorkCenter = [];
				$scope.statementConfig.currentWorkCenterDesc = [];
				$scope.statementConfig.currentLine = [];
				$scope.statementConfig.currentLineDesc = [];
				$scope.statementConfig.currentDeviceNum = null;
				$scope.statementConfig.multipleCurrentDeviceNum = [];
				$scope.statementConfig.currentDeviceType = [];
				$scope.statementConfig.currentDeviceTypeDesc = [];
				$scope.config.model='';
				$scope.config.prd='';
				$scope.statementConfig.shiftsChange = [ {
					shift : '',
					description : '请选择',
					descriptionNew : '请选择'
				}, {
					shift : 'M',
					description : '早班',
					descriptionNew : '早班 代码:M'
				}, {
					shift : 'E',
					description : '晚班',
					descriptionNew : '晚班 代码:E'
				} ];
				var year = new Date().getFullYear();
				$scope.statementConfig.startYears = [];
				for (var i = 2011; i < 2021; i++) {
					var obj = {
						id:i,
						year : i+'-01-01',
						description : i + '年'
					};
					$scope.statementConfig.startYears.push(obj);
				}
				$scope.statementConfig.startYear = {
						id:1,
						year : year+'-01-01',
						description : year + '年'
					};
				

				$scope.statementConfig.endYear = {
					id:2,
					year : year+'-12-31',
					description : year + '年'
				};
				$scope.statementConfig.endYears = [];
				for (var i = year - 5; i < year + 5; i++) {
					var obj = {
						id:i,
						year : i+'-12-31',
						description : i + '年'
					};
					$scope.statementConfig.endYears.push(obj);
				}
			};
			$scope.init();
			// 按照生产区域查询
			$scope.OEETrendAnalysisGridWorkAreaStatement = {
				data : [],
				enableCellEdit : false,
				columnDefs : [ 
				/*{
					name : "shiftCode",
					displayName : '班次代码',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				},*/
				{
					name : "byTime",
					displayName : '班次',
					minWidth : 100,
					visible:false,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "workArea",
					displayName : '生产区域描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "workCenter",
					displayName : '拉线描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrceType",
					displayName : '设备类型描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrce",
					displayName : '设备编码',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "asset",
					displayName : '资产号',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrceDescription",
					displayName : '设备描述',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "item",
					displayName : '物料编码',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "userId",
					displayName : 'PRD操作员',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : "text-align-right",
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "oee",
					displayName : 'OEE',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					},
					cellClass : function(row, col) {
						return 'text-align-right';
					},
				}, {
					name : "a",
					displayName : '时间利用率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "p",
					displayName : '性能率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "q",
					displayName : '优率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, ],
				onRegisterApi:__requestGridApi,
			};
			// 按照拉线查询
			$scope.OEETrendAnalysisGridLineAreaStatement = {
				data : [],
				enableCellEdit : false,
				columnDefs : [ {
					name : "byTime",
					displayName : '班次',
					visible:false,
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "workArea",
					displayName : '生产区域描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "workCenter",
					displayName : '拉线描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrceType",
					displayName : '设备类型描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrce",
					displayName : '设备编码',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "asset",
					displayName : '资产号',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrceDescription",
					displayName : '设备描述',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "item",
					displayName : '物料编码',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "userId",
					displayName : 'PRD操作员',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : "text-align-right",
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "oee",
					displayName : 'OEE',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					},
					cellClass : function(row, col) {
						return 'text-align-right';
					},
				}, {
					name : "a",
					displayName : '时间利用率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "p",
					displayName : '性能率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "q",
					displayName : '优率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, ],
				onRegisterApi:__requestGridApi,
			};
			// 按照设备类型查询
			$scope.OEETrendAnalysisGridResourceTypeStatement = {
				data : [],
				enableCellEdit : false,
				columnDefs : [ /*{
					name : "shiftCode",
					displayName : '班次代码',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}*/{
					name : "byTime",
					displayName : '班次',
					minWidth : 100,
					visible:false,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "workArea",
					displayName : '生产区域描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "workCenter",
					displayName : '拉线描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrceType",
					displayName : '设备类型描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrce",
					displayName : '设备编码',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "asset",
					displayName : '资产号',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrceDescription",
					displayName : '设备描述',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "item",
					displayName : '物料编码',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "userId",
					displayName : 'PRD操作员',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : "text-align-right",
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "oee",
					displayName : 'OEE',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					},
					cellClass : function(row, col) {
						return 'text-align-right';
					},
				}, {
					name : "a",
					displayName : '时间利用率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "p",
					displayName : '性能率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "q",
					displayName : '优率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, ],
				onRegisterApi:__requestGridApi,
			};
			// 按设备查询
			$scope.OEETrendAnalysisGridResourceStatement = {
				data : [],
				enableCellEdit : false,
				columnDefs : [  /*{
					name : "shiftCode",
					displayName : '班次代码',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, */{
					name : "byTime",
					displayName : '班次',
					visible:false,
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "workArea",
					displayName : '生产区域描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "workCenter",
					displayName : '拉线描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrceType",
					displayName : '设备类型描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrce",
					displayName : '设备编码',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "asset",
					displayName : '资产号',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrceDescription",
					displayName : '设备描述',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "item",
					displayName : '物料编码',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "userId",
					displayName : 'PRD操作员',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : "text-align-right",
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "oee",
					displayName : 'OEE',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					},
					cellClass : function(row, col) {
						return 'text-align-right';
					},
				}, {
					name : "a",
					displayName : '时间利用率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					},
				}, {
					name : "p",
					displayName : '性能率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "q",
					displayName : '优率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, ],
				onRegisterApi:__requestGridApi,
			};
			//按物料编码
			$scope.OEETrendAnalysisGridItemStatement = {
				data : [],
				enableCellEdit : false,
				columnDefs : [ {
					name : "byTime",
					displayName : '班次',
					visible:false,
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "workArea",
					displayName : '生产区域描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "workCenter",
					displayName : '拉线描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrceType",
					displayName : '设备类型描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrce",
					displayName : '设备编码',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "asset",
					displayName : '资产号',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrceDescription",
					displayName : '设备描述',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "item",
					displayName : '物料编码',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "userId",
					displayName : 'PRD操作员',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : "text-align-right",
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "oee",
					displayName : 'OEE',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					},
					cellClass : function(row, col) {
						return 'text-align-right';
					},
				}, {
					name : "a",
					displayName : '时间利用率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "p",
					displayName : '性能率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "q",
					displayName : '优率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, ],
				onRegisterApi:__requestGridApi,
			};
			$scope.changeSelect = function(type) {
				if (type == 'W') {
					$scope.config.byType = "W";
					$scope.config.showWorkArea = true;
					$scope.config.showLineArea = false;
					$scope.config.showResourceType = false;
					$scope.config.showResource = false;
					$scope.config.showModel = false;
				} else if (type == 'L') {
					$scope.config.byType = "L";
					$scope.config.showLineArea = true;
					$scope.config.showWorkArea = false;
					$scope.config.showResourceType = false;
					$scope.config.showResource = false;
					$scope.config.showModel = false;
				} else if (type == 'RT') {
					$scope.config.byType = "T";
					$scope.config.showResourceType = true;
					$scope.config.showWorkArea = false;
					$scope.config.showLineArea = false;
					$scope.config.showResource = false;
					$scope.config.showModel = false;
				} else if (type == 'R') {
					$scope.config.byType = "R";
					$scope.config.showResource = true;
					$scope.config.showWorkArea = false;
					$scope.config.showLineArea = false;
					$scope.config.showResourceType = false;
					$scope.config.showModel = false;
				} else if (type == 'I') {
					$scope.config.byType = "I";
					$scope.config.showModel = true;
					$scope.config.showWorkArea = false;
					$scope.config.showLineArea = false;
					$scope.config.showResourceType = false;
					$scope.config.showResource = false;
				}
				$scope.controlQueryBtn();
			}
			// 查询按钮的控制
			$scope.controlQueryBtn = function() {
				if (($scope.statementConfig.currentWorkCenterDesc == null || $scope.statementConfig.currentWorkCenterDesc == '')
						&& ($scope.statementConfig.currentLineDesc == null || $scope.statementConfig.currentLineDesc == '')
						&& ($scope.statementConfig.currentDeviceTypeDesc == null || $scope.statementConfig.currentDeviceTypeDesc == '')
						&& ($scope.statementConfig.currentDeviceNum == null || $scope.statementConfig.currentDeviceNum == '')) {
					$scope.config.queryBtn = true;
				} else {
					$scope.config.queryBtn = false;
				}
				if ($scope.config.showWorkArea == true) {
					if ($scope.statementConfig.currentWorkCenterDesc != null && $scope.statementConfig.currentWorkCenterDesc != '') {
						$scope.config.queryBtn = false;
					} else {
						$scope.config.queryBtn = true;
					}
				} else if ($scope.config.showLineArea == true) {
					if ($scope.statementConfig.currentLineDesc != null && $scope.statementConfig.currentLineDesc != '') {
						$scope.config.queryBtn = false;
					} else {
						$scope.config.queryBtn = true;
					}
				} else if ($scope.config.showResourceType == true) {
					if ($scope.statementConfig.currentDeviceTypeDesc != null && $scope.statementConfig.currentDeviceTypeDesc != '') {
						$scope.config.queryBtn = false;
					} else {
						$scope.config.queryBtn = true;
					}
				} else if ($scope.config.showResource == true) {
					if ($scope.statementConfig.currentDeviceNum != null && $scope.statementConfig.currentDeviceNum != '') {
						$scope.config.queryBtn = false;
					} else {
						$scope.config.queryBtn = true;
					}
				} else if ($scope.config.showModel == true) {
					if ($scope.config.model != null && $scope.config.model != '') {
						$scope.config.queryBtn = false;
					} else {
						$scope.config.queryBtn = true;
					}
				}
			}
			// 第一个Echart隐藏显示控制
			$scope.hideOeeEChart = function() {
				var hideDiv = document.getElementById('oee');
				hideDiv.style.height = "0px";
				$scope.config.showOeeEChart = false;
			};
			$scope.showOeeEChart = function() {
				var hideDiv = document.getElementById('oee');
				hideDiv.style.height = "500px";
				$scope.config.showOeeEChart = true;
			};

			$scope.hideAEChart = function() {
				var hideDiv = document.getElementById('a');
				hideDiv.style.height = "0px";
				$scope.config.showAEChart = false;
			};
			$scope.showAEChart = function() {
				var hideDiv = document.getElementById('a');
				hideDiv.style.height = "500px";
				$scope.config.showAEChart = true;
			};

			$scope.hidePEChart = function() {
				var hideDiv = document.getElementById('p');
				hideDiv.style.height = "0px";
				$scope.config.showPEChart = false;
			};
			$scope.showPEChart = function() {
				var hideDiv = document.getElementById('p');
				hideDiv.style.height = "500px";
				$scope.config.showPEChart = true;
			};

			$scope.hideQEChart = function() {
				var hideDiv = document.getElementById('q');
				hideDiv.style.height = "0px";
				$scope.config.showQEChart = false;
			};
			$scope.showQEChart = function() {
				var hideDiv = document.getElementById('q');
				hideDiv.style.height = "500px";
				$scope.config.showQEChart = true;
			};

			$scope.oeeEchartsShow = function(obj) {
				//var test={ADEG0002: [45.8,0], ADEG0001: [49.5,0], x: ["2017-01","2017-02"], ADEG0003: [54.2,0]};
				var resArray=[];
				for(pro in obj){
					if(pro=='x'){
						continue;
					}
					for(var i=0;i<obj[pro].length;i++){
						resArray.push(obj[pro][i]);
					}
				}
				var maxData=Math.max.apply(null,resArray);
				var minData=Math.min.apply(null,resArray);
				if(maxData%10 > 0){
					maxData = (parseInt(maxData/10)+1)*10;
				};
				minData = (parseInt(minData/10))*10;
				
				
				// var MaxTotaPproportions=Math.max.apply(null,
				// echartData[0].totaPproportions);
				$scope.oeeChart = echarts.init(document.getElementById('oee'));
				// 指定图表的配置项和数据
				var option = {
					title : {
						text : 'OEE'
					},
					tooltip : {
						trigger : 'axis',
						formatter: function(data){
		                    var res="";
		                    res+=data[0].name;
		                    for (var i = 0; i<data.length; i++) {
		                    	var color=data[i].color;
		                    	var span='<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:'+color+'"></span>'
		                        res += '<br/>' + span + data[i].seriesName+' : ' + data[i].value+"%";
		                    }
		                    return res;
		                }
					},
					legend : {
						data : [/* 'AWIN0010','AWIN0007' */]
					},
					toolbox : {
						feature : {
							dataView : {
								show : true,
								readOnly : true
							},
							magicType : {
								show : true,
								type : [ 'line', 'bar' ]
							},
							restore : {
								show : true
							},
							saveAsImage : {
								show : true
							}
						}
					},
					grid : {
						left : '3%',
						right : '4%',
						bottom : '3%',
						containLabel : true
					},
					xAxis : [ {	
						type : 'category',
						boundaryGap : false,
						data : obj.x
					} ],
					yAxis : [ {
						type : 'value',
						axisLabel: {
                            formatter: '{value}%'
                        },
                        min: minData,
                        max: maxData,
                        interval: parseInt((maxData-minData)/5),
					} ],
					series : [
					/*
					 * { name:'AWIN0010', type:'line',
					 * data:obj.AWIN0010 }, { name:'AWIN0007',
					 * type:'line', data:obj.AWIN0007 },
					 */
					]
				};
				for ( var i in obj) {
					if (i == 'x') {
						continue;
					}
					var y = {
						name : i,
						type : 'line',
						itemStyle: {
                            normal: {
                                color:  "#"+("00000"+((Math.random()*16777215+0.5)>>0).toString(16)).slice(-6),
                                /*label : {show:true,position:'top',formatter:'{c} %'}*/
                                
                            }
                        },
						data : obj[i]
					};
					option.legend.data.push(i);
					option.series.push(y);
				}
				$scope.oeeChart.setOption(option);
				window.onresize = $scope.oeeChart.resize;
				$scope.hideBodyModel();
			}
			$scope.aEchartsShow = function(obj) {
				var resArray=[];
				for(pro in obj){
					if(pro=='x'){
						continue;
					}
					for(var i=0;i<obj[pro].length;i++){
						resArray.push(obj[pro][i]);
					}
				}
				var maxData=Math.max.apply(null,resArray);
				var minData=Math.min.apply(null,resArray);
				if(maxData%10 > 0){
					maxData = (parseInt(maxData/10)+1)*10;
				};
				minData = (parseInt(minData/10))*10;
				$scope.aChart = echarts.init(document.getElementById('a'));
				// 指定图表的配置项和数据
				var option = {
					title : {
						text : '时间利用率'
					},
					tooltip : {
						trigger : 'axis',
						formatter: function(data){
		                    var res="";
		                    res+=data[0].name;
		                    for (var i = 0; i<data.length; i++) {
		                    	var color=data[i].color;
		                    	var span='<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:'+color+'"></span>'
		                        res += '<br/>' + span + data[i].seriesName+' : ' + data[i].value+"%";
		                    }
		                    return res;
		                }
					},
					legend : {
						data : []
					},
					toolbox : {
						feature : {
							dataView : {
								show : true,
								readOnly : true
							},
							magicType : {
								show : true,
								type : [ 'line', 'bar' ]
							},
							restore : {
								show : true
							},
							saveAsImage : {
								show : true
							}
						}
					},
					grid : {
						left : '3%',
						right : '4%',
						bottom : '3%',
						containLabel : true
					},
					xAxis : [ {
						type : 'category',
						boundaryGap : false,
						data : obj.x
					} ],
					yAxis : [ {
						type : 'value',
						axisLabel: {
                            formatter: '{value}%'
                        },
                        min: minData,
                        max: maxData,
                        interval: parseInt((maxData-minData)/5),
					} ],
					series : []
				};
				for ( var i in obj) {
					if (i == 'x') {
						continue;
					}
					var y = {
						name : i,
						type : 'line',
						itemStyle: {
                            normal: {
                                color: "#"+("00000"+((Math.random()*16777215+0.5)>>0).toString(16)).slice(-6)
                            }
                        },
						data : obj[i]
					};
					option.legend.data.push(i);
					option.series.push(y);
				}
				$scope.aChart.setOption(option);
				window.onresize = $scope.aChart.resize;
				$scope.hideBodyModel();
			}
			$scope.pEchartsShow = function(obj) {
				var resArray=[];
				for(pro in obj){
					if(pro=='x'){
						continue;
					}
					for(var i=0;i<obj[pro].length;i++){
						resArray.push(obj[pro][i]);
					}
				}
				var maxData=Math.max.apply(null,resArray);
				var minData=Math.min.apply(null,resArray);
				if(maxData%10 > 0){
					maxData = (parseInt(maxData/10)+1)*10;
				};
				minData = (parseInt(minData/10))*10;
				$scope.pChart = echarts.init(document.getElementById('p'));
				// 指定图表的配置项和数据
				var option = {
					title : {
						text : '性能率'
					},
					tooltip : {
						trigger : 'axis',
						formatter: function(data){
		                    var res="";
		                    res+=data[0].name;
		                    for (var i = 0; i<data.length; i++) {
		                    	var color=data[i].color;
		                    	var span='<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:'+color+'"></span>'
		                        res += '<br/>' + span + data[i].seriesName+' : ' + data[i].value+"%";
		                    }
		                    return res;
		                }
					},
					legend : {
						data : []
					},
					toolbox : {
						feature : {
							dataView : {
								show : true,
								readOnly : true
							},
							magicType : {
								show : true,
								type : [ 'line', 'bar' ]
							},
							restore : {
								show : true
							},
							saveAsImage : {
								show : true
							}
						}
					},
					grid : {
						left : '3%',
						right : '4%',
						bottom : '3%',
						containLabel : true
					},
					xAxis : [ {
						type : 'category',
						boundaryGap : false,
						data : obj.x
					} ],
					yAxis : [ {
						type : 'value',
						axisLabel: {
                            formatter: '{value}%'
                        },
                        min: minData,
                        max: maxData,
                        interval: parseInt((maxData-minData)/5),
					} ],
					series : []
				};
				for ( var i in obj) {
					if (i == 'x') {
						continue;
					}
					var y = {
						name : i,
						type : 'line',
						itemStyle: {
                            normal: {
                                color: "#"+("00000"+((Math.random()*16777215+0.5)>>0).toString(16)).slice(-6)
                            }
                        },
						data : obj[i]
					};
					option.legend.data.push(i);
					option.series.push(y);
				}
				$scope.pChart.setOption(option);
				window.onresize = $scope.pChart.resize;
				$scope.hideBodyModel();
			}
			$scope.qEchartsShow = function(obj) {
				var resArray=[];
				for(pro in obj){
					if(pro=='x'){
						continue;
					}
					for(var i=0;i<obj[pro].length;i++){
						resArray.push(obj[pro][i]);
					}
				}
				var maxData=Math.max.apply(null,resArray);
				var minData=Math.min.apply(null,resArray);
				if(maxData%10 > 0){
					maxData = (parseInt(maxData/10)+1)*10;
				};
				minData = (parseInt(minData/10))*10;
				$scope.qChart = echarts.init(document.getElementById('q'));
				// 指定图表的配置项和数据
				var option = {
					title : {
						text : '优率'
					},
					tooltip : {
						trigger : 'axis',
						formatter: function(data){
		                    var res="";
		                    res+=data[0].name;
		                    for (var i = 0; i<data.length; i++) {
		                    	var color=data[i].color;
		                    	var span='<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:'+color+'"></span>'
		                        res += '<br/>' + span + data[i].seriesName+' : ' + data[i].value+"%";
		                    }
		                    return res;
		                }
					},
					legend : {
						data : []
					},
					toolbox : {
						feature : {
							dataView : {
								show : true,
								readOnly : true
							},
							magicType : {
								show : true,
								type : [ 'line', 'bar' ]
							},
							restore : {
								show : true
							},
							saveAsImage : {
								show : true
							}
						}
					},
					grid : {
						left : '3%',
						right : '4%',
						bottom : '3%',
						containLabel : true
					},
					xAxis : [ {
						type : 'category',
						boundaryGap : false,
						data : obj.x
					} ],
					yAxis : [ {
						type : 'value',
						axisLabel: {
                            formatter: '{value}%'
                        },
                        min: minData,
                        max: maxData,
                        interval: parseInt((maxData-minData)/5),
					} ],
					series : []
				};
				for ( var i in obj) {
					if (i == 'x') {
						continue;
					}
					var y = {
						name : i,
						type : 'line',
						itemStyle: {
                            normal: {
                                color: "#"+("00000"+((Math.random()*16777215+0.5)>>0).toString(16)).slice(-6)
                            }
                        },
						data : obj[i]
					};
					option.legend.data.push(i);
					option.series.push(y);
				}
				$scope.qChart.setOption(option);
				window.onresize = $scope.qChart.resize;
				$scope.hideBodyModel();
			}
			$scope.changeGrid=function(timeType){
				var obj={};
				if(timeType=='Y'){
					obj = {name : "byTimeChart",displayName : '日期',minWidth : 100,enableCellEdit : false,enableCellEditOnFocus : false,
							cellTooltip : function(row, col) {
								return row.entity[col.colDef.name];
							}
						  }
					if($scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridItemStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridItemStatement.columnDefs.unshift(obj);
					}
					
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[1].visible=false;
					if($scope.config.byType!='I'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[8].visible=false;
					}
					if($scope.config.prd==''){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=false;
					}else{
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=true;
					}
				}else if(timeType=='M'){
					obj = {name : "byTimeChart",displayName : '日期',minWidth : 100,enableCellEdit : false,enableCellEditOnFocus : false,
							cellTooltip : function(row, col) {
								return row.entity[col.colDef.name];
							}
						  }
					if($scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridItemStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridItemStatement.columnDefs.unshift(obj);
					}
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[1].visible=false;
					
					if($scope.config.byType!='I'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[8].visible=false;
					}
					if($scope.config.prd==''){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=false;
					}else{
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=true;
					}
				}else if(timeType=='W'){
					obj = {name : "byTimeChart",displayName : '日期',minWidth : 100,enableCellEdit : false,enableCellEditOnFocus : false,
							cellTooltip : function(row, col) {
								return row.entity[col.colDef.name];
							}
						  }
					if($scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridItemStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridItemStatement.columnDefs.unshift(obj);
					}
					
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[1].visible=false;
					
					if($scope.config.byType!='I'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[8].visible=false;
					}
					if($scope.config.prd==''){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=false;
					}else{
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=true;
					}
				}else if(timeType=='D'){
					obj = {name : "byTimeChart",displayName : '日期',minWidth : 100,enableCellEdit : false,enableCellEditOnFocus : false,
							cellTooltip : function(row, col) {
								return row.entity[col.colDef.name];
							}
						  }
					if($scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridItemStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridItemStatement.columnDefs.unshift(obj);
					}
					
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[1].visible=false;
					
					if($scope.config.byType!='I'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[8].visible=false;
					}
					if($scope.config.prd==''){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=false;
					}else{
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=true;
					}
				}else if(timeType=='S'){
					/*obj = {name : "shiftStartTime",displayName : '班次',minWidth : 100,enableCellEdit : false,enableCellEditOnFocus : false,
							cellTooltip : function(row, col) {
								return row.entity[col.colDef.name];
							}	
						 }*/
					if($scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[0].name=='byTimeChart'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs.splice(0, 1);
					}
					if($scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[0].name=='byTimeChart'){
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs.splice(0, 1);
					}
					if($scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[0].name=='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs.splice(0, 1);
					}
					if($scope.OEETrendAnalysisGridResourceStatement.columnDefs[0].name=='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs.splice(0, 1);
					}
					if($scope.OEETrendAnalysisGridItemStatement.columnDefs[0].name=='byTimeChart'){
						$scope.OEETrendAnalysisGridItemStatement.columnDefs.splice(0, 1);
					}
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[0].visible=true;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[0].visible=true;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[0].visible=true;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[0].visible=true;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[0].visible=true;
					
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[7].visible=true;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[7].visible=true;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[7].visible=true;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[7].visible=true;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[7].visible=true;
						
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[8].visible=true;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[8].visible=true;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[8].visible=true;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[8].visible=true;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[8].visible=true;
				}
			}
			// 查询
			$scope.queryDatas = function() {
				$scope.config.showEcharts = true;
				$scope.config.showOeeEChart = true;
				$scope.config.showAEChart = true;
				$scope.config.showPEChart = true;
				$scope.config.showQEChart = true;
				
				var params = {
					byType : $scope.config.byType,
					workCenter : $scope.statementConfig.currentWorkCenter,
					lineArea : $scope.statementConfig.currentLine,
					resourceType : $scope.statementConfig.currentDeviceType,
					resource : $scope.statementConfig.currentDeviceNum,
					model : $scope.config.model,
					prd : $scope.config.prd,
					byTimeType : $scope.config.dateType,
					startDate : '',
					endDate : '',
				};
				if ($scope.statementConfig.currentShift) {
					params.shift = $scope.statementConfig.currentShift.shift;
				}
				if ($scope.config.selectYear == true) {
					params.startDate = $scope.statementConfig.startYear.year;
					params.endDate = $scope.statementConfig.endYear.year;
				} else if ($scope.config.selectType == true) {
					params.startDate = UtilsService.getMonthDate(new Date($scope.config.startDateMouth), 'start');
					params.endDate = UtilsService.getMonthDate(new Date($scope.config.endDateMouth), 'end');
				} else {
					params.startDate = UtilsService.serverFommateDate(new Date($scope.config.startDate));
					params.endDate = UtilsService.serverFommateDate(new Date($scope.config.endDate));
				}
				if (new Date(params.startDate) > new Date(params.endDate)) {
					$scope.addAlert('danger', '开始时间不能大于结束时间');
					return;
				}
				if (params.byTimeType == 'Y') {
					$scope.changeGrid(params.byTimeType);
					if (UtilsService.addMonth(new Date(params.startDate), 36) < new Date(params.endDate)) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过3年');
						$scope.hideBodyModel();
						return;
					}
				} else if (params.byTimeType == 'M') {
					$scope.changeGrid(params.byTimeType);
					if (UtilsService.addMonth(new Date(params.startDate), 12) < new Date(params.endDate)) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过12个月');
						$scope.hideBodyModel();
						return;
					}
				} else if (params.byTimeType == 'W') {
					$scope.changeGrid(params.byTimeType);
					if ((new Date(params.endDate)).getTime() - (new Date(params.startDate)).getTime() > 105 * 24 * 60 * 60 * 1000) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过15周');
						$scope.hideBodyModel();
						return;
					}
				} else if (params.byTimeType == 'D') {
					$scope.changeGrid(params.byTimeType);
					if ((new Date(params.endDate)).getTime() - (new Date(params.startDate)).getTime() > 92 * 24 * 60 * 60 * 1000) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过92天');
						return;
					}
				} else if (params.byTimeType == 'S') {
					$scope.changeGrid(params.byTimeType);
					if ((new Date(params.endDate)).getTime() - (new Date(params.startDate)).getTime() > 92 * 24 * 60 * 60 * 1000) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过92天');
						return;
					}
				}
				$scope.showBodyModel("正在加载数据,请稍后...");
				statementService.OEETrendAnalysisGridList(params).then(function(resultDatas) {
					$scope.hideBodyModel();
					if (resultDatas.response && resultDatas.response[0].length > 0) {
						$scope.config.dataNum = "总记录数:" + resultDatas.response[0].length;
						$scope.config.downLoadBtn = false;
						$scope.config.showCount = true;
						$scope.showOeeEChart();
						$scope.showAEChart();
						$scope.showPEChart();
						$scope.showQEChart();
						$scope.oeeEchartsShow(resultDatas.response[1].oee);
						$scope.aEchartsShow(resultDatas.response[1].a);
						$scope.pEchartsShow(resultDatas.response[1].p);
						$scope.qEchartsShow(resultDatas.response[1].q);
						if ($scope.config.byType == 'W') {
							$scope.OEETrendAnalysisGridWorkAreaStatement.data = resultDatas.response[0];
							for(var i=0;i<$scope.OEETrendAnalysisGridWorkAreaStatement.data.length;i++){
								if($scope.OEETrendAnalysisGridWorkAreaStatement.data[i].oee==null ||$scope.OEETrendAnalysisGridWorkAreaStatement.data[i].oee==''){
									$scope.OEETrendAnalysisGridWorkAreaStatement.data[i].oee='N/A';
								}else{
									$scope.OEETrendAnalysisGridWorkAreaStatement.data[i].oee+="%";
								}
								if($scope.OEETrendAnalysisGridWorkAreaStatement.data[i].a==null ||$scope.OEETrendAnalysisGridWorkAreaStatement.data[i].a==''){
									$scope.OEETrendAnalysisGridWorkAreaStatement.data[i].a='N/A';
								}else{
									$scope.OEETrendAnalysisGridWorkAreaStatement.data[i].a+="%";
								}
								if($scope.OEETrendAnalysisGridWorkAreaStatement.data[i].p==null ||$scope.OEETrendAnalysisGridWorkAreaStatement.data[i].p==''){
									$scope.OEETrendAnalysisGridWorkAreaStatement.data[i].p='N/A';
								}else{
									$scope.OEETrendAnalysisGridWorkAreaStatement.data[i].p+="%";
								}
								if($scope.OEETrendAnalysisGridWorkAreaStatement.data[i].q==null ||$scope.OEETrendAnalysisGridWorkAreaStatement.data[i].q==''){
									$scope.OEETrendAnalysisGridWorkAreaStatement.data[i].q='N/A';
								}else{
									$scope.OEETrendAnalysisGridWorkAreaStatement.data[i].q+="%";
								}
							}
							$scope.config.queryGridAreaShow = true;
							$scope.config.queryGridLineShow = false;
							$scope.config.queryGridDeviceShow = false;
							$scope.config.queryGridAreaCodeShow = false;
							$scope.config.queryGridItemShow=false;
						} else if ($scope.config.byType == 'L') {
							$scope.OEETrendAnalysisGridLineAreaStatement.data = resultDatas.response[0];
							for(var i=0;i<$scope.OEETrendAnalysisGridLineAreaStatement.data.length;i++){
								if($scope.OEETrendAnalysisGridLineAreaStatement.data[i].oee==null ||$scope.OEETrendAnalysisGridLineAreaStatement.data[i].oee==''){
									$scope.OEETrendAnalysisGridLineAreaStatement.data[i].oee='N/A';
								}else{
									$scope.OEETrendAnalysisGridLineAreaStatement.data[i].oee+="%";
								}
								if($scope.OEETrendAnalysisGridLineAreaStatement.data[i].a==null ||$scope.OEETrendAnalysisGridLineAreaStatement.data[i].a==''){
									$scope.OEETrendAnalysisGridLineAreaStatement.data[i].a='N/A';
								}else{
									$scope.OEETrendAnalysisGridLineAreaStatement.data[i].a+="%";
								}
								if($scope.OEETrendAnalysisGridLineAreaStatement.data[i].p==null ||$scope.OEETrendAnalysisGridLineAreaStatement.data[i].p==''){
									$scope.OEETrendAnalysisGridLineAreaStatement.data[i].p='N/A';
								}else{
									$scope.OEETrendAnalysisGridLineAreaStatement.data[i].p+="%";
								}
								if($scope.OEETrendAnalysisGridLineAreaStatement.data[i].q==null ||$scope.OEETrendAnalysisGridLineAreaStatement.data[i].q==''){
									$scope.OEETrendAnalysisGridLineAreaStatement.data[i].q='N/A';
								}else{
									$scope.OEETrendAnalysisGridLineAreaStatement.data[i].q+="%";
								}
							}
							$scope.config.queryGridLineShow = true;
							$scope.config.queryGridAreaShow = false;
							$scope.config.queryGridDeviceShow = false;
							$scope.config.queryGridAreaCodeShow = false;
							$scope.config.queryGridItemShow=false;
						} else if ($scope.config.byType == 'T') {
							$scope.OEETrendAnalysisGridResourceTypeStatement.data = resultDatas.response[0];
							for(var i=0;i<$scope.OEETrendAnalysisGridResourceTypeStatement.data.length;i++){
								if($scope.OEETrendAnalysisGridResourceTypeStatement.data[i].oee==null ||$scope.OEETrendAnalysisGridResourceTypeStatement.data[i].oee==''){
									$scope.OEETrendAnalysisGridResourceTypeStatement.data[i].oee='N/A';
								}else{
									$scope.OEETrendAnalysisGridResourceTypeStatement.data[i].oee+="%";
								}
								if($scope.OEETrendAnalysisGridResourceTypeStatement.data[i].a==null ||$scope.OEETrendAnalysisGridResourceTypeStatement.data[i].a==''){
									$scope.OEETrendAnalysisGridResourceTypeStatement.data[i].a='N/A';
								}else{
									$scope.OEETrendAnalysisGridResourceTypeStatement.data[i].a+="%";
								}
								if($scope.OEETrendAnalysisGridResourceTypeStatement.data[i].p==null ||$scope.OEETrendAnalysisGridResourceTypeStatement.data[i].p==''){
									$scope.OEETrendAnalysisGridResourceTypeStatement.data[i].p='N/A';
								}else{
									$scope.OEETrendAnalysisGridResourceTypeStatement.data[i].p+="%";
								}
								if($scope.OEETrendAnalysisGridResourceTypeStatement.data[i].q==null ||$scope.OEETrendAnalysisGridResourceTypeStatement.data[i].q==''){
									$scope.OEETrendAnalysisGridResourceTypeStatement.data[i].q='N/A';
								}else{
									$scope.OEETrendAnalysisGridResourceTypeStatement.data[i].q+="%";
								}
							}
							$scope.config.queryGridDeviceShow = true;
							$scope.config.queryGridLineShow = false;
							$scope.config.queryGridAreaShow = false;
							$scope.config.queryGridAreaCodeShow = false;
							$scope.config.queryGridItemShow=false;
						} else if ($scope.config.byType == 'R') {
							$scope.OEETrendAnalysisGridResourceStatement.data = resultDatas.response[0];
							for(var i=0;i<$scope.OEETrendAnalysisGridResourceStatement.data.length;i++){
								if($scope.OEETrendAnalysisGridResourceStatement.data[i].oee==null ||$scope.OEETrendAnalysisGridResourceStatement.data[i].oee==''){
									$scope.OEETrendAnalysisGridResourceStatement.data[i].oee='N/A';
								}else{
									$scope.OEETrendAnalysisGridResourceStatement.data[i].oee+="%";
								}
								if($scope.OEETrendAnalysisGridResourceStatement.data[i].a==null ||$scope.OEETrendAnalysisGridResourceStatement.data[i].a==''){
									$scope.OEETrendAnalysisGridResourceStatement.data[i].a='N/A';
								}else{
									$scope.OEETrendAnalysisGridResourceStatement.data[i].a+="%";
								}
								if($scope.OEETrendAnalysisGridResourceStatement.data[i].p==null ||$scope.OEETrendAnalysisGridResourceStatement.data[i].p==''){
									$scope.OEETrendAnalysisGridResourceStatement.data[i].p='N/A';
								}else{
									$scope.OEETrendAnalysisGridResourceStatement.data[i].p+="%";
								}
								if($scope.OEETrendAnalysisGridResourceStatement.data[i].q==null ||$scope.OEETrendAnalysisGridResourceStatement.data[i].q==''){
									$scope.OEETrendAnalysisGridResourceStatement.data[i].q='N/A';
								}else{
									$scope.OEETrendAnalysisGridResourceStatement.data[i].q+="%";
								}
							}
							$scope.config.queryGridAreaCodeShow = true;
							$scope.config.queryGridDeviceShow = false;
							$scope.config.queryGridLineShow = false;
							$scope.config.queryGridAreaShow = false;
							$scope.config.queryGridItemShow=false;
						}else if ($scope.config.byType == 'I') {
							$scope.OEETrendAnalysisGridItemStatement.data = resultDatas.response[0];
							for(var i=0;i<$scope.OEETrendAnalysisGridItemStatement.data.length;i++){
								if($scope.OEETrendAnalysisGridItemStatement.data[i].oee==null ||$scope.OEETrendAnalysisGridItemStatement.data[i].oee==''){
									$scope.OEETrendAnalysisGridItemStatement.data[i].oee='N/A';
								}else{
									$scope.OEETrendAnalysisGridItemStatement.data[i].oee+="%";
								}
								if($scope.OEETrendAnalysisGridItemStatement.data[i].a==null ||$scope.OEETrendAnalysisGridItemStatement.data[i].a==''){
									$scope.OEETrendAnalysisGridItemStatement.data[i].a='N/A';
								}else{
									$scope.OEETrendAnalysisGridItemStatement.data[i].a+="%";
								}
								if($scope.OEETrendAnalysisGridItemStatement.data[i].p==null ||$scope.OEETrendAnalysisGridItemStatement.data[i].p==''){
									$scope.OEETrendAnalysisGridItemStatement.data[i].p='N/A';
								}else{
									$scope.OEETrendAnalysisGridItemStatement.data[i].p+="%";
								}
								if($scope.OEETrendAnalysisGridItemStatement.data[i].q==null ||$scope.OEETrendAnalysisGridItemStatement.data[i].q==''){
									$scope.OEETrendAnalysisGridItemStatement.data[i].q='N/A';
								}else{
									$scope.OEETrendAnalysisGridItemStatement.data[i].q+="%";
								}
							}
							$scope.config.queryGridItemShow=true;
							$scope.config.queryGridAreaShow = false;
							$scope.config.queryGridLineShow = false;
							$scope.config.queryGridDeviceShow = false;
							$scope.config.queryGridAreaCodeShow = false;
						}
					} else {
						$scope.addAlert("暂无数据");
						$scope.config.showCount = false;
						$scope.config.queryGridAreaShow = false;
						$scope.config.queryGridLineShow = false;
						$scope.config.queryGridDeviceShow = false;
						$scope.config.queryGridAreaCodeShow = false;
						$scope.OEETrendAnalysisGridWorkAreaStatement.data = [];
						$scope.OEETrendAnalysisGridLineAreaStatement.data = [];
						$scope.OEETrendAnalysisGridResourceTypeStatement.data = [];
						$scope.OEETrendAnalysisGridResourceStatement.data = [];
						$scope.OEETrendAnalysisGridItemStatement.data = [];
						$scope.config.showEcharts = false;
					}
				}, function(resultDatas) {
					$scope.hideBodyModel();
					$scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
				});
			}
			$scope.downLoad = function() {
				$scope.showBodyModel("正在导出数据,请稍后...");
				var params = {
					byType : $scope.config.byType,
					workCenter : $scope.statementConfig.currentWorkCenter,
					lineArea : $scope.statementConfig.currentLine,
					resourceType : $scope.statementConfig.currentDeviceType,
					resource : $scope.statementConfig.currentDeviceNum,
					model : $scope.config.model,
					prd : $scope.config.prd,
					byTimeType : $scope.config.dateType,
					startDate : '',
					endDate : '',
				};
				if ($scope.statementConfig.currentShift) {
					params.shift = $scope.statementConfig.currentShift.shift;
				}
				if ($scope.config.selectYear == true) {
					params.startDate = $scope.statementConfig.startYear.year;
					params.endDate = $scope.statementConfig.endYear.year;
				} else if ($scope.config.selectType == true) {
					params.startDate = UtilsService.getMonthDate(new Date($scope.config.startDateMouth), 'start');
					params.endDate = UtilsService.getMonthDate(new Date($scope.config.endDateMouth), 'end');
				} else {
					params.startDate = UtilsService.serverFommateDate(new Date($scope.config.startDate));
					params.endDate = UtilsService.serverFommateDate(new Date($scope.config.endDate));
				}
				if (new Date(params.startDate) > new Date(params.endDate)) {
					$scope.addAlert('danger', '开始时间不能大于结束时间');
					return;
				}
				if (params.byTimeType == 'Y') {
					$scope.changeGrid(params.byTimeType);
					if (UtilsService.addMonth(new Date(params.startDate), 36) < new Date(params.endDate)) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过3年');
						$scope.hideBodyModel();
						return;
					}
				}else if (params.byTimeType == 'M') {
					if (UtilsService.addMonth(new Date(params.startDate), 12) < new Date(params.endDate)) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过12个月');
						$scope.hideBodyModel();
						return;
					}
				} else if (params.byTimeType == 'W') {
					if ((new Date(params.endDate)).getTime() - (new Date(params.startDate)).getTime() > 105 * 24 * 60 * 60 * 1000) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过15周');
						$scope.hideBodyModel();
						return;
					}
				} else if (params.byTimeType == 'D') {
					if ((new Date(params.endDate)).getTime() - (new Date(params.startDate)).getTime() > 92 * 24 * 60 * 60 * 1000) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过92天');
						return;
					}
				} else if (params.byTimeType == 'S') {
					if ((new Date(params.endDate)).getTime() - (new Date(params.startDate)).getTime() > 92 * 24 * 60 * 60 * 1000) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过92天');
						return;
					}
				}
				var url = statementService.OEETrendAnalysisExport(params);
				url = url.replace(/undefined/g, "").replace(/null/g, "");
				url = HttpAppService.handleCommenUrl(url);
				$http({
					url : url,
					method : "GET",
					headers : {
						'Content-type' : 'application/json'
					},
					responseType : 'arraybuffer'
				}).success(function(data, status, headers, config) {
					var blob = new Blob([ data ], {
						type : "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
					});
					var objectUrl = URL.createObjectURL(blob);
					window.location.href = objectUrl;
					$scope.hideBodyModel();
				}).error(function(data, status, headers, config) {
				});
			}
			// 按照 月 周 天 班次 汇总
			$scope.changeAddType = function(type) {
				if (type == "Y") {
					$scope.config.dateType = "Y";
					$scope.config.selectYear = true;
					$scope.config.selectMouth = false;
					$scope.config.selectWeek = false;
					$scope.config.selectDay = false;
					$scope.config.selectShift = false;
					$scope.config.selectType = false;
					$scope.config.startDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.endDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.startDateMouth = UtilsService.serverFormateDateMouth(new Date());
					$scope.config.endDateMouth = UtilsService.serverFormateDateMouth(new Date());
				} else if (type == "M") {
					$scope.config.dateType = "M";
					$scope.config.selectYear = false;
					$scope.config.selectMouth = true;
					$scope.config.selectWeek = false;
					$scope.config.selectDay = false;
					$scope.config.selectShift = false;
					$scope.config.selectType = true;
					$scope.config.startDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.endDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.startDateMouth = UtilsService.serverFormateDateMouth(new Date());
					$scope.config.endDateMouth = UtilsService.serverFormateDateMouth(new Date());
				} else if (type == "W") {
					$scope.config.dateType = "W";
					$scope.config.selectYear = false;
					$scope.config.selectMouth = false;
					$scope.config.selectWeek = true;
					$scope.config.selectDay = false;
					$scope.config.selectShift = false;
					$scope.config.selectType = false;
					$scope.config.startDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.endDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.startDateMouth = UtilsService.serverFormateDateMouth(new Date());
					$scope.config.endDateMouth = UtilsService.serverFormateDateMouth(new Date());
				} else if (type == "D") {
					$scope.config.dateType = "D";
					$scope.config.selectYear = false;
					$scope.config.selectMouth = false;
					$scope.config.selectWeek = false;
					$scope.config.selectDay = true;
					$scope.config.selectShift = false;
					$scope.config.selectType = false;
					$scope.config.startDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.endDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.startDateMouth = UtilsService.serverFormateDateMouth(new Date());
					$scope.config.endDateMouth = UtilsService.serverFormateDateMouth(new Date());
				} else if (type == "S") {
					$scope.config.dateType = "S";
					$scope.config.selectYear = false;
					$scope.config.selectMouth = false;
					$scope.config.selectWeek = false;
					$scope.config.selectDay = false;
					$scope.config.selectShift = true;
					$scope.config.selectType = false;
					$scope.config.startDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.endDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.startDateMouth = UtilsService.serverFormateDateMouth(new Date());
					$scope.config.endDateMouth = UtilsService.serverFormateDateMouth(new Date());
				}
			};
			// 清除输入框
			$scope.deleteAll = function(item) {
				if (item == 'area') {
					$scope.statementConfig.currentWorkCenter = [];
					$scope.statementConfig.currentWorkCenterDesc = [];
					$scope.statementConfig.currentLine = [];
					$scope.statementConfig.currentLineDesc = [];
					$scope.statementConfig.deviceNums = [];
					$scope.statementConfig.currentDeviceNum = null;
					$scope.statementConfig.multipleCurrentDeviceNum = [];
					$scope.config.showSelectArea = true;
					$scope.config.showSelectLine = true;
					$scope.config.deviceCode = true;
					$scope.controlQueryBtn();
				}
				if (item == 'line') {
					$scope.statementConfig.currentLine = [];
					$scope.statementConfig.currentLineDesc = [];
					$scope.statementConfig.deviceNums = [];
					$scope.statementConfig.currentDeviceNum = null;
					$scope.statementConfig.multipleCurrentDeviceNum = [];
					$scope.config.showSelectLine = true;
					$scope.config.deviceCode = true;
					$scope.controlQueryBtn();
				}
				if (item == 'deviceType') {
					$scope.statementConfig.currentDeviceType = [];
					$scope.statementConfig.currentDeviceTypeDesc = [];
					$scope.statementConfig.multipleCurrentDeviceNum = [];
					$scope.config.deviceType = true;
					$scope.config.deviceCode = true;
					$scope.controlQueryBtn();
				}
				if (item == 'deviceCode') {
					$scope.statementConfig.currentDeviceNum = [];
					$scope.config.deviceCode = true;
					$scope.controlQueryBtn();
				}
				if (item == 'model') {
					$scope.config.model = [];
					$scope.config.modelIcon = true;
					$scope.controlQueryBtn();
				}
				if (item == 'prd') {
					$scope.config.prd = [];
					$scope.config.showSelectPrd = true;
					$scope.controlQueryBtn();
				}
			}
			// 选择时间 这是选择月的
			// 生产区域帮助
			$scope.toAreaHelp = function() {
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/modal/areaSelectMoreModal.html',
					controller : 'areaSelectMoreCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						selectAreaModal : function() {
							return {
								workArea : $scope.statementConfig.workCenters,
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.statementConfig.currentWorkCenter = selectedItem.area.join(",");
					$scope.statementConfig.currentWorkCenterDesc = selectedItem.areaName.join(",");
					$scope.config.showSelectArea = false;
					$scope.deleteAll('line');
					$scope.deleteAll('deviceCode');
					$scope.controlQueryBtn();
				}, function() {
				});
			};
			// 拉线多选帮助
			$scope.toWireLineHelp = function() {
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/modal/lineSelectModal.html',
					controller : 'LineSelectMoreCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						selectLineModal : function() {
							return {
								workArea : $scope.statementConfig.currentWorkCenter,
								workLine : $scope.statementConfig.lines,
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.statementConfig.currentLine = selectedItem.line;
					$scope.statementConfig.currentLineDesc = selectedItem.lineName;
					if ($scope.statementConfig.currentLine.length > 0) {
						$scope.config.showSelectLine = false;
					}
					$scope.deleteAll('deviceCode');
					$scope.controlQueryBtn();
				}, function() {
				});
			};
			// 设备类型帮助
			$scope.toDeviceTypeHelp = function() {
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/modal/deviceTypeSelectMoreModal.html',
					controller : 'deviceTypeMoreCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						selectTypeMoreDevice : function() {
							return {
								deviceCode : $scope.statementConfig.deviceTypes,
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.statementConfig.currentDeviceType = selectedItem.resourceType;
					$scope.statementConfig.currentDeviceTypeDesc = selectedItem.resourceTypeDesc;
					if ($scope.statementConfig.currentDeviceType.length > 0) {
						$scope.config.deviceType = false;
					}
					$scope.deleteAll('deviceCode');
					$scope.controlQueryBtn();
				}, function() {
				});
			};
			// 设备帮助
			$scope.toDeviceHelp = function() {
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/modal/modalDeviceHelp.html',
					controller : 'deviceMoreCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						selectMoreDevice : function() {
							return {
								workArea : $scope.statementConfig.currentWorkCenter,
								workLine : $scope.statementConfig.currentLine,
								deviceCode : $scope.statementConfig.currentDeviceType,
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.statementConfig.currentDeviceNum = selectedItem.area;
					if ($scope.statementConfig.currentDeviceNum.length > 0) {
						$scope.config.deviceCode = false;
					}
					$scope.controlQueryBtn();
				}, function() {
				});
			};
			// model帮助
			$scope.toModelHelp = function() {
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/outputStatement/modal/modalModelHelp.html',
					controller : 'modelHelpCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						outPutReportModel : function() {
							return {
								model : $scope.config.model
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.config.model = selectedItem.join(",");
					if ($scope.config.model.length > 0) {
						$scope.config.modelIcon = false;
					}
					$scope.controlQueryBtn();
				}, function() {
				});
			}
			// PRD帮助
			$scope.toPrdHelp = function() {
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/outputStatement/modal/prdModelHelp.html',
					controller : 'prdHelpCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						outPutReportModel : function() {
							return {
								prd : $scope.config.prd
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.config.prd = selectedItem.join(",");
					if ($scope.config.prd.length > 0) {
						$scope.config.showSelectPrd = false;
					}
					$scope.controlQueryBtn();
				}, function() {
				});
			}
			function __requestGridApi(gridApi){
				$scope.config.gridApi = gridApi;
			}
			//控制显示隐藏列
			$scope.showHideCol=function () {
				if($scope.config.queryGridAreaShow){
					for(var i=0;i<$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs.length;i++) {
						if (!$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[i].visible) {
							$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[i].visible = true;
						}
					}
				}
				else if($scope.config.queryGridLineShow){
					for(var i=0;i<$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs.length;i++)
					{
						if(!$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[i].visible){
							$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[i].visible=true;
						}
					}
				}
				else if($scope.config.queryGridDeviceShow){
					for(var i=0;i<$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs.length;i++)
					{
						if(!$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[i].visible){
							$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[i].visible=true;
						}
					}
				}
				else if($scope.config.queryGridAreaCodeShow){
					for(var i=0;i<$scope.OEETrendAnalysisGridResourceStatement.columnDefs.length;i++)
					{
						if(!$scope.OEETrendAnalysisGridResourceStatement.columnDefs[i].visible){
							$scope.OEETrendAnalysisGridResourceStatement.columnDefs[i].visible=true;
						}                }
				}
				else if($scope.config.queryGridItemShow){
					for(var i=0;i<$scope.gridDeviceRtReasonCodeStatement.columnDefs.length;i++)
					{
						if(!$scope.OEETrendAnalysisGridItemStatement.columnDefs[i].visible){
							$scope.OEETrendAnalysisGridItemStatement.columnDefs[i].visible=true;
						}                }
				}
				$scope.config.gridApi.core.refresh();
			};

			var refreshEchart=$scope.$on("refreshEchart", function(event,data) {
				UtilsService.log("接受成功shutdownReason");
				if($scope.config.showEcharts) {
					$scope.timeOuter = $timeout(function () {
						$scope.oeeChart.resize();
						$scope.aChart.resize();
						$scope.pChart.resize();
						$scope.qChart.resize();
					}, 350);
				}
			});
			$scope.$on('$destroy',function(){
				refreshEchart();
				$timeout.cancel($scope.timeOuter);
				UtilsService.log("销毁成功shutdownReason");
			});
		} ])