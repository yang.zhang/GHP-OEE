statementModule.controller('deviceStatusStatementCtrl',['$rootScope','$scope', '$http',
    'HttpAppService', '$timeout','UtilsService',
    'uiGridConstants','$uibModal','plcService','statementService',
    function ($rootScope,$scope, $http,
              HttpAppService, $timeout,UtilsService,
              uiGridConstants,$uibModal,plcService,statementService) {

        $scope.config = {
            statement : '设备明细报表',
            isCollapsed : false,
            gridApi: null,
            currentThemeGrid:null,
            themeGrid:[
                {
                    id:'S',
                    name:'设备状态明细报表'
                },
                {
                    id:'A',
                    name:'设备报警明细报表'
                },
                {
                    id:'H',
                    name:'最终停机原因报表'
                },
                {
                    id:'O',
                    name:'设备产量明细报表'
                },
                {
                    id:'R',
                    name:'实时停机原因报表'
                }],
            btnDisabledDownload:true,
            alertCount:null,
            itemCount:null,
            reasonCodeCount:null,
            outputCount:null,
            rtReasonCodeCount:null,
            totalHalt:null,
            totalTime:null,
            showSelectLine : true,
            deviceCode : true,
            showSelectArea : true,
            deviceType : true,
            showSelectAlarm:true
        };
        $scope.getHeight=function(){
        	var defaultRowNum=20;
        	var rowHeight=31;
        	return {
        		height:(defaultRowNum*rowHeight)+"px"
        	};
        }
        //设备停机原因

        $scope.gridDeviceHaltStatement = {
            enablePagination: true,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,
            useExternalPagination: true,
            paginationCurrentPage: 1,
            totalItems: 0,
            enableCellEdit:false,

            columnDefs:[
                {
                    name:"strDate",
                    displayName:'日期',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"shift",
                    displayName:'班次',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"prdUserId",
                    displayName:'生产操作员',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceCode",
                    displayName:'设备编码',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceTypeDesc",
                    displayName:'设备类型',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"asset",
                    displayName:'资产号',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceDesc",
                    displayName:'设备名称',
                    minWidth:250,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceAddr",
                    displayName:'拉线',
                    minWidth:200,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"strStartTime",
                    displayName:'开始时间',
                    minWidth:170,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"strEndTime",
                    displayName:'结束时间',
                    minWidth:170,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceState",
                    displayName:'设备状态',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"duration",
                    displayName:'时长（min）',
                    minWidth:100,cellClass:"text-align-right",
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"reasonCodeDesc",
                    displayName:'停机原因',
                    minWidth:200,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"comment",
                    displayName:'匹配异常的停机原因',
                    minWidth:200,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
            ],
            onRegisterApi:__requestDeviceHaltApi,
        };
        //生产区域帮助
        $scope.toAreaHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/areaSelectMoreModal.html',
                controller: 'areaSelectMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectAreaModal: function () {
                        return {workArea : $scope.statementConfig.workCenters};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentWorkCenter = selectedItem.area.join(",");
                $scope.statementConfig.currentWorkCenterDesc = selectedItem.areaName.join(",");
                $scope.config.showSelectArea = false;
                $scope.deleteAll('line');
                $scope.deleteAll('deviceCode');
                //$scope.workCenterChanged('T');
            }, function () {
            });
        };
        //拉线多选帮助
        $scope.toWireLineHelp = function () {
            /*if($scope.statementConfig.currentWorkCenter == ''){
                $scope.addAlert('','请先选择生产区域');
                return;
            }*/
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/lineSelectModal.html',
                controller: 'LineSelectMoreCtrl',
                size: 'lg',
                backdrop: 'false',
                scope: $scope,
                openedClass: 'flex-center-parent',
                resolve: {
                    selectLineModal: function () {
                        return {
                        	workArea: $scope.statementConfig.currentWorkCenter,
                        	workLine: $scope.statementConfig.lines
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentLine = selectedItem.line;
                $scope.statementConfig.currentLineDesc = selectedItem.lineName;
                if($scope.statementConfig.currentLine.length >0){
                    $scope.config.showSelectLine = false;
                }
                $scope.deleteAll('deviceCode');
                //$scope.lineChanged('S');
            }, function () {
            });
        };
        //设备类型帮助
        $scope.toDeviceTypeHelp = function(){
            /*if($scope.statementConfig.currentWorkCenter == ''){
                $scope.addAlert('','请先选择生产区域');
                return;
            }*/
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/deviceTypeSelectMoreModal.html',
                controller: 'deviceTypeMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectTypeMoreDevice: function () {
                        return {deviceCode : $scope.statementConfig.deviceTypes};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentDeviceType = selectedItem.resourceType;
                $scope.statementConfig.currentDeviceTypeDesc = selectedItem.resourceTypeDesc;
                if($scope.statementConfig.currentDeviceType.length >0){
                    $scope.config.deviceType = false;
                }
                $scope.deleteAll('deviceCode');
                //$scope.deviceTypeChanged('S');
            }, function () {
            });
        };	
        //设备帮助
        $scope.toDeviceHelp = function(){
            /*if($scope.statementConfig.currentWorkCenter == ''){
                $scope.addAlert('','请先选择生产区域');
                return;
            }*/
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/modalDeviceHelp.html',
                controller: 'deviceMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectMoreDevice: function () {
                        return {
                        	workArea: $scope.statementConfig.currentWorkCenter,
                        	workLine: $scope.statementConfig.currentLine,
                        	deviceCode : $scope.statementConfig.currentDeviceType
                        	};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentDeviceNum = selectedItem.area;
                if($scope.statementConfig.currentDeviceNum.length >0){
                    $scope.config.deviceCode = false;
                }
            }, function () {
            });
        };
        //报警备注选择
        $scope.toChooseAlarmRemark = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/alarmRemarkSelectMoreModal.html',
                controller: 'chooseAlarmRemarkMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectAlarmRemarkModal: function () {
                        return {workArea : $scope.statementConfig.workCenters};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                //代表在modal里勾选的数组
                UtilsService.log(angular.toJson(selectedItem,true));
                $scope.statementConfig.alarmRemarkCommentCode = selectedItem.commentCode.join(",");
                $scope.statementConfig.alarmRemarkCommentDesc = selectedItem.commentDesc.join(",");
                UtilsService.log("desc"+$scope.statementConfig.alarmRemarkCommentDesc);
                UtilsService.log("code"+$scope.statementConfig.alarmRemarkCommentCode);
                $scope.config.showSelectAlarm = false;
                //$scope.deleteAll('alarmRemark');
                //$scope.workCenterChanged('T');
            }, function () {
            });
        };
        $scope.deleteAll = function(item){
            if(item == 'area'){
                $scope.statementConfig.currentWorkCenter = [];
                $scope.statementConfig.currentWorkCenterDesc = [];
                $scope.statementConfig.currentLine = [];
                $scope.statementConfig.currentLineDesc = [];
                $scope.statementConfig.deviceNums = [];
                $scope.statementConfig.currentDeviceNum = null;
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.showSelectArea = true;
                $scope.config.showSelectLine = true;
                $scope.config.deviceCode = true;
            }
            if(item == 'line'){
                $scope.statementConfig.currentLine = [];
                $scope.statementConfig.currentLineDesc = [];
                $scope.statementConfig.deviceNums = [];
                $scope.statementConfig.currentDeviceNum = null;
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.showSelectLine = true;
                $scope.config.deviceCode = true;
                //$scope.lineChanged('S');
            }
            if(item == 'deviceType'){
                $scope.statementConfig.currentDeviceType = [];
                $scope.statementConfig.currentDeviceTypeDesc = [];
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.deviceType = true;
                $scope.config.deviceCode = true;
                //$scope.workCenterChanged('T');

            }
            if(item == 'deviceCode'){
                $scope.statementConfig.currentDeviceNum = [];
                $scope.config.deviceCode = true;
            }

            if(item == 'alarmRemark'){
                $scope.statementConfig.alarmRemark = "";
                $scope.statementConfig.alarmRemarkCommentDesc = "";
                $scope.statementConfig.alarmRemarkCommentCode = "";
                $scope.config.showSelectAlarm = true;
            }

        }
        //设备报警明细
        $scope.gridDeviceAlertStatement = {
            enablePagination: true,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,

            useExternalPagination: true,
            paginationCurrentPage: 1,
            totalItems: 0,
            enableCellEdit:false,
            data:null,
            columnDefs:[
                {
                    name:"strDate",displayName:'日期',minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"shift",displayName:'班次',minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"prdUserId",displayName:'生产操作员',minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resource",displayName:'设备编码',minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceTypeDesc",
                    displayName:'设备类型',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceDesc",displayName:'设备名称',minWidth:250,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"strStartTime",displayName:'开始时间',minWidth:170,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"strEndTime",displayName:'结束时间',minWidth:170,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"duration",displayName:'时长（min）',minWidth:100,cellClass:"text-align-right",
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"alertInfo",displayName:'报警信息',minWidth:200,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"strAlertDateTime",displayName:'报警时间',minWidth:200,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },//add by panxu
                {
                    name:"alertCommentDescription",displayName:'备注',minWidth:200,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceState",displayName:'设备状态',minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceAddr",displayName:'拉线',minWidth:140,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"asset",displayName:'资产号',minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    visible:true,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                }
            ],
            onRegisterApi:__requestDeviceAlertApi,
        };
        //设备状态明细

        $scope.gridDeviceStatusStatement = {
            enablePagination: true,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,

            useExternalPagination: true,
            paginationCurrentPage: 1,
            totalItems: 0,
            enableCellEdit:false,

            columnDefs:[
                {
                    name:"strDate",displayName:'日期',minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"shift",displayName:'班次',minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"prdUserId",displayName:'生产操作员',minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resource",displayName:'设备编码',minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceTypeDesc",
                    displayName:'设备类型',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"asset",displayName:'资产号',minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceDesc",displayName:'设备名称',minWidth:250,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceAddr",displayName:'拉线',minWidth:140,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"strStartTime",displayName:'开始时间',minWidth:170,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"strEndTime",displayName:'结束时间',minWidth:170,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceState",displayName:'设备状态',minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"duration",displayName:'时长（min）',minWidth:100,cellClass:"text-align-right",enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"model",displayName:'MODEL',minWidth:100,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"item",displayName:'物料编码',minWidth:200,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                }

            ],
            onRegisterApi:__requestDeviceStatusApi,
        };

        //设备实时产量

        $scope.gridOutputStatement = {
            enablePagination: true,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,
            useExternalPagination: true,
            paginationCurrentPage: 1,
            totalItems: 0,
            enableCellEdit:false,

            columnDefs:[
                {
                    name:"strDate",displayName:'日期',minWidth:100,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"prdUserId",displayName:'生产操作员',minWidth:100,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resource",displayName:'设备编码',minWidth:100,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceTypeDesc",
                    displayName:'设备类型',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"asset",displayName:'资产号',minWidth:100,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceDesc",displayName:'设备名称',minWidth:250,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceAddr",displayName:'拉线',minWidth:140,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"strStartTime",displayName:'开始时间',minWidth:170,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"strEndTime",displayName:'结束时间',minWidth:170,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"duration",displayName:'时长（min）',minWidth:100,cellClass:"text-align-right",enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"item",displayName:'物料编码',minWidth:200,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"qtyInput",displayName:'总投入',minWidth:100,cellClass:"text-align-right",enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"qtyYield",displayName:'机判良品数',minWidth:100,cellClass:"text-align-right",enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"qtyScrap",displayName:'机判坏品数',minWidth:100,cellClass:"text-align-right",enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"qtyTotal",displayName:'总产出',minWidth:100,cellClass:"text-align-right",enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },

            ],
            onRegisterApi:__requestOutputApi,
        };

        //设备实时停机原因

        $scope.gridDeviceRtReasonCodeStatement = {
            enablePagination: true,
            paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes,
            paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize,

            useExternalPagination: true,
            paginationCurrentPage: 1,
            totalItems: 0,
            enableCellEdit:false,

            columnDefs:[
                {
                    name:"strDate",displayName:'日期',minWidth:100,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"shift",displayName:'班次',minWidth:100,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"prdUserId",displayName:'生产操作员',minWidth:100,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resource",displayName:'设备编码',minWidth:100,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceTypeDesc",
                    displayName:'设备类型',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"asset",displayName:'资产号',minWidth:100,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceAddr",displayName:'拉线',minWidth:140,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceDesc",displayName:'设备名称',minWidth:250,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"strStartTime",displayName:'停机开始时间',minWidth:170,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"strEndTime",displayName:'停机结束时间',minWidth:170,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"resourceState",displayName:'设备状态',minWidth:100,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"duration",displayName:'停机时长（min）',minWidth:150,cellClass:"text-align-right",enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"strReasonCodeTime",displayName:'点选时间',minWidth:170,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"reasonCodeDescription",displayName:'点选原因',minWidth:200,enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                }

            ] ,
            onRegisterApi:_requestDeviceRtReasonCodeApi,
        };
        $scope.init = function(){
            $scope.config.currentThemeGrid = {id:'T',name:'设备状态明细报表'};
            $scope.statementConfig.currentWorkCenter = [];
            $scope.statementConfig.currentWorkCenterDesc = [];
            $scope.statementConfig.currentLine = [];
            $scope.statementConfig.currentLineDesc = [];
            $scope.statementConfig.currentDeviceNum = null;
            $scope.statementConfig.multipleCurrentDeviceNum = [];
            $scope.statementConfig.currentDeviceType = [];
            $scope.statementConfig.currentDeviceTypeDesc = [];
            $scope.statementConfig.startDate = UtilsService.resourceSDStartFommateDateTimeShow(new Date());
            $scope.statementConfig.endDate = UtilsService.resourceSDEndFommateDateTimeShow(new Date());
            $scope.statementConfig.shiftsChange = [{shift : '',description : '请选择',descriptionNew : '请选择'},{shift : 'M',description : '早班',descriptionNew : '早班 代码:M'},
                {shift : 'E',description : '晚班',descriptionNew : '晚班 代码:E'}];
        };

        $scope.init();

        $scope.loadOtherReport=function(){

            if(($scope.statementConfig.currentWorkCenter == ''||$scope.statementConfig.currentWorkCenter ==null)
                &&($scope.statementConfig.currentLine== ''||$scope.statementConfig.currentLine==null)
                &&($scope.statementConfig.currentDeviceType== ''||$scope.statementConfig.currentDeviceType== null)
                &&($scope.statementConfig.currentDeviceNum==''||$scope.statementConfig.currentDeviceNum==null)
                &&($scope.statementConfig.currentShift==null||$scope.statementConfig.currentShift.description=='请选择')){
                $scope.addAlert('','查询条件不可以为空');
                return;
            }

            var startDate=new Date($scope.statementConfig.startDate);
            var endDate=new Date($scope.statementConfig.endDate);
            if(startDate>endDate){
                $scope.addAlert('danger','结束日期必须大于开始日期');
                $scope.hideBodyModel();
                return;
            }
            if(endDate-startDate>92*24*60*60*1000){
                $scope.addAlert('danger','开始日期和结束日期的间隔必须小于92天');
                $scope.hideBodyModel();
                return;
            }
            /*if($scope.statementConfig.currentWorkCenterDesc == null || $scope.statementConfig.currentWorkCenterDesc == ''){
                $scope.addAlert('danger','请先选择生产区域');
                $scope.hideBodyModel();
                return;
            }*/
            var currReport=$scope.config.currentThemeGrid.id;
            UtilsService.log("当前报表id:"+currReport);
            if(currReport=="A"){
                $scope.showBodyModel("正在加载数据,请稍后...");
                __requestDeviceAlertDatas();
                __requestResourceCount("A");
            }else if(currReport=="H"){
                $scope.showBodyModel("正在加载数据,请稍后...");
                __requestDeviceHaltDatas();
                __requestResourceCount("H");
            }else if(currReport=="O"){
                $scope.showBodyModel("正在加载数据,请稍后...");
                __requestOutputDatas();
                __requestResourceCount("O");
            }else if(currReport=="R"){
                $scope.showBodyModel("正在加载数据,请稍后...");
                __requestDeviceRtReasonCodeDatas();
                __requestResourceCount("R");
            }else if(currReport=="S"){
                $scope.showBodyModel("正在加载数据,请稍后...");
                UtilsService.log("在这里调用了设备状态明细报表111111");
                __requestDeviceStatusDatas();
                __requestResourceCount("S");
            }else if(currReport=="T"){
                $scope.showBodyModel("正在加载数据,请稍后...");
                UtilsService.log("在这里调用了设备状态明细报表333333");
                __requestDeviceStatusDatas();
                __requestResourceCount("S");
            }
        }
        $scope.queryDatas = function(){

            if(($scope.statementConfig.currentWorkCenter == ''||$scope.statementConfig.currentWorkCenter ==null)
                &&($scope.statementConfig.currentLine== ''||$scope.statementConfig.currentLine==null)
                &&($scope.statementConfig.currentDeviceType== ''||$scope.statementConfig.currentDeviceType== null)
                &&($scope.statementConfig.currentDeviceNum==''||$scope.statementConfig.currentDeviceNum==null)
                &&($scope.statementConfig.currentShift==null||$scope.statementConfig.currentShift.description=='请选择')){
                $scope.addAlert('','查询条件不可以为空');
                return;
            }

        	$scope.gridDeviceStatusStatement.paginationCurrentPage = 1;
        	$scope.gridDeviceHaltStatement.paginationCurrentPage = 1;
        	$scope.gridDeviceAlertStatement.paginationCurrentPage = 1;
        	$scope.gridOutputStatement.paginationCurrentPage = 1;
        	$scope.gridDeviceRtReasonCodeStatement.paginationCurrentPage=1;
        	
        	var startDate=new Date($scope.statementConfig.startDate);
        	var endDate=new Date($scope.statementConfig.endDate);
        	if(startDate>endDate){
        		$scope.addAlert('danger','结束日期必须大于开始日期');
        		$scope.hideBodyModel();
                return;
            }
            if(endDate-startDate>92*24*60*60*1000){
                $scope.addAlert('danger','开始日期和结束日期的间隔必须小于92天');
                $scope.hideBodyModel();
                return;
            }
            var currReport=$scope.config.currentThemeGrid.id;
            UtilsService.log("当前报表id:"+currReport);
            __requestDeviceCount();
            if(currReport=="A"){
                $scope.showBodyModel("正在加载数据,请稍后...");
                __requestDeviceAlertDatas();
                __requestResourceCount("A");
            }if(currReport=="H"){
                $scope.showBodyModel("正在加载数据,请稍后...");
                __requestDeviceHaltDatas();
                __requestResourceCount("H");
            }else if(currReport=="O"){
                $scope.showBodyModel("正在加载数据,请稍后...");
                __requestOutputDatas();
                __requestResourceCount("O");
            }else if(currReport=="R"){
                $scope.showBodyModel("正在加载数据,请稍后...");
                __requestDeviceRtReasonCodeDatas();
                __requestResourceCount("R");
            }else if(currReport=="S"){
                $scope.showBodyModel("正在加载数据,请稍后...");
                UtilsService.log("在这里调用了设备状态明细报表3333333");
                __requestDeviceStatusDatas();
                __requestResourceCount("S");
            }else if(currReport=="T"){
                $scope.showBodyModel("正在加载数据,请稍后...");
                UtilsService.log("在这里调用了设备状态明细报表3333333");
                __requestDeviceStatusDatas();
                __requestResourceCount("S");
            }

        };
        $scope.downLoad = function(){
        	var params = {
                    workCenter: $scope.statementConfig.currentWorkCenter,//生产区域
                    line:$scope.statementConfig.currentLine,
                    startDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.startDate)),
                    endDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.endDate)),
                    type:$scope.config.currentThemeGrid.name
                    /*resourceType : $scope.statementConfig.currentDeviceType?$scope.statementConfig.currentDeviceType.resourceType:'',
                     resrce : $scope.statementConfig.currentDeviceNum?$scope.statementConfig.currentDeviceNum.resrce:'',
                     shift : $scope.statementConfig.currentShift?$scope.statementConfig.currentShift.shift:''*/
                };
                //设备类型
                if($scope.statementConfig.currentDeviceType){
                    params.resourceType = $scope.statementConfig.currentDeviceType;
                }
                //设备编码
                if($scope.statementConfig.currentDeviceNum){
                    params.resrce = $scope.statementConfig.currentDeviceNum;
                }
                //班次
                if($scope.statementConfig.currentShift){
                    params.shift = $scope.statementConfig.currentShift.shift;
                }
                $scope.showBodyModel("正在导出数据,请稍后...");
                var url = statementService.ReportPull(params);
                url = url.replace(/undefined/g,"").replace(/null/g,"");
                url = HttpAppService.handleCommenUrl(url);
                $http({
                    url: url,
                    method: "GET",
                    headers: {
                        'Content-type': 'application/json'
                    },
                    responseType: 'arraybuffer'
                }).success(function (data, status, headers, config) {
                    var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                    var objectUrl = URL.createObjectURL(blob);
                    window.location.href=objectUrl;
                    $scope.hideBodyModel();
                }).error(function (data, status, headers, config) {
                });
        };

        function __requestDeviceHaltApi(gridApi){
            $scope.config.gridApi = gridApi;
            $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                __requestDeviceHaltDatas(currentPage, pageSize);
                return true;
            }, $scope);
        };
        function __requestDeviceStatusApi(gridApi){
            $scope.config.gridApi = gridApi;
            $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                __requestDeviceStatusDatas(currentPage, pageSize);
                return true;
            }, $scope);
        };
        function __requestDeviceAlertApi(gridApi){
            $scope.config.gridApi = gridApi;
            $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                __requestDeviceAlertDatas(currentPage, pageSize);
                return true;
            }, $scope);
        };
        function __requestOutputApi(gridApi){
            $scope.config.gridApi = gridApi;
            $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                __requestOutputDatas(currentPage, pageSize);
                return true;
            }, $scope);
        };
        function _requestDeviceRtReasonCodeApi(gridApi){
            $scope.config.gridApi = gridApi;
            $scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
                __requestDeviceRtReasonCodeDatas(currentPage, pageSize);
                return true;
            }, $scope);
        };

        function __arrayToString(array){
            var string = [];
            for(var i=0;i<array.length;i++){
                string[i] = array[i].workCenter;
            }
            return string.toString();
        };

        function __requestDeviceCount(){
            var params = {
                workCenter: $scope.statementConfig.currentWorkCenter,//生产区域
                line:$scope.statementConfig.currentLine,
                startDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.startDate)),
                endDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.endDate)),
                type:$scope.config.currentThemeGrid.name,
            };
            //设备类型
            if($scope.statementConfig.currentDeviceType){
                params.resourceType = $scope.statementConfig.currentDeviceType;
            }
            //设备编码
            if($scope.statementConfig.currentDeviceNum){
                params.resrce = $scope.statementConfig.currentDeviceNum;
            }
            //班次
            if($scope.statementConfig.currentShift){
                params.shift = $scope.statementConfig.currentShift.shift;
            }

            statementService.ListCount(params).then(function (resultDatas){
            	$scope.hideBodyModel();

                $scope.config.totalHalt=resultDatas.response.haltCount;
                $scope.config.totalTime=resultDatas.response.duration;
                UtilsService.log(angular.toJson(resultDatas,true));
            }, function (resultDatas){
                $scope.gridDeviceHaltStatement.totalItems = 0;
                $scope.gridDeviceAlertStatement.totalItems = 0;
                $scope.gridDeviceStatusStatement.totalItems = 0;
                $scope.gridOutputStatement.totalItems = 0;
                $scope.gridDeviceRtReasonCodeStatement.totalItems = 0;
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });
        };

        function __requestResourceCount(currReport){
            var params = {
                workCenter: $scope.statementConfig.currentWorkCenter,//生产区域
                line:$scope.statementConfig.currentLine,
                startDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.startDate)),
                endDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.endDate)),
                type:currReport,
                commentCode:$scope.statementConfig.alarmRemarkCommentCode
            };
            //设备类型
            if($scope.statementConfig.currentDeviceType){
                params.resourceType = $scope.statementConfig.currentDeviceType;
            }
            //设备编码
            if($scope.statementConfig.currentDeviceNum){
                params.resrce = $scope.statementConfig.currentDeviceNum;
            }
            //班次
            if($scope.statementConfig.currentShift){
                params.shift = $scope.statementConfig.currentShift.shift;
            }
            statementService.listTotal(params).then(function (resultDatas){
                $scope.hideBodyModel();
                if(currReport=="A"){
                    $scope.config.alertCount=resultDatas.response.alertCount;
                    $scope.gridDeviceAlertStatement.totalItems = resultDatas.response.alertCount;
                }else if(currReport=="H"){
                    $scope.config.reasonCodeCount=resultDatas.response.reasonCodeCount;
                    $scope.gridDeviceHaltStatement.totalItems = resultDatas.response.reasonCodeCount;
                }
                else if(currReport=="O"){
                    $scope.config.outputCount=resultDatas.response.outputCount;
                    $scope.gridOutputStatement.totalItems = resultDatas.response.outputCount;
                }
                else if(currReport=="R"){
                    $scope.config.rtReasonCodeCount=resultDatas.response.rtReasonCodeCount;
                    $scope.gridDeviceRtReasonCodeStatement.totalItems = resultDatas.response.rtReasonCodeCount;
                }
                else if(currReport=="S"){
                    $scope.config.itemCount=resultDatas.response.itemCount;
                    $scope.gridDeviceStatusStatement.totalItems = resultDatas.response.itemCount;
                }
                else if(currReport=="T"){
                    $scope.config.itemCount=resultDatas.response.itemCount;
                    $scope.gridDeviceStatusStatement.totalItems = resultDatas.response.itemCount;
                }
            }, function (resultDatas){
                $scope.gridDeviceHaltStatement.totalItems = 0;
                $scope.gridDeviceAlertStatement.totalItems = 0;
                $scope.gridDeviceStatusStatement.totalItems = 0;
                $scope.gridOutputStatement.totalItems = 0;
                $scope.gridDeviceRtReasonCodeStatement.totalItems = 0;
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });
        }
        function __requestDeviceStatusDatas(pageIndex, pageCount){
        	
            var pageIndexX = pageIndex,
                pageCountX = pageCount;
            if(angular.isUndefined(pageIndexX) || pageIndexX == null || pageIndexX <= 0){
                pageIndexX = 1;
            }
            if(angular.isUndefined(pageCountX) || pageCountX == null || pageCountX <= 0){
                pageCountX = $scope.gridDeviceStatusStatement.paginationPageSize;
            }
            var params = {
                workCenter: $scope.statementConfig.currentWorkCenter,//生产区域
                line:$scope.statementConfig.currentLine,
                startDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.startDate)),
                endDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.endDate)),
                type:'S',
                pageIndex: pageIndexX,
                pageCount: pageCountX
            };
            //设备类型
            if($scope.statementConfig.currentDeviceType){
                params.resourceType = $scope.statementConfig.currentDeviceType;
            }
            //设备编码
            if($scope.statementConfig.currentDeviceNum){
                params.resrce = $scope.statementConfig.currentDeviceNum;
            }
            //班次
            if($scope.statementConfig.currentShift){
                params.shift = $scope.statementConfig.currentShift.shift;
            }
            UtilsService.log("调用了一次设备状态明细报表");
            //设备状态明细
            statementService.DeviceStatusList(params).then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridDeviceStatusStatement.data = resultDatas.response;
                    return;
                }else{
                    $scope.gridDeviceStatusStatement.data = [];
                    $scope.addAlert('','暂无数据');
                }
            }, function (resultDatas){
                $scope.hideBodyModel();
                $scope.gridDeviceStatusStatement.data = [];
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });
        };
        function __requestDeviceAlertDatas(pageIndex, pageCount){
            UtilsService.log("执行次数");
            var pageIndexX = pageIndex,
                pageCountX = pageCount;
            if(angular.isUndefined(pageIndexX) || pageIndexX == null || pageIndexX <= 0){
                pageIndexX = 1;
            }
            if(angular.isUndefined(pageCountX) || pageCountX == null || pageCountX <= 0){
                pageCountX = $scope.gridDeviceAlertStatement.paginationPageSize;
            }
            var params = {
                workCenter: $scope.statementConfig.currentWorkCenter,//生产区域
                line:$scope.statementConfig.currentLine,
                startDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.startDate)),
                endDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.endDate)),
                type:'A',
                pageIndex: pageIndexX,
                pageCount: pageCountX,
                alarmRemarkCode:$scope.statementConfig.alarmRemarkCommentCode
            };
            UtilsService.log("调用方法前："+$scope.statementConfig.alarmRemarkCommentCode);
            //设备类型
            if($scope.statementConfig.currentDeviceType){
                params.resourceType = $scope.statementConfig.currentDeviceType;
            }
            //设备编码
            if($scope.statementConfig.currentDeviceNum){
                params.resrce = $scope.statementConfig.currentDeviceNum;
            }
            //班次
            if($scope.statementConfig.currentShift){
                params.shift = $scope.statementConfig.currentShift.shift;
            }
            //设备报警明细
            statementService.DeviceAlertList(params).then(function (resultDatas){
                $scope.hideBodyModel();
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridDeviceAlertStatement.data = resultDatas.response;
                    UtilsService.log(angular.toJson(resultDatas.response,true));
                    return;
                }else{
                    $scope.gridDeviceAlertStatement.data = [];
                    $scope.addAlert('','暂无数据');
                }
            }, function (resultDatas){
                $scope.hideBodyModel();
                $scope.gridDeviceAlertStatement.data = [];
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });
        };

        function __requestDeviceHaltDatas(pageIndex, pageCount){
            var pageIndexX = pageIndex,
                pageCountX = pageCount;
            if(angular.isUndefined(pageIndexX) || pageIndexX == null || pageIndexX <= 0){
                pageIndexX = 1;
            }
            if(angular.isUndefined(pageCountX) || pageCountX == null || pageCountX <= 0){
                pageCountX = $scope.gridDeviceHaltStatement.paginationPageSize;
            }

            var params = {
                workCenter: $scope.statementConfig.currentWorkCenter,//生产区域
                line:$scope.statementConfig.currentLine,
                startDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.startDate)),
                endDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.endDate)),
                type:'H',
                pageIndex: pageIndexX,
                pageCount: pageCountX
            };
            //设备类型
            if($scope.statementConfig.currentDeviceType){
                params.resourceType = $scope.statementConfig.currentDeviceType;
            }
            //设备编码
            if($scope.statementConfig.currentDeviceNum){
                params.resrce = $scope.statementConfig.currentDeviceNum;
            }
            //班次
            if($scope.statementConfig.currentShift){
                params.shift = $scope.statementConfig.currentShift.shift;
            }
            //设备停机原因
            statementService.DeviceHaltList(params).then(function (resultDatas){
                $scope.hideBodyModel();
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridDeviceHaltStatement.data = resultDatas.response;
                    return;
                }else{
                    $scope.gridDeviceHaltStatement.data = [];
                    $scope.addAlert('','暂无数据');
                }
            }, function (resultDatas){
                $scope.hideBodyModel();
                $scope.gridDeviceHaltStatement.data = [];
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });
        };
        function __requestOutputDatas(pageIndex, pageCount){
            var pageIndexX = pageIndex,
                pageCountX = pageCount;
            if(angular.isUndefined(pageIndexX) || pageIndexX == null || pageIndexX <= 0){
                pageIndexX = 1;
            }
            if(angular.isUndefined(pageCountX) || pageCountX == null || pageCountX <= 0){
                pageCountX = $scope.gridOutputStatement.paginationPageSize;
            }
            var params = {
                workCenter: $scope.statementConfig.currentWorkCenter,//生产区域
                line:$scope.statementConfig.currentLine,
                startDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.startDate)),
                endDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.endDate)),
                type:'O',
                pageIndex: pageIndexX,
                pageCount: pageCountX
            };
            //设备类型
            if($scope.statementConfig.currentDeviceType){
                params.resourceType = $scope.statementConfig.currentDeviceType;
            }
            //设备编码
            if($scope.statementConfig.currentDeviceNum){
                params.resrce = $scope.statementConfig.currentDeviceNum;
            }
            //班次
            if($scope.statementConfig.currentShift){
                params.shift = $scope.statementConfig.currentShift.shift;
            }
            //设备实时产出
            statementService.DeviceOutputList(params).then(function (resultDatas){
            	$scope.hideBodyModel();
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridOutputStatement.data = resultDatas.response;
                    return;
                }else{
                    $scope.gridOutputStatement.data = [];
                    $scope.addAlert('','暂无数据');
                }
            }, function (resultDatas){
                $scope.hideBodyModel();
                $scope.gridOutputStatement.data = [];
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });
        };
        // 实时停机原因
        function __requestDeviceRtReasonCodeDatas(pageIndex, pageCount){
            var pageIndexX = pageIndex,
                pageCountX = pageCount;
            if(angular.isUndefined(pageIndexX) || pageIndexX == null || pageIndexX <= 0){
                pageIndexX = 1;
            }
            if(angular.isUndefined(pageCountX) || pageCountX == null || pageCountX <= 0){
                pageCountX = $scope.gridDeviceRtReasonCodeStatement.paginationPageSize;
            }
            var params = {
                workCenter: $scope.statementConfig.currentWorkCenter,//生产区域
                line:$scope.statementConfig.currentLine,
                startDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.startDate)),
                endDate: UtilsService.serverFommateDateTime(new Date($scope.statementConfig.endDate)),
                type:'R',
                pageIndex: pageIndexX,
                pageCount: pageCountX
            };
            //设备类型
            if($scope.statementConfig.currentDeviceType){
                params.resourceType = $scope.statementConfig.currentDeviceType;
            }
            //设备编码
            if($scope.statementConfig.currentDeviceNum){
                params.resrce = $scope.statementConfig.currentDeviceNum;
            }
            //班次
            if($scope.statementConfig.currentShift){
                params.shift = $scope.statementConfig.currentShift.shift;
            }
            //设备实时停机原因
            statementService.DeviceRtReasonCodeList(params).then(function (resultDatas){
                $scope.hideBodyModel();
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridDeviceRtReasonCodeStatement.data = resultDatas.response;
                    return;
                }else{
                    $scope.gridDeviceRtReasonCodeStatement.data = [];
                    $scope.addAlert('','暂无数据');
                }
            }, function (resultDatas){
                $scope.hideBodyModel();
                $scope.gridDeviceRtReasonCodeStatement.data = [];
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });
        };

        function __downLoad(){
            
        };

        //控制显示隐藏列
        $scope.showHideCol=function () {
            if($scope.config.currentThemeGrid.id == "S" || !$scope.config.currentThemeGrid.id){
                  for(var i=0;i<$scope.gridDeviceStatusStatement.columnDefs.length;i++) {
                      if (!$scope.gridDeviceStatusStatement.columnDefs[i].visible) {
                          $scope.gridDeviceStatusStatement.columnDefs[i].visible = true;
                      }
                  }
            }
            else if($scope.config.currentThemeGrid.id == "A"){
                for(var i=0;i<$scope.gridDeviceAlertStatement.columnDefs.length;i++)
                {
                    if(!$scope.gridDeviceAlertStatement.columnDefs[i].visible){
                        $scope.gridDeviceAlertStatement.columnDefs[i].visible=true;
                    }
                }
            }
            else if($scope.config.currentThemeGrid.id == "H"){
                for(var i=0;i<$scope.gridDeviceHaltStatement.columnDefs.length;i++)
                {
                    if(!$scope.gridDeviceHaltStatement.columnDefs[i].visible){
                        $scope.gridDeviceHaltStatement.columnDefs[i].visible=true;
                    }
                }
            }
            else if($scope.config.currentThemeGrid.id == "O"){
                for(var i=0;i<$scope.gridOutputStatement.columnDefs.length;i++)
                {
                    if(!$scope.gridOutputStatement.columnDefs[i].visible){
                        $scope.gridOutputStatement.columnDefs[i].visible=true;
                    }                }
            }
            else if($scope.config.currentThemeGrid.id == "R"){
                for(var i=0;i<$scope.gridDeviceRtReasonCodeStatement.columnDefs.length;i++)
                {
                    if(!$scope.gridDeviceRtReasonCodeStatement.columnDefs[i].visible){
                        $scope.gridDeviceRtReasonCodeStatement.columnDefs[i].visible=true;
                    }                }
            }
            else{
                for(var i=0;i<$scope.gridDeviceStatusStatement.columnDefs.length;i++) {
                    if (!$scope.gridDeviceStatusStatement.columnDefs[i].visible) {
                        $scope.gridDeviceStatusStatement.columnDefs[i].visible = true;
                    }
                }
            }
            $scope.config.gridApi.core.refresh();
        };
    }]);