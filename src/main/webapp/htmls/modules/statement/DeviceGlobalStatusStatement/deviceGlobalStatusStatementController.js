statementModule.controller('DeviceGlobalStatusController', ['$scope','$http','HttpAppService','$timeout',
		'UtilsService','uiGridConstants','$uibModal','$echarts','$interval','statementService',
		function($scope, $http, HttpAppService, $timeout, UtilsService, uiGridConstants, $uibModal,
				$echarts, $interval, statementService) {
			//var c=null;
			//UtilsService.log("c:"+ c.a);
			//var f="123";
			//UtilsService.log("f:"+typeof f);
			$scope.config = {
				title : '设备宏观状态看板（时间范围：8:00-当前）',
				gridApi: null,
				byType : "W",
				workArea : "",
				workAreaName : "",
				workLineName : "",
				workLine : "",
				resourceType : "",
				resourceTypeDesc : "",
				startDate : UtilsService.serverFommateDateShow(new Date()),
				endDate : UtilsService.serverFommateDateShow(new Date()),
				startDateMouth : UtilsService.serverFormateDateMouth(new Date()),
				endDateMouth : UtilsService.serverFormateDateMouth(new Date()),

				selectType : true,
				type : 'RT',
				dateType : "Y",

				// 放大镜是否显示
				showSelectArea : true,
				showSelectLine : true,
				deviceType : true,
				deviceCode : true,
				factoryCode:true,
				modelIcon : true,
				showSelectPrd : true,


				// 是否必选,红色星号
				showWorkArea : false,
				showLineArea : false,
				showResourceType : false,
				showResource : false,
				showFactory : true,//
				showCount : false,

				selectYear : true,
				selectMouth : true,
				selectWeek : false,
				selectDay : false,
				selectShift : false,

				queryChartShow : false,

				queryGridAreaShow : false,
				queryGridLineShow : false,
				queryGridDeviceShow : false,
				queryGridAreaCodeShow : false,
				addInfosShow : false,

				// 是否禁用查询按钮
				queryBtn : true,
				downLoadBtn : true,
				byDateDisplayName : '',

				showOeeEChart : false,
				showAEChart : false,
				showPEChart : false,
				showQEChart : false,
				showEcharts : false,

				// 报表
				outPutStatementInstance : null,
				dataEcharts : null,
				dateTypeEcharts : null,

				selectName : "",
				currentSelected : "",
				dataSelected : "",
				selectedType : "",
				itemCodeSelectedCopy : [],
				dataNum : "",
				dimension:1,
				deviceGlobalList:[],
				testShowStatus:false
			};

			//把那些基本条件和信息copy下来
			var copyDataInfo={
				dimension:1,
				sites :  "",
				workCenter : "",
				lineArea : "",
				resourceType :""
			};

			$scope.init = function() {
				$scope.statementConfig.currentWorkCenter = [];
				$scope.statementConfig.currentWorkCenterDesc = [];
				$scope.statementConfig.currentLine = [];
				$scope.statementConfig.currentLineDesc = [];
				$scope.statementConfig.currentDeviceNum = null;
				$scope.statementConfig.multipleCurrentDeviceNum = [];
				$scope.statementConfig.currentDeviceType = [];
				$scope.statementConfig.currentDeviceTypeDesc = [];
				$scope.statementConfig.factoryContentDesc = "";
				$scope.statementConfig.factoryContentCode = "";
				$scope.config.model='';
				$scope.config.prd='';
				$scope.statementConfig.shiftsChange = [ {
					shift : '',
					description : '请选择',
					descriptionNew : '请选择'
				}, {
					shift : 'M',
					description : '早班',
					descriptionNew : '早班 代码:M'
				}, {
					shift : 'E',
					description : '晚班',
					descriptionNew : '晚班 代码:E'
				} ];
				var year = new Date().getFullYear();
				$scope.statementConfig.startYears = [];
				for (var i = 2011; i < 2021; i++) {
					var obj = {
						id:i,
						year : i+'-01-01',
						description : i + '年'
					};
					$scope.statementConfig.startYears.push(obj);
				}
				$scope.statementConfig.startYear = {
						id:1,
						year : year+'-01-01',
						description : year + '年'
					};
				

				$scope.statementConfig.endYear = {
					id:2,
					year : year+'-12-31',
					description : year + '年'
				};
				$scope.statementConfig.endYears = [];
				for (var i = year - 5; i < year + 5; i++) {
					var obj = {
						id:i,
						year : i+'-12-31',
						description : i + '年'
					};
					$scope.statementConfig.endYears.push(obj);
				}

				$scope.statementConfig.currentWorkCenter=window.localStorage.currentWorkCenter ? window.localStorage.currentWorkCenter:$scope.statementConfig.currentWorkCenter;
				$scope.statementConfig.currentWorkCenterDesc=window.localStorage.currentWorkCenterDesc ? window.localStorage.currentWorkCenterDesc:$scope.statementConfig.currentWorkCenterDesc;
				$scope.statementConfig.currentLineDesc=window.localStorage.currentLineDesc ? window.localStorage.currentLineDesc:$scope.statementConfig.currentLineDesc;
				$scope.statementConfig.currentLine=window.localStorage.currentLine ? window.localStorage.currentLine:$scope.statementConfig.currentLine;
				$scope.statementConfig.currentDeviceType=window.localStorage.currentDeviceType ? window.localStorage.currentDeviceType:$scope.statementConfig.currentDeviceType;
				$scope.statementConfig.currentDeviceTypeDesc=window.localStorage.currentDeviceTypeDesc ? window.localStorage.currentDeviceTypeDesc:$scope.statementConfig.currentDeviceTypeDesc;
				$scope.statementConfig.factoryContentCode=window.localStorage.factoryContentCode ? window.localStorage.factoryContentCode:$scope.statementConfig.factoryContentCode;
				$scope.statementConfig.factoryContentDesc=window.localStorage.factoryContentDesc ? window.localStorage.factoryContentDesc:$scope.statementConfig.factoryContentDesc;;


				if(!angular.isUndefined(window.localStorage.showFactory)){
					if(window.localStorage.showFactory === "true")
					{
						$scope.config.showFactory=true;
					}else{
						$scope.config.showFactory=false;
					}
				}

				if(!angular.isUndefined(window.localStorage.showWorkArea)){
					if(window.localStorage.showWorkArea === "true")
					{
						$scope.config.showWorkArea=true;
					}else{
						$scope.config.showWorkArea=false;
					}
				}

				if(!angular.isUndefined(window.localStorage.showLineArea)){
					if(window.localStorage.showLineArea === "true")
					{
						$scope.config.showLineArea=true;
					}else{
						$scope.config.showLineArea=false;
					}
				}

				if(!angular.isUndefined(window.localStorage.showResourceType)){
					if(window.localStorage.showResourceType === "true")
					{
						$scope.config.showResourceType=true;
					}else{
						$scope.config.showResourceType=false;
					}
				}

				if(!angular.isUndefined(window.localStorage.queryBtn)){
					if(window.localStorage.queryBtn === "true")
					{
						$scope.config.queryBtn=true;
					}else{
						$scope.config.queryBtn=false;
					}
				}

				if(!angular.isUndefined(window.localStorage.showSelectArea)){
					if(window.localStorage.showSelectArea === "true")
					{
						$scope.config.showSelectArea=true;
					}else{
						$scope.config.showSelectArea=false;
					}
				}

				if(!angular.isUndefined(window.localStorage.showSelectLine)){
					if(window.localStorage.showSelectLine === "true")
					{
						$scope.config.showSelectLine=true;
					}else{
						$scope.config.showSelectLine=false;
					}
				}

				if(!angular.isUndefined(window.localStorage.deviceType)){
					if(window.localStorage.deviceType === "true")
					{
						$scope.config.deviceType=true;
					}else{
						$scope.config.deviceType=false;
					}
				}

				if(!angular.isUndefined(window.localStorage.factoryCode)){
					if(window.localStorage.factoryCode === "true")
					{
						$scope.config.factoryCode=true;
					}else{
						$scope.config.factoryCode=false;
					}
				}

				//dimension维度
				if(!angular.isUndefined(window.localStorage.dimension)){
					$scope.config.dimension=window.localStorage.dimension;
				}

				if($scope.config.dimension == 1)
				{
					document.getElementById('inlineRadio4').checked=true;
				}else if($scope.config.dimension == 2)
				{
					document.getElementById('inlineRadio1').checked=true;
				}else if($scope.config.dimension == 3)
				{
					document.getElementById('inlineRadio2').checked=true;
				}else if($scope.config.dimension == 4)
				{
					document.getElementById('inlineRadio3').checked=true;
				}
			};

			$scope.init();

			// 按照生产区域查询
			$scope.OEETrendAnalysisGridWorkAreaStatement = {
				data : [],
				enableCellEdit : false,
				columnDefs : [ 
				/*{
					name : "shiftCode",
					displayName : '班次代码',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				},*/
				{
					name : "byTime",
					displayName : '班次',
					minWidth : 100,
					visible:false,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "workArea",
					displayName : '生产区域描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "workCenter",
					displayName : '拉线描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrceType",
					displayName : '设备类型描述',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrce",
					displayName : '设备编码',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "asset",
					displayName : '资产号',
					minWidth : 100,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "resrceDescription",
					displayName : '设备描述',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "item",
					displayName : '物料编码',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "userId",
					displayName : 'PRD操作员',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : "text-align-right",
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "oee",
					displayName : 'OEE',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					},
					cellClass : function(row, col) {
						return 'text-align-right';
					},
				}, {
					name : "a",
					displayName : '时间利用率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "p",
					displayName : '性能率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, {
					name : "q",
					displayName : '优率',
					minWidth : 140,
					enableCellEdit : false,
					enableCellEditOnFocus : false,
					cellClass : function(row, col) {
						return 'text-align-right';
					},
					cellTooltip : function(row, col) {
						return row.entity[col.colDef.name];
					}
				}, ],
				onRegisterApi:__requestGridApi
			};

            //设置color比例
			function setColorWidth(){
				$scope.setColorTimeout=$timeout(function(){
					var elementArrayR=document.getElementsByClassName("px-device-real-item-red");
					var elementArrayY=document.getElementsByClassName("px-device-real-item-yellow");
					var elementArrayG=document.getElementsByClassName("px-device-real-item-green");
					var elementArrayTopR=document.getElementsByClassName("px-device-real-item-red-without");
					var elementArrayTopY=document.getElementsByClassName("px-device-real-item-yellow-without");
					var elementArrayTopG=document.getElementsByClassName("px-device-real-item-green-without");

					for(var i=0;i<elementArrayR.length;i++){

						var redHeight=$scope.config.deviceGlobalList[i].redPercent;
						var yellowHeight=$scope.config.deviceGlobalList[i].yellowPercent;
						var greenHeight=$scope.config.deviceGlobalList[i].greenPercent;

						var redHeightTop=100-parseInt($scope.config.deviceGlobalList[i].redPercent);
						var yellowHeightTop=100-parseInt($scope.config.deviceGlobalList[i].yellowPercent);
						var greenHeightTop=100-parseInt($scope.config.deviceGlobalList[i].greenPercent);


						elementArrayR[i].style.height=redHeight;
						elementArrayR[i].style.backgroundColor="#FF0000";
						elementArrayTopR[i].style.height=redHeightTop+"%";
						elementArrayY[i].style.height=yellowHeight;
						elementArrayY[i].style.backgroundColor="#FFFF00";
						elementArrayTopY[i].style.height=yellowHeightTop+"%";
						elementArrayG[i].style.height=greenHeight;
						elementArrayG[i].style.backgroundColor="#92D050";
						elementArrayTopG[i].style.height=greenHeightTop+"%";
					}
				},
				300);
			}

			$scope.enterEvent=function(index,colorStatus){
				if(colorStatus=="R")
				{
					$scope.config.deviceGlobalList[index].statusR=false;
					//UtilsService.log("执行进入成功红灯："+index);
				}else if(colorStatus=="G")
				{
					$scope.config.deviceGlobalList[index].statusG=false;
				}else{
					$scope.config.deviceGlobalList[index].statusY=false;
				}
			};
			$scope.leaveEvent=function(index,colorStatus){
				if(colorStatus=="R")
				{
					$scope.config.deviceGlobalList[index].statusR=true;
					//UtilsService.log("执行移除成功红灯："+index);
				}else if(colorStatus=="G")
				{
					$scope.config.deviceGlobalList[index].statusG=true;
				}else{
					$scope.config.deviceGlobalList[index].statusY=true;
				}
			};

			// detailModal
			$scope.detailModalShow = function(index,item,color,colorCode) {
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/modal/deviceLampDetailModal.html',
					controller : 'deviceLampDetailModalCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						lampDetailData: function() {
							return {
								sites :  copyDataInfo.sites,
								workCenter : copyDataInfo.workCenter,
								lineArea : copyDataInfo.lineArea,
								resourceType : copyDataInfo.resourceType,
								lampColor:color,
								colorCode:colorCode,
								dimensionName:item.dimensionName,
								dimension:copyDataInfo.dimension,
								dimensionCode:item.dimensionCode
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					//返回的时候做什么
				}, function(reason) {
					UtilsService.log("原因："+reason);
				});
			}


			$scope.changeSelect = function(type) {
				if (type == 'W') {
					$scope.config.byType = "W";
					$scope.config.dimension = 2;
					$scope.config.showWorkArea = true;
					$scope.config.showLineArea = false;
					$scope.config.showResourceType = false;
					$scope.config.showFactory = false;
					$scope.config.showModel = false;
				} else if (type == 'L') {
					$scope.config.byType = "L";
					$scope.config.dimension = 3;
					$scope.config.showLineArea = true;
					$scope.config.showWorkArea = false;
					$scope.config.showResourceType = false;
					$scope.config.showFactory = false;
					$scope.config.showModel = false;
				} else if (type == 'RT') {
					$scope.config.byType = "T";
					$scope.config.dimension = 4;
					$scope.config.showResourceType = true;
					$scope.config.showWorkArea = false;
					$scope.config.showLineArea = false;
					$scope.config.showFactory = false;
					$scope.config.showModel = false;
				} else if (type == 'R') {
					$scope.config.byType = "R";
					$scope.config.showFactory = false;
					$scope.config.showWorkArea = false;
					$scope.config.showLineArea = false;
					$scope.config.showResourceType = false;
					$scope.config.showModel = false;
				} else if (type == 'I') {
					$scope.config.byType = "I";
					$scope.config.showModel = true;
					$scope.config.showWorkArea = false;
					$scope.config.showLineArea = false;
					$scope.config.showResourceType = false;
					$scope.config.showFactory = false;
				}  else if (type == 'F') {
					$scope.config.byType = "F";
					$scope.config.dimension = 1;
					$scope.config.showFactory = true;
					$scope.config.showWorkArea = false;
					$scope.config.showLineArea = false;
					$scope.config.showResourceType = false;
					$scope.config.showResource = false;
				}
				$scope.controlQueryBtn();
			}
			// 查询按钮的控制
			$scope.controlQueryBtn = function() {
				if (($scope.statementConfig.currentWorkCenterDesc == null || $scope.statementConfig.currentWorkCenterDesc == '')
						&& ($scope.statementConfig.currentLineDesc == null || $scope.statementConfig.currentLineDesc == '')
						&& ($scope.statementConfig.currentDeviceTypeDesc == null || $scope.statementConfig.currentDeviceTypeDesc == '')
						&& ($scope.statementConfig.currentDeviceNum == null || $scope.statementConfig.currentDeviceNum == '')) {
					$scope.config.queryBtn = true;
				} else {
					$scope.config.queryBtn = false;
				}
				if ($scope.config.showWorkArea == true) {
					if ($scope.statementConfig.currentWorkCenterDesc != null && $scope.statementConfig.currentWorkCenterDesc != '') {
						$scope.config.queryBtn = false;
					} else {
						$scope.config.queryBtn = true;
					}
				} else if ($scope.config.showLineArea == true) {
					if ($scope.statementConfig.currentLineDesc != null && $scope.statementConfig.currentLineDesc != '') {
						$scope.config.queryBtn = false;
					} else {
						$scope.config.queryBtn = true;
					}
				} else if ($scope.config.showResourceType == true) {
					if ($scope.statementConfig.currentDeviceTypeDesc != null && $scope.statementConfig.currentDeviceTypeDesc != '') {
						$scope.config.queryBtn = false;
					} else {
						$scope.config.queryBtn = true;
					}
				} else if ($scope.config.showResource == true) {
					if ($scope.statementConfig.currentDeviceNum != null && $scope.statementConfig.currentDeviceNum != '') {
						$scope.config.queryBtn = false;
					} else {
						$scope.config.queryBtn = true;
					}
				} else if ($scope.config.showModel == true) {
					if ($scope.config.model != null && $scope.config.model != '') {
						$scope.config.queryBtn = false;
					} else {
						$scope.config.queryBtn = true;
					}
				} else if ($scope.config.showFactory == true) {
					if ($scope.statementConfig.factoryContentDesc != null && $scope.statementConfig.factoryContentDesc != '') {
						$scope.config.queryBtn = false;
					} else {
						$scope.config.queryBtn = true;
					}
				}
			}
			// 第一个Echart隐藏显示控制
			$scope.hideOeeEChart = function() {
				var hideDiv = document.getElementById('oee');
				hideDiv.style.height = "0px";
				$scope.config.showOeeEChart = false;
			};
			$scope.showOeeEChart = function() {
				var hideDiv = document.getElementById('oee');
				hideDiv.style.height = "500px";
				$scope.config.showOeeEChart = true;
			};

			$scope.hideAEChart = function() {
				var hideDiv = document.getElementById('a');
				hideDiv.style.height = "0px";
				$scope.config.showAEChart = false;
			};
			$scope.showAEChart = function() {
				var hideDiv = document.getElementById('a');
				hideDiv.style.height = "500px";
				$scope.config.showAEChart = true;
			};

			$scope.hidePEChart = function() {
				var hideDiv = document.getElementById('p');
				hideDiv.style.height = "0px";
				$scope.config.showPEChart = false;
			};
			$scope.showPEChart = function() {
				var hideDiv = document.getElementById('p');
				hideDiv.style.height = "500px";
				$scope.config.showPEChart = true;
			};

			$scope.hideQEChart = function() {
				var hideDiv = document.getElementById('q');
				hideDiv.style.height = "0px";
				$scope.config.showQEChart = false;
			};
			$scope.showQEChart = function() {
				var hideDiv = document.getElementById('q');
				hideDiv.style.height = "500px";
				$scope.config.showQEChart = true;
			};

			$scope.oeeEchartsShow = function(obj) {
				//var test={ADEG0002: [45.8,0], ADEG0001: [49.5,0], x: ["2017-01","2017-02"], ADEG0003: [54.2,0]};
				var resArray=[];
				for(pro in obj){
					if(pro=='x'){
						continue;
					}
					for(var i=0;i<obj[pro].length;i++){
						resArray.push(obj[pro][i]);
					}
				}
				var maxData=Math.max.apply(null,resArray);
				var minData=Math.min.apply(null,resArray);
				if(maxData%10 > 0){
					maxData = (parseInt(maxData/10)+1)*10;
				};
				minData = (parseInt(minData/10))*10;
				
				
				// var MaxTotaPproportions=Math.max.apply(null,
				// echartData[0].totaPproportions);
				$scope.oeeChart = echarts.init(document.getElementById('oee'));
				// 指定图表的配置项和数据
				var option = {
					title : {
						text : 'OEE'
					},
					tooltip : {
						trigger : 'axis',
						formatter: function(data){
		                    var res="";
		                    res+=data[0].name;
		                    for (var i = 0; i<data.length; i++) {
		                    	var color=data[i].color;
		                    	var span='<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:'+color+'"></span>'
		                        res += '<br/>' + span + data[i].seriesName+' : ' + data[i].value+"%";
		                    }
		                    return res;
		                }
					},
					legend : {
						data : [/* 'AWIN0010','AWIN0007' */]
					},
					toolbox : {
						feature : {
							dataView : {
								show : true,
								readOnly : true
							},
							magicType : {
								show : true,
								type : [ 'line', 'bar' ]
							},
							restore : {
								show : true
							},
							saveAsImage : {
								show : true
							}
						}
					},
					grid : {
						left : '3%',
						right : '4%',
						bottom : '3%',
						containLabel : true
					},
					xAxis : [ {	
						type : 'category',
						boundaryGap : false,
						data : obj.x
					} ],
					yAxis : [ {
						type : 'value',
						axisLabel: {
                            formatter: '{value}%'
                        },
                        min: minData,
                        max: maxData,
                        interval: 10,
					} ],
					series : [
					/*
					 * { name:'AWIN0010', type:'line',
					 * data:obj.AWIN0010 }, { name:'AWIN0007',
					 * type:'line', data:obj.AWIN0007 },
					 */
					]
				};
				for ( var i in obj) {
					if (i == 'x') {
						continue;
					}
					var y = {
						name : i,
						type : 'line',
						itemStyle: {
                            normal: {
                                color:  "#"+("00000"+((Math.random()*16777215+0.5)>>0).toString(16)).slice(-6),
                                /*label : {show:true,position:'top',formatter:'{c} %'}*/
                                
                            }
                        },
						data : obj[i]
					};
					option.legend.data.push(i);
					option.series.push(y);
				}
				$scope.oeeChart.setOption(option);
				window.onresize = $scope.oeeChart.resize;
				$scope.hideBodyModel();
			}
			$scope.aEchartsShow = function(obj) {
				var resArray=[];
				for(pro in obj){
					if(pro=='x'){
						continue;
					}
					for(var i=0;i<obj[pro].length;i++){
						resArray.push(obj[pro][i]);
					}
				}
				var maxData=Math.max.apply(null,resArray);
				var minData=Math.min.apply(null,resArray);
				if(maxData%10 > 0){
					maxData = (parseInt(maxData/10)+1)*10;
				};
				minData = (parseInt(minData/10))*10;
				$scope.aChart = echarts.init(document.getElementById('a'));
				// 指定图表的配置项和数据
				var option = {
					title : {
						text : '时间利用率'
					},
					tooltip : {
						trigger : 'axis',
						formatter: function(data){
		                    var res="";
		                    res+=data[0].name;
		                    for (var i = 0; i<data.length; i++) {
		                    	var color=data[i].color;
		                    	var span='<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:'+color+'"></span>'
		                        res += '<br/>' + span + data[i].seriesName+' : ' + data[i].value+"%";
		                    }
		                    return res;
		                }
					},
					legend : {
						data : []
					},
					toolbox : {
						feature : {
							dataView : {
								show : true,
								readOnly : true
							},
							magicType : {
								show : true,
								type : [ 'line', 'bar' ]
							},
							restore : {
								show : true
							},
							saveAsImage : {
								show : true
							}
						}
					},
					grid : {
						left : '3%',
						right : '4%',
						bottom : '3%',
						containLabel : true
					},
					xAxis : [ {
						type : 'category',
						boundaryGap : false,
						data : obj.x
					} ],
					yAxis : [ {
						type : 'value',
						axisLabel: {
                            formatter: '{value}%'
                        },
                        min: minData,
                        max: maxData,
                        interval: 10,
					} ],
					series : []
				};
				for ( var i in obj) {
					if (i == 'x') {
						continue;
					}
					var y = {
						name : i,
						type : 'line',
						itemStyle: {
                            normal: {
                                color: "#"+("00000"+((Math.random()*16777215+0.5)>>0).toString(16)).slice(-6)
                            }
                        },
						data : obj[i]
					};
					option.legend.data.push(i);
					option.series.push(y);
				}
				$scope.aChart.setOption(option);
				window.onresize = $scope.aChart.resize;
				$scope.hideBodyModel();
			}
			$scope.pEchartsShow = function(obj) {
				var resArray=[];
				for(pro in obj){
					if(pro=='x'){
						continue;
					}
					for(var i=0;i<obj[pro].length;i++){
						resArray.push(obj[pro][i]);
					}
				}
				var maxData=Math.max.apply(null,resArray);
				var minData=Math.min.apply(null,resArray);
				if(maxData%10 > 0){
					maxData = (parseInt(maxData/10)+1)*10;
				};
				minData = (parseInt(minData/10))*10;
				$scope.pChart = echarts.init(document.getElementById('p'));
				// 指定图表的配置项和数据
				var option = {
					title : {
						text : '性能率'
					},
					tooltip : {
						trigger : 'axis',
						formatter: function(data){
		                    var res="";
		                    res+=data[0].name;
		                    for (var i = 0; i<data.length; i++) {
		                    	var color=data[i].color;
		                    	var span='<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:'+color+'"></span>'
		                        res += '<br/>' + span + data[i].seriesName+' : ' + data[i].value+"%";
		                    }
		                    return res;
		                }
					},
					legend : {
						data : []
					},
					toolbox : {
						feature : {
							dataView : {
								show : true,
								readOnly : true
							},
							magicType : {
								show : true,
								type : [ 'line', 'bar' ]
							},
							restore : {
								show : true
							},
							saveAsImage : {
								show : true
							}
						}
					},
					grid : {
						left : '3%',
						right : '4%',
						bottom : '3%',
						containLabel : true
					},
					xAxis : [ {
						type : 'category',
						boundaryGap : false,
						data : obj.x
					} ],
					yAxis : [ {
						type : 'value',
						axisLabel: {
                            formatter: '{value}%'
                        },
                        min: minData,
                        max: maxData,
                        interval: 10,
					} ],
					series : []
				};
				for ( var i in obj) {
					if (i == 'x') {
						continue;
					}
					var y = {
						name : i,
						type : 'line',
						itemStyle: {
                            normal: {
                                color: "#"+("00000"+((Math.random()*16777215+0.5)>>0).toString(16)).slice(-6)
                            }
                        },
						data : obj[i]
					};
					option.legend.data.push(i);
					option.series.push(y);
				}
				$scope.pChart.setOption(option);
				window.onresize = $scope.pChart.resize;
				$scope.hideBodyModel();
			}
			$scope.qEchartsShow = function(obj) {
				var resArray=[];
				for(pro in obj){
					if(pro=='x'){
						continue;
					}
					for(var i=0;i<obj[pro].length;i++){
						resArray.push(obj[pro][i]);
					}
				}
				var maxData=Math.max.apply(null,resArray);
				var minData=Math.min.apply(null,resArray);
				if(maxData%10 > 0){
					maxData = (parseInt(maxData/10)+1)*10;
				};
				minData = (parseInt(minData/10))*10;
				$scope.qChart = echarts.init(document.getElementById('q'));
				// 指定图表的配置项和数据
				var option = {
					title : {
						text : '优率'
					},
					tooltip : {
						trigger : 'axis',
						formatter: function(data){
		                    var res="";
		                    res+=data[0].name;
		                    for (var i = 0; i<data.length; i++) {
		                    	var color=data[i].color;
		                    	var span='<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:'+color+'"></span>'
		                        res += '<br/>' + span + data[i].seriesName+' : ' + data[i].value+"%";
		                    }
		                    return res;
		                }
					},
					legend : {
						data : []
					},
					toolbox : {
						feature : {
							dataView : {
								show : true,
								readOnly : true
							},
							magicType : {
								show : true,
								type : [ 'line', 'bar' ]
							},
							restore : {
								show : true
							},
							saveAsImage : {
								show : true
							}
						}
					},
					grid : {
						left : '3%',
						right : '4%',
						bottom : '3%',
						containLabel : true
					},
					xAxis : [ {
						type : 'category',
						boundaryGap : false,
						data : obj.x
					} ],
					yAxis : [ {
						type : 'value',
						axisLabel: {
                            formatter: '{value}%'
                        },
                        min: minData,
                        max: maxData,
                        interval: 10,
					} ],
					series : []
				};
				for ( var i in obj) {
					if (i == 'x') {
						continue;
					}
					var y = {
						name : i,
						type : 'line',
						itemStyle: {
                            normal: {
                                color: "#"+("00000"+((Math.random()*16777215+0.5)>>0).toString(16)).slice(-6)
                            }
                        },
						data : obj[i]
					};
					option.legend.data.push(i);
					option.series.push(y);
				}
				$scope.qChart.setOption(option);
				window.onresize = $scope.qChart.resize;
				$scope.hideBodyModel();
			}
			$scope.changeGrid=function(timeType){
				var obj={};
				if(timeType=='Y'){
					obj = {name : "byTimeChart",displayName : '日期',minWidth : 100,enableCellEdit : false,enableCellEditOnFocus : false,
							cellTooltip : function(row, col) {
								return row.entity[col.colDef.name];
							}
						  }
					if($scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridItemStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridItemStatement.columnDefs.unshift(obj);
					}
					
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[1].visible=false;
					if($scope.config.byType!='I'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[8].visible=false;
					}
					if($scope.config.prd==''){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=false;
					}else{
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=true;
					}
				}else if(timeType=='M'){
					obj = {name : "byTimeChart",displayName : '日期',minWidth : 100,enableCellEdit : false,enableCellEditOnFocus : false,
							cellTooltip : function(row, col) {
								return row.entity[col.colDef.name];
							}
						  }
					if($scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridItemStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridItemStatement.columnDefs.unshift(obj);
					}
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[1].visible=false;
					
					if($scope.config.byType!='I'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[8].visible=false;
					}
					if($scope.config.prd==''){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=false;
					}else{
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=true;
					}
				}else if(timeType=='W'){
					obj = {name : "byTimeChart",displayName : '日期',minWidth : 100,enableCellEdit : false,enableCellEditOnFocus : false,
							cellTooltip : function(row, col) {
								return row.entity[col.colDef.name];
							}
						  }
					if($scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridItemStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridItemStatement.columnDefs.unshift(obj);
					}
					
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[1].visible=false;
					
					if($scope.config.byType!='I'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[8].visible=false;
					}
					if($scope.config.prd==''){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=false;
					}else{
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=true;
					}
				}else if(timeType=='D'){
					obj = {name : "byTimeChart",displayName : '日期',minWidth : 100,enableCellEdit : false,enableCellEditOnFocus : false,
							cellTooltip : function(row, col) {
								return row.entity[col.colDef.name];
							}
						  }
					if($scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridResourceStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs.unshift(obj);
					}
					if($scope.OEETrendAnalysisGridItemStatement.columnDefs[0].name!='byTimeChart'){
						$scope.OEETrendAnalysisGridItemStatement.columnDefs.unshift(obj);
					}
					
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[1].visible=false;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[1].visible=false;
					
					if($scope.config.byType!='I'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[8].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[8].visible=false;
					}
					if($scope.config.prd==''){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=false;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=false;
					}else{
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs[9].visible=true;
						$scope.OEETrendAnalysisGridItemStatement.columnDefs[9].visible=true;
					}
				}else if(timeType=='S'){
					/*obj = {name : "shiftStartTime",displayName : '班次',minWidth : 100,enableCellEdit : false,enableCellEditOnFocus : false,
							cellTooltip : function(row, col) {
								return row.entity[col.colDef.name];
							}	
						 }*/
					if($scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[0].name=='byTimeChart'){
						$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs.splice(0, 1);
					}
					if($scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[0].name=='byTimeChart'){
						$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs.splice(0, 1);
					}
					if($scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[0].name=='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs.splice(0, 1);
					}
					if($scope.OEETrendAnalysisGridResourceStatement.columnDefs[0].name=='byTimeChart'){
						$scope.OEETrendAnalysisGridResourceStatement.columnDefs.splice(0, 1);
					}
					if($scope.OEETrendAnalysisGridItemStatement.columnDefs[0].name=='byTimeChart'){
						$scope.OEETrendAnalysisGridItemStatement.columnDefs.splice(0, 1);
					}
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[0].visible=true;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[0].visible=true;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[0].visible=true;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[0].visible=true;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[0].visible=true;
					
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[7].visible=true;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[7].visible=true;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[7].visible=true;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[7].visible=true;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[7].visible=true;
						
					$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[8].visible=true;
					$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[8].visible=true;
					$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[8].visible=true;
					$scope.OEETrendAnalysisGridResourceStatement.columnDefs[8].visible=true;
					$scope.OEETrendAnalysisGridItemStatement.columnDefs[8].visible=true;
				}
			}
			// 查询
			$scope.queryDatas = function() {
				if($scope.timeouter){
				 $timeout.cancel($scope.timeouter);
				}
				search(1);
			}

			function search(code){
				var params={};
				if(code)
				{
					params = {
						sites :  $scope.statementConfig.factoryContentCode ?  $scope.statementConfig.factoryContentCode:HttpAppService.getSite(),
						workCenter : $scope.statementConfig.currentWorkCenter,
						lineArea : $scope.statementConfig.currentLine,
						resourceType : $scope.statementConfig.currentDeviceType,
						dimension : $scope.config.dimension,
					};
				}else{
					params = {
						sites :  copyDataInfo.sites,
						workCenter : copyDataInfo.workCenter,
						lineArea : copyDataInfo.lineArea,
						resourceType : copyDataInfo.resourceType,
						dimension : copyDataInfo.dimension,
					};
				}


				//把数据copy下来
				//copyDataInfo.sites = $scope.statementConfig.factoryContentCode ?  $scope.statementConfig.factoryContentCode:HttpAppService.getSite();
				//copyDataInfo.workCenter = $scope.statementConfig.currentWorkCenter;
				//copyDataInfo.lineArea  = $scope.statementConfig.currentLine;
				//copyDataInfo.resourceType = $scope.statementConfig.currentDeviceType;
				//copyDataInfo.resourceType = $scope.config.dimension;
				copyDataInfo=angular.copy(params);

				$scope.config.deviceGlobalList=[];
				//$scope.showBodyModel("正在加载数据,请稍后...");
				statementService.deviceGlobalStatusList(params).then(function(resultData) {
					//$scope.hideBodyModel();
					UtilsService.log("返回结果:"+angular.toJson(resultData,true));
					if (resultData.response.length > 0) {

						var averNum=resultData.response.length%4;
						for(var i=0;i<resultData.response.length;i++)
						{
							var item={
								resourceCount:"",
								statusR:true,
								statusY:true,
								statusG:true,
								redNum:"",
								yellowNum:"",
								greenNum:"",
								redPercent:"",
								yellowPercent:"",
								greenPercent:"",
								productionCount:"",
								dimensionName:"",
								dimensionCode:"",
								averageNum:""
							};
							item.dimensionCode=resultData.response[i].dimensionCode;
							item.resourceCount=resultData.response[i].resourceCount;
							item.redNum=resultData.response[i].redCount;
							item.yellowNum=resultData.response[i].yellowCount;
							item.greenNum=resultData.response[i].greenCount;
							item.redPercent=UtilsService.toDecimal2(resultData.response[i].redCount/resultData.response[i].resourceCount);
							item.yellowPercent=UtilsService.toDecimal2(resultData.response[i].yellowCount/resultData.response[i].resourceCount);
							item.greenPercent=UtilsService.toDecimal2(resultData.response[i].greenCount/resultData.response[i].resourceCount);
							item.productionCount=resultData.response[i].productionCount;
							if($scope.config.dimension == 1)
							{
								item.dimensionName=resultData.response[i].dimensionName+"工厂";
							}else if($scope.config.dimension == 2)
							{
								item.dimensionName=resultData.response[i].dimensionName+"工厂区域";
							}else if($scope.config.dimension == 3)
							{
								item.dimensionName=resultData.response[i].dimensionName+"拉线";
							}else if($scope.config.dimension == 4)
							{
								item.dimensionName=resultData.response[i].dimensionName+"设备类型";
							}

							if(averNum == 0){
								item.averageNum=4;
							}else if(averNum == 1){
								if(i == resultData.response.length-1)
								{
									item.averageNum=1;
								}else{
									item.averageNum=4;
								}
							}else if(averNum == 2){
								if(i == resultData.response.length-1 || i == resultData.response.length-2)
								{
									item.averageNum=2;
								}else{
									item.averageNum=4;
								}
							}else if(averNum == 3){
								if(i == resultData.response.length-1 || i == resultData.response.length-2 ||i == resultData.response.length-3)
								{
									item.averageNum=3;
								}else{
									item.averageNum=4;
								}
							}
							$scope.config.deviceGlobalList.push(item);
						}
						setColorWidth();

					} else {
						$scope.addAlert("暂无数据");
						$scope.config.deviceGlobalList=[];
					}
				}, function(resultData) {
					//$scope.hideBodyModel();
					$scope.addAlert("danger", resultData.myHttpConfig.statusDesc);
				});
				$scope.timeouter = $timeout(function () {
					//if(!$scope.config.queryBtn)
					//{
					//	search(0);
					//}
					search(0);
				},72000);
			}
			$scope.downLoad = function() {
				$scope.showBodyModel("正在导出数据,请稍后...");
				var params = {
					byType : $scope.config.byType,
					workCenter : $scope.statementConfig.currentWorkCenter,
					lineArea : $scope.statementConfig.currentLine,
					resourceType : $scope.statementConfig.currentDeviceType,
					resource : $scope.statementConfig.currentDeviceNum,
					model : $scope.config.model,
					prd : $scope.config.prd,
					byTimeType : $scope.config.dateType,
					startDate : '',
					endDate : '',
				};
				if ($scope.statementConfig.currentShift) {
					params.shift = $scope.statementConfig.currentShift.shift;
				}
				if ($scope.config.selectYear == true) {
					params.startDate = $scope.statementConfig.startYear.year;
					params.endDate = $scope.statementConfig.endYear.year;
				} else if ($scope.config.selectType == true) {
					params.startDate = UtilsService.getMonthDate(new Date($scope.config.startDateMouth), 'start');
					params.endDate = UtilsService.getMonthDate(new Date($scope.config.endDateMouth), 'end');
				} else {
					params.startDate = UtilsService.serverFommateDate(new Date($scope.config.startDate));
					params.endDate = UtilsService.serverFommateDate(new Date($scope.config.endDate));
				}
				if (new Date(params.startDate) > new Date(params.endDate)) {
					$scope.addAlert('danger', '开始时间不能大于结束时间');
					return;
				}
				if (params.byTimeType == 'Y') {
					$scope.changeGrid(params.byTimeType);
					if (UtilsService.addMonth(new Date(params.startDate), 36) < new Date(params.endDate)) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过3年');
						$scope.hideBodyModel();
						return;
					}
				}else if (params.byTimeType == 'M') {
					if (UtilsService.addMonth(new Date(params.startDate), 12) < new Date(params.endDate)) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过12个月');
						$scope.hideBodyModel();
						return;
					}
				} else if (params.byTimeType == 'W') {
					if ((new Date(params.endDate)).getTime() - (new Date(params.startDate)).getTime() > 105 * 24 * 60 * 60 * 1000) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过15周');
						$scope.hideBodyModel();
						return;
					}
				} else if (params.byTimeType == 'D') {
					if ((new Date(params.endDate)).getTime() - (new Date(params.startDate)).getTime() > 92 * 24 * 60 * 60 * 1000) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过92天');
						return;
					}
				} else if (params.byTimeType == 'S') {
					if ((new Date(params.endDate)).getTime() - (new Date(params.startDate)).getTime() > 92 * 24 * 60 * 60 * 1000) {
						$scope.addAlert('danger', '开始和结束时间跨度不超过92天');
						return;
					}
				}
				var url = statementService.OEETrendAnalysisExport(params);
				url = url.replace(/undefined/g, "").replace(/null/g, "");
				url = HttpAppService.handleCommenUrl(url);
				$http({
					url : url,
					method : "GET",
					headers : {
						'Content-type' : 'application/json'
					},
					responseType : 'arraybuffer'
				}).success(function(data, status, headers, config) {
					var blob = new Blob([ data ], {
						type : "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
					});
					var objectUrl = URL.createObjectURL(blob);
					window.location.href = objectUrl;
					$scope.hideBodyModel();
				}).error(function(data, status, headers, config) {
				});
			}
			// 按照 月 周 天 班次 汇总
			$scope.changeAddType = function(type) {
				if (type == "Y") {
					$scope.config.dateType = "Y";
					$scope.config.selectYear = true;
					$scope.config.selectMouth = false;
					$scope.config.selectWeek = false;
					$scope.config.selectDay = false;
					$scope.config.selectShift = false;
					$scope.config.selectType = false;
					$scope.config.startDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.endDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.startDateMouth = UtilsService.serverFormateDateMouth(new Date());
					$scope.config.endDateMouth = UtilsService.serverFormateDateMouth(new Date());
				} else if (type == "M") {
					$scope.config.dateType = "M";
					$scope.config.selectYear = false;
					$scope.config.selectMouth = true;
					$scope.config.selectWeek = false;
					$scope.config.selectDay = false;
					$scope.config.selectShift = false;
					$scope.config.selectType = true;
					$scope.config.startDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.endDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.startDateMouth = UtilsService.serverFormateDateMouth(new Date());
					$scope.config.endDateMouth = UtilsService.serverFormateDateMouth(new Date());
				} else if (type == "W") {
					$scope.config.dateType = "W";
					$scope.config.selectYear = false;
					$scope.config.selectMouth = false;
					$scope.config.selectWeek = true;
					$scope.config.selectDay = false;
					$scope.config.selectShift = false;
					$scope.config.selectType = false;
					$scope.config.startDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.endDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.startDateMouth = UtilsService.serverFormateDateMouth(new Date());
					$scope.config.endDateMouth = UtilsService.serverFormateDateMouth(new Date());
				} else if (type == "D") {
					$scope.config.dateType = "D";
					$scope.config.selectYear = false;
					$scope.config.selectMouth = false;
					$scope.config.selectWeek = false;
					$scope.config.selectDay = true;
					$scope.config.selectShift = false;
					$scope.config.selectType = false;
					$scope.config.startDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.endDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.startDateMouth = UtilsService.serverFormateDateMouth(new Date());
					$scope.config.endDateMouth = UtilsService.serverFormateDateMouth(new Date());
				} else if (type == "S") {
					$scope.config.dateType = "S";
					$scope.config.selectYear = false;
					$scope.config.selectMouth = false;
					$scope.config.selectWeek = false;
					$scope.config.selectDay = false;
					$scope.config.selectShift = true;
					$scope.config.selectType = false;
					$scope.config.startDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.endDate = UtilsService.serverFommateDateShow(new Date());
					$scope.config.startDateMouth = UtilsService.serverFormateDateMouth(new Date());
					$scope.config.endDateMouth = UtilsService.serverFormateDateMouth(new Date());
				}
			};
			// 清除输入框
			$scope.deleteAll = function(item) {
				if (item == 'area') {
					$scope.statementConfig.currentWorkCenter = [];
					$scope.statementConfig.currentWorkCenterDesc = [];
					$scope.statementConfig.currentLine = [];
					$scope.statementConfig.currentLineDesc = [];
					$scope.statementConfig.deviceNums = [];
					$scope.statementConfig.currentDeviceNum = null;
					$scope.statementConfig.multipleCurrentDeviceNum = [];
					$scope.config.showSelectArea = true;
					$scope.config.showSelectLine = true;
					$scope.config.deviceCode = true;
					$scope.config.factoryCode = true;
					$scope.controlQueryBtn();
				}
				if (item == 'line') {
					$scope.statementConfig.currentLine = [];
					$scope.statementConfig.currentLineDesc = [];
					$scope.statementConfig.deviceNums = [];
					$scope.statementConfig.currentDeviceNum = null;
					$scope.statementConfig.multipleCurrentDeviceNum = [];
					$scope.config.showSelectLine = true;
					$scope.config.deviceCode = true;
					$scope.config.factoryCode = true;
					$scope.controlQueryBtn();
				}
				if (item == 'deviceType') {
					$scope.statementConfig.currentDeviceType = [];
					$scope.statementConfig.currentDeviceTypeDesc = [];
					$scope.statementConfig.multipleCurrentDeviceNum = [];
					$scope.config.deviceType = true;
					$scope.config.deviceCode = true;
					$scope.config.factoryCode = true;
					$scope.controlQueryBtn();
				}
				if (item == 'factoryCode') {
					$scope.statementConfig.factoryContentDesc = "";
					$scope.statementConfig.factoryContentCode = "";
					$scope.config.factoryCode = true;
					$scope.controlQueryBtn();
				}
				if (item == 'deviceCode') {
					$scope.statementConfig.currentDeviceNum = [];
					$scope.config.deviceCode = true;
					$scope.controlQueryBtn();
				}
			}

			//选择工厂
			$scope.toFactoryChoose=function(){
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/modal/factoryModal.html',
					controller : 'factoryChooseCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						selectFactoryModal : function() {
							return {
								//workArea : $scope.statementConfig.workCenters,
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.statementConfig.factoryContentCode = selectedItem.siteList.join(",");
					$scope.statementConfig.factoryContentDesc = selectedItem.descriptionList.join(",");
					//$scope.config.showSelectArea = false;
					//$scope.deleteAll('line');
					//$scope.deleteAll('deviceCode');
					$scope.controlQueryBtn();
				}, function() {
				});
			};
			// 选择时间 这是选择月的
			// 生产区域帮助
			$scope.toAreaHelp = function() {
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/modal/areaSelectMoreModal.html',
					controller : 'areaSelectMoreCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						selectAreaModal : function() {
							return {
								workArea : $scope.statementConfig.workCenters,
								withSiteStatus:true,//代表此接口是否关联选择工厂
								sites: $scope.statementConfig.factoryContentCode
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.statementConfig.currentWorkCenter = selectedItem.area.join(",");
					$scope.statementConfig.currentWorkCenterDesc = selectedItem.areaName.join(",");
					$scope.config.showSelectArea = false;
					$scope.deleteAll('line');
					$scope.deleteAll('deviceCode');
					$scope.controlQueryBtn();
				}, function() {
				});
			};
			// 拉线多选帮助
			$scope.toWireLineHelp = function() {
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/modal/lineSelectModal.html',
					controller : 'LineSelectMoreCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						selectLineModal : function() {
							return {
								workArea : $scope.statementConfig.currentWorkCenter,
								workLine : $scope.statementConfig.lines,
								withSiteStatus:true,//代表此接口是否关联选择工厂
								sites: $scope.statementConfig.factoryContentCode
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.statementConfig.currentLine = selectedItem.line;
					$scope.statementConfig.currentLineDesc = selectedItem.lineName;
					if ($scope.statementConfig.currentLine.length > 0) {
						$scope.config.showSelectLine = false;
					}
					$scope.deleteAll('deviceCode');
					$scope.controlQueryBtn();
				}, function() {
				});
			};
			// 设备类型帮助
			$scope.toDeviceTypeHelp = function() {
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/modal/deviceTypeSelectMoreModal.html',
					controller : 'deviceTypeMoreCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						selectTypeMoreDevice : function() {
							return {
								deviceCode : $scope.statementConfig.deviceTypes,
								withSiteStatus:true,//代表此接口是否关联选择工厂
								sites: $scope.statementConfig.factoryContentCode
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.statementConfig.currentDeviceType = selectedItem.resourceType;
					$scope.statementConfig.currentDeviceTypeDesc = selectedItem.resourceTypeDesc;
					if ($scope.statementConfig.currentDeviceType.length > 0) {
						$scope.config.deviceType = false;
					}
					$scope.deleteAll('deviceCode');
					$scope.controlQueryBtn();
				}, function() {
				});
			};
			// 设备帮助
			$scope.toDeviceHelp = function() {
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/modal/modalDeviceHelp.html',
					controller : 'deviceMoreCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						selectMoreDevice : function() {
							return {
								workArea : $scope.statementConfig.currentWorkCenter,
								workLine : $scope.statementConfig.currentLine,
								deviceCode : $scope.statementConfig.currentDeviceType,
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.statementConfig.currentDeviceNum = selectedItem.area;
					if ($scope.statementConfig.currentDeviceNum.length > 0) {
						$scope.config.deviceCode = false;
					}
					$scope.controlQueryBtn();
				}, function() {
				});
			};
			// model帮助
			$scope.toModelHelp = function() {
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/outputStatement/modal/modalModelHelp.html',
					controller : 'modelHelpCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						outPutReportModel : function() {
							return {
								model : $scope.config.model
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.config.model = selectedItem.join(",");
					if ($scope.config.model.length > 0) {
						$scope.config.modelIcon = false;
					}
					$scope.controlQueryBtn();
				}, function() {
				});
			}
			// PRD帮助
			$scope.toPrdHelp = function() {
				var modalInstance = $uibModal.open({
					animation : true,
					templateUrl : 'modules/statement/outputStatement/modal/prdModelHelp.html',
					controller : 'prdHelpCtrl',
					size : 'lg',
					backdrop : 'false',
					scope : $scope,
					openedClass : 'flex-center-parent',
					resolve : {
						outPutReportModel : function() {
							return {
								prd : $scope.config.prd
							};
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.config.prd = selectedItem.join(",");
					if ($scope.config.prd.length > 0) {
						$scope.config.showSelectPrd = false;
					}
					$scope.controlQueryBtn();
				}, function() {
				});
			}
			function __requestGridApi(gridApi){
				$scope.config.gridApi = gridApi;
			}
			//控制显示隐藏列
			$scope.showHideCol=function () {
				if($scope.config.queryGridAreaShow){
					for(var i=0;i<$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs.length;i++) {
						if (!$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[i].visible) {
							$scope.OEETrendAnalysisGridWorkAreaStatement.columnDefs[i].visible = true;
						}
					}
				}
				else if($scope.config.queryGridLineShow){
					for(var i=0;i<$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs.length;i++)
					{
						if(!$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[i].visible){
							$scope.OEETrendAnalysisGridLineAreaStatement.columnDefs[i].visible=true;
						}
					}
				}
				else if($scope.config.queryGridDeviceShow){
					for(var i=0;i<$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs.length;i++)
					{
						if(!$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[i].visible){
							$scope.OEETrendAnalysisGridResourceTypeStatement.columnDefs[i].visible=true;
						}
					}
				}
				else if($scope.config.queryGridAreaCodeShow){
					for(var i=0;i<$scope.OEETrendAnalysisGridResourceStatement.columnDefs.length;i++)
					{
						if(!$scope.OEETrendAnalysisGridResourceStatement.columnDefs[i].visible){
							$scope.OEETrendAnalysisGridResourceStatement.columnDefs[i].visible=true;
						}                }
				}
				else if($scope.config.queryGridItemShow){
					for(var i=0;i<$scope.OEETrendAnalysisGridItemStatement.columnDefs.length;i++)
					{
						if(!$scope.OEETrendAnalysisGridItemStatement.columnDefs[i].visible){
							$scope.OEETrendAnalysisGridItemStatement.columnDefs[i].visible=true;
						}                }
				}
				$scope.config.gridApi.core.refresh();
			};

			$scope.testHref=function(){
				//a 标签target="_blank"  href=' http://www.baidu.com '
				//localStorage.userId = data;
				//var url = $state.href('index.userList.userBasicinfo',{basicType:8});
				//window.open(url,'_blank');

				//window.open(' http://www.baidu.com ','_blank');
				//$scope.config.testShowStatus=true;
				//$timeout(function(){
				//	UtilsService.log("隐藏的div高度："+document.getElementById('divTest').style.height);
				//},0);
			};

			//var refreshEchart=$scope.$on("refreshEchart", function(event,data) {
			//	UtilsService.log("接受成功shutdownReason");
			//	if($scope.config.showEcharts) {
			//		$scope.timeOuter = $timeout(function () {
			//			$scope.oeeChart.resize();
			//			$scope.aChart.resize();
			//			$scope.pChart.resize();
			//			$scope.qChart.resize();
			//		}, 350);
			//	}
			//});
			$scope.$on('$destroy',function(){
				window.localStorage.currentWorkCenter=$scope.statementConfig.currentWorkCenter;
				window.localStorage.currentWorkCenterDesc=$scope.statementConfig.currentWorkCenterDesc;
				window.localStorage.currentLine=$scope.statementConfig.currentLine;
				window.localStorage.currentLineDesc=$scope.statementConfig.currentLineDesc;
				window.localStorage.currentDeviceType=$scope.statementConfig.currentDeviceType;
				window.localStorage.currentDeviceTypeDesc=$scope.statementConfig.currentDeviceTypeDesc;
				window.localStorage.factoryContentCode=$scope.statementConfig.factoryContentCode;
				window.localStorage.factoryContentDesc=$scope.statementConfig.factoryContentDesc;

				window.localStorage.showFactory=$scope.config.showFactory;
				window.localStorage.showWorkArea=$scope.config.showWorkArea;
				window.localStorage.showLineArea=$scope.config.showLineArea;
				window.localStorage.showResourceType=$scope.config.showResourceType;

				window.localStorage.showSelectArea=$scope.config.showSelectArea;
				window.localStorage.showSelectLine=$scope.config.showSelectLine;
				window.localStorage.deviceType=$scope.config.deviceType;
				window.localStorage.factoryCode=$scope.config.factoryCode;

				window.localStorage.queryBtn=$scope.config.queryBtn;
				window.localStorage.dimension=$scope.config.dimension;

				if($scope.timeouter){
					$timeout.cancel($scope.timeouter);
					UtilsService.log("销毁timeouter");
				}

				if($scope.setColorTimeout)
				{
					$timeout.cancel($scope.setColorTimeout);
				}

				//refreshEchart();
				//$timeout.cancel($scope.timeOuter);
				//UtilsService.log("销毁成功shutdownReason");
			});
		} ])