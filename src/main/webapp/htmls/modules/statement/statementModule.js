statementModule.service('statementService', [
    'HttpAppService','UtilsService',
    function (HttpAppService,UtilsService) {
        return{
            //产出报表接口start
            outputReportAreaHelp : __outputReportAreaHelp,
            outputReportNewAreaHelp : __outputReportNewAreaHelp,
            outputReportLineHelp : __outputReportLineHelp,
            outputReportNewLineHelp : __outputReportNewLineHelp,
            outputReportDeviceHelp : __outputReportDeviceHelp,
            outputReportModelHelp : __outputReportModelHelp,
            outputReportRrdHelp:____outputReportRrdHelp,
            outputReportCodeHelp : __outputReportCodeHelp,
            outputReportSelect : __outputReportSelect,
            outputReportPull : __outputReportPull,
            //产出报表接口end
            //PPM报表接口start
            PPMReportSelect : __PPMReportSelect,
            PPMReportPull : __PPMReportPull,
            //PPM数据导出报表
            PPMDateExport:__PPMDataExport,
            PPMDateExportCount:__PPMDataExportCount,

            //PPM报表接口end



            //OEE报表开始
            OEEReportList:__OEEReportList,
            OEEReasonCodeReportInside:__OEEReasonCodeReportInside,
            OEEReasonCodeReportOutside:__OEEReasonCodeReportOutside,
            OEEReportAlarmLine:__OEEReportAlarmLine,
            OEEExportExcelFile:__OEEExportExcelFile,
            OEESelectDeviceCode : __OEESelectDeviceCode,
            //OEE报表结束

            //设备状态明细开始
            DeviceStatusList:queryOutputTargetList,
            DeviceAlertList:queryOutputTargetList,
            DeviceHaltList:queryOutputTargetList,
            DeviceOutputList:queryOutputTargetList,
            DeviceRtReasonCodeList:queryOutputTargetList,
            DeviceRtReasonCodeList:queryOutputTargetList,
            ReportPull:exportExcel,
            ListCount:listCount,
            listTotal:listTotal,
            countRSD:__countRSD,
            countOEED:__countOEED,
            //设备状态明细结束

            //设备实时看板
            DeviceRealTimeQuery : DeviceRealTimeQuery,
            //OEE明细报表
            OEEDetailExport:exportOEEDetail,
            RSDetailExport:exportRSDetail,
            
            
            //停机原因柏拉图报表
            resourcetypeList:resourcetypeList,
            ShutdownReasonList:ShutdownReasonList,
            ShutdownReasonEchart:ShutdownReasonEchart,
            shutdownReasonExport:shutdownReasonExport,
            //生产微计划看板
            ProduceMicroPlanList:produceMicroPlanList,
            ProduceMicroPlanExport:produceMicroPlanExport,
            //OEE趋势分析报表
            OEETrendAnalysisGridList:__OEETrendAnalysisGridList,
            OEETrendAnalysisExport:__OEETrendAnalysisExport,
            deviceGlobalStatusList:__deviceGlobalStatusList,
            //设备宏观状态 不同颜色灯列表
            dataGlobalListByColor:dataGlobalListByColor,
            mouth : [
                [{num:1,name: "一月",selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false},
                    {num:2,name: "二月",selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false},
                    {num:3,name: "三月",selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false},
                    {num:4,name: "四月",selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false}],
                [{num:5,name: "五月",selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false},
                    {num:6,name: "六月",selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false},
                    {num:7,name: "七月",selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false},
                    {num:8,name: "八月",selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false}],
                [{num:9,name: "九月",selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false},
                    {num:10,name: "十月",selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false},
                    {num:11,name: "十一月",selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false},
                    {num:12,name: "十二月",selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false}]
            ]
        }
        //实时看板
        function DeviceRealTimeQuery(work_center, line, device_type, device_code){
            var url = HttpAppService.URLS.DEVICE_REALTIME_QUERY
                    .replace("#{site}#", HttpAppService.getSite())
                    .replace("#{work_center}#", work_center)
                    .replace("#{line}#", line)
                    .replace("#{device_type}#", device_type)
                    .replace("#{device_code}#", device_code)
                ;
            return HttpAppService.get({
                url: url
            });
        }
        //产出报表接口start
        function __outputReportAreaHelp(work_center){
            var url = HttpAppService.URLS.OUTPUTREPORT_AREA
                .replace("#{site}#", HttpAppService.getSite())
                .replace("#{work_center}#", work_center)
                ;
            return HttpAppService.get({
                url: url
            });
        }
        //产出报表接口start 新的接口带工厂选择
        function  __outputReportNewAreaHelp(work_center,sites){
            var url = HttpAppService.URLS.OUTPUTREPORT_AREA_NEW
                .replace("#{sites}#", sites ? sites:HttpAppService.getSite())
                .replace("#{work_center}#", work_center)
                ;
            return HttpAppService.get({
                url: url
            });
        }
        function __outputReportLineHelp(line,work_center){
            var url = HttpAppService.URLS.OUTPUTREPORT_LINE
                .replace("#{site}#", HttpAppService.getSite())
                .replace("#{line}#", line)
                .replace("#{work_center}#", work_center)
                ;
            return HttpAppService.get({
                url: url
            });
        }
        //__outputReportNewLineHelp
        function __outputReportNewLineHelp(line,work_center,sites){
            var url = HttpAppService.URLS.OUTPUTREPORT_LINE_NEW
                .replace("#{sites}#", sites ? sites:HttpAppService.getSite())
                .replace("#{line}#", line)
                .replace("#{work_center}#", work_center)
                ;
            return HttpAppService.get({
                url: url
            });
        }
        function __outputReportDeviceHelp(resrce,lineArea,workArea){
            var url = HttpAppService.URLS.OUTPUTREPORT_DEVICE
                .replace("#{site}#", HttpAppService.getSite())
                .replace("#{resrce}#", resrce)
                .replace("#{lineArea}#", lineArea)
                .replace("#{workArea}#", workArea)
                ;
            return HttpAppService.get({
                url: url
            });
        }
        function __outputReportModelHelp(model){
            var url = HttpAppService.URLS.OUTPUTREPORT_MODEL
                .replace("#{site}#", HttpAppService.getSite())
                .replace("#{model}#", model)
                ;
            return HttpAppService.get({
                url: url
            });
        }
        
        function ____outputReportRrdHelp(model){
            var url = HttpAppService.URLS.OUTPUTREPORT_PRD
                .replace("#{site}#", HttpAppService.getSite());
            return HttpAppService.get({
                url: url
            });
        }
        function __outputReportCodeHelp(model,item,status){
            var url="";
            if(status)
            {
                url = HttpAppService.URLS.PLC_OUTPUTREPORT_CODE
                    .replace("#{site}#", HttpAppService.getSite())
                    .replace("#{model}#", model)
                    .replace("#{item}#", item)
                    .replace("#{plcIp}#",window.localStorage.plcIp)
                ;
            }else{
                url = HttpAppService.URLS.OUTPUTREPORT_CODE
                    .replace("#{site}#", HttpAppService.getSite())
                    .replace("#{model}#", model)
                    .replace("#{item}#", item)
                ;
            }
            return HttpAppService.get({
                url: url
            });
        }
        function __outputReportSelect(paramActions){
            var url = HttpAppService.URLS.OUTPUTREPORT_SELECT
                .replace("#{site}#", HttpAppService.getSite());
            return HttpAppService.post({
                url: url,
                paras: paramActions
            });
        }
        function __outputReportPull(){
            var url = HttpAppService.URLS.OUTPUTREPORT_PULL
                    .replace("#{site}#", HttpAppService.getSite())
                ;
            return url;
        }
        //产出报表接口end

        //PPM报表接口start
        function __PPMReportSelect(paramActions){
            var url = HttpAppService.URLS.PPMREPORT_SELECT
                .replace("#{site}#", HttpAppService.getSite());
            return HttpAppService.post({
                url: url,
                paras: paramActions
            });
        }
        function __PPMReportPull(){
            var url = HttpAppService.URLS.PPMREPORT_PULL
                .replace("#{site}#", HttpAppService.getSite());
            return url;
        }
        //PPM报表接口end
        function __PPMDataExport(){
            var url = HttpAppService.URLS.PPM_DATA_DOWNLOAD
                .replace("#{site}#", HttpAppService.getSite());
            return url;
        }
        function __PPMDataExportCount(){
            var url = HttpAppService.URLS.PPM_DATA_DOWNLOAD_NUM
                .replace("#{site}#", HttpAppService.getSite());
            return url;
        }

        //OEE报表
        function __OEEReportList(param){
            var url = HttpAppService.URLS.OEE_REPORT_LIST
                .replace("#{site}#", param.site)
                .replace("#{resourceBo}#",param.resourceBo)
                .replace("#{startDateTime}#",param.startDateTime)
                .replace("#{endDateTime}#",param.endDateTime)
                .replace("#{byTimeType}#",param.byTimeType)
                ;
            return HttpAppService.get({
                url: url
            });
        };
        function __OEEReasonCodeReportInside(param){
            var url = HttpAppService.URLS.OEE_REASON_CODE_REPORT_INSIDE
                .replace("#{site}#", param.site)
                .replace("#{resourceBo}#",param.resourceBo)
                .replace("#{startDateTime}#",param.startDateTime)
                .replace("#{endDateTime}#",param.endDateTime)
                .replace("#{byTimeType}#",param.byTimeType)
                ;
            return HttpAppService.get({
                url: url
            });
        };

        function __OEEReasonCodeReportOutside(param){
            var url = HttpAppService.URLS.OEE_REASON_CODE_REPORT_OUTSIDE
                .replace("#{site}#", param.site)
                .replace("#{resourceBo}#",param.resourceBo)
                .replace("#{startDateTime}#",param.startDateTime)
                .replace("#{endDateTime}#",param.endDateTime)
                .replace("#{byTimeType}#",param.byTimeType)
                ;
            return HttpAppService.get({
                url: url
            });
        };
        function __OEEReportAlarmLine(){
            var url = HttpAppService.URLS.OEE_REPORT_ALARM_LINE
                    .replace("#{site}#", HttpAppService.getSite())
                ;
            return HttpAppService.get({
                url: url
            });

        };

        function __OEEExportExcelFile(param){
            var url = HttpAppService.URLS.OEE_EXPORT_EXCEL_FILE
                .replace("#{site}#", param.site)
                .replace("#{resourceBo}#",param.resourceBo)
                .replace("#{startDateTime}#",param.startDateTime)
                .replace("#{endDateTime}#",param.endDateTime)
                .replace("#{byTimeType}#",param.byTimeType)
                ;
            return url;
        };
        function __OEESelectDeviceCode(resrce){
            var url = HttpAppService.URLS.OEE_SELECT_DEVICE_CODE
                    .replace("#{site}#", HttpAppService.getSite())
                    .replace("#{resrce}#",resrce)
                ;
            return HttpAppService.get({
                url: url
            });
        };
        //设备状态明细
        function queryOutputTargetList(params){
            var requestType="";
            if(params.type=="S"){
                params.type="result_item";
            }else if(params.type=="A"){
                params.type="result_alert";
                requestType="A";
            }else if(params.type=="O"){
                params.type="result_output";
            }else if(params.type=="R"){
                params.type="result_rt_reason_code";
            }else{
                params.type="result_reason_code";
            }
            var url='';
            UtilsService.log("log params.type的值－－－－－－"+params.type);
            if(requestType == "A")
            {
                url = HttpAppService.URLS.RESOURCE_STATUS_DETAIL_WITH_ALARM_COMMENT_CODE
                    .replace("#{site}#", HttpAppService.getSite())
                    .replace("#{page_size}#", params.pageCount)
                    .replace("#{curr_page}#", params.pageIndex)
                    .replace("#{work_area}#", params.workCenter)
                    .replace("#{line_area}#", params.line)
                    .replace("#{resource_type}#", params.resourceType)
                    .replace("#{resource_code}#", params.resrce)
                    .replace("#{shift}#", params.shift)
                    .replace("#{type}#", params.type)
                    .replace("#{start_date}#", params.startDate)
                    .replace("#{end_date}#", params.endDate)
                    .replace("#{comment_code}#", params.alarmRemarkCode);
                UtilsService.log("type设备报警明细报表执行aaaaaaaaa");
            }else {
                url = HttpAppService.URLS.RESOURCE_STATUS_DETAIL
                    .replace("#{site}#", HttpAppService.getSite())
                    .replace("#{page_size}#", params.pageCount)
                    .replace("#{curr_page}#", params.pageIndex)
                    .replace("#{work_area}#", params.workCenter)
                    .replace("#{line_area}#", params.line)
                    .replace("#{resource_type}#", params.resourceType)
                    .replace("#{resource_code}#", params.resrce)
                    .replace("#{shift}#", params.shift)
                    .replace("#{type}#", params.type)
                    .replace("#{start_date}#", params.startDate)
                    .replace("#{end_date}#", params.endDate);
                UtilsService.log("type设备报警明细报表执行ooooooo");
            }

            UtilsService.log("get方法前url－－－:"+url);
            UtilsService.log("params－－－:"+angular.toJson(params,true));
            UtilsService.log("alarmRemarkCode－－－:"+params.alarmRemarkCode);

            return HttpAppService.get({
                url: url
            });
        };
        function exportOEEDetail(params){
            var url = HttpAppService.URLS.OEE_DETAIL_EXCEL
                .replace("#{site}#", HttpAppService.getSite())
                .replace("#{work_area}#", params.workCenter)
                .replace("#{line_area}#", params.line)
                .replace("#{resource_type}#", params.resourceType)
                .replace("#{resource_code}#", params.resrce)
                .replace("#{shift}#", params.shift)
                .replace("#{start_date}#", params.startDate)
                .replace("#{end_date}#", params.endDate);
            return url;
        }
        function __countRSD(params){
            var url = HttpAppService.URLS.RS_DETAIL_EXCEL_COUNT
                .replace("#{site}#", HttpAppService.getSite())
                .replace("#{work_area}#", params.workCenter)
                .replace("#{line_area}#", params.line)
                .replace("#{resource_type}#", params.resourceType)
                .replace("#{resource_code}#", params.resrce)
                .replace("#{shift}#", params.shift)
                .replace("#{start_date}#", params.startDate)
                .replace("#{end_date}#", params.endDate)
                .replace("#{resource_state}#", params.resourceState)
                ;
            return HttpAppService.get({
                url: url
            });
        }
        function __countOEED(params){
            var url = HttpAppService.URLS.OEE_DETAIL_EXCEL_COUNT
                .replace("#{site}#", HttpAppService.getSite())
                .replace("#{work_area}#", params.workCenter)
                .replace("#{line_area}#", params.line)
                .replace("#{resource_type}#", params.resourceType)
                .replace("#{resource_code}#", params.resrce)
                .replace("#{shift}#", params.shift)
                .replace("#{start_date}#", params.startDate)
                .replace("#{end_date}#", params.endDate)
                ;
            return HttpAppService.get({
                url: url
            });
        }
        function exportRSDetail(params){
            var url = HttpAppService.URLS.RS_DETAIL_EXCEL
                .replace("#{site}#", HttpAppService.getSite())
                .replace("#{work_area}#", params.workCenter)
                .replace("#{line_area}#", params.line)
                .replace("#{resource_type}#", params.resourceType)
                .replace("#{resource_code}#", params.resrce)
                .replace("#{shift}#", params.shift)
                .replace("#{start_date}#", params.startDate)
                .replace("#{end_date}#", params.endDate)
                .replace("#{resource_state}#", params.resourceState)
                ;
            return url;
        }
        function exportExcel(params){
            if(params.type=="设备状态明细报表"){
                params.type="result_item";
            }else if(params.type=="设备报警明细报表"){
                params.type="result_alert";
            }else if(params.type=="设备产量明细报表"){
                params.type="result_output";
            }else if(params.type=="实时停机原因报表"){
                params.type="result_rt_reason_code";
            }else{
                params.type="result_reason_code";
            }
            var url = HttpAppService.URLS.RESOURCE_STATUS_DETAIL_EXCEL
                .replace("#{site}#", HttpAppService.getSite())
                .replace("#{work_area}#", params.workCenter)
                .replace("#{line_area}#", params.line)
                .replace("#{resource_type}#", params.resourceType)
                .replace("#{resource_code}#", params.resrce)
                .replace("#{shift}#", params.shift)
                .replace("#{type}#", params.type)
                .replace("#{start_date}#", params.startDate)
                .replace("#{end_date}#", params.endDate);
            return url;
        }
        function listCount(params){
            var url = HttpAppService.URLS.RESOURCE_STATUS_DETAIL_COUNT
                .replace("#{site}#", HttpAppService.getSite())
                .replace("#{work_area}#", params.workCenter)
                .replace("#{line_area}#", params.line)
                .replace("#{resource_type}#", params.resourceType)
                .replace("#{resource_code}#", params.resrce)
                .replace("#{shift}#", params.shift)
                .replace("#{start_date}#", params.startDate)
                .replace("#{end_date}#", params.endDate);
            return HttpAppService.get({
                url: url
            });
        }
        function listTotal(params){
            UtilsService.log("列表记录总数接口："+angular.toJson(params,true));
            var url="";
            if(params.type == "A")
            {
                url = HttpAppService.URLS.RESOURCE_STATUS_DETAIL_TOTAL_WITH_ALARM_COMMENT_CODE
                    .replace("#{site}#", HttpAppService.getSite())
                    .replace("#{type}#", params.type)
                    .replace("#{work_area}#", params.workCenter)
                    .replace("#{line_area}#", params.line)
                    .replace("#{resource_type}#", params.resourceType)
                    .replace("#{resource_code}#", params.resrce)
                    .replace("#{shift}#", params.shift)
                    .replace("#{start_date}#", params.startDate)
                    .replace("#{end_date}#", params.endDate)
                    .replace("#{comment_code}#", params.commentCode);
            }else {
                url = HttpAppService.URLS.RESOURCE_STATUS_DETAIL_TOTAL
                    .replace("#{site}#", HttpAppService.getSite())
                    .replace("#{type}#", params.type)
                    .replace("#{work_area}#", params.workCenter)
                    .replace("#{line_area}#", params.line)
                    .replace("#{resource_type}#", params.resourceType)
                    .replace("#{resource_code}#", params.resrce)
                    .replace("#{shift}#", params.shift)
                    .replace("#{start_date}#", params.startDate)
                    .replace("#{end_date}#", params.endDate);
            }

            return HttpAppService.get({
                url: url
            });
        }
        function queryOutputTargetListCount(params){
            var url = HttpAppService.URLS.RESOURCE_STATUS_DETAIL_COUNT
                .replace("#{site}#", HttpAppService.getSite())
                .replace("#{work_area}#", params.workCenter)
                .replace("#{line_area}#", params.line)
                .replace("#{resource_type}#", params.resourceType)
                .replace("#{resource_code}#", params.resrce)
                .replace("#{shift}#", params.shift)
                .replace("#{start_date}#", params.startDate)
                .replace("#{end_date}#", params.endDate);
            return HttpAppService.get({
                url: url
            });
        }

        function resourcetypeList(resrceType){
            var url = HttpAppService.URLS.ALARM_RESRCE_TYPE_ALL
                .replace("#{SITE}#", HttpAppService.getSite())
                .replace("#{RESRCE_TYPE}#", resrceType);
            return HttpAppService.get({
                url: url
            });
        }


        function ShutdownReasonEchart(params){
            var url = HttpAppService.URLS.SHUTDOWN_REASON_REPORT
	            .replace("#{site}#",HttpAppService.getSite())
	            .replace("#{data_type}#",'CHAR')
	            .replace("#{work_center}#", params.workCenter)
	            .replace("#{line_in}#", params.lineArea)
	            .replace("#{resource_type}#",params.resourceType)
	            .replace("#{resource_code}#",params.resource)
	            .replace("#{start_date}#", params.startDate)
	            .replace("#{end_date}#", params.endDate)
	            .replace("#{by_timetype}#", params.byTimeType)
	            .replace("#{shift}#", params.shift)
	            .replace("#{by_type}#", params.byType);
            return HttpAppService.get({
                url: url
            });
        }

        function ShutdownReasonList(params){
            var url = HttpAppService.URLS.SHUTDOWN_REASON_REPORT
                .replace("#{site}#",HttpAppService.getSite())
                .replace("#{data_type}#",'GRID')
                .replace("#{work_center}#", params.workCenter)
                .replace("#{line_in}#", params.lineArea)
                .replace("#{resource_type}#",params.resourceType)
                .replace("#{resource_code}#",params.resource)
                .replace("#{start_date}#", params.startDate)
                .replace("#{end_date}#", params.endDate)
                .replace("#{by_timetype}#", params.byTimeType)
                .replace("#{shift}#", params.shift)
                .replace("#{by_type}#", params.byType);
            return HttpAppService.get({
                url: url
            });
        }
        
        
        function shutdownReasonExport(params){
            var url = HttpAppService.URLS.SHUTDOWN_REASON_EXPORT
                .replace("#{site}#",HttpAppService.getSite())
                .replace("#{data_type}#",'GRID')
                .replace("#{work_center}#", params.workCenter)
                .replace("#{line_in}#", params.lineArea)
                .replace("#{resource_type}#",params.resourceType)
                .replace("#{resource_code}#",params.resource)
                .replace("#{start_date}#", params.startDate)
                .replace("#{end_date}#", params.endDate)
                .replace("#{by_timetype}#", params.byTimeType)
                 .replace("#{shift}#", params.shift)
                .replace("#{by_type}#", params.byType);
                return url;
        }
        
        function produceMicroPlanList(params){
			var url = HttpAppService.URLS.PRODUCE_MOCRO_PLAN_REPORT
		        .replace("#{site}#",HttpAppService.getSite())
		        .replace("#{work_center}#", params.workCenter)
		        .replace("#{line_in}#", params.lineArea)
		        .replace("#{resource_type}#",params.resourceType)
		        .replace("#{resource_code}#",params.resource)
		        .replace("#{start_date}#", params.startDate)
		        .replace("#{end_date}#", params.endDate)
		        .replace("#{model}#", params.model)
		        .replace("#{item}#", params.item)
		        .replace("#{colNum}#", params.colNum);
		    return HttpAppService.get({
		        url: url
		    });
		}
        
        function produceMicroPlanExport(params){
			var url = HttpAppService.URLS.PRODUCE_MOCRO_PLAN_EXPORT
		        .replace("#{site}#",HttpAppService.getSite())
		        .replace("#{work_center}#", params.workCenter)
		        .replace("#{line_in}#", params.lineArea)
		        .replace("#{resource_type}#",params.resourceType)
		        .replace("#{resource_code}#",params.resource)
		        .replace("#{start_date}#", params.startDate)
		        .replace("#{end_date}#", params.endDate)
		        .replace("#{model}#", params.model)
		        .replace("#{item}#", params.item)
		        .replace("#{colNum}#", params.colNum);
			return url;
		}
        
        function __OEETrendAnalysisGridList(params){
            var url = HttpAppService.URLS.OEE_TREND_ANALYSIS_REPORT
                .replace("#{site}#",HttpAppService.getSite())
                .replace("#{work_area}#", params.workCenter)
                .replace("#{line_area}#", params.lineArea)
                .replace("#{resource_type}#",params.resourceType)
                .replace("#{resource_code}#",params.resource)
                .replace("#{start_date}#", params.startDate)
                .replace("#{end_date}#", params.endDate)
                .replace("#{byTimeType}#", params.byTimeType)
                .replace("#{byResrceType}#", params.byType)
                .replace("#{model}#", params.model)
                .replace("#{pro}#", params.prd)
                .replace("#{shift}#", params.shift);
            return HttpAppService.get({
                url: url
            });
        }

        //宏观状态列表接口
        function __deviceGlobalStatusList(params){
            var url = HttpAppService.URLS.DEVICE_GLOBAL_STATUS_REPORT
                .replace("#{work_area_list}#", params.workCenter)
                .replace("#{line_area_list}#", params.lineArea)
                .replace("#{source_type_list}#",params.resourceType)
                .replace("#{site_list}#",params.sites)
                .replace("#{dimension}#",params.dimension)
            return HttpAppService.get({
                url: url
            });
        }

        //设备宏观状态各种颜色灯列表
        function dataGlobalListByColor(params) {
            var url = HttpAppService.URLS.DEVICE_GLOBAL_LIST_QUERY_BY_COLOR
                .replace("#{dimensionCode}#", params.dimensionCode)
                .replace("#{dimension}#", params.dimension)
                .replace("#{work_area_list}#", params.workCenter)
                .replace("#{line_area_list}#", params.lineArea)
                .replace("#{source_type_list}#",params.resourceType)
                .replace("#{site_list}#",params.sites)
                .replace("#{color}#",params.color);
            return HttpAppService.get({
                url: url
            });
        }

        function __OEETrendAnalysisExport(params){
            var url = HttpAppService.URLS.OEE_TREND_ANALYSIS_EXPORT
                .replace("#{site}#",HttpAppService.getSite())
                .replace("#{work_area}#", params.workCenter)
                .replace("#{line_area}#", params.lineArea)
                .replace("#{resource_type}#",params.resourceType)
                .replace("#{resource_code}#",params.resource)
                .replace("#{start_date}#", params.startDate)
                .replace("#{end_date}#", params.endDate)
                .replace("#{byTimeType}#", params.byTimeType)
                .replace("#{byResrceType}#", params.byType)
                .replace("#{model}#", params.model)
                .replace("#{pro}#", params.prd)
                .replace("#{shift}#", params.shift);
            return url;
        }
    }]);



statementModule
    .run([
        '$templateCache',
        function ($templateCache){

            $templateCache.put('andon-ui-grid-rscg/statement-checkbox',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents flex-center\"  ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><input type=\"checkbox\" class=\"checkbox-column\" ng-disabled='true'  ng-checked =\"row.entity.enabled\" /></div>"
            );
        }]);

//选择月的控件
statementModule
    .directive('dateDirective', ['statementService',function(statementService) {
        return {
            restrict: 'EA',
            //replace: true,
            scope : {
                dateYearMouth : "=",
                dateDiv : "=",
                dateStatus : "=",
                dateYearMouthDay : "=",
                //yearData : "@yearData",
                //dataMouth : "@dataMouth",
            },
            //transclude: true,
            controller : ['$scope','$element',function($scope,$element){
                if($scope.dateYearMouth == "Invalid Date"){
                    $scope.dateDiv = false;
                    return;
                }
                //$scope.dateDiv = true;
                $scope.dataMouth = statementService.mouth;
                //console.log("dataMouth");
                //console.log($scope.dataMouth);
                $scope.selectedYear = new Date($scope.dateYearMouth).getFullYear();
                $scope.selectedMouth = new Date($scope.dateYearMouth).getMonth()+1;
                $scope.initDate = function(){
                    $scope.dateFirst = 'Y';
                    //console.log($scope.dateDiv);
                    $scope.yearData = [
                        {num : $scope.selectedYear-1,selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false},
                        {num : $scope.selectedYear,selected : true,mouseoverClass : false,mouseleaveClass : false,currentDate : false},
                        {num : $scope.selectedYear+1,selected : false,mouseoverClass : false,mouseleaveClass : false,currentDate : false},
                    ];
                    for(var i=0;i<$scope.dataMouth.length;i++){
                        for(var j=0;j<$scope.dataMouth[i].length;j++){
                            if($scope.dataMouth[i][j].num == $scope.selectedMouth){
                                $scope.dataMouth[i][j].selected = true;
                            }else{
                                $scope.dataMouth[i][j].selected = false;
                            }
                            if((new Date().getMonth()+1) == $scope.dataMouth[i][j].num && $scope.dataMouth[i][j].selected == false){
                                $scope.dataMouth[i][j].currentDate = true;
                                //console.log((new Date().getMonth()+1));
                            }else{
                                $scope.dataMouth[i][j].currentDate = false;
                            }
                        }
                    }
                    //console.log($scope.yearData);
                    //console.log($scope.dataMouth);
                }


                $scope.goLeft = function(){
                    for(var i=0;i<$scope.yearData.length;i++){
                        $scope.yearData[i].num = $scope.yearData[i].num - 1;
                        if($scope.selectedYear == $scope.yearData[i].num){
                            $scope.yearData[i].selected = true;
                        }else{
                            $scope.yearData[i].selected = false;
                        }
                        if((new Date().getFullYear()) == $scope.yearData[i].num && $scope.yearData[i].selected == false){
                            //console.log(new Date().getFullYear());
                            //console.log($scope.yearData[i].num);
                            $scope.yearData[i].currentDate = true;
                        }else{
                            $scope.yearData[i].currentDate = false;
                        }
                    }
                }
                $scope.goRight = function(){
                    $scope.$emit('dateDivShow', true);
                    for(var i=0;i<$scope.yearData.length;i++){
                        $scope.yearData[i].num = $scope.yearData[i].num + 1;
                        if($scope.selectedYear == $scope.yearData[i].num){
                            $scope.yearData[i].selected = true;
                        }else{
                            $scope.yearData[i].selected = false;
                        }
                        if((new Date().getFullYear()) == $scope.yearData[i].num && $scope.yearData[i].selected == false){
                            $scope.yearData[i].currentDate = true;
                        }else{
                            $scope.yearData[i].currentDate = false;
                        }
                    }
                }
                $scope.selectDateYear = function(item){
                    for(var i=0;i<$scope.yearData.length;i++){
                        $scope.yearData[i].selected = false;
                        if((new Date().getFullYear()) == $scope.yearData[i].num && $scope.yearData[i].selected == false){
                            $scope.yearData[i].currentDate = true;
                        }else{
                            $scope.yearData[i].currentDate = false;
                        }
                    }
                    item.selected = true;
                    item.currentDate = false;
                    $scope.selectedYear = item.num;
                }
                $scope.selectDateMouth = function(item){
                    for(var i=0;i<$scope.dataMouth.length;i++){
                        $scope.dataMouth[i].selected = false;
                    }
                    item.selected = true;
                    $scope.selectedMouth = item.num;
                    $scope.dateDiv = false;
                    $scope.mouseleave(item);
                    if($scope.selectedMouth <10){
                        if($scope.dateStatus == 'start'){
                            $scope.dateYearMouthDay = $scope.selectedYear+"-0"+$scope.selectedMouth+"-01";
                        }else{
                            $scope.dateYearMouthDay = __getLastDate($scope.selectedYear,$scope.selectedMouth);
                        }
                        $scope.dateYearMouth = $scope.selectedYear+"-0"+$scope.selectedMouth;
                        //console.log($scope.dateYearMouth);
                        //console.log($scope.dateYearMouthDay);
                    }else{
                        if($scope.dateStatus == 'start'){
                            $scope.dateYearMouthDay = $scope.selectedYear+"-0"+$scope.selectedMouth+"-01";
                        }else{
                            $scope.dateYearMouthDay = __getLastDate($scope.selectedYear,$scope.selectedMouth);
                        }
                        $scope.dateYearMouth = $scope.selectedYear+"-"+$scope.selectedMouth;
                        //console.log($scope.dateYearMouth);
                        //console.log($scope.dateYearMouthDay);
                    }
                }
                function __getLastDate(year,mouth){
                    var date = new Date(year,mouth,0);
                    var day = date.getDate();
                    return year+"-"+mouth+"-"+day;
                }
                $scope.mouseover = function(item){
                    if(item.selected == true){
                        item.selected = false;
                        $scope.numDate =0;
                    }
                    item.mouseoverClass = true;
                    item.mouseleaveClass = false;
                }
                $scope.mouseleave = function(item){
                    item.mouseoverClass = false;
                    item.mouseleaveClass = true;
                    if($scope.numDate == 0){
                        item.selected = true;
                        $scope.numDate = 1;
                    }
                }
                $scope.dateFocus = function(){
                    $scope.dateDiv = true;
                }
                $scope.dateBlur = function(){
                    $scope.dateDiv = false;
                    //console.log($scope.dateDiv);
                }
                $scope.$on('dateMouthHide', function(event,dateMouthHide) {
                    if(dateMouthHide == 'W' && $scope.clickDateDiv == "N"){
                        $scope.clickDateDiv = '';
                    }else if(dateMouthHide == 'W' && $scope.clickDateDiv == ''){
                        if($scope.dateDiv == true && $scope.dateFirst == 'N'){
                            $scope.dateDiv = false;
                        }else if($scope.dateDiv == true && $scope.dateFirst == 'Y'){

                        }
                        $scope.dateFirst = 'N';
                    }
                });
                $scope.clickDateDiv = '';
                $scope.dateMouthShow = function(){
                    $scope.clickDateDiv = 'N';
                }
            }],
            templateUrl:"modules/statement/dateYearMouth.html",
        };
    }]);
statementModule
    .run([
        '$templateCache',
        function ($templateCache){
            $templateCache.put('andon-ui-grid-threeLight/device-timely',
                "<div class=\"ui-grid-cell-contents flex-center\" title=\"{{col.cellTooltip(row,col)}}\">" +
                "<div ng-switch=\"row.entity.colorLight\">" +
                "<img ng-switch-when=\"R\" src=\"images/red.gif\" width=\"20\" height=\"20\" />" +
                "<img ng-switch-when=\"G\" src=\"images/green.gif\" width=\"20\" height=\"20\" />" +
                "<img ng-switch-when=\"Y\" src=\"images/yellow.gif\" width=\"20\" height=\"20\" />" +
                "</div></div>"
            );

        }]);