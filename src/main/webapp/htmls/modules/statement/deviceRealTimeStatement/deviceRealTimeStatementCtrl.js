statementModule.controller('deviceRealTimeStatementCtrl', [
    '$scope', '$http', 'HttpAppService', '$timeout', 'plcService',
    'homeService', 'statementService',
    'uiGridConstants', '$q', 'UtilsService', 'uiGridValidateService','$uibModal',
    function ($scope, $http, HttpAppService, $timeout, plcService, homeService, statementService,
              uiGridConstants, $q, UtilsService, uiGridValidateService,$uibModal) {

        $scope.netInfoGridOptions = {
            enablePagination: false,
            totalItems: 0,
            useExternalPagination: false,
            showGridFooter: false,
            modifierKeysToMultiSelectCells: false,
            data: [],
            columnDefs: [
                {
                    //设备编码
                    field: 'resrce',
                    name: 'resrce',
                    displayName: '\u8BBE\u5907\u7F16\u7801',
                    visible: true,
                    minWidth: 100,
                    enableCellEdit: false,
                    enableCellEditOnFocus: false,
                    visible: true,
                    validators: {
                        required: false
                    },
                   /* cellClass: function (row, col) {
                        switch (col.entity.colorLight) {
                            case 'R' :
                                return 'grid-no-editable high-light-red';
                                break;

                            case 'Y' :
                                return 'grid-no-editable high-light-yellow';
                                break;
                        }
                    },*/
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    //设备名称
                    field: 'resrceDes',
                    name: 'resrceDes',
                    displayName: '\u8BBE\u5907\u540D\u79F0',
                    visible: true,
                    minWidth: 300,
                    enableCellEdit: false,
                    enableCellEditOnFocus: false,
                    validators: {
                        required: true
                    },
                    /*cellClass: function (row, col) {
                        switch (col.entity.colorLight) {
                            case 'R' :
                                return 'grid-no-editable high-light-red';
                                break;

                            case 'Y' :
                                return 'grid-no-editable high-light-yellow';
                                break;
                        }
                    },*/
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    //拉线
                    field: 'lineAreaDes',
                    name: 'lineAreaDes',
                    displayName: '\u62c9\u7ebf',
                    visible: true,
                    minWidth: 150,
                    enableCellEdit: false,
                    enableCellEditOnFocus: false,
                    validators: {
                        required: true
                    },
                    /*cellClass: function (row, col) {
                        switch (col.entity.colorLight) {
                            case 'R' :
                                return 'grid-no-editable high-light-red';
                                break;

                            case 'Y' :
                                return 'grid-no-editable high-light-yellow';
                                break;
                        }
                    },*/
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    //运行状态
                    field: 'resourceState',
                    name: 'resourceState',
                    displayName: '\u8FD0\u884C\u72B6\u6001',
                    visible: true,
                    minWidth: 80,
                    enableCellEdit: false,
                    enableCellEditOnFocus: false,
                    validators: {
                        required: true
                    },
                    /*cellClass: function (row, col) {
                        switch (col.entity.colorLight) {
                            case 'R' :
                                return 'grid-no-editable high-light-red';
                                break;

                            case 'Y' :
                                return 'grid-no-editable high-light-yellow';
                                break;
                        }
                    },*/
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
             /*   {
                    //PPM
                    field: 'intPPM',
                    name: 'intPPM',
                    displayName: '\u5B9E\u65F6PPM',
                    visible: false,
                    minWidth: 100,
                    enableCellEdit: false,
                    enableCellEditOnFocus: false,
                    validators: {
                        required: true
                    },
                    cellClass: function (row, col) {
                        switch (col.entity.colorLight) {
                            case 'R' :
                                return 'text-align-right';
                                break;

                            case 'Y' :
                                return 'text-align-right';
                                break;
                            default :
                                return 'text-align-right';
                                break;
                        }
                    },
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },*/
                {
                    //三色灯
                    field: 'colorLight',
                    name: 'colorLight',
                    displayName: '\u4E09\u8272\u706F',
                    visible: true,
                    minWidth: 80,
                    enableCellEdit: false,
                    enableCellEditOnFocus: false,
                    cellTemplate: 'andon-ui-grid-threeLight/device-timely',
                    validators: {
                        required: true
                    },
                    cellClass: function (row, col) {
                        switch (col.entity.colorLight) {
                            case 'R' :
                                return 'high-light-red';
                                break;

                            case 'Y' :
                                return 'high-light-yellow';
                                break;
                        }
                    },
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {

                    //物料编码    displayName: '\u62A5\u8B66\u6B21\u6570',
                    field: 'item',
                    name: 'item',
                    displayName: '物料编码',
                    visible: true,
                    minWidth: 190,
                    enableCellEdit: false,
                    enableCellEditOnFocus: false,
                    //cellTemplate: 'andon-ui-grid-tpls/addinfopart-plcRealTime',
                    validators: {
                        required: true
                    },
                    /*cellClass: function (row, col) {
                        switch (col.entity.colorLight) {
                            case 'R' :
                                return 'text-align-right';
                                break;

                            case 'Y' :
                                return 'text-align-right';
                                break;
                            default :
                                return 'text-align-right';
                                break;
                        }
                    },*/
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    //报警信息
                    field: 'description',
                    name: 'description',
                    displayName: '报警信息',
                    visible: true,
                    minWidth: 300,
                    enableCellEdit: false,
                    enableCellEditOnFocus: false,
                    validators: {
                        required: true
                    },
                   /* cellClass: function (row, col) {
                        switch (col.entity.colorLight) {
                            case 'R' :
                                return 'text-align-right';
                                break;

                            case 'Y' :
                                return 'text-align-right';
                                break;
                            default :
                                return 'text-align-right';
                                break;
                        }
                    },*/
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    //总产量
                    field: 'strQtyTotal',
                    name: 'strQtyTotal',
                    displayName: '\u603b\u4ea7\u91cf',
                    visible: true,
                    minWidth: 90,
                    enableCellEdit: false,
                    enableCellEditOnFocus: false,
                    validators: {
                        required: true
                    },
                    type:'numberStr',
                    cellClass: function (row, col) {
                        switch (col.entity.colorLight) {
                            case 'R' :
                                return 'text-align-right';
                                break;
                            case 'Y' :
                                return 'text-align-right';
                                break;
                            default :
                                return 'text-align-right';
                                break;
                        }
                    },
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                }
            ],
            onRegisterApi:__requestGridApi
        };

        $scope.config = {
            gridApi: null,
            $workCenterSelect: null,
            $lineSelect: null,
            $deviceTypeSelect: null,
            $deviceNumSelect: null,

            btnDisabledQuery: false,

            defaultSelectItem: {
                descriptionNew: "--- \u8BF7\u9009\u62E9 ---",
                DESCRIPTION: "---\u8BF7\u9009\u62E9 ---",
                WORK_CENTER: null
            },
            defaultSelectItemType: {
                descriptionNew: "--- \u8BF7\u9009\u62E9 ---",
                DESCRIPTION: "---\u8BF7\u9009\u62E9 ---",
                RESOURCE_TYPE: ""
            },
            defaultSelectItemCode: {
                descriptionNew: "--- \u8BF7\u9009\u62E9 ---",
                DESCRIPTION: "--- \u8BF7\u9009\u62E9 ---",
            },

            plcProtocols: [],

            showTable: false,
            workCenter: null,
            line: null,
            showSelectLine: true,
            deviceCode : true,
            showSelectArea : true,
            deviceType : true,
            showLineDesc : false,
            showDeviceDesc : false,
            deviceTypeDesc : null
        };
        $scope.enableBtn=function(){
        	if($scope.statementConfig.multipleCurrentDeviceNum==''){
        		$scope.statementConfig.queryBtn=true;
        	}else{
        		$scope.statementConfig.queryBtn=false;
        	}
        }
        //生产区域帮助
        $scope.toAreaHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/areaSelectMoreModal.html',
                controller: 'areaSelectMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectAreaModal: function () {
                        return {workArea : $scope.statementConfig.workCenters};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentWorkCenter = selectedItem.area.join(",");
                $scope.statementConfig.currentWorkCenterDesc = selectedItem.areaName.join(",");
                $scope.config.showSelectArea = false;
                $scope.statementConfig.queryBtn=false;
                $scope.deleteAll('line');
                $scope.deleteAll('deviceCode');
            }, function () {
            });
        };
        //拉线多选帮助
        $scope.toWireLineHelp = function () {
            /*if($scope.statementConfig.currentWorkCenter == ''){
                $scope.addAlert('','请先选择生产区域');
                return;
            }*/
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/lineSelectModal.html',
                controller: 'LineSelectMoreCtrl',
                size: 'lg',
                backdrop: 'false',
                scope: $scope,
                openedClass: 'flex-center-parent',
                resolve: {
                    selectLineModal: function () {
                        return {
                        	workArea: $scope.statementConfig.currentWorkCenter,
                        	workLine: $scope.statementConfig.lines
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentLine = selectedItem.line;
                $scope.statementConfig.currentLineDesc = selectedItem.lineName;
                if($scope.statementConfig.currentLine.length >0){
                    $scope.config.showSelectLine = false;
                }
                $scope.deleteAll('deviceCode');
                $scope.statementConfig.queryBtn=false;
                //$scope.lineChanged('T');
            }, function () {
            });
        };
        //设备类型帮助
        $scope.toDeviceTypeHelp = function(){
            /*if($scope.statementConfig.currentWorkCenter == ''){
                $scope.addAlert('','请先选择生产区域');
                return;
            }*/
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/deviceTypeSelectMoreModal.html',
                controller: 'deviceTypeMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectTypeMoreDevice: function () {
                        return {
                        	deviceCode : $scope.statementConfig.deviceTypes
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentDeviceType = selectedItem.resourceType;
                $scope.statementConfig.currentDeviceTypeDesc = selectedItem.resourceTypeDesc;
                if($scope.statementConfig.currentDeviceType.length >0){
                    $scope.config.deviceType = false;
                }
                $scope.deleteAll('deviceCode');
                $scope.statementConfig.queryBtn=false;
                //$scope.deviceTypeChanged('T');
            }, function () {
            });
        };
        //设备帮助
        $scope.toDeviceHelp = function(){
            /*if($scope.statementConfig.currentWorkCenter == ''){
                $scope.addAlert('','请先选择生产区域');
                return;
            }*/
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/modalDeviceHelp.html',
                controller: 'deviceMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectMoreDevice: function () {
                        return {
                        	workArea: $scope.statementConfig.currentWorkCenter,
                        	workLine: $scope.statementConfig.currentLine,
                        	deviceCode : $scope.statementConfig.currentDeviceType
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.multipleCurrentDeviceNum = selectedItem.area;
                if($scope.statementConfig.multipleCurrentDeviceNum.length >0){
                    $scope.config.deviceCode = false;
                }
                $scope.statementConfig.queryBtn=false;
            }, function () {
            });
        };
        //展示数据分析modal
        $scope.showDataDescription=function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/alertDataDescriptionModal.html',
                controller: 'DataDescriptionCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectDescriptionData: function () {
                        return {

                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {

            }, function () {
            });
        };
        $scope.deleteAll = function(item){
            if(item == 'area'){
                $scope.statementConfig.currentWorkCenter = [];
                $scope.statementConfig.currentWorkCenterDesc = [];
                $scope.statementConfig.currentLine = [];
                $scope.statementConfig.currentLineDesc = [];
                $scope.statementConfig.deviceNums = [];
                $scope.statementConfig.currentDeviceNum = null;
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.showSelectArea = true;
                $scope.config.showSelectLine = true;
                $scope.config.deviceCode = true;
                $scope.statementConfig.btnDisabledQuery = true;
            }
            if(item == 'line'){
                $scope.statementConfig.currentLine = [];
                $scope.statementConfig.currentLineDesc = [];
                $scope.statementConfig.deviceNums = [];
                $scope.statementConfig.currentDeviceNum = null;
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.showSelectLine = true;
                $scope.config.deviceCode = true;
                $scope.lineChanged('T');
            }
            if(item == 'deviceType'){
                $scope.statementConfig.currentDeviceType = [];
                $scope.statementConfig.currentDeviceTypeDesc = [];
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.deviceType = true;
                $scope.config.deviceCode = true;
                $scope.deviceTypeChanged('T');
            }
            if(item == 'deviceCode'){
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.deviceCode = true;
            }
            if($scope.statementConfig.currentWorkCenter == ''&&$scope.statementConfig.currentLine== ''&&$scope.statementConfig.currentDeviceType== ''&&$scope.statementConfig.multipleCurrentDeviceNum==''){
        		$scope.statementConfig.queryBtn=true;
        	}
            
        }

        $scope.init = function () {
            $scope.statementConfig.currentWorkCenter = [];
            $scope.statementConfig.currentWorkCenterDesc = [];
            $scope.statementConfig.currentLine = [];
            $scope.statementConfig.currentLineDesc = [];
            $scope.statementConfig.currentDeviceNum = null;
            $scope.statementConfig.multipleCurrentDeviceNum = [];
            $scope.statementConfig.currentDeviceType = [];
            $scope.statementConfig.currentDeviceTypeDesc = [];
            $scope.statementConfig.btnDisabledQuery = true;
            $scope.statementConfig.localData = [];
        };

        $scope.init();

        $scope.queryDatas = function () {
            $timeout.cancel(window.recTime);
            search();
        };

        function search() {
        	//$scope.showBodyModel("正在加载数据,请稍后...");
        	if($scope.statementConfig.currentWorkCenter == ''&&$scope.statementConfig.currentLine== ''&&$scope.statementConfig.currentDeviceType== ''&&$scope.statementConfig.multipleCurrentDeviceNum==''){
        		//$scope.addAlert('','查询条件不可以为空');
        		$scope.statementConfig.queryBtn=true;
        	}else{
        		$scope.statementConfig.queryBtn=false;
        	}
            var workCenter = $scope.statementConfig.currentWorkCenter;
            
            var line = [];
            for (var i = 0; i < $scope.statementConfig.currentLine.length; i++) {
                line.push($scope.statementConfig.currentLine[i]);
            }
            var deviceType = $scope.statementConfig.currentDeviceType == null ? "" : $scope.statementConfig.currentDeviceType;
            if($scope.statementConfig.currentDeviceType == '' || $scope.statementConfig.currentDeviceType == null){
                $scope.config.showDeviceDesc = false;
            }else{
                $scope.config.showDeviceDesc = true;
            }
            $scope.config.deviceTypeDesc = angular.copy($scope.statementConfig.currentDeviceTypeDesc);
            if($scope.config.deviceTypeDesc.length>1){
                $scope.config.deviceTypeDesc = $scope.config.deviceTypeDesc[0]+'...';
            }else{
                $scope.config.deviceTypeDesc = $scope.config.deviceTypeDesc[0];
            }
            var deviceCode = $scope.statementConfig.multipleCurrentDeviceNum;

            var desString = [];
            for (var i = 0; i < $scope.statementConfig.currentLine.length; i++) {
                desString[i] = $scope.statementConfig.currentLine[i];
            }
            $scope.config.workCenter = null;
            $scope.config.line = null;

            /*$scope.config.workCenter = $scope.statementConfig.currentWorkCenterDesc.split(',');
            if($scope.config.workCenter.length>1){
                $scope.config.workCenter = $scope.config.workCenter[0]+'...';
            }else{
                $scope.config.workCenter = $scope.config.workCenter[0];
            }*/
            
            if($scope.statementConfig.currentLineDesc.length>0){
                $scope.config.showLineDesc = true;
            }else{
                $scope.config.showLineDesc = false;
            }
            $scope.config.line = $scope.statementConfig.currentLineDesc;
            if($scope.config.line.length>1){
                $scope.config.line = $scope.config.line[0]+'...';
            }else{
                $scope.config.line = $scope.config.line[0];
            }
            $scope.netInfoGridOptions.data = $scope.statementConfig.localData;


            statementService.DeviceRealTimeQuery(workCenter, line, deviceType, deviceCode.toString()).then(function (resultDatas) {
            	//$scope.hideBodyModel();
                var array = resultDatas.response;
                UtilsService.log("返回数据："+angular.toJson(resultDatas.response,true));
                $scope.config.dataNum = resultDatas.response.length;

                $scope.netInfoGridOptions.data = array;
                $scope.statementConfig.localData=resultDatas.response;
                if (array.length > 0) {
                    $scope.config.showTable = true;
                } else {
                    $scope.config.showTable = false;
                    $scope.addAlert('', '暂无数据');
                }
            }, function (resultDatas){
            	$scope.hideBodyModel();
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });

            window.recTime = $timeout(function () {
            	search();
            }, 72000);
        };
        

        $scope.getTableHeight = function () {
            if ($scope.netInfoGridOptions.data.length > 0) {
                var rowHeight = 31;
                var headerHeight = 200;
                return {
                    height: ($scope.netInfoGridOptions.data.length * rowHeight + headerHeight) + "px"
                };
            }
        };

        function __requestGridApi(gridApi){
            $scope.config.gridApi = gridApi;
        }
        //控制显示隐藏列
        $scope.showHideCol=function () {
            for(var i=0;i<$scope.netInfoGridOptions.columnDefs.length;i++) {
                if (!$scope.netInfoGridOptions.columnDefs[i].visible) {
                    $scope.netInfoGridOptions.columnDefs[i].visible = true;
                }
            }
            $scope.config.gridApi.core.refresh();
        };


    }]);