statementModule.controller('PPMStatementCtrl',['$scope', '$http',
    'HttpAppService', '$timeout',
    'uiGridConstants','$uibModal','$echarts','$interval','statementService','UtilsService',
    function ($scope, $http,
              HttpAppService, $timeout,
              uiGridConstants,$uibModal,$echarts,$interval,statementService,UtilsService) {
        $scope.config ={
            outputStatement : 'PPM报表',
            gridApi: null,
            workArea : "",
            workLine : "",
            workAreaName : "",
            workLineName : "",
            deviceCode : "",
            deviceCodeArray:[],
            model : "",
            itemCode : "",

            startDate : UtilsService.serverFommateDateShow(new Date()),
            endDate : UtilsService.serverFommateDateShow(new Date()),
            startDateMouth : UtilsService.serverFormateDateMouth(new Date()),
            endDateMouth : UtilsService.serverFormateDateMouth(new Date()),
            selectType : true,

            type : 'DEV',
            dateType : "M",

            showSelectArea : true,
            showSelectLine : true,

            showProArea : false,
            showDevice : true,
            showModel : false,
            showCode : false,
            showLine : false,

            selectMouth : true,
            selectWeek : false,
            selectDay : false,
            selectShift : false,

            queryChartShow : false,

            queryGridAreaShow : false,
            queryGridLineShow : false,
            queryGridDeviceShow : false,
            queryGridAreaCodeShow : false,
            addInfosShow : false,

            queryBtn : true,
            pullFileBtn : true,



            //报表
            outPutStatementInstance : null,
            dataEcharts : null,
            dateTypeEcharts : null,
            itemCodeSelected : [],
            itemCodeSelectedCopy : [],
            currentItemCode : null,
            xbrEchartsPPMInstance : null,
            dataNum : 0
        };
        //区域
        $scope.gridOutputAreaStatement = {
            columnDefs:[
                {name:"workArea",displayName:'生产区域', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"workAreaDes",displayName:'生产区域描述', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"model",displayName:'Model', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"item",displayName:'物料编码', minWidth: 200, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"byTime",displayName:'月/周/天/班次', minWidth: 230, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"qtySum",displayName:'生产总数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"qtyYieldResult",displayName:'初次良品数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"qtyScrapResult",displayName:'初次坏品数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"qtyUnconfirm",displayName:'待确认数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"firstCal",displayName:'一次优', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"qtyYield",displayName:'最终良品数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"qtyScrap",displayName:'最终坏品数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"resultCal",displayName:'最终优', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,}],
            onRegisterApi:__requestGridApi
        };

        //拉线
        $scope.gridOutputLineStatement = {
            columnDefs:[
                {name:"workArea",displayName:'生产区域', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"workAreaDes",displayName:'生产区域描述', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"lineArea",displayName:'拉线', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"lineAreaDes",displayName:'拉线描述', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"model",displayName:'Model', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"item",displayName:'物料编码', minWidth: 200, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"byTime",displayName:'月/周/天/班次', minWidth: 230, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"qtySum",displayName:'生产总数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"qtyYieldResult",displayName:'初次良品数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"qtyScrapResult",displayName:'初次坏品数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"qtyUnconfirm",displayName:'待确认数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"firstCal",displayName:'一次优', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"qtyYield",displayName:'最终良品数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"qtyScrap",displayName:'最终坏品数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"resultCal",displayName:'最终优', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,}],
            onRegisterApi:__requestGridApi
        };
        //设备
        $scope.gridOutputDeviceStatement = {
            columnDefs:[
                {name:"workArea",displayName:'生产区域', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"workAreaDes",displayName:'生产区域描述', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"lineArea",displayName:'拉线', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"lineAreaDes",displayName:'拉线描述', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"resrce",displayName:'设备号', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"description",displayName:'设备描述', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"asset",displayName:'资产号', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"model",displayName:'Model', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"item",displayName:'物料编码', minWidth: 200, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"byTime",displayName:'月/周/天/班次', minWidth: 230, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"ppmTheory",displayName:'理论PPM', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"},
                {name:"ppm",displayName:'实际PPM', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"},
                {name:"firstCal",displayName:'达成率', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"}],
            onRegisterApi:__requestGridApi
        };
        //物料编码
        $scope.gridOutputCodeStatement = {
            columnDefs:[
                {name:"model",displayName:'Model', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"item",displayName:'物料编码', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"workArea",displayName:'生产区域', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"workAreaDes",displayName:'生产区域描述', minWidth: 200, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"lineArea",displayName:'拉线', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"lineAreaDes",displayName:'拉线描述', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"resrce",displayName:'设备号', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"description",displayName:'设备描述', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"asset",displayName:'资产号', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"byTime",displayName:'月/周/天/班次', minWidth: 230, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"ppmTheory",displayName:'理论PPM', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"},
                {name:"ppm",displayName:'实际PPM', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"},
                {name:"firstCal",displayName:'达成率', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"}],
            onRegisterApi:__requestGridApi
        };
//查询
        $scope.queryDatas = function(){
            $scope.showBodyModel("正在加载数据,请稍后...");
            $scope.gridOutputAreaStatement.data = [];
            $scope.gridOutputLineStatement.data = [];
            $scope.gridOutputDeviceStatement.data = [];
            $scope.gridOutputCodeStatement.data = [];
            $scope.config.queryChartShow = false;
            $scope.config.addInfosShow = false;

            $scope.config.queryGridAreaShow = false;
            $scope.config.queryGridLineShow = false;
            $scope.config.queryGridDeviceShow = false;
            $scope.config.queryGridAreaCodeShow = false;

            $scope.config.itemCodeSelected = [];
            $scope.config.itemCodeSelectedCopy = [];
            $scope.config.currentItemCode = null;

            var resrce = $scope.config.deviceCode;
            var line_area = $scope.config.workLine;
            var work_area = $scope.config.workArea;
            var model = $scope.config.model;
            var item = $scope.config.itemCode;
            var time_type = $scope.config.dateType;
            var condition_type = $scope.config.type;
            if($scope.config.selectType == true){
                if($scope.config.startDateMouth == "" || $scope.config.endDateMouth == ""){
                    $scope.addAlert("","请选择查询时间!");
                    $scope.hideBodyModel();
                    return;
                }
                var start_date_time = UtilsService.getMonthDate(new Date($scope.config.startDateMouth),'start');
                var end_date_time = UtilsService.getMonthDate(new Date($scope.config.endDateMouth),'end');
            }else{
                if($scope.config.startDate == "" || $scope.config.endDate == ""){
                    $scope.addAlert("","请选择查询时间!");
                    $scope.hideBodyModel();
                    return;
                }
                var start_date_time = UtilsService.serverFommateDate(new Date($scope.config.startDate));
                var end_date_time = UtilsService.serverFommateDate(new Date($scope.config.endDate));
            }
            
            if(new Date(start_date_time)>new Date(end_date_time)){
                $scope.addAlert('danger','结束日期必须大于开始日期');
                $scope.hideBodyModel();
                return;
            }
            
            if(time_type=='M'){
                if(UtilsService.addMonth(new Date(start_date_time),6)<new Date(end_date_time)){
                    $scope.addAlert('danger','开始和结束时间跨度不超过六个月');
                    $scope.hideBodyModel();
                    return;
                }
            }else if(time_type=='W'){
                if(UtilsService.addMonth(new Date(start_date_time),3)<new Date(end_date_time)){
                    $scope.addAlert('danger','开始和结束时间跨度不超过三个月');
                    $scope.hideBodyModel();
                    return;
                }
            }else if(time_type=='D'){
                if(UtilsService.addMonth(new Date(start_date_time),1)<new Date(end_date_time)){
                    $scope.addAlert('danger','开始和结束时间跨度不超一个月');
                    $scope.hideBodyModel();
                    return;
                }
            }else if(time_type=='S'){
                if(UtilsService.addMonth(new Date(start_date_time),1)<new Date(end_date_time)){
                    $scope.addAlert('danger','开始和结束时间跨度不超一个月');
                    $scope.hideBodyModel();
                    return;
                }
            }
            if(model == '' || model == null){
                model = [];
            }else{
                model = model.split(',');
            }
            if(item == '' || item == null){
                item = [];
            }else{
                item = item.split(',');
            }
            if(resrce == '' || resrce == null){
                resrce =[];
            }else{
                resrce = resrce.split(',');
            }
            if(line_area == '' || line_area == null){
                line_area = [];
            }else{
                line_area = line_area.split(',');
            }
            if(work_area == '' || work_area == null){
                work_area = [];
            }else{
                work_area = work_area.split(',');
            }
            var paramActions = {
                model : model,
                itemIn : item,
                resrceIn : resrce,
                startDate :start_date_time,
                endDate :end_date_time,
                byConditionType:condition_type,
                lineIn:line_area,
                byTimeType : time_type,
                workCenterIn:work_area
            }
            statementService
                .PPMReportSelect(paramActions)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.dataNum = resultDatas.response.length;
                        $scope.config.queryChartShow = true;
                        $scope.config.addInfosShow = true;
                        if($scope.config.type == "W") {
                            $scope.config.queryGridAreaShow = true;
                            $scope.gridOutputAreaStatement.data = resultDatas.response;
                        }else if($scope.config.type == "L") {
                            $scope.config.queryGridLineShow = true;
                            $scope.gridOutputLineStatement.data = resultDatas.response;
                        }else if($scope.config.type == "DEV") {
                            $scope.config.queryGridDeviceShow = true;
                            $scope.gridOutputDeviceStatement.data = resultDatas.response;
                        }else if($scope.config.type == "ITEM") {
                            $scope.config.queryGridAreaCodeShow = true;
                            $scope.gridOutputCodeStatement.data = resultDatas.response;
                        }
                        $scope.config.dataEcharts = resultDatas.response;
                        $scope.config.pullFileBtn = false;
                        $scope.config.dateTypeEcharts = time_type;
                        for(var i=0;i<$scope.config.dataEcharts.length;i++){
                           $scope.config.itemCodeSelected.push($scope.config.dataEcharts[i].resrce);
                        }
                        $scope.config.itemCodeSelected = __unique1($scope.config.itemCodeSelected).sort();
                        var arr = [];
                        for(var i=0;i<$scope.config.itemCodeSelected.length;i++){
                            arr.push({
                                code : $scope.config.itemCodeSelected[i],
                                desc : '',
                                descNew : ''
                            });
                        }
                        for(var j=0;j<arr.length;j++){
                            for(var i=0;i<resultDatas.response.length;i++){
                               if(arr[j].code == resultDatas.response[i].resrce){
                                   arr[j] = {
                                       code : resultDatas.response[i].resrce,
                                       desc : resultDatas.response[i].description,
                                       descNew : "代码:"+resultDatas.response[i].resrce + " 描述:" + resultDatas.response[i].description
                                   };
                               }
                           }
                        }
                        $scope.config.itemCodeSelected = arr;
                        $scope.config.itemCodeSelectedCopy = angular.copy($scope.config.itemCodeSelected);
                        $scope.config.currentItemCode = $scope.config.itemCodeSelected[0];
                        //$scope.echartsShow();
                        return;
                    }else{
                        $scope.addAlert("","暂无数据");
                        $scope.hideBodyModel();
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    $scope.hideBodyModel();
                });
        }
        //导出数据
        $scope.pullQueryFile = function(){
        	$scope.showBodyModel("正在导出数据,请稍后...");
            var resrce = $scope.config.deviceCode;
            var line_area = $scope.config.workLine;
            var work_area = $scope.config.workArea;
            var model = $scope.config.model;
            var item = $scope.config.itemCode;
            var time_type = $scope.config.dateType;
            var condition_type = $scope.config.type;
            if(model == '' || model == null){
                model = [];
            }else{
                model = model.split(',');
            }
            if(item == '' || item == null){
                item = [];
            }else{
                item = item.split(',');
            }
            if(resrce == '' || resrce == null){
                resrce =[];
            }else{
                resrce = resrce.split(',');
            }
            if(line_area == '' || line_area == null){
                line_area = [];
            }else{
                line_area = line_area.split(',');
            }
            if(work_area == '' || work_area == null){
                work_area = [];
            }else{
                work_area = work_area.split(',');
            }
            if($scope.config.selectType == true){
                var start_date_time = UtilsService.getMonthDate(new Date($scope.config.startDateMouth),'start');
                var end_date_time = UtilsService.getMonthDate(new Date($scope.config.endDateMouth),'end');
            }else{
                var start_date_time = UtilsService.serverFommateDate(new Date($scope.config.startDate));
                var end_date_time = UtilsService.serverFommateDate(new Date($scope.config.endDate));
            }
            var paramActions = {
                model : model,
                itemIn : item,
                resrceIn : resrce,
                startDate :start_date_time,
                endDate :end_date_time,
                byConditionType:condition_type,
                lineIn:line_area,
                byTimeType : time_type,
                workCenterIn:work_area
            }
            var url = statementService
                .PPMReportPull(paramActions);
            url = HttpAppService.handleCommenUrl(url);
            $http({
                url: url,
                method: "POST",
                data: paramActions, //this is your json data string
                headers: {
                    'Content-type': 'application/json'
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                var objectUrl = URL.createObjectURL(blob);
                window.location.href=objectUrl;
                $scope.hideBodyModel();
            }).error(function (data, status, headers, config) {
                //upload failed
            });
        }
        //转换大小写
        $scope.upModel = function(item){
            if(item == "workArea"){
                $scope.config.workArea = angular.uppercase($scope.config.workArea);
            } else if(item == "workLine"){
                $scope.config.workLine = angular.uppercase($scope.config.workLine);
            } else if(item == "deviceCode"){
                $scope.config.deviceCode = angular.uppercase($scope.config.deviceCode);
            } else if(item == "model"){
                $scope.config.model = angular.uppercase($scope.config.model);
            } else if(item == "itemCode"){
                $scope.config.itemCode = angular.uppercase($scope.config.itemCode);
            }
        }
        //查询按钮的控制
        $scope.disableBtn = function(){
            if($scope.config.workArea == null || $scope.config.workArea == ""){
                $scope.config.showSelectArea = true;
            }else{
                $scope.config.showSelectArea = false;
            }
            if($scope.config.workLine == null || $scope.config.workLine == ""){
                $scope.config.showSelectLine = true;
            }else{
                $scope.config.showSelectLine = false;
            }
            if($scope.config.type == "W"){
                if($scope.config.workArea == null || $scope.config.workArea == ""){
                    $scope.config.queryBtn = true;
                    return;
                }else{
                    $scope.config.queryBtn = false;
                }
            }else if($scope.config.type == "L"){
                if($scope.config.workLine == null || $scope.config.workLine == ""){
                    $scope.config.queryBtn = true;
                    return;
                }else{
                    $scope.config.queryBtn = false;
                }
            }else if($scope.config.type == "DEV" ){
                if ($scope.config.deviceCode == null || $scope.config.deviceCode == ""){
                    $scope.config.queryBtn = true;
                    return;
                }else{
                    $scope.config.queryBtn = false;
                }
            }else if($scope.config.type == "ITEM" ){
                if($scope.config.itemCode == null || $scope.config.itemCode == ""){
                    $scope.config.queryBtn = true;
                    return;
                }else{
                    $scope.config.queryBtn = false;
                }
            }
        }
        //按照区域 拉线 设备 型号查询
        $scope.changeSelect = function(type){
            if(type == "W"){
                $scope.config.type = "W"
                $scope.config.showProArea = true;
                $scope.config.showLine = false;
                $scope.config.showDevice = false;
                $scope.config.showCode = false;
            }else if(type == "L"){
                $scope.config.type = "L"
                $scope.config.showProArea = false;
                $scope.config.showLine = true;
                $scope.config.showDevice = false;
                $scope.config.showCode = false;
            }else if(type == "DEV"){
                $scope.config.type = "DEV"
                $scope.config.showProArea = false;
                $scope.config.showLine = false;
                $scope.config.showDevice = true;
                $scope.config.showCode = false;
            }else if(type == "ITEM"){
                $scope.config.type = "ITEM"
                $scope.config.showProArea = false;
                $scope.config.showLine = false;
                $scope.config.showDevice = false;
                $scope.config.showCode = true;
            }
            $scope.disableBtn();
        };
        //按照 月 周 天 班次 汇总
        $scope.changeAddType = function(type){
            $scope.config.startDate = UtilsService.serverFommateDateShow(new Date());
            $scope.config.endDate = UtilsService.serverFommateDateShow(new Date());
            $scope.config.startDateMouth = UtilsService.serverFormateDateMouth(new Date());
            $scope.config.endDateMouth = UtilsService.serverFormateDateMouth(new Date());

            if(type == "M"){
                $scope.config.dateType = "M";
                $scope.config.selectMouth = true;
                $scope.config.selectWeek = false;
                $scope.config.selectDay = false;
                $scope.config.selectShift = false;
                $scope.config.selectType = true;
            }else if(type == "W"){
                $scope.config.dateType = "W";
                $scope.config.selectMouth = false;
                $scope.config.selectWeek = true;
                $scope.config.selectDay = false;
                $scope.config.selectShift = false;
                $scope.config.selectType = false;
            }else if(type == "D"){
                $scope.config.dateType = "D";
                $scope.config.selectMouth = false;
                $scope.config.selectWeek = false;
                $scope.config.selectDay = true;
                $scope.config.selectShift = false;
                $scope.config.selectType = false;
            }else if(type == "S"){
                $scope.config.dateType = "S";
                $scope.config.selectMouth = false;
                $scope.config.selectWeek = false;
                $scope.config.selectDay = false;
                $scope.config.selectShift = true;
                $scope.config.selectType = false;
            }
        };
        //更换物料
        $scope.itemCodeChanged = function($select){
            $scope.config.itemCodeSelected = angular.copy($scope.config.itemCodeSelectedCopy);
            $scope.echartsShow();
        }
        //清除输入框
        $scope.deleteAll = function(item){
            if(item == "area"){
                $scope.config.workAreaName ='';
                $scope.config.workArea ='';
                $scope.config.showSelectArea = true;
            }
            if(item == 'line'){
                $scope.config.workLineName ='';
                $scope.config.workLine ='';
                $scope.config.showSelectLine = true;
            }
            if(item == 'device'){
                $scope.config.deviceCode = '' ;
            }
            if(item == 'model'){
                $scope.config.model = '' ;
            }
            if(item == 'itemCode'){
                $scope.config.itemCode = '' ;
            }

        }
        //生产区域帮助
        $scope.toAreaHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalAreaHelp.html',
                controller: 'areaHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportArea: function () {
                        return {workArea : $scope.config.workArea};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.workArea = selectedItem.area.join(",");
                $scope.config.workAreaName = selectedItem.areaName.join(",");
                $scope.disableBtn();
            }, function () {
            });
        };
        //拉线帮助
        $scope.toWireLineHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalLineHelp.html',
                controller: 'LineHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportLine: function () {
                        return {workLine : $scope.config.workLine,workArea : $scope.config.workArea};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.workLine = selectedItem.line.join(",");
                $scope.config.workLineName = selectedItem.lineName.join(",");
                $scope.disableBtn();
            }, function () {
            });
        };
        //设备帮助
        $scope.toDeviceHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalDeviceHelp.html',
                controller: 'deviceHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportDevice: function () {
                        return {workLine : $scope.config.workLine,workArea : $scope.config.workArea,
                            deviceCode : $scope.config.deviceCode};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.deviceCode = selectedItem.area.join(",");
                $scope.config.deviceCodeArray = selectedItem.selectdDatas
                $scope.disableBtn();
            }, function () {
            });
        };
        //model帮助
        $scope.toModelHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalModelHelp.html',
                controller: 'modelHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportModel: function () {
                        return {model : $scope.config.model};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.model = selectedItem.join(",");
                $scope.disableBtn();
            }, function () {
            });
        }
        //物料编码帮助
        $scope.toCodeHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalCodeHelp.html',
                controller: 'codeHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportItemcode: function () {
                        return {model : $scope.config.model,itemCode:$scope.config.itemCode};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.itemCode = selectedItem.join(",");
                $scope.disableBtn();
            }, function () {
            });
        }
//echarts 图表
        function __unique1(arr){
            var tmpArr = [];
            for(var i=0; i<arr.length; i++){
                //如果当前数组的第i已经保存进了临时数组，那么跳过，
                //否则把当前项push到临时数组里面
                if(tmpArr.indexOf(arr[i]) == -1){
                    tmpArr.push(arr[i]);
                }
            }
            return tmpArr;
        }
        $scope.echartsShow = function(){
            var item = $scope.config.currentItemCode.code;
            var dataEcharts = [];
            for(var j=0;j<$scope.config.dataEcharts.length;j++){
                if(item == $scope.config.dataEcharts[j].resrce){
                    dataEcharts.push($scope.config.dataEcharts[j]);
                }
            }
            var arrX=[];
            var codeMateArr = [];
            for(var i=0;i<dataEcharts.length;i++){
                arrX.push(dataEcharts[i].byTime);
                codeMateArr.push(dataEcharts[i].item);
            }
            arrX = __unique1(arrX);
            codeMateArr = __unique1(codeMateArr);
            if($scope.config.dateTypeEcharts == "M"){
                arrX.sort(function(a,b){return new Date(a).getTime()-new Date(b).getTime()});
            }else if($scope.config.dateTypeEcharts == "W"){
                arrX.sort();
            }else if($scope.config.dateTypeEcharts == "D"){
                arrX.sort(function(a,b){return new Date(a).getTime()-new Date(b).getTime()});
            }else if($scope.config.dateTypeEcharts == "S"){
            }
            var dataFirstGoodOld = [];
            var dataFirstBadOld = [];
            var codeAndData = [];
            for(var k=0;k<codeMateArr.length;k++) {
                codeAndData.push({
                    key: codeMateArr[k],
                    value: [],
                });
                for (var i = 0; i < arrX.length; i++) {
                    codeAndData[k].value[i] = {
                        byOther : arrX[i],
                        details : [],
                        addAllFirstGood : 0,
                        addAllFirstBad : 0,
                    }
                }
            }
            for(var i=0;i<dataEcharts.length;i++) {
                for(var k=0;k<codeAndData.length;k++){
                    for(var j=0;j<codeAndData[k].value.length;j++){
                        if(dataEcharts[i].item == codeAndData[k].key && dataEcharts[i].byTime == codeAndData[k].value[j].byOther){
                            codeAndData[k].value[j].details.push(dataEcharts[i]);
                        }
                    }
                }
            }
            var options = [];

            for(var i=0;i<codeAndData.length;i++){
                for(var j=0;j<codeAndData[i].value.length;j++){
                    for(var k=0;k<codeAndData[i].value[j].details.length;k++){
                        codeAndData[i].value[j].addAllFirstGood = (parseFloat(codeAndData[i].value[j].details[k].ppm)*1000 + codeAndData[i].value[j].addAllFirstGood*1000)/1000;
                        codeAndData[i].value[j].addAllFirstBad = (parseFloat(codeAndData[i].value[j].details[k].ppmTheory)*1000 + codeAndData[i].value[j].addAllFirstBad*1000)/1000;
                    }
                }
            }
            for(var i=0;i<codeAndData.length;i++){
                for(var j=0;j<codeAndData[i].value.length;j++){
                    codeAndData[i].value[j].data = {
                        name : codeAndData[i].value[j].byOther,
                        value : codeAndData[i].value[j].addAllFirstGood
                    };
                    codeAndData[i].value[j].data1 = {
                        name : codeAndData[i].value[j].byOther,
                        value : codeAndData[i].value[j].addAllFirstBad
                    };
                }
            }

            for(var i=0;i<codeAndData.length;i++){
                dataFirstGoodOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                dataFirstBadOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                for(var j=0;j<codeAndData[i].value.length;j++){
                    dataFirstGoodOld[i].value[j]=codeAndData[i].value[j].data;
                    dataFirstBadOld[i].value[j]=codeAndData[i].value[j].data1;
                }
            }


            var options = [];
            for(var i=0;i<codeMateArr.length;i++){
                options.push(
                    {
                        title : {text: 'PPM报表'},
                        series : [
                            {data: dataFirstGoodOld[i].value},
                            {data: dataFirstBadOld[i].value},
                        ]
                    }
                );
            }


            $scope.myChart = echarts.init(document.getElementById('PPMEchartsmain'));

            // 指定图表的配置项和数据
            var option = {
                baseOption: {
                    timeline: {
                        // y: 0,
                        axisType: 'category',
                        // realtime: false,
                        // loop: false,
                        autoPlay: true,
                        // currentIndex: 2,
                        playInterval: 3000,
                        // controlStyle: {
                        //     position: 'left'
                        // },
                        data: [],
                        label: {
                            formatter : function(s) {
                                return s;
                            }
                        }
                    },
                    title: {
                        subtext: ''
                    },
                    tooltip : {
                        trigger: 'axis',
                    },
                    color : ['#91c7ae','#9BA076',  '#A26F1C','#61a0a8', '#BC9B90', '#bda29a','#7C8EAF','#93959A', '#95B5CA', '#c4ccd3'],
                    legend: {
                        x: 'center',
                        data: ['实际PPM', '理论PPM'],
                        selected: {
                            //'GDP': false, '金融': false, '房地产': false
                        }
                    },
                    calculable : true,
                    grid: {
                        top: 80,
                        bottom: 100
                    },
                    toolbox: {
                        show : true,
                        feature : {
                            mark : {show: true},
                            dataView : {show: true, readOnly: true}, //optionToContent: optionToContent
                            magicType : {show: true, type: ['line', 'bar']},
                            restore : {show: true},
                            saveAsImage : {show: true},
                            dataZoom: {
                                show: false,
                                title: {
                                    zoom: '放大',
                                    back: '还原'
                                },
                                xAxisIndex: false,//[0,3],
                                yAxisIndex: 3
                            },
                            brush: {
                                type: 'keep',
                                title: ''
                            }
                        }
                    },
                    xAxis: [
                        {
                            'type':'category',
                            'axisLabel':{'interval':0},
                            'data':[],
                            splitLine: {show: false},
                            // axisLabel: {
                            //     rotate: 60,
                            // },
                            axisLabel : {interval : 0,
                            show : true}
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            name: 'PPM'
                        }
                    ],
                    series: [
                        {name: '实际PPM', type: 'line'},
                        {name: '理论PPM', type: 'line'},
                    ]
                },
                options : []
            };
            option.baseOption.timeline.data = codeMateArr;
            for(var i=0;i<arrX.length;i++){
                arrX[i] = arrX[i].replace('早班-7点30','M');
                arrX[i] = arrX[i].replace('晚班-19点30','E');
            }
            var  arrXCopy = [];
             if(arrX.length>4){
                for(var i=0;i<arrX.length;i++){
                    if(i%2 !=0){
                        arrXCopy.push("\n"+arrX[i]);
                    }else{
                        arrXCopy.push(arrX[i]);
                    }
                }
                option.baseOption.xAxis[0].data = arrXCopy;
            }else{
                option.baseOption.xAxis[0].data = arrX;
            }

            if(arrX.length>20){
                option.baseOption.xAxis[0].axisLabel.interval = 2;
            }else if(arrX.length>10){
                option.baseOption.xAxis[0].axisLabel.interval = 1;
            }
            option.options = options;
            if($scope.config.dateTypeEcharts == "M"){
                option.baseOption.title.subtext = "按月汇总";
            }else if($scope.config.dateTypeEcharts == "W"){
                option.baseOption.title.subtext = "按周汇总";
            }else if($scope.config.dateTypeEcharts == "D"){
                option.baseOption.title.subtext = "按日汇总";
            }else if($scope.config.dateTypeEcharts == "S"){
                option.baseOption.title.subtext = "按班次汇总";
            }
            // 使用刚指定的配置项和数据显示图表。
            $scope.myChart.setOption(option);
            window.onresize = $scope.myChart.resize;
            $scope.hideBodyModel();

        }
        function __requestGridApi(gridApi){
            $scope.config.gridApi = gridApi;
        }
        //控制显示隐藏列
        $scope.showHideCol=function () {
            if($scope.config.queryGridAreaShow){
                for(var i=0;i<$scope.gridOutputAreaStatement.columnDefs.length;i++) {
                    if (!$scope.gridOutputAreaStatement.columnDefs[i].visible) {
                        $scope.gridOutputAreaStatement.columnDefs[i].visible = true;
                    }
                }
            }
            else if($scope.config.queryGridLineShow){
                for(var i=0;i<$scope.gridOutputLineStatement.columnDefs.length;i++)
                {
                    if(!$scope.gridOutputLineStatement.columnDefs[i].visible){
                        $scope.gridOutputLineStatement.columnDefs[i].visible=true;
                    }
                }
            }
            else if($scope.config.queryGridDeviceShow){
                for(var i=0;i<$scope.gridOutputDeviceStatement.columnDefs.length;i++)
                {
                    if(!$scope.gridOutputDeviceStatement.columnDefs[i].visible){
                        $scope.gridOutputDeviceStatement.columnDefs[i].visible=true;
                    }
                }
            }
            else if($scope.config.queryGridAreaCodeShow){
                for(var i=0;i<$scope.gridOutputCodeStatement.columnDefs.length;i++)
                {
                    if(!$scope.gridOutputCodeStatement.columnDefs[i].visible){
                        $scope.gridOutputCodeStatement.columnDefs[i].visible=true;
                    }
                }
            }
            $scope.config.gridApi.core.refresh();
        };
        var refreshEchart=$scope.$on("refreshEchart", function(event,data) {
            UtilsService.log("接受成功shutdownReason");
            if($scope.config.addInfosShow) {
                $scope.timeOuter = $timeout(function () {
                    $scope.myChart.resize();
                }, 350);
            }
        });
        $scope.$on('$destroy',function(){
            refreshEchart();
            $timeout.cancel($scope.timeOuter);
            UtilsService.log("销毁成功shutdownReason");
        });
    }]);