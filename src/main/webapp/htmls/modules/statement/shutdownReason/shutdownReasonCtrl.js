statementModule.controller('shutdownReasonCtrl',['$scope', '$http',
    'HttpAppService', '$timeout','UtilsService',
    'uiGridConstants','$uibModal','$echarts','$interval','statementService','UtilsService',
    function ($scope, $http,
              HttpAppService, $timeout,UtilsService,
              uiGridConstants,$uibModal,$echarts,$interval,statementService) {
        $scope.config ={
            shutdownReasonStatement : '停机原因柏拉图报表',
            gridApi: null,
            byType:"W",
            workArea : "",
            workAreaName : "",
            workLineName : "",
            workLine : "",
            resourceType : "",
            resourceTypeDesc : "",

            startDate : UtilsService.serverFommateDateShow(new Date()),
            endDate : UtilsService.serverFommateDateShow(new Date()),
            startDateMouth : UtilsService.serverFormateDateMouth(new Date()),
            endDateMouth : UtilsService.serverFormateDateMouth(new Date()),

            selectType : true,

            type : 'RT',
            dateType : "M",

            //放大镜是否显示
            showSelectArea : true,
            showSelectLine : true,
            deviceType : true,
            deviceCode : true,


            //是否必选,红色星号
            showWorkArea : true,
            showLineArea : false,
            showResourceType : false,
            showResource : false,
            showCount:false,

            selectMouth : true,
            selectWeek : false,
            selectDay : false,
            selectShift : false,

            queryChartShow : false,

            queryGridAreaShow : false,
            queryGridLineShow : false,
            queryGridDeviceShow : false,
            queryGridAreaCodeShow : false,
            addInfosShow : false,

            //是否禁用查询按钮
            queryBtn : true,
            downLoadBtn : true,


            byDateDisplayName:'',

            //报表
            outPutStatementInstance : null,
            dataEcharts : null,
            dateTypeEcharts : null,
            showEcharts : true,

            selectName : "",
            currentSelected : "",
            dataSelected : "",
            selectedType :"",
            itemCodeSelectedCopy : [],
        	dataNum:"",
        	byTypeDesc:"",
        	byTimeTypeDesc:"",
        	showShift:false,
        	echartList:[]
        };
        $scope.init = function(){
        	 $scope.statementConfig.currentWorkCenter = [];
             $scope.statementConfig.currentWorkCenterDesc = [];
             $scope.statementConfig.currentLine = [];
             $scope.statementConfig.currentLineDesc = [];
             $scope.statementConfig.currentDeviceNum = null;
             $scope.statementConfig.multipleCurrentDeviceNum = [];
             $scope.statementConfig.currentDeviceType = [];
             $scope.statementConfig.currentDeviceTypeDesc = [];
            $scope.statementConfig.shiftsChange = [
                                                   {shift : '',description : '请选择',descriptionNew : '请选择'},
                                                   {shift : 'M',description : '早班',descriptionNew : '早班 代码:M'},
                                                   {shift : 'E',description : '晚班',descriptionNew : '晚班 代码:E'}
                                                  ];
            
        };
        $scope.init();
        //按照生产区域查询
        $scope.ShutdownReasonGridWorkAreaStatement = {
        	data:[],
            enableCellEdit:false,
            columnDefs:[
                {
                    name:"byTime",
                    displayName:'日期',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                },
                {
                    name:"workCenterDescription",
                    displayName:'生产区域',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                },
                {
                    name:"reasonCodeDescription",
                    displayName:'停机类型',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                },
                {
                    name:"shutdownTimes",
                    displayName:'发生次数',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass:"text-align-right",
                },
                {
                    name:"strTotalTime",
                    displayName:'时长（分钟）',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass:"text-align-right",
                },
                {
                    name:"strTotaPproportionNum",
                    displayName:'停机项/总时长',
                    minWidth:100,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass:"text-align-right",
                },
                {
                    name:"strDtProportionNum",
                    displayName:'停机项/总停机时长',
                    minWidth:140,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass:"text-align-right",
                }
            ],
            onRegisterApi:__requestGridApi
        };
        //按照拉线查询
        $scope.ShutdownReasonGridLineAreaStatement = {
            	data:[],
                enableCellEdit:false,
                columnDefs:[
                    {
                        name:"byTime",
                        displayName:'日期',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"workCenterDescription",
                        displayName:'生产区域',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"lineAreaDescription",
                        displayName:'拉线',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"reasonCodeDescription",
                        displayName:'停机类型',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"shutdownTimes",
                        displayName:'发生次数',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass:"text-align-right",
                    },
                    {
                        name:"strTotalTime",
                        displayName:'时长（分钟）',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass:"text-align-right",
                    },
                    {
                        name:"strTotaPproportionNum",
                        displayName:'停机项/总时长',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass:"text-align-right",
                    },
                    {
                        name:"strDtProportionNum",
                        displayName:'停机项/总停机时长',
                        minWidth:140,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass:"text-align-right",
                    }
                ],
            onRegisterApi:__requestGridApi
            };
        //按照设备类型查询
        $scope.ShutdownReasonGridResourceTypeStatement = {
            	data:[],
                enableCellEdit:false,
                columnDefs:[
                    {
                        name:"byTime",
                        displayName:'日期',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"resourceTypeDescription",
                        displayName:'设备类型',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"resourceTypeCode",
                        displayName:'设备类型编码',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"reasonCodeDescription",
                        displayName:'停机类型',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"shutdownTimes",
                        displayName:'发生次数',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass:"text-align-right",
                    },
                    {
                        name:"strTotalTime",
                        displayName:'时长（分钟）',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass:"text-align-right",
                    },
                    {
                        name:"strTotaPproportionNum",
                        displayName:'停机项/总时长',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass:"text-align-right",
                    },
                    {
                        name:"strDtProportionNum",
                        displayName:'停机项/总停机时长',
                        minWidth:140,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass:"text-align-right",
                    }
                ],
            onRegisterApi:__requestGridApi
            };
        //按设备查询
        $scope.ShutdownReasonGridResourceStatement = {
            	data:[],
                enableCellEdit:false,
                columnDefs:[
                    {
                        name:"byTime",
                        displayName:'日期',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"workCenterDescription",
                        displayName:'生产区域',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"lineAreaDescription",
                        displayName:'拉线',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"resrceCode",
                        displayName:'设备编码',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"asset",
                        displayName:'资产号',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"resrceDescription",
                        displayName:'设备描述',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"reasonCodeDescription",
                        displayName:'停机类型',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                    },
                    {
                        name:"shutdownTimes",
                        displayName:'发生次数',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass:"text-align-right",
                    },
                    {
                        name:"strTotalTime",
                        displayName:'时长（分钟）',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass:"text-align-right",
                    },
                    {
                        name:"strTotaPproportionNum",
                        displayName:'停机项/总时长',
                        minWidth:100,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass:"text-align-right",
                    },
                    {
                        name:"strDtProportionNum",
                        displayName:'停机项/总停机时长',
                        minWidth:140,
                        enableCellEdit : false,
                        enableCellEditOnFocus : false,
                        cellClass:"text-align-right",
                    }
                ],
            onRegisterApi:__requestGridApi
            };
        $scope.changeSelect=function(type){
        	if(type=='W'){
        		$scope.config.byType="W";
        		$scope.config.byTypeDesc="按生产区域汇总"
        		$scope.config.showWorkArea=true;
        		$scope.config.showLineArea=false;
        		$scope.config.showResourceType=false;
        		$scope.config.showResource=false;
        	}else if(type=='L'){
        		$scope.config.byType="L";
        		$scope.config.byTypeDesc="按拉线汇总"
        		$scope.config.showLineArea=true;
        		$scope.config.showWorkArea=false;
        		$scope.config.showResourceType=false;
        		$scope.config.showResource=false;
        	}else if(type=='RT'){
        		$scope.config.byType="T";
        		$scope.config.byTypeDesc="按设备类型汇总"
        		$scope.config.showResourceType=true;
        		$scope.config.showWorkArea=false;
        		$scope.config.showLineArea=false;
        		$scope.config.showResource=false;
        	}else if(type=='R'){
        		$scope.config.byType="R";
        		$scope.config.byTypeDesc="按设备汇总"
        		$scope.config.showResource=true;
        		$scope.config.showWorkArea=false;
        		$scope.config.showLineArea=false;
        		$scope.config.showResourceType=false;
        	}
        	$scope.controlQueryBtn();
        }
      //查询按钮的控制
        $scope.controlQueryBtn = function(){
        	if(($scope.statementConfig.currentWorkCenterDesc==null||$scope.statementConfig.currentWorkCenterDesc=='')
        			&&($scope.statementConfig.currentLineDesc==null||$scope.statementConfig.currentLineDesc=='')
        			&&($scope.statementConfig.currentDeviceTypeDesc==null||$scope.statementConfig.currentDeviceTypeDesc=='')
        			&&($scope.statementConfig.currentDeviceNum==null||$scope.statementConfig.currentDeviceNum=='')){
        		$scope.config.queryBtn=true;
        	}else{
        		$scope.config.queryBtn=false;
        	}
        	if($scope.config.showWorkArea==true){
        		if($scope.statementConfig.currentWorkCenterDesc!=null&&$scope.statementConfig.currentWorkCenterDesc!=''){
        			$scope.config.queryBtn=false;
        		}else{
        			$scope.config.queryBtn=true;
        		}
        	}else if($scope.config.showLineArea==true){
        		if($scope.statementConfig.currentLineDesc!=null&&$scope.statementConfig.currentLineDesc!=''){
        			$scope.config.queryBtn=false;
        		}else{
        			$scope.config.queryBtn=true;
        		}
        	}else if($scope.config.showResourceType==true){
        		if($scope.statementConfig.currentDeviceTypeDesc!=null&&$scope.statementConfig.currentDeviceTypeDesc!=''){
        			$scope.config.queryBtn=false;
        		}else{
        			$scope.config.queryBtn=true;
        		}
        	}else if($scope.config.showResource==true){
        		if($scope.statementConfig.currentDeviceNum!=null&&$scope.statementConfig.currentDeviceNum!=''){
        			$scope.config.queryBtn=false;
        		}else{
        			$scope.config.queryBtn=true;
        		}
        	}	
        }
        $scope.echartsShow = function(echartData){
            UtilsService.log("图表返回数据："+angular.toJson(echartData,true));
            var max=Math.ceil(Math.max.apply(null, echartData[0].totaPproportions));//向上取整
            var min=Math.floor(Math.min.apply(null, echartData[0].totaPproportions));//向下取整
            
            var maxDT=Math.ceil(Math.max.apply(null, echartData[0].dtProportions));
            var minDT=Math.floor(Math.min.apply(null, echartData[0].dtProportions));
            
            $scope.myChart = echarts.init(document.getElementById('main'));
            // 指定图表的配置项和数据
            var option = {
                tooltip: {
                    trigger: 'axis'
                },
                toolbox: {
                    feature: {
                        dataView: {show: true, readOnly: true},
                        magicType: {show: true, type: ['line', 'bar']},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                legend: {
                    data:['停机项/总时长','（停机项/总停机时长）累计']
                },
                xAxis: [
                    {
                        type: 'category',
                        axisLabel:{
                            interval: 0,
                            rotate:30,
                            margin:2,
                            textStyle:{
                                color:"#222"
                            }
                        },
                        data: echartData[0].reasonCodeDescriptions
                    }
                ],
                grid: { // 控制图的大小，调整下面这些值就可以，
                    x: 30,
                    left:'4%',
                    x2: 100,
                    y2: 80,// y2可以控制 X轴跟Zoom控件之间的间隔，避免以为倾斜后造成 label重叠到zoom上
                },
                yAxis: [
                    {
                        type: 'value',
                        name: '停机项/总时长',
                        min: min,
                        max: max,
                        interval: parseInt((max-min)/6),
                        axisLabel: {
                            formatter: '{value}%'
                        },
                        label:{
                        	normal:{
        	                	show: true,
        	                	position: 'inside',
                        	}
                        },
                    },
                    {
                        type: 'value',
                        name: '（停机项/总停机时长）累计',
                        splitLine:{show:false},
                        min: 0,
                        max: 100,
                        interval: 20,
                        axisLabel: {
                            formatter: '{value}%'
                        }
                    }
                ],
                series: [
                    {
                        name:'停机项/总时长',
                        type:'bar',
                        itemStyle: {
                            normal: {
                                color: new echarts.graphic.LinearGradient(
                                    0, 0, 0, 1,
                                    [
                                        {offset: 0, color: '#83bff6'},
                                        {offset: 0.5, color: '#188df0'},
                                        {offset: 1, color: '#188df0'}
                                    ]
                                )
                            },
                            emphasis: {
                                color: new echarts.graphic.LinearGradient(
                                    0, 0, 0, 1,
                                    [
                                        {offset: 0, color: '#2378f7'},
                                        {offset: 0.7, color: '#2378f7'},
                                        {offset: 1, color: '#83bff6'}
                                    ]
                                )
                            }
                        },
                        data:echartData[0].totaPproportions,
                        label:{
                        	normal:{
        	                	show: true, 
        	                	position: 'top',
                        	} 
                        },
                    },
                    {
                        name:'（停机项/总停机时长）累计',
                        type:'line',
                        itemStyle: {
                            normal: {
                                color: '#f5ad71'
                            }
                        },
                        yAxisIndex: 1,
                        data:echartData[0].dtProportions,
                    }
                ]
            };
            $scope.myChart.setOption(option);
            window.onresize = $scope.myChart.resize;
            $scope.hideBodyModel();
        }
        //查询
        $scope.queryDatas = function(){
        	$scope.config.showEcharts=true;
            var params = {
            	byType:$scope.config.byType,
                workCenter: $scope.statementConfig.currentWorkCenter,
                lineArea:$scope.statementConfig.currentLine,
                resourceType:$scope.statementConfig.currentDeviceType,
                resource:$scope.statementConfig.currentDeviceNum,
                byTimeType:$scope.config.dateType,
                startDate: '',
                endDate: '',
            };
            if($scope.statementConfig.currentShift){
                params.shift = $scope.statementConfig.currentShift.shift;
            }
            if($scope.config.selectType == true){
            	params.startDate=UtilsService.getMonthDate(new Date($scope.config.startDateMouth),'start');
            	params.endDate=UtilsService.getMonthDate(new Date($scope.config.endDateMouth),'end');
            }else{
            	params.startDate= UtilsService.serverFommateDate(new Date($scope.config.startDate));
            	params.endDate= UtilsService.serverFommateDate(new Date($scope.config.endDate));
            }
            if(new Date(params.startDate)>new Date(params.endDate)){
                $scope.addAlert('danger','开始时间不能大于结束时间');
                return;
            }
            if(params.byTimeType=='M'){
                if(UtilsService.addMonth(new Date(params.startDate),12)<new Date(params.endDate)){
                    $scope.addAlert('danger','开始和结束时间跨度不超过12个月');
                    $scope.hideBodyModel();
                    return;
                }
            }else if(params.byTimeType=='W'){
                if((new Date(params.endDate)).getTime()-(new Date(params.startDate)).getTime()>105*24*60*60*1000){
                    $scope.addAlert('danger','开始和结束时间跨度不超过15周');
                    $scope.hideBodyModel();
                    return;
                }
            }else if(params.byTimeType=='D'){
                if((new Date(params.endDate)).getTime()-(new Date(params.startDate)).getTime()>92*24*60*60*1000){
                	$scope.addAlert('danger','开始和结束时间跨度不超过92天');
                    return;
                }
            }else if(params.byTimeType=='S'){
                if((new Date(params.endDate)).getTime()-(new Date(params.startDate)).getTime()>92*24*60*60*1000){
                	$scope.addAlert('danger','开始和结束时间跨度不超过92天');
                    return;
                }
            }
            
            if(params.byType=='W'){
        		$scope.config.byTypeDesc="按生产区域汇总 "
        	}else if(params.byType=='L'){
        		$scope.config.byTypeDesc="按拉线汇总 "
        	}else if(params.byType=='T'){
        		$scope.config.byTypeDesc="按设备类型汇总 "
        	}else if(params.byType=='R'){
        		$scope.config.byTypeDesc="按设备汇总 "
        	}
            $scope.config.byTimeTypeDesc="时间范围："+UtilsService.serverFommateDateShow(new Date(Date.parse(params.startDate)))+"至"+UtilsService.serverFommateDateShow(new Date(Date.parse(params.endDate)));
            $scope.showBodyModel("正在加载数据,请稍后...");
            statementService.ShutdownReasonEchart(params).then(function (resultDatas){
                $scope.hideBodyModel();
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.config.echartList=resultDatas.response;
                    $scope.echartsShow(resultDatas.response);
                    return;
                }else{
                	$scope.config.showEcharts=false;
                }
            }, function (resultDatas){
                $scope.hideBodyModel();
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });

            statementService.ShutdownReasonList(params).then(function (resultDatas){
                $scope.hideBodyModel();
                if(resultDatas.response && resultDatas.response.length > 0){
                	$scope.config.dataNum="总记录数:"+resultDatas.response.length;
                	$scope.config.downLoadBtn=false;
                	$scope.config.showCount=true;
                	if($scope.config.byType=='W'){
                		 $scope.ShutdownReasonGridWorkAreaStatement.data = resultDatas.response;
                         $scope.config.queryGridAreaShow = true;
                         $scope.config.queryGridLineShow = false;
                         $scope.config.queryGridDeviceShow = false;
                         $scope.config.queryGridAreaCodeShow = false;
                	}else if($scope.config.byType=='L'){
                		$scope.ShutdownReasonGridLineAreaStatement.data = resultDatas.response;
                        $scope.config.queryGridLineShow = true;
                        $scope.config.queryGridAreaShow = false;
                        $scope.config.queryGridDeviceShow = false;
                        $scope.config.queryGridAreaCodeShow = false;
                	}else if($scope.config.byType=='T'){
                		$scope.ShutdownReasonGridResourceTypeStatement.data = resultDatas.response;
                        $scope.config.queryGridDeviceShow = true;
                        $scope.config.queryGridLineShow = false;
                        $scope.config.queryGridAreaShow = false;
                        $scope.config.queryGridAreaCodeShow = false;
                	}else if($scope.config.byType=='R'){
                		$scope.ShutdownReasonGridResourceStatement.data = resultDatas.response;
                        $scope.config.queryGridAreaCodeShow = true;
                        $scope.config.queryGridDeviceShow = false;
                        $scope.config.queryGridLineShow = false;
                        $scope.config.queryGridAreaShow = false;
                	}
                }else{
                    $scope.addAlert("","暂无数据");
                    $scope.config.showCount=false;
                    $scope.config.queryGridAreaShow = false;
                    $scope.config.queryGridLineShow = false;
                    $scope.config.queryGridDeviceShow = false;
                    $scope.config.queryGridAreaCodeShow = false;
                    $scope.ShutdownReasonGridWorkAreaStatement.data=[];
                    $scope.ShutdownReasonGridLineAreaStatement.data=[];
                    $scope.ShutdownReasonGridResourceTypeStatement.data=[];
                    $scope.ShutdownReasonGridResourceStatement.data=[];
                }
            }, function (resultDatas){
                $scope.hideBodyModel();
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });
        }
        
        $scope.downLoad=function(){
        	$scope.showBodyModel("正在导出数据,请稍后...");
        	var params = {
                	byType:$scope.config.byType,
                    workCenter: $scope.statementConfig.currentWorkCenter,
                    lineArea:$scope.statementConfig.currentLine,
                    resourceType:$scope.statementConfig.currentDeviceType,
                    resource:$scope.statementConfig.currentDeviceNum,
                    byTimeType:$scope.config.dateType,
                    startDate: '',
                    endDate: '',
                };
        	if($scope.statementConfig.currentShift){
                params.shift = $scope.statementConfig.currentShift.shift;
            }
            if($scope.config.selectType == true){
            	params.startDate=UtilsService.getMonthDate(new Date($scope.config.startDateMouth),'start');
            	params.endDate=UtilsService.getMonthDate(new Date($scope.config.endDateMouth),'end');
            }else{
            	params.startDate= UtilsService.serverFommateDate(new Date($scope.config.startDate));
            	params.endDate= UtilsService.serverFommateDate(new Date($scope.config.endDate));
            }
                if($scope.config.selectType == true){
                	params.startDate=UtilsService.getMonthDate(new Date($scope.config.startDateMouth),'start');
                	params.endDate=UtilsService.getMonthDate(new Date($scope.config.endDateMouth),'end');
                }else{
                	params.startDate= UtilsService.serverFommateDate(new Date($scope.config.startDate));
                	params.endDate= UtilsService.serverFommateDate(new Date($scope.config.endDate));
                }
                var url = statementService.shutdownReasonExport(params);
                url = url.replace(/undefined/g,"").replace(/null/g,"");
                url = HttpAppService.handleCommenUrl(url);
                $http({
                    url: url,
                    method: "GET",
                    headers: {
                        'Content-type': 'application/json'
                    },
                    responseType: 'arraybuffer'
                }).success(function (data, status, headers, config) {
                    var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                    var objectUrl = URL.createObjectURL(blob);
                    window.location.href=objectUrl;
                    $scope.hideBodyModel();
                }).error(function (data, status, headers, config) {
                });
        }
        //按照 月 周 天 班次 汇总
        $scope.changeAddType = function(type){
            if(type == "M"){
                $scope.config.dateType = "M";
                $scope.config.selectMouth = true;
                $scope.config.selectWeek = false;
                $scope.config.selectDay = false;
                $scope.config.selectShift = false;
                $scope.config.selectType = true;
                $scope.config.startDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.endDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.startDateMouth= UtilsService.serverFormateDateMouth(new Date());
                $scope.config.endDateMouth= UtilsService.serverFormateDateMouth(new Date());
                $scope.config.showShift=false;
            }else if(type == "W"){
                $scope.config.dateType = "W";
                $scope.config.selectMouth = false;
                $scope.config.selectWeek = true;
                $scope.config.selectDay = false;
                $scope.config.selectShift = false;
                $scope.config.selectType = false;
                $scope.config.startDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.endDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.startDateMouth= UtilsService.serverFormateDateMouth(new Date());
                $scope.config.endDateMouth= UtilsService.serverFormateDateMouth(new Date());
                $scope.config.showShift=false;
            }else if(type == "D"){
                $scope.config.dateType = "D";
                $scope.config.selectMouth = false;
                $scope.config.selectWeek = false;
                $scope.config.selectDay = true;
                $scope.config.selectShift = false;
                $scope.config.selectType = false;
                $scope.config.startDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.endDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.startDateMouth= UtilsService.serverFormateDateMouth(new Date());
                $scope.config.endDateMouth= UtilsService.serverFormateDateMouth(new Date());
                $scope.config.showShift=false;
            }else if(type == "S"){
                $scope.config.dateType = "S";
                $scope.config.selectMouth = false;
                $scope.config.selectWeek = false;
                $scope.config.selectDay = false;
                $scope.config.selectShift = true;
                $scope.config.selectType = false;
                $scope.config.startDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.endDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.startDateMouth= UtilsService.serverFormateDateMouth(new Date());
                $scope.config.endDateMouth= UtilsService.serverFormateDateMouth(new Date());
                $scope.config.showShift=true;
            }
        };

        //清除输入框
        $scope.deleteAll = function(item){
            if(item == 'area'){
                $scope.statementConfig.currentWorkCenter = [];
                $scope.statementConfig.currentWorkCenterDesc = [];
                $scope.statementConfig.currentLine = [];
                $scope.statementConfig.currentLineDesc = [];
                $scope.statementConfig.deviceNums = [];
                $scope.statementConfig.currentDeviceNum = null;
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.showSelectArea = true;
                $scope.config.showSelectLine = true;
                $scope.config.deviceCode = true;
                $scope.controlQueryBtn();
            }
            if(item == 'line'){
                $scope.statementConfig.currentLine = [];
                $scope.statementConfig.currentLineDesc = [];
                $scope.statementConfig.deviceNums = [];
                $scope.statementConfig.currentDeviceNum = null;
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.showSelectLine = true;
                $scope.config.deviceCode = true;
                $scope.controlQueryBtn();
            }
            if(item == 'deviceType'){
                $scope.statementConfig.currentDeviceType = [];
                $scope.statementConfig.currentDeviceTypeDesc = [];
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.deviceType = true;
                $scope.config.deviceCode = true;
                $scope.controlQueryBtn();
            }
            if(item == 'deviceCode'){
                $scope.statementConfig.currentDeviceNum = [];
                $scope.config.deviceCode = true;
                $scope.controlQueryBtn();
            }
        }
        //选择时间 这是选择月的
        //生产区域帮助
        $scope.toAreaHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/areaSelectMoreModal.html',
                controller: 'areaSelectMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectAreaModal: function () {
                        return {
                        	workArea : $scope.statementConfig.workCenters,
                        	reportType:'Plato'
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentWorkCenter = selectedItem.area.join(",");
                $scope.statementConfig.currentWorkCenterDesc = selectedItem.areaName.join(",");
                $scope.config.showSelectArea = false;
                $scope.deleteAll('line');
                $scope.deleteAll('deviceCode');
                $scope.controlQueryBtn();
            }, function () {
            });
        };
        //拉线多选帮助
        $scope.toWireLineHelp = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/lineSelectModal.html',
                controller: 'LineSelectMoreCtrl',
                size: 'lg',
                backdrop: 'false',
                scope: $scope,
                openedClass: 'flex-center-parent',
                resolve: {
                    selectLineModal: function () {
                        return {
                        	workArea: $scope.statementConfig.currentWorkCenter,
                        	workLine: $scope.statementConfig.lines,
                        	reportType:'Plato'
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentLine = selectedItem.line;
                $scope.statementConfig.currentLineDesc = selectedItem.lineName;
                if($scope.statementConfig.currentLine.length >0){
                    $scope.config.showSelectLine = false;
                }
                $scope.deleteAll('deviceCode');
                $scope.controlQueryBtn();
            }, function () {
            });
        };
        //设备类型帮助
        $scope.toDeviceTypeHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/deviceTypeSelectMoreModal.html',
                controller: 'deviceTypeMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectTypeMoreDevice: function () {
                        return {
                        	deviceCode : $scope.statementConfig.deviceTypes,
                        	reportType:'Plato'
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentDeviceType = selectedItem.resourceType;
                $scope.statementConfig.currentDeviceTypeDesc = selectedItem.resourceTypeDesc;
                if($scope.statementConfig.currentDeviceType.length >0){
                    $scope.config.deviceType = false;
                }
                $scope.deleteAll('deviceCode');
                $scope.controlQueryBtn();
            }, function () {
            });
        };	
        //设备帮助
        $scope.toDeviceHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/modalDeviceHelp.html',
                controller: 'deviceMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectMoreDevice: function () {
                        return {
                        	workArea: $scope.statementConfig.currentWorkCenter,
                        	workLine: $scope.statementConfig.currentLine,
                        	deviceCode : $scope.statementConfig.currentDeviceType,
                        	reportType:'Plato'
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentDeviceNum = selectedItem.area;
                if($scope.statementConfig.currentDeviceNum.length >0){
                    $scope.config.deviceCode = false;
                }
                $scope.controlQueryBtn();
            }, function () {
            });
        };

        function __requestGridApi(gridApi){
            $scope.config.gridApi = gridApi;
        }
        //控制显示隐藏列
        $scope.showHideCol=function () {
            if($scope.config.queryGridAreaShow){
                for(var i=0;i<$scope.ShutdownReasonGridWorkAreaStatement.columnDefs.length;i++) {
                    if (!$scope.ShutdownReasonGridWorkAreaStatement.columnDefs[i].visible) {
                        $scope.ShutdownReasonGridWorkAreaStatement.columnDefs[i].visible = true;
                    }
                }
            }
            else if($scope.config.queryGridLineShow){
                for(var i=0;i<$scope.ShutdownReasonGridLineAreaStatement.columnDefs.length;i++)
                {
                    if(!$scope.ShutdownReasonGridLineAreaStatement.columnDefs[i].visible){
                        $scope.ShutdownReasonGridLineAreaStatement.columnDefs[i].visible=true;
                    }
                }
            }
            else if($scope.config.queryGridDeviceShow){
                for(var i=0;i<$scope.ShutdownReasonGridResourceTypeStatement.columnDefs.length;i++)
                {
                    if(!$scope.ShutdownReasonGridResourceTypeStatement.columnDefs[i].visible){
                        $scope.ShutdownReasonGridResourceTypeStatement.columnDefs[i].visible=true;
                    }
                }
            }
            else if($scope.config.queryGridAreaCodeShow){
                for(var i=0;i<$scope.ShutdownReasonGridResourceStatement.columnDefs.length;i++)
                {
                    if(!$scope.ShutdownReasonGridResourceStatement.columnDefs[i].visible){
                        $scope.ShutdownReasonGridResourceStatement.columnDefs[i].visible=true;
                    }                }
            }
            $scope.config.gridApi.core.refresh();
        };

        var refreshEchart=$scope.$on("refreshEchart", function(event,data) {
            UtilsService.log("接受成功shutdownReason");
            if($scope.myChart && $scope.config.showEcharts) {
                $scope.timeOuter = $timeout(function () {
                    $scope.myChart.resize();
                }, 350);
            }
        });
        $scope.$on('$destroy',function(){
            refreshEchart();
            $timeout.cancel($scope.timeOuter);
            UtilsService.log("销毁成功shutdownReason");
        });
    }])