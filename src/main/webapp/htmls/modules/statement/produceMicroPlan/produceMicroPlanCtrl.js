statementModule.controller('produceMicroPlanCtrl',['$scope', '$http',
    'HttpAppService', '$timeout','UtilsService',
    'uiGridConstants','$uibModal','$echarts','$interval','statementService','UtilsService',
    function ($scope, $http,
              HttpAppService, $timeout,UtilsService,
              uiGridConstants,$uibModal,$echarts,$interval,statementService) {
        $scope.config ={
            produceMicorPlanReport : '生产微计划报表',

            byType:"realTime",
            workArea : "",
            workAreaName : "",
            workLineName : "",
            workLine : "",
            resourceType : "",
            resourceTypeDesc : "",

            startDate : UtilsService.serverFommateDateShow(new Date()),
            endDate : UtilsService.serverFommateDateShow(new Date()),

            selectType : true,

            type : 'RT',
            dateType : "M",

            //放大镜是否显示
            showSelectArea : true,
            showSelectLine : true,
            deviceType : true,
            deviceCode : true,


            //是否必选,红色星号
            showWorkArea : true,
            showLineArea : false,
            showResourceType : false,
            showResource : false,
            showCount:false,

            selectMouth : true,
            selectWeek : false,
            selectDay : false,
            selectShift : false,


            queryGridAreaShow : false,
            queryGridLineShow : false,
            queryGridDeviceShow : false,
            queryGridAreaCodeShow : false,
            addInfosShow : false,

            //是否禁用查询按钮
            queryBtn : true,
            LoadBtn : true,


            byDateDisplayName:'',

            //报表
            outPutStatementInstance : null,
            dateTypeEcharts : null,

            selectName : "",
            currentSelected : "",
            dataSelected : "",
            selectedType :"",
            itemCodeSelectedCopy : [],
        	dataNum:"",
        	byTypeDesc:"",
        	byTimeTypeDesc:"",
        	showShift:false,
        	showDate:false,
        	dateCol:{
                name:"RESULT_DATE",
                displayName:'日期',
                minWidth:100,
                pinnedLeft:true,
                enableCellEdit : false,
                enableCellEditOnFocus : false,
                cellTooltip: function (row, col) {
                    return row.entity[col.colDef.name];
                },
            },
        	workCenterCol:{
                    name:"WORK_CENTER_DESCRIPTION",displayName:'生产区域描述',minWidth:130,enableCellEdit : false,enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    },
                },
            lineAreaCol:{
                    name:"LINE_AREA_DESCRIPTION",displayName:'拉线描述',minWidth:150,enableCellEdit : false,enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
            assetCol:{
                name:"ASSET",
                displayName:'资产号',
                minWidth:100,
                enableCellEdit : false,
                enableCellEditOnFocus : false,
                cellTooltip: function (row, col) {
                    return row.entity[col.colDef.name];
                }
            },
            modelCol:{
                name:"MODEL",
                displayName:'Model',
                minWidth:100,
                enableCellEdit : false,
                enableCellEditOnFocus : false,
                cellTooltip: function (row, col) {
                    return row.entity[col.colDef.name];
                }
            },
            resrceCol: {
                name:"RESRCE",
                displayName:'设备编码',
                minWidth:100,
                enableCellEdit : false,
                enableCellEditOnFocus : false,
                cellTooltip: function (row, col) {
                    return row.entity[col.colDef.name];
                }
            },
        };
        $scope.getTableHeight = function () {
            if ($scope.produceMicroPlanStatement.data.length > 0) {
                var rowHeight = 31;
                var headerHeight = 50;
                return {
                    height: (15 * rowHeight + headerHeight) + "px"
                };
            }
        };
        $scope.dispalayDate=function(){
        	if($scope.config.showDate==true){
        		$scope.config.byType='history';
        	}else {
        		$scope.config.byType='realTime';
        	}
        }
        $scope.init = function(){
        	 $scope.statementConfig.currentWorkCenter = [];
             $scope.statementConfig.currentWorkCenterDesc = [];
             $scope.statementConfig.currentLine = [];
             $scope.statementConfig.currentLineDesc = [];
             $scope.statementConfig.currentDeviceNum = null;
             $scope.statementConfig.multipleCurrentDeviceNum = [];
             $scope.statementConfig.currentDeviceType = [];
             $scope.statementConfig.currentDeviceTypeDesc = [];
            $scope.statementConfig.shiftsChange = [
                                                   {shift : '',description : '请选择',descriptionNew : '请选择'},
                                                   {shift : 'M',description : '早班',descriptionNew : '早班 代码:M'},
                                                   {shift : 'E',description : '晚班',descriptionNew : '晚班 代码:E'}
                                                  ];
            $scope.statementConfig.timeChange=[
                                               {time:30,id:2,colNum:48,description:'0.5小时'},
                                               {time:120,id:3,colNum:12,description:'2小时'}
                                               ];
            $scope.statementConfig.timeInterval={time:30,id:1,colNum:48,description:'0.5小时'};
        };
        $scope.init();
        /*$scope.test=function(){
        	$scope.statementConfig.timeInterval = angular.copy($scope.statementConfig.timeChange);
        }*/
        $scope.produceMicroPlanStatement = {
        	data:[],
            enableCellEdit:false,
            columnDefs:[
                {
                    name:"RESRCE_DESCRIPTION",
                    displayName:'设备描述',
                    minWidth:200,
                    pinnedLeft:true,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"PRODUCT_ITEM",
                    displayName:'物料编码',
                    minWidth:150,
                    pinnedLeft:true,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    }
                },
                {
                    name:"DAY_TARGE_QTY_YIELD",
                    displayName:'产能目标',
                    minWidth:80,
                    pinnedLeft:true,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    },
                    cellClass: function (row, col) {
                    	return 'text-align-right';
                    },
                },
                {
                    name:"PART_TARGE_QTY_YIELD",
                    displayName:'分时段目标',
                    minWidth:80,
                    pinnedLeft:true,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    },
                    cellClass: function (row, col) {
                    	return 'text-align-right';
                    },
                },
                {
                    name:"M_PRO_SHIFT_TOTAL",
                    displayName:'早班合计',
                    minWidth:80,
                    pinnedLeft:true,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    },
                    cellClass: function (row, col) {
                    	return 'text-align-right';
                    },
                },
                {
                    name:"E_PRO_SHIFT_TOTAL",
                    displayName:'晚班合计',
                    minWidth:80,
                    pinnedLeft:true,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    },
                    cellClass: function (row, col) {
                    	return 'text-align-right';
                    },
                },
                {
                    name:"DAY_PRO_SHIFT_TOTAL",
                    displayName:'合计',
                    pinnedLeft:true,
                    minWidth:80,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellTooltip: function (row, col) {
                        return row.entity[col.colDef.name];
                    },
                    cellClass: function (row, col) {
                    	return 'text-align-right';
                    },
                },
            ],
            onRegisterApi:__requestGridApi
        };
      //查询按钮的控制
        $scope.controlQueryBtn = function(){
        	if(($scope.statementConfig.currentWorkCenterDesc==null||$scope.statementConfig.currentWorkCenterDesc=='')
        			&&($scope.statementConfig.currentLineDesc==null||$scope.statementConfig.currentLineDesc=='')
        			&&($scope.statementConfig.currentDeviceTypeDesc==null||$scope.statementConfig.currentDeviceTypeDesc=='')
        			&&($scope.statementConfig.currentDeviceNum==null||$scope.statementConfig.currentDeviceNum=='')){
        		$scope.config.queryBtn=true;
        	}else{
        		$scope.config.queryBtn=false;
        	}
        	if($scope.config.showWorkArea==true){
        		if($scope.statementConfig.currentWorkCenterDesc!=null&&$scope.statementConfig.currentWorkCenterDesc!=''){
        			$scope.config.queryBtn=false;
        		}else{
        			$scope.config.queryBtn=true;
        		}
        	}else if($scope.config.showLineArea==true){
        		if($scope.statementConfig.currentLineDesc!=null&&$scope.statementConfig.currentLineDesc!=''){
        			$scope.config.queryBtn=false;
        		}else{
        			$scope.config.queryBtn=true;
        		}
        	}else if($scope.config.showResourceType==true){
        		if($scope.statementConfig.currentDeviceTypeDesc!=null&&$scope.statementConfig.currentDeviceTypeDesc!=''){
        			$scope.config.queryBtn=false;
        		}else{
        			$scope.config.queryBtn=true;
        		}
        	}else if($scope.config.showResource==true){
        		if($scope.statementConfig.currentDeviceNum!=null&&$scope.statementConfig.currentDeviceNum!=''){
        			$scope.config.queryBtn=false;
        		}else{
        			$scope.config.queryBtn=true;
        		}
        	}	
        }
        $scope.queryDatas = function () {
        	if($scope.config.byType=='realTime'){
        		$timeout.cancel(window.recTime);
        		realTimeSearch();
        	}else if($scope.config.byType=='history'){
        		historySearch();
        	}
        };
        //查询
        function realTimeSearch(){
        	if($scope.config.byType=='realTime'){
                var params = {
                    	byType:$scope.config.byType,
                        workCenter: $scope.statementConfig.currentWorkCenter,
                        lineArea:$scope.statementConfig.currentLine,
                        resourceType:$scope.statementConfig.currentDeviceType,
                        resource:$scope.statementConfig.currentDeviceNum,
                        model:$scope.config.model,
                        item:$scope.config.itemCode,
                        startDate: '',
                        endDate: '',
                    };
                    if((params.workCenter==''||params.workCenter==null)
                    	&&(params.lineArea==''||params.lineArea==null)
                    	&&(params.resourceType==''||params.resourceType==null)
                    	&&(params.resource==''||params.resource==null)
                    	&&(params.model==''||params.model==null)
                    	&&(params.item==''||params.item==null)){
                    	$scope.addAlert('danger','条件不可以为空');
                        return;
                    }
                    if($scope.statementConfig.currentShift){
                        params.shift = $scope.statementConfig.currentShift.shift;
                    }
                    if($scope.statementConfig.timeInterval){
                    	params.timeInterval=$scope.statementConfig.timeInterval.time;
                    	params.colNum=$scope.statementConfig.timeInterval.colNum;
                    }
                    var currDate=new Date();
                    var m1Shift=currDate.getFullYear()+"-"+(currDate.getMonth()+1)+"-"+currDate.getDate()+" 07:30:00";
                    var e1Shift=currDate.getFullYear()+"-"+(currDate.getMonth()+1)+"-"+currDate.getDate()+" 19:30:00";
                    var m2Shift=currDate.getFullYear()+"-"+(currDate.getMonth()+1)+"-"+currDate.getDate()+" 08:00:00";
                    var e2Shift=currDate.getFullYear()+"-"+(currDate.getMonth()+1)+"-"+currDate.getDate()+" 20:00:00";
                    if($scope.config.byType=='realTime'){
                    	 params.startDate= UtilsService.serverFommateDate(new Date());
                         params.endDate= UtilsService.serverFommateDate(new Date());
                    }else{
                    	 params.startDate= UtilsService.serverFommateDate(new Date($scope.statementConfig.PMPstartDate));
                         params.endDate= UtilsService.serverFommateDate(new Date($scope.statementConfig.endDate));
                         if((new Date(params.endDate)).getTime()-(new Date(params.startDate)).getTime()>92*24*60*60*1000){
                         	$scope.addAlert('danger','开始和结束时间跨度不超过92天');
                             return;
                         }
                    }
                    if(new Date(params.startDate)>new Date(params.endDate)){
                        $scope.addAlert('danger','开始时间不能大于结束时间');
                        return;
                    }
                    //$scope.showBodyModel("正在加载数据,请稍后...");
                    statementService.ProduceMicroPlanList(params).then(function (resultDatas){
                        //$scope.hideBodyModel();
                        if(resultDatas.response && resultDatas.response.length > 0){
                        	$scope.config.queryGridAreaShow = true;
                        	$scope.config.dataNum="总记录数:"+resultDatas.response.length;
                        	$scope.config.LoadBtn=false;
                        	$scope.config.showCount=true;
                        	$scope.produceMicroPlanStatement.data = [];
                                 if(params.timeInterval==30){
                                	 
                                	for(var i=0;i<$scope.produceMicroPlanStatement.columnDefs.length;i++){
                                		if($scope.produceMicroPlanStatement.columnDefs[0].name=="RESULT_DATE"){
                                			$scope.produceMicroPlanStatement.columnDefs.splice(0,1);
                                		}
                              			$scope.produceMicroPlanStatement.columnDefs.splice(7,$scope.produceMicroPlanStatement.columnDefs.length-7);
                              		}
                             		var columnNum=Math.ceil((currDate.getTime()-new Date(m1Shift).getTime())/(params.timeInterval*60*1000));
                             		for(var i=0;i<columnNum;i++){
                             			var name=$scope.getGridName(i);
                             			var displayName=$scope.getGridDisplayName('7:30',i,params.timeInterval);
                             			var obj={
                                 				 name:name,
                                                 displayName:displayName,
                                                 minWidth:60,
                                                 enableCellEdit : false,
                                                 enableCellEditOnFocus : false,
                                                 cellTooltip: function (row, col) {
                                                     return row.entity[col.colDef.name];
                                                 },
                                                 cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
        		                                        	 if(parseFloat(row.entity[col.colDef.name]) < parseFloat(row.entity.PART_TARGE_QTY_YIELD)){
        		                                                 return 'background-red text-align-right';
        		                                             }else if(parseFloat(row.entity[col.colDef.name]) >= parseFloat(row.entity.PART_TARGE_QTY_YIELD)){
        		                                                 return 'background-green text-align-right';
        		                                             }else {
        		                                            	 return 'text-align-right';
        		                                             }
                                                 		}
                                                 };
                             			$scope.produceMicroPlanStatement.columnDefs.push(obj);
                             		}
                             	}else if(params.timeInterval==120){
                             		for(var i=0;i<$scope.produceMicroPlanStatement.columnDefs.length;i++){
                             			if($scope.produceMicroPlanStatement.columnDefs[0].name=="RESULT_DATE"){
                                			$scope.produceMicroPlanStatement.columnDefs.splice(0,1);
                                		}
                             			$scope.produceMicroPlanStatement.columnDefs.splice(7,$scope.produceMicroPlanStatement.columnDefs.length-7);
                             		}
                             		var columnNum=Math.ceil((currDate.getTime()-new Date(m2Shift).getTime())/(params.timeInterval*60*1000));
                             		for(var j=0;j<columnNum;j++){
                             			var name=$scope.getGridName(j);
                             			var displayName=$scope.getGridDisplayName('8:00',j,params.timeInterval);
                             			var obj={
                                 				 name:name,
                                                 displayName:displayName,
                                                 minWidth:60,
                                                 enableCellEdit : false,
                                                 enableCellEditOnFocus : false,
                                                 cellTooltip: function (row, col) {
                                                	 //console.info(row.entity[col.colDef.name]);
                                                     return row.entity[col.colDef.name];
                                                 },
                                                 cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
        	                                        	 if(parseFloat(row.entity[col.colDef.name]) < parseFloat(row.entity.PART_TARGE_QTY_YIELD)){
        	                                                 return 'background-red text-align-right';
        	                                             }else if(parseFloat(row.entity[col.colDef.name]) >= parseFloat(row.entity.PART_TARGE_QTY_YIELD)){
        	                                                 return 'background-green text-align-right';
        	                                             }else {
        	                                            	 return 'text-align-right';
        	                                             }
                                         			}
                                                 };
                             			$scope.produceMicroPlanStatement.columnDefs.push(obj);
                             		}
                             	}
                                 $scope.produceMicroPlanStatement.columnDefs.push($scope.config.workCenterCol);
                                 $scope.produceMicroPlanStatement.columnDefs.push($scope.config.lineAreaCol);
                                 $scope.produceMicroPlanStatement.columnDefs.push($scope.config.assetCol);
                                 $scope.produceMicroPlanStatement.columnDefs.push($scope.config.modelCol);
                                 $scope.produceMicroPlanStatement.columnDefs.push($scope.config.resrceCol);
                        	$scope.produceMicroPlanStatement.data=resultDatas.response;
                        }else{
                            $scope.addAlert("","暂无数据");
                            $scope.config.showCount=false;
                            $scope.config.queryGridAreaShow = false;
                            $scope.config.queryGridLineShow = false;
                            $scope.config.queryGridDeviceShow = false;
                            $scope.config.queryGridAreaCodeShow = false;
                            $scope.produceMicroPlanStatement.data=[];
                        }
                    }, function (resultDatas){
                        //$scope.hideBodyModel();
                        $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
                    });
                    window.recTime = $timeout(function () {
                    	realTimeSearch();
                    }, 60*1000);
        	}
        }
      //查询
        function historySearch(){
            var params = {
            	byType:$scope.config.byType,
                workCenter: $scope.statementConfig.currentWorkCenter,
                lineArea:$scope.statementConfig.currentLine,
                resourceType:$scope.statementConfig.currentDeviceType,
                resource:$scope.statementConfig.currentDeviceNum,
                model:$scope.config.model,
                item:$scope.config.itemCode,
                startDate: '',
                endDate: '',
                
            };
            if((params.workCenter==''||params.workCenter==null)
            	&&(params.lineArea==''||params.lineArea==null)
            	&&(params.resourceType==''||params.resourceType==null)
            	&&(params.resource==''||params.resource==null)
            	&&(params.model==''||params.model==null)
            	&&(params.item==''||params.item==null)){
            	$scope.addAlert('danger','条件不可以为空');
                return;
            }
            if($scope.statementConfig.currentShift){
                params.shift = $scope.statementConfig.currentShift.shift;
            }
            if($scope.statementConfig.timeInterval){
            	params.timeInterval=$scope.statementConfig.timeInterval.time;
            	params.colNum=$scope.statementConfig.timeInterval.colNum;
            }
            var currDate=new Date();
            var m1Shift=currDate.getFullYear()+"-"+(currDate.getMonth()+1)+"-"+currDate.getDate()+" 07:30:00";
            var e1Shift=currDate.getFullYear()+"-"+(currDate.getMonth()+1)+"-"+currDate.getDate()+" 19:30:00";
            var m2Shift=currDate.getFullYear()+"-"+(currDate.getMonth()+1)+"-"+currDate.getDate()+" 08:00:00";
            var e2Shift=currDate.getFullYear()+"-"+(currDate.getMonth()+1)+"-"+currDate.getDate()+" 20:00:00";
            if($scope.config.byType=='realTime'){
            	 params.startDate= UtilsService.serverFommateDate(new Date());
                 params.endDate= UtilsService.serverFommateDate(new Date());
            }else{
            	 params.startDate= UtilsService.serverFommateDate(new Date($scope.statementConfig.PMPstartDate));
                 params.endDate= UtilsService.serverFommateDate(new Date($scope.statementConfig.endDate));
                 if((new Date(params.endDate)).getTime()-(new Date(params.startDate)).getTime()>92*24*60*60*1000){
                 	$scope.addAlert('danger','开始和结束时间跨度不超过92天');
                     return;
                 }
            }
           
            if(new Date(params.startDate)>new Date(params.endDate)){
                $scope.addAlert('danger','开始时间不能大于结束时间');
                return;
            }
            $scope.showBodyModel("正在加载数据,请稍后...");
            statementService.ProduceMicroPlanList(params).then(function (resultDatas){
                $scope.hideBodyModel();
                if(resultDatas.response && resultDatas.response.length > 0){
                	if($scope.produceMicroPlanStatement.columnDefs[0].name!="RESULT_DATE"){
                		$scope.produceMicroPlanStatement.columnDefs.unshift($scope.config.dateCol);
            		}
                	$scope.config.queryGridAreaShow = true;
                	$scope.config.dataNum="总记录数:"+resultDatas.response.length;
                	$scope.config.LoadBtn=false;
                	$scope.config.showCount=true;
                	$scope.produceMicroPlanStatement.data = [];
                		 if(params.timeInterval==30){
                        	 for(var i=0;i<$scope.produceMicroPlanStatement.columnDefs.length;i++){
                      			$scope.produceMicroPlanStatement.columnDefs.splice(8,$scope.produceMicroPlanStatement.columnDefs.length-8);
                      		}
                     		for(var i=0;i<49;i++){
                     			var name=$scope.getGridName(i);
                     			var displayName=$scope.getGridDisplayName('7:30',i,params.timeInterval);
                     			var obj={
                         				 name:name,
                                         displayName:displayName,
                                         minWidth:60,
                                         enableCellEdit : false,
                                         enableCellEditOnFocus : false,
                                         cellTooltip: function (row, col) {
                                             return row.entity[col.colDef.name];
                                         },
                                         cellClass: function(grid, row, col) {
                                        		 if(parseFloat(row.entity[col.colDef.name]) < parseFloat(row.entity.PART_TARGE_QTY_YIELD)){
                                                     return 'background-red text-align-right';
                                                 }else if(parseFloat(row.entity[col.colDef.name]) >= parseFloat(row.entity.PART_TARGE_QTY_YIELD)){
                                                     return 'background-green text-align-right';
                                                 }else {
                                                	 return 'text-align-right';
                                                 }
                                         }
                     			};
                     			$scope.produceMicroPlanStatement.columnDefs.push(obj);
                     		}
                     	}else if(params.timeInterval==120){
                     		if($scope.produceMicroPlanStatement.columnDefs[0].name!="RESULT_DATE"){
                        		$scope.produceMicroPlanStatement.columnDefs.unshift($scope.config.dateCol);
                    		}
                     		for(var i=0;i<$scope.produceMicroPlanStatement.columnDefs.length;i++){
                     			$scope.produceMicroPlanStatement.columnDefs.splice(8,$scope.produceMicroPlanStatement.columnDefs.length-8);
                     		}
                     		for(var j=0;j<12;j++){
                     			var name=$scope.getGridName(j);
                     			var displayName=$scope.getGridDisplayName('8:00',j,params.timeInterval);
                     			var obj={
                         				 name:name,
                                         displayName:displayName,
                                         minWidth:60,
                                         enableCellEdit : false,
                                         enableCellEditOnFocus : false,
                                         cellTooltip: function (row, col) {
                                             return row.entity[col.colDef.name];
                                         },
                                         cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                        	 if(parseFloat(row.entity[col.colDef.name]) < parseFloat(row.entity.PART_TARGE_QTY_YIELD)){
                                                 return 'background-red text-align-right';
                                             }else if(parseFloat(row.entity[col.colDef.name]) >= parseFloat(row.entity.PART_TARGE_QTY_YIELD)){
                                                 return 'background-green text-align-right';
                                             }else {
                                            	 return 'text-align-right';
                                             }
                                 			}
                                         };
                     			$scope.produceMicroPlanStatement.columnDefs.push(obj);
                     		}
                     	}
                		 $scope.produceMicroPlanStatement.columnDefs.push($scope.config.workCenterCol);
                         $scope.produceMicroPlanStatement.columnDefs.push($scope.config.lineAreaCol);
                         $scope.produceMicroPlanStatement.columnDefs.push($scope.config.assetCol);
                         $scope.produceMicroPlanStatement.columnDefs.push($scope.config.modelCol);
                         $scope.produceMicroPlanStatement.columnDefs.push($scope.config.resrceCol);
                	$scope.produceMicroPlanStatement.data=resultDatas.response;
                }else{
                    $scope.addAlert("","暂无数据");
                    $scope.config.showCount=false;
                    $scope.config.queryGridAreaShow = false;
                    $scope.config.queryGridLineShow = false;
                    $scope.config.queryGridDeviceShow = false;
                    $scope.config.queryGridAreaCodeShow = false;
                    $scope.produceMicroPlanStatement.data=[];
                }
            }, function (resultDatas){
                $scope.hideBodyModel();
                $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
            });
        }
        $scope.getGridDisplayName=function(initTime,i,interval){
        	var initDate='2000-01-01 '+initTime;
        	var addMin=Date.parse(initDate)+interval*60*1000*i;
        	var currDate=new Date(addMin);
        	var hour=currDate.getHours();
        	var min='';
        	if(currDate.getMinutes()==0){
        		min='00';
        	}else{
        		min=currDate.getMinutes();
        	}
        	return currDate.getHours()+":"+min;
        }
        $scope.getGridName=function(i){
        	var column=i.toString();
        	var lastStr=column.substr(column.length-1,1);
        	var lastChar=$scope.getLastChar(lastStr);
        	if(i>=0&&i<=9){
        		return "TIME_A"+lastChar;
        	}else if(i>=10&&i<=19){
        		return "TIME_B"+lastChar;
        	}else if(i>=20&&i<=29){
        		return "TIME_C"+lastChar;
        	}else if(i>=30&&i<=39){
        		return "TIME_D"+lastChar;
        	}else if(i>=40&&i<=49){
        		return "TIME_E"+lastChar;
        	}
        }
        $scope.getLastChar=function(lastStr){
        	if(lastStr=="0"){
        		return "A";
        	}else if(lastStr=="1"){
        		return "B"
        	}else if(lastStr=="2"){
        		return "C"
        	}else if(lastStr=="3"){
        		return "D"
        	}else if(lastStr=="4"){
        		return "E"
        	}else if(lastStr=="5"){
        		return "F"
        	}else if(lastStr=="6"){
        		return "G"
        	}else if(lastStr=="7"){
        		return "H"
        	}else if(lastStr=="8"){
        		return "I"
        	}else if(lastStr=="9"){
        		return "J"
        	}
        }
        $scope.downLoad=function(){
        	$scope.showBodyModel("正在导出数据,请稍后...");
        	var params = {
                	byType:$scope.config.byType,
                    workCenter: $scope.statementConfig.currentWorkCenter,
                    lineArea:$scope.statementConfig.currentLine,
                    resourceType:$scope.statementConfig.currentDeviceType,
                    resource:$scope.statementConfig.currentDeviceNum,
                    model:$scope.config.model,
                    item:$scope.config.itemCode,
                    startDate: '',
                    endDate: '',
                };
                if($scope.statementConfig.currentShift){
                    params.shift = $scope.statementConfig.currentShift.shift;
                }
                if($scope.statementConfig.timeInterval){
                	params.timeInterval=$scope.statementConfig.timeInterval.time;
                	params.colNum=$scope.statementConfig.timeInterval.colNum;
                }
                var currDate=new Date();
                var m1Shift=currDate.getFullYear()+"-"+(currDate.getMonth()+1)+"-"+currDate.getDate()+" 07:30:00";
                var e1Shift=currDate.getFullYear()+"-"+(currDate.getMonth()+1)+"-"+currDate.getDate()+" 19:30:00";
                var m2Shift=currDate.getFullYear()+"-"+(currDate.getMonth()+1)+"-"+currDate.getDate()+" 08:00:00";
                var e2Shift=currDate.getFullYear()+"-"+(currDate.getMonth()+1)+"-"+currDate.getDate()+" 20:00:00";
                if($scope.config.byType=='realTime'){
                	 params.startDate= UtilsService.serverFommateDate(new Date());
                     params.endDate= UtilsService.serverFommateDate(new Date());
                }else{
                	 params.startDate= UtilsService.serverFommateDate(new Date($scope.statementConfig.PMPstartDate));
                     params.endDate= UtilsService.serverFommateDate(new Date($scope.statementConfig.endDate));
                     if((new Date(params.endDate)).getTime()-(new Date(params.startDate)).getTime()>92*24*60*60*1000){
                     	$scope.addAlert('danger','开始和结束时间跨度不超过92天');
                         return;
                     }
                }
               
                if(new Date(params.startDate)>new Date(params.endDate)){
                    $scope.addAlert('danger','开始时间不能大于结束时间');
                    return;
                }
                var url = statementService.ProduceMicroPlanExport(params);
                url = url.replace(/undefined/g,"").replace(/null/g,"");
                url = HttpAppService.handleCommenUrl(url);
                $http({
                    url: url,
                    method: "GET",
                    headers: {
                        'Content-type': 'application/json'
                    },
                    responseType: 'arraybuffer'
                }).success(function (data, status, headers, config) {
                    var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                    var objectUrl = URL.createObjectURL(blob);
                    window.location.href=objectUrl;
                    $scope.hideBodyModel();
                }).error(function (data, status, headers, config) {
                });
        }

        //清除输入框
        $scope.deleteAll = function(item){
            if(item == 'area'){
                $scope.statementConfig.currentWorkCenter = [];
                $scope.statementConfig.currentWorkCenterDesc = [];
                $scope.statementConfig.currentLine = [];
                $scope.statementConfig.currentLineDesc = [];
                $scope.statementConfig.deviceNums = [];
                $scope.statementConfig.currentDeviceNum = null;
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.showSelectArea = true;
                $scope.config.showSelectLine = true;
                $scope.config.deviceCode = true;
                $scope.controlQueryBtn();
            }
            if(item == 'line'){
                $scope.statementConfig.currentLine = [];
                $scope.statementConfig.currentLineDesc = [];
                $scope.statementConfig.deviceNums = [];
                $scope.statementConfig.currentDeviceNum = null;
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.showSelectLine = true;
                $scope.config.deviceCode = true;
                $scope.controlQueryBtn();
            }
            if(item == 'deviceType'){
                $scope.statementConfig.currentDeviceType = [];
                $scope.statementConfig.currentDeviceTypeDesc = [];
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.config.deviceType = true;
                $scope.config.deviceCode = true;
                $scope.controlQueryBtn();
            }
            if(item == 'deviceCode'){
                $scope.statementConfig.currentDeviceNum = [];
                $scope.config.deviceCode = true;
                $scope.controlQueryBtn();
            }
            if(item == 'model'){
                $scope.config.model = '' ;
            }
            if(item == 'itemCode'){
                $scope.config.itemCode = '' ;
            }
        }
        //选择时间 这是选择月的
        //生产区域帮助
        $scope.toAreaHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/areaSelectMoreModal.html',
                controller: 'areaSelectMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectAreaModal: function () {
                        return {
                        	workArea : $scope.statementConfig.workCenters
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentWorkCenter = selectedItem.area.join(",");
                $scope.statementConfig.currentWorkCenterDesc = selectedItem.areaName.join(",");
                $scope.config.showSelectArea = false;
                $scope.deleteAll('line');
                $scope.deleteAll('deviceCode');
                $scope.controlQueryBtn();
            }, function () {
            });
        };
        //拉线多选帮助
        $scope.toWireLineHelp = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/lineSelectModal.html',
                controller: 'LineSelectMoreCtrl',
                size: 'lg',
                backdrop: 'false',
                scope: $scope,
                openedClass: 'flex-center-parent',
                resolve: {
                    selectLineModal: function () {
                        return {
                        	workArea: $scope.statementConfig.currentWorkCenter,
                        	workLine: $scope.statementConfig.lines
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentLine = selectedItem.line;
                $scope.statementConfig.currentLineDesc = selectedItem.lineName;
                if($scope.statementConfig.currentLine.length >0){
                    $scope.config.showSelectLine = false;
                }
                $scope.deleteAll('deviceCode');
                $scope.controlQueryBtn();
            }, function () {
            });
        };
        //设备类型帮助
        $scope.toDeviceTypeHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/deviceTypeSelectMoreModal.html',
                controller: 'deviceTypeMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectTypeMoreDevice: function () {
                        return {
                        	deviceCode : $scope.statementConfig.deviceTypes
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentDeviceType = selectedItem.resourceType;
                $scope.statementConfig.currentDeviceTypeDesc = selectedItem.resourceTypeDesc;
                if($scope.statementConfig.currentDeviceType.length >0){
                    $scope.config.deviceType = false;
                }
                $scope.deleteAll('deviceCode');
                $scope.controlQueryBtn();
            }, function () {
            });
        };	
        //设备帮助
        $scope.toDeviceHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/modal/modalDeviceHelp.html',
                controller: 'deviceMoreCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    selectMoreDevice: function () {
                        return {
                        	workArea: $scope.statementConfig.currentWorkCenter,
                        	workLine: $scope.statementConfig.currentLine,
                        	deviceCode : $scope.statementConfig.currentDeviceType
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.statementConfig.currentDeviceNum = selectedItem.area;
                if($scope.statementConfig.currentDeviceNum.length >0){
                    $scope.config.deviceCode = false;
                }
                $scope.controlQueryBtn();
            }, function () {
            });
        };
        //model帮助
        $scope.toModelHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalModelHelp.html',
                controller: 'modelHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportModel: function () {
                        return {
                        	model : $scope.config.model
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.model = selectedItem.join(",");
                $scope.controlQueryBtn();
            }, function () {
            });
        }
        //物料编码帮助
        $scope.toCodeHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalCodeHelp.html',
                controller: 'codeHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportItemcode: function () {
                        return {
                        	model : $scope.config.model,
                        	itemCode:$scope.config.itemCode
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.itemCode = selectedItem.join(",");
                $scope.controlQueryBtn();
            }, function () {
            });
        }
        function __requestGridApi(gridApi){
            $scope.config.gridApi = gridApi;
        }
        //控制显示隐藏列
        $scope.showHideCol=function () {
            if($scope.config.queryGridAreaShow){
                for(var i=0;i<$scope.produceMicroPlanStatement.columnDefs.length;i++) {
                    if (!$scope.produceMicroPlanStatement.columnDefs[i].visible) {
                        $scope.produceMicroPlanStatement.columnDefs[i].visible = true;
                    }
                }
            }
            $scope.config.gridApi.core.refresh();
        };
    }])