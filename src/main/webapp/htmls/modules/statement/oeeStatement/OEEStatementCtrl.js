statementModule.controller('OEEStatementCtrl',['$scope', '$http',
    'HttpAppService', '$timeout','UtilsService',
    'uiGridConstants','$uibModal','plcService','outputTargetService',
    'statementService','$echarts','yieldMaintainService',
    function ($scope, $http,
              HttpAppService, $timeout,UtilsService,
              uiGridConstants,$uibModal,plcService,outputTargetService,
              statementService,$echarts,yieldMaintainService) {

        $scope.config = {
            gridApi: null,
            workCenters : null,
            wireLine : null,
            workCentersForDevice:null,
            wireLineForDevice:null,
            device : '',
            asset:null,
            byTimeType: 'M',
            startDate : UtilsService.serverFommateDateShow(new Date(new Date().getTime()-24*60*60*1000)),
            endDate : UtilsService.serverFommateDateShow(new Date()),
            startDateMouth : UtilsService.serverFormateDateMouth(new Date()),
            endDateMouth : UtilsService.serverFormateDateMouth(new Date()),
            btnDisabledQuery : true,
            OEEReasonCodeReportInside:[],
            OEEReasonCodeReportOutside:[],
            OEEReasonCodeReportBar:[],
            legendData:[],
            alarmLine:[],
            showEchart:false,
            isNecessary:false,
            index:0,
            deviceInput : false,
            dataNum : 0,
            showFirstChar:true,
            showSecondChar:true,
        };

        $scope.gridOEEStatement = {

            columnDefs:[
                {
                    name:"byTime",displayName:'时间',enableCellEdit : false,
                    enableCellEditOnFocus : false,
                },
                {
                    name:"oee",displayName:'OEE',enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                        for(var i=0;i<$scope.config.alarmLine.length;i++){
                            if($scope.config.alarmLine[i].paramId == 'VA01'){
                                if(row.entity.oee=="" || row.entity.oee == null){
                                    return 'background-yellow';
                                }else{
                                    if(parseFloat(row.entity.oee) < parseFloat($scope.config.alarmLine[i].paramValue)){
                                        return 'background-red text-align-right';
                                    }else{
                                        return 'text-align-right';
                                    }
                                }

                            }
                        }
                    }
                },
                {
                    name:"a",displayName:'时间利用率',//availableRate
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                        for(var i=0;i<$scope.config.alarmLine.length;i++){
                            if($scope.config.alarmLine[i].paramId == 'VA03'){
                                if(row.entity.a=="" || row.entity.a == null){
                                    return 'background-yellow text-align-right';
                                }else{
                                    if(parseFloat(row.entity.a) < parseFloat($scope.config.alarmLine[i].paramValue)){
                                        return 'background-red text-align-right';
                                    }else{
                                        return 'text-align-right';
                                    }
                                }

                            }
                        }
                    }
                },
                {
                    name:"p",displayName:'性能率',//property,
                    enableCellEdit : false,
                    enableCellEditOnFocus : false,
                    cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                        for(var i=0;i<$scope.config.alarmLine.length;i++){
                            if($scope.config.alarmLine[i].paramId == 'VA04'){
                                if(row.entity.p=="" || row.entity.p == null){
                                    return 'background-yellow';
                                }else{
                                    if(parseFloat(row.entity.p) < parseFloat($scope.config.alarmLine[i].paramValue)){
                                        return 'background-red text-align-right';
                                    }else{
                                        return 'text-align-right';
                                    }
                                }

                            }
                        }
                    }
                },
                {
                    name:"q",displayName:'优率',//quality
                    cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                        for(var i=0;i<$scope.config.alarmLine.length;i++){
                            if($scope.config.alarmLine[i].paramId == 'VA02'){
                                if(row.entity.q =="" || row.entity.q == null){
                                    return 'background-yellow';
                                }else{
                                    if(parseFloat(row.entity.q) < parseFloat($scope.config.alarmLine[i].paramValue)){
                                        return 'background-red text-align-right';
                                    }else{
                                        return 'text-align-right';
                                    }
                                }

                            }
                        }
                    },enableCellEdit : false,
                    enableCellEditOnFocus : false,
                },
            ],
            onRegisterApi:__onRegisterApi
        };

        function __onRegisterApi(gridApi){
            $scope.config.gridApi = gridApi;
            //$scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue) {
            //
            //});
            //gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
            //
            //});
            // 分页相关
            //$scope.config.gridApi.pagination.on.paginationChanged($scope, function(currentPage, pageSize){
            //    __queryOEEDatas(currentPage, pageSize);
            //    return true;
            //}, $scope);

            //$scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
            //
            //});
            //$scope.config.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
            //    $scope.config.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
            //});
            //选择相关
            //$scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            //
            //});
            //$scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            //
            //});
        };

        $scope.toWorkCenterHelp = function(){
            __toWorkCenterHelp();
        };

        $scope.initWorkCenter = function(){
            $scope.config.wireLine = null;
            $scope.config.device = null;
            $scope.config.asset = null;
        };

        //拉线帮助
        $scope.toWireLineHelp = function(){
            __toWireLineHelp();
        };

        $scope.initWireLine = function(){
            $scope.config.device = null;
            $scope.config.asset = null;
        };

        //设备帮助
        $scope.toDeviceHelp = function(){
            __toDeviceHelp();
        };

        $scope.checkDeviceIsNull = function(){
            __checkDeviceIsNull();
        };

        $scope.queryDatas = function(){
            __queryOEEDatas();
        };

        $scope.selectedTimeType = function(type){
            __selectedTimeType(type);
        };

        $scope.downLoadOEE = function(){
            __downLoadOEE();
        };
        //第一个Echart隐藏显示控制
        $scope.hideFirstChar = function(){
        	var hideDiv=document.getElementById('first-oee-bar'); 
        	hideDiv.style.height="0px";
        	$scope.config.showFirstChar=false;
        };
        $scope.showFirstChar = function(){
        	var hideDiv=document.getElementById('first-oee-bar'); 
        	hideDiv.style.height="500px";
        	$scope.config.showFirstChar=true;
        };
        //第二个Echart隐藏显示控制
        $scope.hideSecondChar = function(){
        	var hideDiv=document.getElementById('second-oee-pie'); 
        	hideDiv.style.height="0px";
        	$scope.config.showSecondChar=false;
        };
        $scope.showSecondChar = function(){
        	var hideDiv=document.getElementById('second-oee-pie'); 
        	hideDiv.style.height="500px";
        	$scope.config.showSecondChar=true;
        };
        
        $scope.deleteAll = function(type){
            __deleteAll(type);
        };
        $scope.focusDeviceCode = function(item){
            if(item == 'resrce'){
                $scope.config.deviceInput = true;
            }else{
                $scope.config.focusInputCode = true;
            }
        }
        $scope.blurDeviceCode = function(item){
            if(item == 'resrce'){
                $scope.config.deviceInput = false;
                var paramActions = $scope.config.device;
                if(paramActions == null || paramActions == ''){
                    return
                }
                $scope.config.resrceDetail = [];
                statementService
                    .OEESelectDeviceCode(paramActions)
                    .then(function (resultDatas){
                        $scope.config.deviceShow = resultDatas.response[0].resrce;
                        $scope.config.assetShow = resultDatas.response[0].asset;
                        $scope.config.AreaShow = resultDatas.response[0].workAreaDes;
                        $scope.config.lineShow = resultDatas.response[0].lineAreaDes;
                    },function (resultDatas){
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    });
            }
        }
        function __unique1(arr){
            var tmpArr = [];
            for(var i=0; i<arr.length; i++){
                //如果当前数组的第i已经保存进了临时数组，那么跳过，
                //否则把当前项push到临时数组里面
                if(tmpArr.indexOf(arr[i]) == -1){
                    tmpArr.push(arr[i]);
                }
            }
            return tmpArr;
        }
        $scope.distribution = {
            echartInstance:null,
            currentIndex:0,
            identity: $echarts.generateInstanceIdentity(),
            dimension: '16:9',
            config: {
                backgroundColor: '#F3F3F3',
                title:{
                    show:true,
                    text:'设备OEE原因代码分布',
                    textStyle:{
                        color:'#000',
                        fontWeight:'bolder',
                        fontSize:20
                    },
                    x: 'left',

                    //itemGap:20,//主副标题之间的间距
                },
                legend:{
                    show:true,
                    x: 'left',
                    y:'middle',
                    orient:'vertical',//horizontal
                    data:[],
                    top:60,
                    //itemGap:20,
                    itemWidth:25,
                    itemHeight:14,
                    //formatter: function (name) {
                    //    return echarts.format.truncateText(name, 40, '14px Microsoft Yahei', '…');
                    //},
                    selectedMode:'multiple',//false/single
                    tooltip: {
                        show: true
                    },
                    //backgroundColor:'rgb(62,142,175)',
                    //shadowColor: 'rgba(0, 0, 0, 0.5)',
                    //shadowBlur: 20,
                    //shadowOffsetX:20,
                    //shadowOffsetY:20,
                },
                tooltip:{
                    show:true,
                    trigger:'item',
                    showContent:true,
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                    //triggerOn:'mousemove',
                    //alwaysShowContent:true,
                },
                toolbox:{
                    show:true,
                    orient:'horizontal',// 'vertical',
                    //itemSize:20,
                    //itemGap:20,
                    showTitle:true,
                    //top:10,
                    //right:10,
                    feature: {
                        magicType: {type: ['line', 'bar']},
                        dataView: {readOnly: true},
                        restore: {},
                        saveAsImage:{},
                    },

                },
                series: [
                    {
                        color:['#B9BC9D', '#64A793', '#E0CB93', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#93959A', '#95B5CA', '#c4ccd3'],
                        name:'原因代码组',
                        type:'pie',
                        selectedMode: 'single',//multiple/single
                        radius: [0, '30%'],
                        center: ['60%','50%'],
                        label: {
                            normal: {
                                show:true,
                                position: 'inner',//'outside'/inner/center
                                //formatter: '{b}: {d}'
                            }
                        },
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        //data:$scope.config.OEEReasonCodeReportInside
                        data:[]
                    },
                    {
                        color:['#7C8EAF', '#61a0a8', '#BC9B90', '#91c7ae','#9BA076',  '#A26F1C', '#bda29a','#93959A', '#95B5CA', '#c4ccd3'],
                        name:'原因代码',
                        type:'pie',
                       /* radius: ['40%', '55%'],*/
                        radius : '55%',
                        center: ['50%','60%'],
                        //data:$scope.config.OEEReasonCodeReportOutside
                        data:[]
                    }
                ]
            },
        };

        $scope.histogram = {
            echartInstance:null,
            currentIndex:0,
            dimension: '16:9',
            identity: 'ad-bar-echart',
            config: {
                backgroundColor: '#F3F3F3',
                title:{
                    show:true,
                    text:'设备OEE',
                    textStyle:{
                        color:'#000',
                        fontWeight:'bolder',
                        fontSize:20
                    },
                    x: 'left',
                    //itemGap:20,//主副标题之间的间距
                },
                tooltip:{
                    show:true,
                    trigger:'axis',
                    showContent:true,
                    formatter: "{a} <br/>{b}: {c}%"
                    //triggerOn:'mousemove',
                    //alwaysShowContent:true,
                },
                toolbox:{
                    show:true,
                    orient:'horizontal',// 'vertical',
                    //itemSize:20,
                    //itemGap:20,
                    showTitle:true,
                    //top:10,
                    //right:10,
                    feature: {
                        magicType: {type: ['line', 'bar']},
                        dataView: {readOnly: true},
                        restore: {},
                        saveAsImage:{},
                    },

                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis : [
                    {
                        type : 'category',
                        data : [],
                        axisTick: {
                            alignWithLabel: true
                        }
                    }
                ],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                series : [
                    {
                        color: ['#61a0a8'],
                        name:'OEE',
                        type:'line',
                        data:[]
                        //data:$scope.config.OEEReasonCodeReportBar
                    }
                ]
            }
        };

        function __toWorkCenterHelp(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalAreaHelp.html',
                controller: 'areaHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportArea: function () {
                        return {
                            workArea : $scope.config.workCenters,
                            multiSelect: false
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.workCenters = {
                    workCenter:selectedItem.area[0],
                    description:selectedItem.areaName[0]
                };
                $scope.initWorkCenter();
            }, function () {
            });
        };

        function __toWireLineHelp(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalLineHelp.html',
                controller: 'LineHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportLine: function () {
                        return {
                            workLine : $scope.config.wireLine?$scope.config.wireLine.workCenter:'',
                            workArea : $scope.config.workCenters?$scope.config.workCenters.workCenter:'',
                            multiSelect: false
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.wireLine = {
                    workCenter:selectedItem.line[0],
                    description: selectedItem.lineName[0]
                };
                $scope.initWireLine();
            }, function () {
            });
        };

        function __toDeviceHelp(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalDeviceHelp.html',
                controller: 'deviceHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportDevice: function () {
                        return {
                            workLine : $scope.config.wireLine?$scope.config.wireLine.workCenter:'',
                            workArea : $scope.config.workCenters?$scope.config.workCenters.workCenter:'',
                            deviceCode : $scope.config.device?$scope.config.device.workCenter:'',
                            multiSelect: false
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.device = selectedItem.selectdDatas[0].resrce;
                $scope.config.asset = selectedItem.selectdDatas[0].asset;

                $scope.config.workCentersForDevice = {
                    workCenter: selectedItem.selectdDatas[0].workArea,
                    description : selectedItem.selectdDatas[0].workAreaDes
                };

                $scope.config.wireLineForDevice = {
                    workCenter:selectedItem.selectdDatas[0].lineArea,
                    description : selectedItem.selectdDatas[0].lineAreaDes
                };
                $scope.config.deviceShow = selectedItem.selectdDatas[0].resrce;
                $scope.config.assetShow = selectedItem.selectdDatas[0].asset;
                $scope.config.AreaShow = selectedItem.selectdDatas[0].workAreaDes;
                $scope.config.lineShow = selectedItem.selectdDatas[0].lineAreaDes;
                if($scope.config.device == null || $scope.config.device == ''){
                    $scope.config.isNecessary = true;
                }else{
                    $scope.config.isNecessary = false;
                }
                __setBtnsDisable();
            }, function () {
            });
        };

        function __checkDate(){
            var start = __parseDateByTimeType($scope.config.startDate,'start',$scope.config.byTimeType);
            var end = __parseDateByTimeType($scope.config.endDate,'end',$scope.config.byTimeType);
            //开始时间不能大于结束时间
            if(new Date(start).getTime()-new Date(end).getTime()>0){
                $scope.addAlert('danger','开始时间不能大于结束时间');
                return false;
            }else{
                if(!__parseTimeLimitByTimeType(start,end,$scope.config.byTimeType)){
                   return false;
                }
            }
            return true;
        };

        function __compareDate(start,end,span){
            if(new Date(end).getTime()-new Date(start).getTime() <= span*31*24*60*60*1000){
                return true;
            }else{
                $scope.addAlert('danger',"开始和结束时间跨度不超过"+span+"个月");
                return false;
            }
        };

        function __parseDateByTimeType(date,dateType,byTimetype){
            if(byTimetype == 'M'){
                return UtilsService.getMonthDate(new Date(date),dateType);
            }else{
                return UtilsService.serverFommateDate(new Date(date));
            }
        };

        function __parseShowDateByTimeType(date,dateType,byTimetype){
            if(byTimetype == 'M'){
                return UtilsService.serverFommateDateShow(new Date(UtilsService.getMonthDate(new Date(date),dateType)));
            }else{
                return UtilsService.serverFommateDateShow(new Date(date));
            }
        };

        function __parseTimeLimitByTimeType(start,end,byTimetype){
            if(byTimetype == 'M'){
                //按月开始结束时间跨度限制：不超过6个月
                return __compareDate(start,end,6);
            }
            else if(byTimetype == 'W'){
            	return __compareDate(start,end,3);
            }
            else{
                //按日、班次查询，开始结束时间跨度限制：不超过1个月
                return __compareDate(start,end,1);
            }
        };

        function __queryOEEDatas(){
            $scope.showBodyModel("正在加载数据,请稍后......");
            $scope.config.OEEReasonCodeReportBar = [];
            $scope.config.OEEReasonCodeReportInside = [];
            $scope.config.OEEReasonCodeReportOutside = [];
            $scope.gridOEEStatement.data = [];
            $scope.config.legendData = [];
            $scope.config.index = 0;
            var xAxis = [];
            var color = [];
            $scope.config.showEchart = false;

            if($scope.config.device == null || $scope.config.device == ''){
                $scope.config.isNecessary = true;
                $scope.hideBodyModel();
                $scope.addAlert('','请选择设备编码');
                return;
            }else{
                if(__checkDate()){
                    $scope.config.isNecessary = false;
                    var paramActions = {
                        site:HttpAppService.getSite(),
                        resourceBo : 'ResourceBO:' + HttpAppService.getSite() +','+$scope.config.device,
                        startDateTime : __parseDateByTimeType($scope.config.startDate,'start',$scope.config.byTimeType),
                        endDateTime : __parseDateByTimeType($scope.config.endDate,'end',$scope.config.byTimeType),
                        byTimeType : $scope.config.byTimeType
                    };
                    $echarts.queryEchartsInstance('ad-bar-echart')
                        .then(function(datas){
                            $scope.histogram.echartInstance = datas;
                            $scope.histogram.echartInstance.showLoading();
                            window.onresize = function(){
                                $scope.distribution.echartInstance.resize();
                                $scope.histogram.echartInstance.resize();
                            };
                            statementService
                                .OEEReportList(paramActions)
                                .then(function (resultDatas){
                                    $scope.histogram.echartInstance.hideLoading();
                                    $scope.hideBodyModel();
                                    if(resultDatas.response && resultDatas.response.chart.length > 0){
                                        for(var i=0;i<resultDatas.response.chart.length;i++){
                                            for(var j=0;j<$scope.config.alarmLine.length;j++){
                                                if($scope.config.alarmLine[j].paramId == 'VA01'){
                                                    if(parseFloat(resultDatas.response.chart[i].oeeAvg) < parseFloat($scope.config.alarmLine[j].paramValue)){
                                                        color.push('#F76F6A');
                                                    }else{
                                                        color.push('#61a0a8');
                                                    }
                                                }
                                            }
                                            if(resultDatas.response.chart[i].byTime.indexOf('早班')>0){
                                            	xAxis.push(resultDatas.response.chart[i].byTime.replace('早班-7点30','M'));
                                            }else if(resultDatas.response.chart[i].byTime.indexOf('晚班')>0){
                                            	xAxis.push(resultDatas.response.chart[i].byTime.replace('晚班-19点30','E'));
                                            }else{
                                            	xAxis.push(resultDatas.response.chart[i].byTime);
                                            }
                                            $scope.config.OEEReasonCodeReportBar.push({
                                                value:resultDatas.response.chart[i].oeeAvg,
                                                itemStyle:{
                                                    normal:{color:color[i]}
                                                }
                                            })
                                        };
                                        $scope.histogram.echartInstance.setOption({
                                            title:{
                                                subtext:__parseDateByTimeType($scope.config.startDate,'start',$scope.config.byTimeType)+"-"+__parseDateByTimeType($scope.config.endDate,'end',$scope.config.byTimeType),
                                                text:$scope.config.device + '设备OEE',
                                            },
                                            /*legend:{
                                                data:['OEE']
                                            },*/
                                            xAxis : [
                                                {
                                                    type : 'category',
                                                    data :xAxis,
                                                    axisTick: {
                                                        alignWithLabel: true
                                                    }
                                                }
                                            ],
                                            yAxis : [
                                                {
                                                    type : 'value',
                                                    axisLabel: {
                                                        show: true,
                                                        interval: 'auto',
                                                        formatter: '{value} %'
                                                    }
                                                }
                                            ],
                                            series: [
                                                {
                                                    name:'OEE',
                                                    type:'bar',
                                                    data:$scope.config.OEEReasonCodeReportBar
                                                }
                                            ]
                                        });


                                    }else{
                                        $scope.config.OEEReasonCodeReportBar = [];
                                    }
                                    var legendData = resultDatas.response.chart.length > 0 ? ['OEE']:[];
                                    $scope.histogram.echartInstance.setOption({
                                        color: color,
                                        title:{

                                            subtext:__parseShowDateByTimeType($scope.config.startDate,'start',$scope.config.byTimeType)+"至"+__parseShowDateByTimeType($scope.config.endDate,'end',$scope.config.byTimeType),
                                            text:$scope.config.device + '设备OEE',
                                        },
                                        /*legend:{
                                            data:legendData
                                        },*/
                                        xAxis : [
                                            {
                                                type : 'category',
                                                data :xAxis,
                                                axisTick: {
                                                    alignWithLabel: true
                                                }
                                            }
                                        ],
                                        yAxis : [
                                            {
                                                type : 'value',
                                                axisLabel: {
                                                    show: true,
                                                    interval: 'auto',
                                                    formatter: '{value} %'
                                                }
                                            }
                                        ],
                                        series: [
                                            {
                                                name:'OEE',
                                                type:'bar',
                                                data:$scope.config.OEEReasonCodeReportBar
                                            }
                                        ]
                                    });
                                    if(resultDatas.response && resultDatas.response.table.length > 0){
                                        $scope.gridOEEStatement.data = resultDatas.response.table;
                                        for(var i=0;i<$scope.gridOEEStatement.data.length;i++){
                                        	if($scope.gridOEEStatement.data[i].oee==null||$scope.gridOEEStatement.data[i].oee==''){
                                        		$scope.gridOEEStatement.data[i].oee='N/A';
                                        	}
                                        	if($scope.gridOEEStatement.data[i].a==null||$scope.gridOEEStatement.data[i].a==''){
                                        		$scope.gridOEEStatement.data[i].a='N/A';
                                        	}
                                        	if($scope.gridOEEStatement.data[i].p==null||$scope.gridOEEStatement.data[i].p==''){
                                        		$scope.gridOEEStatement.data[i].p='N/A';
                                        	}
                                        	if($scope.gridOEEStatement.data[i].q==null||$scope.gridOEEStatement.data[i].q==''){
                                        		$scope.gridOEEStatement.data[i].q='N/A';
                                        	}
                                        }
                                        $scope.config.dataNum = $scope.gridOEEStatement.data.length;
                                    }else{
                                        $scope.gridOEEStatement.data = [];
                                    }
                                    $scope.config.index = $scope.config.index+1;
                                    //柱状图、表格、饼图都没值则弹框，
                                    if($scope.config.index == 4 ){
                                        if(__noTableAndEchart(resultDatas.response.chart,resultDatas.response.table,$scope.config.OEEReasonCodeReportInside,$scope.config.OEEReasonCodeReportOutside)){
                                            $scope.addAlert('','暂无数据');
                                            return;
                                        };
                                    }
                                }, function (resultDatas){
                                    $scope.hideBodyModel();
                                    $scope.gridOEEStatement.data = [];
                                    $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
                                });
                        },function(){});

                    $echarts.queryEchartsInstance($scope.distribution.identity)
                        .then(function(datas){
                            $scope.distribution.echartInstance = datas;
                            $scope.distribution.echartInstance.showLoading();
                            window.onresize = function(){
                                $scope.distribution.echartInstance.resize();
                                $scope.histogram.echartInstance.resize();
                            };
                            statementService
                                .OEEReasonCodeReportInside(paramActions)
                                .then(function (resultDatas){
                                    $scope.distribution.echartInstance.hideLoading();
                                    $scope.hideBodyModel();
                                    if(resultDatas.response && resultDatas.response.length > 0){

                                        for(var i=0;i<resultDatas.response.length;i++){
                                            var reasonCodeGroup = resultDatas.response[i].reasonCodeGroup;
                                            var position = parseFloat(resultDatas.response[i].percent) > parseFloat("10%") ? 'inside':'outside';
                                            /*$scope.config.OEEReasonCodeReportInside.push({
                                                name:resultDatas.response[i].reasonCodeGroup + " " + resultDatas.response[i].description,
                                                value:resultDatas.response[i].intersectionTimeDurationSum,
                                                label:{normal:{
                                                    position : position,
                                                    formatter:reasonCodeGroup
                                                }},
                                                labelLine: {
                                                    normal: {
                                                        show: true,
                                                        length:4,
                                                        length2:3,
                                                    }
                                                },
                                            });*/
                                            
                                            var legend = resultDatas.response[i].reasonCodeGroup + " " + resultDatas.response[i].description
                                            $scope.config.legendData.push(legend);
                                        };

                                    }else{
                                        $scope.config.OEEReasonCodeReportInside = [];
                                    }
                                    $scope.distribution.echartInstance.setOption({
                                        title:{
                                            text:$scope.config.device + '设备OEE原因代码分布',
                                            subtext:__parseShowDateByTimeType($scope.config.startDate,'start',$scope.config.byTimeType)+"至"+__parseShowDateByTimeType($scope.config.endDate,'end',$scope.config.byTimeType) +" "+"单位：分钟",
                                        },
                                        legend:{
                                            data:$scope.config.legendData
                                        },
                                        label: {
                                            normal: {
                                                position: 'outside'
                                            }
                                        },
                                        labelLine: {
                                            normal: {
                                                show: true,
                                                length:4,
                                                length2:5,
                                            }
                                        },
                                        series: [
                                            {
                                                data:$scope.config.OEEReasonCodeReportInside
                                            },
                                            {
                                                data:$scope.config.OEEReasonCodeReportOutside
                                            }
                                        ]
                                    });
                                    //柱状图、表格、饼图都没值则弹框，
                                    $scope.config.index = $scope.config.index+1;
                                    if($scope.config.index == 4 ){
                                        if(__noTableAndEchart($scope.config.OEEReasonCodeReportBar,$scope.gridOEEStatement.data,$scope.config.OEEReasonCodeReportInside,$scope.config.OEEReasonCodeReportOutside)){
                                            $scope.addAlert('','暂无数据');
                                            return;
                                        };
                                    }

                                }, function (resultDatas){
                                    $scope.hideBodyModel();
                                    $scope.config.OEEReasonCodeReportInside = [];
                                    $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
                                });
                            statementService
                                .OEEReasonCodeReportOutside(paramActions)
                                .then(function (resultDatas){
                                    $scope.distribution.echartInstance.hideLoading();
                                    $scope.hideBodyModel();
                                    window.onresize = function(){
                                        $scope.distribution.echartInstance.resize();
                                        $scope.histogram.echartInstance.resize();
                                    };
                                    if(resultDatas.response && resultDatas.response.length > 0){
                                        for(var i=0;i<resultDatas.response.length;i++){
                                            $scope.distribution.config.series[1].data = $scope.config.OEEReasonCodeReportOutside;
                                            var reasonCode = resultDatas.response[i].reasonCode;
                                            $scope.config.OEEReasonCodeReportOutside.push({
                                                name:resultDatas.response[i].reasonCode + " " + resultDatas.response[i].description,
                                                value:resultDatas.response[i].intersectionTimeDurationSum,
                                                label:{normal:{
                                                    formatter:reasonCode+ ' '+ '{d}%'
                                                }},
                                            });
                                            var legend = resultDatas.response[i].reasonCode + " " + resultDatas.response[i].description
                                            $scope.config.legendData.push(legend);

                                        };
                                        $scope.distribution.echartInstance.setOption({
                                            legend:{
                                                data:$scope.config.legendData
                                            },
                                            series: [
                                                {
                                                    data:$scope.config.OEEReasonCodeReportInside
                                                },
                                                {
                                                    data:$scope.config.OEEReasonCodeReportOutside
                                                }
                                            ]
                                        });
                                    }else{
                                        $scope.config.OEEReasonCodeReportOutside = [];
                                    }
                                    $scope.config.index = $scope.config.index+1;
                                    //柱状图、表格、饼图都没值则弹框，
                                    if($scope.config.index == 4 ){
                                        if(__noTableAndEchart($scope.config.OEEReasonCodeReportBar,$scope.gridOEEStatement.data,$scope.config.OEEReasonCodeReportInside,$scope.config.OEEReasonCodeReportOutside)){
                                            $scope.addAlert('','暂无数据');
                                            return;
                                        };
                                    }

                                }, function (resultDatas){
                                    $scope.hideBodyModel();
                                    $scope.config.OEEReasonCodeReportOutside = [];
                                    $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
                                });
                            //$scope.distribution.echartInstance.on('click', function (params) {
                            //    console.log(params);
                            //});
                            //$scope.distribution.echartInstance.on('legendselectchanged', function (params) {
                            //    console.log(params);
                            //});
                            //$interval(function(){
                            //    var dataLen = $scope.distribution.config.series[0].data.length;
                            //    // 取消之前高亮的图形
                            //    $scope.distribution.echartInstance.dispatchAction({
                            //        type: 'downplay',
                            //        seriesIndex: 0,
                            //        dataIndex: $scope.distribution.currentIndex
                            //    });
                            //    $scope.distribution.currentIndex = ($scope.distribution.currentIndex + 1) % dataLen;
                            //    // 高亮当前图形
                            //    $scope.distribution.echartInstance.dispatchAction({
                            //        type: 'highlight',
                            //        seriesIndex: 0,
                            //        dataIndex: $scope.distribution.currentIndex
                            //    });
                            //    // 显示 tooltip
                            //    $scope.distribution.echartInstance.dispatchAction({
                            //        type: 'showTip',
                            //        seriesIndex: 0,
                            //        dataIndex: $scope.distribution.currentIndex
                            //    });
                            //},1000)
                        },function(){});

                    statementService
                        .OEEReportAlarmLine()
                        .then(function (resultDatas){
                            //$scope.hideBodyModel();
                            $scope.config.index = $scope.config.index+1;
                            $scope.config.alarmLine = resultDatas.response;

                        }, function (resultDatas){
                            $scope.hideBodyModel();
                            $scope.addAlert("danger", resultDatas.myHttpConfig.statusDesc);
                        });
                }else{
                    $scope.hideBodyModel();
                };


            }


        };

        function __noTableAndEchart(barDatas,tableData,insideEchart,outSideEchart){
            if(barDatas.length==0
                &&  tableData.length==0
                &&  insideEchart.length==0
                &&  outSideEchart.length == 0){
                $scope.config.showEchart = false;
                $scope.addAlert('','暂无数据');
                return true;
            }
            $scope.config.showEchart = true;
            return false;
        };

        function __setBtnsDisable(){
            if(!$scope.config.device){
                $scope.config.btnDisabledQuery = true;
                return;
            }
            $scope.config.btnDisabledQuery = false;
        };

        function __checkDeviceIsNull(){
            $scope.config.device = $scope.config.device?$scope.config.device.toUpperCase():null;
            __setBtnsDisable();
        };

        function __selectedTimeType(type){
            $scope.config.byTimeType = type;
        };

        function __downLoadOEE(){
        	$scope.showBodyModel("正在导出数据,请稍后...");
            var paramActions = {
                site:HttpAppService.getSite(),
                resourceBo : 'ResourceBO:' + HttpAppService.getSite() +','+$scope.config.device,
                startDateTime : __parseDateByTimeType($scope.config.startDate,'start',$scope.config.byTimeType),
                endDateTime : __parseDateByTimeType($scope.config.endDate,'end',$scope.config.byTimeType),
                byTimeType : $scope.config.byTimeType
            };
            var url = statementService.OEEExportExcelFile(paramActions);
            url = HttpAppService.handleCommenUrl(url);
            $http({
                url: url,
                method: "GET",
                headers: {
                    'Content-type': 'application/json'
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                var objectUrl = URL.createObjectURL(blob);
                window.location.href=objectUrl;
                $scope.hideBodyModel();
            }).error(function (data, status, headers, config) {
            });
        };

        function __deleteAll(type){
            if(type == 'workCenter'){
                $scope.config.workCenters = null;
                $scope.config.wireLine = null;
                $scope.config.device = null;
            }else if(type == 'wireLine'){
                $scope.config.wireLine = null;
                $scope.config.device = null;
            }else if(type == 'device'){
                $scope.config.device = null;
            };
        };

        //控制显示隐藏列
        $scope.showHideCol=function () {
            for(var i=0;i<$scope.gridOEEStatement.columnDefs.length;i++) {
                if (!$scope.gridOEEStatement.columnDefs[i].visible) {
                    $scope.gridOEEStatement.columnDefs[i].visible = true;
                }
            }
            $scope.config.gridApi.core.refresh();
        };

        var refreshEchart=$scope.$on("refreshEchart", function(event,data) {
            UtilsService.log("接受成功shutdownReason");

           if($scope.config.showEchart)
           {
               $scope.timeOuter=$timeout(function(){
                   if($scope.distribution.echartInstance)
                   {
                       $scope.distribution.echartInstance.resize();
                   }
                   if($scope.histogram.echartInstance)
                   {
                       $scope.histogram.echartInstance.resize();
                   }
               },350);
           }

        });

        $scope.$on('$destroy',function(){
            refreshEchart();
            $timeout.cancel($scope.timeOuter);
            UtilsService.log("销毁成功shutdownReason");
        });

    }]);