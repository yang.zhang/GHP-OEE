/**
 * Created by daidongdong on 16/8/10.
 */
//区域帮助
statementModule.controller('areaHelpCtrl', ['$scope','$uibModalInstance','outPutReportArea','statementService','$timeout', function ($scope, $uibModalInstance,outPutReportArea,statementService,$timeout) {

    $scope.config = {
        confirmBtn : true,
        multiSelect: outPutReportArea.multiSelect,
        areaHelp:null,
        isFirstSelect:true
    };
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        multiSelect:$scope.config.multiSelect,

        columnDefs: [
            {
                field: 'description', name: 'description', displayName: '描述', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'workCenter', name: 'workCenter', displayName: '生产区域', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field:"enabled",name:"enabled",displayName:'是否已启用',width: 150,
                cellTooltip: function(row, col){ return "是否已启用"; },
                enableCellEdit: false, enableCellEditOnFocus: false,
                enableColumnMenu: false, enableColumnMenus: false,
                cellTemplate:'andon-ui-grid-rscg/statement-checkbox'
            }
        ],data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
            //gridRow.isSelected = false;
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    };
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.areaHelp = $scope.config.areaHelp?$scope.config.areaHelp.toUpperCase():'';

        if(keycode==13){
            $scope.init();
        }
    };
    $scope.init = function(){
        //var area= outPutReportArea.workArea?outPutReportArea.workArea.split(","):'';
        $scope.gridModalAreaHelp.data = [];
        var area= $scope.config.areaHelp?$scope.config.areaHelp:'';
        statementService
            .outputReportAreaHelp(area)
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridModalAreaHelp.data = resultDatas.response;
                    /*清除ui-grid横向白色块区域 start 覆盖默认样式*/
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }else{
                    $scope.gridModalAreaHelp.data = [];
                    $scope.addAlert('', '暂无数据!');
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
            });
    }
    $scope.init();
    $scope.ok = function () {
        var area = [];
        var areaName = [];
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                area.push($scope.gridModalAreaHelp.data[i].workCenter);
                areaName.push($scope.gridModalAreaHelp.data[i].description);
            }
        }
        var allArea = {
            area : area,
            areaName : areaName
        }
        if(outPutReportArea.reportType!='ppmExport'){
        	if(area.length>10){
                $scope.addAlert("","您选择的数据已超过十条!");
                return;
            }
        }
        $uibModalInstance.close(allArea);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//拉线帮助
statementModule.controller('LineHelpCtrl', ['$scope','$uibModalInstance','outPutReportLine','statementService','$timeout',function ($scope, $uibModalInstance,outPutReportLine,statementService,$timeout) {

    $scope.config = {
        confirmBtn : true,
        multiSelect: outPutReportLine.multiSelect,
        lineHelp:null,
        isFirstSelect:true
    };
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        multiSelect:$scope.config.multiSelect,
        columnDefs: [
			{
			    field: 'description', name: 'description', displayName: '描述', minWidth: 100,
			    enableCellEdit: false, enableCellEditOnFocus: false,
			    validators: { required: true },
			    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
			
			},
            {
                field: 'workCenter', name: 'workCenter', displayName: '拉线', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field:"enabled",name:"enabled",displayName:'是否已启用',width: 150,
                cellTooltip: function(row, col){ return "是否已启用"; },
                enableCellEdit: false, enableCellEditOnFocus: false,
                enableColumnMenu: false, enableColumnMenus: false,
                cellTemplate:'andon-ui-grid-rscg/statement-checkbox'
            }

        ],data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    }
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.lineHelp = $scope.config.lineHelp?$scope.config.lineHelp.toUpperCase():'';

        if(keycode==13){
            $scope.init();
        }
    };
    $scope.init = function(){
        $scope.gridModalAreaHelp.data = [];
        var area = outPutReportLine.workArea?outPutReportLine.workArea.split(","):'';
        var workLine = $scope.config.lineHelp?$scope.config.lineHelp:'';
        statementService
            .outputReportLineHelp(workLine,area)
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridModalAreaHelp.data = resultDatas.response;
                    /*清除ui-grid横向白色块区域 start 覆盖默认样式*/
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }else{
                    $scope.gridModalAreaHelp.data = [];
                    $scope.addAlert('', '暂无数据!');
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
            });
    }
    $scope.init();
    $scope.ok = function () {
        var line = [];
        var lineName = [];
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                line.push($scope.gridModalAreaHelp.data[i].workCenter);
                lineName.push($scope.gridModalAreaHelp.data[i].description);
            }
        }
        var allLine = {
            line : line,
            lineName : lineName
        }
        if(outPutReportLine.reportType!='ppmExport'){
        	if(line.length>10){
                $scope.addAlert("","您选择的数据已超过十条!");
                return;
            }
        }
        $uibModalInstance.close(allLine);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//设备帮助
statementModule.controller('deviceHelpCtrl',['$scope','$uibModalInstance','statementService','outPutReportDevice','$timeout', function ($scope, $uibModalInstance,statementService,outPutReportDevice,$timeout) {

    $scope.config = {
        confirmBtn : true,
        multiSelect: outPutReportDevice.multiSelect,
        deviceHelp:null,
        localResource:[],
        isFirstSelect:true

    };
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        multiSelect:$scope.config.multiSelect,
        columnDefs: [
             {
                 field: 'description', name: 'description', displayName: '描述', minWidth: 100,
                 enableCellEdit: false, enableCellEditOnFocus: false,
                 validators: { required: true },
                 cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

             },
            {
                field: 'resrce', name: 'resrce', displayName: '设备编码', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'asset', name: 'asset', displayName: '资产号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'subAsset', name: 'subAsset', displayName: '子资产号', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'workAreaDes', name: 'workAreaDes', displayName: '生产区域', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'lineAreaDes', name: 'lineAreaDes', displayName: '拉线', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field:"enabled",name:"enabled",displayName:'是否已启用',width: 150,
                cellTooltip: function(row, col){ return "是否已启用"; },
                enableCellEdit: false, enableCellEditOnFocus: false,
                enableColumnMenu: false, enableColumnMenus: false,
                cellTemplate:'andon-ui-grid-rscg/statement-checkbox'
            }

        ],data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    }
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.deviceHelp = $scope.config.deviceHelp?$scope.config.deviceHelp.toUpperCase():'';

        if(keycode==13){
            $scope.filterResource();
        }
    };
    $scope.init = function(){
        $scope.gridModalAreaHelp.data = [];

        var resrce = $scope.config.deviceHelp?$scope.config.deviceHelp:'';
        var lineArea = outPutReportDevice.workLine?outPutReportDevice.workLine.split(","):'';
        var workArea = outPutReportDevice.workArea?outPutReportDevice.workArea.split(","):'';

        statementService
            .outputReportDeviceHelp(resrce,lineArea,workArea)
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridModalAreaHelp.data = resultDatas.response;
                    $scope.config.localResource=resultDatas.response;
                    /*清除ui-grid横向白色块区域 start 覆盖默认样式*/
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }else{
                    $scope.gridModalAreaHelp.data = [];
                    $scope.addAlert('', '暂无数据!');
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
            });
    }
    $scope.init();
    $scope.filterResource=function(){
    	var resource= $scope.config.deviceHelp?$scope.config.deviceHelp:'';
    	if(resource==''){
    		$scope.gridModalAreaHelp.data=$scope.config.localResource;
    	}else{
    		var newRes=resource.replace(/；/ig,';');
            var resourceArrary=newRes.split(";");
            var filterResourceArray=[];
            $scope.config.gridFilterArea=[];
            $scope.gridModalAreaHelp.data=$scope.config.localResource;
            for(var x=0;x<resourceArrary.length;x++){
            	if(resourceArrary[x]!=''){
            		filterResourceArray.push(resourceArrary[x]);
            	}
            }if(filterResourceArray.length==0){
            	$scope.gridModalAreaHelp.data=$scope.config.localResource;
            }else{
            	for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
                	var desc=$scope.gridModalAreaHelp.data[i].description;
                	for(var j=0;j<filterResourceArray.length;j++){
                		if(desc.indexOf(filterResourceArray[j])!=-1){
                    		$scope.config.gridFilterArea.push($scope.gridModalAreaHelp.data[i]);
                    		break;
                    	}
                	}
                }
                $scope.gridModalAreaHelp.data=angular.copy($scope.config.gridFilterArea);
            }
    	}
    }
    $scope.ok = function () {
        var allDatas = {};
        var selectdDatas = [];
        var area = [];

        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                area.push($scope.gridModalAreaHelp.data[i].resrce);
                selectdDatas.push($scope.gridModalAreaHelp.data[i])
            }
        }
        if(outPutReportDevice.reportType!='ppmExport'){
        	 if(area.length>50){
                 $scope.addAlert("","您选择的数据已超过五十条!");
                 return;
             }
        }
        allDatas.area = area;
        allDatas.selectdDatas = selectdDatas;

        $uibModalInstance.close(allDatas);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//设备实时看板重构 modal

//设备帮助
statementModule.controller('modelDetailCtrl',['$scope','$uibModalInstance','statementService','deviceRealData', function ($scope, $uibModalInstance,statementService,deviceRealData) {

    $scope.config = {
        confirmBtn: true,
        deviceHelp: null,
        localResource: [],

    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);


//model帮助
statementModule.controller('modelHelpCtrl', ['$scope','$uibModalInstance','statementService','outPutReportModel','$timeout',function ($scope, $uibModalInstance,statementService,outPutReportModel,$timeout) {

    $scope.config = {
        confirmBtn : true,
        multiSelect: outPutReportModel.multiSelect,
        modelHelp:null,
        localModel:[],
        isFirstSelect:true
    };
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        multiSelect:$scope.config.multiSelect,
        columnDefs: [
            {
                field: 'model', name: 'model', displayName: 'Model', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }

        ],data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    };
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.modelHelp = $scope.config.modelHelp?$scope.config.modelHelp.toUpperCase():'';

        if(keycode==13){
            $scope.filterModel();
        }
    };
    $scope.init = function(){
        $scope.gridModalAreaHelp.data = [];
        var model = $scope.config.modelHelp?$scope.config.modelHelp:'';
        statementService
            .outputReportModelHelp(model)
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridModalAreaHelp.data = resultDatas.response;
                    $scope.config.localModel=resultDatas.response;
                    /*清除ui-grid横向白色块区域 start 覆盖默认样式*/
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }else{
                    $scope.gridModalAreaHelp.data = [];
                    $scope.addAlert('', '暂无数据!');
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
            });
    }
    $scope.init();
    $scope.filterModel=function(){
    	var model= $scope.config.modelHelp?$scope.config.modelHelp:'';
    	if(model==''){
    		$scope.gridModalAreaHelp.data=$scope.config.localModel;
    	}else{
    		var newRes=model.replace(/；/ig,';');
            var resourceArrary=newRes.split(";");
            var filterResourceArray=[];
            $scope.config.gridFilterArea=[];
            $scope.gridModalAreaHelp.data=$scope.config.localModel;
            for(var x=0;x<resourceArrary.length;x++){
            	if(resourceArrary[x]!=''){
            		filterResourceArray.push(resourceArrary[x]);
            	}
            }if(filterResourceArray.length==0){
            	$scope.gridModalAreaHelp.data=$scope.config.localModel;
            }else{
            	for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
                	var desc=$scope.gridModalAreaHelp.data[i].model;
                	for(var j=0;j<filterResourceArray.length;j++){
                		if(desc.indexOf(filterResourceArray[j])!=-1){
                    		$scope.config.gridFilterArea.push($scope.gridModalAreaHelp.data[i]);
                    		break;
                    	}
                	}
                }
                $scope.gridModalAreaHelp.data=angular.copy($scope.config.gridFilterArea);
            }
    	}
    }
    $scope.ok = function () {
        var model = [];
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                model.push($scope.gridModalAreaHelp.data[i].model);
            }
        }
        if(outPutReportModel.reportType!='ppmExport'){
        	if(model.length>10){
                $scope.addAlert("","您选择的数据已超过十条!");
                return;
            }
        }
        $uibModalInstance.close(model);    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//物料编码帮助
statementModule.controller('codeHelpCtrl', ['$scope','$uibModalInstance','statementService','outPutReportItemcode','$timeout',function ($scope, $uibModalInstance,statementService,outPutReportItemcode,$timeout) {
    $scope.config = {
        confirmBtn : true,
        multiSelect: outPutReportItemcode.multiSelect,
        codeHelp:null,
        localItem:[],
        isFirstSelect:true
    };
    $scope.gridModalAreaHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        multiSelect:$scope.config.multiSelect,
        columnDefs: [
            {
                field: 'item', name: 'item', displayName: '物料编码', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'description', name: 'description', displayName: '描述', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'model', name: 'model', displayName: 'Model', minWidth: 100,
                enableCellEdit: false, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field:"enabled",name:"enabled",displayName:'是否已启用',width: 150,
                cellTooltip: function(row, col){ return "是否已启用"; },
                enableCellEdit: false, enableCellEditOnFocus: false,
                enableColumnMenu: false, enableColumnMenus: false,
                cellTemplate:'andon-ui-grid-rscg/statement-checkbox'
            }

        ],data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    };
    function __selectAny(){
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.codeHelp = $scope.config.codeHelp?$scope.config.codeHelp.toUpperCase():'';

        if(keycode==13){
            $scope.filterItem();
        }
    };
    $scope.init = function(){
        $scope.gridModalAreaHelp.data = [];
        var model = outPutReportItemcode.model?outPutReportItemcode.model.split(","):'';
        var item = $scope.config.codeHelp?$scope.config.codeHelp:'';
        statementService
            .outputReportCodeHelp(model,item)
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridModalAreaHelp.data = resultDatas.response;
                    $scope.config.localItem = resultDatas.response;
                    /*清除ui-grid横向白色块区域 start 覆盖默认样式*/
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }else{
                    $scope.gridModalAreaHelp.data = [];
                    $scope.addAlert('', '暂无数据!');
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
            });
    }
    $scope.init();
    $scope.filterItem=function(){
    	var item= $scope.config.codeHelp?$scope.config.codeHelp:'';
    	if(item==''){
    		$scope.gridModalAreaHelp.data=$scope.config.localItem;
    	}else{
    		var newRes=item.replace(/；/ig,';');
            var resourceArrary=newRes.split(";");
            var filterResourceArray=[];
            $scope.config.gridFilterArea=[];
            $scope.gridModalAreaHelp.data=$scope.config.localItem;
            for(var x=0;x<resourceArrary.length;x++){
            	if(resourceArrary[x]!=''){
            		filterResourceArray.push(resourceArrary[x]);
            	}
            }if(filterResourceArray.length==0){
            	$scope.gridModalAreaHelp.data=$scope.config.localItem;
            }else{
            	for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
                	var desc=$scope.gridModalAreaHelp.data[i].item;
                	for(var j=0;j<filterResourceArray.length;j++){
                		if(desc.indexOf(filterResourceArray[j])!=-1){
                    		$scope.config.gridFilterArea.push($scope.gridModalAreaHelp.data[i]);
                    		break;
                    	}
                	}
                }
                $scope.gridModalAreaHelp.data=angular.copy($scope.config.gridFilterArea);
            }
    	}
    }
    $scope.ok = function () {
        var area = [];
        for(var i=0;i<$scope.gridModalAreaHelp.data.length;i++){
            if($scope.gridModalAreaHelp.data[i].isSelected == true){
                area.push($scope.gridModalAreaHelp.data[i].item);
            }
        }
        if(outPutReportItemcode.reportType!='ppmExport'){
        	if(area.length>10){
                $scope.addAlert("","您选择的数据已超过十条!");
                return;
            }
        }
        $uibModalInstance.close(area);    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//PRD作业员帮助
statementModule.controller('prdHelpCtrl', ['$scope','$uibModalInstance','statementService','outPutReportModel','$timeout',function ($scope, $uibModalInstance,statementService,outPutReportModel,$timeout) {

    $scope.config = {
        confirmBtn : true,
        multiSelect: outPutReportModel.multiSelect,
        prdlHelp:null,
        localPrd:[],
        isFirstSelect:true
    };
    $scope.gridPrdModalHelp = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        multiSelect:$scope.config.multiSelect,
        columnDefs: [
            {
                field: 'userId', 
                name: 'userId', 
                displayName: 'PRD操作员', 
                minWidth: 100,
                enableCellEdit: false, 
                enableCellEditOnFocus: false,
                validators: {
                	required: true 
                	},
                cellTooltip: function(row, col){
                	return row.entity[col.colDef.name]; 
                },
            },
        ],
        data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
            __selectAny();
        });
    };
    function __selectAny(){
        for(var i=0;i<$scope.gridPrdModalHelp.data.length;i++){
            if($scope.gridPrdModalHelp.data[i].isSelected == true){
                $scope.config.confirmBtn = false;
                return;
            }
        }
        $scope.config.confirmBtn = true;
    };
    $scope.enterKey = function(e){
        var keycode = window.event ? e.keyCode : e.which;
        $scope.config.prdHelp = $scope.config.prdHelp?$scope.config.prdHelp.toUpperCase():'';
        if(keycode==13){
            $scope.filterPrd();
        }
    };
    $scope.init = function(){
        $scope.gridPrdModalHelp.data = [];
        var prd = $scope.config.prdHelp?$scope.config.prdHelp:'';
        statementService
            .outputReportRrdHelp(prd)
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridPrdModalHelp.data = resultDatas.response;
                    $scope.config.localPrd=resultDatas.response;
                    /*清除ui-grid横向白色块区域 start 覆盖默认样式*/
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }else{
                    $scope.gridPrdModalHelp.data = [];
                    $scope.addAlert('', '暂无数据!');
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
            });
    }
    $scope.init();
    $scope.filterPrd=function(){
    	var prd= $scope.config.prdHelp?$scope.config.prdHelp:'';
    	if(prd==''){
    		$scope.gridPrdModalHelp.data=$scope.config.localPrd;
    	}else{
    		var newRes=prd.replace(/；/ig,';');
            var resourceArrary=newRes.split(";");
            var filterResourceArray=[];
            $scope.config.gridFilterArea=[];
            $scope.gridPrdModalHelp.data=$scope.config.localPrd;
            for(var x=0;x<resourceArrary.length;x++){
            	if(resourceArrary[x]!=''){
            		filterResourceArray.push(resourceArrary[x]);
            	}
            }if(filterResourceArray.length==0){
            	$scope.gridPrdModalHelp.data=$scope.config.localPrd;
            }else{
            	for(var i=0;i<$scope.gridPrdModalHelp.data.length;i++){
                	var desc=$scope.gridPrdModalHelp.data[i].userId;
                	for(var j=0;j<filterResourceArray.length;j++){
                		if(desc.indexOf(filterResourceArray[j])!=-1){
                    		$scope.config.gridFilterArea.push($scope.gridPrdModalHelp.data[i]);
                    		break;
                    	}
                	}
                }
                $scope.gridPrdModalHelp.data=angular.copy($scope.config.gridFilterArea);
            }
    	}
    }
    $scope.ok = function () {
        var prd = [];
        for(var i=0;i<$scope.gridPrdModalHelp.data.length;i++){
            if($scope.gridPrdModalHelp.data[i].isSelected == true){
            	prd.push($scope.gridPrdModalHelp.data[i].userId);
            }
        }
        if(outPutReportModel.reportType!='ppmExport'){
        	if(prd.length>10){
                $scope.addAlert("","您选择的数据已超过十条!");
                return;
            }
        }
        $uibModalInstance.close(prd);    
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);