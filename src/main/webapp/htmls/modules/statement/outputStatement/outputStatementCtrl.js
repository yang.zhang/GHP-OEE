statementModule.controller('outputStatementCtrl',['$scope', '$http',
    'HttpAppService', '$timeout',
    'uiGridConstants','$uibModal','$echarts','$interval','statementService','UtilsService',
    function ($scope, $http,
              HttpAppService, $timeout,
              uiGridConstants,$uibModal,$echarts,$interval,statementService,UtilsService) {
        $scope.config ={
            outputStatement : '产出报表',

            workArea : "",
            workAreaName : "",
            workLineName : "",
            workLine : "",
            deviceCode : "",
            model : "",
            itemCode : "",
            startDate : UtilsService.serverFommateDateShow(new Date()),
            endDate : UtilsService.serverFommateDateShow(new Date()),
            startDateMouth : UtilsService.serverFormateDateMouth(new Date()),
            endDateMouth : UtilsService.serverFormateDateMouth(new Date()),

            selectType : true,

            type : 'W',
            dateType : "M",

            showSelectArea : true,
            showSelectLine : true,

            showProArea : true,
            showDevice : false,
            showModel : false,
            showCode : false,
            showLine : false,

            selectMouth : true,
            selectWeek : false,
            selectDay : false,
            selectShift : false,

            queryChartShow : false,

            queryGridAreaShow : false,
            queryGridLineShow : false,
            queryGridDeviceShow : false,
            queryGridAreaCodeShow : false,
            addInfosShow : false,

            queryBtn : true,
            pullFileBtn : true,


            byDateDisplayName:'',

            //报表
            outPutStatementInstance : null,
            dataEcharts : null,
            dateTypeEcharts : null,
            showEcharts : false,
            showEchartsOther : false,

            selectName : "",
            currentSelected : "",
            dataSelected : "",
            selectedType :"",
            itemCodeSelectedCopy : [],
            dataNum : 0
        };
        //区域
        $scope.gridOutputAreaStatement = {
            columnDefs:[
                {name:"workArea",displayName:'生产区域', minWidth: 100, enableCellEdit: false,
                    enableCellEditOnFocus : false},
                {name:"workAreaDes",displayName:'生产区域描述', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false},
                {name:"model",displayName:'Model', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false},
                {name:"item",displayName:'物料编码', minWidth: 200, enableCellEdit: false,enableCellEditOnFocus : false},
                {name:"byTime",displayName:'月/周/天/班次', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false},
                {name:"gridQtySum",displayName:'生产总数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"},
                {name:"gridQtyYield",displayName:'良品数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"},
                {name:"gridQtyScrap",displayName:'人工录入坏品数', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"},
                {name:"firstCal",displayName:'优率', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"}
                /* {name:"qtyUnconfirm",displayName:'待确认数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"firstCal",displayName:'一次优', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"qtyYieldResult",displayName:'最终良品数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"qtyScrapResult",displayName:'最终坏品数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"resultCal",displayName:'最终优', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"}*/
            ],
            onRegisterApi:__requestGridApi,
        };

        //拉线
        $scope.gridOutputLineStatement = {
            columnDefs:[
                {name:"workArea",displayName:'生产区域', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"workAreaDes",displayName:'生产区域描述', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"lineArea",displayName:'拉线', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"lineAreaDes",displayName:'拉线描述', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"model",displayName:'Model', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"item",displayName:'物料编码', minWidth: 200, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"byTime",displayName:'月/周/天/班次', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"gridQtySum",displayName:'生产总数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"},
                {name:"gridQtyYield",displayName:'良品数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"},
                {name:"gridQtyScrap",displayName:'人工录入坏品数', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"},
                {name:"firstCal",displayName:'优率', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"}
                /* {name:"qtyUnconfirm",displayName:'待确认数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"firstCal",displayName:'一次优', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"qtyYieldResult",displayName:'最终良品数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"qtyScrapResult",displayName:'最终坏品数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"resultCal",displayName:'最终优', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"}*/
            ],
            onRegisterApi:__requestGridApi,
        };
        //设备
        $scope.gridOutputDeviceStatement = {
            columnDefs:[
                {name:"workArea",displayName:'生产区域', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"workAreaDes",displayName:'生产区域描述', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"lineArea",displayName:'拉线', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"lineAreaDes",displayName:'拉线描述', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"resrce",displayName:'设备号', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"resrceDes",displayName:'设备描述', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"model",displayName:'Model', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"item",displayName:'物料编码', minWidth: 200, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"byTime",displayName:'月/周/天/班次', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"gridQtySum",displayName:'生产总数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right",enableCellEditOnFocus : false,},
                {name:"gridQtyYield",displayName:'良品数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right",enableCellEditOnFocus : false,},
                {name:"gridQtyScrap",displayName:'人工录入坏品数', minWidth: 150, enableCellEdit: false,cellClass:"text-align-right",enableCellEditOnFocus : false,},
                {name:"firstCal",displayName:'优率', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right",enableCellEditOnFocus : false,}
                /*{name:"qtyYield",displayName:'初次良品数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"qtyScrap",displayName:'初次坏品数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"qtyUnconfirm",displayName:'待确认数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"firstCal",displayName:'一次优', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"qtyYieldResult",displayName:'最终良品数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"qtyScrapResult",displayName:'最终坏品数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"resultCal",displayName:'最终优', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"}*/
            ],
            onRegisterApi:__requestGridApi,
        };
        //物料编码
        $scope.gridOutputCodeStatement = {
            columnDefs:[
                {name:"model",displayName:'Model', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"item",displayName:'物料编码', minWidth: 200, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"byTime",displayName:'月/周/天/班次', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,},
                {name:"gridQtySum",displayName:'生产总数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"},
                {name:"gridQtyYield",displayName:'良品数', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"},
                {name:"gridQtyScrap",displayName:'人工录入坏品数', minWidth: 150, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"},
                {name:"firstCal",displayName:'优率', minWidth: 100, enableCellEdit: false,enableCellEditOnFocus : false,cellClass:"text-align-right"}
                /* {name:"qtyYield",displayName:'初次良品数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"qtyScrap",displayName:'初次坏品数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"qtyUnconfirm",displayName:'待确认数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"firstCal",displayName:'一次优', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"qtyYieldResult",displayName:'最终良品数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"qtyScrapResult",displayName:'最终坏品数', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"},
                 {name:"resultCal",displayName:'最终优', minWidth: 100, enableCellEdit: false,cellClass:"text-align-right"}*/
            ],
            onRegisterApi:__requestGridApi,
        };
//查询
        $scope.queryDatas = function(){
            $scope.showBodyModel("正在加载数据,请稍后...");
            $scope.gridOutputAreaStatement.data = [];
            $scope.gridOutputLineStatement.data = [];
            $scope.gridOutputDeviceStatement.data = [];
            $scope.gridOutputCodeStatement.data = [];
            $scope.config.queryChartShow = false;
            $scope.config.addInfosShow = false;

            $scope.config.queryGridAreaShow = false;
            $scope.config.queryGridLineShow = false;
            $scope.config.queryGridDeviceShow = false;
            $scope.config.queryGridAreaCodeShow = false;

            $scope.config.dataCode = [];
            $scope.config.dataSelected = [];
            $scope.config.dataSelectedCopy=[];
            $scope.config.currentSelected = [];
            $scope.config.showEcharts = false;
            $scope.config.showEchartsOther = false;


            var resrce = $scope.config.deviceCode;
            var line_area = $scope.config.workLine;
            var work_area = $scope.config.workArea;
            var model = $scope.config.model;
            var item = $scope.config.itemCode;
            var time_type = $scope.config.dateType;
            var condition_type = $scope.config.type;
            var start_date_time='';
            var end_date_time='';




            if($scope.config.selectType == true){
                start_date_time = UtilsService.getMonthDate(new Date($scope.config.startDateMouth),'start');
                end_date_time = UtilsService.getMonthDate(new Date($scope.config.endDateMouth),'end');
            }else{
                start_date_time = UtilsService.serverFommateDate(new Date($scope.config.startDate));
                end_date_time = UtilsService.serverFommateDate(new Date($scope.config.endDate));
            }

            if(new Date(start_date_time)>new Date(end_date_time)){
                $scope.addAlert('danger','结束日期必须大于开始日期');
                $scope.hideBodyModel();
                return;
            }
            
            if(time_type=='M'){
                if(UtilsService.addMonth(new Date(start_date_time),6)<new Date(end_date_time)){
                    $scope.addAlert('danger','开始和结束时间跨度不超过六个月');
                    $scope.hideBodyModel();
                    return;
                }
            }else if(time_type=='W'){
                if(UtilsService.addMonth(new Date(start_date_time),3)<new Date(end_date_time)){
                    $scope.addAlert('danger','开始和结束时间跨度不超过三个月');
                    $scope.hideBodyModel();
                    return;
                }
            }else if(time_type=='D'){
                if(UtilsService.addMonth(new Date(start_date_time),1)<new Date(end_date_time)){
                    $scope.addAlert('danger','开始日期和结束日期的间隔必须小于一个月');
                    $scope.hideBodyModel();
                    return;
                }
            }else if(time_type=='S'){
                if(UtilsService.addMonth(new Date(start_date_time),1)<new Date(end_date_time)){
                    $scope.addAlert('danger','开始日期和结束日期的间隔必须小于一个月');
                    $scope.hideBodyModel();
                    return;
                }
            }
            if(model == '' || model == null){
                model = [];
            }else{
                model = model.split(',');
            }
            if(item == '' || item == null){
                item = [];
            }else{
                item = item.split(',');
            }
            if(resrce == '' || resrce == null){
                resrce =[];
            }else{
                resrce = resrce.split(',');
            }
            if(line_area == '' || line_area == null){
                line_area = [];
            }else{
                line_area = line_area.split(',');
            }
            if(work_area == '' || work_area == null){
                work_area = [];
            }else{
                work_area = work_area.split(',');
            }
            var paramActions = {
                model : model,
                item : item,
                resrce : resrce,
                startDateTime :start_date_time,
                endDateTime :end_date_time,
                byConditionType:condition_type,
                lineArea:line_area,
                byTimeType : time_type,
                workArea:work_area
            }
            statementService
                .outputReportSelect(paramActions)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.config.queryChartShow = true;
                        $scope.config.addInfosShow = true;
                        $scope.config.dataEcharts = resultDatas.response;
                        $scope.config.dataNum = resultDatas.response.length;
                        $scope.config.pullFileBtn = false;
                        $scope.config.dateTypeEcharts = time_type;
                        if(time_type=='M'){
                            $scope.config.byDateDisplayName='月';
                        }else if(time_type=='W'){
                            $scope.config.byDateDisplayName='周';
                        }else if(time_type=='D'){
                            $scope.config.byDateDisplayName='天';
                        }else if(time_type=='S'){
                            $scope.config.byDateDisplayName='班次';
                        }
                        if($scope.config.type == "W") {
                            $scope.gridOutputAreaStatement.columnDefs[4].displayName =  $scope.config.byDateDisplayName;
                            $scope.config.selectedType="W";
                            $scope.config.queryGridAreaShow = true;
                            $scope.gridOutputAreaStatement.data = resultDatas.response;

                            for(var i=0;i<$scope.config.dataEcharts.length;i++){
                                $scope.config.dataCode.push($scope.config.dataEcharts[i].workArea);
                            }
                            $scope.config.dataCode = __unique1($scope.config.dataCode);
                            //console.log($scope.config.dataCode);
                            for(var j=0;j<$scope.config.dataCode.length;j++){
                                for(var i=0;i<$scope.config.dataEcharts.length;i++) {
                                    if($scope.config.dataCode[j] == $scope.config.dataEcharts[i].workArea){
                                        $scope.config.dataSelected.push(
                                            {
                                                code : $scope.config.dataEcharts[i].workArea,
                                                desc : $scope.config.dataEcharts[i].workAreaDes,
                                                info : "代码:"+$scope.config.dataEcharts[i].workArea + " 描述:"+$scope.config.dataEcharts[i].workAreaDes
                                            }
                                        );
                                        break;
                                    }
                                }
                            }
                            $scope.config.selectName = "生产区域";
                            $scope.config.currentSelected = $scope.config.dataSelected[0];
                            $scope.config.dataSelectedCopy=$scope.config.dataSelected;
                            $scope.config.showEcharts = false
                            $scope.config.showEchartsOther = true;
                        }else if($scope.config.type == "L") {
                            $scope.gridOutputLineStatement.columnDefs[6].displayName =  $scope.config.byDateDisplayName;
                            $scope.config.selectedType="L";
                            $scope.config.queryGridLineShow = true;
                            $scope.gridOutputLineStatement.data = resultDatas.response;

                            for(var i=0;i<$scope.config.dataEcharts.length;i++){
                                $scope.config.dataCode.push($scope.config.dataEcharts[i].lineArea);
                            }
                            $scope.config.dataCode = __unique1($scope.config.dataCode);
                            for(var j=0;j<$scope.config.dataCode.length;j++){
                                for(var i=0;i<$scope.config.dataEcharts.length;i++) {
                                    if($scope.config.dataCode[j] == $scope.config.dataEcharts[i].lineArea){
                                        $scope.config.dataSelected.push(
                                            {
                                                code : $scope.config.dataEcharts[i].lineArea,
                                                desc : $scope.config.dataEcharts[i].lineAreaDes,
                                                info : "代码:"+$scope.config.dataEcharts[i].lineArea + " 描述:"+$scope.config.dataEcharts[i].lineAreaDes
                                            }
                                        );
                                        break;
                                    }
                                }
                            }
                            $scope.config.selectName = "拉线";
                            $scope.config.currentSelected = $scope.config.dataSelected[0];
                            $scope.config.dataSelectedCopy=$scope.config.dataSelected;
                            $scope.config.showEcharts = false
                            $scope.config.showEchartsOther = true;
                        }else if($scope.config.type == "R") {
                            $scope.gridOutputDeviceStatement.columnDefs[8].displayName =  $scope.config.byDateDisplayName;
                            $scope.config.selectedType="R";
                            $scope.config.queryGridDeviceShow = true;
                            $scope.gridOutputDeviceStatement.data = resultDatas.response;

                            for(var i=0;i<$scope.config.dataEcharts.length;i++){
                                $scope.config.dataCode.push($scope.config.dataEcharts[i].resrce);
                            }
                            $scope.config.dataCode = __unique1($scope.config.dataCode);
                            for(var j=0;j<$scope.config.dataCode.length;j++){
                                for(var i=0;i<$scope.config.dataEcharts.length;i++) {
                                    if($scope.config.dataCode[j] == $scope.config.dataEcharts[i].resrce){
                                        $scope.config.dataSelected.push(
                                            {
                                                code : $scope.config.dataEcharts[i].resrce,
                                                desc : $scope.config.dataEcharts[i].resrceDes,
                                                info : "代码:"+$scope.config.dataEcharts[i].resrce + " 描述:"+$scope.config.dataEcharts[i].resrceDes
                                            }
                                        );
                                        break;
                                    }
                                }
                            }
                            $scope.config.selectName = "设备";
                            $scope.config.currentSelected = $scope.config.dataSelected[0];
                            $scope.config.dataSelectedCopy=$scope.config.dataSelected;
                            $scope.config.showEcharts = false;
                            $scope.config.showEchartsOther = true;
                            //$scope.echartsShowOther();
                        }else if($scope.config.type == "I") {
                            $scope.gridOutputCodeStatement.columnDefs[2].displayName =  $scope.config.byDateDisplayName;
                            $scope.config.selectedType="I";
                            $scope.config.queryGridAreaCodeShow = true;
                            $scope.gridOutputCodeStatement.data = resultDatas.response;
                            $scope.hideBodyModel();

                            $scope.config.showEcharts = true;
                            $scope.config.showEchartsOther = false;
                        }
                    }else{
                        $scope.addAlert("","查询无数据，请检查员工是否有维护坏品数");
                        $scope.hideBodyModel();
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.hideBodyModel();
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }
        //改变下拉值
        $scope.selectedChanged = function(){
        	$scope.config.dataSelected = angular.copy($scope.config.dataSelectedCopy);
            $scope.echartsShowOther();
        }
        //导出数据
        $scope.pullQueryFile = function(){
        	$scope.showBodyModel("正在导出数据,请稍后...");
            var resrce = $scope.config.deviceCode;
            var line_area = $scope.config.workLine;
            var work_area = $scope.config.workArea;
            var model = $scope.config.model;
            var item = $scope.config.itemCode;
            var time_type = $scope.config.dateType;
            var condition_type = $scope.config.type;

            if(model == '' || model == null){
                model = [];
            }else{
                model = model.split(',');
            }
            if(item == '' || item == null){
                item = [];
            }else{
                item = item.split(',');
            }
            if(resrce == '' || resrce == null){
                resrce =[];
            }else{
                resrce = resrce.split(',');
            }
            if(line_area == '' || line_area == null){
                line_area = [];
            }else{
                line_area = line_area.split(',');
            }
            if(work_area == '' || work_area == null){
                work_area = [];
            }else{
                work_area = work_area.split(',');
            }
            if($scope.config.selectType == true){
                var start_date_time = UtilsService.getMonthDate(new Date($scope.config.startDateMouth),'start');
                var end_date_time = UtilsService.getMonthDate(new Date($scope.config.endDateMouth),'end');
            }else{
                var start_date_time = UtilsService.serverFommateDate(new Date($scope.config.startDate));
                var end_date_time = UtilsService.serverFommateDate(new Date($scope.config.endDate));
            }
            var paramActions = {
                model : model,
                item : item,
                resrce : resrce,
                startDateTime :start_date_time,
                endDateTime :end_date_time,
                byConditionType:condition_type,
                lineArea:line_area,
                byTimeType : time_type,
                workArea:work_area
            }
            var url = statementService
                .outputReportPull();
            // window.open(url);

            $http({
                url: url,
                method: "POST",
                data: paramActions, //this is your json data string
                headers: {
                    'Content-type': 'application/json'
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                var objectUrl = URL.createObjectURL(blob);
                window.location.href=objectUrl;
                $scope.hideBodyModel();
            }).error(function (data, status, headers, config) {
            });
        }
        //动画
        $scope.animateToggle = function(){
            $scope.config.animateOut = !$scope.config.animateOut;
            $scope.config.animateSelect = !$scope.config.animateOut;
            $timeout(function (){
                $scope.config.animateSelect = !$scope.config.animateSelect
            }, 300);

        };
        //转换大小写
        $scope.upModel = function(item){
            if(item == "workArea"){
                $scope.config.workArea = angular.uppercase($scope.config.workArea);
            } else if(item == "workLine"){
                $scope.config.workLine = angular.uppercase($scope.config.workLine);
            } else if(item == "deviceCode"){
                $scope.config.deviceCode = angular.uppercase($scope.config.deviceCode);
            } else if(item == "model"){
                $scope.config.model = angular.uppercase($scope.config.model);
            } else if(item == "itemCode"){
                $scope.config.itemCode = angular.uppercase($scope.config.itemCode);
            }
        }
        //查询按钮的控制
        $scope.disableBtn = function(){
            if($scope.config.workArea == null || $scope.config.workArea == ""){
                $scope.config.showSelectArea = true;
            }else{
                $scope.config.showSelectArea = false;
            }
            if($scope.config.workLine == null || $scope.config.workLine == ""){
                $scope.config.showSelectLine = true;
            }else{
                $scope.config.showSelectLine = false;
            }
            if($scope.config.type == "W"){
                if($scope.config.workArea == null || $scope.config.workArea == ""){
                    $scope.config.queryBtn = true;
                    return;
                }else{
                    $scope.config.queryBtn = false;
                }
            }else if($scope.config.type == "L"){
                if($scope.config.workLine == null || $scope.config.workLine == ""){
                    $scope.config.queryBtn = true;
                    return;
                }else{
                    $scope.config.queryBtn = false;
                }
            }else if($scope.config.type == "R" ){
                if ($scope.config.deviceCode == null || $scope.config.deviceCode == ""){
                    $scope.config.queryBtn = true;
                    return;
                }else{
                    $scope.config.queryBtn = false;
                }
            }else if($scope.config.type == "I" ){
                if($scope.config.itemCode == null || $scope.config.itemCode == ""){
                    $scope.config.queryBtn = true;
                    return;
                }else{
                    $scope.config.queryBtn = false;
                }
            }
        }
        //按照区域 拉线 设备 型号查询
        $scope.changeSelect = function(type){
            if(type == "W"){
                $scope.config.type = "W"
                $scope.config.showProArea = true;
                $scope.config.showLine = false;
                $scope.config.showDevice = false;
                $scope.config.showCode = false;
            }else if(type == "L"){
                $scope.config.type = "L"
                $scope.config.showProArea = false;
                $scope.config.showLine = true;
                $scope.config.showDevice = false;
                $scope.config.showCode = false;
            }else if(type == "R"){
                $scope.config.type = "R"
                $scope.config.showProArea = false;
                $scope.config.showLine = false;
                $scope.config.showDevice = true;
                $scope.config.showCode = false;
            }else if(type == "I"){
                $scope.config.type = "I"
                $scope.config.showProArea = false;
                $scope.config.showLine = false;
                $scope.config.showDevice = false;
                $scope.config.showCode = true;
            }
            $scope.disableBtn();
        };
        //按照 月 周 天 班次 汇总
        $scope.changeAddType = function(type){
            if(type == "M"){
                $scope.config.dateType = "M";
                $scope.config.selectMouth = true;
                $scope.config.selectWeek = false;
                $scope.config.selectDay = false;
                $scope.config.selectShift = false;
                $scope.config.selectType = true;
                $scope.config.startDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.endDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.startDateMouth= UtilsService.serverFormateDateMouth(new Date());
                $scope.config.endDateMouth= UtilsService.serverFormateDateMouth(new Date())
            }else if(type == "W"){
                $scope.config.dateType = "W";
                $scope.config.selectMouth = false;
                $scope.config.selectWeek = true;
                $scope.config.selectDay = false;
                $scope.config.selectShift = false;
                $scope.config.selectType = false;
                $scope.config.startDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.endDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.startDateMouth= UtilsService.serverFormateDateMouth(new Date());
                $scope.config.endDateMouth= UtilsService.serverFormateDateMouth(new Date())
            }else if(type == "D"){
                $scope.config.dateType = "D";
                $scope.config.selectMouth = false;
                $scope.config.selectWeek = false;
                $scope.config.selectDay = true;
                $scope.config.selectShift = false;
                $scope.config.selectType = false;
                $scope.config.startDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.endDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.startDateMouth= UtilsService.serverFormateDateMouth(new Date());
                $scope.config.endDateMouth= UtilsService.serverFormateDateMouth(new Date())
            }else if(type == "S"){
                $scope.config.dateType = "S";
                $scope.config.selectMouth = false;
                $scope.config.selectWeek = false;
                $scope.config.selectDay = false;
                $scope.config.selectShift = true;
                $scope.config.selectType = false;
                $scope.config.startDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.endDate= UtilsService.serverFommateDateShow(new Date());
                $scope.config.startDateMouth= UtilsService.serverFormateDateMouth(new Date());
                $scope.config.endDateMouth= UtilsService.serverFormateDateMouth(new Date())
            }
        };
        //清除输入框
        $scope.deleteAll = function(item){
            if(item == "area"){
                $scope.config.workAreaName ='';
                $scope.config.workArea ='';
                $scope.config.showSelectArea = true;
            }
            if(item == 'line'){
                $scope.config.workLineName ='';
                $scope.config.workLine ='';
                $scope.config.showSelectLine = true;
            }
            if(item == 'deviceCode'){
                $scope.config.deviceCode ='';
            }
            if(item == 'model'){
                $scope.config.model ='';
            }
            if(item == 'itemCode'){
                $scope.config.itemCode ='';
            }

            $scope.disableBtn();
        }
        //选择时间 这是选择月的
        //$scope.
        //生产区域帮助
        $scope.toAreaHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalAreaHelp.html',
                controller: 'areaHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportArea: function () {
                        return {workArea : $scope.config.workArea};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.workArea = selectedItem.area.join(",");
                $scope.config.workAreaName = selectedItem.areaName.join(",");
                $scope.disableBtn();
            }, function () {
            });
        };
        //拉线帮助
        $scope.toWireLineHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalLineHelp.html',
                controller: 'LineHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportLine: function () {
                        return {workLine : $scope.config.workLine,workArea : $scope.config.workArea};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.workLine = selectedItem.line.join(",");
                $scope.config.workLineName = selectedItem.lineName.join(",");
                $scope.disableBtn();
            }, function () {
            });
        };
        //设备帮助
        $scope.toDeviceHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalDeviceHelp.html',
                controller: 'deviceHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportDevice: function () {
                        return {workLine : $scope.config.workLine,workArea : $scope.config.workArea,
                            deviceCode : $scope.config.deviceCode};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.deviceCode = selectedItem.area.join(",");
                $scope.disableBtn();
            }, function () {
            });
        };
        //model帮助
        $scope.toModelHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalModelHelp.html',
                controller: 'modelHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportModel: function () {
                        return {model : $scope.config.model};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.model = selectedItem.join(",");
                $scope.disableBtn();
            }, function () {
            });
        }
        //物料编码帮助
        $scope.toCodeHelp = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/statement/outputStatement/modal/modalCodeHelp.html',
                controller: 'codeHelpCtrl',
                size: 'lg',
                backdrop:'false',
                scope : $scope,
                openedClass:'flex-center-parent',
                resolve: {
                    outPutReportItemcode: function () {
                        return {model : $scope.config.model,itemCode:$scope.config.itemCode};
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.config.itemCode = selectedItem.join(",");
                $scope.disableBtn();
            }, function () {
            });
        }
//echarts 图表
        function __unique1(arr){
            var tmpArr = [];
            for(var i=0; i<arr.length; i++){
                //如果当前数组的第i已经保存进了临时数组，那么跳过，
                //否则把当前项push到临时数组里面
                if(tmpArr.indexOf(arr[i]) == -1){
                    tmpArr.push(arr[i]);
                }
            }
            return tmpArr;
        }
        //图表的生成  选择物料编码时候的处理
        $scope.echartsShow = function(){
            var arrX=[];
            var codeMateArr = [];
            for(var i=0;i<$scope.config.dataEcharts.length;i++){
                arrX.push($scope.config.dataEcharts[i].byTime);
                codeMateArr.push($scope.config.dataEcharts[i].item);
            }
            arrX = __unique1(arrX);
            codeMateArr = __unique1(codeMateArr);
            if($scope.config.dateTypeEcharts == "M"){
                arrX.sort(function(a,b){return new Date(a).getTime()-new Date(b).getTime()});
            }else if($scope.config.dateTypeEcharts == "W"){
                arrX.sort();
            }else if($scope.config.dateTypeEcharts == "D"){
                arrX.sort(function(a,b){return new Date(a).getTime()-new Date(b).getTime()});
            }else if($scope.config.dateTypeEcharts == "S"){
            }

            var dataFirstGoodOld = [];
            var dataFirstBadOld = [];
            var dataAddGoodOld = [];
            var dataAddBadOld = [];
            var codeAndData = [];
            var dataGoodOld = [];
            var dataSumGoodOld = [];
            var dataAddNum=[];
            for(var k=0;k<codeMateArr.length;k++) {
                codeAndData.push({
                    key: codeMateArr[k],
                    value: [],
                });
                for (var i = 0; i < arrX.length; i++) {
                    codeAndData[k].value[i] = {
                        byOther : arrX[i],
                        details : [],
                        addAllFirstGood : 0,
                        addAllFirstBad : 0,
                        addAllGoodNum : 0,
                        addAllBadNum : 0,
                        addAllNum : 0
                    }
                }
            }
            for(var i=0;i<$scope.config.dataEcharts.length;i++) {
                for(var k=0;k<codeAndData.length;k++){
                    for(var j=0;j<codeAndData[k].value.length;j++){
                        if($scope.config.dataEcharts[i].item == codeAndData[k].key && $scope.config.dataEcharts[i].byTime == codeAndData[k].value[j].byOther){
                            codeAndData[k].value[j].details.push($scope.config.dataEcharts[i]);
                        }
                    }
                }
            }
            var options = [];

            for(var i=0;i<codeAndData.length;i++){
                for(var j=0;j<codeAndData[i].value.length;j++){
                    for(var k=0;k<codeAndData[i].value[j].details.length;k++){
                        codeAndData[i].value[j].addAllFirstGood = (__stringToNum(codeAndData[i].value[j].details[k].qtyYield) + codeAndData[i].value[j].addAllFirstGood *1000)/1000;
                        codeAndData[i].value[j].addAllFirstBad = (__stringToNum(codeAndData[i].value[j].details[k].qtyScrap) + codeAndData[i].value[j].addAllFirstBad *1000)/1000;
                        codeAndData[i].value[j].addAllGoodNum = (__stringToNum(codeAndData[i].value[j].details[k].qtyYieldResult) + codeAndData[i].value[j].addAllGoodNum *1000)/1000;
                        codeAndData[i].value[j].addAllBadNum = (__stringToNum(codeAndData[i].value[j].details[k].qtyScrapResult) + codeAndData[i].value[j].addAllBadNum *1000)/1000;
                        codeAndData[i].value[j].addAllNum = (__stringToNum(codeAndData[i].value[j].details[k].qtySum) + codeAndData[i].value[j].addAllNum *1000)/1000;

                    }
                }
            }
            function __stringToNum(item){
                if(isNaN(item)){
                    return 0;
                }else {
                    return (parseFloat(item) * 1000);
                }
            }
            for(var i=0;i<codeAndData.length;i++){
                for(var j=0;j<codeAndData[i].value.length;j++){
                    codeAndData[i].value[j].data = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : codeAndData[i].value[j].addAllFirstGood
                    };
                    codeAndData[i].value[j].data1 = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : codeAndData[i].value[j].addAllFirstBad
                    };
                    codeAndData[i].value[j].data2 = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : codeAndData[i].value[j].addAllGoodNum
                    };
                    codeAndData[i].value[j].data3 = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : codeAndData[i].value[j].addAllBadNum
                    };
                    codeAndData[i].value[j].data4 = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : __isZeroToNum(codeAndData[i].value[j].addAllFirstGood,codeAndData[i].value[j].addAllNum)
                    };
                    codeAndData[i].value[j].data5 = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : __isZeroToNum(codeAndData[i].value[j].addAllGoodNum,codeAndData[i].value[j].addAllNum)
                    };
                    codeAndData[i].value[j].data6 = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : codeAndData[i].value[j].addAllNum
                    };
                }
            }
            function __isZeroToNum(num,numAdd){
                if(numAdd == 0){
                    return 0;
                }else{
                    return (num/numAdd).toFixed(4);
                }
            };
            for(var i=0;i<codeAndData.length;i++){
                dataFirstGoodOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                dataFirstBadOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                dataAddGoodOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                dataAddBadOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                dataGoodOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                dataSumGoodOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                dataAddNum[i]={
                    key : codeAndData[i].key,
                    value : []
                }
                for(var j=0;j<codeAndData[i].value.length;j++){
                    dataFirstGoodOld[i].value[j]=codeAndData[i].value[j].data;
                    dataFirstBadOld[i].value[j]=codeAndData[i].value[j].data1;
                    dataAddGoodOld[i].value[j]=codeAndData[i].value[j].data2;
                    dataAddBadOld[i].value[j]=codeAndData[i].value[j].data3;
                    dataGoodOld[i].value[j]=codeAndData[i].value[j].data4;
                    dataSumGoodOld[i].value[j]=codeAndData[i].value[j].data5;
                    dataAddNum[i].value[j]=codeAndData[i].value[j].data6;

                }
            }


            var options = [];
            for(var i=0;i<codeAndData.length;i++){
                var name = codeAndData[i].key;
                options.push(
                    {
                        title : {text: '产量汇总'},
                        series : [
                            {data: dataFirstGoodOld[i].value},
                            {data: dataFirstBadOld[i].value},
                            /* {data: dataAddGoodOld[i].value},
                             {data: dataAddBadOld[i].value},*/
                            {data: dataGoodOld[i].value},
                            /*  {data: dataSumGoodOld[i].value},*/
                            {data: dataAddNum[i].value},
                        ]
                    }
                );
            }

            // 基于准备好的dom，初始化echarts实例
            $scope.myChart = echarts.init(document.getElementById('main'));

            // 指定图表的配置项和数据
            var option = {
                baseOption: {
                    timeline: {
                        // y: 0,
                        axisType: 'category',
                        // realtime: false,
                        // loop: false,
                        autoPlay: true,
                        // currentIndex: 2,
                        playInterval: 3000,
                        // controlStyle: {
                        //     position: 'left'
                        // },
                        data: [],
                        label: {
                            formatter : function(s) {
                                return s;
                            }
                        }
                    },
                    title: {
                        subtext: ''
                    },
                    tooltip : {
                        trigger: 'axis',
                        formatter: function(params){
                            if(params.componentSubType == "slider" && params.componentType == "timeline"){
                                return "物料编码:"+params.name
                            }else{
                                var p1 = params[0];
                                var p2 = params[1];
                                var p3 = params[2];
                                var p4 = params[3];
                                return p1.name+"  "+ p1.data.key+
                                    "<br/> <span class='xbr-tooltip-cls' style='background:"+p1.color+";'></span>"+p1.seriesName+":   "+p1.value+
                                    "<br/> <span class='xbr-tooltip-cls' style='background:"+p2.color+";'></span>"+p2.seriesName+":   "+p2.value+
                                    "<br/> <span class='xbr-tooltip-cls' style='background:"+p4.color+";'></span>"+p4.seriesName+":   "+p4.value+
                                    "<br/> <span class='xbr-tooltip-cls' style='background:"+p3.color+";'></span>"+p3.seriesName+":   "+(p3.value*100).toFixed(2)+"%";

                            }
                        }
                    },
                    color : ['#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3'],
                    legend: {
                        x: 'center',
                        data: ['良品数', '人工录入坏品数','生产总数','优率'],
                        selected: {
                            //'GDP': false, '金融': false, '房地产': false
                        }
                    },
                    calculable : true,
                    grid: {
                        top: 80,
                        bottom: 100
                    },
                    toolbox: {
                        feature : {
                            dataView: {show: true, readOnly: true},
                        	magicType: {show: true, type: ['line', 'bar']},
                            restore: {show: true},
                            saveAsImage: {show: true}
                        }
                    },
                    xAxis: [
                        {
                            'type':'category',
                            'axisLabel':{'interval':0},
                            'data':[],
                            splitLine: {show: false}
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            name: '产量'
                        },
                        {
                            type: 'value',
                            name: '优率',
                            position: 'right',
                            axisLine: {
                                lineStyle: {
                                    color: 'blue'
                                }
                            },
                            axisLabel: {
                                formatter:  function(value){
                                    return  (value * 100)+"%"
                                }
                            }
                        },
                    ],
                    series: [
                        {   name: '良品数',
                            type: 'bar',
                            stack: '数量',
                            itemStyle: {
                                normal: {
                                    color: '#9dd772'
                                }
                            }
                        },
                        {name: '人工录入坏品数', type: 'bar',stack: '数量',
                            itemStyle: {
                                normal: {
                                    color: '#f5ad71'
                                }
                            }},
                        /*  {name: '最终良品数', type: 'bar'},
                         {name: '最终坏品数', type: 'bar'},*/
                        {name: '优率', type: 'line',yAxisIndex: 1,},
                        /*{name: '生产总数', type: 'bar',stack: '数量',
                            itemStyle: {
                                normal: {
                                    color: '#97d4ee'
                                }
                            }},*/
                        /* {name: '最终优', type: 'line',yAxisIndex: 1,},*/
                    ]
                },
                options : []
            };
            option.baseOption.timeline.data = codeMateArr;
            var  arrXCopy = [];
            if(arrX.length>8){
                for(var i=0;i<arrX.length;i++){
                    if(i%2 !=0){
                        arrXCopy.push("\n"+arrX[i]);
                    }else{
                        arrXCopy.push(arrX[i]);
                    }
                }
                option.baseOption.xAxis[0].data = arrXCopy;
            }else{
                option.baseOption.xAxis[0].data = arrX;
            }
            for(var i=0;i<arrX.length;i++){
                arrX[i] = arrX[i].replace('早班','M');
                arrX[i] = arrX[i].replace('晚班','E');
            }
            if(arrX.length>20){
                option.baseOption.xAxis[0].axisLabel.interval = 2;
            }else if(arrX.length>10){
                option.baseOption.xAxis[0].axisLabel.interval = 1;
            }
            option.options = options;
            if($scope.config.dateTypeEcharts == "M"){
                option.baseOption.title.subtext = "按月汇总";
            }else if($scope.config.dateTypeEcharts == "W"){
                option.baseOption.title.subtext = "按周汇总";
            }else if($scope.config.dateTypeEcharts == "D"){
                option.baseOption.title.subtext = "按日汇总";
            }else if($scope.config.dateTypeEcharts == "S"){
                option.baseOption.title.subtext = "按班次汇总";
            }

            //console.log(option.options);
            // 使用刚指定的配置项和数据显示图表。
            $scope.myChart.setOption(option);
            window.onresize = $scope.myChart.resize;
            $scope.hideBodyModel();

        }
        $scope.echartsShowOther = function(){
            var dataEcharts = [];
            if($scope.config.selectedType == "W") {
                for(var j=0;j<$scope.config.dataEcharts.length;j++){
                    if($scope.config.currentSelected.code == $scope.config.dataEcharts[j].workArea){
                        dataEcharts.push($scope.config.dataEcharts[j]);
                    }
                }
            }else if($scope.config.selectedType == "L") {
                for(var j=0;j<$scope.config.dataEcharts.length;j++){
                    if($scope.config.currentSelected.code == $scope.config.dataEcharts[j].lineArea){
                        dataEcharts.push($scope.config.dataEcharts[j]);
                    }
                }
            }else if($scope.config.selectedType == "R") {
                for(var j=0;j<$scope.config.dataEcharts.length;j++){
                    if($scope.config.currentSelected.code == $scope.config.dataEcharts[j].resrce){
                        dataEcharts.push($scope.config.dataEcharts[j]);
                    }
                }
            }else if($scope.config.selectedType == "I") {
                return;
            }


            var arrX=[];
            var codeMateArr = [];
            for(var i=0;i<dataEcharts.length;i++){
                arrX.push(dataEcharts[i].byTime);
                codeMateArr.push(dataEcharts[i].item);
            }
            arrX = __unique1(arrX);
            codeMateArr = __unique1(codeMateArr);
            if($scope.config.dateTypeEcharts == "M"){
                arrX.sort(function(a,b){return new Date(a).getTime()-new Date(b).getTime()});
            }else if($scope.config.dateTypeEcharts == "W"){
                arrX.sort();
            }else if($scope.config.dateTypeEcharts == "D"){
                arrX.sort(function(a,b){return new Date(a).getTime()-new Date(b).getTime()});
            }else if($scope.config.dateTypeEcharts == "S"){
            }

            var dataFirstGoodOld = [];
            var dataFirstBadOld = [];
            var dataAddGoodOld = [];
            var dataAddBadOld = [];
            var codeAndData = [];
            var dataGoodOld = [];
            var dataSumGoodOld = [];
            var dataAddNum=[];
            for(var k=0;k<codeMateArr.length;k++) {
                codeAndData.push({
                    key: codeMateArr[k],
                    value: [],
                });
                for (var i = 0; i < arrX.length; i++) {
                    codeAndData[k].value[i] = {
                        byOther : arrX[i],
                        details : [],
                        addAllFirstGood : 0,
                        addAllFirstBad : 0,
                        addAllGoodNum : 0,
                        addAllBadNum : 0,
                        addAllNum : 0
                    }
                }
            }
            for(var i=0;i<dataEcharts.length;i++) {
                for(var k=0;k<codeAndData.length;k++){
                    for(var j=0;j<codeAndData[k].value.length;j++){
                        if(dataEcharts[i].item == codeAndData[k].key && dataEcharts[i].byTime == codeAndData[k].value[j].byOther){
                            codeAndData[k].value[j].details.push(dataEcharts[i]);
                        }
                    }
                }
            }
            var options = [];

            for(var i=0;i<codeAndData.length;i++){
                for(var j=0;j<codeAndData[i].value.length;j++){
                    for(var k=0;k<codeAndData[i].value[j].details.length;k++){
                        codeAndData[i].value[j].addAllFirstGood = (__stringToNum(codeAndData[i].value[j].details[k].qtyYield) + codeAndData[i].value[j].addAllFirstGood *1000)/1000;
                        codeAndData[i].value[j].addAllFirstBad = (__stringToNum(codeAndData[i].value[j].details[k].qtyScrap) + codeAndData[i].value[j].addAllFirstBad *1000)/1000;
                        codeAndData[i].value[j].addAllGoodNum = (__stringToNum(codeAndData[i].value[j].details[k].qtyYieldResult) + codeAndData[i].value[j].addAllGoodNum *1000)/1000;
                        codeAndData[i].value[j].addAllBadNum = (__stringToNum(codeAndData[i].value[j].details[k].qtyScrapResult) + codeAndData[i].value[j].addAllBadNum *1000)/1000;
                        codeAndData[i].value[j].addAllNum = (__stringToNum(codeAndData[i].value[j].details[k].qtySum) + codeAndData[i].value[j].addAllNum *1000)/1000;

                    }
                }
            }
            function __stringToNum(item){
                if(isNaN(item)){
                    return 0;
                }else {
                    return (parseFloat(item) * 1000);
                }
            }
            for(var i=0;i<codeAndData.length;i++){
                for(var j=0;j<codeAndData[i].value.length;j++){
                    codeAndData[i].value[j].data = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : codeAndData[i].value[j].addAllFirstGood
                    };
                    codeAndData[i].value[j].data1 = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : codeAndData[i].value[j].addAllFirstBad
                    };
                    codeAndData[i].value[j].data2 = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : codeAndData[i].value[j].addAllGoodNum
                    };
                    codeAndData[i].value[j].data3 = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : codeAndData[i].value[j].addAllBadNum
                    };
                    codeAndData[i].value[j].data4 = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : __isZeroToNum(codeAndData[i].value[j].addAllFirstGood,codeAndData[i].value[j].addAllNum)
                    };
                    codeAndData[i].value[j].data5 = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : __isZeroToNum(codeAndData[i].value[j].addAllGoodNum,codeAndData[i].value[j].addAllNum)
                    };
                    codeAndData[i].value[j].data6 = {
                        key : codeAndData[i].key,
                        name : codeAndData[i].value[j].byOther,
                        value : codeAndData[i].value[j].addAllNum
                    };
                }
            }
            function __isZeroToNum(num,numAdd){
                if(numAdd == 0){
                    return 0;
                }else{
                    return (num/numAdd).toFixed(4);
                }
            };
            for(var i=0;i<codeAndData.length;i++){
                dataFirstGoodOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                dataFirstBadOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                dataAddGoodOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                dataAddBadOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                dataGoodOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                dataSumGoodOld[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                dataAddNum[i] = {
                    key : codeAndData[i].key,
                    value : []
                }
                for(var j=0;j<codeAndData[i].value.length;j++){
                    dataFirstGoodOld[i].value[j]=codeAndData[i].value[j].data;
                    dataFirstBadOld[i].value[j]=codeAndData[i].value[j].data1;
                    dataAddGoodOld[i].value[j]=codeAndData[i].value[j].data2;
                    dataAddBadOld[i].value[j]=codeAndData[i].value[j].data3;
                    dataGoodOld[i].value[j]=codeAndData[i].value[j].data4;
                    dataSumGoodOld[i].value[j]=codeAndData[i].value[j].data5;
                    dataAddNum[i].value[j]=codeAndData[i].value[j].data6;
                }
            }


            var options = [];
            for(var i=0;i<codeAndData.length;i++){
                var name = codeAndData[i].key;
                options.push(
                    {
                        title : {text: '产量汇总'},
                        series : [
                            {data: dataFirstGoodOld[i].value},
                            {data: dataFirstBadOld[i].value},
                            /*{data: dataAddGoodOld[i].value},
                             {data: dataAddBadOld[i].value},*/
                            {data: dataGoodOld[i].value},
                            /*{data: dataAddNum[i].value},*/
                            /*{data: dataSumGoodOld[i].value},*/
                        ]
                    }
                );
            }

            //console.log(codeAndData);
            //console.log(dataFirstGoodOld);
            //console.log(options);



            // 基于准备好的dom，初始化echarts实例
            $scope.myChartOther = echarts.init(document.getElementById('mainOther'));

            // 指定图表的配置项和数据
            var option = {
                baseOption: {
                    timeline: {
                        // y: 0,
                        axisType: 'category',
                        // realtime: false,
                        // loop: false,
                        autoPlay: true,
                        playInterval: 3000,
                        // controlStyle: {
                        //     position: 'left'
                        // },
                        data: [],
                        label: {
                            formatter : function(s) {
                                return s;
                            }
                        }
                    },
                    title: {
                        subtext: ''
                    },
                    tooltip : {
                        trigger: 'axis',
                        formatter: function(params){
                            if(params.componentSubType == "slider" && params.componentType == "timeline"){
                                return "物料编码:"+params.name
                            }else{
                                var p1 = params[0];
                                var p2 = params[1];
                                var p3 = params[2];
                                var p4 = params[3];
                                return p1.name+"  "+ p1.data.key+
                                    "<br/> <span class='xbr-tooltip-cls' style='background:"+p1.color+";'></span>"+p1.seriesName+":   "+p1.value+
                                    "<br/> <span class='xbr-tooltip-cls' style='background:"+p2.color+";'></span>"+p2.seriesName+":   "+p2.value+
                                    /*"<br/> <span class='xbr-tooltip-cls' style='background:"+p4.color+";'></span>"+p4.seriesName+":   "+p4.value+*/
                                    "<br/> <span class='xbr-tooltip-cls' style='background:"+p3.color+";'></span>"+p3.seriesName+":   "+(p3.value*100).toFixed(2)+"%";
                            }
                        }

                    },
                    color : ['#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3'],
                    legend: {
                        x: 'center',
                        data: ['良品数', '人工录入坏品数', '优率'],
                        selected: {
                        }
                    },
                    calculable : true,
                    grid: {
                        top: 80,
                        bottom: 100
                    },
                    toolbox: {
                        feature : {
                        	dataView: {show: true, readOnly: true},
                            magicType: {show: true, type: ['line', 'bar']},
                            restore: {show: true},
                            saveAsImage: {show: true}
                        }
                    },
                    xAxis: [
                        {
                            'type':'category',
                            'axisLabel':{'interval':0},
                            'data':[],
                            splitLine: {show: false}
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            name: '产量'
                        },
                        {
                            type: 'value',
                            name: '优率',
                            position: 'right',
                            axisLine: {
                                lineStyle: {
                                    color: 'blue'
                                }
                            },
                            axisLabel: {
                                formatter:  function(value){
                                    return  (value * 100)+"%"
                                }
                            }
                        },
                    ],
                    series: [
                        {   name: '良品数',
                            type: 'bar',
                            stack: '数量',
                            itemStyle: {
                                normal: {
                                    color: '#9dd772'
                                }
                            }
                        },
                        {name: '人工录入坏品数', type: 'bar',stack: '数量',
                            itemStyle: {
                                normal: {
                                    color: '#f5ad71'
                                }
                            }},
                        /*  {name: '最终良品数', type: 'bar'},
                         {name: '最终坏品数', type: 'bar'},*/
                        {name: '优率', type: 'line',yAxisIndex: 1,},
                        /*{name: '生产总数', type: 'bar',
                            itemStyle: {
                                normal: {
                                    color: '#97d4ee'
                                }
                            }},*/
                        /* {name: '最终优', type: 'line',yAxisIndex: 1,},*/
                    ]
                },
                options : []
            };
            option.baseOption.timeline.data = codeMateArr;

            for(var i=0;i<arrX.length;i++){
                arrX[i] = arrX[i].replace('早班-7点30','M');
                arrX[i] = arrX[i].replace('晚班-19点30','E');
            }
            var  arrXCopy = [];
            if(arrX.length>4){
                for(var i=0;i<arrX.length;i++){
                    if(i%2 !=0){
                        arrXCopy.push("\n"+arrX[i]);
                    }else{
                        arrXCopy.push(arrX[i]);
                    }
                }
                option.baseOption.xAxis[0].data = arrXCopy;
            }else{
                option.baseOption.xAxis[0].data = arrX;
            }

            if(arrX.length>20){
                option.baseOption.xAxis[0].axisLabel.interval = 2;
            }else if(arrX.length>10){
                option.baseOption.xAxis[0].axisLabel.interval = 1;
            }
            option.options = options;
            if($scope.config.dateTypeEcharts == "M"){
                option.baseOption.title.subtext = "按月汇总";
            }else if($scope.config.dateTypeEcharts == "W"){
                option.baseOption.title.subtext = "按周汇总";
            }else if($scope.config.dateTypeEcharts == "D"){
                option.baseOption.title.subtext = "按日汇总";
            }else if($scope.config.dateTypeEcharts == "S"){
                option.baseOption.title.subtext = "按班次汇总";
            }
            //console.log(option.options);
            // 使用刚指定的配置项和数据显示图表。
            $scope.myChartOther.setOption(option);
            window.onresize = $scope.myChartOther.resize;
            $scope.hideBodyModel();
            // 使用刚指定的配置项和数据显示图表。
            //myChart.setOption(option);
        }

        function __requestGridApi(gridApi){
            $scope.config.gridApi = gridApi;
        }
        //控制显示隐藏列
        $scope.showHideCol=function () {
            if($scope.config.queryGridAreaShow){
                for(var i=0;i<$scope.gridOutputAreaStatement.columnDefs.length;i++) {
                    if (!$scope.gridOutputAreaStatement.columnDefs[i].visible) {
                        $scope.gridOutputAreaStatement.columnDefs[i].visible = true;
                    }
                }
            }
            else if($scope.config.queryGridLineShow){
                for(var i=0;i<$scope.gridOutputLineStatement.columnDefs.length;i++)
                {
                    if(!$scope.gridOutputLineStatement.columnDefs[i].visible){
                        $scope.gridOutputLineStatement.columnDefs[i].visible=true;
                    }
                }
            }
            else if($scope.config.queryGridDeviceShow){
                for(var i=0;i<$scope.gridOutputDeviceStatement.columnDefs.length;i++)
                {
                    if(!$scope.gridOutputDeviceStatement.columnDefs[i].visible){
                        $scope.gridOutputDeviceStatement.columnDefs[i].visible=true;
                    }
                }
            }
            else if($scope.config.queryGridAreaCodeShow){
                for(var i=0;i<$scope.gridOutputCodeStatement.columnDefs.length;i++)
                {
                    if(!$scope.gridOutputCodeStatement.columnDefs[i].visible){
                        $scope.gridOutputCodeStatement.columnDefs[i].visible=true;
                    }
                }
            }
            $scope.config.gridApi.core.refresh();
        };


        var refreshEchart=$scope.$on("refreshEchart", function(event,data) {
            UtilsService.log("接受成功shutdownReason");

            $scope.timeOuter=$timeout(function(){
                if($scope.config.showEcharts)
                {
                    $scope.myChart.resize();
                }
                else if($scope.config.showEchartsOther)
                {
                    $scope.myChartOther.resize();
                }
            },350);

        });

        $scope.$on('$destroy',function(){
            refreshEchart();
            $timeout.cancel($scope.timeOuter);
            UtilsService.log("销毁成功shutdownReason");
        });





        // $scope.config.autoChange.delay
        ////生产区域
//        $scope.toArea = function(){
//            console.log("进入生产区域")
//            var modalInstance = $uibModal.open({
//                animation: true,
//                templateUrl: 'Area.html',
//                controller: 'areaCtrl',
//                size: 'lg',
//                backdrop:'false',
//                scope : $scope,
//                openedClass:'flex-center-parent',
//                //resolve: {
//                //    items: function () {
//                //        return $scope.config.items;
//                //    }
//                //}
//            });
//            modalInstance.result.then(function (selectedItem) {
//            }, function () {
//            });
//        }
//        //拉线
//        $scope.toWireLine = function(){
//            var modalInstance = $uibModal.open({
//                animation: true,
//                templateUrl: 'Wire.html',
//                controller: 'wireCtrl',
//                size: 'lg',
//                backdrop:'false',
//                scope : $scope,
//                openedClass:'flex-center-parent',
//                //resolve: {
//                //    items: function () {
//                //        return $scope.config.items;
//                //    }
//                //}
//            });
//            modalInstance.result.then(function (selectedItem) {
//            }, function () {
//            });
//        }
//        //设备
//        $scope.toDevice = function(){
//            var modalInstance = $uibModal.open({
//                animation: true,
//                templateUrl: 'Device.html',
//                controller: 'deviceCtrl',
//                size: 'lg',
//                backdrop:'false',
//                scope : $scope,
//                openedClass:'flex-center-parent',
//                //resolve: {
//                //    items: function () {
//                //        return $scope.config.items;
//                //    }
//                //}
//            });
//            modalInstance.result.then(function (selectedItem) {
//            }, function () {
//            });
//        }
//        //产品型号
//        $scope.toProductType = function(){
//            var modalInstance = $uibModal.open({
//                animation: true,
//                templateUrl: 'Type.html',
//                controller: 'productTypeCtrl',
//                size: 'lg',
//                backdrop:'false',
//                scope : $scope,
//                openedClass:'flex-center-parent',
//                //resolve: {
//                //    items: function () {
//                //        return $scope.config.items;
//                //    }
//                //}
//            });
//            modalInstance.result.then(function (selectedItem) {
//            }, function () {
//            });
//        }
    }])
//区域
statementModule.controller('areaCtrl', function ($scope, $uibModalInstance) {
    $scope.config = {

    }
    $scope.gridArea = {
        paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes, //[5, 10, 15, 20, 50, 100],
        paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize, //10,
        useExternalPagination: true,

        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
            {
                field: 'manufacture', name: 'manufacture', displayName: '生产区域', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'PRDPrincipal', name: 'PRDPrincipal', displayName: 'PRD负责人', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                // cellTemplate: 'andon-ui-grid-tpls/dataCategory-categoryCode',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
            }
        ],data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
        });
    }

    $scope.ok = function () {
        //$uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
//拉线
statementModule.controller('wireCtrl', function ($scope, $uibModalInstance) {

    $scope.config = {

    }
    $scope.gridWireLine = {
        paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes, //[5, 10, 15, 20, 50, 100],
        paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize, //10,
        useExternalPagination: true,

        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
            {
                field: 'wire', name: 'wire', displayName: '拉线', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }
        ],data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
        });
    }
    $scope.ok = function () {
        //$uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
//设备
statementModule.controller('deviceCtrl', function ($scope, $uibModalInstance) {

    $scope.config = {

    }
    $scope.gridDevice = {
        paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes, //[5, 10, 15, 20, 50, 100],
        paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize, //10,
        useExternalPagination: true,

        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
            {
                field: 'deviceCode', name: 'deviceCode', displayName: '设备编码', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'propertyNum', name: 'propertyNum', displayName: '资产号', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'deviceName', name: 'deviceName', displayName: '设备名称', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }

        ],data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
        });
    }

    $scope.ok = function () {
        //$uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

//产品类型
statementModule.controller('productTypeCtrl', function ($scope, $uibModalInstance) {

    $scope.config = {

    }
    $scope.gridProductType = {
        paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes, //[5, 10, 15, 20, 50, 100],
        paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize, //10,
        useExternalPagination: true,

        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        columnDefs: [
            {
                field: 'deviceCode', name: 'deviceCode', displayName: '设备编码', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'propertyNum', name: 'propertyNum', displayName: '资产号', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            },
            {
                field: 'deviceName', name: 'deviceName', displayName: '设备名称', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },

            }

        ],data: [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
            }
        });
    }
    $scope.ok = function () {
        //$uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});