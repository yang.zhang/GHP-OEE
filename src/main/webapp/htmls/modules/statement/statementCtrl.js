statementModule.controller('statementCtrl',['$scope', '$http',
    'HttpAppService', '$timeout','UtilsService',
    'uiGridConstants','$uibModal','plcService','outputTargetService','yieldMaintainService','statementService',
    function ($scope, $http,
              HttpAppService, $timeout,UtilsService,
              uiGridConstants,$uibModal,plcService,outputTargetService,yieldMaintainService,statementService) {

        $scope.statementConfig = {
            gridApi: null,

            animateOut:false,
            animateSelect:false,

            workCenters:[],
            currentWorkCenter:[],
            currentWorkCenterDesc:null,

            lines:[],
            currentLine:[],
            currentLineDesc:[],

            deviceTypes:[],
            currentDeviceType:null,
            currentDeviceTypeDesc:null,

            alarmRemark:"",
            alarmRemarkCommentDesc:"",
            alarmRemarkCommentCode:"",

            factoryContent:"",
            factoryContentCode:"",
            factoryContentDesc:"",

            deviceNums:[],
            currentDeviceNum:null,
            multipleCurrentDeviceNum:[],

            shifts:[],
            currentShift:null,
            startDate:UtilsService.serverFommateDateShow(new Date(new Date().getTime()-24*60*60*1000)),
            PMPstartDate:UtilsService.serverFommateDateShow(new Date()),
            endDate:UtilsService.serverFommateDateShow(new Date()),

            btnDisabledQuery:true,
            defaultSelectItem1: { descriptionNew:"", description:"", workCenter: "" },
            defaultSelectItem: { descriptionNew:"--- 请选择 ---", description:"--- 请选择 ---", workCenter: null },
            
            queryBtn:true,
            
            //设备状态明细查询按钮
            rsdQueryBtn:true,
            localResourceType:[],
            localResource:[],
            localFactory:[]
        };

        $scope.init = function(){
            //界面打开，则加载，生产区域、设备类型的数据
            //__initDropdowns('init');
            //生产区域
            //__requestDropdownWorkCenters();
            //设备类型
            //__requestDropdownDeviceTypes();
        };

        $scope.init();

        $scope.animateToggle = function(){
            $scope.statementConfig.animateOut = !$scope.statementConfig.animateOut;
            $scope.statementConfig.animateSelect = !$scope.statementConfig.animateOut;
            $timeout(function (){
                $scope.statementConfig.animateSelect = !$scope.statementConfig.animateSelect
            }, 300);

        };

        //设备状态明细报表S,设备实时看板T
        $scope.workCenterChanged = function(type){
                __initDropdowns('workCenterChanged');
                //拉线只有选择了某个生产区域后，拉线下拉选择列表才有相应的拉线数据可选；
                var workCenter = $scope.statementConfig.currentWorkCenter;
                __requestDropdownLines(workCenter);
                __requestDropdownShifts(workCenter);
                var line = [];
                for(var i=0;i<$scope.statementConfig.currentLine.length;i++){
                    line.push($scope.statementConfig.currentLine[i]);
                }                var resourceType = null;
                if($scope.statementConfig.currentDeviceType){
                    resourceType = $scope.statementConfig.currentDeviceType.resourceType;
                }
                __requestDropdownDeviceCodes(workCenter, line, resourceType);
            // }
            __setBtnsDisable(type);
        };

        $scope.dateChanged = function(type){
            __setBtnsDisable(type);
        };

        $scope.arrayToString = function(array){
            var string = [];
            for(var i=0;i<array.length;i++){
                string[i] = array[i].workCenter;
            }
            return string.toString();
        };

        $scope.lineChanged = function(type){
            __initDropdowns('lineChanged');
            var workCenter = $scope.statementConfig.currentWorkCenter;
            var line = [];
            for(var i=0;i<$scope.statementConfig.currentLine.length;i++){
                line.push($scope.statementConfig.currentLine[i]);
            }
            var resourceType = null;
            if($scope.statementConfig.currentDeviceType){
                resourceType = $scope.statementConfig.currentDeviceType;
            }
            __requestDropdownDeviceCodes(workCenter, line, resourceType);
            __setBtnsDisable(type);
        };

        $scope.deviceTypeChanged = function(){
            __initDropdowns('deviceTypeChanged');
            var workCenter = $scope.statementConfig.currentWorkCenter;
            var line = [];
            for(var i=0;i<$scope.statementConfig.currentLine.length;i++){
                line.push($scope.statementConfig.currentLine[i]);
            }                var resourceType = null;
            if($scope.statementConfig.currentDeviceType){
                resourceType = $scope.statementConfig.currentDeviceType;
            }

            __requestDropdownDeviceCodes(workCenter, line, resourceType);
        };

        function __initDropdowns(type){
            if(type == 'init'){
                $scope.statementConfig.workCenters = [];
                $scope.statementConfig.currentWorkCenter = null;

                $scope.statementConfig.deviceTypes = [];
                $scope.statementConfig.currentDeviceType = null;
            }

            if(type == 'init' || type == 'workCenterChanged'){
                $scope.statementConfig.lines = [];
                $scope.statementConfig.currentLine = [];
                $scope.statementConfig.deviceNums = [];
                $scope.statementConfig.currentDeviceNum = null;
                $scope.statementConfig.multipleCurrentDeviceNum = [];
                $scope.statementConfig.shifts = [];
                $scope.statementConfig.currentShift = null;
            }

            if(type == 'lineChanged'){
                $scope.statementConfig.deviceNums = [];
                $scope.statementConfig.currentDeviceNum = null;
                $scope.statementConfig.multipleCurrentDeviceNum = [];
            }

            if(type == 'deviceTypeChanged'){
                $scope.statementConfig.deviceNums = [];
                $scope.statementConfig.currentDeviceNum = null;
                $scope.statementConfig.multipleCurrentDeviceNum = [];
            }

        } ;

        function __requestDropdownWorkCenters(){
            plcService
                .dataWorkCenters()
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.statementConfig.workCenters = [];
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = resultDatas.response[i].description + "  代码:"+resultDatas.response[i].workCenter;

                        }
                        $scope.statementConfig.workCenters=resultDatas.response;
                        if($scope.statementConfig.currentWorkCenter == null || $scope.statementConfig.currentWorkCenter == ''){
                        }else{
                        }
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    if(resultDatas.myHttpConfig.isSessionOut == true || resultDatas.myHttpConfig.isTimeout == true || resultDatas.myHttpConfig.status == 1 || resultDatas.myHttpConfig.status == -1){
                    }else{
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    }
                });
        };

        function __requestDropdownLines(workCenter){
            statementService
                .outputReportLineHelp('',workCenter)
                .then(function (resultDatas){
                    $scope.statementConfig.lines = [];
                    if(resultDatas.response && resultDatas.response.length > 0){
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = resultDatas.response[i].description + "  代码:"+resultDatas.response[i].workCenter;
                            $scope.statementConfig.lines.push(resultDatas.response[i]);

                        }
                        return;
                    }
                    $scope.statementConfig.lines = [];
                },function (resultDatas){ //TODO 检验失败
                    $scope.statementConfig.lines = [];
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };

        function __requestDropdownDeviceTypes(){
            plcService
                .dataResourceTypes()
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.statementConfig.deviceTypes = [];
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = resultDatas.response[i].description + " 代码:"+resultDatas.response[i].resourceType;
                            $scope.statementConfig.deviceTypes.push(resultDatas.response[i]);
                        }
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    if(resultDatas.myHttpConfig.isSessionOut == true || resultDatas.myHttpConfig.isTimeout == true || resultDatas.myHttpConfig.status == 1 || resultDatas.myHttpConfig.status == -1){
                    }else{
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    }
                });
        };

        function __requestDropdownDeviceCodes(workCenter, line, resourceType){
            yieldMaintainService
                .requestDataResourceCodesByLine(workCenter, line, resourceType)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.statementConfig.deviceNums = [];
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = resultDatas.response[i].description + " 代码:"+resultDatas.response[i].resrce;
                            $scope.statementConfig.deviceNums.push(resultDatas.response[i]);
                        }
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };

        function __requestDropdownShifts(){
            var workCenter;
            workCenter = $scope.statementConfig.currentWorkCenter.workCenter;
            outputTargetService
                .dataShifts('',workCenter)
                .then(function (resultDatas){
                    $scope.statementConfig.shifts = [{
                        shift:null,
                        description:'---请选择---',
                        descriptionNew:'---请选择---'
                    }];
                    if(resultDatas.response && resultDatas.response.length > 0){
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = resultDatas.response[i].description + " 代码:"+resultDatas.response[i].shift;
                            $scope.statementConfig.shifts.push(resultDatas.response[i]);
                        }
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };

        //设备状态明细报表S,设备实时看板T
        function __setBtnsDisable(type){
            if(type == 'S'){
                if(!$scope.statementConfig.currentWorkCenter
                    || !$scope.statementConfig.startDate
                    || !$scope.statementConfig.endDate){
                    $scope.statementConfig.btnDisabledQuery = true;
                    return;
                }
            }else if(type == 'T'){
                if(!$scope.statementConfig.currentWorkCenter){
                    $scope.statementConfig.btnDisabledQuery = true;
                    return;
                }
            }

            $scope.statementConfig.btnDisabledQuery = false;
        };

        $scope.upModel = function(){
            $scope.statementConfig.currentDeviceNum = angular.uppercase($scope.statementConfig.currentDeviceNum);
            $scope.statementConfig.multipleCurrentDeviceNum = angular.uppercase($scope.statementConfig.multipleCurrentDeviceNum);
        }
    }]);