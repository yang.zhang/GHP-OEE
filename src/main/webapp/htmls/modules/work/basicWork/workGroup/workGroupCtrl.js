workModule.controller('workGroupCtrl', [
    '$scope', '$http', '$q',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants', 'gridUtil', 'UtilsService','workService',
    function ($scope, $http, $q, HttpAppService, $timeout, $interval,
              uiGridConstants, gridUtil, UtilsService,workService){

		$scope.config = {
			header:{
				'available':'可用作业组',
				'assigend':'已分配作业组'
			},
			getAssigendActivityGroup:[],
			getAvailableActivityGroup:[],
			copyAssigendActivityGroup:[],
			copyAvailableActivityGroup:[],

		};

		//实时监控basicWorkBodyConfig.getAssigendActivityGroup
		$scope.$watch('basicWorkBodyConfig.getAssigendActivityGroup', function(newValue,oldValue, scope){
			$scope.config.getAssigendActivityGroup = newValue;//已分配
		});
		//实时监控basicWorkBodyConfig.getAvailableActivityGroup
		$scope.$watch('basicWorkBodyConfig.getAvailableActivityGroup', function(newValue,oldValue, scope){
			$scope.config.getAvailableActivityGroup = newValue;//可用
		});
		//实时监控basicWorkBodyConfig.copyAssigendActivityGroup
		$scope.$watch('basicWorkBodyConfig.copyAssigendActivityGroup', function(newValue,oldValue, scope){
			$scope.config.copyAssigendActivityGroup = newValue;//复制已分配
		});
		//实时监控basicWorkBodyConfig.copyAvailableActivityGroup
		$scope.$watch('basicWorkBodyConfig.copyAvailableActivityGroup', function(newValue,oldValue, scope){
			$scope.config.copyAvailableActivityGroup = newValue;//复制可用
		});

		//点击编辑，显示保存按钮和取消按钮，编辑之前备注一份数据
		$scope.workGroupEdit = function(){
			if(!$scope.basicWorkBodyConfig.workGroupBtnDisEdit){

				$scope.bodyConfig.overallWatchChanged = true;

				var copyAssigendActivityGroup = $scope.basicWorkBodyConfig.copyAssigendActivityGroup;
				var copyAvailableActivityGroup = $scope.basicWorkBodyConfig.copyAvailableActivityGroup;
				if(angular.isUndefined(copyAssigendActivityGroup) || copyAssigendActivityGroup == null || copyAssigendActivityGroup.length ==0){
					$scope.basicWorkBodyConfig.copyAssigendActivityGroup = angular.copy($scope.basicWorkBodyConfig.getAssigendActivityGroup);
				}
				if(angular.isUndefined(copyAvailableActivityGroup) || copyAvailableActivityGroup == null || copyAvailableActivityGroup.length ==0){
					$scope.basicWorkBodyConfig.copyAvailableActivityGroup = angular.copy($scope.basicWorkBodyConfig.getAvailableActivityGroup);
				}

				$scope.basicWorkBodyConfig.workGroupIsNoEdit = false;
				$scope.basicWorkBodyConfig.workGroupBtnDisEdit = true;
				$scope.basicWorkBodyConfig.workGroupBtnHiddenCancel = false;
				$scope.basicWorkBodyConfig.workGroupBtnHiddenSave = false;
			}else{
				return;
			}

		};

		//点击取消，隐藏取消按钮和保存按钮，回滚所有编辑
		$scope.workGroupCancel = function(){
			if(!$scope.basicWorkBodyConfig.workGroupBtnHiddenCancel){

				$scope.bodyConfig.overallWatchChanged = false;

				$scope.basicWorkBodyConfig.getAvailableActivityGroup = $scope.basicWorkBodyConfig.copyAvailableActivityGroup;
				$scope.basicWorkBodyConfig.getAssigendActivityGroup = $scope.basicWorkBodyConfig.copyAssigendActivityGroup;
				$scope.basicWorkBodyConfig.copyAvailableActivityGroup = [];
				$scope.basicWorkBodyConfig.copyAssigendActivityGroup = [];

				$scope.basicWorkBodyConfig.workGroupIsNoEdit = true;
				$scope.basicWorkBodyConfig.workGroupBtnDisEdit = false;
				$scope.basicWorkBodyConfig.workGroupBtnHiddenCancel = true;
				$scope.basicWorkBodyConfig.workGroupBtnHiddenSave = true;
			}else{
				return;
			}
		};

		//保存
		$scope.workGroupSave = function(){
			__workGroupSave();
		};

		function __workGroupSave(){

			var saveParams  = {
				"avaliableActivityGroupVos":[],
				"assigendActivityGroupVos":[]
			};
			var available = $scope.config.getAvailableActivityGroup;
			var assigend = $scope.config.getAssigendActivityGroup;
			if(available.length > 0){
				for(var i=0;i<available.length;i++){
					saveParams.avaliableActivityGroupVos.push({
						"activityGroup":available[i].activityGroup
					});
				}
			}
			//else{
			//	saveParams.avaliableActivityGroupVOs.push({});
			//}
			if(assigend.length > 0 ){
				for(var j=0;j<assigend.length;j++){
					saveParams.assigendActivityGroupVos.push({
						"activityGroup":assigend[j].activityGroup
					});
				}
			}
			//else{
			//	saveParams.assigendActivityGroupVOs.push({});
			//};
			//console.log(saveParams);
			if($scope.basicWorkBodyConfig.searchBasicWork){
				saveParams.activity = $scope.basicWorkBodyConfig.searchBasicWork;
			}else{
				$scope.addAlert('danger', '作业不可为空');
			};

			$scope.showBodyModel("正在保存");
			workService.
				saveBasicWorkGroup(saveParams)
				.then(function (resultDatas){
					if(!resultDatas.response){
						$scope.hideBodyModel();
						$scope.bodyConfig.overallWatchChanged = false;
						$scope.addAlert('info', '保存成功');

						$scope.queryBasicWork();

						return;
					}else{
						$scope.hideBodyModel();
						$scope.addAlert('danger', '保存失败');
					}
				}, function (resultDatas){
					$scope.hideBodyModel();
					$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
				});
		};
    	
    }]);