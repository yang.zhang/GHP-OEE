workModule.controller('workParamCtrl', [
    '$scope', '$http', '$q','UtilsService',
    'HttpAppService', '$timeout', '$interval','uiGridValidateService',
    'uiGridConstants', 'gridUtil','workService',
    function ($scope, $http, $q,UtilsService,
              HttpAppService, $timeout, $interval,uiGridValidateService,
              uiGridConstants, gridUtil,workService){

        $scope.config = {
            isEdit : false,
            deleteWorkParamList:[],
            index:-1,
            successDatas:[],
            errorDatas:[],
        };

        $scope.gridWorkParam = {

            showGridFooter: false,
            modifierKeysToMultiSelectCells: false,
            columnDefs:[
                {
                    name:"paramId",displayName:'参数序号',
                    enableCellEdit: true,cellClass:'ad-uppercase',
                    enableCellEditOnFocus:true,
                    enableColumnMenu: false,enableColumnMenus: false,
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row;
                        var col = arguments[0].col;
                        // 非新增不可编辑 、新增行可编辑
                        if(!angular.isUndefined(row.entity.modifiedDateTime) && row.entity.modifiedDateTime!=null){
                            return false;
                        }else{
                            return true;
                        }
                    },
                    //editableCellTemplate:'andon-ui-grid-bwp/basicWorkParam-edit-paramId',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }

                },
                {
                    name:"paramDescription",displayName:'参数描述',
                    enableCellEdit: true,
                    enableCellEditOnFocus:true,
                    enableColumnMenu: false,enableColumnMenus: false,
                    cellTemplate: 'andon-ui-grid-bwp/basicWorkParam-edit-temp-description',
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row;
                        var col = arguments[0].col;
                        // 新增行可编辑、点击编辑按钮之后可编辑
                        if(angular.isUndefined(row.entity.modifiedDateTime)
                            || row.entity.modifiedDateTime == null
                            || row.entity.isEdit){
                            return true;
                        }else{
                            return false;
                        }
                    },
                    //editableCellTemplate:'andon-ui-grid-bwp/basicWorkParam-edit-description',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    name:"paramDefaultValue",displayName:'参数默认值',
                    enableCellEdit:true,
                    enableCellEditOnFocus:true,
                    enableColumnMenu: false,enableColumnMenus: false,
                    cellTemplate: 'andon-ui-grid-bwp/basicWorkParam-edit-temp',
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row;
                        var col = arguments[0].col;
                        // 新增行可编辑、点击编辑按钮之后可编辑
                        if(angular.isUndefined(row.entity.modifiedDateTime)
                            || row.entity.modifiedDateTime == null
                            || row.entity.isEdit){
                            return true;
                        }else{
                            return false;
                        }
                    },
                    //editableCellTemplate:'andon-ui-grid-bwp/basicWorkParam-edit',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    name:'modifiedDateTime',visible:false
                },
                {
                    name:'sequenceId',visible:false
                },
                {
                    name:'isEdit',displayName:'是否编辑',visible:false,type:'boolean'
                },
                {
                    name:'isSelected',displayName:'是否显示',visible:false,type: 'boolean'
                }
            ],
            onRegisterApi : __onRegisterApi,
        };

        //初始化后去除grid左边区域
        //$timeout(function(){
        //    /* To hide the blank gap when use selecting and grouping */
        //    $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
        //    $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
        //},0);

        //实时监控basicWorkBodyConfig.workParamDatas
        $scope.$watch('basicWorkBodyConfig.workParamDatas', function(newValue,oldValue, scope){
            $scope.basicWorkBodyConfig.copyWorkParamDatas = angular.copy(newValue);
            $scope.gridWorkParam.data = newValue
        });

        //新增
        $scope.workParamAdd = function(){
            __workParamAdd();
        };

        //删除
        $scope.workParamDelete = function(){
            __workParamDelete();
        };

        //编辑
        $scope.workParamEdit = function(){
            __workParamEdit();

        };

        //取消
        $scope.workParamCancel = function(){
            __workParamCancel();
        };

        //保存
        $scope.workParamSave = function(){
            __workParamSave();
        };

        function __workParamAdd(){
            if(!$scope.basicWorkBodyConfig.workParamBtnDisAdd){
                $scope.bodyConfig.overallWatchChanged = true;
                $scope.basicWorkBodyConfig.workParamBtnDisAdd = false;
                $scope.basicWorkBodyConfig.workParamBtnDisDelete = true;
                $scope.basicWorkBodyConfig.workParamBtnDisEdit = true;

                $scope.basicWorkBodyConfig.workParamBtnHiddenCancel = false;
                $scope.basicWorkBodyConfig.workParamBtnHiddenSave = false;

                if(angular.isUndefined($scope.gridWorkParam.data) || $scope.gridWorkParam.data == null){
                    $scope.gridWorkParam.data = [];
                }
                $scope.gridWorkParam.data.unshift({
                });
            }else{
                return
            }
        };

        function __workParamDelete(){
            if(!$scope.basicWorkBodyConfig.workParamBtnDisDelete){
                var rows = $scope.gridApi.selection.getSelectedRows();
                if(rows.length > 0){
                    var promise = UtilsService.confirm('是否删除', '提示', ['确定', '取消']);
                    promise
                        .then(function(){ // ok
                            for(var i=0;i<$scope.gridWorkParam.data.length;i++){
                                $scope.config.index = $scope.config.index+1;
                                if($scope.gridWorkParam.data[$scope.config.index].isSelected){
                                    workService
                                        .workParamIsUsed($scope.gridWorkParam.data[$scope.config.index].paramId)
                                        .then(function (resultDatas){
                                            if(resultDatas.response){
                                                $scope.config.errorDatas.push($scope.gridWorkParam.data[$scope.config.index])
                                            }else{
                                                $scope.config.successDatas.push($scope.gridWorkParam.data[$scope.config.index]);
                                            }
                                            if($scope.config.index == $scope.gridWorkParam.data.length-1){
                                                if($scope.config.errorDatas.length>0){
                                                    $scope.addAlert('danger','已被引用，无法删除');
                                                    $scope.config.index = -1;
                                                }else{
                                                    $scope.basicWorkBodyConfig.workParamBtnDisDelete = false;
                                                    $scope.basicWorkBodyConfig.workParamBtnDisAdd = true;
                                                    $scope.basicWorkBodyConfig.workParamBtnDisEdit = true;

                                                    $scope.basicWorkBodyConfig.workParamBtnHiddenCancel = false;
                                                    $scope.basicWorkBodyConfig.workParamBtnHiddenSave = false;

                                                    //deleteWorkParamList

                                                    $scope.config.deleteWorkParamList = rows;
                                                    for(var i =0;i<rows.length;i++){
                                                        __workParamDeleteByHashKey(rows[i].$$hashKey);
                                                    };
                                                    $scope.bodyConfig.overallWatchChanged = true;
                                                    $scope.addAlert('info','删除成功');
                                                    $scope.config.index = -1;
                                                }
                                            };

                                        }, function (resultDatas){
                                            $scope.hideBodyModel();
                                            $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                                        });
                                }

                            };

                        }, function(){   // cancel
                            __workParamCancel();
                        });
                }else{
                    $scope.addAlert("danger","先选择要删除的行");
                }
            }else{
                return;
            }
        };

        function __workParamDeleteByHashKey(hashKey){
            for(var i = 0; i < $scope.gridWorkParam.data.length; i++){
                if(hashKey == $scope.gridWorkParam.data[i].$$hashKey){
                    $scope.gridWorkParam.data = UtilsService.removeArrayByIndex($scope.gridWorkParam.data, i);
                    break;
                }
            }
        };

        function __workParamEdit(){
            if(!$scope.basicWorkBodyConfig.workParamBtnDisEdit){
                $scope.bodyConfig.overallWatchChanged = true;

                $scope.basicWorkBodyConfig.workParamBtnDisEdit = false;
                $scope.basicWorkBodyConfig.workParamBtnDisAdd = true;
                $scope.basicWorkBodyConfig.workParamBtnDisDelete = true;
                $scope.basicWorkBodyConfig.workParamBtnHiddenCancel = false;
                $scope.basicWorkBodyConfig.workParamBtnHiddenSave = false;

                for(var i=0;i<$scope.gridWorkParam.data.length;i++){
                    $scope.gridWorkParam.data[i].isEdit = true;
                }

                $scope.config.isEdit = true;
            }else{
                return;
            };

        };

        function __workParamCancel(){
            $scope.bodyConfig.overallWatchChanged = false;
            $scope.config.isEdit = false;
            $scope.basicWorkBodyConfig.workParamBtnDisEdit = false;
            $scope.basicWorkBodyConfig.workParamBtnDisAdd = false;
            $scope.basicWorkBodyConfig.workParamBtnDisDelete = false;

            $scope.basicWorkBodyConfig.workParamBtnHiddenCancel = true;
            $scope.basicWorkBodyConfig.workParamBtnHiddenSave = true;

            for(var i=0;i<$scope.gridWorkParam.data.length;i++){
                $scope.gridWorkParam.data[i].isEdit = false;
            }

            $scope.basicWorkBodyConfig.workParamDatas = $scope.basicWorkBodyConfig.copyWorkParamDatas;
            $scope.gridWorkParam.data = $scope.basicWorkBodyConfig.copyWorkParamDatas;

        };

        function __workParamSave(){
            $scope.showBodyModel("正在保存");
            if(!$scope.basicWorkBodyConfig.workParamBtnHiddenSave){
                var paramDatas = [];
                if(!$scope.basicWorkBodyConfig.workParamBtnDisAdd){
                    var datas = $scope.gridWorkParam.data;
                    //添加
                    for(var i=0;i<datas.length;i++){
                        if(angular.isUndefined(datas[i].modifiedDateTime) || datas[i].modifiedDateTime == null){
                            if(UtilsService.isEmptyString(datas[i].paramId)){
                                $scope.addAlert('','参数序号不可为空');
                                $scope.hideBodyModel();
                                return;
                            }
                            paramDatas.push({
                                "activity": $scope.basicWorkBodyConfig.searchBasicWork,
                                "paramId": datas[i].paramId,
                                "paramDescription": datas[i].paramDescription,
                                "paramDefaultValue": datas[i].paramDefaultValue,
                                "viewActionCode": "C"
                            });
                        };
                    };
                }
                else if(!$scope.basicWorkBodyConfig.workParamBtnDisDelete){
                    //删除
                    var deleteDatas = $scope.config.deleteWorkParamList;
                    for(var j=0;j<deleteDatas.length;j++){
                        paramDatas.push({
                            "activity": $scope.basicWorkBodyConfig.searchBasicWork,
                            "paramId": deleteDatas[j].paramId,
                            "paramDescription": deleteDatas[j].paramDescription,
                            "paramDefaultValue": deleteDatas[j].paramDefaultValue,
                            "sequenceId": deleteDatas[j].sequenceId,
                            "modifiedDateTime": deleteDatas[j].modifiedDateTime,
                            "viewActionCode": "D"
                        });
                    };
                }
                else if(!$scope.basicWorkBodyConfig.workParamBtnDisEdit){
                    //修改
                    var datasUpdate = $scope.gridWorkParam.data;
                    for(var k=0;k<datasUpdate.length;k++){
                        paramDatas.push({
                            "activity": $scope.basicWorkBodyConfig.searchBasicWork,
                            "paramId": datasUpdate[k].paramId,
                            "paramDescription": datasUpdate[k].paramDescription,
                            "paramDefaultValue": datasUpdate[k].paramDefaultValue,
                            "firstModifiedDateTime": datasUpdate[k].modifiedDateTime,
                            "viewActionCode": "U"
                        });
                    };
                };
                console.log(paramDatas);
                workService
                    .saveWorkParams(paramDatas)
                    .then(function (resultDatas){
                        if(!resultDatas.response){
                            $scope.hideBodyModel();
                            $scope.bodyConfig.overallWatchChanged = false;
                            $scope.addAlert('info', '保存成功');
                            $scope.queryBasicWork();
                            return;
                        }else{
                            $scope.hideBodyModel();
                            $scope.addAlert('danger', '保存失败');
                        }
                    }, function (resultDatas){
                        $scope.hideBodyModel();
                        $scope.addAlert('danger', resultDatas.myHttpConfig.errorMessage);
                    });
            }

        };

        function __onRegisterApi(gridApi){
            $scope.gridApi = gridApi;

            $scope.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                var promise = $q.defer();
                if($scope.gridApi.rowEdit){
                    $scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
                }
                //原因代码组必须大写
                if(colDef.field == "paramId" || colDef.name=="paramId"){
                    if(UtilsService.isEmptyString(rowEntity.paramId)){
                        return;
                    };
                    rowEntity.paramId = rowEntity.paramId.toUpperCase();
                };
                promise.resolve();

            });


            //当选择一行时触发
            $scope.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                gridRow.entity.isSelected = gridRow.isSelected;

            });

            //当全选时
            $scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                for(var i = 0; i < gridRows.length; i++){
                    gridRows[i].entity.isSelected = gridRows[i].isSelected;
                }
            });

        };

    }])