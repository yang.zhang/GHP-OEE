workModule.controller('modalInfoSearchResultCtrl', [
    '$scope', '$http', '$q','UtilsService',
    'HttpAppService', '$timeout', '$interval','uiGridValidateService',
    'uiGridConstants', 'gridUtil','$uibModalInstance','workService','items',
    function ($scope, $http, $q, UtilsService ,
              HttpAppService, $timeout, $interval,uiGridValidateService,
              uiGridConstants, gridUtil,$uibModalInstance,workService,items){


        $scope.config = {
            workIsNecessary:false,//必填字段是否为空
            addDatas:{},//新增时数据
            currentExecutionType:null,//已选类型
            executionTypeList:items//所有类型
        };

        //保存新增作业基本信息
        $scope.saveWorkInfo = function(){
            __saveWorkInfo();
        };

        $scope.modalBlurWorkInfo = function(){
            __modalBlurWorkInfo();
        };

        $scope.modalKeyUpWorkInfo = function(){
            __modalKeyUpWorkInfo();
        };

        function __saveWorkInfo(){
            if(!__checkWorkIsNull()){
                $scope.config.workIsNecessary = true;
                return;
            }else{
                var paramsDatas = $scope.config.addDatas;
                paramsDatas.enabled = false;
                paramsDatas.visible = false;

                if($scope.config.currentExecutionType){
                    paramsDatas.executionType = $scope.config.currentExecutionType.executionType;
                };
                paramsDatas.viewActionCode = 'C';

                workService.
                    saveChangedWorkInfo(paramsDatas)
                    .then(function (resultDatas){
                        if(!resultDatas.response){

                            $uibModalInstance.close(paramsDatas);
                        }else{
                            $scope.addAlert('danger', resultDatas.myHttpConfig.errorMessage);
                        }
                    }, function (resultDatas){
                        $scope.addAlert('danger', resultDatas.myHttpConfig.errorMessage);
                    });

            };
        };

        //判定activity是否为空
        function __checkWorkIsNull(){
            var work = $scope.config.addDatas.activity;
            if(angular.isUndefined(work) || work == null || work == ''){
                return false;
            }else{
                return true;
            };
        };

        //activity必须填并且大写
        function __modalBlurWorkInfo(){
            var work = $scope.config.addDatas.activity;
            if(work){
                $scope.config.addDatas.activity = work.toUpperCase();
            }

            if($scope.config.workIsNecessary){
                if(__checkWorkIsNull){
                    $scope.config.workIsNecessary = false;
                }
            };
        };
        //activity必须填并且大写
        function __modalKeyUpWorkInfo(){
            var work = $scope.config.addDatas.activity;
            if(work){
                $scope.config.addDatas.activity = work.toUpperCase();
            }
        };

        //关闭模态框
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }])