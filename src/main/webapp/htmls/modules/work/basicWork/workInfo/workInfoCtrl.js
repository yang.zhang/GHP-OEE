workModule.controller('workInfoCtrl', [
    '$scope', '$http', '$q',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants', 'gridUtil','workService',
    function ($scope, $http, $q, HttpAppService, $timeout, $interval,
              uiGridConstants, gridUtil,workService){


        //点击编辑按钮，进入编辑状态，
        $scope.workInfoEdit = function(){
            if(!$scope.basicWorkBodyConfig.workInfoBtnDisEdit){
                //$scope.bodyConfig.overallWatchChanged = true;
                //编辑之前先复制一份
                var copyWorkInfoData = $scope.basicWorkBodyConfig.copyWorkInfoData;
                if(angular.isUndefined(copyWorkInfoData) || copyWorkInfoData == null || copyWorkInfoData.length ==0){
                    $scope.basicWorkBodyConfig.copyWorkInfoData = angular.copy($scope.basicWorkBodyConfig.workInfoDatas);
                }
                //显示取消按钮和保存按钮
                $scope.basicWorkBodyConfig.workInfoIsNoEdit = false;
                $scope.basicWorkBodyConfig.workInfoBtnDisEdit = true;
                $scope.basicWorkBodyConfig.workInfoBtnHiddenCancel = false;
                $scope.basicWorkBodyConfig.workInfoBtnHiddenSave = false;
            }else{
                return;
            }

        };

        //判定基本信息是否存在修改，如果有修改，在进入其他界面或者再查询的时候显示提示
        $scope.isChanged = function(){
            if(!$scope.basicWorkBodyConfig.workInfoIsNoEdit){
                __isChanged($scope.basicWorkBodyConfig.copyWorkInfoData,$scope.basicWorkBodyConfig.workInfoDatas);
            }

        };

        //点击取消
        $scope.workInfoCancel = function(){
            if(!$scope.basicWorkBodyConfig.workInfoBtnHiddenCancel){
                //点击取消，不是修改状态
                $scope.bodyConfig.overallWatchChanged = false;

                //所有修改回滚
                $scope.basicWorkBodyConfig.workInfoDatas = $scope.basicWorkBodyConfig.copyWorkInfoData;

                var type = $scope.basicWorkBodyConfig.copyWorkInfoData.executionType;
                $scope.getCurrentExecutionTypeObjectByType(type);
                ////隐藏取消按钮和保存按钮
                $scope.basicWorkBodyConfig.copyWorkInfoData = [];
                $scope.basicWorkBodyConfig.workInfoIsNoEdit = true;
                $scope.basicWorkBodyConfig.workInfoBtnDisEdit = false;
                $scope.basicWorkBodyConfig.workInfoBtnHiddenCancel = true;
                $scope.basicWorkBodyConfig.workInfoBtnHiddenSave = true;
            }else{
                return;
            }
        };

        $scope.getCurrentExecutionTypeObjectByType = function(type){
            var typeList = $scope.basicWorkBodyConfig.executionTypeList;
            for(var i=0;i<typeList.length;i++){
                if(typeList[i].executionType == type){
                    $scope.basicWorkBodyConfig.currentExecutionType = typeList[i];
                    break;
                }
            };
        };

        //保存
        $scope.workInfoSave = function(){
            var params = $scope.basicWorkBodyConfig.workInfoDatas;
            var search = $scope.basicWorkBodyConfig.searchBasicWork;
            var currentType = $scope.basicWorkBodyConfig.currentExecutionType;

            var paramsDatas = {};   //保存需要的参数
            paramsDatas.activity = search;
            paramsDatas.description = params.description;
            paramsDatas.enabled = params.enabled;
            paramsDatas.visible = params.visible;
            paramsDatas.sequenceId = params.sequenceId;
            if(currentType){
                paramsDatas.executionType = currentType.executionType;
            }

            paramsDatas.classOrProgram = params.classOrProgram;
            paramsDatas.firstModifiedDateTime = params.modifiedDateTime;
            paramsDatas.viewActionCode = "U";
            if(__workInfoSave($scope.basicWorkBodyConfig.copyWorkInfoData,paramsDatas)){
                $scope.showBodyModel("正在保存");
                workService.
                    saveChangedWorkInfo(paramsDatas)
                    .then(function (resultDatas){
                        if(!resultDatas.response){
                            $scope.bodyConfig.overallWatchChanged = false;
                            $scope.hideBodyModel();
                            $scope.addAlert('info', '保存成功');
                            $scope.basicWorkBodyConfig.copyWorkInfoData=[];
                            $scope.queryBasicWork();
                            return;
                        }else{
                            $scope.hideBodyModel();
                            $scope.addAlert('danger', '保存失败');
                            $scope.workInfoCancel();
                        }
                    }, function (resultDatas){
                        $scope.hideBodyModel();
                        $scope.workInfoCancel();
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
            }else{
                $scope.addAlert('info', '无数据保存');
            }

        };

        //判定是否有数据修改，没有则提示无修改数据，无需保存
        function __workInfoSave(copy,workInfoDatas){

            var copyWorkInfoData = copy;
            if(copyWorkInfoData.description !=workInfoDatas.description){
                return true;
            }else{
                if(copyWorkInfoData.enabled != workInfoDatas.enabled){
                    return true;
                }else{
                    if(copyWorkInfoData.visible != workInfoDatas.visible){
                        return true;
                    }else{
                        if(copyWorkInfoData.executionType != workInfoDatas.executionType){
                            return true;
                        }else{
                            if(copyWorkInfoData.classOrProgram != workInfoDatas.classOrProgram){
                                return true;
                            };
                        };
                    }
                }
            };
            return false;
        };

        function __isChanged(copy,origin){
            var params = origin;
            var currentType = $scope.basicWorkBodyConfig.currentExecutionType;
            if(currentType){
                params.executionType = currentType.executionType?currentType.executionType:__getExecutionTypeByValue(currentType.value);
            }
            if(__workInfoSave(copy,params)){
                $scope.bodyConfig.overallWatchChanged = true;
            }

        };

        function __getExecutionTypeByValue(value){
            var typeList = $scope.basicWorkBodyConfig.executionTypeList;
            for(var i=0;i<typeList.length;i++){
                if(typeList[i].value == value){
                    $scope.basicWorkBodyConfig.currentExecutionType = typeList[i];
                    return $scope.basicWorkBodyConfig.currentExecutionType.executionType
                }
            };

        };
    }])