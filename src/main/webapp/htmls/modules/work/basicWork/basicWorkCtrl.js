workModule.controller('basicWorkCtrl', [
    '$scope', '$http', '$q','UtilsService','$state',
    'HttpAppService', '$timeout', '$interval','uiGridValidateService',
    'uiGridConstants', 'gridUtil','$uibModal','workService',
    function ($scope, $http, $q, UtilsService ,$state,
              HttpAppService, $timeout, $interval,uiGridValidateService,
              uiGridConstants, gridUtil,$uibModal,workService){

        $scope.basicWorkBodyConfig = {
            basicWorkMenuList:[
                {
                    title:'基础信息',
                    distState:'#/andon/BasicWork/WorkInfo',
                    distStateName:'andon.BasicWork.WorkInfo',
                    isActive:true
                },
                {
                    title:'参数',
                    distState:'#/andon/BasicWork/WorkParam',
                    distStateName:'andon.BasicWork.WorkParam',
                    isActive:true
                },
                {
                    title:'作业组',
                    distState:'#/andon/BasicWork/WorkGroup',
                    distStateName:'andon.BasicWork.WorkGroup',
                    isActive:true
                }],
            isHidden:false,
            searchBasicWork: null,//搜索框数据
            searchBasicWorkBack: null,
            searchBasicWorkBackEdit: null,
            isNoValue : false,//搜索框没数据时，提示错误

            //基本信息
            workInfoDatas:{},//查询出来的数据--基本信息
            currentExecutionType:{},//选中的类型
            executionTypeList:[
                {'executionType':null,'value':'---请选择---'},
                {'executionType':'B','value':'后台作业'},
                {'executionType':'P','value':'网页'},
                {'executionType':'R','value':'原因代码识别方式'},
                {'executionType':'F','value':'原因代码识别防呆验证'},
                {'executionType':'E','value':'公式'}
            ],//所有类型
            workInfoMessage:null,//查询出来的数据，如果为空，则显示无此作业
            copyWorkInfoData:null,//复制一份编辑之前的数据angular.copy();
            workInfoIsNoEdit:true,//是否是编辑状态
            workInfoBtnDisEdit:true,//编辑按钮
            workInfoBtnHiddenCancel : true,//取消按钮
            workInfoBtnHiddenSave : true,//保存按钮

            //作业组
            header:{
                'available':'可用作业组',
                'assigend':'已分配作业组'
            },
            getAssigendActivityGroup:[],//已分配
            getAvailableActivityGroup:[],//可用
            copyAssigendActivityGroup:[],//复制一份编辑之前的数据angular.copy();
            copyAvailableActivityGroup:[],//复制一份编辑之前的数据angular.copy();
            workGroupMessage:null,//查询出来的数据，如果为空，则显示无此作业组
            workGroupIsNoEdit:true,//是否是编辑状态
            workGroupBtnDisEdit : true,//编辑按钮
            workGroupBtnHiddenCancel:true,//取消按钮
            workGroupBtnHiddenSave:true,//保存按钮


            //作业参数
            workParamDatas : [],//查询出来的数据
            copyWorkParamDatas: [],//复制一份编辑之前的数据angular.copy();
            workParamMessage:null,//查询出来的数据，如果为空，则显示无此作业
            workParamBtnDisAdd : true,//新增按钮
            workParamBtnDisDelete : true,//删除按钮
            workParamBtnDisEdit : true,//编辑按钮
            workParamBtnHiddenCancel : true,//取消按钮
            workParamBtnHiddenSave : true,//保存按钮
            editRw:false,//权限

            btnDisabledQuery: true,//查询按钮

            hasQueryedOk: false
        };
        
        $scope.basicWorkBodyConfig.editRw = $scope.modulesRWFlag("#/andon/BasicWork/WorkInfo");

        //搜索输入框必须有值且为大学，查询按钮才能点击
        $scope.basicInfoSearchInputChanged = function(){
            if(UtilsService.isEmptyString($scope.basicWorkBodyConfig.searchBasicWork)){
                $scope.basicWorkBodyConfig.btnDisabledQuery = true;
            }else{
                if(!UtilsService.isEmptyString($scope.basicWorkBodyConfig.searchBasicWork) && !UtilsService.isEmptyString($scope.basicWorkBodyConfig.searchBasicWorkBack) && $scope.basicWorkBodyConfig.searchBasicWork.toUpperCase() == $scope.basicWorkBodyConfig.searchBasicWorkBack.toUpperCase()){
                    $scope.basicWorkBodyConfig.btnDisabledQuery = false;
                }else{  
                    $scope.basicWorkBodyConfig.btnDisabledQuery = true;
                }
            }
            if($scope.basicWorkBodyConfig.hasQueryedOk && !UtilsService.isEmptyString($scope.basicWorkBodyConfig.searchBasicWorkBackEdit)){
                if($scope.basicWorkBodyConfig.searchBasicWorkBackEdit.toUpperCase() != $scope.basicWorkBodyConfig.searchBasicWork.toUpperCase()){
                    $scope.basicWorkBodyConfig.workInfoBtnDisEdit = true;
                    $scope.basicWorkBodyConfig.workParamBtnDisAdd = true;
                    $scope.basicWorkBodyConfig.workParamBtnDisDelete = true;
                    $scope.basicWorkBodyConfig.workParamBtnDisEdit = true;
                    $scope.basicWorkBodyConfig.workGroupBtnDisEdit = true;
                }else{
                    $scope.basicWorkBodyConfig.workInfoBtnDisEdit = false;
                    $scope.basicWorkBodyConfig.workParamBtnDisAdd = false;
                    $scope.basicWorkBodyConfig.workParamBtnDisDelete = false;
                    $scope.basicWorkBodyConfig.workParamBtnDisEdit = false;
                    $scope.basicWorkBodyConfig.workGroupBtnDisEdit = false;
                }
            }
        };

        //模糊查询
        $scope.modalSearchResult = function(){
            if($scope.basicWorkBodyConfig.editRw && !$scope.basicWorkBodyConfig.workInfoBtnHiddenCancel){
                return;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/work/basicWork/workParams/ModalParamSearchResult.html',
                controller: 'modalParamSearchResultCtrl',
                backdrop:'false',
                openedClass:'flex-center-parent',
                resolve: {
                    items: function () {
                        return $scope.basicWorkBodyConfig.searchBasicWork;
                    }
                }
            });
            //将模糊查询选中的记录显示在输入框中
            modalInstance.result.then(function (searchResults) {
                if(UtilsService.isEmptyString(searchResults)){
                    $scope.basicWorkBodyConfig.btnDisabledQuery = true;
                }else{
                    $scope.basicWorkBodyConfig.btnDisabledQuery = false;
                    $scope.basicWorkBodyConfig.searchBasicWork = searchResults;
                    $scope.basicWorkBodyConfig.searchBasicWorkBack = searchResults;
                }
            });
        };

        //查询，如果存在修改为保存，提示，点击确定表示退出，点击取消留在原界面
        $scope.queryBasicWork = function(){
            if($scope.bodyConfig.overallWatchChanged){
                var promise =UtilsService.confirm('还有修改为保存，是否继续', '提示', ['确定', '取消']);
                promise.then(function(){
                    $scope.bodyConfig.overallWatchChanged = false;
                    __queryBasicWork();
                }, function(){
                    //取消停留在原页面
                });

            }else{
                $scope.bodyConfig.overallWatchChanged = false;
                __queryBasicWork();
            };

        };

        //回车查询
        $scope.enterKey = function(e){
            if($scope.basicWorkBodyConfig.searchBasicWork){
                // $scope.basicWorkBodyConfig.btnDisabledQuery = false;
                $scope.basicWorkBodyConfig.searchBasicWork = $scope.basicWorkBodyConfig.searchBasicWork.toUpperCase();
            }
            var keycode = window.event ? e.keyCode : e.which;
            if(keycode==13){
                __queryBasicWork();
            }

        };

        //新增数据
        $scope.addBasicWork = function(){
            __addBasicWork();
        };

        //查询
        function __queryBasicWork(){
            if(!__checkIsNoValue()){//判断搜索输入框是否为空，否则继续
                var search = $scope.basicWorkBodyConfig.searchBasicWork;
                workService
                    .getSearchResultByWork(search)
                    .then(function (resultDatas){
                        if(resultDatas.response){
                            $scope.basicWorkBodyConfig.searchBasicWorkBackEdit = $scope.basicWorkBodyConfig.searchBasicWork;
                            $scope.basicWorkBodyConfig.hasQueryedOk = true;
                            // 处理作业基本信息
                            if(resultDatas.response.basicInfo.length > 0){
                                var results = resultDatas.response.basicInfo[0];
                                $scope.basicWorkBodyConfig.workInfoMessage = "S";
                                $scope.basicWorkBodyConfig.workInfoIsNoEdit = true;
                                $scope.basicWorkBodyConfig.workInfoBtnDisEdit = false;
                                $scope.basicWorkBodyConfig.workInfoBtnHiddenCancel = true;
                                $scope.basicWorkBodyConfig.workInfoBtnHiddenSave = true;
                                $scope.basicWorkBodyConfig.currentExecutionType = {};

                                if(results.visible == "true"){
                                    results.visible = true;
                                }else{
                                    results.visible = false;
                                };
                                if(results.enabled == "true"){
                                    results.enabled = true;
                                }else{
                                    results.enabled = false;
                                };
                                var type = results.executionType;
                                results.searchBasicWork = $scope.basicWorkBodyConfig.searchBasicWork;

                                $scope.getCurrentExecutionTypeByType(type);

                                $scope.basicWorkBodyConfig.copyWorkInfoData = angular.copy(resultDatas.response.basicInfo[0]);
                                $scope.basicWorkBodyConfig.workInfoDatas = resultDatas.response.basicInfo[0];

                            }else{
                                $scope.basicWorkBodyConfig.workInfoMessage = "NA";
                            };
                            // 处理作业参数
                            if(resultDatas.response.params.length >= 0){
                                $scope.basicWorkBodyConfig.workParamMessage = "S";
                                $scope.basicWorkBodyConfig.workParamBtnDisAdd = false;
                                $scope.basicWorkBodyConfig.workParamBtnDisDelete = false;
                                $scope.basicWorkBodyConfig.workParamBtnDisEdit = false;

                                $scope.basicWorkBodyConfig.workParamBtnHiddenCancel = true;
                                $scope.basicWorkBodyConfig.workParamBtnHiddenSave = true;

                                $scope.basicWorkBodyConfig.workParamDatas = resultDatas.response.params;

                                for(var i = 0; i< $scope.basicWorkBodyConfig.workParamDatas.length; i++){
                                    $scope.basicWorkBodyConfig.workParamDatas[i].isEdit = false;
                                    $scope.basicWorkBodyConfig.workParamDatas[i].isSelected = false;
                                }
                                //$scope.basicWorkBodyConfig.copyWorkParamDatas = angular.copy($scope.basicWorkBodyConfig.workParamDatas);
                            }else{
                                $scope.basicWorkBodyConfig.workParamBtnDisAdd = true;
                                $scope.basicWorkBodyConfig.workParamBtnDisDelete = true;
                                $scope.basicWorkBodyConfig.workParamBtnDisEdit = true;

                                $scope.basicWorkBodyConfig.workParamBtnHiddenCancel = true;
                                $scope.basicWorkBodyConfig.workParamBtnHiddenSave = true;
                                $scope.basicWorkBodyConfig.workParamMessage = "NA"
                            };
                        }else{
                            $scope.basicWorkBodyConfig.workInfoMessage = "NA";
                            $scope.basicWorkBodyConfig.workParamDatas = [];
                        }
                    }, function (resultDatas){
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
                //处理已分配作业组
                workService
                    .getAssigendActivityGroup(search)
                    .then(function(resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0;i< resultDatas.response.length;i++){
                                resultDatas.response[i].isHover = false;
                                resultDatas.response[i].isSelected = false;
                            };
                            $scope.basicWorkBodyConfig.workGroupIsNoEdit = true;
                            $scope.basicWorkBodyConfig.workGroupBtnDisEdit = false;
                            $scope.basicWorkBodyConfig.workGroupBtnHiddenCancel = true;
                            $scope.basicWorkBodyConfig.workGroupBtnHiddenSave = true;

                            $scope.basicWorkBodyConfig.getAssigendActivityGroup = resultDatas.response;


                        };
                    },function(resultDatas){
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
                //处理可用作业组
                workService
                    .getAvailableActivityGroup(search)
                    .then(function(resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0;i< resultDatas.response.length;i++){
                                resultDatas.response[i].isHover = false;
                                resultDatas.response[i].isSelected = false;
                            };
                            $scope.basicWorkBodyConfig.workGroupIsNoEdit = true;
                            $scope.basicWorkBodyConfig.workGroupBtnDisEdit = false;
                            $scope.basicWorkBodyConfig.workGroupBtnHiddenCancel = true;
                            $scope.basicWorkBodyConfig.workGroupBtnHiddenSave = true;
                            $scope.basicWorkBodyConfig.getAvailableActivityGroup = resultDatas.response;

                        };
                    },function(resultDatas){
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
            };
        };

        //通过类型id获取类型描述
        $scope.getCurrentExecutionTypeByType = function(type){
            var typeList = $scope.basicWorkBodyConfig.executionTypeList;
            for(var i=0;i<typeList.length;i++){
                if(typeList[i].executionType == type){
                    $scope.basicWorkBodyConfig.currentExecutionType.value = typeList[i].value;
                    break;
                }
            };
        };


        function __checkIsNoValue(){
            var search = $scope.basicWorkBodyConfig.searchBasicWork;
            if(angular.isUndefined(search) || search == null || search == ''){
                $scope.basicWorkBodyConfig.isNoValue = true;
                $scope.basicWorkBodyConfig.workInfoDatas = [];
                $scope.basicWorkBodyConfig.workParamDatas = [];
                $scope.basicWorkBodyConfig.getAssigendActivityGroup = [];
                $scope.basicWorkBodyConfig.getAvailableActivityGroup = [];
                return true;
            }else{
                $scope.basicWorkBodyConfig.isNoValue = false;
                return false;
            }
        };

        //新增数据
        function __addBasicWork(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/work/basicWork/workInfo/ModalInfoSearchResult.html',
                controller: 'modalInfoSearchResultCtrl',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope:$scope,
                resolve:{
                    items:function(){
                        return $scope.basicWorkBodyConfig.executionTypeList;
                    }
                },

            });
            //新增数据保存
            modalInstance.result.then(function (searchResults) {
                $scope.addAlert('info', '保存成功');
                $scope.basicWorkBodyConfig.searchBasicWork = searchResults.activity;
                $scope.basicWorkBodyConfig.copyWorkInfoData=[];
                $scope.queryBasicWork();
            });
        };

        $scope.$on("$stateChangeSuccess", function (event2, toState, toParams, fromState, fromParam){

            for(var i = 0 ; i < $scope.basicWorkBodyConfig.basicWorkMenuList.length ; i++){
                var item = $scope.basicWorkBodyConfig.basicWorkMenuList;
                if(fromState && toState.name == item[i].distStateName){
                    item[i].isActive = true
                }else{
                    item[i].isActive = false;
                }
                if(fromState && toState.name == "andon.BasicWork.WorkInfo"){
                    $scope.basicWorkBodyConfig.isHidden = false;
                }else{
                    $scope.basicWorkBodyConfig.isHidden = true;
                }
            }
        });

    }]);