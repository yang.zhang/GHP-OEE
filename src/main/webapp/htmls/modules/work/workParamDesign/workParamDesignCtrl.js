workModule.controller('workParamDesignCtrl', [
    '$scope', '$http', '$q','$state',
    'HttpAppService', '$timeout', '$interval','workService',
    'uiGridConstants', 'gridUtil', 'UtilsService','$uibModal',
    function ($scope, $http, $q, $state,
              HttpAppService, $timeout, $interval,workService,
              uiGridConstants, gridUtil, UtilsService,$uibModal){
        $scope.workParamDesignConfig = {
            editRw:false,
            searchWorkParamDesign : null,
            isNoValue : false,
            workParamDesignMessage:null,
            workParamDesignDatas : [],
            workParamDesignBtnDisEdit:true,
            workParamDesignBtnHiddenCancel:true,
            workParamDesignBtnHiddenSave:true,
            isEdit:false,
            activityDesc:null,
            copyActivityDesc : null,

            hasActivity: false
        };

        //权限
        $scope.workParamDesignConfig.editRw = $scope.modulesRWFlag("#"+$state.$current.url.source);
        //$scope.modulesRWFlag("#/andon/WorkParamDesign")
        //    .then(function(item){
        //        console.log(item);
        //        $scope.workParamDesignConfig.editRw = item.write;
        //    }, function(){
        //        //console.log();
        //    });

        //表格信息
        $scope.gridWorkParamDesign = {
            showGridFooter: false,
            modifierKeysToMultiSelectCells: false,
            columnDefs:[
                {
                    name:"paramId",displayName:'参数序号',
                    enableCellEdit: false,
                    enableCellEditOnFocus:false,
                    cellClass:'grid-no-editable',
                    enableColumnMenu: false,enableColumnMenus: false,
                    //cellEditableCondition: function($scope){
                    //    var row = arguments[0].row;
                    //    var col = arguments[0].col;
                    //    //新增行可编辑，非新增行不可编辑
                    //    if(!angular.isUndefined(row.entity.modifiedDateTime) && row.entity.modifiedDateTime!=null){
                    //        return false;
                    //    }else{
                    //        return true;
                    //    }
                    //},
                    //editableCellTemplate:'andon-ui-grid-bwp/basicWorkParam-edit-paramId',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }

                },
                {
                    name:"paramDescription",displayName:'参数描述',
                    enableCellEdit: false,
                    enableCellEditOnFocus:false,
                    cellClass:'grid-no-editable',
                    enableColumnMenu: false,enableColumnMenus: false,
                    //cellTemplate: 'andon-ui-grid-bwp/basicWorkParam-edit-temp-description',
                    //cellEditableCondition: function($scope){
                    //    var row = arguments[0].row;
                    //    var col = arguments[0].col;
                    //    //新增行可编辑，非新增行不可编辑
                    //    if(angular.isUndefined(row.entity.modifiedDateTime)
                    //        || row.entity.modifiedDateTime == null
                    //        || row.entity.isEdit){
                    //        return true;
                    //    }else{
                    //        return false;
                    //    }
                    //},
                    //editableCellTemplate:'andon-ui-grid-bwp/basicWorkParam-edit-description',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    name:"paramDefaultValue",displayName:'参数默认值',
                    enableCellEdit:false,
                    enableCellEditOnFocus:false,
                    cellClass:'grid-no-editable text-align-right',
                    enableColumnMenu: false,enableColumnMenus: false,
                    //cellTemplate: 'andon-ui-grid-bwp/basicWorkParam-edit-temp',
                    //cellEditableCondition: function($scope){
                    //    var row = arguments[0].row;
                    //    var col = arguments[0].col;
                    //    //新增行可编辑，非新增行不可编辑
                    //    if(angular.isUndefined(row.entity.modifiedDateTime)
                    //        || row.entity.modifiedDateTime == null
                    //        || row.entity.isEdit){
                    //        return true;
                    //    }else{
                    //        return false;
                    //    }
                    //},
                    //editableCellTemplate:'andon-ui-grid-bwp/basicWorkParam-edit',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    name:"paramValue",displayName:'参数设定值',
                    enableCellEdit:true,cellClass:'text-align-right',
                    enableCellEditOnFocus:true,
                    enableColumnMenu: false,enableColumnMenus: false,
                    cellTemplate: 'andon-ui-grid-wpd/workParamDesign-edit-value',
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row;
                        var col = arguments[0].col;
                        return row.entity.isEdit;
                        //新增行可编辑，非新增行不可编辑
                        //if(angular.isUndefined(row.entity.activityParamValueModifiedDateTime)
                        //    || row.entity.activityParamValueModifiedDateTime == null
                        //    || row.entity.isEdit){
                        //    return true;
                        //}else{
                        //    return false;
                        //}
                    },
                    //editableCellTemplate:'andon-ui-grid-bwp/basicWorkParam-edit',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    name:'activityParamValueModifiedDateTime',visible:false,
                },
                {
                    name:'isEdit',displayName:'是否编辑',visible:false,type:'boolean'
                },
                {
                    name:'isSelected',displayName:'是否选择',visible:false,type: 'boolean'
                }
            ],
            onRegisterApi : __onRegisterApi,
        };

        //回车查询
        $scope.enterKeyWorkParamDesign = function(e){
            if($scope.workParamDesignConfig.searchWorkParamDesign){
                $scope.workParamDesignConfig.searchWorkParamDesign = $scope.workParamDesignConfig.searchWorkParamDesign.toUpperCase();
            }


            var keycode = window.event ? e.keyCode : e.which;
            if(keycode==13){
                __queryWorkParamDesign();
            }

        };

        //模糊查询
        $scope.modalSearchResultWorkParamDesign = function(){
            __modalSearchResultWorkParamDesign();
        };

        //查询
        $scope.queryWorkParamDesign = function(){
            if($scope.bodyConfig.overallWatchChanged){
                var promise = UtilsService.confirm('重新查询会刷新现有数据', '提示', ['确定', '取消']);
                promise
                    .then(function(){ // ok
                        $scope.bodyConfig.overallWatchChanged = false;
                        __queryWorkParamDesign();
                    }, function(){   // cancel

                    });
            }else{
                __queryWorkParamDesign();
            }

        };

        //编辑
        $scope.workParamDesignEdit = function(){
            __workParamDesignEdit();
        };

        //取消
        $scope.workParamDesignCancel = function(){
            __workParamDesignCancel();
        };

        //保存
        $scope.workParamDesignSave = function(){
            __workParamDesignSave();
        };

        //查询
        function __queryWorkParamDesign(){
            if(!__checkIsNoValue()){
                $scope.gridWorkParamDesign.data = [];
                var search = $scope.workParamDesignConfig.searchWorkParamDesign;
                workService
                    .getSearchResultByWorkParamDesign(search)
                    .then(function (resultDatas){
                        var hasActivity = resultDatas.response.hasActivity;
                        var resultArray = resultDatas.response.list;
                        $scope.workParamDesignConfig.hasActivity = hasActivity;

                        if(resultArray && resultArray.length > 0){
                            $scope.workParamDesignConfig.workParamDesignMessage = "S";
                            $scope.workParamDesignConfig.isEdit = false;
                            $scope.workParamDesignConfig.workParamDesignBtnDisEdit = false;

                            $scope.workParamDesignConfig.workParamDesignBtnHiddenCancel = true;
                            $scope.workParamDesignConfig.workParamDesignBtnHiddenSave = true;

                            for(var i = 0; i< resultArray.length; i++){
                                resultArray[i].isEdit = false;
                                resultArray[i].isSelected = false;
                            }
                            $scope.workParamDesignConfig.activityDesc = resultArray[0].activityDesc;
                            $scope.workParamDesignConfig.copyActivityDesc = angular.copy(resultArray[0].activityDesc);

                            $scope.gridWorkParamDesign.data = resultArray;
                        }else{
                            $scope.workParamDesignConfig.workParamDesignMessage = "NA";
                            $scope.workParamDesignConfig.workParamDesignDatas = [];
                            $scope.gridWorkParamDesign.data = [];
                        }
                    }, function (resultDatas){
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
            };
        };

        //搜索框不能为空
        function __checkIsNoValue(){
            var search = $scope.workParamDesignConfig.searchWorkParamDesign;
            if(angular.isUndefined(search) || search == null || search == ''){
                $scope.workParamDesignConfig.isNoValue = true;
                $scope.workParamDesignConfig.workParamDesignDatas = [];
                return true;
            }else{
                $scope.workParamDesignConfig.isNoValue = false;
                return false;
            }
        };

        //模糊查询
        function __modalSearchResultWorkParamDesign(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/work/workParamDesign/ModalWorkParamDesign.html',
                controller: 'modalWorkParamDesignCtrl',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope:$scope,
                resolve: {
                    items: function () {
                        return $scope.workParamDesignConfig.searchWorkParamDesign;
                    }
                }
            });
            modalInstance.result.then(function (searchResults) {
                $scope.workParamDesignConfig.searchWorkParamDesign = searchResults
            });
        };

        //编辑
        function __workParamDesignEdit(){
            if(!$scope.workParamDesignConfig.workParamDesignBtnDisEdit){
                $scope.bodyConfig.overallWatchChanged = true;

                $scope.workParamDesignConfig.workParamDesignDatas = angular.copy($scope.gridWorkParamDesign.data);

                $scope.workParamDesignConfig.workParamDesignBtnDisEdit = true;
                $scope.workParamDesignConfig.workParamDesignBtnHiddenCancel = false;
                $scope.workParamDesignConfig.workParamDesignBtnHiddenSave = false;

                for(var i=0;i<$scope.gridWorkParamDesign.data.length;i++){
                    $scope.gridWorkParamDesign.data[i].isEdit = true;
                }

                $scope.workParamDesignConfig.isEdit = true;

                console.log($scope.workParamDesignConfig.workParamDesignDatas);
            }else{
                return;
            };
        };

        //取消
        function __workParamDesignCancel(){
            $scope.bodyConfig.overallWatchChanged = false;

            $scope.workParamDesignConfig.activityDesc = $scope.workParamDesignConfig.copyActivityDesc;

            $scope.gridWorkParamDesign.data = $scope.workParamDesignConfig.workParamDesignDatas;

            $scope.workParamDesignConfig.isEdit = false;
            $scope.workParamDesignConfig.workParamDesignBtnDisEdit = false;

            $scope.workParamDesignConfig.workParamDesignBtnHiddenCancel = true;
            $scope.workParamDesignConfig.workParamDesignBtnHiddenSave = true;

            for(var i=0;i<$scope.gridWorkParamDesign.data.length;i++){
                $scope.gridWorkParamDesign.data[i].isEdit = false;
            }

            console.log($scope.workParamDesignConfig.workParamDesignDatas);
        };

        //保存
        function __workParamDesignSave(){
            $scope.showBodyModel("正在保存");
            if(!$scope.workParamDesignConfig.workParamDesignBtnHiddenSave){
                var activity = $scope.workParamDesignConfig.searchWorkParamDesign;
                var paramDatas = [];
                var copyParamDesign = $scope.workParamDesignConfig.workParamDesignDatas;
                var paramDesign = $scope.gridWorkParamDesign.data;

                var activityDesc = $scope.workParamDesignConfig.activityDesc;
                var copyActivityDesc = $scope.workParamDesignConfig.copyActivityDesc;

                for(var i=0;i<copyParamDesign.length;i++){
                    if(copyParamDesign[i].paramValue == null){
                        if(paramDesign[i].paramValue != null){
                            paramDesign[i].viewActionCode = "C";
                            paramDesign[i].activityDesc = activityDesc;
                            paramDatas.push(paramDesign[i]);
                        }
                    }else{
                        if(paramDesign[i].paramValue == null){
                            paramDesign[i].viewActionCode = "D";
                            paramDesign[i].activityDesc = activityDesc;
                            paramDatas.push(paramDesign[i])
                        }else if(copyParamDesign[i].paramValue != paramDesign[i].paramValue){
                            paramDesign[i].viewActionCode = "U";
                            paramDesign[i].activityDesc = activityDesc;
                            paramDatas.push(paramDesign[i])
                        }else if(activityDesc!=copyActivityDesc){
                            paramDesign[i].viewActionCode = "U";
                            paramDesign[i].activityDesc = activityDesc;
                            paramDatas.push(paramDesign[i])
                        };
                    };
                };

                workService
                    .saveWorkParamDesign(paramDatas,activity)
                    .then(function (resultDatas){
                        if(!resultDatas.response){
                            $scope.hideBodyModel();
                            $scope.bodyConfig.overallWatchChanged = false;
                            $scope.addAlert('info', '保存成功');
                            $scope.queryWorkParamDesign();
                            return;
                        }else{
                            $scope.hideBodyModel();
                            $scope.addAlert('danger', '保存失败');
                        }
                    }, function (resultDatas){
                        $scope.hideBodyModel();
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
            }
        };

        function __onRegisterApi(gridApi){
            $scope.gridApi = gridApi;

            //编辑相关
            $scope.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                var promise = $q.defer();
                if($scope.gridApi.rowEdit){
                    $scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
                }
                //小写转大写
                if(colDef.field == "paramId" || colDef.name=="paramId"){
                    if(UtilsService.isEmptyString(rowEntity.paramId)){
                        return;
                    };
                    rowEntity.paramId = rowEntity.paramId.toUpperCase();
                };
                promise.resolve();

            });


            //选择当行
            $scope.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                gridRow.entity.isSelected = gridRow.isSelected;

            });

            //全选
            $scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                for(var i = 0; i < gridRows.length; i++){
                    gridRows[i].entity.isSelected = gridRows[i].isSelected;
                }
            });

        };

    }]);