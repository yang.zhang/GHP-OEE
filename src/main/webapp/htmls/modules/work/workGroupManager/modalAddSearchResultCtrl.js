workModule.controller('modalAddSearchResultCtrl', [
    '$scope', '$http', '$q','UtilsService',
    'HttpAppService', '$timeout', '$interval','uiGridValidateService',
    'uiGridConstants', 'gridUtil','$uibModalInstance','workService',
    function ($scope, $http, $q, UtilsService ,
              HttpAppService, $timeout, $interval,uiGridValidateService,
              uiGridConstants, gridUtil,$uibModalInstance,workService){


        $scope.config = {
            workIsNecessary:false,//必填字段
            addDatas:{}//新增数据
        };

        //保存作业组
        $scope.workGroupManagerSave = function(){
            __save();
        };

        $scope.modalWorkGroupManagerBlur = function(){
            __modalBlur();
        };

        $scope.modalWorkGroupManagerKeyUp = function(){
            __modalWorkGroupManagerKeyUp();
        };

        function __save(){
            if(!__checkWorkIsNull()){//必填字段未填，直接返回
                $scope.config.workIsNecessary = true;
                return;
            }else{
                $scope.showBodyModel("正在保存");
                var workGroupDatas = $scope.config.addDatas;
                workGroupDatas.viewActionCode = 'C';
                workService.
                    saveChangedWorkGroup(workGroupDatas)
                    .then(function (resultDatas){
                        if(!resultDatas.response){
                            $scope.hideBodyModel();
                            $uibModalInstance.close($scope.config.addDatas);
                            return;
                        }else{
                            $scope.hideBodyModel();
                            $scope.addAlert('danger', '保存失败');
                        }
                    }, function (resultDatas){
                        $scope.hideBodyModel();
                        //$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                        $scope.addAlert('danger', resultDatas.myHttpConfig.errorMessage);

                    });

            };
        };

        //检查activityGroup是否为空
        function __checkWorkIsNull(){
            var work = $scope.config.addDatas.activityGroup;
            if(angular.isUndefined(work) || work == null || work == ''){
                return false;
            }else{
                return true;
            };
        };

        //移除鼠标触发事件，activityGroup必填且大写
        function __modalBlur(){
            var work = $scope.config.addDatas.activityGroup;
            if(work){
                $scope.config.addDatas.activityGroup = work.toUpperCase();
            }
            if($scope.config.workIsNecessary){
                if(__checkWorkIsNull){
                    $scope.config.workIsNecessary = false;
                }
            };
        };

        //键盘松开触发事件，activityGroup必填且大写
        function __modalWorkGroupManagerKeyUp(){
            var work = $scope.config.addDatas.activityGroup;
            if(work){
                $scope.config.addDatas.activityGroup = work.toUpperCase();
            }
        };

        //关闭模态框
        $scope.workGroupManagerCancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }])