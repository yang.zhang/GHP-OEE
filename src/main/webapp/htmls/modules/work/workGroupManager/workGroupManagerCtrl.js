workModule.controller('workGroupManagerCtrl', [
    '$scope', '$http', '$q','$state',
    'HttpAppService', '$timeout', '$interval','workService',
    'uiGridConstants', 'gridUtil', 'UtilsService','$uibModal',
    function ($scope, $http, $q, $state,
              HttpAppService, $timeout, $interval,workService,
              uiGridConstants, gridUtil, UtilsService,$uibModal){

        $scope.config = {
            editRw:false,//权限
            header:{
                'available':'可用作业',
                'assigend':'可分配作业'
            },
            isNoValue : false,
            isChanged:false,
            searchWorkGroup:null,//作业组输入框
            searchWorkGroupBack: null,
            searchWorkGroupBackEdit: null,  // 用来控制 下方编辑按钮，不用searchWorkGroupBack是因为考虑到未点击过查询按钮的情况
            description:'',

            copyWorkGroupData:null,//备份作业组描述数据
            workGroupDatas:null,//作业组描述数据

            workGroupMessage:null,//如果查询出来没数据，显示无此作业组

            getAssigendActivityGroup:[],//已分配
            getAvailableActivityGroup:[],//可用

            copyAssigendActivityGroup:[],//备份已分配
            copyAvailableActivityGroup:[],//备份可用

            workGroupIsNoEdit:true,
            workGroupBtnDisEdit:true,//编辑按钮
            workGroupBtnHiddenSave:true,//保存按钮
            workGroupBtnHiddenCancel:true,//取消按钮

            btnDisabledQuery: true,//查询按钮

            hasQueryedOk: false
        };

        $scope.config.editRw = $scope.modulesRWFlag("#"+$state.$current.url.source);

        //作业组搜索框有值才能点击查询按钮
        $scope.searchWorkGroupInputChanged = function(){
            if(UtilsService.isEmptyString($scope.config.searchWorkGroup)){
                $scope.config.btnDisabledQuery = true;
            }else{
                if(!UtilsService.isEmptyString($scope.config.searchWorkGroup) && !UtilsService.isEmptyString($scope.config.searchWorkGroupBack) && $scope.config.searchWorkGroup.toUpperCase() == $scope.config.searchWorkGroupBack.toUpperCase()){
                    $scope.config.btnDisabledQuery = false;
                }else{
                    $scope.config.btnDisabledQuery = true;
                }
            }
            if(!UtilsService.isEmptyString($scope.config.searchWorkGroupBackEdit)){
                if($scope.config.searchWorkGroupBackEdit.toUpperCase() != $scope.config.searchWorkGroup.toUpperCase()){
                    $scope.config.workGroupBtnDisEdit = true;
                }else{
                    $scope.config.workGroupBtnDisEdit = false;
                }
            }
        };

        //模糊查询
        $scope.modalSearchResult = function(){
            if($scope.config.editRw && !$scope.config.workGroupBtnHiddenCancel){
                return;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/work/workGroupManager/ModalWorkGroupSearchResult.html',
                controller: 'modalWorkGroupSearchResultCtrl',
                backdrop:'false',
                openedClass:'flex-center-parent',
                resolve: {
                    items: function(){
                        return $scope.config.searchWorkGroup;
                    }
                }
            });
            // 将模糊查询出来的数据显示在搜索输入框中
            modalInstance.result.then(function (searchResults) {
                if(UtilsService.isEmptyString(searchResults)){
                    $scope.config.btnDisabledQuery = true;
                }else{
                    $scope.config.btnDisabledQuery = false;
                    $scope.config.searchWorkGroup = searchResults;
                    $scope.config.searchWorkGroupBack = searchResults;
                }
            });
        };

        $scope.queryWorkGroup = function(){
            __queryWorkGroup();
        };

        //点击编辑，显示取消和保存按钮，备份数据
        $scope.workGroupEdit = function(){
            if(!$scope.config.workGroupBtnDisEdit){
                //$scope.bodyConfig.overallWatchChanged = true;

                var copyAssigendActivityGroup = $scope.config.copyAssigendActivityGroup;
                var copyAvailableActivityGroup = $scope.config.copyAvailableActivityGroup;
                var copyWorkGroupData = $scope.config.copyWorkGroupData;
                if(angular.isUndefined(copyAssigendActivityGroup) || copyAssigendActivityGroup == null || copyAssigendActivityGroup.length ==0){
                    $scope.config.copyAssigendActivityGroup = angular.copy($scope.config.getAssigendActivityGroup);
                }
                if(angular.isUndefined(copyAvailableActivityGroup) || copyAvailableActivityGroup == null || copyAvailableActivityGroup.length ==0){
                    $scope.config.copyAvailableActivityGroup = angular.copy($scope.config.getAvailableActivityGroup);
                }
                if(angular.isUndefined(copyWorkGroupData) || copyWorkGroupData == null || copyWorkGroupData.length ==0){
                    $scope.config.copyWorkGroupData = angular.copy($scope.config.workGroupDatas);
                }

                $scope.config.workGroupIsNoEdit = false;
                $scope.config.workGroupBtnDisEdit = true;
                $scope.config.workGroupBtnHiddenCancel = false;
                $scope.config.workGroupBtnHiddenSave = false;
            }else{
                return;
            }

        };

        //点击取消按钮，回滚所有操作，隐藏取消和保存按钮
        $scope.workGroupCancel = function(){
            if(!$scope.config.workGroupBtnHiddenCancel){

                $scope.bodyConfig.overallWatchChanged = false;

                $scope.config.getAvailableActivityGroup = $scope.config.copyAvailableActivityGroup;
                $scope.config.getAssigendActivityGroup = $scope.config.copyAssigendActivityGroup;
                $scope.config.workGroupDatas = $scope.config.copyWorkGroupData;


                $scope.config.copyAvailableActivityGroup = [];
                $scope.config.copyAssigendActivityGroup = [];
                $scope.config.copyWorkGroupData = null;

                $scope.config.workGroupIsNoEdit = true;
                $scope.config.workGroupBtnDisEdit = false;
                $scope.config.workGroupBtnHiddenCancel = true;
                $scope.config.workGroupBtnHiddenSave = true;
            }else{
                return;
            }
        };

        $scope.workGroupSave = function(){
            __workGroupSave();
        };

        $scope.addWorkGroup = function(){
            __addWorkGroup();
        };

        //回车查询
        $scope.enterKeyWorkGroup = function(e){
            if($scope.config.searchWorkGroup){
                $scope.config.searchWorkGroup = $scope.config.searchWorkGroup.toUpperCase();
            }
            var keycode = window.event ? e.keyCode : e.which;
            if(keycode==13){
                __queryWorkGroup();
            }

        };

        $scope.workGroupIsChanged = function(){
            __workGroupIsChanged();
        };

        //搜索框不能为空，为空点击查询报错
        function __checkIsNoValue(){
            var search = $scope.config.searchWorkGroup;
            if(angular.isUndefined(search) || search == null || search == ''){
                $scope.config.isNoValue = true;
                return true;
            }else{
                $scope.config.isNoValue = false;
                return false;
            }
        };

        //查询
        function __queryWorkGroup(){
            if(!__checkIsNoValue()){//
                var search = $scope.config.searchWorkGroup;
                workService
                    .getSearchResultByWorkGroup(search)
                    .then(function (resultDatas){
                        $scope.config.hasQueryedOk = true;
                        $scope.config.searchWorkGroupBackEdit = $scope.config.searchWorkGroup;
                        if(resultDatas.response){
                            $scope.config.workGroupIsNoEdit = true;
                            $scope.config.workGroupBtnDisEdit = false;
                            $scope.config.workGroupBtnHiddenSave = true;
                            $scope.config.workGroupBtnHiddenCancel = true;

                            for(var i = 0; i< resultDatas.response.length; i++){
                                resultDatas.response[i].isEdit = false;
                                resultDatas.response[i].isSelected = false;
                            }
                            $scope.config.workGroupMessage = 'S';
                            $scope.config.workGroupDatas = resultDatas.response;

                            return;
                        }else{
                            $scope.config.workGroupMessage = 'NA';
                            $scope.config.workGroupDatas = null;
                        }
                    }, function (resultDatas){
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
                workService
                    .getAssigendActivityGroupManager(search)
                    .then(function(resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0;i< resultDatas.response.length;i++){
                                resultDatas.response[i].isHover = false;
                                resultDatas.response[i].isSelected = false;
                                resultDatas.response[i].activityGroup = resultDatas.response[i].activity;
                            };
                            $scope.config.workGroupIsNoEdit = true;
                            $scope.config.workGroupBtnDisEdit = false;
                            $scope.config.workParamBtnHiddenCancel = true;
                            $scope.config.workParamBtnHiddenSave = true;

                            $scope.config.getAssigendActivityGroup = resultDatas.response;

                            $scope.config.copyAssigendActivityGroup = angular.copy(resultDatas.response);


                        };
                    },function(resultDatas){
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
                workService
                    .getAvailableActivityGroupManager(search)
                    .then(function(resultDatas){
                        if(resultDatas.response && resultDatas.response.length > 0){
                            for(var i = 0;i< resultDatas.response.length;i++){
                                resultDatas.response[i].isHover = false;
                                resultDatas.response[i].isSelected = false;
                                resultDatas.response[i].activityGroup = resultDatas.response[i].activity
                            };
                            $scope.config.workGroupIsNoEdit = true;
                            $scope.config.workGroupBtnDisEdit = false;
                            $scope.config.workParamBtnHiddenCancel = true;
                            $scope.config.workParamBtnHiddenSave = true;
                            $scope.config.getAvailableActivityGroup = resultDatas.response;
                            $scope.config.copyAvailableActivityGroup = angular.copy(resultDatas.response);

                        };
                        if($scope.config.getAvailableActivityGroup.length == 0
                            && $scope.config.getAssigendActivityGroup.length == 0){
                            $scope.config.workGroupMessage = 'NA';
                        };
                    },function(resultDatas){
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
            };
        };

        //新增
        function __addWorkGroup(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/work/workGroupManager/ModalAddSearchResult.html',
                controller: 'modalAddSearchResultCtrl',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope:$scope
            });
            modalInstance.result.then(function (searchResults) {
                $scope.config.searchWorkGroup = searchResults.activityGroup;
                $scope.queryWorkGroup();
                $scope.addAlert('info','保存成功');
            });
        };

        //保存
        function __workGroupSave(){


            var workGroupDatas = $scope.config.workGroupDatas;
            if(workGroupDatas.handle){
                workGroupDatas.firstModifiedDateTime = workGroupDatas.modifiedDateTime;
                workGroupDatas.viewActionCode = "U";
            }else{
                workGroupDatas.viewActionCode = "C";
            }

            var saveParams  = {
                "avaliableActivityVos":[],
                "assigendActivityVos":[]
            };
            var available = $scope.config.getAvailableActivityGroup;
            var assigend = $scope.config.getAssigendActivityGroup;
            if(available.length > 0){
                for(var i=0;i<available.length;i++){
                    saveParams.avaliableActivityVos.push({
                        "activity":available[i].activityGroup
                    });
                }
            };
            if(assigend.length > 0 ){
                for(var j=0;j<assigend.length;j++){
                    saveParams.assigendActivityVos.push({
                        "activity":assigend[j].activityGroup
                    });
                }
            };
            if($scope.config.searchWorkGroup){
                saveParams.activityGroup = $scope.config.searchWorkGroup;
            }else{
                $scope.addAlert('danger', '作业不可为空');
            };
            console.log(saveParams);
            if(__checkWorkGroupSave(workGroupDatas,saveParams.assigendActivityVos,saveParams.avaliableActivityVos)){
                $scope.showBodyModel("正在保存");
                workService.
                    saveChangedWorkGroup(workGroupDatas)
                    .then(function (resultDatas){
                        console.log(resultDatas.response);
                        if(!resultDatas.response){
                            $scope.hideBodyModel();
                            $scope.bodyConfig.overallWatchChanged = false;
                            $scope.config.copyWorkGroupData = [];
                            $scope.queryWorkGroup();
                            //$scope.addAlert('info', '保存成功');
                            return;
                        }else{
                            $scope.hideBodyModel();
                            $scope.workGroupCancel();
                            $scope.addAlert('danger', '保存失败');
                        }
                    }, function (resultDatas){
                        $scope.hideBodyModel();
                        $scope.workGroupCancel();
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
                if($scope.config.searchWorkGroup){
                    saveParams.activityGroup = $scope.config.searchWorkGroup;
                }else{
                    $scope.addAlert('danger','作业组不可为空');
                };

                workService.
                    saveMoveWorkGroup(saveParams)
                    .then(function (resultDatas){
                        if(!resultDatas.response){
                            $scope.hideBodyModel();
                            $scope.bodyConfig.overallWatchChanged = false;
                            $scope.config.copyAssigendActivityGroup = [];
                            $scope.config.copyAvailableActivityGroup = [];
                            $scope.config.getAvailableActivityGroup=[];
                            $scope.config.getAssigendActivityGroup=[];
                            $scope.queryWorkGroup();
                            $scope.addAlert('info', '保存成功');
                            return;
                        }else{
                            $scope.hideBodyModel();
                            $scope.workGroupCancel();
                            $scope.addAlert('danger', '保存失败');
                        }
                    }, function (resultDatas){
                        $scope.hideBodyModel();
                        $scope.workGroupCancel();
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
            }else{
                $scope.addAlert('info', '没有修改，无需保存');
                $scope.workGroupCancel();
            }

        };

        function __workGroupIsChanged(){
            var workGroupDatas = $scope.config.workGroupDatas;
            var getAssigendActivityGroup = $scope.config.getAssigendActivityGroup;
            var getAvailableActivityGroup = $scope.config.getAvailableActivityGroup;

            if(__checkWorkGroupSave(workGroupDatas,getAssigendActivityGroup,getAvailableActivityGroup)){
                $scope.bodyConfig.overallWatchChanged = true;
            }
        };

        function __checkWorkGroupSave(workGroupDatas,getAssigendActivityGroup,getAvailableActivityGroup){
            var copyWorkGroupData = $scope.config.copyWorkGroupData;
            var copyAssigendActivityGroup = $scope.config.copyAssigendActivityGroup;
            var copyAvailableActivityGroup = $scope.config.copyAvailableActivityGroup;
            if(copyWorkGroupData.description != workGroupDatas.description){
                return true;
            }
            if(copyAssigendActivityGroup.length != getAssigendActivityGroup.length){
                return true;
            }else{
                var assigend = [];
                var copyAssigend = [];
                for(var i=0;i<getAssigendActivityGroup.length;i++){
                    assigend.push(getAssigendActivityGroup[i].activity)
                };
                for(var i=0;i<copyAssigendActivityGroup.length;i++){
                    copyAssigend.push(copyAssigendActivityGroup[i].activity)
                };
                for(var i=0;i<assigend.length;i++){
                    if(copyAssigend.indexOf(assigend[i])<0){
                        return true;
                    }
                }
            };

            if(copyAvailableActivityGroup.length != getAvailableActivityGroup.length){
                return true;
            }else{
                var available = [];
                var copyAvailable = [];
                for(var i=0;i<getAvailableActivityGroup.length;i++){
                    available.push(getAvailableActivityGroup[i].activity)
                };
                for(var i=0;i<copyAvailableActivityGroup.length;i++){
                    copyAvailable.push(copyAvailableActivityGroup[i].activity)
                };
                for(var i=0;i<available.length;i++){
                    if(copyAvailable.indexOf(available[i])<0){
                        return true;
                    }
                }
            };
            return false;

        };

    }]);