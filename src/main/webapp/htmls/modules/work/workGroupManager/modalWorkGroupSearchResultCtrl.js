workModule.controller('modalWorkGroupSearchResultCtrl', [
    '$scope', '$http', '$q','UtilsService',
    'HttpAppService', '$timeout', '$interval','uiGridValidateService',
    'uiGridConstants', 'gridUtil','$uibModalInstance','workService','items',
    function ($scope, $http, $q, UtilsService ,
              HttpAppService, $timeout, $interval,uiGridValidateService,
              uiGridConstants, gridUtil,$uibModalInstance,workService,items){


        $scope.config = {
            workIsNecessary:false,
            addDatas:{},
            selectedModalType:null,
            isFirstSelect:true
        };

        $scope.gridModalWorkGroupSearchResult = {
            columnDefs:[
                {
                    field:"activityGroup",name:"activityGroup",displayName:'作业组',
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field:"description",name:"description",displayName:'作业组描述',
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }

                }
            ],
            onRegisterApi: __onRegisterApi,
            data:[],
        };

        //初始化数据
        $scope.init = function(){
            __requestTableDatas()
        };

        $scope.init();

        function __onRegisterApi(gridApi){
            $scope.gridApi = gridApi;

            //选择任一行或一列时触发
            $scope.gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
                $uibModalInstance.close(newRowCol.row.entity.activityGroup);
            });

            //当选择一行时触发
            $scope.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                $uibModalInstance.close(gridRow.entity.activityGroup);
            });

            //当选时
            //$scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            //
            //    $uibModalInstance.close(gridRows.entity.work);
            //});
        };

        //获取表格数据
        function __requestTableDatas(){
            workService
                .getWorkGroupSearchResultByKey(items)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.gridModalWorkGroupSearchResult.data = resultDatas.response;
                        if($scope.config.isFirstSelect)
                        {
                            $timeout(function(){
                                $scope.config.isFirstSelect=false;
                                /* To hide the blank gap when use selecting and grouping */
                                $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                                $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                            },0);
                        }
                        return;
                    }else{
                        $scope.gridModalWorkGroupSearchResult.data = [];
                    }
                }, function (resultDatas){
                    //$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };

        //关闭模态框
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }])