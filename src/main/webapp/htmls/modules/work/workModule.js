workModule.service('workService',[
    'HttpAppService',
    function(HttpAppService){
        return{
            //通过关键字搜索作业参数
            getSearchResultByKey:getSearchResultByKey,
            //通过作业搜索所需数据
            getSearchResultByWork:getSearchResultByWork,
            //保存作业参数
            saveWorkParams : saveWorkParams,
            //判断作业参数是否引用
            workParamIsUsed:workParamIsUsed,

            //保存新建的作业基本信息
            saveChangedWorkInfo : saveChangedWorkInfo,



            //已分配作业组查询接口
            getAssigendActivityGroup:getAssigendActivityGroup,
            //可用作业组查询接口
            getAvailableActivityGroup:getAvailableActivityGroup,
            //保存作业组
            saveBasicWorkGroup:saveBasicWorkGroup,



            //通过关键字搜索客户端作业
            getClientSearchResultByKey:getClientSearchResultByKey,
            //通过客户端作业查询数据
            getSearchResultByClientWork:getSearchResultByClientWork,
            //保存客户端
            saveClientWork:saveClientWork,


            //通过关键字搜索作业组
            getWorkGroupSearchResultByKey:getWorkGroupSearchResultByKey,
            //通过作业组查询数据
            getSearchResultByWorkGroup:getSearchResultByWorkGroup,
            //得到作业组管理模块的已分配作业组
            getAssigendActivityGroupManager:getAssigendActivityGroupManager,
            //得到作业组管理模块的可用作业组
            getAvailableActivityGroupManager:getAvailableActivityGroupManager,
            //保存作业组增删改
            saveChangedWorkGroup:saveChangedWorkGroup,
            //保存移动作业组
            saveMoveWorkGroup:saveMoveWorkGroup,



            //通过关键字搜索作业参数值设定
            getWorkParamDesignByKey:getWorkParamDesignByKey,
            //通过作业参数值设定查询数据
            getSearchResultByWorkParamDesign:getSearchResultByWorkParamDesign,
            //保存作业参数值设定
            saveWorkParamDesign:saveWorkParamDesign,
        };


        //通过关键字搜索作业参数 GET
        function getSearchResultByKey(params){
            var url = HttpAppService.URLS.GET_SEARCH_RESULT_BY_KEY
                .replace("#{ACTIVITY}#", params);
            return HttpAppService.get({
                url: url
            });
        };

        //通过工作搜索所需作业参数 GET
        function getSearchResultByWork(params){
            var url = HttpAppService.URLS.GET_SEARCH_RESULT_BY_WORK
                .replace("#{ACTIVITY}#", params);
            return HttpAppService.get({
                url: url
            });
        };

        //保存作业参数
        function saveWorkParams(workParams){
            var url = HttpAppService.URLS.SAVE_WORK_PARAMS;
            return HttpAppService.post({
                url: url,
                paras:workParams
            });
        }
        //判断作业参数是否引用
        function workParamIsUsed(paramId){
            var url = HttpAppService.URLS.WORK_PARAM_IS_USED
                .replace('#{PARAMID}#',paramId);
            return HttpAppService.get({
                url: url
            });
        };



        //保存新建的作业基本信息
        function saveChangedWorkInfo(params){
            var url = HttpAppService.URLS.SAVE_CHANGED_WORK_INFO;
            return HttpAppService.post({
                url: url,
                paras:params
            });
        };




        //已分配作业组查询接口
        function getAssigendActivityGroup(params){
            var url = HttpAppService.URLS.GET_ASSIGEND_ACTIVITY_GROUP
                .replace("#{ACTIVITY}#", params);
            return HttpAppService.get({
                url: url
            });
        };
        //可用作业组查询接口
        function getAvailableActivityGroup(params){
            var url = HttpAppService.URLS.GET_AVAILABLE_ACTIVITY_GROUP
                .replace("#{ACTIVITY}#", params);
            return HttpAppService.get({
                url: url
            });
        };

        //保存作业组
        function saveBasicWorkGroup(params){
            var url = HttpAppService.URLS.SAVE_BASIC_WORKGROUP;
            return HttpAppService.post({
                url: url,
                paras:params
            });
        };


        //通过关键字搜索客户端作业 GET
        function getClientSearchResultByKey(params){
            var url = HttpAppService.URLS.GET_CLIENT_SEARCH_RESULT_BY_KEY
                .replace("#{ACTIVITYCLIENT}#", params);
            return HttpAppService.get({
                url: url
            });
        };
        //通过客户端作业查询数据
        function getSearchResultByClientWork(params){
            var url = HttpAppService.URLS.GET_SEARCH_RESULT_BY_CLIENT_WORK
                .replace("#{ACTIVITYCLIENT}#", params);
            return HttpAppService.get({
                url: url
            });
        };
        //保存客户端
        function saveClientWork(params){
            var url = HttpAppService.URLS.SAVE_CLIENT_WORK;
            return HttpAppService.post({
                url: url,
                paras:params,
            });
        };




        //通过关键字搜索作业组
        function getWorkGroupSearchResultByKey(params){
            var url = HttpAppService.URLS.GET_WORK_GROUP_SEARCH_RESULT_BY_KEY
                .replace("#{ACTIVITYGROUP}#", params);
            return HttpAppService.get({
                url: url
            });
        };
        //通过作业组查询数据
        function getSearchResultByWorkGroup(params){
            var url = HttpAppService.URLS.GET_SEARCH_RESULT_BY_WORK_GROUP
                .replace("#{ACTIVITYGROUP}#", params);
            return HttpAppService.get({
                url: url
            });
        };
        //得到作业组管理模块的已分配作业组
        function getAssigendActivityGroupManager(params){
            var url = HttpAppService.URLS.GET_ASSIGEND_ACTIVITY_GROUP_MANAGER
                .replace("#{ACTIVITYGROUP}#", params);
            return HttpAppService.get({
                url: url
            });
        };
        //得到作业组管理模块的可用作业组
        function getAvailableActivityGroupManager(params){
            var url = HttpAppService.URLS.GET_AVAILABLE_ACTIVITY_GROUP_MANAGER
                .replace("#{ACTIVITYGROUP}#", params);
            return HttpAppService.get({
                url: url
            });
        };
        //保存作业组增删改
        function saveChangedWorkGroup(params){
            var url = HttpAppService.URLS.SAVE_CHANGED_WORK_GROUP;
            return HttpAppService.post({
                url: url,
                paras:params
            });
        };
        //保存移动作业组
        function saveMoveWorkGroup(params){
            var url = HttpAppService.URLS.SAVE_MOVE_WORK_GROUP;
            return HttpAppService.post({
                url: url,
                paras:params
            });
        };

        //通过关键字搜索作业参数值设定
        function getWorkParamDesignByKey(params){
            var url = HttpAppService.URLS.GET_WORK_PARAM_DESIGN_BY_KEY
                .replace("#{ACTIVITY}#", params);
            return HttpAppService.get({
                url: url
            });
        };
        //通过作业参数值设定查询数据
        function getSearchResultByWorkParamDesign(params){
            var url = HttpAppService.URLS.GET_SEARCH_RESULT_BY_WORK_PARAM_DESIGN
                .replace("#{SITE}#",HttpAppService.getSite())
                .replace("#{ACTIVITY}#", params);
            return HttpAppService.get({
                url: url
            });
        };
        //保存作业参数值设定
        function saveWorkParamDesign(params,activity){
            var url = HttpAppService.URLS.SAVE_WORK_PARAM_DESIGN
                .replace("#{SITE}#",HttpAppService.getSite())
                .replace("#{ACTIVITY}#", activity);
            return HttpAppService.post({
                url: url,
                paras:params
            });
        };

    }]);


workModule
    .run([
        '$templateCache',
        function ($templateCache){
            //paramId
            $templateCache.put('andon-ui-grid-bwp/basicWorkParam-edit-paramId',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents \" ><input type=\"text\" class=\"\"  ng-model=\"row.entity.paramId\" autofocus /></div>"
            );
            //paramdescription编辑之后
            $templateCache.put('andon-ui-grid-bwp/basicWorkParam-edit-description',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents \" ng-if=\"row.entity.isEdit\"><input type=\"text\" class=\"\"  ng-model=\"row.entity.paramDescription\" autofocus /></div>"
            );
            //paramdescription编辑
            $templateCache.put('andon-ui-grid-bwp/basicWorkParam-edit-temp-description',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" >" +
                "<div ng-if=\"!row.entity.isEdit\">{{row.entity.paramDescription}}</div><div ng-if=\"row.entity.isEdit\"><input type=\"text\" ng-model=\"row.entity.paramDescription\" autofocus/></div>" +
                "</div>"
            );
            //paramdefaultValue编辑之后
            $templateCache.put('andon-ui-grid-bwp/basicWorkParam-edit',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents \" ng-if=\"row.entity.isEdit\"><input type=\"text\" class=\"\"  ng-model=\"row.entity.paramDefaultValue\" autofocus /></div>"
            );
            //paramdefaultValue编辑
            $templateCache.put('andon-ui-grid-bwp/basicWorkParam-edit-temp',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" >" +
                "<div ng-if=\"!row.entity.isEdit\">{{row.entity.paramDefaultValue}}</div><div ng-if=\"row.entity.isEdit\"><input type=\"text\" ng-model=\"row.entity.paramDefaultValue\" autofocus/></div>" +
                "</div>"
            );
            //workParamDesign编辑
            $templateCache.put('andon-ui-grid-wpd/workParamDesign-edit-value',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" >" +
                "<div ng-if=\"!row.entity.isEdit\">{{row.entity.paramValue}}</div><div ng-if=\"row.entity.isEdit\"><input type=\"text\" ng-model=\"row.entity.paramValue\" autofocus/></div>" +
                "</div>"
            );
        }]);