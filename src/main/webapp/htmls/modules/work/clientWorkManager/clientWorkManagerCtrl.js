workModule.controller('clientWorkManagerCtrl', [
    '$scope', '$http', '$q','UtilsService','$uibModal',
    'HttpAppService', '$timeout', '$interval','$state',
    'uiGridConstants', 'gridUtil', 'UtilsService','workService',
    function ($scope, $http, $q, UtilsService,$uibModal,
              HttpAppService, $timeout, $interval,$state,
              uiGridConstants, gridUtil, UtilsService,workService){


        $scope.clientWorkConfig = {
            editRw:false,//权限
            isNoValue:false,//搜索框是否有数据
            searchClientWork:null,//搜索框里的数据
            searchClientWorkBack:null,
            searchClientWorkBackEdit:null,  // 用来控制 下方编辑按钮，不用searchClientWorkBack是因为考虑到未点击过查询按钮的情况
            message:null,//如果没数据显示无此客户端作业
            clientWorkDatas : {},//查询出来的数据
            currentType:{},//已选类型
            typeList:[ //类型List
                {type:null,value:'---请选择---'},
                {type:'B',value:'按钮'},
                {type:'T',value:'TAB页'},


            ],
            copyClientWorkData:null,//编辑之前备份一份数据，以便回滚
            clientWorkIsNoEdit:true,//是否是编辑状态
            clientWorkBtnDisEdit : true,//编辑按钮
            clientWorkBtnHiddenCancel : true,//取消按钮
            clientWorkBtnHiddenSave : true,//保存按钮

            btnDisabledQuery: true//查询按钮
        };

        $scope.init = function(){
            //$scope.clientWorkConfig.currentType = {type:null,value:''}
        };

        $scope.init();

        //赋权限
        $scope.clientWorkConfig.editRw = $scope.modulesRWFlag("#"+$state.$current.url.source);

        //搜索框不能为空且需为大写，查询按钮才能点击
        $scope.clientWorkInputChanged = function(){
            if(UtilsService.isEmptyString($scope.clientWorkConfig.searchClientWork)){
                $scope.clientWorkConfig.btnDisabledQuery = true;
            }else{
                if($scope.clientWorkConfig.searchClientWork == $scope.clientWorkConfig.searchClientWorkBack){
                    $scope.clientWorkConfig.btnDisabledQuery = false;
                }else{  
                    $scope.clientWorkConfig.btnDisabledQuery = true;
                }
            }
            if(!UtilsService.isEmptyString($scope.clientWorkConfig.searchClientWorkBackEdit)){
                if($scope.clientWorkConfig.searchClientWork.toUpperCase() != $scope.clientWorkConfig.searchClientWorkBackEdit.toUpperCase()){
                    $scope.clientWorkConfig.clientWorkBtnDisEdit = true;
                }else{
                    $scope.clientWorkConfig.clientWorkBtnDisEdit = false;
                }
            }
        };

        //模糊查询
        $scope.modalClientSearchResult = function(){
            if($scope.clientWorkConfig.editRw && !$scope.clientWorkConfig.clientWorkBtnHiddenCancel){
                return;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/work/clientWorkManager/ModalClientSearchResult.html',
                controller: 'modalClientSearchResultCtrl',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope:$scope,
                resolve: {
                    items: function () {
                        return $scope.clientWorkConfig.searchClientWork;
                    }
                }
            });
            //将模糊查询出来的数据显示在搜索框中
            modalInstance.result.then(function (searchResults) {
                if(UtilsService.isEmptyString(searchResults)){
                    $scope.clientWorkConfig.btnDisabledQuery = true;
                }else{
                    $scope.clientWorkConfig.btnDisabledQuery = false;
                    $scope.clientWorkConfig.searchClientWork = searchResults;
                    $scope.clientWorkConfig.searchClientWorkBack = searchResults;
                }
            });
        };

        //还有数据未保存时，点击查询，提示
        $scope.queryClientWork = function(){
            if($scope.bodyConfig.overallWatchChanged){
                var promise =UtilsService.confirm('还有修改为保存，是否继续', '提示', ['确定', '取消']);
                promise.then(function(){
                    $scope.bodyConfig.overallWatchChanged = false;
                    __queryClientWork();
                }, function(){
                    //取消停留在原页面
                });

            }else{
                $scope.bodyConfig.overallWatchChanged = false;
                __queryClientWork();
            };

        };

        //还有数据未保存时，点击新增，提示
        $scope.addClientWork = function(){
            if($scope.bodyConfig.overallWatchChanged){
                var promise =UtilsService.confirm('还有修改未保存，是否继续', '提示', ['确定', '取消']);
                promise.then(function(){
                    $scope.bodyConfig.overallWatchChanged = false;
                    $scope.clientWorkCancel();
                    __addClientWork();
                }, function(){
                    //取消停留在原页面
                });

            }else{
                $scope.bodyConfig.overallWatchChanged = false;
                __addClientWork();
            };

        };

        $scope.clientWorkEdit = function(){
            __clientWorkEdit();
        };

        $scope.clientWorkCancel = function(){
            __clientWorkCancel();
        };

        $scope.clientWorkSave = function(){
            __clientWorkSave();
        };

        //回车
        $scope.enterKeyClientWork = function(e){

            if(!UtilsService.isEmptyString($scope.clientWorkConfig.searchClientWork)){
                $scope.clientWorkConfig.searchClientWork = $scope.clientWorkConfig.searchClientWork.toUpperCase();
            }


            var keycode = window.event ? e.keyCode : e.which;
            if(keycode==13){
                __queryClientWork();
            }

        };

        //判定是否有数据修改
        $scope.checkIsChanged = function(){
            __checkIsChanged()
        };

        //检查搜索框是否为空，如果为空，提示错误，如果不为空，继续操作
        function __checkIsNoValue(){
            var search = $scope.clientWorkConfig.searchClientWork;
            if(angular.isUndefined(search) || search == null || search == ''){
                $scope.clientWorkConfig.isNoValue = true;
                return true;
            }else{
                $scope.clientWorkConfig.isNoValue = false;
                return false;
            }
        };

        //查询数据
        function __queryClientWork(){
            if(!__checkIsNoValue()){
                //$scope.bodyConfig.
                var search = $scope.clientWorkConfig.searchClientWork;
                workService
                    .getSearchResultByClientWork(search)
                    .then(function (resultDatas){
                        $scope.clientWorkConfig.searchClientWorkBackEdit = angular.copy($scope.clientWorkConfig.searchClientWork);
                        if(resultDatas.response){
                            $scope.clientWorkConfig.clientWorkIsNoEdit = true;
                            $scope.clientWorkConfig.clientWorkBtnDisEdit = false;
                            $scope.clientWorkConfig.clientWorkBtnHiddenCancel = true;
                            $scope.clientWorkConfig.clientWorkBtnHiddenSave = true;

                            resultDatas.response.isEdit = false;
                            resultDatas.response.isSelected = false;
                            if(resultDatas.response.visible == "true"){
                                resultDatas.response.visible = true;
                            }else{
                                resultDatas.response.visible = false;
                            };
                            __getCurrentTypeByType(resultDatas.response.type);
                            $scope.clientWorkConfig.clientWorkDatas = resultDatas.response;
                            $scope.clientWorkConfig.message = 'S';

                            return;
                        }else{
                            $scope.clientWorkConfig.message = 'NA';
                            $scope.clientWorkConfig.clientWorkDatas = [];
                        }
                    }, function (resultDatas){
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
            };
        };

        //通过类型id得到类型描述
        function __getCurrentTypeByType(type){
            for(var i=0;i<$scope.clientWorkConfig.typeList.length;i++){
                if(type == $scope.clientWorkConfig.typeList[i].type){
                    $scope.clientWorkConfig.currentType.value = $scope.clientWorkConfig.typeList[i].value;
                    return;
                };
            };
        };

        //新增
        function __addClientWork(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/work/clientWorkManager/ModalAddClientSearchResult.html',
                controller: 'modalAddClientSearchResultCtrl',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope:$scope,
                resolve:{
                    items:function(){
                        return $scope.clientWorkConfig.typeList;
                    }
                }
            });
            //新增之后显示出来
            modalInstance.result.then(function (searchResults) {
                $scope.clientWorkConfig.searchClientWork = searchResults.activityClient;
                $scope.queryClientWork();
            });
        };

        //点击编辑，显示取消和保存按钮，备份编辑之前数据，以便回滚
        function __clientWorkEdit(){
            if(!$scope.clientWorkConfig.clientWorkBtnDisEdit){

                var copyClientWorkData = $scope.clientWorkConfig.copyClientWorkData;
                if(angular.isUndefined(copyClientWorkData) || copyClientWorkData == null || copyClientWorkData.length == 0){
                    $scope.clientWorkConfig.copyClientWorkData = angular.copy($scope.clientWorkConfig.clientWorkDatas);
                }

                $scope.clientWorkConfig.clientWorkIsNoEdit = false;
                $scope.clientWorkConfig.clientWorkBtnDisEdit = true;
                $scope.clientWorkConfig.clientWorkBtnHiddenCancel = false;
                $scope.clientWorkConfig.clientWorkBtnHiddenSave = false;
            }else{
                return;
            }
        };

        //点击取消，隐藏取消和保存按钮，回滚所有操作
        function __clientWorkCancel(){
            if(!$scope.clientWorkConfig.clientWorkBtnHiddenCancel){

                $scope.clientWorkConfig.clientWorkBtnDisEdit = false;

                $scope.bodyConfig.overallWatchChanged = false;

                $scope.clientWorkConfig.clientWorkDatas = $scope.clientWorkConfig.copyClientWorkData;

                __getCurrentTypeByType($scope.clientWorkConfig.copyClientWorkData.type);
                $scope.clientWorkConfig.copyClientWorkData = [];
                $scope.clientWorkConfig.clientWorkIsNoEdit = true;
                $scope.clientWorkConfig.clientWorkBtnDisEdit = false;
                $scope.clientWorkConfig.clientWorkBtnHiddenCancel = true;
                $scope.clientWorkConfig.clientWorkBtnHiddenSave = true;
            }else{
                return;
            }
        };

        //保存
        function __clientWorkSave(){

            $scope.bodyConfig.overallWatchChanged = false;

            var clientWorkDatas = $scope.clientWorkConfig.clientWorkDatas;
            var clientParams = {};
            clientParams.activityClient = clientWorkDatas.activityClient;
            clientParams.description = clientWorkDatas.description;
            clientParams.type = clientWorkDatas.type;
            clientParams.visible = clientWorkDatas.visible.toString();
            clientParams.firstModifiedDateTime = clientWorkDatas.modifiedDateTime;
            if(clientWorkDatas.handle){
                clientParams.handle = clientWorkDatas.handle;
                clientParams.viewActionCode = 'U';
            }else{
                clientParams.viewActionCode = 'C';
            };
            if(__workClientSave(clientParams)){
                $scope.showBodyModel("正在保存");
                workService.
                    saveClientWork(clientParams)
                    .then(function (resultDatas){
                        if(!resultDatas.response){
                            $scope.hideBodyModel();
                            $scope.clientWorkConfig.clientWorkBtnDisEdit = false;
                            $scope.clientWorkConfig.copyClientWorkData=[];
                            $scope.queryClientWork();
                            $scope.addAlert('info', '保存成功');
                            return;
                        }else{
                            $scope.hideBodyModel();
                            $scope.addAlert('danger', '保存失败');
                        }
                    }, function (resultDatas){
                        $scope.hideBodyModel();
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });
            }else{
                $scope.addAlert('info', '没有修改，无需保存');
                __clientWorkCancel();
            }


        };

        //如果有修改，跳转链接或重新查询都将提示
        function __checkIsChanged(){
            var workData = $scope.clientWorkConfig.clientWorkDatas;

            var currentType = $scope.clientWorkConfig.currentType;
            workData.type = $scope.clientWorkConfig.currentType?currentType.type:'';
            if(__workClientSave(workData)){
                $scope.bodyConfig.overallWatchChanged = true;
            }

        };

        //是否有修改，有true,否false
        function __workClientSave(workClientData){
            var copyClientWorkData = $scope.clientWorkConfig.copyClientWorkData;
            if(copyClientWorkData.description != workClientData.description){
                return true;
            }else{
                if(copyClientWorkData.visible.toString() != workClientData.visible.toString()){
                    return true;
                }else{
                    if(copyClientWorkData.type != workClientData.type){
                        return true;
                    }
                }
            };
            return false;
        };
    }]);