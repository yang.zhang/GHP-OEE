workModule.controller('modalClientSearchResultCtrl', [
    '$scope', '$http', '$q','UtilsService',
    'HttpAppService', '$timeout', '$interval','uiGridValidateService',
    'uiGridConstants', 'gridUtil','$uibModalInstance','workService','items',
    function ($scope, $http, $q, UtilsService ,
              HttpAppService, $timeout, $interval,uiGridValidateService,
              uiGridConstants, gridUtil,$uibModalInstance,workService,items){

        $scope.config={
            isFirstSelect:true
        };

        $scope.gridClientModalSearchResult = {
            columnDefs:[
                {
                    name:"handle",visible:false
                },
                {
                    name:"activityClient",displayName:'客户端作业',
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    name:"description",displayName:'客户端作业描述',
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }

                },
                {
                    name:"type",displayName:'类型',
                    enableCellEdit: false, enableCellEditOnFocus: false,
                    enableColumnMenu: false, enableColumnMenus: false,
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    name:"visible",visible:false
                },
                {
                    name:"modifiedDateTime",visible:false
                }
            ],
            onRegisterApi: __onRegisterApi
        };

        function __onRegisterApi(gridApi){
            $scope.gridApi = gridApi;

            //选择任一行或一列时触发
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
                $uibModalInstance.close(newRowCol.row.entity.activityClient);
            });

            //当选择一行时触发
            $scope.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){

                $uibModalInstance.close(gridRow.entity.activityClient);
            });

            //当选时
            $scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                //$uibModalInstance.close(gridRows.entity.activityClient);
            });
        };

        //加载表格数据
        $scope.init = function(){
            __requestTableDatas()
        };

        $scope.init();

        //通过搜索条件返回工作参数
        function __requestTableDatas(){
            workService
                .getClientSearchResultByKey(items)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        $scope.gridClientModalSearchResult.data = resultDatas.response;
                        if($scope.config.isFirstSelect)
                        {
                            $timeout(function(){
                                $scope.config.isFirstSelect=false;
                                /* To hide the blank gap when use selecting and grouping */
                                $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                                $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                            },0);
                        }
                        return;
                    }else{
                        $scope.gridClientModalSearchResult.data = [];
                    }
                }, function (resultDatas){
                    //$scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                });
        };

        //关闭模态框
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };


    }])