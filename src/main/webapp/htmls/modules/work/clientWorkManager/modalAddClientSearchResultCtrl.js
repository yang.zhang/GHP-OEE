workModule.controller('modalAddClientSearchResultCtrl', [
    '$scope', '$http', '$q','UtilsService',
    'HttpAppService', '$timeout', '$interval','uiGridValidateService',
    'uiGridConstants', 'gridUtil','$uibModalInstance','workService','items',
    function ($scope, $http, $q, UtilsService ,
              HttpAppService, $timeout, $interval,uiGridValidateService,
              uiGridConstants, gridUtil,$uibModalInstance,workService,items){


        $scope.config = {
            workIsNecessary:false,//新增时必填字段
            addClientDatas:{},//新增的数据
            typeList:items,//所有类型
            currentType:null,//已选类型
        };

        //点击新增
        $scope.saveClientAdd = function(){
            __saveClientAdd();
        };

        //鼠标移除触发事件
        $scope.modalBlur = function(){
            __modalBlur();
        };

        //回车
        $scope.enterKeyModalClientWork = function(){
            __enterKeyModalClientWork();
        };

        //保存新增数据
        function __saveClientAdd(){
            if(!__checkWorkIsNull()){
                $scope.config.workIsNecessary = true;//如果必填字段有未填的，提示错误
                return;
            }else{
                var clientParams = $scope.config.addClientDatas;//新增时需要 传的参数
                clientParams.viewActionCode = 'C';
                if($scope.config.currentType){
                    clientParams.type = $scope.config.currentType.type;
                }
                clientParams.visible = clientParams.visible?clientParams.visible:false;
                workService.
                    saveClientWork(clientParams)
                    .then(function (resultDatas){
                        if(!resultDatas.response){
                            $uibModalInstance.close($scope.config.addClientDatas);
                            //$scope.addAlert('info', '保存成功');
                            return;
                        }else{
                            $scope.addAlert('danger', '保存失败');
                        }
                    }, function (resultDatas){
                        $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                    });

            };
        };

        //判定必填字段activityClient是否已填
        function __checkWorkIsNull(){
            var work = $scope.config.addClientDatas.activityClient;
            if(angular.isUndefined(work) || work == null || work == ''){
                return false;
            }else{
                return true;
            };
        };

        //鼠标移除触发事件，activityClient必须为大写，且必填
        function __modalBlur(){
            var work = $scope.config.addClientDatas.activityClient;
            if(work){
                $scope.config.addClientDatas.activityClient = UtilsService.toUpperCaseWord(work);
            }
            if($scope.config.workIsNecessary){
                if(__checkWorkIsNull){
                    $scope.config.workIsNecessary = false;
                }
            };
        };

        //activityClient必须为大写，且必填
        function __enterKeyModalClientWork(){
            var work = $scope.config.addClientDatas.activityClient;
            if(work){
                $scope.config.addClientDatas.activityClient = UtilsService.toUpperCaseWord(work);
            }

        };

        //关闭模态框
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }])