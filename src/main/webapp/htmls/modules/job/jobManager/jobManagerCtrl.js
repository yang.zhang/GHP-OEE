userModule.controller('jobManagerCtrl', [ 
    '$scope', '$http', '$q', '$state',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants', 'gridUtil', 'jobService',
    'uiGridValidateService', 'UtilsService',
    function ($scope, $http, $q, $state, HttpAppService, $timeout, $interval,
              uiGridConstants, gridUtil, jobService,
              uiGridValidateService, UtilsService){
        
        $scope.config = {
        		title:"job管理",
        		job:[],
        		jobRunTime:[],
        		startDate:"",
        		currentJob:{activity:"",description:""},
        		currentRunTime:{paramDefaultValue:"",paramDescription:""},
        		selectJob:[],
        };
        $scope.init = function(){
        	jobSelect();
        };
        
        $scope.init();
        
        $scope.jobChanged = function(){
            var job = $scope.config.currentJob.handle;
            linkTime(job);
        };
        
        $scope.search = function(){ 	
        	var param={};
        	param.jobName=$scope.config.currentJob.activity;
        	param.jobRunTime=$scope.config.currentRunTime.paramDefaultValue;
        	param.jobRunDay=$scope.config.startDate;
        	
        	/*if(param.jobName==""||param.jobRunTime==""||param.jobRunDay==""){
        		$scope.addAlert("","请输入必填项");
        		return;
        	}*/
        	
        	if(param.jobName==""||param.jobRunDay==""){
        		$scope.addAlert("","请输入必填项");
        		return;
        	}
        	$scope.showBodyModel("正在加载数据,请稍后...");
        	jobService.jobLogList(param).then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                	$scope.jobGrid.data=resultDatas.response;
                	$scope.config.selectJob=[];
                	$scope.hideBodyModel();
                        return;
                    }else{
                    $scope.addAlert("","暂无数据");
                    $scope.jobGrid.data=[];
                    $scope.hideBodyModel();
                }
                },function (resultDatas){
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    $scope.hideBodyModel();
                });
        }
        
        $scope.execute = function(){ 
        	$scope.config.selectJob=[];
        	var data=$scope.jobGrid.data;
        	for(var i=0;i<data.length;i++){
        		if(data[i].isSelected == true){
        			 $scope.config.selectJob.push(data[i]);
        		}
        	}
        	if($scope.config.selectJob.length==0){
        		$scope.addAlert("","选择一条要执行的记录");
        		return;
        	}
        	if($scope.config.selectJob.length>1){
        		$scope.addAlert("","只能选择一条记录");
        		return;
        	}
        	
        	var handle=$scope.config.selectJob[0].handle;
        	jobService.executeJob(handle).then(function (resultDatas){
        		if(resultDatas.myHttpConfig.status == 200){
        			$scope.addAlert("success","原因规则识别作业已启动");
                }
            },function (resultDatas){
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                $scope.hideBodyModel();
            });
        }
        
        function jobSelect(){
        	jobService.jobSelect().then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                    	    $scope.config.job = [];
	                    	for(var i = 0; i < resultDatas.response.length; i++){
	                            resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].activity+"   描述:"+resultDatas.response[i].description;
	                            $scope.config.job.push(resultDatas.response[i]);
	                        }
                            return;
                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    });
        };
        
        
        function linkTime(job){
        	jobService.linkTime(job).then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                    		$scope.config.jobRunTime=[];
                    		for(var i=0;i<resultDatas.response.length;i++){
                    			resultDatas.response[i].descriptionNew = "时间:"+resultDatas.response[i].paramDefaultValue+"   描述:"+resultDatas.response[i].paramDescription;
                    			$scope.config.jobRunTime.push(resultDatas.response[i]);
                    		}
                            return;
                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    });
        };
        
        $scope.jobGrid = {
            useExternalPagination: true,
    		showGridFooter: false, 
    		modifierKeysToMultiSelectCells: false, 	
            data: [],
            onRegisterApi: __onRegisterApi,

            columnDefs: [
                { 
                    field: 'handle', name: 'handle', visible: false
                },
                {   
                    field: 'job', 
                    name: 'job', 
                    displayName: 'job', 
                    minWidth: 150, 
                    validators: { required: true }, 
                    maxLength: 6,
                    cellTooltip: function(row, col){  return row.entity[col.colDef.name];  }, 
                }, 
                {
                    field: 'plannedDateTime', 
                    name: 'plannedDateTime', 
                    displayName: '计划时间', 
                    minWidth: 200,
                    validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'startDateTime', 
                    name: 'startDateTime', 
                    displayName: '开始时间', 
                    minWidth: 200,
                    validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'endDateTime', 
                    name: 'endDateTime', 
                    displayName: '结束时间', 
                    minWidth: 200,
                    validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'serverNode', 
                    name: 'serverNode',
                    displayName: 'serverNode', 
                    minWidth: 200,
                    validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'fetchSize', 
                    name: 'fetchSize',
                    displayName: 'fetchSize', 
                    minWidth: 200,
                    validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
            ]
        };
        function __onRegisterApi(gridApi){
            $scope.config.gridApi = gridApi;
            
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol){
    			if(newRowCol.col.name == "edit" && !angular.isUndefined(window.event) && window.event != null && window.event.type=='click'&&!UtilsService.isNull(newRowCol.row.entity.AUFNR)){
    				edit(newRowCol.row.entity.AUFNR);
    			}
    		});
            $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                gridRow.entity.isSelected = gridRow.isSelected;
                if(gridRow.isSelected==true){
                	$scope.config.selectJob.push(gridRow.entity);
                }
            });
            $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                for(var i = 0; i < gridRows.length; i++){
                    gridRows[i].entity.isSelected = gridRows[i].isSelected;
                }
            });
        }
    }]);