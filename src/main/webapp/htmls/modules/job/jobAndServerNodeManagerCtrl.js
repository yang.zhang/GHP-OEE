reasonCodeModule.controller('jobAndServerNodeManagerCtrl',['$scope', '$http',
    'HttpAppService', '$timeout',
    'uiGridConstants',
    function ($scope, $http, HttpAppService, $timeout,
              uiGridConstants) {
        $scope.config = {
            jobMenuList:[{
                title:'job',
                distState: '#/andon/job/jobManager',
                distStateName:'andon.job.jobManager',
                isActive:true,
                beIcon:'search'
            },{
                title:'serverNode管理',
                distState: '#/andon/job/serverNode',
                distStateName:'andon.job.serverNode',
                isActive:false,
                beIcon:'edit'
            }]
        };  
        
        $scope.reasonCodeConfig = {
            canEdit: false
        };
        
        $scope.reasonCodeConfig.canEdit = $scope.modulesRWFlag("#/andon/ReasonCode/ReasonCodeGroup");

        $scope.$on("$stateChangeSuccess", function (event2, toState, toParams, fromState, fromParam){
            for(var i=0;i<$scope.config.jobMenuList.length;i++){
                var item = $scope.config.jobMenuList
                if(fromState && toState.name == item[i].distStateName){
                    item[i].isActive = true
                }else{
                    item[i].isActive = false;
                }
            }
        });
    }])