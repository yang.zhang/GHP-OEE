userModule.controller('jobManagerCtrl', [ 
    '$scope', '$http', '$q', '$state',
    'HttpAppService', '$timeout', '$interval',
    'uiGridConstants', 'gridUtil', 'jobService',
    'uiGridValidateService', 'UtilsService',
    function ($scope, $http, $q, $state, HttpAppService, $timeout, $interval,
              uiGridConstants, gridUtil, jobService,
              uiGridValidateService, UtilsService){
        
        $scope.config = {
        		title:"job管理",
        		job:[],
        		jobRunTime:[],
        		startDate:"",
        		currentJob:{activity:"",description:""},
        		currentRunTime:{paramDefaultValue:"",paramDescription:""},
        };
        $scope.init = function(){
        	jobSelect();
        };
        
        $scope.init();
        
        $scope.jobChanged = function(){
            var job = $scope.config.currentJob.handle;
            linkTime(job);
        };
        
        $scope.search = function(){ 	
        	var param={};
        	param.jobName=$scope.config.currentJob.activity;
        	param.jobRunTime=$scope.config.currentRunTime.paramDefaultValue;
        	param.jobRunDay=$scope.config.startDate;
        	
        	if(param.jobName==""||param.jobRunTime==""||param.jobRunDay==""){
        		$scope.addAlert("","请输入必填项");
        		return;
        	}
        	$scope.showBodyModel("正在加载数据,请稍后...");
        	jobService.jobLogList(param).then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                	$scope.jobGrid.data=resultDatas.response;
                	$scope.hideBodyModel();
                        return;
                    }else{
                    $scope.addAlert("","暂无数据");
                    $scope.jobGrid.data=[];
                    $scope.hideBodyModel();
                }
                },function (resultDatas){
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    $scope.hideBodyModel();
                });
        }
        
        function jobSelect(){
        	jobService.jobSelect().then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                    	    $scope.config.job = [];
	                    	for(var i = 0; i < resultDatas.response.length; i++){
	                            resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].activity+"   描述:"+resultDatas.response[i].description;
	                            $scope.config.job.push(resultDatas.response[i]);
	                        }
                            return;
                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    });
        };
        
        
        function linkTime(job){
        	jobService.linkTime(job).then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                    		$scope.config.jobRunTime=[];
                    		for(var i=0;i<resultDatas.response.length;i++){
                    			resultDatas.response[i].descriptionNew = "时间:"+resultDatas.response[i].paramDefaultValue+"   描述:"+resultDatas.response[i].paramDescription;
                    			$scope.config.jobRunTime.push(resultDatas.response[i]);
                    		}
                            return;
                        }
                    },function (resultDatas){ //TODO 检验失败
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    });
        };
        
        $scope.jobGrid = {
            showGridFooter: false, 
            modifierKeysToMultiSelectCells: false, 
            enableRowSelection: false,
            enableSelectAll: false,

            data: [],
            onRegisterApi: __onRegisterApi,

            columnDefs: [
                { 
                    field: 'handle', name: 'handle', visible: false
                },
                {   
                    field: 'job', 
                    name: 'job', 
                    displayName: 'job', 
                    minWidth: 150, 
                    validators: { required: true }, 
                    maxLength: 6,
                    cellTooltip: function(row, col){  return row.entity[col.colDef.name];  }, 
                }, 
                {
                    field: 'strt', 
                    name: 'strt', 
                    displayName: '开始时间', 
                    minWidth: 200,
                    validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                }, 
                {
                    field: 'plannedDateTime', 
                    name: 'plannedDateTime', 
                    displayName: '开始时间', 
                    minWidth: 200,
                    validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
                {
                    field: 'serverNode', 
                    name: 'serverNode',
                    displayName: 'serverNode', 
                    minWidth: 200,
                    validators: {  required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }
                },
            ]
        };
        function __onRegisterApi(gridApi){
            $scope.config.gridApi = gridApi;
        }
    }]);