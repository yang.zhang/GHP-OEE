userModule.service('jobService', [
	"HttpAppService",
	function (HttpAppService) {

		return {
			jobSelect: jobSelect,
			linkTime:linkTime,
			jobLogList:jobLogList,
			executeJob:executeJob
		};

		function jobSelect(){
			var url = HttpAppService.URLS.JOB_SELECT
			return HttpAppService.get({
				url: url
			});
		}
		function linkTime(job){
			var url = HttpAppService.URLS.JOB_RUNTIME
			.replace("#{activity_bo}#",job)
			return HttpAppService.get({
				url: url
			});
		}
		function jobLogList(param){
			var url = HttpAppService.URLS.JOB_LOG_LIST
			.replace("#{job_name}#",param.jobName)
			.replace("#{job_run_time}#",param.jobRunTime)
			.replace("#{job_run_day}#",param.jobRunDay)
			return HttpAppService.get({
				url: url
			});
		}
		
		function executeJob(handle){
			var url = HttpAppService.URLS.JOB_EXECUTE
			return HttpAppService.post({
				url: url,
				paras: handle,
			});
		}
		
		
}]);

userModule.run([
	'$templateCache',
	function($templateCache){
		$templateCache.put('andon-ui-grid-tpls/factory-site', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.site\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);
		$templateCache.put('andon-ui-grid-tpls/userUpload-user', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.user\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);
		$templateCache.put('andon-ui-grid-tpls/userUpload-userGroup', 
			"<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.userGroup\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
		);
	}]);