yieldMaintainModule.controller('yieldMaintainCtrl',['$scope', '$http',
    'HttpAppService', '$timeout',
    'uiGridConstants','$q','uiGridValidateService','$uibModal','outputTargetService','yieldMaintainService','UtilsService','$state','Grid',
    function ($scope, $http, HttpAppService, $timeout,
              uiGridConstants,$q,uiGridValidateService,$uibModal,outputTargetService,yieldMaintainService,UtilsService,$state,Grid) {

        $scope.config = {
            resrce : '',
            item : '',
            itemArr : [],
            productDate : UtilsService.serverFommateDateShow(new Date()),
            shift : null,
            shiftCopy : null,
            model : null,
            shifts : [],
            queryBtn : true,
            materileCodeArr : [],
            editRW : true,
            btnDisabledSave : true,
            nowDate:new Date(),
            resourceDesc : null,

            focusInput : false,
            focusInputCode : false,
            resrceDetail : [],
            dataNum : 0
        };
//权限控制
        $scope.config.editRW = $scope.modulesRWFlag("#"+$state.$current.url.source);

        $scope.upModel = function(item){
            if(item == "resrce"){
                $scope.config.shift = null;
                $scope.config.resrce = angular.uppercase($scope.config.resrce);
                $scope.config.resourceDesc = "";
                __requestDropdownShifts(($scope.config.resrce.split(","))[0]);
            }else{
                $scope.config.materileCodeArr = [];
                $scope.config.item = angular.uppercase($scope.config.item);
            }
        };

        //清除表框里的值
        $scope.deleteAll = function(item){
            if(item == 'deviceCode'){
                $scope.config.resrce = "";
                $scope.disableBtn();
                $scope.upModel('resrce');
            }else{
                $scope.config.item = "";
                $scope.disableBtn();
            }
        }
        $scope.gridYieldMaintainTitle = '坏品数维护';
//表格相关
        $scope.gridYieldMaintain = {
            //paginationPageSizes: ROOTCONFIG.AndonConfig.UIConfig.PageSizes, //[5, 10, 15, 20, 50, 100],
            //paginationPageSize: ROOTCONFIG.AndonConfig.UIConfig.defaultPageSize, //10,
            //useExternalPagination: true,
            showGridFooter: false,
            modifierKeysToMultiSelectCells: false,
            columnDefs:[
                {
                    field: 'handle', name: 'handle', visible: false
                },
                {
                    field: 'isSelected', name: 'isSelected', displayName: '选择', minWidth: 60, maxWidth: 60,
                    enableCellEdit: true, type: 'boolean', visible: false,enablePinning:false,
                    enableColumnMenu: false, enableColumnMenus: false, enableSorting: false
                },
                {
                    field: 'resrce', name: 'resrce', displayName: '设备编码', minWidth: 100,pinnedLeft:true,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'resrceDescription', name: 'resrceDescription', displayName: '设备名称', minWidth: 150,pinnedLeft:true,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'item', name: 'item', displayName: '物料编码', minWidth: 150,pinnedLeft:true,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'totalOutput', name: 'totalOutput', displayName: '总产出', minWidth: 100,
                    enableCellEdit: true,cellClass:'text-align-right grid-no-editable',
                    enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },type: 'number',
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'badQuity', name: 'badQuity', displayName: '人工录入坏品数', minWidth: 150,
                    enableCellEdit: $scope.config.editRW, cellClass:'text-align-right',
                    enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; }, type: 'number',
                    cellTemplate: 'andon-ui-grid-tpls/maintenance-oneGoodNum',
                    cellEditableCondition: function($scope){
                        var row = arguments[0].row,
                            col = arguments[0].col;
                        var nowDate = new Date().getTime();
                        var nowDateShow = UtilsService.serverFommateDateShow(nowDate);
                        var productionDate = row.entity.productionDate;
                        var productionDateLast = UtilsService.serverFommateDateShow(new Date(new Date(nowDate).getTime() - 24*60*60*1000));
                        if(nowDateShow != productionDate && productionDate != productionDateLast){
                            // console.log('11');
                            return false;
                        }
                        if(row.entity.createdDateDec == undefined || row.entity.createdDateDec == null){
                            if(row.entity.totalOutput == 0 || row.entity.totalOutput == '' || row.entity.totalOutput == null){
                                // console.log('1122');
                                return false;
                            }else{
                                return true;
                            }
                        }else{
                            var createdDateDec = new Date(UtilsService.serverStringFormatDateTime(row.entity.createdDateDec)).getTime();
                            var createdDateDecLast = nowDate - 12*60*60*1000;
                            // console.log('444');
                            if(createdDateDec > createdDateDecLast && createdDateDec < nowDate){
                                if(row.entity.totalOutput == 0 || row.entity.totalOutput == '' || row.entity.totalOutput == null){
                                    // console.log('11333');
                                    return false;
                                }else{
                                    return true;
                                }
                            }else{
                                // console.log('1133344');
                                return false;
                            }
                        }

                    }
                },
                {
                    field: 'goodQuity', name: 'goodQuity', displayName: '良品数', minWidth: 100,
                    enableCellEdit: true, cellClass:'text-align-right grid-no-editable',
                    enableCellEditOnFocus: false, validators: { required: true },
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },type: 'number',
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'model', name: 'model', displayName: 'model', minWidth: 100,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'productionDate', name: 'productionDate', displayName: '生产日期', minWidth: 100,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
                {
                    field: 'shiftDescription', name: 'shiftDescription', displayName: '班次', minWidth: 135,
                    enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },cellClass:'grid-no-editable',
                    cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                    cellEditableCondition: function($scope){
                        return false;
                    }
                },
            ],
            data: [],
            onRegisterApi: __onRegisterApi
        };


        function __onRegisterApi(gridApi){
            $scope.config.gridApi = gridApi;
            gridApi.cellNav.on.navigate($scope,function(newRowCol, oldRowCol){

            });
            $scope.config.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                $scope.bodyConfig.overallWatchChanged = true;
                var promise = $q.defer();
                if($scope.config.gridApi.rowEdit){
                    $scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
                }
                rowEntity.hasChanged = true;
                if(colDef.field === "badQuity" || colDef.name === "badQuity"){
                    //if(rowEntity.firstQtyYield){
                    if(isNaN(parseInt(rowEntity.badQuity)) || parseInt(rowEntity.badQuity)<0){
                        rowEntity.badQuity = 0;
                        rowEntity.goodQuity = ($scope.stringForNum(rowEntity.totalOutput) - $scope.stringForNum(rowEntity.badQuity))/1000;
                        $scope.addAlert("","坏品数必须大于等于0且小于等于总产出");
                        // rowEntity.plcObjectIsValid = false;
                        // rowEntity.dataTypeIsValid = false;
                        // uiGridValidateService.setInvalid(rowEntity, colDef);
                        return;
                    }
                    if(rowEntity.badQuity > rowEntity.totalOutput){
                        $scope.addAlert("","坏品数必须大于等于0且小于等于总产出");
                        rowEntity.plcObjectIsValid = false;
                        rowEntity.dataTypeIsValid = false;
                        uiGridValidateService.setInvalid(rowEntity, colDef);
                        return;
                    }
                    if(rowEntity.badQuity.toString().indexOf('.')>0){
                        rowEntity.badQuity = rowEntity.badQuity.toFixed(2);
                    }
                    rowEntity.goodQuity = ($scope.stringForNum(rowEntity.totalOutput) - $scope.stringForNum(rowEntity.badQuity))/1000;
                    promise.resolve();
                    rowEntity.hasChanged = true;
                    rowEntity.plcObjectIsValid = true;
                    rowEntity.dataTypeIsValid = true;
                    uiGridValidateService.setValid(rowEntity, colDef);
                }
            });
            $scope.config.gridApi.core.on.sortChanged( $scope, function( grid, sort ) {
                $scope.config.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
            });
            //选择相关
            $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
                gridRow.entity.isSelected = gridRow.isSelected;
            });
            $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
                for(var i = 0; i < gridRows.length; i++){
                    gridRows[i].entity.isSelected = gridRows[i].isSelected;
                }
            });
        };
//字符串到数字
        $scope.stringForNum = function(item){
            if(item == null || item == undefined || item == ''){
                return 0;
            }else{
                return parseFloat(item)*1000;
            }
        };
        //班次
        function __requestDropdownShifts(resrce){
            $scope.config.shifts = [];
            yieldMaintainService
                .yieldMaintainqueryShift(resrce)
                .then(function (resultDatas){
                    if(resultDatas.response && resultDatas.response.length > 0){
                        for(var i = 0; i < resultDatas.response.length; i++){
                            resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].shift+"   描述:"+resultDatas.response[i].description;
                            $scope.config.shifts.push(resultDatas.response[i]);
                        }
                        // $scope.config.deviceNums = $scope.config.deviceNums.concat(resultDatas.response);
                        return;
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        };
        //设备编码
        $scope.modalQueryDeviceCode = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/yieldMaintain/model/ModelQueryDeviceCode.html',
                controller: 'ModelQueryDeviceCodeCtr',
                size: 'lg',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope : $scope,
            });
            modalInstance.result.then(function (selectedItem) {
                var resrce = [];
                var resourceDesc = [];
                for(var i=0;i<selectedItem.length;i++){
                    resrce.push(selectedItem[i].deviceCode.resrce);
                    resourceDesc.push(selectedItem[i].deviceCode.description);
                }
                $scope.config.resrce = resrce.join(",");
                $scope.config.resourceDesc = resourceDesc.join(",");
                __requestDropdownShifts(selectedItem[0].deviceCode.resrce);
                $scope.disableBtn();
                $scope.upModel();
                $scope.blurInput('resrce');
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });

        };
//物料编码
        $scope.modalQueryMaterialsCode = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/yieldMaintain/model/ModalQueryMaterialsCode.html',
                controller: 'ModelQueryMaterialsCodeCtr',
                size: 'lg',
                backdrop:'false',
                openedClass:'flex-center-parent',
                scope : $scope,
                resolve: {
                    modalMaterialsCodeOptions: function () {
                        return {
                            multiSelect: true
                        };
                    }
                }
            });
            modalInstance.result.then(function (selectedMaterials) {
                // console.log(selectedMaterials);
                $scope.config.materileCodeArr = selectedMaterials.Materials;
                var arr = [];
                for(var i=0;i<selectedMaterials.Materials.length;i++){
                    arr.push(selectedMaterials.Materials[i].item);
                }
                $scope.config.model = selectedMaterials.model;
                $scope.config.item = arr.join(",");
                $scope.disableBtn();
                $scope.blurInput('item');
            }, function () {
            });
        };

            //保存
        $scope.saveAllAcitons = function(){
            $scope.showBodyModel("正在保存");
            for(var i=0;i<$scope.gridYieldMaintain.data.length;i++){
                if($scope.gridYieldMaintain.data[i].dataTypeIsValid == false){
                    $scope.addAlert("","数据存在错误,请修改后保存");
                    $scope.hideBodyModel();
                    return;
                }
            }
            // if(!__checkCanSaveDatas()){
            //     $scope.hideBodyModel();
            //     return;
            // };
            var paramActions = [];
            for(var i = 0; i < $scope.gridYieldMaintain.data.length; i++){
                var row = $scope.gridYieldMaintain.data[i];
                if(row.hasChanged){
                    if(!__deletedRowsExitThisRowByHashKey(row.$$hashKey)){
                            paramActions.push({
                                "handle" : row.handle,
                                "resrce": row.resrce,
                                "resrceDescription":row.resrceDescription,
                                "item":row.item,
                                "model":row.model,
                                "productionDate":row.productionDate,
                                "shift":row.shift,
                                "totalOutput":row.totalOutput,
                                "goodQuity":row.goodQuity,
                                "badQuity":row.badQuity,
                                "startDateTime":row.startDateTime,
                                "endDateTime":row.endDateTime,
                                "shiftDescription":row.shiftDescription,
                                "site" : HttpAppService.getSite(),
                                "modifiedDateTime" : row.modifiedDateTime
                            });
                    }
                }
            }
            if(paramActions.length > 0){
                yieldMaintainService
                    .yieldMaintainKeepData(paramActions)
                    .then(function (resultDatas){
                        $scope.hideBodyModel();
                        //$scope.config.btnDisabledSave = true;
                        if(!$scope.$$phase){
                            $scope.$apply();
                        }
                        $scope.bodyConfig.overallWatchChanged = false;
                        $scope.addAlert('success', '保存成功!');
                        $scope.queryDatas();
                    },function (resultDatas){
                        $scope.hideBodyModel();
                        $scope.addAlert('danger', '保存失败!');
                    });
            }else{
                $scope.hideBodyModel();
                $scope.addAlert('没有修改,无需保存!');
            }
        };
        //验证数据表格
        function __checkCanSaveDatas(){
            if(new Date($scope.config.productDate) <=  $scope.config.nowDate){
                for(var i = 0; i < $scope.gridYieldMaintain.data.length; i++){
                    var item = $scope.gridYieldMaintain.data[i];
                    if(item.hasChanged){  // TODO
                        var rules = [
                            { field: 'firstQtyYield', emptyDesc: '第'+(i+1)+'行: 一次良品数不能为空!' }
                        ];
                        for(var ruleIndex in rules){
                            var rule = rules[ruleIndex];
                            if(item[rule.field] == 0){
                                return true;
                            }
                            if(angular.isUndefined(item[rule.field]) || item[rule.field] == null || item[rule.field] == ""){

                                $scope.addAlert('', rule.emptyDesc);
                                return false;
                            }
                        }
                    }
                };
            }else{
                $scope.addAlert('info','不得维护未来时间的产量数据');
                return false;
            };
            return true;
        }
        function __deletedRowsExitThisRowByHashKey(rowHashKey){
            for(var n = 0; n < $scope.config.deletedRows; n++){
                if(rowHashKey == $scope.config.deletedRows[n].$$hashKey){
                    return true;
                }
            }
            return false;
        }
        //按钮控制
        $scope.disableBtn = function(){
            if($scope.config.resrce == null || $scope.config.resrce == "" ||
                $scope.config.productDate == null
             || $scope.config.shift == null){
                $scope.config.queryBtn = true;
            }else{
                $scope.config.queryBtn = false;
            }
        }
//查询
        $scope.queryDatas = function(){
            if($scope.bodyConfig.overallWatchChanged){
                var promise =UtilsService.confirm('还有修改未保存，是否继续', '提示', ['确定', '取消']);
                promise.then(function(){
                    __requestTableDatas();
                }, function(){
                    //取消停留在原页面
                    $scope.bodyConfig.overallWatchChanged = true;
                });
            }else{
                __requestTableDatas();
            };
        };
        function __requestTableDatas(){
            $scope.config.btnDisabledSave = false;
            $scope.gridYieldMaintain.data = [];
            var resrce = $scope.config.resrce ,
                item = $scope.config.item,
                productionDate = UtilsService.serverFommateDate(new Date($scope.config.productDate)) ,
                startTime = $scope.config.shift.startTime,
                shift =$scope.config.shift.shift,
                endTime =$scope.config.shift.endTime,
                shiftDescription =$scope.config.shift.description;
            $scope.config.shiftCopy = $scope.config.shift;
            yieldMaintainService
                .yieldMaintainQueryData(resrce,item,productionDate,startTime,shift,endTime,shiftDescription)
                .then(function (resultDatas){
                    $scope.bodyConfig.overallWatchChanged = false;
                    var itemDetail = $scope.config.materileCodeArr;
                    var dataAll = [];
                    var resrceDetail = $scope.config.resrceDetail;
                        for(var j=0;j<resrceDetail.length;j++){
                            for(var i=0;i<itemDetail.length;i++){
                                dataAll.push(
                                {
                                    "resrce": resrceDetail[j].key,
                                    "resrceDescription":resrceDetail[j].description,
                                    "item":itemDetail[i].key,
                                    "model":itemDetail[i].model,
                                    "productionDate":UtilsService.serverFommateDateShow(new Date($scope.config.productDate)),
                                    "totalOutput":null,
                                    "goodQuity":null,
                                    "badQuity":null,
                                    "shift":$scope.config.shift.shift,
                                    "startDateTime":$scope.config.shift.startTime,
                                    "endDateTime":$scope.config.shift.endTime,
                                    "shiftDescription":$scope.config.shiftCopy.description
                                });
                        }
                    }
                    if(resultDatas.response && resultDatas.response.length > 0){
                        if($scope.config.item.length > 0) {
                            var data = resultDatas.response;
                            console.log(data);
                            for(var i=0;i<data.length;i++){
                                data[i].productionDate = UtilsService.serverFommateDateShow(new Date(data[i].productionDate));
                                console.log(data[i].modifiedDateTime);
                                if(data[i].modifiedDateTime == undefined && data[i].badQuity == 0 && data[i].goodQuity == 0){
                                    data[i].badQuity = '';
                                    data[i].goodQuity =  '';
                                }
                            }
                            for(var i=0;i<data.length;i++){
                                for(var j=0;j<dataAll.length;j++) {
                                    if(data[i].resrce == dataAll[j].resrce
                                    && data[i].item == dataAll[j].item){
                                        dataAll[j] = data[i];
                                    }
                                }
                            }
                            $scope.gridYieldMaintain.data = dataAll;
                        }else{
                            var data = resultDatas.response;
                            console.log(data);
                            for(var i=0;i<data.length;i++){
                                data[i].productionDate = UtilsService.serverFommateDateShow(new Date(data[i].productionDate));
                                console.log(data[i].modifiedDateTime);
                                if(data[i].modifiedDateTime == undefined && data[i].badQuity == 0 && data[i].goodQuity == 0){
                                    data[i].badQuity = '';
                                    data[i].goodQuity =  '';
                                }
                            }
                            $scope.gridYieldMaintain.data = data;
                            // for(var i=0;i<$scope.gridYieldMaintain.data.length;i++){
                            //     $scope.gridYieldMaintain.data[i].productionDate = UtilsService.serverFommateDateShow(new Date($scope.gridYieldMaintain.data[i].productionDate));
                            // }
                        }
                        $scope.config.dataNum = $scope.gridYieldMaintain.data.length;
                    } else{
                        if($scope.config.item.length > 0){
                            $scope.gridYieldMaintain.data = dataAll;
                            $scope.config.dataNum = $scope.gridYieldMaintain.data.length;
                        }else{
                            $scope.addAlert("","当前设备编码在指定时间范围内没有产出!");
                        }
                    }
                },function (resultDatas){ //TODO 检验失败
                    $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                });
        }

        //验证物料编码
        $scope.focusInput = function(item){
            if(item == 'resrce'){
                $scope.config.focusInput = true;
            }else{
                $scope.config.focusInputCode = true;
            }
        }
        $scope.blurInput = function(item){
            if(item == 'resrce'){
                $scope.config.focusInput = false;
                var paramActions = $scope.config.resrce;
                if(paramActions == null || paramActions == ''){
                    return
                }
                $scope.config.resrceDetail = [];
                yieldMaintainService
                    .checkResrce(paramActions)
                    .then(function (resultDatas){
                        var infos = [];
                        var areaInfo = [];
                        for(var i=0;i<resultDatas.response.length;i++){
                            if(resultDatas.response[i].exist == false){
                                infos.push(resultDatas.response[i].key);
                            }
                            areaInfo.push(resultDatas.response[i].value);
                        }
                        areaInfo = __unique1(areaInfo);
                        if(infos.length>0){
                            $scope.addAlert('', infos.join(",")+"不存在");
                            return;
                        }
                        if(areaInfo.length > 1){
                            $scope.addAlert('', '生产区域不同');
                            return;
                        }
                        $scope.config.resrceDetail = resultDatas.response;
                    },function (resultDatas){
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    });
            }else{
                $scope.config.focusInputCode = false;
                var paramActions = $scope.config.item;
                if(paramActions == null || paramActions == ''){
                    return;
                }
                yieldMaintainService
                    .checkItem(paramActions)
                    .then(function (resultDatas){
                        var infos = [];
                        for(var i=0;i<resultDatas.response.length;i++){
                            if(resultDatas.response[i].exist == false){
                                infos.push(resultDatas.response[i].key);
                            }
                        }
                        if(infos.length>0){
                            $scope.addAlert('', infos.join(",")+"不存在");
                            return;
                        }
                        $scope.config.materileCodeArr = resultDatas.response;
                    },function (resultDatas){
                        $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
                    });
            }
        }
        function __unique1(arr){
            var tmpArr = [];
            for(var i=0; i<arr.length; i++){
                //如果当前数组的第i已经保存进了临时数组，那么跳过，
                //否则把当前项push到临时数组里面
                if(tmpArr.indexOf(arr[i]) == -1){
                    tmpArr.push(arr[i]);
                }
            }
            return tmpArr;
        }
    }]);

yieldMaintainModule.controller('ModelQueryDeviceCodeCtr',
    ['$scope','$uibModalInstance','plcService',
        'HttpAppService','yieldMaintainService','$timeout',
        function ($scope, $uibModalInstance,
                  plcService,HttpAppService,yieldMaintainService,$timeout) {
    $scope.config = {
        currentWorkCenter : null,
        currentLine : [],
        currentDeviceType : null,
        deviceCode : null,
        $workCenterSelect : null,
        workCenters : [],//生产区域下拉值
        lines : [], //拉线下拉值
        deviceTypes : [],//设备类型下拉值
        defaultSelectItem1: { descriptionNew:"", description:"", workCenter: "" },
        defaultSelectItem: { descriptionNew:"--- 请选择 ---", description:"--- 请选择 ---", workCenter: null },
        query : true,
        confirmBtn : true
    };
    $scope.gridModalQueryManager = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        //multiSelect:modalMaterialsCodeOptions,

        columnDefs: [
            {
                field: 'handle', name: 'handle', visible: false
            },
            {
                field: 'isSelected', name: 'isSelected', displayName: '选择', minWidth: 60, maxWidth: 60,
                enableCellEdit: true, type: 'boolean', visible: false,
                enableColumnMenu: false, enableColumnMenus: false, enableSorting: false
            },
            {
                field: 'resrce', name: 'resrce', displayName: '设备号', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellEditableCondition: function($scope){
                    return false;
                }
            },
            {
                field: 'description', name: 'description', displayName: '设备名称', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                // cellTemplate: 'andon-ui-grid-tpls/dataCategory-categoryCode',
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellEditableCondition: function($scope){
                    return false;
                }
            },
        ],data : [],
        onRegisterApi: __onRegisterApi
    };
    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            gridRow.entity.isSelected = gridRow.isSelected;
            __selectAny();
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
                __selectAny();
            }
        });
    }
            function __selectAny(){
                for(var i=0;i<$scope.gridModalQueryManager.data.length;i++){
                    if($scope.gridModalQueryManager.data[i].isSelected == true){
                        $scope.config.confirmBtn = false;
                        return;
                    }
                }
                $scope.config.confirmBtn = true;
            };
    $scope.tipSelectClick = function(type){
        if(type == 'line'){
            $scope.config.$workCenterSelect.toggle();
        }
    }
            //初始化
    $scope.init = function(){
        __initDropdowns("init");
        __requestDropdownWorkCenters();
        __requestDropdownDeviceTypes();
    }
    $scope.init();
    function __initDropdowns(type){
        if(type == 'init'){
            $scope.config.workCenters = [];
            //$scope.config.workCenters.push($scope.config.defaultSelectItem1);
            //$scope.config.workCenters.push($scope.config.defaultSelectItem);
            //$scope.config.currentWorkCenter = $scope.config.workCenters[0];
            $scope.config.currentWorkCenter = null;

            $scope.config.deviceTypes = [];
            $scope.config.currentDeviceType = null;
            //$scope.config.deviceTypes.push($scope.config.defaultSelectItem1);
            //$scope.config.deviceTypes.push($scope.config.defaultSelectItem);
            //$scope.config.currentDeviceType = $scope.config.deviceTypes[0];


            $scope.config.lines = [];
            //$scope.config.lines.push($scope.config.defaultSelectItem1);
            //$scope.config.currentLine = $scope.config.lines[0];
            $scope.config.currentLine = [];
        }
        if(type == 'workCenterChanged'){
            $scope.config.lines = [];
            $scope.config.currentLine = []
            //$scope.config.lines.push($scope.config.defaultSelectItem);
        }
    }
            //按钮控制
    function __setBtnsDisable(){
        if(!$scope.config.currentWorkCenter){
            $scope.config.query = true;
            return;
        }
        $scope.config.query = false;
        if(!$scope.$$phase){
            $scope.$apply();
        }
    };
            //区域
    $scope.workCenterChanged = function(){
        //if($scope.config.currentWorkCenter == null || $scope.config.currentWorkCenter.workCenter==null){
        //    __initDropdowns('workCenterChanged');
        //}else{
        //    //__initDropdowns('workCenterChanged');
        //
        //}
        __initDropdowns('workCenterChanged');
        $scope.config.lines = [];
        $scope.config.currentLine = [];
        var workCenter = $scope.config.currentWorkCenter.workCenter;
        __requestDropdownLines(workCenter);
        __setBtnsDisable();
    };
            //拉线
    $scope.lineChanged = function(){
        $scope.config.deviceCode = angular.uppercase($scope.config.deviceCode);
        __setBtnsDisable();
    };
            //设备类型
    $scope.deviceTypeChanged = function(){
        __setBtnsDisable();
    };

    /**
     * 将数组转为String，以逗号分隔
     * */
    function __arrayToString (array){
        var lineString = [];
        for(var i=0;i<array.length;i++){
            lineString[i] = array[i].workCenter;
        }
        return lineString.toString();
    };
            //查询
    $scope.queryDatas = function(){
        $scope.gridModalQueryManager.data = [];

        var workCenter = $scope.config.currentWorkCenter ? $scope.config.currentWorkCenter.workCenter:"";
        var line = __arrayToString($scope.config.currentLine);
        var resourceType = $scope.config.currentDeviceType?$scope.config.currentDeviceType.resourceType:"";
        //
        //var  resrce = null;
        //if(!$scope.config.currentWorkCenter.workCenter && !$scope.config.currentLine.workCenter && !$scope.config.currentDeviceType.workCenter){
        //    resrce = $scope.config.deviceCode;
        //};
        __requestDropdownDeviceCodes(workCenter, line,resourceType);
    };
    function __requestDropdownWorkCenters(){
        plcService
            .dataWorkCenters()
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    for(var i = 0; i < resultDatas.response.length; i++){
                        resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].workCenter+"   描述:"+resultDatas.response[i].description;
                        $scope.config.workCenters.push(resultDatas.response[i]);
                    }
                    // $scope.config.workCenters = $scope.config.workCenters.concat(resultDatas.response);
                    return;
                }else{
                    $scope.gridModalQueryManager.data = [];
                    $scope.addAlert('', '暂无数据!');
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
    };
    function __requestDropdownLines(workCenter){
        plcService
            .dataLines(workCenter)
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    for(var i = 0; i < resultDatas.response.length; i++){
                        resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].workCenter+"   描述:"+resultDatas.response[i].description;
                        $scope.config.lines.push(resultDatas.response[i]);
                    }
                    // $scope.config.lines = $scope.config.lines.concat(resultDatas.response);
                    return;
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
    };
    function __requestDropdownDeviceTypes(){
        plcService
            .dataResourceTypes()
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.config.deviceTypes = [];
                    $scope.config.deviceTypes.push($scope.config.defaultSelectItem);
                    for(var i = 0; i < resultDatas.response.length; i++){
                        resultDatas.response[i].descriptionNew = "代码:"+resultDatas.response[i].resourceType+"   描述:"+resultDatas.response[i].description;
                        $scope.config.deviceTypes.push(resultDatas.response[i]);
                    }
                    // $scope.config.deviceTypes = $scope.config.deviceTypes.concat(resultDatas.response);
                    return;
                }
            },function (resultDatas){ //TODO 检验失败
                if(resultDatas.myHttpConfig.isSessionOut == true || resultDatas.myHttpConfig.isTimeout == true || resultDatas.myHttpConfig.status == 1 || resultDatas.myHttpConfig.status == -1){
                }else{
                    $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
                }
            });
    };
    function __requestDropdownDeviceCodes(workCenter, line,resourceType){
        yieldMaintainService
            .requestDataResourceCodesByLine(workCenter,line,resourceType)
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridModalQueryManager.data = resultDatas.response;
                    //$timeout(function(){
                    //    /* To hide the blank gap when use selecting and grouping */
                    //    $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                    //    $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                    //},0);
                    return;
                }else{
                    $scope.addAlert('', '暂无数据!');
                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert('danger', resultDatas.myHttpConfig.statusDesc);
            });
    };
            //确定
    $scope.ok = function () {
        var rows = $scope.gridModalQueryManager.data;
        $scope.selected = [];
        for(var i = 0; i < rows.length; i++){
            var row = rows[i];
            if(row.isSelected){
                $scope.selected.push({
                        workCenter : $scope.config.currentWorkCenter,
                        line : $scope.config.currentLine,
                        resourceType : $scope.config.currentDeviceType,
                        deviceCode : row
                    });
            }
        }
        if($scope.selected == null){
            $scope.addAlert("","请选择设备编码");
        }else{
            $uibModalInstance.close($scope.selected);
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
            //输入框
    $scope.upModel = function(){
        $scope.config.modelInfo = angular.uppercase($scope.config.modelInfo);
    }
}]);

yieldMaintainModule.controller('ModelQueryMaterialsCodeCtr',
    ['$scope','$uibModalInstance','outputTargetService',
        'modalMaterialsCodeOptions','$timeout',
        function ($scope, $uibModalInstance,outputTargetService,
                  modalMaterialsCodeOptions,$timeout) {
    
    $scope.config = {
        modelInfo : null,
        queryBtn : true,
        flagSelect : modalMaterialsCodeOptions.multiSelect,

        btnDisabledOk: true,
        isFirstSelect:true
    };

    $scope.gridModalMaterial = {
        showGridFooter: false,
        modifierKeysToMultiSelectCells: false,
        multiSelect:$scope.config.flagSelect,
        columnDefs: [
            {
                field: 'handle', name: 'handle', visible: false
            },
            {
                field: 'isSelected', name: 'isSelected', displayName: '选择', minWidth: 60, maxWidth: 60,
                enableCellEdit: true, type: 'boolean', visible: false,
                enableColumnMenu: false, enableColumnMenus: false, enableSorting: false
            },
            {
                field: 'item', name: 'item', displayName: '物料编码', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false,
                validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellEditableCondition: function($scope){
                    return false;
                }
            },
            {
                field: 'description', name: 'description', displayName: '物料描述', minWidth: 100,
                enableCellEdit: true, enableCellEditOnFocus: false, validators: { required: true },
                cellTooltip: function(row, col){ return row.entity[col.colDef.name]; },
                cellEditableCondition: function($scope){
                    return false;
                }
            }],
        data : [],
        onRegisterApi: __onRegisterApi
    };

    function __onRegisterApi(gridApi){
        $scope.config.gridApi = gridApi;
        //选择相关
        $scope.config.gridApi.selection.on.rowSelectionChanged($scope, function(gridRow, event){
            var count = $scope.config.gridApi.selection.getSelectedCount();
            $scope.config.btnDisabledOk = count > 0 ? false : true;
            gridRow.entity.isSelected = gridRow.isSelected;
            if($scope.flagSelect == false){
                $scope.config.gridApi.selection.setModifierKeysToMultiSelect(true);
            }else{
            }
        });
        $scope.config.gridApi.selection.on.rowSelectionChangedBatch($scope, function(gridRows, event){
            var count = $scope.config.gridApi.selection.getSelectedCount();
            $scope.config.btnDisabledOk = count > 0 ? false : true;
            for(var i = 0; i < gridRows.length; i++){
                gridRows[i].entity.isSelected = gridRows[i].isSelected;
                if($scope.flagSelect == false){
                    $scope.config.gridApi.selection.unSelectRow(gridRows[i].entity,event);
                }else{
                }
            }
        });
    }

    //初始化加载model列表
    __requestDropdownMaterialCodes("");

    //确定以及验证
    $scope.ok = function () {
        //$uibModalInstance.close($scope.selected.item);
        var rows = $scope.gridModalMaterial.data;
        $scope.selectedMaterials = [];
        for(var i = 0; i < rows.length; i++){
            var row = rows[i];
            if(row.isSelected){
                $scope.selectedMaterials.push(row);
            }
        }
        if($scope.selectedMaterials.length < 1){
            $scope.addAlert("","请选择物料编码");
        }else{
            $scope.allData = {
                model : $scope.config.modelInfo,
                Materials : $scope.selectedMaterials
            }
            $uibModalInstance.close($scope.allData);
        }
    };
            //model的改变
    $scope.modelChange = function(){
        if($scope.config.modelInfo == '' ||$scope.config.modelInfo == null){
            $scope.config.queryBtn = true;
        }else{
            $scope.config.queryBtn = false;
        }
    }
    $scope.query = function(){
        __requestDropdownMaterialCodes($scope.config.modelInfo);
    }
    //物料
    function __requestDropdownMaterialCodes(model){
        outputTargetService
            .dataMaterialCodes(model)
            .then(function (resultDatas){
                if(resultDatas.response && resultDatas.response.length > 0){
                    $scope.gridModalMaterial.data = resultDatas.response;
                    if($scope.config.isFirstSelect)
                    {
                        $timeout(function(){
                            $scope.config.isFirstSelect=false;
                            /* To hide the blank gap when use selecting and grouping */
                            $('.ui-grid-render-container-left .ui-grid-viewport').height($('.ui-grid-render-container-left .ui-grid-viewport').height() + 17);
                            $('.ui-grid-render-container-body .ui-grid-viewport').addClass("no-horizontal-bar");
                        },0);
                    }
                    return;
                }else{
                    $scope.gridModalMaterial.data = [];
                    $scope.addAlert('', '暂无数据!');

                }
            },function (resultDatas){ //TODO 检验失败
                $scope.addAlert(resultDatas.myHttpConfig.statusDesc);
            });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.upModel = function(e){
        $scope.config.modelInfo = angular.uppercase($scope.config.modelInfo);
        var keycode = window.event ? e.keyCode : e.which;
        if(keycode==13){
            $scope.query();
        }
    }
}]);