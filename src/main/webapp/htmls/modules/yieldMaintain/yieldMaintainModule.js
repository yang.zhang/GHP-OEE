yieldMaintainModule.service('yieldMaintainService', [
    'HttpAppService',
    function (HttpAppService) {

        return{
            yieldMaintainQueryResrce : queryDataResrce,
            yieldMaintainQueryData : queryData,
            yieldMaintainKeepData :keepData,
            yieldMaintainqueryShift :queryShiftData,
            checkItem : checkItem,
            checkResrce : checkResrce,
            requestDataResourceCodesByLine : requestDataResourceCodesByLine
        }
        function queryData(resrce,item,productionDate,startTime,shift,endTime,shiftDescription){
            var url = HttpAppService.URLS.YIELD_MAINTAIN_QUERY_DATA
                    .replace("#{site}#", HttpAppService.getSite)
                    .replace("#{resrce}#", resrce)
                    .replace("#{item}#", item)
                    .replace("#{productionDate}#", productionDate)
                    .replace("#{startTime}#", startTime)
                    .replace("#{shift}#", shift)
                    .replace("#{endTime}#", endTime)
                    .replace("#{shiftDescription}#", shiftDescription)
                ;
            return HttpAppService.get({
                url: url
            });
        };  
        function queryDataResrce(workArea,lineArea,resrce,resourceType){
            var url = HttpAppService.URLS.YIELD_MAINTAIN_QUERY_RESRSCE
                    .replace("#{site}#", HttpAppService.getSite)
                    .replace("#{resrce}#", resrce)
                    .replace("#{lineArea}#", lineArea)
                    .replace("#{workArea}#", workArea)
                    .replace("#{resourceType}#", resourceType)
                ;
            return HttpAppService.get({
                url: url
            });
        };  
        function queryShiftData(resrce){
            var url = HttpAppService.URLS.YIELD_MAINTAIN_SHIFTS
                    .replace("#{site}#", HttpAppService.getSite)
                    .replace("#{resrce}#", resrce)
                ;
            return HttpAppService.get({
                url: url
            });
        };
        function keepData(paramActions){
            var url = HttpAppService.URLS.YIELD_MAINTAIN_KEEP_DATA;
            return HttpAppService.post({
                url: url,
                paras: paramActions
            });
        };
        function checkItem(paramActions){
            var url = HttpAppService.URLS.YIELD_MAINTAIN_ITEM
                .replace("#{site}#", HttpAppService.getSite)
                .replace("#{item}#", paramActions)
            return HttpAppService.get({
                url: url,
            });
        };
        function checkResrce(paramActions){
            var url = HttpAppService.URLS.YIELD_MAINTAIN_RESRCE
                    .replace("#{site}#", HttpAppService.getSite)
                    .replace("#{resrce}#", paramActions);
            return HttpAppService.get({
                url: url,
            });
        };
        function requestDataResourceCodesByLine(workCenter, line, resourceType,plcStatus){
            var url="";
            if(plcStatus)
            {
                url = HttpAppService.URLS.PLC_XLZ_RESOURCE_CODES_BYLINES
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{WORK_CENTER}#", workCenter)
                    .replace("#{LINE}#", line)
                    .replace("#{RESOURCE_TYPE}#", resourceType)
                    .replace("#{plcIp}#",window.localStorage.plcIp)
                ;
            }else{
                url = HttpAppService.URLS.XLZ_RESOURCE_CODES_BYLINES
                    .replace("#{SITE}#", HttpAppService.getSite())
                    .replace("#{WORK_CENTER}#", workCenter)
                    .replace("#{LINE}#", line)
                    .replace("#{RESOURCE_TYPE}#", resourceType)
                ;
            }
            return HttpAppService.get({
                url: url
            });
        }
    }]);
yieldMaintainModule
    .run([
        '$templateCache',
        function ($templateCache){
            $templateCache.put('andon-ui-grid-tpls/maintenance-oneGoodNum',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.badQuity\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
            );  //ng-bind=\"row.entity.firstQtyYield\"
            // $templateCache.put('andon-ui-grid-tpls/maintenance-oneGoodNum-edit',
            //     "<div ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><input type=\"number\" min=0  ng-model=\"row.entity.firstQtyYield\"/></div>"
            // );
            // $templateCache.put('andon-ui-grid-tpls/maintenance-oneGoodNum-edit',
            //     "<div ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"><form name=\"inputForm\"><input type=\"number\" min=0  ng-model=\"row.entity.firstQtyYield\"/></form></div>"
            // );
            $templateCache.put('andon-ui-grid-tpls/maintenance-oneBadNum',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.firstQtyScrap\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
            );
            $templateCache.put('andon-ui-grid-tpls/maintenance-confirmNum',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.firstQtyUnconfirm\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
            );
            $templateCache.put('andon-ui-grid-tpls/maintenance-goodNum',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.goodNum\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
            );
            $templateCache.put('andon-ui-grid-tpls/maintenance-badNum',
                "<div title=\"{{col.cellTooltip(row,col)}}\" class=\"ui-grid-cell-contents\" ng-bind=\"row.entity.badNum\" ng-class=\"{'invalid':(grid.validate.isInvalid(row.entity, col.colDef))}\"></div>"
            );
        }]);