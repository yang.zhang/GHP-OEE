var buildConfig = require('./config/gulp_config.json');
var gulp = require('gulp');

// gulp@3.9.0
//npm install --save-dev gulp-clean gulp-if gulp-sass gulp-concat gulp-copy
//npm install --save-dev gulp-rename gulp-run-sequence gulp-html2js gulp-minify-css gulp-notify gulp-ng-config
//npm install --save-dev gulp-clean gulp-minify-css  gulp-concat

var clean = require('gulp-clean');
var gulpif = require('gulp-if');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
//var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var copy = require('gulp-copy');
var runSequence = require('gulp-run-sequence');
var html2js = require('gulp-html2js');
var minifyCss = require('gulp-minify-css');
//var stripDebug = require('gulp-strip-debug');

var colors = require('colors/safe');
var log = console.log;
var notify = require('gulp-notify');//提示信息
var gulpNgConfig = require('gulp-ng-config');//提示信息


//gulp.task('clean', ['clean-libs', 'cliean-users']);
gulp.task('clean', function(){
	gulp.src(buildConfig.cleanDirs, {read: false})
		.pipe(clean());
	gulp.src(buildConfig.dist.distDir, {read: false})
		.pipe(clean());
});


gulp.task('patch-js', function(){
	gulp.src(buildConfig.htmls.jsFiles)
		//.pipe(stripDebug())
		.pipe(rename(buildConfig.dist.jsFile))
		.pipe(gulp.dest(buildConfig.htmls.srcDir + buildConfig.dist.jsDir))
		//.pipe(gulpif(buildConfig.IS_DEBUG_MODE, uglify()))
		.pipe(rename({extname: '.min.js'}))
		.pipe(gulp.dest(buildConfig.htmls.srcDir + buildConfig.dist.jsDir));
});
gulp.task('sass', function(){
	gulp.src(buildConfig.htmls.scssFiles)
		.pipe(sass())
		.pipe(rename(buildConfig.dist.cssFile))
		.pipe(gulp.dest(buildConfig.htmls.srcDir + buildConfig.dist.cssDir))
		.pipe(minifyCss({
			keepSpecialComments: 0
		}))
		.pipe(rename({extname: '.min.css'}))
		.pipe(gulp.dest(buildConfig.htmls.srcDir + buildConfig.dist.cssDir));
});


gulp.task('copy-html', function(){
	gulp.src(buildConfig.htmls.htmlFiles)
		.pipe(copy(buildConfig.dist.distDir, {
			prefix: 1
		}));
});

gulp.task('copy-minify-js-css', function(){
	gulp.src(buildConfig.htmls.srcDir + buildConfig.dist.jsDir + "*.js")
		.pipe(copy(buildConfig.dist.distDir, {
			prefix: 1
		}));
	gulp.src(buildConfig.htmls.srcDir + buildConfig.dist.cssDir + "*.css")
		.pipe(copy(buildConfig.dist.distDir, {
			prefix: 1
		}));

});

gulp.task('copy-libs', function(){
	return gulp.src(buildConfig.libFiles)
		.pipe(copy(buildConfig.dist.distDir + buildConfig.dist.libDir, {
				prefix: 2
			}));
});
gulp.task('copy-img', function(){
	return gulp.src(buildConfig.htmls.images)
		.pipe(copy(buildConfig.dist.distDir + buildConfig.dist.imgDir, {
				prefix: 2
			}));
});





 

gulp.task('watch-sass', ['sass'], function(done){
	gulp.watch(buildConfig.watchSass, ['sass']);
});

//gulp.task('copy-lib', ['copy-libs', 'copy-img']);

gulp.task("copy-all", function(){ 
	log(colors.red("---------   文件有改动   ---------"));
	runSequence(['sass', 'patch-js'], ['copy-libs', 'copy-img', 'copy-html'], 'copy-minify-js-css', "console-build-ok");
	//console.log("*********** 构建完成 ************");
});

/*gulp.task("copy-users", function(){
	runSequence(['sass', 'patch-js'], ['copy-img', 'copy-html'], 'copy-minify-js-css');
});*/

gulp.task("build-all", ["copy-all"]);
//gulp.task("build", ["copy-users"]);

gulp.task("watch", function(){
	gulp.watch(buildConfig.watchBuild, ["build-all"]);
	log(colors.red("*********** 文件监听已开启 ************"));
});

gulp.task("console-build-ok", function(){
	log(colors.red("*********** 构建完成 ************"));
});




gulp.task('default', ['build-all']);



























