package mhp.oee.kafka;//package mhp.spc.kafka.simple;

//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//import kafka.producer.KeyedMessage;
//
//import mhp.oee.common.logging.InjectableLogger;
//import mhp.oee.web.Constants;
//import org.slf4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//@Controller
//public class Producer {
//
//	@InjectableLogger
//	private static Logger logger;
//
//    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
//
//    @Autowired
//    private kafka.javaapi.producer.Producer<String, String> kafkaProducer;
//
//
//    private String kafkaTopic;
//
//	@RequestMapping(value = "/kafka/simple_producer", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
//    public ResponseEntity<?> simpleProducer(HttpServletResponse res, HttpServletRequest request) {
//
//        String message = "{\"handle\":\"PA:2001,1482457615907001315200168\",\"transactionID\":1482457615907001315200168,\"site\":\"2001\",\"paraContextGBO\":\"SFCBO:2001,12B54256003C\",\"type\":\"MANUAL\",\"startTime\":\"2016-12-23 09:46:55\",\"resourceBO\":\"ResourceBO:2001,FXXX1001\",\"userBO\":\"UserBO:2001,SUNJINBIN\",\"itemBO\":\"ItemBO:2001,FC-N42-S5E897-9APL,A\",\"operationBO\":\"DcGroupBO:2001,TEST_007,A\",\"routerBO\":\"RouterBO:2001,CELL-LINE3-ASSEMBLE-T1,U,A\",\"routerSequence\":1,\"testStatus\":\"PASS\",\"testPassed\":\"true\",\"shopOrder\":\"20151024PW\",\"timesProcessed\":0,\"paramMeasureItems\":[{\"handle\":\"PM:PA:2001,1482457615907001315200168,1482457615918001315200168\",\"measureName\":\"PARA01\",\"measureGroup\":\"TEST_007\",\"parametricBO\":\"PA:2001,1482457615907001315200168,1482457615918001315200168\",\"sequence\":1482457615918001315200168,\"description\":\"PARA01\",\"measureStatus\":\"PASS\",\"dataType\":\"N\",\"highLimit\":\"3\",\"lowLimit\":\"1.1\",\"actual\":\"2\",\"actualNum\":2,\"dcParameterBO\":\"DcParameterBO:DcGroupBO:2001,TEST_007,A,PARA01\",\"testDateTime\":\"2016-12-23 09:47:09\",\"originalActual\":\"2\",\"originalTestDateTime\":\"2016-12-23 09:47:09\",\"edited\":\"false\",\"transactionID\":1482457615907001315200168,\"paraContextGBO\":\"SFCBO:2001,12B54256003C\",\"firstSequence\":1482457629305001315200168,\"dcGroupRevision\":\"A\"},{\"handle\":\"PM:PA:2001,1482457615907001315200168,1482457631760001315200168\",\"measureName\":\"CHANNELID\",\"measureGroup\":\"TEST_007\",\"parametricBO\":\"PA:2001,1482457615907001315200168,1482457631760001315200168\",\"sequence\":1482457631760001315200168,\"description\":\"通道\",\"measureStatus\":\"FAIL\",\"dataType\":\"N\",\"highLimit\":\"12\",\"lowLimit\":\"1\",\"actual\":\"14\",\"actualNum\":14,\"dcParameterBO\":\"DcParameterBO:DcGroupBO:2001,TEST_007,A,CHANNELID\",\"testDateTime\":\"2016-12-23 09:47:14\",\"originalActual\":\"14\",\"originalTestDateTime\":\"2016-12-23 09:47:14\",\"edited\":\"false\",\"transactionID\":1482457615907001315200168,\"paraContextGBO\":\"SFCBO:2001,12B54256003C\",\"firstSequence\":1482457634298001315200168,\"overrideUserBO\":\"UserBO:2001,SUNJINBIN\",\"dcGroupRevision\":\"A\"}]}";
//
//        kafkaProducer.send(new KeyedMessage<String, String>(kafkaTopic, message));
//
//        return ResponseEntity.ok().body("Message sent success !!!");
//    }
//
//}
