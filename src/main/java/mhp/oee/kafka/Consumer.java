package mhp.oee.kafka;

import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.message.MessageAndMetadata;
import kafka.serializer.StringDecoder;
import kafka.utils.VerifiableProperties;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import org.slf4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class Consumer implements InitializingBean {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private AllInterfaceKafka allInterfaceKafka;

    @Autowired
    private ConsumerConnector kafkaConsumer;

    @Value("${kafka.consumer.switch}")
    private String isEnableRegister = "false";


    private Map<String, Integer> kafkaTopicMap;
    private ThreadPoolService kafkaConsumerThreadPool;

    public Consumer() {
        // TODO: put topics and partition.size
        kafkaTopicMap = new HashMap<String, Integer>(); // NOTE: map ----> Topic : StreamCount [partition size]
        kafkaTopicMap.put("TOPIC_ANDON_RESOURCE-TIME-LOG_001", 3);
        kafkaTopicMap.put("TOPIC_ANDON_RESOURCE-REASON-LOG_001", 3);
        kafkaTopicMap.put("TOPIC_ANDON_RESOURCE-ALERT-LOG_001", 3);
        kafkaTopicMap.put("TOPIC_ANDON_PRODUCTION-OUTPUT-LOG_001", 3);
        kafkaTopicMap.put("TOPIC_ANDON_SYNC-COLOR-LIGHT_001", 3);
        kafkaTopicMap.put("TOPIC_ANDON_RESOURCE_PARAM-LOG_001",3);
        kafkaTopicMap.put("topic-mingzhu-test", 3);


        // init thread pool
        int threadPoolSize = 0;
        for (Map.Entry<String, Integer> entry : kafkaTopicMap.entrySet()) {
            threadPoolSize += entry.getValue();
        }
        kafkaConsumerThreadPool = new ThreadPoolService(threadPoolSize);
        kafkaConsumerThreadPool.start();
    }

    private void doBusinessByTopic(String topic, String message) {

        //日志格式 [topic#message#code#errMsg#times]

        logger.info("[KAFKA BUSINESS] topic:" + topic);
        long begintime = System.currentTimeMillis();
        Integer code = null;
        String errMsg = null;

        try {

            allInterfaceKafka.postResourceAlertLog(topic,message);

            code = 200;
            errMsg = "消费成功";

        } catch (BusinessException e) {

            logger.info("消息消费异常！"+e.getErrorCode());
            code = e.getErrorCode().getErrorCode();
            errMsg = e.getMessage();

        }finally {
            logger.info( String.format("[#%s#%s#%s#%s#%s]",topic,message,code,errMsg,(System.currentTimeMillis() - begintime)) );
        }


    }

    private Map<String, List<KafkaStream<String, String>>> subscribeTopic() {
        // decoder
        StringDecoder keyDecoder = new StringDecoder(new VerifiableProperties());
        StringDecoder valueDecoder = new StringDecoder(new VerifiableProperties());

        return kafkaConsumer.createMessageStreams(kafkaTopicMap, keyDecoder, valueDecoder);
    }

    private void register() {
        Map<String, List<KafkaStream<String, String>>> consumerMap = subscribeTopic();

        for (Map.Entry<String, List<KafkaStream<String, String>>> entry : consumerMap.entrySet()) { // NOTE: tack each topic
            final String topic = entry.getKey();
            List<KafkaStream<String, String>> streams = entry.getValue();
            logger.info("[KAFKA CONSUMER] topic:" + topic);

            for (final KafkaStream<String, String> stream : streams) { // NOTE: take each stream
                kafkaConsumerThreadPool.executeThread(new Runnable() {
                    @Override
                    public void run() {
                        for (MessageAndMetadata<String, String> msg : stream) { // NOTE: read msg from each stream
                            logger.info("[KAFKA MSG] topic:" + msg.topic() + " partition:" + msg.partition() + " message:" + msg.message());
                            try {
                                doBusinessByTopic(msg.topic(), msg.message());
                            } catch (Exception e) {
                                logger.error("[KAFKA ERROR] topic:" + msg.topic() + " message:" + msg.message() + " error:" + e.getMessage(), e);
                            }
                        }
                    }
                });
            }
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // true 注册 kafka 消费者
        if("true".equals(this.isEnableRegister)){
            register();
        }else {
            this.logger.info(">>>> 未注册KAFKA消费者，若要运行消费者请修改配置[kafka.consumer.switch = true]重启服务");
        }

    }

}
