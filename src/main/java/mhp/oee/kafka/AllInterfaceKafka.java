package mhp.oee.kafka;

import com.google.gson.Gson;
import mhp.oee.api.v1.*;
import mhp.oee.api.v1.request.*;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.web.Constants;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;


/**
 * Created by SAP-GW06 on 2017/4/26.
 */
@Component
public class AllInterfaceKafka {
    @InjectableLogger
    private static Logger logger;

    @Autowired
    private PostResourceTimeLogService postResourceTimeLogService;
    @Autowired
    private PostResourceReasonLogService postResourceReasonLogService;
    @Autowired
    private PostResourceAlertLogService postResourceAlertLogService;
    @Autowired
    private PostProductionOutputRealtimeService postProductionOutputRealtimeService;
    @Autowired
    private SyncColorLightService syncColorLightService;
    @Autowired
    private ResourceParamService resourceParamService;

    public void postResourceAlertLog(String topic, String message) throws BusinessException {

             Gson gson = new Gson();

            //设备活动日志接收resource_time_log_eap
            if("TOPIC_ANDON_RESOURCE-TIME-LOG_001".equals(topic)){
                ResourceTimeLogIn resourceTimeLogIn=gson.fromJson(message,ResourceTimeLogIn.class);
                postResourceTimeLogService.postResourceTimeLogEap(resourceTimeLogIn);
            }

            //设备停机原因日志接收 resource_reason_log_eap
            if("TOPIC_ANDON_RESOURCE-REASON-LOG_001".equals(topic)){
                ResourceReasonLogRequest resourceReasonLogRequest=gson.fromJson(message,ResourceReasonLogRequest.class);
                postResourceReasonLogService.postResourceReasonLogEap(resourceReasonLogRequest);
            }

            //报警信息接收resource_alert_log_eap
            if("TOPIC_ANDON_RESOURCE-ALERT-LOG_001".equals(topic)){
                ResourceAlertLogRequest resourceAlertLogRequest=gson.fromJson(message,ResourceAlertLogRequest.class);
                postResourceAlertLogService.postResourceAlertLogEap(resourceAlertLogRequest);
            }

            //设备产量信息接收 PRODUCTION-OUTPUT-LOG
            if("TOPIC_ANDON_PRODUCTION-OUTPUT-LOG_001".equals(topic)){
                ProductionOutputRealtimeIn productionOutputRealtimeIn=gson.fromJson(message,ProductionOutputRealtimeIn.class);
                postProductionOutputRealtimeService.postProductionOutputRealtimeEap(productionOutputRealtimeIn);
            }

            //三色灯信息接收 SYNC-COLOR-LIGHT
            if("TOPIC_ANDON_SYNC-COLOR-LIGHT_001".equals(topic)){
                ColorLightIn colorLightIn=gson.fromJson(message,ColorLightIn.class);
                syncColorLightService.syncColorLightEap(colorLightIn);
            }

            //设备参数采集信息接收 SYNC-COLOR-LIGHT
            if("TOPIC_ANDON_RESOURCE_PARAM-LOG_001".equals(topic)){
                ResourceParamRequest form=gson.fromJson(message,ResourceParamRequest.class);
                resourceParamService.batchUploadResourceParamData(form.getSite(),form.getResource_type(),form.getResource(),form.getProcessesParamKV(),form.getUploadDatetime(),form.getTimestamp());
            }


    }

}
