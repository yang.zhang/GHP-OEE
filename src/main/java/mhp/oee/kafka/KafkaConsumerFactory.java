package mhp.oee.kafka;

import kafka.consumer.ConsumerConfig;
import kafka.javaapi.consumer.ConsumerConnector;

import java.util.Properties;

/**
 * Created by LinZuK on 2016/12/23.
 */
public class KafkaConsumerFactory {

    private Properties props;

    public KafkaConsumerFactory(Properties props) {
        this.props = props;
    }

    public ConsumerConnector createJavaConsumerConnector() {
        return kafka.consumer.Consumer.createJavaConsumerConnector(new ConsumerConfig(props));
    }

}
