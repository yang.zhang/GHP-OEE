package mhp.oee.api.v1.response;

public class ResourcePlcOut {

    private String site;
    private String resrce;
    private String category;
    private String plcAddress;
    private String plcValue;
    private String plcObject;
    private String description;
    private String logPath;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPlcAddress() {
        return plcAddress;
    }

    public void setPlcAddress(String plcAddress) {
        this.plcAddress = plcAddress;
    }

    public String getPlcValue() {
        return plcValue;
    }

    public void setPlcValue(String plcValue) {
        this.plcValue = plcValue;
    }

    public String getPlcObject() {
        return plcObject;
    }

    public void setPlcObject(String plcObject) {
        this.plcObject = plcObject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogPath() {
        return logPath;
    }

    public void setLogPath(String logPath) {
        this.logPath = logPath;
    }

}
