package mhp.oee.api.v1.response;

import java.util.List;

public class SearchResourcesOut {

    private String site;
    private String resource;
    private String description;
    private String resourceType;
    private String lineArea;
    private String workArea;
    private String workshop;
    private String eqpUserId;
    private String asset;
    private String subAsset;
    private String configuredPlcInfo;
    private String plcIp;
    private String plcPort;
    private String plcAccessType;
    private String enabled;
    private List<MeUserOut> meUserList;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getLineArea() {
        return lineArea;
    }

    public void setLineArea(String lineArea) {
        this.lineArea = lineArea;
    }

    public String getWorkArea() {
        return workArea;
    }

    public void setWorkArea(String workArea) {
        this.workArea = workArea;
    }

    public String getWorkshop() {
        return workshop;
    }

    public void setWorkshop(String workshop) {
        this.workshop = workshop;
    }

    public String getEqpUserId() {
        return eqpUserId;
    }

    public void setEqpUserId(String eqpUserId) {
        this.eqpUserId = eqpUserId;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getSubAsset() {
        return subAsset;
    }

    public void setSubAsset(String subAsset) {
        this.subAsset = subAsset;
    }

    public String getConfiguredPlcInfo() {
        return configuredPlcInfo;
    }

    public void setConfiguredPlcInfo(String configuredPlcInfo) {
        this.configuredPlcInfo = configuredPlcInfo;
    }

    public String getPlcIp() {
        return plcIp;
    }

    public void setPlcIp(String plcIp) {
        this.plcIp = plcIp;
    }

    public String getPlcPort() {
        return plcPort;
    }

    public void setPlcPort(String plcPort) {
        this.plcPort = plcPort;
    }

    public String getPlcAccessType() {
        return plcAccessType;
    }

    public void setPlcAccessType(String plcAccessType) {
        this.plcAccessType = plcAccessType;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public List<MeUserOut> getMeUserList() {
        return meUserList;
    }

    public void setMeUserList(List<MeUserOut> meUserList) {
        this.meUserList = meUserList;
    }

}
