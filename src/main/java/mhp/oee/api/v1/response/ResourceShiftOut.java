package mhp.oee.api.v1.response;

import java.util.List;

public class ResourceShiftOut {

    private String site;
    private String resrce;
    private String workArea;
    private String workAreaDescription;
    private String lineArea;
    private String lineAreaDescription;
    private List<WorkCenterShiftOut> workCenterShiftOutList;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public String getWorkArea() {
        return workArea;
    }

    public void setWorkArea(String workArea) {
        this.workArea = workArea;
    }

    public String getWorkAreaDescription() {
        return workAreaDescription;
    }

    public void setWorkAreaDescription(String workAreaDescription) {
        this.workAreaDescription = workAreaDescription;
    }

    public String getLineArea() {
        return lineArea;
    }

    public void setLineArea(String lineArea) {
        this.lineArea = lineArea;
    }

    public String getLineAreaDescription() {
        return lineAreaDescription;
    }

    public void setLineAreaDescription(String lineAreaDescription) {
        this.lineAreaDescription = lineAreaDescription;
    }

    public List<WorkCenterShiftOut> getWorkCenterShiftOutList() {
        return workCenterShiftOutList;
    }

    public void setWorkCenterShiftOutList(List<WorkCenterShiftOut> workCenterShiftOutList) {
        this.workCenterShiftOutList = workCenterShiftOutList;
    }

}
