package mhp.oee.api.v1.response;

public class SearchWorkAreasOut {

    private String site;
    private String workArea;
    private String workAreaDescription;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getWorkArea() {
        return workArea;
    }

    public void setWorkArea(String workArea) {
        this.workArea = workArea;
    }

    public String getWorkAreaDescription() {
        return workAreaDescription;
    }

    public void setWorkAreaDescription(String workAreaDescription) {
        this.workAreaDescription = workAreaDescription;
    }

}
