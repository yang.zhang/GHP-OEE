package mhp.oee.api.v1.response;

public class SearchLineAreasOut {

    private String site;
    private String lineArea;
    private String lineAreaDescription;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getLineArea() {
        return lineArea;
    }

    public void setLineArea(String lineArea) {
        this.lineArea = lineArea;
    }

    public String getLineAreaDescription() {
        return lineAreaDescription;
    }

    public void setLineAreaDescription(String lineAreaDescription) {
        this.lineAreaDescription = lineAreaDescription;
    }

}
