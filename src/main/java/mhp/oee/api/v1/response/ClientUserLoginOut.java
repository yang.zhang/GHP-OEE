package mhp.oee.api.v1.response;

import java.util.List;

public class ClientUserLoginOut {

    private String firstName;
    private String lastName;
    private String telephone;
    private String cellphone;
    private String email;
    private String department;
    private List<String> activityClientList;
    private String defaultSite;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public List<String> getActivityClientList() {
        return activityClientList;
    }

    public void setActivityClientList(List<String> activityClientList) {
        this.activityClientList = activityClientList;
    }

    public String getDefaultSite() {
        return defaultSite;
    }

    public void setDefaultSite(String defaultSite) {
        this.defaultSite = defaultSite;
    }

}
