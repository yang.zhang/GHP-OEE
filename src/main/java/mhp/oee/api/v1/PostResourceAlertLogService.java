package mhp.oee.api.v1;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.request.ResourceAlertLogIn;
import mhp.oee.api.v1.request.ResourceAlertLogRequest;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.component.plant.ResourcePlcComponent;
import mhp.oee.component.production.ResourceAlertLogComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceAlertLogVO;

/**
 * [WEB022] Post Resource Alert Log
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class PostResourceAlertLogService {

    @Autowired
    private ResourceAlertLogComponent resourceAlertLogComponent;
    
    @Autowired
    ResourcePlcComponent resourcePlcComponent;


    public int postResourceAlertLog(ResourceAlertLogRequest resourceAlertLogRequest) throws BusinessException {
        int ret = 0;
        Date now = new Date();

        for (ResourceAlertLogIn resourceAlertLogIn : resourceAlertLogRequest.getItems()) {
            ResourceAlertLogVO vo = Utils.copyObjectProperties(resourceAlertLogIn, ResourceAlertLogVO.class);
            vo.setDateTime(now);
            ret += resourceAlertLogComponent.createResourceAlertLog(vo);
        }
        return ret;
    }
    
    public int postResourceAlertLogEap(ResourceAlertLogRequest resourceAlertLogRequest) throws BusinessException {
        int ret = 0;
        Date now = null;
        
        
        for (ResourceAlertLogIn resourceAlertLogIn : resourceAlertLogRequest.getItems()) {
            String timestamp = resourceAlertLogIn.getTimestamp();
            try {
                if(StringUtils.isNotBlank(timestamp)){
                    now = DateUtils.parseDate(timestamp,"yyyy-MM-dd HH:mm:ss.SSS");
                }else{
                    now = new Date();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        	
        	ResrceBOHandle resrceBOHandle = new ResrceBOHandle(resourceAlertLogIn.getSite(), resourceAlertLogIn.getResrce());
        	String resourceStateIn = "D";
        	String plcObject = "";
        	String description = resourceAlertLogIn.getDescription();
        	
        	//根据传入参数 plcAddressAlert plcValueAlert 取描述description 若  description 为空，则该条记录不插入表
        	if(resourceAlertLogIn.getDescription() == null || "".equals(resourceAlertLogIn.getDescription())){
        		if(!(resourceAlertLogIn.getPlcAddressAlert() == null || resourceAlertLogIn.getPlcValueAlert() == null)){
        			description = resourcePlcComponent.getPlcDescription(resrceBOHandle.getValue(),resourceAlertLogIn.getPlcAddressAlert(),resourceAlertLogIn.getPlcValueAlert());
        		}
        		
        	}
        	if(description == null || "".equals(description)) break;
        	
        	//根据传入参数 plcAddress plcValue 确定描述设备状态 ，若  resourceStateIn 为ES01，则状态为P，否则为D
        	if(!(resourceAlertLogIn.getPlcValue() == null || resourceAlertLogIn.getPlcAddress() == null )){
        		plcObject = resourcePlcComponent.getPlcObject(resrceBOHandle.getValue(),resourceAlertLogIn.getPlcAddress(),resourceAlertLogIn.getPlcValue());
            	if("ES01".equals(plcObject)){
            		resourceStateIn = "P";
            	}
        	}
        	
            ResourceAlertLogVO vo = Utils.copyObjectProperties(resourceAlertLogIn, ResourceAlertLogVO.class);
            vo.setResourceState(resourceStateIn);
            vo.setDescription(description);
            vo.setResourceState(resourceStateIn);
            vo.setDateTime(now);
            vo.setPlcAddress(resourceAlertLogIn.getPlcAddressAlert());
            ret += resourceAlertLogComponent.createResourceAlertLog(vo);
        }
        return ret;
    }

}
