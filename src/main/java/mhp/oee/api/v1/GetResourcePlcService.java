package mhp.oee.api.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.response.ResourcePlcOut;
import mhp.oee.component.plant.ResourcePlcComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourcePlcVO;


/**
 * [WEB020] Get Resource PLC from Client
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class GetResourcePlcService {

    @Autowired
    ResourcePlcComponent resourcePlcComponent;

    public List<ResourcePlcOut> getResourcePlc(String site, String resource) {
        List<ResourcePlcVO> vos = resourcePlcComponent.selectByExample(site, resource);
        List<ResourcePlcOut> ret = Utils.copyListProperties(vos, ResourcePlcOut.class);
        return ret;
    }

}
