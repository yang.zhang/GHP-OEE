package mhp.oee.api.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.response.ResourceTypeOut;
import mhp.oee.component.plant.ResourceTypeComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceTypeVO;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 *
 * [WEB041] Get Resource Type
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class GetResourceTypeService {

    @Autowired
    private ResourceTypeComponent resourceTypeComponent;

    public List<ResourceTypeOut> getEnabledResourceTypeBySite(String site) {
        List<ResourceTypeVO> vos = resourceTypeComponent.readEnabledResourceTypeBySite(site);
        List<ResourceTypeOut> ret = Utils.copyListProperties(vos, ResourceTypeOut.class);
        return ret;
    }

}
