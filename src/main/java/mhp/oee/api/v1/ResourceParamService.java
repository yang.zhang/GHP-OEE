package mhp.oee.api.v1;

import mhp.oee.common.aspect.Result;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.ServiceErrorException;
import mhp.oee.component.production.ResourceParamComponent;
import mhp.oee.po.extend.ResourceParamMapperPO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by DingLJ on 2017/4/17 017.
 */
@Service
public class ResourceParamService {

    @Autowired
    private ResourceParamComponent resourceParamComponent;


    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.READ_COMMITTED)
    public Result batchUploadResourceParamData(String site, String resourceType,String resource, List<Map<String,String>> paramValues, String uploaDatetime,String timestamp){

        //1.前置判断
        if(StringUtils.isBlank(site)){
            throw new ServiceErrorException(ErrorCodeEnum.COMMON_SITE_NONE,"site 为空");
        }
//        if(StringUtils.isBlank(resourceType)){
//            throw new ServiceErrorException(ErrorCodeEnum.COMMON_RESOURCE_TYPE_NONE,"设备类型为空");
//        }
        if(StringUtils.isBlank(resource)){
            throw new ServiceErrorException(ErrorCodeEnum.COMMON_RESOURCE_NONE,"设备编码为空");
        }
        if(CollectionUtils.isEmpty(paramValues)){
            throw new ServiceErrorException(ErrorCodeEnum.RESOURCE_PARAM_NONE,"上传的参数为空");
        }
        if(StringUtils.isBlank(uploaDatetime)){
            throw new ServiceErrorException(ErrorCodeEnum.RESOURCE_PARAM_DATETIME_NONE,"时间戳为空");
        }
        //
        Date filterDate = null;
        Date now = null;
        try {
            filterDate = DateUtils.parseDate(uploaDatetime,"yyyy-MM-dd HH:mm:ss");
            if(StringUtils.isNotBlank(timestamp)){
                now = DateUtils.parseDate(timestamp,"yyyy-MM-dd HH:mm:ss.SSS");
            }else{
                now = new Date();
            }
        } catch (ParseException e) {
            throw new ServiceErrorException(ErrorCodeEnum.RESOURCE_PARAM_DATETIME_FORMATE_ERROR,"时间格式错误");
        }

        //2.批量写入
        int updates = 0;
        int expectedCount = 0;
        for (Map<String,String> m : paramValues){

            Map.Entry<String,String> entry = this.getMapFirstElement(m);
            if(entry != null){
                expectedCount++;
                updates += this.resourceParamComponent.create(site,resourceType,resource,entry.getKey(),entry.getValue(),filterDate,now);
            }

        }
        //要么全部插入 要么回滚
        if(updates != expectedCount){
            throw new ServiceErrorException(ErrorCodeEnum.RESOURCE_PARAM_WRITE_ERR,"数据写入失败");
        }

        Result result = new Result();
        result.ok();
        return result;
    }
    private Map.Entry<String,String> getMapFirstElement(Map<String,String> m){

        return CollectionUtils.isEmpty(m) ? null : m.entrySet().iterator().next();

    }

    public Result createMapper(String site, String resourceType, String paramName, String paramKey){


        this.resourceParamComponent.createMapper(site,resourceType,paramName,paramKey);


        Result result = new Result();
        result.ok();
        return result;

    }

    public Result<List<ResourceParamMapperPO>> readMappers(String site, String resourceType, String key){


        List<ResourceParamMapperPO> list = this.resourceParamComponent.readMappers(site,resourceType,key);


        Result result = new Result();
        result.ok();
        result.setData(list);
        return result;
    }




}
