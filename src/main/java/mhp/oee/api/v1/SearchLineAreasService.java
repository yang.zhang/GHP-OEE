package mhp.oee.api.v1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.response.SearchLineAreasOut;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.InvalidWorkCenterException;
import mhp.oee.common.handle.WorkCenterBOHandle;
import mhp.oee.component.plant.WorkCenterComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.WorkCenterConditionVO;
import mhp.oee.vo.WorkCenterVO;

/**
 * [WEB039] Get Line Areas
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class SearchLineAreasService {

    @Autowired
    private WorkCenterComponent workCenterComponent;

    public List<SearchLineAreasOut> searchLineAreas(String site, String workArea) throws BusinessException {

        WorkCenterBOHandle workCenterBOHandle = new WorkCenterBOHandle(site, workArea);
        WorkCenterVO workCenterVO = workCenterComponent.readWorkCenterByPrimaryKey(workCenterBOHandle.getValue());
        if (workCenterVO == null) {
            throw new InvalidWorkCenterException();
        }
        if (!WorkCenterComponent.WC_CATEGORY_WORK_AREA.equals(workCenterVO.getWcCategory())) {
            // work area category error
            throw new InvalidWorkCenterException();
        }
        if (!"true".equals(workCenterVO.getEnabled())) {
            // disabled work area
            throw new InvalidWorkCenterException();
        }

        List<WorkCenterConditionVO> lineAreas = workCenterComponent.readLineAreas(site, workCenterVO.getHandle());
        if (Utils.isEmpty(lineAreas)) {
            return Collections.emptyList();
        } else {
            List<SearchLineAreasOut> ret = new ArrayList<SearchLineAreasOut>(lineAreas.size());
            for (WorkCenterConditionVO la : lineAreas) {
                SearchLineAreasOut out = new SearchLineAreasOut();
                out.setSite(site);
                out.setLineArea(la.getWorkCenter());
                out.setLineAreaDescription(la.getDescription());
                ret.add(out);
            }
            return ret;
        }

    }

}
