package mhp.oee.api.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.response.SearchItemsOut;
import mhp.oee.component.production.ItemComponent;
import mhp.oee.po.gen.ItemPO;
import mhp.oee.utils.Utils;

/**
 * [WEB043] Get Materials per search criteria
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class SearchItemsService {

    @Autowired
    private ItemComponent itemComponent;

    public List<SearchItemsOut> searchItems(String site, String item, String revision, String model, String enabled) {
        List<ItemPO> pos = itemComponent.readItems(site, item, revision, model, enabled);
        List<SearchItemsOut> ret = Utils.copyListProperties(pos, SearchItemsOut.class);
        return ret;
    }

}
