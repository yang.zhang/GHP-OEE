package mhp.oee.api.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.request.FunctionalLocationIn;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.WorkCenterBOHandle;
import mhp.oee.component.plant.WorkCenterComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.WorkCenterMemberVO;
import mhp.oee.vo.WorkCenterVO;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 *
 * [WEB049] Synchronize Functional Location
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class SyncFunctionalLocationService {

    @Autowired
    private WorkCenterComponent workCenterComponent;

    public void syncFunctionalLocation(List<FunctionalLocationIn> functionalLocationList) throws BusinessException {
        for (FunctionalLocationIn each : functionalLocationList) {
            // work center
            WorkCenterVO wcVO = Utils.copyObjectProperties(each, WorkCenterVO.class);
            wcVO.setWorkCenter(each.getLineArea());
            wcVO.setWcCategory(WorkCenterComponent.WC_CATEGORY_LINE_AREA);
            if (!workCenterComponent.existWorkCenter(wcVO.getSite(), wcVO.getWorkCenter())) {
                workCenterComponent.createWorkCenter(wcVO);
            } else {
                workCenterComponent.updateWorkCenter(wcVO);
            }

            // work center member
            boolean enabled = "true".equals(each.getEnabled());
            WorkCenterBOHandle memberHandle = new WorkCenterBOHandle(each.getSite(), each.getLineArea());
            workCenterComponent.deleteWorkCenterMemberByMemberRef(memberHandle.getValue());
            if (enabled) {
                WorkCenterMemberVO vo = new WorkCenterMemberVO();
                WorkCenterBOHandle workCenterBOHandle = new WorkCenterBOHandle(each.getSite(), each.getWorkArea());
                vo.setWorkCenterBo(workCenterBOHandle.getValue());
                vo.setWorkCenterGbo(memberHandle.getValue());
                workCenterComponent.createWorkCenterMember(vo);
            }
        }
    }

}
