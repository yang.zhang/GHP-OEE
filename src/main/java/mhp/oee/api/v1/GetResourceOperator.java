package mhp.oee.api.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.response.ResourceOperatorOut;
import mhp.oee.api.v1.response.ResourceShiftOut;
import mhp.oee.api.v1.response.WorkCenterShiftOut;
import mhp.oee.common.handle.ResourceCurrentDataHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.handle.WorkCenterBOHandle;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.component.plant.ResourceCurrentDataComponent;
import mhp.oee.component.plant.WorkCenterComponent;
import mhp.oee.po.gen.ResourceCurrentDataPO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.WorkCenterShiftPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.WorkCenterShiftVO;
import mhp.oee.vo.WorkCenterVO;

/**
 * [WEB039] Get Resource and Shift
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class GetResourceOperator {

	@Autowired
    ResourceCurrentDataComponent resourceCurrentDataComponent;

    public ResourceOperatorOut getResourceOperator(String site, String resource) {

        ResrceBOHandle resrceBOHandle = new ResrceBOHandle(site, resource);
        
        ResourceCurrentDataHandle handle = new ResourceCurrentDataHandle(resrceBOHandle.getValue());
        ResourceCurrentDataPO po = resourceCurrentDataComponent.selectByHandle(handle.getValue());
       
        ResourceOperatorOut out = new ResourceOperatorOut();
        out.setSite(site == null ?"":site);
        out.setResrce(resource== null ?"":resource);
        out.setResourceOperator(po.getResourceOperator() == null ? "" :po.getResourceOperator());
		return out;
    }


}
