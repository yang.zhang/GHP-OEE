package mhp.oee.api.v1;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.DateUtil;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.api.v1.request.ResourceReasonLogIn;
import mhp.oee.api.v1.request.ResourceReasonLogRequest;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.InvalidInputParamException;
import mhp.oee.common.exception.InvalidResourceException;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.component.plant.ResourcePlcComponent;
import mhp.oee.component.production.ResourceReasonLogComponent;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourceReasonLogPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceReasonLogVO;

/**
 * [WEB036] Post Resource Reason Log
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class PostResourceReasonLogService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private ResourceReasonLogComponent resourceReasonLogComponent;
    
    @Autowired
    private ResourceComponent resourceComponent;
    
    @Autowired
    ResourcePlcComponent resourcePlcComponent;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();


    public int postResourceReasonLog(ResourceReasonLogRequest resourceReasonLogRequest) throws BusinessException {
        int ret = 0;

        for (ResourceReasonLogIn resourceReasonLogIn : resourceReasonLogRequest.getItems()) {
            ResourceReasonLogVO vo = Utils.copyObjectProperties(resourceReasonLogIn, ResourceReasonLogVO.class);
            if (Utils.isEmpty(resourceReasonLogIn.getActionCode())) {
                throw new InvalidInputParamException("actionCode");
            }

            ResrceBOHandle resrceBOHandle = new ResrceBOHandle(resourceReasonLogIn.getSite(), resourceReasonLogIn.getResrce());
            ResourcePO po = resourceComponent.getResourceByHandle(resrceBOHandle.getValue());

            if (po == null) {
                logger.error("PostResourceReasonLogService.postResourceReasonLog() : Invalid Resource = " + gson.toJson(resourceReasonLogIn));
                throw new InvalidResourceException();
            }

            if (!"true".equals(po.getConfiguredPlcInfo())) {
                logger.info("PostResourceReasonLogService.postResourceReasonLog() : CONFIGURED_PLC_INFO != true, skipping post process.");
                continue;
            }
            if(!Utils.isEmpty(resourceReasonLogIn.getMaintenanceNotification())||!Utils.isEmpty(resourceReasonLogIn.getMaintenanceOrder())){
                List<ResourceReasonLogPO> pos = resourceReasonLogComponent.selectByExample(
                        resourceReasonLogIn.getResrce(),
                        resourceReasonLogIn.getReasonCode(),
                        resourceReasonLogIn.getActionCode(),
                        resourceReasonLogIn.getMaintenanceNotification(),
                        resourceReasonLogIn.getMaintenanceOrder());
                if (!Utils.isEmpty(pos)) {
                    logger.info("PostResourceReasonLogService.postResourceReasonLog() : Existing records with specified criteria. " + gson.toJson(pos));
                    throw new ExistingRecordException("RESOURCE_REASON_LOG",gson.toJson(pos));
                }else {
                    vo.setDateTime(new Date());
                    ret += resourceReasonLogComponent.createResourceReasonLog(vo);
                }
            }else {
                vo.setDateTime(new Date());
                ret += resourceReasonLogComponent.createResourceReasonLog(vo);
            }
        }

        return ret;
    }
    
    public int postResourceReasonLogEap(ResourceReasonLogRequest resourceReasonLogRequest) throws BusinessException {
        int ret = 0;
       
        for (ResourceReasonLogIn resourceReasonLogIn : resourceReasonLogRequest.getItems()) {

            Date now = null;
            String timestamp = resourceReasonLogIn.getTimestamp();
            try {
                if(StringUtils.isNotBlank(timestamp)){
                    now = DateUtils.parseDate(timestamp,"yyyy-MM-dd HH:mm:ss.SSS");
                }else{
                    now = new Date();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        	 String reasonCode = resourceReasonLogIn.getReasonCode();
        	 ResrceBOHandle resrceBOHandle = new ResrceBOHandle(resourceReasonLogIn.getSite(), resourceReasonLogIn.getResrce());
        	 if(reasonCode == null || "".equals(reasonCode)){
        		 if(!(resourceReasonLogIn.getPlcAddress() == null || resourceReasonLogIn.getPlcValue() == null ||
        				 "".equals(resourceReasonLogIn.getPlcAddress()) || "".equals(resourceReasonLogIn.getPlcValue())))
        		 reasonCode = resourcePlcComponent.getPlcObject(resrceBOHandle.getValue(),
        				 resourceReasonLogIn.getPlcAddress(),resourceReasonLogIn.getPlcValue());
        	 }
        	 if(reasonCode == null || "".equals(reasonCode)){
        		 continue;
        	 }else {
        		 resourceReasonLogIn.setReasonCode(reasonCode);
        	 }
        	
            ResourceReasonLogVO vo = Utils.copyObjectProperties(resourceReasonLogIn, ResourceReasonLogVO.class);
            if (Utils.isEmpty(resourceReasonLogIn.getActionCode())) {
                throw new InvalidInputParamException("actionCode");
            }

            ResourcePO po = resourceComponent.getResourceByHandle(resrceBOHandle.getValue());

            if (po == null) {
                logger.error("PostResourceReasonLogService.postResourceReasonLog() : Invalid Resource = " + gson.toJson(resourceReasonLogIn));
                throw new InvalidResourceException();
            }

            if (!"true".equals(po.getConfiguredPlcInfo())) {
                logger.info("PostResourceReasonLogService.postResourceReasonLog() : CONFIGURED_PLC_INFO != true, skipping post process.");
                continue;
            }
            if(!Utils.isEmpty(resourceReasonLogIn.getMaintenanceNotification())||!Utils.isEmpty(resourceReasonLogIn.getMaintenanceOrder())){
                List<ResourceReasonLogPO> pos = resourceReasonLogComponent.selectByExample(
                        resourceReasonLogIn.getResrce(),
                        resourceReasonLogIn.getReasonCode(),
                        resourceReasonLogIn.getActionCode(),
                        resourceReasonLogIn.getMaintenanceNotification(),
                        resourceReasonLogIn.getMaintenanceOrder());
                if (!Utils.isEmpty(pos)) {
                    logger.info("PostResourceReasonLogService.postResourceReasonLog() : Existing records with specified criteria. " + gson.toJson(pos));
                    throw new ExistingRecordException("RESOURCE_REASON_LOG",gson.toJson(pos));
                }else {
                    vo.setDateTime(now);
                    ret += resourceReasonLogComponent.createResourceReasonLog(vo);
                }
            }else {
                vo.setDateTime(now);
                ret += resourceReasonLogComponent.createResourceReasonLog(vo);
            }
        }

        return ret;
    }
    
    

}
