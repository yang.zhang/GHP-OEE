package mhp.oee.api.v1;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.request.ColorLightIn;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.ColorLightBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.component.plant.ResourcePlcComponent;
import mhp.oee.component.production.ColorLightComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ColorLightVO;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 *
 * [WEB048] Sync color_light interface
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class SyncColorLightService {

    @Autowired
    private ColorLightComponent colorLightComponent;
    
    @Autowired
    ResourcePlcComponent resourcePlcComponent;

    public void syncColorLight(ColorLightIn colorLightIn) throws BusinessException {
        ColorLightVO vo = Utils.copyObjectProperties(colorLightIn, ColorLightVO.class);
        ResrceBOHandle ResrceBOHandle = new ResrceBOHandle(colorLightIn.getSite(), colorLightIn.getResrce());
        vo.setResourceBo(ResrceBOHandle.getValue());
        vo.setDateTime(new Date());
        String handle = new ColorLightBOHandle(vo.getResourceBo()).getValue();
        if (!colorLightComponent.existColorLight(handle)) {
            colorLightComponent.createColorLight(vo);
        } else {
            colorLightComponent.updateColorLight(vo);
        }
    }
    
    public void syncColorLightEap(ColorLightIn colorLightIn) throws BusinessException {
        ColorLightVO vo = Utils.copyObjectProperties(colorLightIn, ColorLightVO.class);
        Date now = null;
        String timestamp =colorLightIn.getTimestamp();
        try {
            if(StringUtils.isNotBlank(timestamp)){
                now = DateUtils.parseDate(timestamp,"yyyy-MM-dd HH:mm:ss.SSS");
            }else{
                now = new Date();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ResrceBOHandle ResrceBOHandle = new ResrceBOHandle(colorLightIn.getSite(), colorLightIn.getResrce());
        vo.setResourceBo(ResrceBOHandle.getValue());
        vo.setDateTime(now);
        String colorLigh = colorLightIn.getColorLight();
    	if(colorLightIn.getColorLight() == null || "".equals(colorLightIn.getColorLight())){
    		if(!(colorLightIn.getPlcAddress() == null || colorLightIn.getPlcValue() == null || 
    				"".equals(colorLightIn.getPlcAddress()) || "".equals(colorLightIn.getPlcValue()))){
    			colorLigh  =  resourcePlcComponent.getPlcObject(ResrceBOHandle.getValue(),colorLightIn.getPlcAddress(),colorLightIn.getPlcValue());
    			if("CL01".equals(colorLigh)){
    				vo.setColorLight("R");
    			}else if("CL02".equals(colorLigh)){
    				vo.setColorLight("Y");
    			}else if("CL03".equals(colorLigh)){
    				vo.setColorLight("G");
    			}else{
    				return;
    			}
    		}
    	}
        String handle = new ColorLightBOHandle(vo.getResourceBo()).getValue();
        if (!colorLightComponent.existColorLight(handle)) {
            colorLightComponent.createColorLight(vo);
        } else {
            colorLightComponent.updateColorLight(vo);
        }
    }

}
