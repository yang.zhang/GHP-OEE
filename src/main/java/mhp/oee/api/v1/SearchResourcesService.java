package mhp.oee.api.v1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.response.MeUserOut;
import mhp.oee.api.v1.response.SearchResourcesOut;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.handle.ResourcePlcInfoBOHandle;
import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.component.plant.ResourceMeUserComponent;
import mhp.oee.component.plant.ResourcePlcInfoComponent;
import mhp.oee.component.plant.ResourceTypeResourceComponent;
import mhp.oee.po.gen.ResourceMeUserPO;
import mhp.oee.po.gen.ResourceMeUserPOExample;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourcePOExample;
import mhp.oee.po.gen.ResourcePOExample.Criteria;
import mhp.oee.po.gen.ResourcePlcInfoPO;
import mhp.oee.po.gen.ResourcePlcInfoPOExample;
import mhp.oee.po.gen.ResourceTypeResourcePO;
import mhp.oee.po.gen.ResourceTypeResourcePOExample;
import mhp.oee.utils.Utils;

/**
 * [WEB037] Get Resources per search criteria
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class SearchResourcesService {

    @Autowired
    private ResourceComponent resourceComponent;
    @Autowired
    private ResourceTypeResourceComponent resourceTypeResourceComponent;
    @Autowired
    private ResourceMeUserComponent resourceMeUserComponent;
    @Autowired
    private ResourcePlcInfoComponent resourcePlcInfoComponent;


    public List<SearchResourcesOut> searchResources(String site, String resource, String resourceType,
            String workArea, String lineArea, String meUserId, String pcId) throws BusinessException {

        
		ResrceBOHandle resrceHandle = new ResrceBOHandle(site, resource);
		/*if (!resourceComponent.existResource(resrceHandle.getValue())) {
			throw new BusinessException(ErrorCodeEnum.RES_NOT_EXIT, "resource does not exist");
		}*/
		if (pcId != null && pcId != "") {
		    if (!resourceComponent.existResource(resrceHandle.getValue())) {
	            throw new BusinessException(ErrorCodeEnum.RES_NOT_EXIT, "resource does not exist");
	        }
			List<ResourcePlcInfoPO> pos1 = resourcePlcInfoComponent.findBySiteAndResource(site, resource, pcId);
			if (pos1.size() == 1) {
				if (pos1.get(0).getPcIp() == null) {
					throw new BusinessException(ErrorCodeEnum.IP_CONFIG_ERROR, "PC IP configuration exception");
				} else if (!pos1.get(0).getPcIp().equals(pcId)) {
					throw new BusinessException(ErrorCodeEnum.IP_ERROR,
							"The host computer IP does not match the device ID");
				}
			} else {
				throw new BusinessException(ErrorCodeEnum.IP_CONFIG_ERROR, "PC IP configuration exception");
			}
			;
		}
    	
    	ResourcePOExample example = new ResourcePOExample();
        Criteria criteria = example.createCriteria();

        criteria.andSiteEqualTo(site);
        if (resource != null) {
            criteria.andResrceEqualTo(resource);
        }

        if (resourceType != null) {
            // RESRCE : RESOURCE_TYPE = n : 1 mapping
            ResourceTypeBOHandle resourceTypeBOHandle = new ResourceTypeBOHandle(site, resourceType);
            List<ResourceTypeResourcePO> pos = resourceTypeResourceComponent
                    .readByResourceTypeBo(resourceTypeBOHandle.getValue());
            if (!Utils.isEmpty(pos)) {
                List<String> resourceList = new ArrayList<String>(pos.size());
                for (ResourceTypeResourcePO po : pos) {
                    resourceList.add(new ResrceBOHandle(po.getResourceBo()).getResrce());
                }
                criteria.andResrceIn(resourceList);
            } else {
                // force to return with an empty object
                return Collections.emptyList();
            }
        }

        if (workArea != null) {
            criteria.andWorkAreaEqualTo(workArea);
        }

        if (lineArea != null) {
            criteria.andLineAreaEqualTo(lineArea);
        }

        if (meUserId != null) {
            List<String> resourceList = getResourcesByMeUserId(site, meUserId);
            if (!Utils.isEmpty(resourceList)) {
                criteria.andResrceIn(resourceList);
            } else {
                // force to return with an empty object
                return Collections.emptyList();
            }
        }
        example.setOrderByClause("RESRCE");
        List<ResourcePO> pos = resourceComponent.readResourcesByExample(example);
        if (Utils.isEmpty(pos)) {
            // force to return with an empty object
            return Collections.emptyList();
        }

        List<SearchResourcesOut> ret = new ArrayList<SearchResourcesOut>(pos.size());
        for (ResourcePO each : pos) {
            SearchResourcesOut out = Utils.copyObjectProperties(each, SearchResourcesOut.class);
            out.setResource(each.getResrce());
            ret.add(out);
        }

        Map<String, String> cache = null;
        if (resourceType == null) {
            Set<String> resourceRefSet = new HashSet<String>();
            for (SearchResourcesOut each : ret) {
                ResrceBOHandle resrceBOHandle = new ResrceBOHandle(site, each.getResource());
                resourceRefSet.add(resrceBOHandle.getValue());
            }
            List<String> resourceRefList = new ArrayList<String>();
            resourceRefList.addAll(resourceRefSet);
            cache = buildResourceTypeCache(resourceRefList);
        }

        for (SearchResourcesOut each : ret) {
            if (resourceType != null) {
                each.setResourceType(resourceType);
            } else {
                // read from cache
                String val = cache.get(each.getResource());
                each.setResourceType(val);
            }

            // TODO: optimize RESOURCE_PLC_INFO by left join with RESRCE
            ResourcePlcInfoPO po = getResourcePlcInfo(site, each.getResource());
            if (po != null) {
                each.setPlcIp(po.getPlcIp());
                each.setPlcPort(po.getPlcPort());
                each.setPlcAccessType(po.getPlcAccessType());
            }

            List<MeUserOut> meUserList = getMeUsers(site, each.getResource());
            each.setMeUserList(meUserList);
        }

        return ret;
    }

    private List<String> getResourcesByMeUserId(String site, String meUserId) {
        ResourceMeUserPOExample example = new ResourceMeUserPOExample();
        example.createCriteria().andEnabledEqualTo("true").andMeUserIdEqualTo(meUserId.toUpperCase());  // force to be upper case
        List<ResourceMeUserPO> pos = resourceMeUserComponent.readResourceMeUsersByExample(example);
        List<String> ret = Collections.emptyList();
        if (pos.size() > 0) {
            ret = new ArrayList<String>(pos.size());
            for (ResourceMeUserPO each : pos) {
                ResrceBOHandle resrceBOHandle = new ResrceBOHandle(each.getResourceBo());
                if (site.equals(resrceBOHandle.getSite())) {
                    ret.add(resrceBOHandle.getResrce());
                }
            }
        }
        return ret;
    }

    private ResourcePlcInfoPO getResourcePlcInfo(String site, String resource) {
        ResrceBOHandle resrceBOHandle = new ResrceBOHandle(site, resource);
        ResourcePlcInfoBOHandle resourcePlcInfoBOHandle = new ResourcePlcInfoBOHandle(site, resrceBOHandle.getValue());
        return resourcePlcInfoComponent.readResPlcInfo(resourcePlcInfoBOHandle.getValue());
    }

    private List<MeUserOut> getMeUsers(String site, String resource) {
        ResourceMeUserPOExample example = new ResourceMeUserPOExample();
        ResrceBOHandle resrceBOHandle = new ResrceBOHandle(site, resource);
        example.createCriteria().andResourceBoEqualTo(resrceBOHandle.getValue()).andEnabledEqualTo("true");
        List<ResourceMeUserPO> meUserPOs = resourceMeUserComponent.readResourceMeUsersByExample(example);
        return Utils.copyListProperties(meUserPOs, MeUserOut.class);

    }

    private Map<String, String> buildResourceTypeCache(List<String> resourceRefList) {
        Map<String, String> cache = new HashMap<String, String>();
        ResourceTypeResourcePOExample example = new ResourceTypeResourcePOExample();
        example.createCriteria().andResourceBoIn(resourceRefList);
        List<ResourceTypeResourcePO> pos = resourceTypeResourceComponent.readByExample(example);
        for (ResourceTypeResourcePO each : pos) {
            String key = new ResrceBOHandle(each.getResourceBo()).getResrce();
            String val = new ResourceTypeBOHandle(each.getResourceTypeBo()).getResourceType();
            cache.put(key, val);
        }
        return cache;
    }

}
