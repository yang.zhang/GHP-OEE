package mhp.oee.api.v1;

import java.util.List;

import mhp.oee.api.v1.request.*;
import mhp.oee.common.aspect.Result;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.ServiceErrorException;
import mhp.oee.common.security.ContextHelper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.api.v1.response.ClientUserLoginOut;
import mhp.oee.api.v1.response.ResourceOperatorOut;
import mhp.oee.api.v1.response.ResourcePlcOut;
import mhp.oee.api.v1.response.ResourceShiftOut;
import mhp.oee.api.v1.response.ResourceTypeOut;
import mhp.oee.api.v1.response.SearchItemsOut;
import mhp.oee.api.v1.response.SearchLineAreasOut;
import mhp.oee.api.v1.response.SearchResourcesOut;
import mhp.oee.api.v1.response.SearchWorkAreasOut;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.web.Constants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class ClientIntegrationController {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    private ClientUserLoginService clientUserLoginService;
    @Autowired
    private GetResourceTypeService getResourceTypeService;
    @Autowired
    private GetResourcePlcService getResourcePlcService;
    @Autowired
    private GetResourceShiftService getResourceShiftService;
    @Autowired
    private GetResourceOperator getResourceOperator;
    
    @Autowired
    private SearchItemsService searchItemsService;
    @Autowired
    private SearchResourcesService searchResourcesService;
    @Autowired
    private SearchWorkAreasService searchWorkAreasService;
    @Autowired
    private SearchLineAreasService searchLineAreasService;

    @Autowired
    private PostResourceAlertLogService postResourceAlertLogService;
    @Autowired
    private PostResourceReasonLogService postResourceReasonLogService;
    @Autowired
    private PostProductionOutputRealtimeService postProductionOutputRealtimeService;
    @Autowired
    private PostResourceTimeLogService postResourceTimeLogService;
    @Autowired
    private PostClientLoginLogService postClientLoginLogService;

    @Autowired
    private SyncRealtimePpmService syncRealtimePpmService;
    @Autowired
    private SyncColorLightService syncColorLightService;

    @Autowired
    private ResourceParamService resourceParamService;

    /**
     * [WEB052] Client User Login
     */
    @RequestMapping(value = "/api/v1/client_user_login/{user}",
                    method = RequestMethod.GET,
                    produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<ClientUserLoginOut> clientUserLogin(@PathVariable("user") String user,
            @RequestParam(value = "login_mode", required = true) String loginMode,
            @RequestParam(value = "resource", required = true) String resource) {
        logger.debug("start to logon v1 user:"+user + "    resource:"+resource);
        String trace = String.format("[API.v1][client_user_login/%s]", user);
        logger.debug(">> " + trace);
        ClientUserLoginOut response = null;

        try {
            response = clientUserLoginService.clientUserLogin(user, loginMode, resource);
        } catch (BusinessException e) {
            logger.error("## " + trace + " : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< " + trace + " : " + response);
            logger.debug("end to logon v1 user:"+user + "    resource:"+resource);
        }
        return ResponseEntity.ok(response);
    }

    /**
     * [WEB039] <1> Get Resource and Shift
     */
    @RequestMapping(value = "/api/v1/site/{site}/resource/{resource}/shift",
            method = RequestMethod.GET, produces=Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<ResourceShiftOut> getResourceShift(
            @PathVariable("site") String site,
            @PathVariable("resource") String resource) {

        String trace = String.format("[API.v1][site/%s/resource/%s/shift]", site, resource);
        logger.debug(">> " + trace);
        ResourceShiftOut ret = getResourceShiftService.getResourceShift(site, resource);
        logger.debug("<< " + trace + " : " + gson.toJson(ret));
        return ResponseEntity.ok(ret);
    }

    /**
     * [WEB020] Get Resource PLC
     */
    @RequestMapping(value = "/api/v1/site/{site}/resource/{resource}/plc",
            method = RequestMethod.GET, produces=Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourcePlcOut>> getResourcePlc(
            @PathVariable("site") String site,
            @PathVariable("resource") String resource) {

        String trace = String.format("[API.v1][site/%s/resource_plc/%s]", site, resource);
        logger.debug(">> " + trace);
        List<ResourcePlcOut> ret = getResourcePlcService.getResourcePlc(site, resource);
        logger.debug("<< " + trace + " : " + gson.toJson(ret));
        return ResponseEntity.ok(ret);
    }

    /**
     * [WEB041] Get Resource Type
     */
    @RequestMapping(value = "/api/v1/site/{site}/resource_type",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceTypeOut>> getResourceType(@PathVariable("site") String site) {
        String trace = String.format("[API.v1][site/%s/resource_type]", site);
        logger.debug(">> " + trace);
        List<ResourceTypeOut> response = getResourceTypeService.getEnabledResourceTypeBySite(site);
        logger.debug("<< " + trace + System.lineSeparator() + gson.toJson(response));
        return ResponseEntity.ok().body(response);
    }

    /**
     * [WEB043] Get Materials per search criteria
     */
    @RequestMapping(value = "/api/v1/site/{site}/materials",
            method = RequestMethod.GET, produces=Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<SearchItemsOut>> searchItems(
            @PathVariable("site") String site,
            @RequestParam(value = "item",     required = false) String item,
            @RequestParam(value = "revision", required = false) String revision,
            @RequestParam(value = "model",    required = false) String model,
            @RequestParam(value = "enabled",  required = false) String enabled) {

        String trace = String.format("[API.v1][site/%s/materials] : item=%s, revision=%s, model=%s, enabled=%s",
                site, item, revision, model, enabled);
        logger.debug(">> " + trace);
        List<SearchItemsOut> items = searchItemsService.searchItems(site, item, revision, model, enabled);
        logger.debug("<< " + trace + " : " + gson.toJson(items));
        return ResponseEntity.ok().body(items);
    }

    /**
     * [WEB037] Get Resources per search criteria
     */
    @RequestMapping(value = "/api/v1/site/{site}/resources",
            method = RequestMethod.GET, produces=Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> searchResources(
            @PathVariable("site") String site,
            @RequestParam(value = "resource", required = false) String resource,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "work_area", required = false) String workArea,
            @RequestParam(value = "line_area", required = false) String lineArea,
            @RequestParam(value = "me_user_id", required = false) String meUserId,
            @RequestParam(value = "pc_ip", required = false) String pcId) {

        String trace = String.format(
                "[API.v1][site/%s/resources] : resource=%s, resourceType=%s, workArea=%s, lineArea=%s, meUserId=%s",
                site, resource, resourceType, workArea, lineArea, meUserId,pcId);
        List<SearchResourcesOut> resources = null;
        logger.debug(">> " + trace);
        try {
            resources = searchResourcesService.searchResources(site, resource, resourceType, workArea, lineArea, meUserId,pcId);
        } catch (BusinessException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< " + trace + " : " + gson.toJson(resources));
        }
        return ResponseEntity.ok().body(resources);
    }

    /**
     * [WEB039] <2> Get Work Areas
     */
    @RequestMapping(value = "/api/v1/site/{site}/work_areas",
            method = RequestMethod.GET, produces=Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<SearchWorkAreasOut>> searchWorkAreas(
            @PathVariable("site") String site) {

        String trace = String.format("[API.v1][site/%s/work_areas]", site);
        logger.debug(">> " + trace);
        List<SearchWorkAreasOut> ret = searchWorkAreasService.searchWorkAreas(site);
        logger.debug("<< " + trace + " : " + gson.toJson(ret));
        return ResponseEntity.ok(ret);
    }

    /**
     * [WEB039] <3> Get Line Areas
     */
    @RequestMapping(value = "/api/v1/site/{site}/work_area/{work_area}/line_areas",
            method = RequestMethod.GET, produces=Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<SearchLineAreasOut>> searchLineAreas(
            @PathVariable("site") String site,
            @PathVariable("work_area") String workArea) {

        String trace = String.format("[API.v1][site/%s/work_area/%s/line_areas]", site, workArea);
        logger.debug(">> " + trace);
        List<SearchLineAreasOut> ret = null;
        try {
            ret = searchLineAreasService.searchLineAreas(site, workArea);
        } catch (BusinessException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< " + trace + " : " + gson.toJson(ret));
        }
        return ResponseEntity.ok(ret);
    }

    /**
     * [WEB022] Post Resource Alert Log
     */
    @RequestMapping(value = "/api/v1/resource_alert_log", method = RequestMethod.POST)
    public ResponseEntity<?> postResourceAlertLog(@RequestBody ResourceAlertLogRequest resourceAlertLogRequest) {
        String trace = String.format("[API.v1][resource_alert_log] : %s", gson.toJson(resourceAlertLogRequest));
        logger.debug(">> " + trace);
        try {
            postResourceAlertLogService.postResourceAlertLog(resourceAlertLogRequest);
        } catch (BusinessException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< " + trace);
        }
        return ResponseEntity.ok().body(null);
    }
    /**
     * [WEB089] Post Resource Alert Log
     */
    @RequestMapping(value = "/api/v1/resource_alert_log_eap", method = RequestMethod.POST)
    public ResponseEntity<?> postResourceAlertLogEap(@RequestBody ResourceAlertLogRequest resourceAlertLogRequest) {
        String trace = String.format("[API.v1][resource_alert_log_eap] : %s", gson.toJson(resourceAlertLogRequest));
        logger.debug(">> " + trace);
        try {
            postResourceAlertLogService.postResourceAlertLogEap(resourceAlertLogRequest);
        } catch (BusinessException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< " + trace);
        }
        return ResponseEntity.ok().body(null);
    }

    /**
     * [WEB036] Post Resource Reason Log
     */
    @RequestMapping(value = "/api/v1/resource_reason_log", method = RequestMethod.POST)
    public ResponseEntity<?> postResourceReasonLog(@RequestBody ResourceReasonLogRequest resourceReasonLogRequest) {
        String trace = String.format("[API.v1][resource_reason_log] : %s", gson.toJson(resourceReasonLogRequest));
        logger.debug(">> " + trace);
        try {
            postResourceReasonLogService.postResourceReasonLog(resourceReasonLogRequest);
        } catch (BusinessException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< " + trace);
        }
        return ResponseEntity.ok().body(null);
    }
    
    /**
     * [WEB092] Post Resource Reason Log Eap
     */
    @RequestMapping(value = "/api/v1/resource_reason_log_eap", method = RequestMethod.POST)
    public ResponseEntity<?> postResourceReasonLogEap(@RequestBody ResourceReasonLogRequest resourceReasonLogRequest) {
    	 String trace = String.format("[API.v1][resource_reason_log] : %s", gson.toJson(resourceReasonLogRequest));
         logger.debug(">> " + trace);
         try {
             postResourceReasonLogService.postResourceReasonLogEap(resourceReasonLogRequest);
         } catch (BusinessException e) {
             logger.error(e.getMessage());
             return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                     .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
         } finally {
             logger.debug("<< " + trace);
         }
         return ResponseEntity.ok().body(null);
    	
    }


    /**
     * [WEB024] Post Production Output Realtime
     */
    @RequestMapping(value = "/api/v1/production_output_realtime", method = RequestMethod.POST)
    public ResponseEntity<?> postProductionOutputRealtime(@RequestBody ProductionOutputRealtimeIn productionOutputRealtimeIn) {
        String trace = String.format("[API.v1][production_output_realtime] : %s", gson.toJson(productionOutputRealtimeIn));
        logger.debug(">> " + trace);
        try {
            postProductionOutputRealtimeService.postProductionOutputRealtime(productionOutputRealtimeIn);
        } catch (BusinessException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< " + trace);
        }
        return ResponseEntity.ok().body(null);
    }
    
    
    /**
     * [WEB091] Post Production Output Realtime Eap
     */
    @RequestMapping(value = "/api/v1/production_output_realtime_eap", method = RequestMethod.POST)
    public ResponseEntity<?> postProductionOutputRealtimeEap(@RequestBody ProductionOutputRealtimeIn productionOutputRealtimeIn) {
        String trace = String.format("[API.v1][production_output_realtime_eap] : %s", gson.toJson(productionOutputRealtimeIn));
        logger.debug(">> " + trace);
        try {
            postProductionOutputRealtimeService.postProductionOutputRealtimeEap(productionOutputRealtimeIn);
        } catch (BusinessException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< " + trace);
        }
        return ResponseEntity.ok().body(null);
    }

    /**
     * [WEB023] Post Resource Time Log
     */
    @RequestMapping(value = "/api/v1/resource_time_log", method = RequestMethod.POST)
    public ResponseEntity<?> postResourceTimeLog(@RequestBody ResourceTimeLogIn resourceTimeLogIn) {
        logger.debug(">> [API.v1][resource_time_log] request = " + gson.toJson(resourceTimeLogIn));
        try {
            postResourceTimeLogService.postResourceTimeLog(resourceTimeLogIn);
        } catch (BusinessException e) {
            logger.error("## [API.v1][resource_time_log] : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< [API.v1][resource_time_log]");
        }
        return ResponseEntity.ok().body(null);
    }
    /**
     * [WEB090] Post Resource Time Log
     */
    @RequestMapping(value = "/api/v1/resource_time_log_eap", method = RequestMethod.POST)
    public ResponseEntity<?> postResourceTimeLogForEap(@RequestBody ResourceTimeLogIn resourceTimeLogIn) {
        logger.debug(">> [API.v1][resource_time_log_eap] request = " + gson.toJson(resourceTimeLogIn));
        try {
            postResourceTimeLogService.postResourceTimeLogEap(resourceTimeLogIn);
        } catch (BusinessException e) {
            logger.error("## [API.v1][resource_time_log_eap] : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< [API.v1][resource_time_log_eap]");
        }
        return ResponseEntity.ok().body(null);
    }

    /**
     * [WEB054] Post Client Login Log
     */
    @RequestMapping(value = "/api/v1/post_client_login_log", method = RequestMethod.POST)
    public ResponseEntity<?> postClentLoginLog(@RequestBody ClientLoginLogIn clientLoginIn) {
        logger.debug("[API.v1][post_client_login_log] request = " + gson.toJson(clientLoginIn));
        try {
            postClientLoginLogService.postClientLoginLog(clientLoginIn);
        } catch (BusinessException e) {
            logger.error("## [API.v1][post_client_login_log] : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< [API.v1][post_client_login_log]");
        }
        return ResponseEntity.ok().body(null);
    }

    /**
     * [WEB047] Sync realtime_ppm interface
     */
    @RequestMapping(value = "/api/v1/snyc_realtime_ppm", method = RequestMethod.POST)
    public ResponseEntity<?> syncRealtimePPM(@RequestBody RealtimePpmIn realtimePpmIn) {
        logger.debug(">> [API.v1][snyc_realtime_ppm] request = " + gson.toJson(realtimePpmIn));
        try {
            syncRealtimePpmService.syncRealtimePpm(realtimePpmIn);
        } catch (BusinessException e) {
            logger.error("## [API.v1][snyc_realtime_ppm] : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< [API.v1][snyc_realtime_ppm]");
        }
        return ResponseEntity.ok().body(null);
    }

    /**
     * [WEB048] Sync color_light interface
     */
    @RequestMapping(value = "/api/v1/sync_color_light", method = RequestMethod.POST)
    public ResponseEntity<?> syncColorLight(@RequestBody ColorLightIn colorLightIn) {
        logger.debug(">> [API.v1][sync_color_light] request = " + gson.toJson(colorLightIn));
        try {
            syncColorLightService.syncColorLight(colorLightIn);
        } catch (BusinessException e) {
            logger.error("## [API.v1][sync_color_light] : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< [API.v1][sync_color_light]");
        return ResponseEntity.ok().body(null);
    }
    
    /**
     * [WEB093] Sync color_light_eap interface
     */
    @RequestMapping(value = "/api/v1/sync_color_light_eap", method = RequestMethod.POST)
    public ResponseEntity<?> syncColorLightEap(@RequestBody ColorLightIn colorLightIn) {
        logger.debug(">> [API.v1][sync_color_light_eap] request = " + gson.toJson(colorLightIn));
        try {
            syncColorLightService.syncColorLightEap(colorLightIn);
        } catch (BusinessException e) {
            logger.error("## [API.v1][sync_color_light_eap] : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< [API.v1][sync_color_light]");
        return ResponseEntity.ok().body(null);
    }
    
    /**
     * [WEB102] <1> Get Resource and Operator
     */
    @RequestMapping(value = "/api/v1/site/{site}/resource/{resource}/operator",
            method = RequestMethod.GET, produces=Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<ResourceOperatorOut> getResourceOperator(
            @PathVariable("site") String site,
            @PathVariable("resource") String resource) {

        String trace = String.format("[API.v1][site/%s/resource/%s/operator]", site, resource);
        logger.debug(">> " + trace);
        ResourceOperatorOut ret = getResourceOperator.getResourceOperator(site, resource);
        logger.debug("<< " + trace + " : " + gson.toJson(ret));
        return ResponseEntity.ok(ret);
    }

    @RequestMapping(value = "/api/v1/processes/resource-param/upload", method = RequestMethod.POST)
    public ResponseEntity<?> uploadResourceParamData(HttpServletRequest request, @RequestBody ResourceParamRequest form){


        try {

            Result result = this.resourceParamService.batchUploadResourceParamData(form.getSite(),form.getResource_type(),form.getResource(),form.getProcessesParamKV(),form.getUploadDatetime(),form.getTimestamp());

            return ResponseEntity.ok(result);

        }catch (ServiceErrorException e){
            Result result = new Result(e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result);

        }catch (Exception e){
            Result result = new Result(new ServiceErrorException(ErrorCodeEnum.SYSTEM_INNER_ERROR,"系统内部异常"));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result);

        }

    }

}
