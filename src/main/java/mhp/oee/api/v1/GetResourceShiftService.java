package mhp.oee.api.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.response.ResourceShiftOut;
import mhp.oee.api.v1.response.WorkCenterShiftOut;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.handle.WorkCenterBOHandle;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.component.plant.WorkCenterComponent;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.WorkCenterShiftPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.WorkCenterShiftVO;
import mhp.oee.vo.WorkCenterVO;

/**
 * [WEB039] Get Resource and Shift
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class GetResourceShiftService {

    @Autowired
    private ResourceComponent resourceComponent;
    @Autowired
    private WorkCenterComponent workCenterComponent;

    public ResourceShiftOut getResourceShift(String site, String resource) {

        ResrceBOHandle resrceBOHandle = new ResrceBOHandle(site, resource);
        ResourcePO po = resourceComponent.getResourceByHandle(resrceBOHandle.getValue());
        if (!"true".equals(po.getEnabled())) {
            return new ResourceShiftOut();
        }

        ResourceShiftOut out = Utils.copyObjectProperties(po, ResourceShiftOut.class);
        if (po.getWorkArea() != null) {
            // populate workArea description
            String desc = getWorkCenterDescription(site, po.getWorkArea(), WorkCenterComponent.WC_CATEGORY_WORK_AREA);
            out.setWorkAreaDescription(desc);
        }
        if (po.getLineArea() != null) {
            // populate lineArea description
            String desc = getWorkCenterDescription(site, po.getLineArea(), WorkCenterComponent.WC_CATEGORY_LINE_AREA);
            out.setLineAreaDescription(desc);
        }
        if (po.getWorkArea() != null) {
            // populate WorkCenterShiftOut list
            List<WorkCenterShiftOut> workCenterShiftOutList = getWorkCenterShiftList(site, po.getWorkArea());
            out.setWorkCenterShiftOutList(workCenterShiftOutList);
        }
        return out;
    }

    private String getWorkCenterDescription(String site, String workArea, String wcCategory) {
        WorkCenterBOHandle workCenterBOHandle = new WorkCenterBOHandle(site, workArea);
        WorkCenterVO vo = workCenterComponent.readWorkCenterByPrimaryKey(workCenterBOHandle.getValue());
        return (vo != null && wcCategory.equals(vo.getWcCategory())) ? vo.getDescription() : "";
    }

    private List<WorkCenterShiftOut> getWorkCenterShiftList(String site, String workCenter) {
        WorkCenterBOHandle workCenterBOHandle = new WorkCenterBOHandle(site, workCenter);

        WorkCenterShiftPOExample example = new WorkCenterShiftPOExample();
        example.createCriteria().andWorkCenterBoEqualTo(workCenterBOHandle.getValue()).andEnabledEqualTo("true");
        List<WorkCenterShiftVO> shifts = workCenterComponent.readWorkCenterShiftByExample(example);
        List<WorkCenterShiftOut> out = new ArrayList<WorkCenterShiftOut>(shifts.size());
        for (WorkCenterShiftVO each : shifts) {
            WorkCenterShiftOut elem = Utils.copyObjectProperties(each, WorkCenterShiftOut.class);
            elem.setStartTime(Utils.timeToStr(each.getStartTime()));
            elem.setEndTime(Utils.timeToStr(each.getEndTime()));
            elem.setShiftDescription(each.getDescription());
            out.add(elem);
        }
        return out;
    }

}
