package mhp.oee.api.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.response.SearchWorkAreasOut;
import mhp.oee.component.plant.WorkCenterComponent;
import mhp.oee.po.gen.WorkCenterPOExample;
import mhp.oee.vo.WorkCenterVO;

/**
 * [WEB039] Get Work Areas
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class SearchWorkAreasService {

    @Autowired
    private WorkCenterComponent workCenterComponent;

    public List<SearchWorkAreasOut> searchWorkAreas(String site) {

        WorkCenterPOExample example = new WorkCenterPOExample();
        example.createCriteria().andSiteEqualTo(site).andEnabledEqualTo("true")
                .andWcCategoryEqualTo(WorkCenterComponent.WC_CATEGORY_WORK_AREA);
        example.setOrderByClause("WORK_CENTER");
        List<WorkCenterVO> list = workCenterComponent.readWorkCenterByExample(example);
        List<SearchWorkAreasOut> ret = new ArrayList<SearchWorkAreasOut>(list.size());
        for (WorkCenterVO each : list) {
            SearchWorkAreasOut elem = new SearchWorkAreasOut();
            elem.setSite(site);
            elem.setWorkArea(each.getWorkCenter());
            elem.setWorkAreaDescription(each.getDescription());
            ret.add(elem);
        }
        return ret;
    }

}
