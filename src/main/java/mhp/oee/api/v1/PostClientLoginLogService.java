package mhp.oee.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.request.ClientLoginLogIn;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.handle.UserBOHandle;
import mhp.oee.component.user.ClientLoginLogComponent;
import mhp.oee.component.user.UserComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ClientLoginLogVO;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 *
 * [WEB054] Post Client Login Log
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class PostClientLoginLogService {

    @Autowired
    private ClientLoginLogComponent clientLoginLogComponent;
    @Autowired
    private UserComponent userComponent;

    public void postClientLoginLog(ClientLoginLogIn clientLoginIn) throws BusinessException {
        UserBOHandle userBOHandle = new UserBOHandle(clientLoginIn.getSite(), clientLoginIn.getUser().toUpperCase());
        if (!userComponent.existUser(userBOHandle.getValue())) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "Invalid UserBO " + userBOHandle.getValue());
        }
        // no need to check if ResourceBO exists
        ResrceBOHandle resrceBOHandle = new ResrceBOHandle(clientLoginIn.getSite(), clientLoginIn.getResrce());
        ClientLoginLogVO vo = Utils.copyObjectProperties(clientLoginIn, ClientLoginLogVO.class);
        vo.setUserBo(userBOHandle.getValue());
        vo.setResourceBo(resrceBOHandle.getValue());
        clientLoginLogComponent.createClientLoginLog(vo);
    }
}
