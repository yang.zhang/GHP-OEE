package mhp.oee.api.v1;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.api.v1.request.ResourceTimeLogIn;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.InvalidInputParamException;
import mhp.oee.common.exception.RecordCorruptionException;
import mhp.oee.common.handle.ResourceTimeLogBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.component.plant.ResourcePlcComponent;
import mhp.oee.component.production.ResourceTimeLogComponent;
import mhp.oee.po.gen.ResourceTimeLogPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceTimeLogVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

/**
 * [WEB023] Post Resource Time Log
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class PostResourceTimeLogService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private ResourceTimeLogComponent resourceTimeLogComponent;
    
    @Autowired
    private ResourcePlcComponent resourcePlcComponent;


    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static Gson gsonFormat = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss:SSS").create();


    public void postResourceTimeLog(ResourceTimeLogIn resourceTimeLogIn) throws BusinessException {
        Date now = new Date();

        if (Utils.isEmpty(resourceTimeLogIn.getStartDateTime())) {
            // TODO: clarify the dateTime should be server time or input parameter
            String nowStr = Utils.datetimeMsToStr(now);
            resourceTimeLogIn.setStartDateTime(nowStr);
        }

        ResrceBOHandle resrceBOHandle = new ResrceBOHandle(resourceTimeLogIn.getSite(), resourceTimeLogIn.getResrce());

        ResourceTimeLogVO vo = pkgResourceTimeLogVO(resourceTimeLogIn.getSite(),
                resrceBOHandle.getValue(), now, resourceTimeLogIn.getResourceState());

        String resourceState = vo.getResourceState();
        if (!"P".equals(resourceState) && !"D".equals(resourceState) && !"U".equals(resourceState)) {
            throw new InvalidInputParamException("resourceState = " + resourceState);
        }

        modifyResourceTimeLog(now, vo, ContextHelper.getContext().getUser());
    }
    public void postResourceTimeLogEap(ResourceTimeLogIn resourceTimeLogIn) throws BusinessException {
        //Date now = new Date();
        Date now = null;

        String timestamp = resourceTimeLogIn.getTimestamp();
        try {
            if(StringUtils.isNotBlank(timestamp)){
                now = DateUtils.parseDate(timestamp,"yyyy-MM-dd HH:mm:ss.SSS");
            }else{
                now = new Date();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String resourceStateIn = "";
        String plcObject = "";
        String plcAddress = resourceTimeLogIn.getPlcAddress();
        String plcValue = resourceTimeLogIn.getPlcValue();
        String resourceBo = "ResourceBO:" + resourceTimeLogIn.getSite() + "," + resourceTimeLogIn.getResrce();
        //根据plcObject 取设备状态 ，若plcAddress 或 plcValue 为空，设备状态为U，
        if(plcAddress == null || "".equals(plcAddress) || plcValue == null || "".equals(plcValue)){
        	resourceStateIn = "U";
        }else{
        	//若plcAddress 或 plcValue不为空plcObject = "ES01" 为P 否则为D
        	plcObject = resourcePlcComponent.getPlcObject(resourceBo,plcAddress,plcValue);
        	if("ES01".equals(plcObject)){
        		resourceStateIn = "P";
        	}else {
        		resourceStateIn = "D";
        	}
        }



        if (Utils.isEmpty(resourceTimeLogIn.getStartDateTime())) {
            // TODO: clarify the dateTime should be server time or input parameter
            String nowStr = Utils.datetimeMsToStr(now);
            resourceTimeLogIn.setStartDateTime(nowStr);
        }

        ResrceBOHandle resrceBOHandle = new ResrceBOHandle(resourceTimeLogIn.getSite(), resourceTimeLogIn.getResrce());

        ResourceTimeLogVO vo = pkgResourceTimeLogVO(resourceTimeLogIn.getSite(),
                resrceBOHandle.getValue(), now, resourceStateIn);
        
        String resourceState = vo.getResourceState();
        if (!"P".equals(resourceState) && !"D".equals(resourceState) && !"U".equals(resourceState)) {
            throw new InvalidInputParamException("resourceState = " + resourceState);
        }

        modifyResourceTimeLog(now, vo, ContextHelper.getContext().getUser());
    }

    public ResourceTimeLogVO pkgResourceTimeLogVO(String site, String resourceBo, Date startDateTime, String state) {
        String strt = Utils.datetimeMsToStr(startDateTime);
        ResourceTimeLogBOHandle rtlHandle = new ResourceTimeLogBOHandle(site, resourceBo, strt);
        ResourceTimeLogVO vo = new ResourceTimeLogVO();
        vo.setHandle(rtlHandle.getValue());
        vo.setSite(site);
        vo.setResourceBo(resourceBo);
        vo.setStrt(strt);
        vo.setResourceState(state);
        vo.setStartDateTime(startDateTime);
        return vo;
    }

    public void modifyResourceTimeLog(Date now, ResourceTimeLogVO vo, String modifyUser) throws BusinessException {
        ResourceTimeLogPO po = resourceTimeLogComponent.readLastRecord(vo.getResourceBo());
        if (po != null) {
            if (!po.getResourceState().equals(vo.getResourceState())) {
                // update existing last active record
                po.setEndDateTime(now);
                try {
                    Date startDt = Utils.strToDatetimeMs(po.getStrt());
                    long diff = now.getTime() - startDt.getTime();  // msec
                    po.setElapsedTime(new BigDecimal(diff));
                } catch (ParseException ex) {
                    throw new RecordCorruptionException(gson.toJson(po));
                }
                resourceTimeLogComponent.updateResourceTimeLog(po, modifyUser);
                // add one more record to be the latest active record
                resourceTimeLogComponent.createResourceTimeLog(vo, modifyUser);
            }
        } else {
            // add one more record to be the latest active record
            resourceTimeLogComponent.createResourceTimeLog(vo, modifyUser);
        }
    }

}
