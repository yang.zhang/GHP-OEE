package mhp.oee.api.v1;

import java.text.ParseException;
import java.util.Date;

import mhp.oee.po.gen.ResourceCurrentDataPO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.request.ProductionOutputRealtimeIn;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.ResourceCurrentDataHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.component.plant.ResourceCurrentDataComponent;
import mhp.oee.component.production.ProductionOutputRealtimeComponent;
import mhp.oee.component.production.ResourceTimeLogComponent;
import mhp.oee.po.gen.ProductionOutputRealtimePO;
import mhp.oee.po.gen.ResourceTimeLogPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ProductionOutputRealtimeVO;

/**
 * [WEB024] Post Production Output Realtime
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class PostProductionOutputRealtimeService {

    @Autowired
    private ProductionOutputRealtimeComponent productionOutputRealtimeComponent;
    @Autowired
    private ResourceTimeLogComponent resourceTimeLogComponent;
    
    @Autowired
    private ResourceCurrentDataComponent  resourceCurrentDataComponent;
   


    public int postProductionOutputRealtime(ProductionOutputRealtimeIn productionOutputRealtimeIn) throws BusinessException {
        ProductionOutputRealtimeVO vo = Utils.copyObjectProperties(productionOutputRealtimeIn, ProductionOutputRealtimeVO.class);

        Date porDateTime = null;
        Date rtlDateTime = null;

        // get last endDateTime from PRODUCTION_OUTPUT_REALTIME
        ProductionOutputRealtimePO porPO = productionOutputRealtimeComponent.readLastRecord(
                productionOutputRealtimeIn.getSite(),
                productionOutputRealtimeIn.getResrce(),
                productionOutputRealtimeIn.getItem());
        if (porPO != null) {
            porDateTime = porPO.getEndDateTime();
            // get last startDateTime from RESOURCE_TIME_LOG
            ResrceBOHandle resourceHandle = new ResrceBOHandle(productionOutputRealtimeIn.getSite(), productionOutputRealtimeIn.getResrce());
            ResourceTimeLogPO rtlPO = resourceTimeLogComponent.readLastActiveRecord(resourceHandle.getValue());
            if (rtlPO != null) {
                rtlDateTime = rtlPO.getStartDateTime();
            }
        }


        Date now = new Date();
        Date startDateTime = null;
        if (porDateTime != null && rtlDateTime != null) {
            // compare
            if (porDateTime.compareTo(rtlDateTime) > 0) {
                startDateTime = porDateTime;
            } else {
                startDateTime = rtlDateTime;
            }
        } else if (porDateTime == null && rtlDateTime == null) {
            // this is an extreme case, commonly only happens if client data-post was missed.
            startDateTime = now;
        } else if (porDateTime != null) {
            startDateTime = porDateTime;
        } else { // rtlDateTime != null
            startDateTime = rtlDateTime;
        }

        vo.setStartDateTime(startDateTime);
        vo.setEndDateTime(now);
        int ret = productionOutputRealtimeComponent.createProductionOutputRealtime(vo);
        return ret;
    }
    
    public int postProductionOutputRealtimeEap(ProductionOutputRealtimeIn productionOutputRealtimeIn) throws BusinessException {
        ProductionOutputRealtimeVO vo = Utils.copyObjectProperties(productionOutputRealtimeIn, ProductionOutputRealtimeVO.class);

        Date porDateTime = null;
        Date rtlDateTime = null;
        
        ResrceBOHandle resrceBOHandle = new ResrceBOHandle(productionOutputRealtimeIn.getSite(), productionOutputRealtimeIn.getResrce());
        ResourceCurrentDataHandle resourceCurrentDataHandle = new ResourceCurrentDataHandle(resrceBOHandle.getValue());
        //String item = resourceCurrentDataComponent.selectByHandle(resourceCurrentDataHandle.getValue()).getItem();
        ResourceCurrentDataPO resourceCurrentDataPO = resourceCurrentDataComponent.selectByHandle(resourceCurrentDataHandle.getValue());
        if(resourceCurrentDataPO != null){
            String item = resourceCurrentDataPO.getItem();
            productionOutputRealtimeIn.setItem(item);
            vo.setItem(item);
            // get last endDateTime from PRODUCTION_OUTPUT_REALTIME
            ProductionOutputRealtimePO porPO = productionOutputRealtimeComponent.readLastRecord(
                    productionOutputRealtimeIn.getSite(),
                    productionOutputRealtimeIn.getResrce(),
                    productionOutputRealtimeIn.getItem());
            if (porPO != null) {
                porDateTime = porPO.getEndDateTime();
            }
        }


        // get last startDateTime from RESOURCE_TIME_LOG
        ResrceBOHandle resourceHandle = new ResrceBOHandle(productionOutputRealtimeIn.getSite(), productionOutputRealtimeIn.getResrce());
        ResourceTimeLogPO rtlPO = resourceTimeLogComponent.readLastActiveRecord(resourceHandle.getValue());
        if (rtlPO != null) {
            rtlDateTime = rtlPO.getStartDateTime();
        }

        Date now = null;
        String timestamp = productionOutputRealtimeIn.getTimestamp();
        try {
            if(StringUtils.isNotBlank(timestamp)){
                now = DateUtils.parseDate(timestamp,"yyyy-MM-dd HH:mm:ss.SSS");
            }else{
                now = new Date();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date startDateTime = null;
        if (porDateTime != null && rtlDateTime != null) {
            // compare
            if (porDateTime.compareTo(rtlDateTime) > 0) {
                startDateTime = porDateTime;
            } else {
                startDateTime = rtlDateTime;
            }
        } else if (porDateTime == null && rtlDateTime == null) {
            // this is an extreme case, commonly only happens if client data-post was missed.
            startDateTime = now;
        } else if (porDateTime != null) {
            startDateTime = porDateTime;
        } else { // rtlDateTime != null
            startDateTime = rtlDateTime;
        }

        vo.setStartDateTime(startDateTime);
        vo.setEndDateTime(now);
        int ret = productionOutputRealtimeComponent.createProductionOutputRealtime(vo);
        return ret;
    }

}
