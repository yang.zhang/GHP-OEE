package mhp.oee.api.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.request.ItemIn;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.component.production.ItemComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ItemVO;

/**
 * [WEB018] Synchronize Material
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class SyncItemService {

    @Autowired
    private ItemComponent itemComponent;

    public void syncItem(List<ItemIn> itemList) throws BusinessException {
        for (ItemIn each : itemList) {
            ItemVO vo = Utils.copyObjectProperties(each, ItemVO.class);
            if (!itemComponent.existItem(vo.getSite(), vo.getItem(), null)) {
                itemComponent.createItem(vo);
            } else {
                itemComponent.updateItem(vo);
            }
        }
    }

}
