package mhp.oee.api.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.request.ResourceIn;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.component.plant.ResourceTypeComponent;
import mhp.oee.component.plant.ResourceTypeResourceComponent;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourceTypeResourcePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceVO;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 *
 * [WEB019] Synchronize Resource
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class SyncResourceService {

    @Autowired
    private ResourceComponent resourceComponent;
    @Autowired
    private ResourceTypeComponent resourceTypeComponent;
    @Autowired
    private ResourceTypeResourceComponent resourceTypeResourceComponent;

    public void syncResource(List<ResourceIn> resourceList) throws BusinessException {
        for (ResourceIn each : resourceList) {
            // sync resource
            ResourceVO vo = Utils.copyObjectProperties(each, ResourceVO.class);
            String resourceRef = new ResrceBOHandle(each.getSite(), each.getResrce()).getValue();

            ResourcePO po = resourceComponent.getResourceByHandle(resourceRef);
            if (po == null) {
                resourceComponent.createResource(vo);
            } else {
                vo.setConfiguredPlcInfo(po.getConfiguredPlcInfo());
                resourceComponent.updateResource(vo);
            }
            // sync resource_type_resource
            if (!resourceTypeComponent.existResourceType(each.getSite(), each.getType())) {
                // TODO: update the exception
                throw new BusinessException(ErrorCodeEnum.BASIC, "getResourceType");
            }
            String resourceTypeRef = new ResourceTypeBOHandle(each.getSite(), each.getType()).getValue();
            ResourceTypeResourcePO rtrPO = resourceTypeResourceComponent.readByResourceBo(resourceRef);
            if (rtrPO != null) {
                resourceTypeResourceComponent.deleteResourceTypeResourceByResourceBo(resourceRef);
            }
            resourceTypeResourceComponent.createResourceTypeResource(resourceTypeRef, resourceRef);
        }
    }

}
