package mhp.oee.api.v1;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.request.RealtimePpmIn;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.RealtimePpmBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.component.production.RealtimePpmComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.RealtimePpmVO;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 *
 * [WEB047] Sync realtime_ppm interface
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class SyncRealtimePpmService {

    @Autowired
    private RealtimePpmComponent realtimePpmComponent;

    public void syncRealtimePpm(RealtimePpmIn realtimePpmIn) throws BusinessException {
        RealtimePpmVO vo = Utils.copyObjectProperties(realtimePpmIn, RealtimePpmVO.class);
        ResrceBOHandle resrceBOHandle = new ResrceBOHandle(realtimePpmIn.getSite(), realtimePpmIn.getResrce());
        vo.setResourceBo(resrceBOHandle.getValue());
        vo.setDateTime(new Date());
        RealtimePpmBOHandle realtimePpmBOHandle = new RealtimePpmBOHandle(resrceBOHandle.getValue());
        if (!realtimePpmComponent.existRealtimePpm(realtimePpmBOHandle.getValue())) {
            realtimePpmComponent.createRealtimePpm(vo);
        } else {
            realtimePpmComponent.updateRealtimePpm(vo);
        }
    }

}
