package mhp.oee.api.v1;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.api.v1.request.FunctionalLocationIn;
import mhp.oee.api.v1.request.FunctionalLocationRequest;
import mhp.oee.api.v1.request.ItemIn;
import mhp.oee.api.v1.request.ItemRequest;
import mhp.oee.api.v1.request.ResourceIn;
import mhp.oee.api.v1.request.ResourceMeUserIn;
import mhp.oee.api.v1.request.ResourceMeUserRequest;
import mhp.oee.api.v1.request.ResourceTypeIn;
import mhp.oee.api.v1.request.ResourceTypeRequest;
import mhp.oee.api.v1.request.ResrouceRequest;
import mhp.oee.api.v1.request.WorkCenterShiftIn;
import mhp.oee.api.v1.request.WorkCenterShiftRequest;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.web.Constants;

@RestController
public class ErpIntegrationController {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();


    @Autowired
    private SyncItemService syncItemService;
    @Autowired
    private SyncResourceTypeService syncResourceTypeService;
    @Autowired
    private SyncResourceMeUserService syncResourceMeUserService;
    @Autowired
    private SyncWorkCenterShiftService syncWorkCenterShiftService;
    @Autowired
    private SyncFunctionalLocationService syncFunctionalLocationService;
    @Autowired
    private SyncResourceService syncResourceService;


    /**
     * [WEB018] Synchronize Material
     */
    @RequestMapping(value = "/api/v1/sync_material", method = RequestMethod.POST)
    public ResponseEntity<?> syncMaterial(@RequestBody ItemRequest itemRequest) {
        logger.debug("[API.v1][sync_material] request = " + gson.toJson(itemRequest.getItems()));
        try {
            syncItemService.syncItem(itemRequest.getItems());
        } catch (BusinessException e) {
            logger.error("## [API.v1][sync_material] : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< [API.v1][sync_material]");
        }
        return ResponseEntity.ok().body(null);
    }

    /**
     * [WEB019] Synchronize Resource
     */
    @RequestMapping(value = "/api/v1/sync_resource", method = RequestMethod.POST)
    public ResponseEntity<?> syncResource(@RequestBody ResrouceRequest resrouceRequest) {
        logger.debug(">> [API.v1][sync_resource] request = " + gson.toJson(resrouceRequest.getItems()));
        try {
            syncResourceService.syncResource(resrouceRequest.getItems());
        } catch (BusinessException e) {
            logger.error("## [API.v1][sync_resource] : " + e.getMessage() +
                    System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< [API.v1][sync_resource]");
        }
        return ResponseEntity.ok().body(null);
    }

    /**
     * [WEB040] Synchronize Resource Type
     */
    @RequestMapping(value = "/api/v1/sync_resource_type", method = RequestMethod.POST)
    public ResponseEntity<?> syncResourceType(@RequestBody ResourceTypeRequest resourceTypeRequest) {
        logger.debug(">> [API.v1][sync_resource_type] request = " + gson.toJson(resourceTypeRequest.getItems()));
        try {
            syncResourceTypeService.syncResourceType(resourceTypeRequest.getItems());
        } catch (BusinessException e) {
            logger.error("## [API.v1][sync_resource_type] : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< [API.v1][sync_resource_type]");
        }
        return ResponseEntity.ok().body(null);
    }

    /**
     * [WEB051] Synchronize Resource ME User
     */
    @RequestMapping(value = "/api/v1/sync_resource_me_user", method = RequestMethod.POST)
    public ResponseEntity<?> syncResourceMeUser(@RequestBody ResourceMeUserRequest resourceMeUserRequest) {
        logger.debug(">> [API.v1][sync_resource_me_user] request = " + gson.toJson(resourceMeUserRequest.getItems()));
        try {
            syncResourceMeUserService.syncResourceMeUser(resourceMeUserRequest.getItems());
        } catch (BusinessException e) {
            logger.error("## [API.v1][sync_resource_me_user] : " + e.getMessage() + System.lineSeparator()
                + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< [API.v1][sync_resource_me_user]");
        }
        return ResponseEntity.ok().body(null);
    }

    /**
     * [WEB038] Synchronize Work Center and Work Center Shift
     */
    @RequestMapping(value = "/api/v1/sync_work_center_shift", method = RequestMethod.POST)
    public ResponseEntity<?> syncWorkCenterShift(@RequestBody WorkCenterShiftRequest workCenterShiftRequest) {
        logger.debug(">> [API.v1][sync_work_center_shift] request = " + gson.toJson(workCenterShiftRequest.getItems()));
        try {
            syncWorkCenterShiftService.syncWorkCenterShift(workCenterShiftRequest.getItems());
        } catch (BusinessException e) {
            logger.error("## [API.v1][sync_work_center_shift] : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< [API.v1][sync_work_center_shift]");
        }
        return ResponseEntity.ok().body(null);
    }

    /**
     * [WEB049] Synchronize Functional Location
     */
    @RequestMapping(value = "/api/v1/sync_functional_location", method = RequestMethod.POST)
    public ResponseEntity<?> syncFunctionalLocation(@RequestBody FunctionalLocationRequest functionalLocationRequest) {
        logger.debug(">> [API.v1][sync_functional_location] request = " + gson.toJson(functionalLocationRequest.getItems()));
        try {
            syncFunctionalLocationService.syncFunctionalLocation(functionalLocationRequest.getItems());
        } catch (BusinessException e) {
            logger.error("## [API.v1][sync_functional_location] : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< [API.v1][sync_functional_location]");
        }
        return ResponseEntity.ok().body(null);
    }


}
