package mhp.oee.api.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.request.ResourceTypeIn;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.component.plant.ResourceTypeComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceTypeVO;

/**
 * [WEB040] Synchronize Resource Type
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class SyncResourceTypeService {

    @Autowired
    private ResourceTypeComponent resourceTypeComponent;

    public void syncResourceType(List<ResourceTypeIn> resourceTypeList) throws BusinessException {
        for (ResourceTypeIn each : resourceTypeList) {
            ResourceTypeVO vo = Utils.copyObjectProperties(each, ResourceTypeVO.class);
            if (!resourceTypeComponent.existResourceType(vo.getSite(), vo.getResourceType())) {
                resourceTypeComponent.createResourceType(vo);
            } else {
                resourceTypeComponent.updateResourceType(vo);
            }
        }
    }

}
