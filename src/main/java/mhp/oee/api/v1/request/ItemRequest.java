package mhp.oee.api.v1.request;

import java.util.List;

public class ItemRequest {
    private List<ItemIn> items;

    public List<ItemIn> getItems() {
        return items;
    }

    public void setItems(List<ItemIn> items) {
        this.items = items;
    }

}
