package mhp.oee.api.v1.request;

import java.util.List;

public class FunctionalLocationRequest {
    private List<FunctionalLocationIn> items;

    public List<FunctionalLocationIn> getItems() {
        return items;
    }

    public void setItems(List<FunctionalLocationIn> items) {
        this.items = items;
    }

}
