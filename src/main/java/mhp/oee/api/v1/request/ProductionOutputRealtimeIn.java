package mhp.oee.api.v1.request;

import java.math.BigDecimal;

public class ProductionOutputRealtimeIn {

    private String site;
    private String resrce;
    private String item;
    private BigDecimal qtyYield;
    private BigDecimal qtyScrap;
    private BigDecimal qtyInput;
    private BigDecimal qtyTotal;
    private String timestamp;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public BigDecimal getQtyYield() {
        return qtyYield;
    }

    public void setQtyYield(BigDecimal qtyYield) {
        this.qtyYield = qtyYield;
    }

    public BigDecimal getQtyScrap() {
        return qtyScrap;
    }

    public void setQtyScrap(BigDecimal qtyScrap) {
        this.qtyScrap = qtyScrap;
    }

    public BigDecimal getQtyInput() {
        return qtyInput;
    }

    public void setQtyInput(BigDecimal qtyInput) {
        this.qtyInput = qtyInput;
    }

    public BigDecimal getQtyTotal() {
        return qtyTotal;
    }

    public void setQtyTotal(BigDecimal qtyTotal) {
        this.qtyTotal = qtyTotal;
    }

}
