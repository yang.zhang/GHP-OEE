package mhp.oee.api.v1.request;

import java.util.List;

public class WorkCenterShiftRequest {
    private List<WorkCenterShiftIn> items;

    public List<WorkCenterShiftIn> getItems() {
        return items;
    }

    public void setItems(List<WorkCenterShiftIn> items) {
        this.items = items;
    }

}
