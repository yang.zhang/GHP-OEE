package mhp.oee.api.v1.request;

import java.math.BigDecimal;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class RealtimePpmIn {
    private String site;
    private String resrce;
    private BigDecimal realtimePpm;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public BigDecimal getRealtimePpm() {
        return realtimePpm;
    }

    public void setRealtimePpm(BigDecimal realtimePpm) {
        this.realtimePpm = realtimePpm;
    }

}
