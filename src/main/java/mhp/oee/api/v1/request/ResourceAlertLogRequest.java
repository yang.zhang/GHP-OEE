package mhp.oee.api.v1.request;

import java.util.List;

public class ResourceAlertLogRequest {

    private List<ResourceAlertLogIn> items;

    public List<ResourceAlertLogIn> getItems() {
        return items;
    }

    public void setItems(List<ResourceAlertLogIn> items) {
        this.items = items;
    }

}
