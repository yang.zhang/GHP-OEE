package mhp.oee.api.v1.request;

import java.util.List;

public class ResourceTypeRequest {
    private List<ResourceTypeIn> items;

    public List<ResourceTypeIn> getItems() {
        return items;
    }

    public void setItems(List<ResourceTypeIn> items) {
        this.items = items;
    }

}
