package mhp.oee.api.v1.request;

/**
 * Created by DingLJ on 2017/4/17 017.
 */
public class ResourceParamMap {
    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
