package mhp.oee.api.v1.request;

import java.util.List;
import java.util.Map;

/** 设备参数
 * Created by DingLJ on 2017/4/14 014.
 */
public class ResourceParamRequest {

    private String site;

    private String resource_type;
    private String resource;
    private List<Map<String,String>> processesParamKV;

//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private Date uploadDatetime;
    private String uploadDatetime;
    private String timestamp;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResource_type() {
        return resource_type;
    }

    public void setResource_type(String resource_type) {
        this.resource_type = resource_type;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public List<Map<String,String>> getProcessesParamKV() {
        return processesParamKV;
    }

    public void setProcessesParamKV(List<Map<String,String>> processesParamKV) {
        this.processesParamKV = processesParamKV;
    }

    public String getUploadDatetime() {
        return uploadDatetime;
    }

    public void setUploadDatetime(String uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }
}
