package mhp.oee.api.v1.request;

public class ResourceReasonLogIn {

    private String site;
    private String resrce;
    private String reasonCode;
    private String actionCode;
    private String maintenanceNotification;
    private String maintenanceOrder;
    private String meUserId;
    private String plcAddress;
    private String plcValue;
    private String timestamp;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getPlcAddress() {
		return plcAddress;
	}

	public void setPlcAddress(String plcAddress) {
		this.plcAddress = plcAddress;
	}

	public String getPlcValue() {
		return plcValue;
	}

	public void setPlcValue(String plcValue) {
		this.plcValue = plcValue;
	}

	public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getMaintenanceNotification() {
        return maintenanceNotification;
    }

    public void setMaintenanceNotification(String maintenanceNotification) {
        this.maintenanceNotification = maintenanceNotification;
    }

    public String getMaintenanceOrder() {
        return maintenanceOrder;
    }

    public void setMaintenanceOrder(String maintenanceOrder) {
        this.maintenanceOrder = maintenanceOrder;
    }

    public String getMeUserId() {
        return meUserId;
    }

    public void setMeUserId(String meUserId) {
        this.meUserId = meUserId;
    }

}
