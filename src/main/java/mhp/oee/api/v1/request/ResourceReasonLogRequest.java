package mhp.oee.api.v1.request;

import java.util.List;

public class ResourceReasonLogRequest {

    private List<ResourceReasonLogIn> items;

    public List<ResourceReasonLogIn> getItems() {
        return items;
    }

    public void setItems(List<ResourceReasonLogIn> items) {
        this.items = items;
    }

}
