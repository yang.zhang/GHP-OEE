package mhp.oee.api.v1.request;

import java.util.Date;

public class ResourceTimeLogIn {

    private String site;
    private String resrce;
    private String resourceState;
    private String startDateTime;
    private String plcAddress;
    private String plcValue;
    private String timestamp;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public String getResourceState() {
        return resourceState;
    }

    public void setResourceState(String resourceState) {
        this.resourceState = resourceState;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

	public String getPlcAddress() {
		return plcAddress;
	}

	public void setPlcAddress(String plcAddress) {
		this.plcAddress = plcAddress;
	}

	public String getPlcValue() {
		return plcValue;
	}

	public void setPlcValue(String plcValue) {
		this.plcValue = plcValue;
	}

    @Override
    public String toString() {
        return "ResourceTimeLogIn{" +
                "site='" + site + '\'' +
                ", resrce='" + resrce + '\'' +
                ", resourceState='" + resourceState + '\'' +
                ", startDateTime='" + startDateTime + '\'' +
                ", plcAddress='" + plcAddress + '\'' +
                ", plcValue='" + plcValue + '\'' +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }


}
