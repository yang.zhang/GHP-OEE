package mhp.oee.api.v1.request;

import java.util.List;

public class ResourceMeUserRequest {
    private List<ResourceMeUserIn> items;

    public List<ResourceMeUserIn> getItems() {
        return items;
    }

    public void setItems(List<ResourceMeUserIn> items) {
        this.items = items;
    }

}
