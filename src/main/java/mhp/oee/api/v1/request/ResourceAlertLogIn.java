package mhp.oee.api.v1.request;

public class ResourceAlertLogIn {

    private String site;
    private String resrce;
    private String resourceState;
    private String plcAddress;
    private String description;
    private String plcValue;
    private String plcAddressAlert;
    private String plcValueAlert;
    private String timestamp;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getPlcValue() {
		return plcValue;
	}

	public void setPlcValue(String plcValue) {
		this.plcValue = plcValue;
	}

	public String getPlcAddressAlert() {
		return plcAddressAlert;
	}

	public void setPlcAddressAlert(String plcAddressAlert) {
		this.plcAddressAlert = plcAddressAlert;
	}

	public String getPlcValueAlert() {
		return plcValueAlert;
	}

	public void setPlcValueAlert(String plcValueAlert) {
		this.plcValueAlert = plcValueAlert;
	}

	public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public String getResourceState() {
        return resourceState;
    }

    public void setResourceState(String resourceState) {
        this.resourceState = resourceState;
    }

    public String getPlcAddress() {
        return plcAddress;
    }

    public void setPlcAddress(String plcAddress) {
        this.plcAddress = plcAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
