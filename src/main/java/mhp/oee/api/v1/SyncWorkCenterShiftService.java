package mhp.oee.api.v1;

import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import mhp.oee.api.v1.request.WorkCenterShiftIn;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.InvalidInputParamException;
import mhp.oee.common.handle.WorkCenterBOHandle;
import mhp.oee.component.plant.WorkCenterComponent;
import mhp.oee.po.gen.WorkCenterShiftPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.WorkCenterShiftVO;
import mhp.oee.vo.WorkCenterVO;

/**
 * [WEB038] Synchronize Work Center and Work Center Shift
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class SyncWorkCenterShiftService {

    @Autowired
    private WorkCenterComponent workCenterComponent;

    private static Gson gson = new Gson();

    public void syncWorkCenterShift(List<WorkCenterShiftIn> workCenterShiftList) throws BusinessException {

        Set<String> wcRefSet = new HashSet<String>();

        for (WorkCenterShiftIn each : workCenterShiftList) {
            // work center
            WorkCenterVO wcVO = Utils.copyObjectProperties(each, WorkCenterVO.class);
            wcVO.setDescription(each.getWcDescription());
            wcVO.setWcCategory(WorkCenterComponent.WC_CATEGORY_WORK_AREA);
            WorkCenterBOHandle wcHandle = new WorkCenterBOHandle(each.getSite(), each.getWorkCenter());
            if (!workCenterComponent.existWorkCenter(wcHandle.getSite(), wcHandle.getWorkCenter())) {
                workCenterComponent.createWorkCenter(wcVO);
            } else {
                workCenterComponent.updateWorkCenter(wcVO);
            }
            wcRefSet.add(wcHandle.getValue());  // prepare the set of work center

            // work center shift
            WorkCenterShiftVO wcsVO = Utils.copyObjectProperties(each, WorkCenterShiftVO.class);
            wcsVO.setWorkCenterBo(wcHandle.getValue());
            try {
                wcsVO.setStartTime(Utils.strToTime(each.getStartTime()));
                wcsVO.setEndTime(Utils.strToTime(each.getEndTime()));
            } catch (ParseException e) {
                throw new InvalidInputParamException(gson.toJson(each));
            }
            wcsVO.setDescription(each.getShiftDescription());
            if (!workCenterComponent.existWorkCenterShift(each.getSite(), each.getWorkCenter(), each.getShift())) {
                workCenterComponent.createWorkCenterShift(wcsVO);
            } else {
                workCenterComponent.updateWorkCenterShift(wcsVO);
            }
        }

        // flush WORK_CENTER.ENABLED by WORK_CENTER_SHIFT records
        for (String wcRef : wcRefSet) {
            WorkCenterVO vo = workCenterComponent.readWorkCenterByPrimaryKey(wcRef);

            // check WORK_CENTER_SHIFT if all ENABLED = false
            WorkCenterShiftPOExample example = new WorkCenterShiftPOExample();
            example.createCriteria().andWorkCenterBoEqualTo(wcRef).andEnabledEqualTo("true");
            List<WorkCenterShiftVO> vos = workCenterComponent.readWorkCenterShiftByExample(example);
            if (Utils.isEmpty(vos)) {
                // set WORK_CENTER.ENABLED to false
                vo.setEnabled("false");
            } else {
                // set WORK_CENTER.ENABLED to true
                vo.setEnabled("true");
            }
            workCenterComponent.updateWorkCenter(vo);
        }

    }

}
