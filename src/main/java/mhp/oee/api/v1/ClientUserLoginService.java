package mhp.oee.api.v1;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.response.ClientUserLoginOut;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.handle.UserBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.activity.ActivityComponent;
import mhp.oee.component.user.ClientLoginLogComponent;
import mhp.oee.component.user.UserComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.AppServerUserDetailsVO;
import mhp.oee.vo.ClientLoginLogVO;

/**
 * [WEB052] Client User Login
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class ClientUserLoginService {

    @Autowired
    private UserComponent userComponent;
    @Autowired
    private ActivityComponent activityComponent;
    @Autowired
    private ClientLoginLogComponent clientLoginLogComponent;

    @InjectableLogger
    private static Logger logger;

    public ClientUserLoginOut clientUserLogin(String user, String loginMode, String resource) throws BusinessException {
        user = user.toUpperCase();  // force user to be upper case
        // get user details from app server
        AppServerUserDetailsVO appServerUserDetailsVO = userComponent.readAppServerUserDetails(user);
        String defaultSite = appServerUserDetailsVO.getDefaultSite();
        if (Utils.isEmpty(defaultSite)) {
            logger.error("ClientUserLoginService.clientUserLogin() : Default site not found.");
            throw new BusinessException(ErrorCodeEnum.BASIC, "Default site not found.");
        }
        ClientUserLoginOut response = Utils.copyObjectProperties(appServerUserDetailsVO, ClientUserLoginOut.class);

        // get all user groups belong to, under default side
        UserBOHandle userHandle = new UserBOHandle(defaultSite, user);
        List<String> ugRefList = userComponent.readUserGroupRefListByUserHandle(userHandle);
        if (ugRefList.isEmpty()) {
            logger.error("ClientUserLoginService.clientUserLogin() : User Group not found.");
            throw new BusinessException(ErrorCodeEnum.BASIC, "User Group not found.");
        }

        // set allowed client activities
        List<String> acList = activityComponent.readAllowedActivityClientListByUserGroups(ugRefList);
        response.setActivityClientList(acList);

        // update CLIENT_LOGIN_LOG table
        ClientLoginLogVO vo = new ClientLoginLogVO();
        vo.setUserBo(userHandle.getValue());
        // Per spec, no need to check if ResourceBO exists.
        ResrceBOHandle resourceBo = new ResrceBOHandle(defaultSite, resource);
        vo.setResourceBo(resourceBo.getValue());
        vo.setActionCode("LOGIN");  // hard-coded action code
        vo.setUserDepartment(response.getDepartment());
        vo.setLoginMode(loginMode);

        // In case above exception occurs, logging client login will not be triggered.
        clientLoginLogComponent.createClientLoginLog(vo);

        return response;
    }

}
