package mhp.oee.api.v1.heartbeat;

import mhp.oee.utils.Utils;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by LinZuK on 2016/9/12.
 */
public class Heartbeat implements Serializable {
    private String site;
    private String resource;
    private String resourceState;
    private Date heartTime;
    private String version;
    private String remark;


    public Heartbeat(String site, String resource, String resourceState, Date heartTime, String enter, String version) {
        this.site = site;
        this.resource = resource;
        this.resourceState = resourceState;
        this.heartTime = heartTime;
        this.version = version;
        this.remark = enter + ":" + Utils.datetimeMsToStr(heartTime);
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getResourceState() {
        return resourceState;
    }

    public void setResourceState(String resourceState) {
        this.resourceState = resourceState;
    }

    public Date getHeartTime() {
        return heartTime;
    }

    public void setHeartTime(Date heartTime) {
        this.heartTime = heartTime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
