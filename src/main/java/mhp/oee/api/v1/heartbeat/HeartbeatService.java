package mhp.oee.api.v1.heartbeat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.api.v1.PostResourceTimeLogService;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.component.production.ResourceTimeLogComponent;
import mhp.oee.dao.extend.ScheduleJobPOMapper;
import mhp.oee.dao.gen.HeartBeatPOMapper;
import mhp.oee.po.extend.ScheduleJobPO;
import mhp.oee.po.gen.HeartBeatPO;
import mhp.oee.po.gen.HeartBeatPOExample;
import mhp.oee.po.gen.ResourceTimeLogPO;
import mhp.oee.utils.AsyncService;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceTimeLogVO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by LinZuK on 2016/9/11.
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class HeartbeatService {

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private ResourceTimeLogComponent resourceTimeLogComponent;
    @Autowired
    private PostResourceTimeLogService postResourceTimeLogService;
    @Autowired
    private ScheduleJobPOMapper scheduleJobPOMapper;
    @Autowired
    private HeartBeatPOMapper heartBeatPOMapper;
    @Autowired
    private AsyncService heartbeatThreadPool;

    //private AsyncService heartbeatThreadPool = new AsyncService();

    private HeartBeatPO createHearBeatPO(String resourceBo, String site, String resource, Date heartTime, String enter, String version) {
        HeartBeatPO heartBeatPO = new HeartBeatPO();
        heartBeatPO.setResourceBo(resourceBo);
        heartBeatPO.setSite(site);
        heartBeatPO.setResource(resource);
        heartBeatPO.setHeartTime(heartTime);
        heartBeatPO.setVersion(version);
        heartBeatPO.setRemark(enter + ":" + Utils.datetimeMsToStr(heartTime));
        return heartBeatPO;
    }

    public void onHeartbeat(final String site, final String resource, final String version) {
        logger.info("[onHeartbeat] site=" + site + " resource=" + resource + " version=" + version);
        // new heartbeat status
        final String resourceBo = new ResrceBOHandle(site, resource).getValue();
        // thread state into queue. and process by thread pool
        heartbeatThreadPool.executeThread(new Runnable() {
            @Override
            public void run() {
                HeartBeatPO heartBeatPO = createHearBeatPO(resourceBo, site, resource, new Date(), "HEARTBEAT", version);
                if (heartBeatPOMapper.selectByPrimaryKey(resourceBo) == null) {
                    heartBeatPOMapper.insert(heartBeatPO);
                } else {
                    heartBeatPOMapper.updateByPrimaryKey(heartBeatPO);
                }
                logger.info("[onHeartbeat] putDB: key=" + resourceBo);
            }
        });
    }

    // 定时查询哪些设备心跳超时
    public void updateOverTimeDeviceStatus(int overTimeMinute) {
        Date now = new Date();
        // 找到所有心跳超时的设备
        Date limitTime = new Date(now.getTime() - overTimeMinute*60*1000);
        HeartBeatPOExample example = new HeartBeatPOExample();
        example.createCriteria().andHeartTimeLessThanOrEqualTo(limitTime);
        List<HeartBeatPO> overtimePOs = heartBeatPOMapper.selectByExample(example);
        // 将超时的设备，且设备当前处于P状态的设备打上D
        for (HeartBeatPO overtimePO : overtimePOs) {
            ResourceTimeLogPO po = resourceTimeLogComponent.readLastRecord(overtimePO.getResourceBo());
            if (po != null) {
                if ("P".equals(po.getResourceState())) {
                    // 打上D
                    ResourceTimeLogVO vo = postResourceTimeLogService.pkgResourceTimeLogVO(po.getSite(), po.getResourceBo(), now, "D");
                    try {
                        postResourceTimeLogService.modifyResourceTimeLog(now, vo, ContextHelper.getContext().getUser());
                    } catch (BusinessException e) {
                        e.printStackTrace();
                        logger.error("心跳超时打上D的时候报错", e);
                    }
                }
            }
        }
    }

    // 获取心跳超时检测JOB参数
    public ScheduleJobPO getHeartBeatJobParam() {
        List<ScheduleJobPO> pos = scheduleJobPOMapper.selectHeartBeatJob();
        if (pos != null && pos.size() > 0) {
            return pos.get(0);
        } else {
            return null;
        }
    }
}
