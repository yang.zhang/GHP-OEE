package mhp.oee.api.v1.heartbeat;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.job.JobLogComponent;
import mhp.oee.job.productjob.ProductJobService;
import mhp.oee.job.quartz.quartzutils.SpringContextUtil;
import mhp.oee.utils.Utils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;

/**
 * Created by LinZuK on 2016/9/12.
 * 用途：更新心跳超时的设备状态
 */
@Service
public class HeartbeatOverTimeJob implements Job{

    @InjectableLogger
    private static Logger logger;

    private JobLogComponent jobLogComponent;
    private HeartbeatService heartbeatService;

    private static final String JOB_NAME = "HEART_BEAT";

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        jobLogComponent = (JobLogComponent) SpringContextUtil.getBean("jobLogComponent");
        heartbeatService = (HeartbeatService) SpringContextUtil.getBean("heartbeatService");

        execute(10);
    }


    public void execute(int overTimeMinute) {
        try {
            // lock and log start
            logger.info("开始争抢锁：" + JOB_NAME);
            Date startDateTime = new Date(); // 实际开始时间
            Date plannedDateTime = Utils.strToReportDateTime(Utils.reportDateTimeToStr((startDateTime))); // 计划开始时间
            String handle = jobLogComponent.jobStartLog(JOB_NAME, plannedDateTime, startDateTime, Utils.getMonoServer());
            if (null != handle) {
                logger.info("当前实例抢到锁：" + JOB_NAME + " " + Utils.datetimeMsToStr(plannedDateTime));
            } else {
                logger.info("当前实例没有抢到锁：" + JOB_NAME + " " + Utils.datetimeMsToStr(plannedDateTime));
                return;
            }

            // execute
            logger.info("开始执行心跳超时检测作业");
            checkOvertimeRTL(overTimeMinute);
            logger.info("结束执行心跳超时检测作业");

            // log end
            jobLogComponent.jobEndLog(handle);
        } catch (Exception e) {
            logger.info("JOB执行错误");
        }
    }

    public void checkOvertimeRTL(int overTimeMinute) {
//        ScheduleJobPO jobParam = heartbeatService.getHeartBeatJobParam();
//        if (jobParam == null) {
//            logger.info("心跳超时检测JOB的参数没有配置");
//            return;
//        }
//        if (!"true".equalsIgnoreCase(jobParam.getEnabled())) {
//            logger.info("心跳超时检测JOB的处于disable状态");
//            return;
//        }
        logger.info("定时查询哪些设备心跳超时");
        heartbeatService.updateOverTimeDeviceStatus(overTimeMinute);
    }


}
