package mhp.oee.api.v1.heartbeat;

import mhp.oee.utils.Utils;
import mhp.oee.web.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * Created by LinZuK on 2016/9/11.
 */
@RestController
public class HeartbeatController {

    @Autowired
    private HeartbeatService heartbeatService;

    @RequestMapping(value = "/api/v1/heartbeat/site/{site}/resource/{resource}",
            method = RequestMethod.GET,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> searchResource(@PathVariable("site") String site,
                                            @PathVariable("resource") String resource,
                                            @RequestParam(value = "version", required = true) String version) {
        heartbeatService.onHeartbeat(site, resource, version);
        return ResponseEntity.ok(Utils.datetimeMsToStr(new Date())); // 返回系统时间
    }

}
