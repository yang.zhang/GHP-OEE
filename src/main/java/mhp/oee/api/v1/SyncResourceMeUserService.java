package mhp.oee.api.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.api.v1.request.ResourceMeUserIn;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.ResourceMeUserBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.component.plant.ResourceMeUserComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceMeUserVO;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 *
 * [WEB051] Synchronize Resource ME User
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class SyncResourceMeUserService {

    @Autowired
    private ResourceMeUserComponent resourceMeUserComponent;

    public void syncResourceMeUser(List<ResourceMeUserIn> resourceMeUserList) throws BusinessException{
        for (ResourceMeUserIn each : resourceMeUserList) {
            ResourceMeUserVO vo = Utils.copyObjectProperties(each, ResourceMeUserVO.class);
            String resourceRef = new ResrceBOHandle(each.getSite(), each.getResrce()).getValue();
            vo.setResourceBo(resourceRef);
            ResourceMeUserBOHandle handle = new ResourceMeUserBOHandle(resourceRef, each.getMeUserId(), each.getShift());
            if (!resourceMeUserComponent.existResourceMeUser(handle.getValue())) {
                resourceMeUserComponent.createResourceMeUser(vo);
            } else {
                resourceMeUserComponent.updateResourceMeUser(vo);
            }
        }
    }

}
