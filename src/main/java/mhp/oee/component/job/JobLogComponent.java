package mhp.oee.component.job;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.ServerNodeException;
import mhp.oee.common.handle.JobLogBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.dao.gen.JobLogPOMapper;
import mhp.oee.po.gen.JobLogPO;
import mhp.oee.po.gen.JobLogPOExample;
import mhp.oee.utils.Utils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by LinZuK on 2016/9/8.
 */
@Component
public class JobLogComponent {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    JobLogPOMapper jobLogPOMapper;

    // 开始
    public String jobStartLog(String job, Date plannedDateTime, Date startDateTime, String serverNode) throws ServerNodeException {

        if(serverNode == null || serverNode.length() == 0) {
            throw new ServerNodeException();
        }

        try {
            JobLogPO po = new JobLogPO();
            po.setJob(job);
            po.setStrt(Utils.datetimeMsToStr(plannedDateTime));
            po.setPlannedDateTime(plannedDateTime);
            po.setStartDateTime(startDateTime);
            po.setEndDateTime(null);
            po.setServerNode(serverNode);
            String handle = new JobLogBOHandle(po.getJob(), po.getStrt(), po.getServerNode()).getValue();
            po.setHandle(handle);
            
            JobLogPO obj = jobLogPOMapper.selectByPrimaryKey(handle);
            if(obj!=null){
            	return null;
            }
            int ret = jobLogPOMapper.insert(po);
            if (ret == 1) {
                return handle;
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    // 结束
    public void jobEndLog(String handle) {
        JobLogPO po = new JobLogPO();
        po.setHandle(handle);
        po.setEndDateTime(new Date());
        jobLogPOMapper.updateByPrimaryKeySelective(po);
    }

    public void jobEndLog(String handle, int fetchSize) {
        JobLogPO po = new JobLogPO();
        po.setHandle(handle);
        po.setEndDateTime(new Date());
        po.setFetchSize(fetchSize);
        jobLogPOMapper.updateByPrimaryKeySelective(po);
    }

    public JobLogPO getIdleJobLog(String handle) {
        return jobLogPOMapper.selectByPrimaryKey(handle);
    }
}
