package mhp.oee.component.user;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.common.handle.ClientLoginLogBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.ClientLoginLogPOMapper;
import mhp.oee.po.gen.ClientLoginLogPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ClientLoginLogVO;

@Component
public class ClientLoginLogComponent {

    @Autowired
    private ClientLoginLogPOMapper clientLoginLogPOMapper;

    public int createClientLoginLog(ClientLoginLogVO vo) {
        ClientLoginLogPO po = Utils.copyObjectProperties(vo, ClientLoginLogPO.class);

        if (po.getLoginDateTime() == null) {
            po.setLoginDateTime(new Date());
        }
        String strt = Utils.datetimeMsToStr(po.getLoginDateTime());
        po.setStrt(strt);

        ClientLoginLogBOHandle clientLoginLogBOHandle = new ClientLoginLogBOHandle(
                po.getUserBo(), po.getResourceBo(), strt);
        po.setHandle(clientLoginLogBOHandle.getValue());
        po.setModifiedUser(ContextHelper.getContext().getUser());
        return clientLoginLogPOMapper.insert(po);
    }

}
