package mhp.oee.component.user;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ActivityClientPermBOHandle;
import mhp.oee.common.handle.ActivityPermBOHandle;
import mhp.oee.common.handle.UserGroupBOHandle;
import mhp.oee.common.handle.UserGroupMemberBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.ActivityAndActivityGroupAndActivityGroupActivityPOMapper;
import mhp.oee.dao.extend.ActivityClientPermAndActivityClientPOMapper;
import mhp.oee.dao.extend.ActivityPermAndActivityPOMapper;
import mhp.oee.dao.gen.ActivityClientPOMapper;
import mhp.oee.dao.gen.ActivityClientPermPOMapper;
import mhp.oee.dao.gen.ActivityPermPOMapper;
import mhp.oee.dao.gen.UserGroupMemberPOMapper;
import mhp.oee.dao.gen.UserGroupPOMapper;
import mhp.oee.dao.gen.UserPOMapper;
import mhp.oee.po.extend.ActivityAndActivityGroupAndActivityGroupActivityPO;
import mhp.oee.po.extend.ActivityClientPermAndActivityClientPO;
import mhp.oee.po.extend.ActivityPermAndActivityPO;
import mhp.oee.po.gen.ActivityClientPO;
import mhp.oee.po.gen.ActivityClientPOExample;
import mhp.oee.po.gen.ActivityClientPermPO;
import mhp.oee.po.gen.ActivityClientPermPOExample;
import mhp.oee.po.gen.ActivityPermPO;
import mhp.oee.po.gen.ActivityPermPOExample;
import mhp.oee.po.gen.PlcObjectPO;
import mhp.oee.po.gen.UserGroupMemberPO;
import mhp.oee.po.gen.UserGroupMemberPOExample;
import mhp.oee.po.gen.UserGroupPO;
import mhp.oee.po.gen.UserGroupPOExample;
import mhp.oee.po.gen.UserPO;
import mhp.oee.po.gen.UserPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ActivityClientPermAndActivityClientVO;
import mhp.oee.vo.ActivityClientVO;
import mhp.oee.vo.ActivityPermAndActivityVO;
import mhp.oee.vo.UserGroupMemberVO;
import mhp.oee.vo.UserGroupVO;

@Component
public class UserGroupComponent {

    @InjectableLogger
    private static Logger logger;
    private static String USER_GROUP = "USER_GROUP";
    private static String USER_GROUP_MEMBER = "USER_GROUP_MEMBER";
    private static String ACTIVITY_PERM = "ACTIVITY_PERM";
    private static String ACTIVITY_CLIENT_PERM = "ACTIVITY_CLIENT_PERM";

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    private UserGroupPOMapper userGroupPOMapper;

    @Autowired
    private UserPOMapper userPOMapper;

    @Autowired
    private UserGroupMemberPOMapper userGroupMemberPOMapper;

    @Autowired
    private ActivityPermAndActivityPOMapper  activityPermAndActivityPOMapper;

    @Autowired
    private ActivityClientPermAndActivityClientPOMapper activityClientPermAndActivityClientPOMapper;

    @Autowired
    private ActivityClientPOMapper activityClientPOMapper;

    @Autowired
    private ActivityPermPOMapper activityPermPOMapper;

    @Autowired
    private ActivityClientPermPOMapper activityClientPermPOMapper;

    @Autowired
    private ActivityAndActivityGroupAndActivityGroupActivityPOMapper activityAndActivityGroupAndActivityGroupActivityPOMapper;

    public List<UserGroupPO> getUserGroups(String site) {

        UserGroupPOExample example = new UserGroupPOExample();
        example.createCriteria().andSiteEqualTo(site);

        return userGroupPOMapper.selectByExample(example);
    }

    public List<UserGroupPO> getUserGroupsLikeGroup(String site, String userGroup) {
        UserGroupPOExample example = new UserGroupPOExample();
        if (!Utils.isEmpty(userGroup)) {
            example.createCriteria().andSiteEqualTo(site).andUserGroupLike("%" + userGroup + "%");
        } else {
            example.createCriteria().andSiteEqualTo(site);
        }
        return userGroupPOMapper.selectByExample(example);
    }

    public List<UserGroupMemberPO> getUserGroupsByUser(String userBo) {
        UserGroupMemberPOExample example = new UserGroupMemberPOExample();
        example.createCriteria().andUserBoEqualTo(userBo);
        return userGroupMemberPOMapper.selectByExample(example);
    }

    public List<UserGroupPO> getUserGroupsByGroup(String site, String userGroup) {
        UserGroupPOExample example = new UserGroupPOExample();
        example.createCriteria().andSiteEqualTo(site).andUserGroupEqualTo(userGroup);
        return userGroupPOMapper.selectByExample(example);
    }

    public List<UserPO> getAllUser(String site) {
        UserPOExample example = new UserPOExample();
        example.createCriteria().andSiteEqualTo(site);
        return userPOMapper.selectByExample(example);
    }

    public List<UserGroupMemberPO> getUserByUserGroup(String site, String userGroup) {
        UserGroupBOHandle handle = new UserGroupBOHandle(site, userGroup);
        UserGroupMemberPOExample example = new UserGroupMemberPOExample();
        example.createCriteria().andUserGroupBoEqualTo(handle.getValue());
        return userGroupMemberPOMapper.selectByExample(example);
    }

    public UserGroupPO existUserGroup(String site, String userGroup) {
        String handle = new UserGroupBOHandle(site, userGroup).getValue();
        UserGroupPO po = userGroupPOMapper.selectByPrimaryKey(handle);
        return po;
    }

    public String createUserGroup(UserGroupVO vo) throws ExistingRecordException {
        String handle = new UserGroupBOHandle(vo.getSite(), vo.getUserGroup()).getValue();
        UserGroupPO existingPO = userGroupPOMapper.selectByPrimaryKey(handle);
        if (existingPO != null) {
            logger.error("UserGroupComponent.createUserGroup() : Existing USER_GROUP record");
            throw new ExistingRecordException(USER_GROUP, gson.toJson(existingPO));
        }
        UserGroupPO po = Utils.copyObjectProperties(vo, UserGroupPO.class);
        po.setHandle(handle); // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = userGroupPOMapper.insert(po);
        if (ret != 1) {
            logger.error("UserGroupComponent.createUserGroup() : Existing USER_GROUP record");
            existingPO = userGroupPOMapper.selectByPrimaryKey(handle);
            throw new ExistingRecordException(USER_GROUP, gson.toJson(existingPO));
        }
        return handle;
    }

    public void updateUserGroup(UserGroupVO vo) throws UpdateErrorException {
        UserGroupPOExample example = new UserGroupPOExample();
        example.createCriteria().andHandleEqualTo(vo.getHandle()).andModifiedDateTimeEqualTo(vo.getModifiedDateTime());
        UserGroupPO po = Utils.copyObjectProperties(vo, UserGroupPO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = userGroupPOMapper.updateByExample(po, example);
        if (ret != 1) {
            logger.error("UserGroupComponent.updateUserGroup() : updateUserGroup");
            throw new UpdateErrorException(USER_GROUP, gson.toJson(po));
        }

    }

    public String createUserGroupMember(UserGroupMemberVO vo) throws ExistingRecordException {
        String handle = new UserGroupMemberBOHandle(vo.getUserGroupBo(), vo.getUserBo()).getValue();
        UserGroupMemberPO existingPO = userGroupMemberPOMapper.selectByPrimaryKey(handle);
        if (existingPO != null) {
            logger.error("UserGroupComponent.createUserGroupMember() : Existing USER_GROUP_MEMBER record ");
            throw new ExistingRecordException(USER_GROUP_MEMBER, gson.toJson(existingPO));
        }
        UserGroupMemberPO po = Utils.copyObjectProperties(vo, UserGroupMemberPO.class);
        po.setHandle(handle); // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = userGroupMemberPOMapper.insert(po);
        if (ret != 1) {
            logger.error("UserGroupComponent.createUserGroupMember() : Existing USER_GROUP_MEMBER record ");
            existingPO = userGroupMemberPOMapper.selectByPrimaryKey(handle);
            throw new ExistingRecordException(USER_GROUP_MEMBER, gson.toJson(existingPO));
        }
        return handle;
    }

    public void createUserGroupMemberByPO(UserGroupMemberPO userGroupMemberPO) throws ExistingRecordException {
        UserGroupMemberPO existingPO = userGroupMemberPOMapper.selectByPrimaryKey(userGroupMemberPO.getHandle());
        if (existingPO != null) {
            throw new ExistingRecordException("USER_GROUP_MEMBER", gson.toJson(existingPO));
        }
        userGroupMemberPO.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = userGroupMemberPOMapper.insert(userGroupMemberPO);
        if (ret != 1) {
            logger.error("USER_GROUP_MEMBER" + " : Existing record: "+gson.toJson(userGroupMemberPO));
            throw new ExistingRecordException("USER_GROUP_MEMBER", gson.toJson(userGroupMemberPO));
        }
    }

    public int deleteUserGroupMember(UserGroupMemberVO vo) throws BusinessException {
        List<UserGroupMemberPO> vGroupMemberPOs = getUserGroupsByUser(vo.getUserBo());
        if (vGroupMemberPOs != null && vGroupMemberPOs.size() == 1) {
            if (vGroupMemberPOs.get(0).getUserGroupBo().equals(vo.getUserGroupBo())) {
                userPOMapper.deleteByPrimaryKey(vo.getUserBo());
            }
        }
        String handle = new UserGroupMemberBOHandle(vo.getUserGroupBo(), vo.getUserBo()).getValue();
        return userGroupMemberPOMapper.deleteByPrimaryKey(handle);
    }

    public ActivityPermAndActivityVO getActivityPermByUserGroup(String userGroupBO, String activityBO){
        ActivityPermAndActivityPO  pos = activityPermAndActivityPOMapper.selectActivityPermAndActivity(userGroupBO, activityBO);
        return Utils.copyObjectProperties(pos, ActivityPermAndActivityVO.class);
    }

    public List<ActivityClientPermAndActivityClientVO> getActivityClientPermByUserGroup(String userGroupBO){
        List<ActivityClientPermAndActivityClientPO>  pos = activityClientPermAndActivityClientPOMapper.selectActivityClientPermAndActivityClientPO(userGroupBO);
        return Utils.copyListProperties(pos, ActivityClientPermAndActivityClientVO.class);
    }

    public List<ActivityClientVO> getAllActivityClient(){
        ActivityClientPOExample example = new ActivityClientPOExample();
        List<ActivityClientPO> pos = activityClientPOMapper.selectByExample(example);
        return Utils.copyListProperties(pos, ActivityClientVO.class);
    }

    public String createActivityPerm(ActivityPermAndActivityVO vo) throws ExistingRecordException {
        String handle = new ActivityPermBOHandle(vo.getUserGroupBo(), vo.getActivityBo()).getValue();
        ActivityPermPO existingPO = activityPermPOMapper.selectByPrimaryKey(handle);
        if (existingPO != null) {
            logger.error("UserGroupComponent.createActivityPerm() : Existing ACTIVITY_PERM record ");
            throw new ExistingRecordException(ACTIVITY_PERM, gson.toJson(existingPO));
        }
        ActivityPermPO po = Utils.copyObjectProperties(vo, ActivityPermPO.class);
        po.setHandle(handle); // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = activityPermPOMapper.insert(po);
        if (ret != 1) {
            logger.error("UserGroupComponent.createActivityPerm() : Existing ACTIVITY_PERM record ");
            existingPO = activityPermPOMapper.selectByPrimaryKey(handle);
            throw new ExistingRecordException(ACTIVITY_PERM, gson.toJson(existingPO));
        }
        return handle;
    }

    public void updateActivityPerm(ActivityPermAndActivityVO vo) throws UpdateErrorException {
        ActivityPermPOExample example = new ActivityPermPOExample();
        example.createCriteria().andHandleEqualTo(vo.getHandle()).andModifiedDateTimeEqualTo(vo.getModifiedDateTime());
        ActivityPermPO po = Utils.copyObjectProperties(vo, ActivityPermPO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = activityPermPOMapper.updateByExample(po, example);
        if (ret != 1) {
            logger.error("UserGroupComponent.updateActivityPerm() : Existing ACTIVITY_PERM record ");
            throw new UpdateErrorException(ACTIVITY_PERM, gson.toJson(po));
        }
    }
    
    public ActivityClientPermPO exsitActivityClientPermPOMapper(ActivityClientPermAndActivityClientVO vo){
        String handle = new ActivityClientPermBOHandle(vo.getUserGroupBo(), vo.getActivityClientBo()).getValue();
        ActivityClientPermPO existingPO = activityClientPermPOMapper.selectByPrimaryKey(handle);
        return existingPO;
    }

    public String createActivityClientPerm(ActivityClientPermAndActivityClientVO vo) throws ExistingRecordException {
        String handle = new ActivityClientPermBOHandle(vo.getUserGroupBo(), vo.getActivityClientBo()).getValue();
        ActivityClientPermPO po = Utils.copyObjectProperties(vo, ActivityClientPermPO.class);
        po.setHandle(handle); // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = activityClientPermPOMapper.insert(po);
        if (ret != 1) {
            logger.error("UserGroupComponent.createActivityClientPerm() : Existing ACTIVITY_CLIENT_PERM record ");
            ActivityClientPermPO existingPO = activityClientPermPOMapper.selectByPrimaryKey(handle);
            throw new ExistingRecordException(ACTIVITY_CLIENT_PERM, gson.toJson(existingPO));
        }
        return handle;
    }

    public void updateActivityClientPerm(ActivityClientPermAndActivityClientVO vo) throws UpdateErrorException {
        ActivityClientPermPOExample example = new ActivityClientPermPOExample();
        example.createCriteria().andHandleEqualTo(vo.getHandle()).andModifiedDateTimeEqualTo(vo.getModifiedDateTime());
        ActivityClientPermPO po = Utils.copyObjectProperties(vo, ActivityClientPermPO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = activityClientPermPOMapper.updateByExample(po, example);
        if (ret != 1) {
            logger.error("UserGroupComponent.updateActivityClientPerm() : updateActivityClientPerm");
            throw new UpdateErrorException(ACTIVITY_CLIENT_PERM, gson.toJson(po));
        }
    }

    public List<ActivityAndActivityGroupAndActivityGroupActivityPO>  getAllActivityGroupAndActivity(){
        return activityAndActivityGroupAndActivityGroupActivityPOMapper.getAllActivityGroupAndActivity();
    }

}
