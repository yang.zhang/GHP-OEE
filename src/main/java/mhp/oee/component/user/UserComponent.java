package mhp.oee.component.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.appserver.netweaver.NetWeaverUserProvider;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.handle.UserBOHandle;
import mhp.oee.common.handle.UserGroupMemberBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.config.AppServerEnvironment;
import mhp.oee.dao.gen.UserGroupMemberPOMapper;
import mhp.oee.dao.gen.UserGroupPOMapper;
import mhp.oee.dao.gen.UserPOMapper;
import mhp.oee.po.gen.UserGroupMemberPO;
import mhp.oee.po.gen.UserGroupMemberPOExample;
import mhp.oee.po.gen.UserGroupPO;
import mhp.oee.po.gen.UserPO;
import mhp.oee.po.gen.UserPOExample;
import mhp.oee.po.gen.UserPOExample.Criteria;
import mhp.oee.vo.AppServerUserDetailsVO;

@Component
public class UserComponent {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private UserPOMapper userPOMapper;
    @Autowired
    private UserGroupPOMapper userGroupPOMapper;
    @Autowired
    private UserGroupMemberPOMapper userGroupMemberPOMapper;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    //==========================================================================
    // App Server Part
    //==========================================================================

    public AppServerUserDetailsVO readAppServerUserDetails(String user) {
        AppServerUserDetailsVO result = new AppServerUserDetailsVO();
        if (AppServerEnvironment.getAppServerType() == AppServerEnvironment.AppServerType.NETWEAVER) {
            result = NetWeaverUserProvider.readUserDetails(user);
        } else {
            // dummy data for development purpose
            result = AppServerUserDetailsVO.getDefaultVO();
        }
        return result;
    }

    public boolean readAppServerUserDetailsWithoutDummy(String user) {
        if (AppServerEnvironment.getAppServerType() == AppServerEnvironment.AppServerType.NETWEAVER) {
            return NetWeaverUserProvider.readUserDetailsBySearchUser(user);
        }
        return false;
    }


    //==========================================================================
    // User_Group, User_Group_Member
    //==========================================================================

    public List<String> readUserGroupRefListByUserHandle(UserBOHandle userHandle) {

        UserGroupMemberPOExample example = new UserGroupMemberPOExample();
        example.createCriteria().andUserBoEqualTo(userHandle.getValue());
        List<UserGroupMemberPO> ugmList = userGroupMemberPOMapper.selectByExample(example);
        List<String> result = new ArrayList<String>(ugmList.size());
        for (UserGroupMemberPO each : ugmList) {
            result.add(each.getUserGroupBo());
        }
        return result;
    }


    public boolean existUser(String handle) {
        UserPO po = userPOMapper.selectByPrimaryKey(handle);
        return (po != null) ? true : false;
    }

    public UserPO getUserDetails(String site, String user) {
        UserBOHandle handle = (new UserBOHandle(site, user));
        return userPOMapper.selectByPrimaryKey(handle.getValue());
    }


    public void deleteOriginUserGroups(List<UserGroupMemberPO> list) throws BusinessException {
        for (UserGroupMemberPO po : list) {
            UserGroupMemberPOExample example = new UserGroupMemberPOExample();
            example.createCriteria().andUserBoEqualTo(po.getUserBo())
                                    .andUserGroupBoEqualTo(po.getUserGroupBo())
                                    .andCreatedDateTimeEqualTo(po.getCreatedDateTime());

            int ret = userGroupMemberPOMapper.deleteByExample(example);
            if (ret != 1) {
                // TODO: change to dedicated BusinessException
                throw new BusinessException(ErrorCodeEnum.BASIC, "Deleted Record was not found in the Database." + gson.toJson(po));
            }

        }
    }


    public List<UserGroupMemberPO> getUserGroupMemberPOList(String site, String user) {
        UserGroupMemberPOExample example = new UserGroupMemberPOExample();
        example.createCriteria().andUserBoEqualTo(new UserBOHandle(site, user).getValue());
        return userGroupMemberPOMapper.selectByExample(example);
    }


    public void createUserGroupMember(UserPO user, List<UserGroupPO> updatedUserGroup) throws BusinessException {
        for(UserGroupPO userGroupPo : updatedUserGroup) {
            UserGroupMemberBOHandle handle = new UserGroupMemberBOHandle(userGroupPo.getHandle(), user.getHandle());
            UserGroupMemberPO po = new UserGroupMemberPO();
            po.setUserBo(user.getHandle());
            po.setUserGroupBo(userGroupPo.getHandle());
            po.setHandle(handle.getValue());
            po.setModifiedUser(ContextHelper.getContext().getUser());
            int ret = userGroupMemberPOMapper.insert(po);
            if (ret != 1) {
                // TODO: change to dedicated BusinessException
                throw new BusinessException(ErrorCodeEnum.BASIC, "Insert Record was already found in the Database." + gson.toJson(po));
            }
        }
    }


    public void createUserDetails(UserPO user) throws BusinessException {
        UserBOHandle handle = new UserBOHandle(user.getSite(), user.getUserId());
        user.setHandle(handle.getValue());
        user.setModifiedUser(ContextHelper.getContext().getUser());
        user.setEnabledModifiedDateTime(new Date());
        int ret = userPOMapper.insert(user);
        if (ret != 1) {
            UserPO existingPO = userPOMapper.selectByPrimaryKey(handle.getValue());
            // TODO: change to dedicated BusinessException
            throw new BusinessException(ErrorCodeEnum.BASIC, "Existing USER record : " + gson.toJson(existingPO));
        }
    }


    public void createUserDetailsByPO(UserPO userPO) throws ExistingRecordException {
        UserPO existingPO = userPOMapper.selectByPrimaryKey(userPO.getHandle());
        if (existingPO != null) {
            throw new ExistingRecordException("USER", gson.toJson(existingPO));
        }
        userPO.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = userPOMapper.insert(userPO);
        if (ret != 1) {
            logger.error("USER" + " : Existing record: "+gson.toJson(userPO));
            throw new ExistingRecordException("USER", gson.toJson(userPO));
        }
    }


    public void updateUserDetails(UserPO user) throws BusinessException {
        user.setEnabledModifiedDateTime(new Date());
        user.setModifiedUser(ContextHelper.getContext().getUser());

        UserPOExample example = new UserPOExample();
        example.createCriteria().andHandleEqualTo(user.getHandle()).andModifiedDateTimeEqualTo(user.getModifiedDateTime());
        int ret = userPOMapper.updateByExample(user, example);
        if (ret != 1) {
            // TODO: change to dedicated BusinessException
            throw new BusinessException(ErrorCodeEnum.BASIC, "update user details: " + gson.toJson(user));
        }
    }


    public List<UserPO> getSiteList(String userId) {
        UserPOExample example = new UserPOExample();
        example.createCriteria().andUserIdEqualTo(userId);
        return userPOMapper.selectByExample(example);
    }

	public List<UserPO> getAllPrd(String site) {
		UserPOExample example = new UserPOExample();
		Criteria criteria = example.createCriteria();
		criteria.andEnabledEqualTo("true");
		criteria.andSiteEqualTo("1030");
		return userPOMapper.selectByExample(example);
	}

}
