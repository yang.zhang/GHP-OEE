package mhp.oee.component.user;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.dao.extend.ActivityPermAndActivityAndUserAndUserGroupAndUserGroupMemberPOMapper;
import mhp.oee.po.extend.ActivityPermAndActivityAndUserAndUserGroupAndUserGroupMemberPO;

@Component
public class PermissionComponent {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private ActivityPermAndActivityAndUserAndUserGroupAndUserGroupMemberPOMapper mapper;

    public ActivityPermAndActivityAndUserAndUserGroupAndUserGroupMemberPO getPermissionRecord(String site, String user,
            String activity, String permission) throws ServletException {
        logger.debug("site:user:activity:permission>>>" + site + " " + user + " " + activity + " " + permission);
        if (site == null || activity == null ||
            site.trim().length() == 0 || activity.trim().length() == 0) {
            throw new ServletException();
        }
        return mapper.selectActivityPermByUsr(site, user, activity, permission);
    }

}
