package mhp.oee.component.activity;

import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.DeleteErrorException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ActivityBOHandle;
import mhp.oee.common.handle.ActivityParamBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.ActivityParamPOMapper;
import mhp.oee.dao.gen.ActivityParamValuePOMapper;
import mhp.oee.dao.gen.ReasonCodeRuleParamPOMapper;
import mhp.oee.po.gen.ActivityParamPO;
import mhp.oee.po.gen.ActivityParamPOExample;
import mhp.oee.po.gen.ActivityParamValuePO;
import mhp.oee.po.gen.ActivityParamValuePOExample;
import mhp.oee.po.gen.ReasonCodeRuleParamPO;
import mhp.oee.po.gen.ReasonCodeRuleParamPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ActivityParamVO;

@Component
public class ActivityParamComponent {

    private static final String ACTIVITY_PARAM = "ACTIVITY_PARAM";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    
    @Autowired
    private ActivityParamPOMapper activityParamPOMapper;
    @Autowired
    private ActivityParamValuePOMapper activityParamValuePOMapper;
    @Autowired
    private ReasonCodeRuleParamPOMapper reasonCodeRuleParamPOMapper;
    
    public List<ActivityParamVO> getActivityParam(String activityBOHandle) {
        ActivityParamPOExample example = new ActivityParamPOExample();
        example.createCriteria().andActivityBoEqualTo(activityBOHandle);
        List<ActivityParamVO> vos = new LinkedList<ActivityParamVO>();
        List<ActivityParamPO> pos = activityParamPOMapper.selectByExample(example);
        for (ActivityParamPO po : pos) {
            ActivityParamVO vo = Utils.copyObjectProperties(po, ActivityParamVO.class);
            vos.add(vo);
        }
        return vos;
    }

    public void createActivityParam(ActivityParamVO vo) throws ExistingRecordException, UpdateErrorException  {
        ActivityParamPO po = Utils.copyObjectProperties(vo, ActivityParamPO.class);
        String activityBOHandle=new ActivityBOHandle(vo.getActivity()).getValue();
        String activityParamBOHandle=new ActivityParamBOHandle(activityBOHandle, vo.getParamId()).getValue();
        if(activityParamPOMapper.selectByPrimaryKey(activityParamBOHandle)!=null){
            throw new ExistingRecordException(ACTIVITY_PARAM, gson.toJson(po));
        }
        po.setActivityBo(activityBOHandle);
        po.setHandle(activityParamBOHandle);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = activityParamPOMapper.insert(po);
        if (ret != 1) {
            throw new UpdateErrorException(ACTIVITY_PARAM, gson.toJson(po));
        }
    }

    public void updateActivityParam(ActivityParamVO vo) throws UpdateErrorException  {
        String activityBOHandle=new ActivityBOHandle(vo.getActivity()).getValue();
        String activityParamBOHandle=new ActivityParamBOHandle(activityBOHandle, vo.getParamId()).getValue();
        ActivityParamPO po = Utils.copyObjectProperties(vo, ActivityParamPO.class);
        po.setActivityBo(activityBOHandle);
        po.setHandle(activityParamBOHandle);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        ActivityParamPOExample example = new ActivityParamPOExample();
        example.createCriteria().andHandleEqualTo(activityParamBOHandle).andModifiedDateTimeEqualTo(vo.getFirstModifiedDateTime());
        int ret = activityParamPOMapper.updateByExample(po, example);
        if(ret!=1){
            throw new UpdateErrorException(ACTIVITY_PARAM, gson.toJson(po));
        }
    }
    public void deleteActivityParam(ActivityParamVO vo) throws DeleteErrorException {
        String activityBOHandle=new ActivityBOHandle(vo.getActivity()).getValue();
        String activityParamBOHandle=new ActivityParamBOHandle(activityBOHandle, vo.getParamId()).getValue();
        int ret= activityParamPOMapper.deleteByPrimaryKey(activityParamBOHandle);
        if(ret!=1){
            throw new DeleteErrorException(ACTIVITY_PARAM, gson.toJson(vo));
        }
    }

    public List<ActivityParamPO> getActivityParamByActivityBO(String activityBO) {
        ActivityParamPOExample example = new ActivityParamPOExample();
        example.createCriteria().andActivityBoEqualTo(activityBO);
        return activityParamPOMapper.selectByExample(example);
    }

    public Boolean idParamUsed(String paramID) {
        ActivityParamValuePOExample example = new ActivityParamValuePOExample();
        example.createCriteria().andParamIdEqualTo(paramID);
        List<ActivityParamValuePO> activityParamValuePOs = activityParamValuePOMapper.selectByExample(example);
        ReasonCodeRuleParamPOExample example2 = new ReasonCodeRuleParamPOExample();
        example2.createCriteria().andParamIdEqualTo(paramID);
        List<ReasonCodeRuleParamPO> reasonCodeRuleParamPOs = reasonCodeRuleParamPOMapper.selectByExample(example2);
        if(activityParamValuePOs.size()>0||reasonCodeRuleParamPOs.size()>0){
            return true;
        }else{
            return false;
        }
    }

}
