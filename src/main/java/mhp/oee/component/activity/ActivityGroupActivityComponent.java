package mhp.oee.component.activity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.CreateErrorException;
import mhp.oee.common.exception.DeleteErrorException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.handle.ActivityGroupActivityBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.ActivityGroupActivityPOMapper;
import mhp.oee.po.gen.ActivityGroupActivityPO;

@Component
public class ActivityGroupActivityComponent {

    private static final String ACTIVITY_GROUP_ACTIVITY = "ACTIVITY_GROUP_ACTIVITY";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    @Autowired
    private ActivityGroupActivityPOMapper activityGroupActivityPOMapper;

    public void deleteActivityGroupActivity(String handle) throws DeleteErrorException  {
        int ret = activityGroupActivityPOMapper.deleteByPrimaryKey(handle);
        if (ret != 1) {
            throw new DeleteErrorException(ACTIVITY_GROUP_ACTIVITY, handle);
        }
    }

    public boolean existActivityGroupActivity(String handle) {
        ActivityGroupActivityPO po = activityGroupActivityPOMapper.selectByPrimaryKey(handle);
        return (po != null) ? true : false;
    }

    public void createActivityGroupActivity(String activityGroupBOHandle, String activityBOHandle) throws CreateErrorException {
        String handle = new ActivityGroupActivityBOHandle(activityGroupBOHandle, activityBOHandle).getValue();
        ActivityGroupActivityPO po = new ActivityGroupActivityPO();
        po.setHandle(handle);
        po.setActivityGroupBo(activityGroupBOHandle);
        po.setActivityBo(activityBOHandle);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = activityGroupActivityPOMapper.insert(po);
        if (ret != 1) {
            throw new CreateErrorException(ACTIVITY_GROUP_ACTIVITY, gson.toJson(po));
        }
    }

    /*
     * public void deleteActivity(String handle) throws BusinessException { int
     * ret = activityGroupActivityPOMapper.deleteByPrimaryKey(handle); if (ret
     * != 1) { throw new DeleteErrorException(ACTIVITY_GROUP_ACTIVITY,handle); }
     * } public void createActivity(String activityBOHandle, String
     * activityGroupBOHandle) throws BusinessException { String handle = new
     * ActivityGroupActivityBOHandle(activityGroupBOHandle,
     * activityBOHandle).getValue(); ActivityGroupActivityPO po = new
     * ActivityGroupActivityPO(); po.setHandle(handle);
     * po.setActivityGroupBo(activityGroupBOHandle);
     * po.setActivityBo(activityBOHandle);
     * po.setModifiedUser(ContextHelper.getContext().getUser()); int ret =
     * activityGroupActivityPOMapper.insert(po); if (ret != 1) { throw new
     * BusinessException(ErrorCodeEnum.BASIC, "createActivity"); } }
     */

}
