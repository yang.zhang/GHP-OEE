package mhp.oee.component.activity;

import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.CreateErrorException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ActivityClientBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.ActivityClientPOMapper;
import mhp.oee.po.gen.ActivityClientPO;
import mhp.oee.po.gen.ActivityClientPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ActivityClientVO;

@Component
public class ActivityClientComponent {
    @InjectableLogger
    private static Logger logger;

    private static final String ACTIVITY_CLIENT = "ACTIVITY_CLIENT";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    private ActivityClientPOMapper activityClientPOMapper;

    public List<ActivityClientVO> getActivityClientForInputAssist(String activityClient) {
        List<ActivityClientVO> vos = new LinkedList<ActivityClientVO>();
        List<ActivityClientPO> pos = new LinkedList<ActivityClientPO>();
        ActivityClientPOExample example = new ActivityClientPOExample();
        example.createCriteria().andActivityClientLike("%" + activityClient + "%");
        example.setOrderByClause("HANDLE");
        pos = activityClientPOMapper.selectByExample(example);
        for (ActivityClientPO po : pos) {
            ActivityClientVO vo = Utils.copyObjectProperties(po, ActivityClientVO.class);
            vos.add(vo);
        }
        return vos;
    }

    public ActivityClientVO getActivityClientByPrimaryKey(String handle) {
        ActivityClientPO po = activityClientPOMapper.selectByPrimaryKey(handle);
        ActivityClientVO vo = Utils.copyObjectProperties(po, ActivityClientVO.class);
        return vo;
    }

    public void createActivityClient(ActivityClientVO activityClientVO) throws ExistingRecordException, CreateErrorException {
        String handle = new ActivityClientBOHandle(activityClientVO.getActivityClient()).getValue();
        ActivityClientPO po = Utils.copyObjectProperties(activityClientVO, ActivityClientPO.class);
        po.setHandle(handle);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        if (activityClientPOMapper.selectByPrimaryKey(handle) != null) {
            throw new ExistingRecordException(ACTIVITY_CLIENT, gson.toJson(po));
        }
        int ret = activityClientPOMapper.insert(po);
        if (ret != 1) {
            throw new CreateErrorException(ACTIVITY_CLIENT, gson.toJson(po));
        }
    }

    public void updateActivityClient(ActivityClientVO activityClientVO) throws UpdateErrorException {
        String handle = new ActivityClientBOHandle(activityClientVO.getActivityClient()).getValue();
        ActivityClientPOExample example = new ActivityClientPOExample();
        example.createCriteria().andHandleEqualTo(handle).andModifiedDateTimeEqualTo(activityClientVO.getFirstModifiedDateTime());
        ActivityClientPO po = Utils.copyObjectProperties(activityClientVO, ActivityClientPO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = activityClientPOMapper.updateByExample(po, example);
        if (ret != 1) {
            throw new UpdateErrorException(ACTIVITY_CLIENT, gson.toJson(po));
        }
    }
}
