package mhp.oee.component.activity;

import java.util.List;

import mhp.oee.dao.gen.ActivityPOMapper;
import mhp.oee.po.gen.ActivityPO;
import mhp.oee.po.gen.ActivityPOExample;
import mhp.oee.web.angular.activity.management.ActivityManageVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.handle.ActivityParamValueBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.ActivityAndActivityParamAndActivityParamValuePOMapper;
import mhp.oee.dao.gen.ActivityParamValuePOMapper;
import mhp.oee.po.extend.ActivityAndActivityParamAndActivityParamValuePO;
import mhp.oee.po.gen.ActivityParamValuePO;
import mhp.oee.po.gen.ActivityParamValuePOExample;
import mhp.oee.utils.Utils;

/**
 * Created by LinZuK on 2016/7/25.
 */
@Component
public class ActivityParamValueComponent {

    @Autowired
    private ActivityParamValuePOMapper activityParamValuePOMapper;
    @Autowired
    private ActivityAndActivityParamAndActivityParamValuePOMapper activityAndActivityParamAndActivityParamValuePOMapper;
    @Autowired
    private ActivityPOMapper activityPOMapper;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();


    public List<ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO> readActivityParams(String site, String activity) {
        List<ActivityAndActivityParamAndActivityParamValuePO> pos = activityAndActivityParamAndActivityParamValuePOMapper.select(site, activity);
        return Utils.copyListProperties(pos, ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO.class);
    }


    public void updateParamValueObject(String site, ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO vo) throws BusinessException {
        ActivityParamValuePO po = Utils.copyObjectProperties(vo, ActivityParamValuePO.class);
        po.setHandle(vo.getActivityParamValueHandle());
        po.setSite(site);
        po.setModifiedUser(ContextHelper.getContext().getUser());

        if (StringUtils.isBlank(po.getParamValue())) { // 如果参数为空，就删除这个记录
            activityParamValuePOMapper.deleteByPrimaryKey(po.getHandle());
        } else { // 如果参数不为空，就更新这个记录
            ActivityParamValuePOExample example = new ActivityParamValuePOExample();
            example.createCriteria().andHandleEqualTo(vo.getActivityParamValueHandle())
                    .andModifiedDateTimeEqualTo(vo.getActivityParamValueModifiedDateTime());
            int ret = activityParamValuePOMapper.updateByExample(po, example);
            if (ret != 1) {
                // TODO: change to dedicated BusinessException
                throw new BusinessException(ErrorCodeEnum.BASIC, "Modified by other user");
            }
        }
    }

    public String createParamValueObject(String site, ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO vo) throws BusinessException {
        String handle = new ActivityParamValueBOHandle(site, vo.getActivityBo(), vo.getParamId()).getValue();
        ActivityParamValuePO po = Utils.copyObjectProperties(vo, ActivityParamValuePO.class);
        po.setHandle(handle);
        po.setSite(site);
        po.setModifiedUser(ContextHelper.getContext().getUser());

        int ret = activityParamValuePOMapper.insert(po);
        if (ret != 1) {
            // TODO: change to dedicated BusinessException
            throw new BusinessException(ErrorCodeEnum.BASIC, "insert ActivityParamValue");
        }
        return handle;
    }

    public List<ActivityPO> findActivity(String activity) {
        ActivityPOExample example = new ActivityPOExample();
        example.createCriteria().andActivityEqualTo(activity);
        return activityPOMapper.selectByExample(example);
    }
}
