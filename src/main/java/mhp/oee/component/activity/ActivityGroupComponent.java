package mhp.oee.component.activity;

import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.CreateErrorException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ActivityBOHandle;
import mhp.oee.common.handle.ActivityGroupBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.ActivityAndActivityGroupAndActivityGroupActivityPOMapper;
import mhp.oee.dao.gen.ActivityGroupPOMapper;
import mhp.oee.dao.gen.ActivityPOMapper;
import mhp.oee.po.gen.ActivityGroupPO;
import mhp.oee.po.gen.ActivityGroupPOExample;
import mhp.oee.po.gen.ActivityPO;
import mhp.oee.po.gen.ActivityParamPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ActivityGroupVO;
import mhp.oee.vo.ActivityVO;

@Component
public class ActivityGroupComponent {

    private static final String ACTIVITY_GROUP = "ACTIVITY_GROUP";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    
    @Autowired
    private ActivityGroupPOMapper activityGroupPOMapper;
    @Autowired
    private ActivityPOMapper activityPOMapper;

    @Autowired
    private ActivityAndActivityGroupAndActivityGroupActivityPOMapper activityAndActivityGroupAndActivityGroupActivityPOMapper;


    public List<ActivityGroupVO> getAssigendActivityGroup(String activity) {
        List<ActivityGroupVO> vos=activityAndActivityGroupAndActivityGroupActivityPOMapper.getAssigendActivityGroup(activity);
        return vos;
    }

    public List<ActivityGroupVO> getActivityGroup() {
        ActivityGroupPOExample example = new ActivityGroupPOExample();
        List<ActivityGroupPO> pos=activityGroupPOMapper.selectByExample(example);
        List<ActivityGroupVO> vos=new LinkedList<ActivityGroupVO>();
        for(ActivityGroupPO po:pos){
            ActivityGroupVO vo = Utils.copyObjectProperties(po, ActivityGroupVO.class);
            vos.add(vo);
        }
        return vos;
    }

    public List<ActivityGroupVO> getAvailableActivityGroup(String activity) {
        ActivityPO po=activityPOMapper.selectByPrimaryKey(new ActivityBOHandle(activity).getValue());
        List<ActivityGroupVO> list=new LinkedList<ActivityGroupVO>();
        if(po!=null){
            list=activityAndActivityGroupAndActivityGroupActivityPOMapper.getAvailableActivityGroup(activity);
        }
        return list;
    }

    public List<ActivityGroupVO> getActivityGroupForInputAssist(String activityGroup) {
        List<ActivityGroupVO> vos = new LinkedList<ActivityGroupVO>();
        ActivityGroupPOExample example = new ActivityGroupPOExample();
        example.createCriteria().andActivityGroupLike("%" + activityGroup + "%");
        example.setOrderByClause("HANDLE");
        List<ActivityGroupPO> pos =  activityGroupPOMapper.selectByExample(example);
        for (ActivityGroupPO po : pos) {
            ActivityGroupVO vo = Utils.copyObjectProperties(po, ActivityGroupVO.class);
            vos.add(vo);
        }
        return vos;
    }

    public void createActivityGroup(ActivityGroupVO activityGroupVO) throws ExistingRecordException, CreateErrorException {
        String handle = new ActivityGroupBOHandle(activityGroupVO.getActivityGroup()).getValue();
        ActivityGroupPO po = Utils.copyObjectProperties(activityGroupVO, ActivityGroupPO.class);
        po.setHandle(handle);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        if(activityGroupPOMapper.selectByPrimaryKey(handle)!=null){
            throw new ExistingRecordException(ACTIVITY_GROUP,gson.toJson(po));
        }
        int ret = activityGroupPOMapper.insert(po);
        if (ret != 1) {
            throw new CreateErrorException(ACTIVITY_GROUP,gson.toJson(po));
        }
    }

    public void updateActivityGroup(ActivityGroupVO activityGroupVO) throws UpdateErrorException  {
        String handle = new ActivityGroupBOHandle(activityGroupVO.getActivityGroup()).getValue();
        ActivityGroupPO po = Utils.copyObjectProperties(activityGroupVO, ActivityGroupPO.class);
        ActivityGroupPOExample example = new ActivityGroupPOExample();
        example.createCriteria().andHandleEqualTo(handle).andModifiedDateTimeEqualTo(activityGroupVO.getFirstModifiedDateTime());
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = activityGroupPOMapper.updateByExampleSelective(po, example);
        if (ret != 1) {
            // TODO: change to dedicated BusinessException
            throw new UpdateErrorException(ACTIVITY_GROUP, gson.toJson(po));
        }
    }

    public List<ActivityVO> getAvaliableActivity(String activityGroup) {
        ActivityGroupPO po=activityGroupPOMapper.selectByPrimaryKey(new ActivityGroupBOHandle(activityGroup).getValue());
        List<ActivityVO> list=new LinkedList<ActivityVO>();
        if(po!=null){
            list=activityAndActivityGroupAndActivityGroupActivityPOMapper.getAvailableActivity(activityGroup);
        }
        return list;
    }

    public List<ActivityVO> getAssigendActivity(String activityGroup) {
        return activityAndActivityGroupAndActivityGroupActivityPOMapper.getAssigendActivity(activityGroup);
    }

    public ActivityGroupVO getActivityGroupByPrimaryKey(String handle) {
        ActivityGroupPO po = activityGroupPOMapper.selectByPrimaryKey(handle);
        ActivityGroupVO vo = Utils.copyObjectProperties(po, ActivityGroupVO.class);
        return vo;
    }


}
