package mhp.oee.component.activity;

import java.util.*;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.CreateErrorException;
import mhp.oee.common.exception.ExistingException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.InvalidItemException;
import mhp.oee.common.exception.ItemDisableException;
import mhp.oee.common.exception.NoSuchException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ActivityBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.ActivityClientPOMapper;
import mhp.oee.dao.gen.ActivityClientPermPOMapper;
import mhp.oee.dao.gen.ActivityGroupActivityPOMapper;
import mhp.oee.dao.gen.ActivityGroupPOMapper;
import mhp.oee.dao.gen.ActivityPOMapper;
import mhp.oee.dao.gen.ActivityParamPOMapper;
import mhp.oee.dao.gen.ActivityPermPOMapper;
import mhp.oee.po.gen.ActivityClientPO;
import mhp.oee.po.gen.ActivityClientPOExample;
import mhp.oee.po.gen.ActivityClientPermPO;
import mhp.oee.po.gen.ActivityClientPermPOExample;
import mhp.oee.po.gen.ActivityGroupActivityPO;
import mhp.oee.po.gen.ActivityGroupActivityPOExample;
import mhp.oee.po.gen.ActivityGroupPO;
import mhp.oee.po.gen.ActivityPO;
import mhp.oee.po.gen.ActivityPOExample;
import mhp.oee.po.gen.ActivityParamPO;
import mhp.oee.po.gen.ActivityParamPOExample;
import mhp.oee.po.gen.ActivityPermPO;
import mhp.oee.po.gen.ActivityPermPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ActivityGroupVO;
import mhp.oee.vo.ActivityVO;
import mhp.oee.web.angular.activity.management.ActivityManageVO;

@Component
public class ActivityComponent {

    @InjectableLogger
    private static Logger logger;
    private static final String ACTIVITY = "ACTIVITY";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    private ActivityPOMapper activityPOMapper;
    @Autowired
    private ActivityParamPOMapper activityParamPOMapper;

    @Autowired
    private ActivityGroupPOMapper activityGroupPOMapper;
    @Autowired
    private ActivityGroupActivityPOMapper activityGroupActivityPOMapper;

    @Autowired
    private ActivityClientPOMapper activityClientPOMapper;
    @Autowired
    private ActivityClientPermPOMapper activityClientPermPOMapper;
    @Autowired
    private ActivityPermPOMapper activityPermPOMapper;

    // ==========================================================================
    // Activity, Activity_Param, Activity_Param_Value
    // ==========================================================================

    // ==========================================================================
    // Activity_Group, Activity_Group_Activity, Activity_Group_Perm
    // ==========================================================================

    // ==========================================================================
    // Activity_Client, Activity_Client_Perm
    // ==========================================================================

    public List<String> readAllowedActivityClientListByUserGroups(List<String> userGroupRefList) {
        ActivityClientPermPOExample example = new ActivityClientPermPOExample();
        example.createCriteria().andUserGroupBoIn(userGroupRefList).andPermissionSettingEqualTo("true");
        List<ActivityClientPermPO> list = activityClientPermPOMapper.selectByExample(example);

        // build a unique set
        Set<String> activityClientPermSet = new HashSet<String>();
        for (ActivityClientPermPO each : list) {
            String activityClientRef = each.getActivityClientBo();
            activityClientPermSet.add(activityClientRef);
        }

        List<String> result = new ArrayList<String>();
        if (!activityClientPermSet.isEmpty()) {
            List<String> activityClientRefList = new ArrayList<String>(activityClientPermSet);

            ActivityClientPOExample acExample = new ActivityClientPOExample();
            acExample.createCriteria().andHandleIn(activityClientRefList).andVisibleEqualTo("true");
            acExample.setOrderByClause("HANDLE ASC");
            List<ActivityClientPO> acList = activityClientPOMapper.selectByExample(acExample);

            for (ActivityClientPO each : acList) {
                result.add(each.getActivityClient());
            }
        }
        return result;
    }

    public List<ActivityManageVO.ActivityForInputAssitVO> getActivityForInputAssist(String activity) {
        ActivityPOExample example = new ActivityPOExample();
        example.createCriteria().andActivityLike("%" + activity + "%");
        example.setOrderByClause("EXECUTION_TYPE,SEQUENCE_ID");
        List<ActivityPO> pos = activityPOMapper.selectByExample(example);
        List<ActivityManageVO.ActivityForInputAssitVO> vos = new LinkedList<ActivityManageVO.ActivityForInputAssitVO>();
        for (ActivityPO po : pos) {
            ActivityManageVO.ActivityForInputAssitVO vo = Utils.copyObjectProperties(po,
                    ActivityManageVO.ActivityForInputAssitVO.class);
            vos.add(vo);
        }
        return vos;
    }

    public List<ActivityManageVO.ActivityForBasicInfoVO> getActivityForBasicInfo(String activity) {
        ActivityPOExample example = new ActivityPOExample();
        example.createCriteria().andActivityEqualTo(activity);
        List<ActivityPO> pos = activityPOMapper.selectByExample(example);
        List<ActivityManageVO.ActivityForBasicInfoVO> vos = new LinkedList<ActivityManageVO.ActivityForBasicInfoVO>();
        for (ActivityPO po : pos) {
            ActivityManageVO.ActivityForBasicInfoVO vo = Utils.copyObjectProperties(po,
                    ActivityManageVO.ActivityForBasicInfoVO.class);
            vos.add(vo);
        }
        return vos;
    }

    public void createActivity(ActivityVO vo) throws ExistingRecordException, CreateErrorException {
        ActivityPO po = Utils.copyObjectProperties(vo, ActivityPO.class);
        String handle = new ActivityBOHandle(po.getActivity()).getValue();
        po.setHandle(handle);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        if (activityPOMapper.selectByPrimaryKey(handle) != null) {
            throw new ExistingRecordException(ACTIVITY, gson.toJson(po));
        }
        int ret = activityPOMapper.insert(po);
        if (ret != 1) {
            throw new CreateErrorException(ACTIVITY, gson.toJson(po));
        }
    }

    public void updateActivity(ActivityVO vo) throws UpdateErrorException {
        ActivityPOExample example = new ActivityPOExample();
        example.createCriteria().andHandleEqualTo(new ActivityBOHandle(vo.getActivity()).getValue())
                .andModifiedDateTimeEqualTo(vo.getFirstModifiedDateTime());
        ActivityPO po = Utils.copyObjectProperties(vo, ActivityPO.class);
        po.setHandle(new ActivityBOHandle(vo.getActivity()).getValue());
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = activityPOMapper.updateByExample(po, example);
        if (ret != 1) {
            throw new UpdateErrorException(ACTIVITY, gson.toJson(po));
        }
    }

    public List<ActivityVO> getActivityByUserGroup(String userGroupBo) {
        ActivityPermPOExample example = new ActivityPermPOExample();
        example.createCriteria().andUserGroupBoEqualTo(userGroupBo).andPermissionSettingEqualTo("true");
        List<ActivityPermPO> permList = activityPermPOMapper.selectByExample(example);
        if (permList != null && permList.size() != 0) {
            List<ActivityVO> activityList = new ArrayList<ActivityVO>();
            for (ActivityPermPO po : permList) {
                ActivityPO activity = activityPOMapper.selectByPrimaryKey(po.getActivityBo());
                if (activity.getEnabled().equals("true")) {
                    ActivityVO activityVO = Utils.copyObjectProperties(activity, ActivityVO.class);
                    activityVO.setPermPO(po);
                    activityList.add(activityVO);
                }
            }
            return activityList;
        }
        return null;
    }

    public List<ActivityGroupVO> getActivityGroupByActivity(ActivityVO activityBO) {
        ActivityGroupActivityPOExample example = new ActivityGroupActivityPOExample();
        example.createCriteria().andActivityBoEqualTo(activityBO.getHandle());

        List<ActivityGroupActivityPO> pos = activityGroupActivityPOMapper.selectByExample(example);
        if (pos != null && pos.size() != 0) {
            List<ActivityGroupVO> activityGroups = new ArrayList<ActivityGroupVO>();
            for (ActivityGroupActivityPO po : pos) {
                ActivityGroupPO activityGroupPO = activityGroupPOMapper.selectByPrimaryKey(po.getActivityGroupBo());
                if (activityGroupPO != null) {
                    ActivityGroupVO activityGroupVO = Utils.copyObjectProperties(activityGroupPO, ActivityGroupVO.class);
                    activityGroupVO.setActivityVOs(new ArrayList<ActivityVO>());

                    activityGroupVO.getActivityVOs().add(activityBO);
                    activityGroups.add(activityGroupVO);
                } else {
                    logger.error(">>>>>>>" + po.getActivityGroupBo() + " was not exist");
                }
            }

            return activityGroups;
        }
        return null;
    }

    public List<ActivityManageVO.ActivityConditionVO> getActivitiesLike(String activity) {
        ActivityPOExample example = new ActivityPOExample();
        example.createCriteria()
                .andActivityLike("%" + activity + "%")
                .andExecutionTypeIn(Arrays.asList("P", "E"))
                .andEnabledEqualTo("true");
        example.setOrderByClause("EXECUTION_TYPE, SEQUENCE_ID");
        List<ActivityPO> pos = activityPOMapper.selectByExample(example);
        return Utils.copyListProperties(pos, ActivityManageVO.ActivityConditionVO.class);
    }

    public List<ActivityPO> getActivitiesLikeAndExecutionType(String activity, String executionType) {
        ActivityPOExample example = new ActivityPOExample();
        example.createCriteria().andExecutionTypeEqualTo(executionType).andEnabledEqualTo("true")
                .andActivityLike("%" + activity + "%");
        example.setOrderByClause("SEQUENCE_ID, ACTIVITY");
        return activityPOMapper.selectByExample(example);
    }

    public ActivityPO selectActivityByPrimaryKey(String handle) {
        return activityPOMapper.selectByPrimaryKey(handle);
    }

    public void checkActivityPO(ActivityPO activityPO, String rule)
            throws NoSuchException, ItemDisableException, InvalidItemException {
        if (activityPO == null) {
            throw new NoSuchException("activity");
        }
        if ("false".equals(activityPO.getEnabled())) {
            throw new ItemDisableException("Activity");
        }
        if (!rule.equals(activityPO.getExecutionType())) {
            throw new InvalidItemException();
        }
    }

    public void updateActivityByCondition(String activityBo, String activity) throws UpdateErrorException {
        ActivityPO activityPO = selectActivityByPrimaryKey(activityBo);
        if (activityPO != null) {
            activityPO.setActivity(activity);
            int ret = activityPOMapper.updateByPrimaryKey(activityPO);
            if (ret != 1) {
                throw new UpdateErrorException(ACTIVITY, gson.toJson(activityPO));
            }
        }
    }

    public ActivityPO getActivityByCondition(String activityBo, String enable, String executionType)
            throws ExistingException {
        ActivityPOExample example = new ActivityPOExample();
        example.createCriteria().andHandleEqualTo(activityBo).andEnabledEqualTo(enable)
                .andExecutionTypeEqualTo(executionType);
        List<ActivityPO> activityPOs = activityPOMapper.selectByExample(example);
        if (activityPOs.size() != 0) {
            return activityPOs.get(0);
        } else {
            throw new ExistingException();
        }
    }

    public ActivityParamPO getActivityParamByCondition(String activityBo, String activityFParamId)
            throws ExistingException {
        ActivityParamPOExample example = new ActivityParamPOExample();
        example.createCriteria().andActivityBoEqualTo(activityBo).andParamIdEqualTo(activityFParamId);
        List<ActivityParamPO> activityParamPOs = activityParamPOMapper.selectByExample(example);
        if (activityParamPOs.size() == 0) {
            throw new ExistingException();
        }
        return activityParamPOs.get(0);
    }
}
