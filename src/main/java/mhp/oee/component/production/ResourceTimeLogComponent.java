package mhp.oee.component.production;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.InvalidResourceException;
import mhp.oee.common.exception.MultipleRecordsFoundException;
import mhp.oee.common.handle.ResourceTimeLogBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.dao.gen.ResourceTimeLogPOMapper;
import mhp.oee.po.gen.ResourceTimeLogPO;
import mhp.oee.po.gen.ResourceTimeLogPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceTimeLogVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class ResourceTimeLogComponent {

	@Autowired
	private ResourceTimeLogPOMapper resourceTimeLogPOMapper;
	@Autowired
	private ResourceComponent resourceComponent;

	public int createResourceTimeLog(ResourceTimeLogVO resourceTimeLogVO, String modifyUser) throws BusinessException {
		if (!resourceComponent.isEnabledResource(resourceTimeLogVO.getResourceBo())) {
			throw new InvalidResourceException();
		}

		ResourceTimeLogPO po = Utils.copyObjectProperties(resourceTimeLogVO, ResourceTimeLogPO.class);
		ResourceTimeLogBOHandle handle = new ResourceTimeLogBOHandle(po.getSite(), po.getResourceBo(), po.getStrt());
		po.setHandle(handle.getValue());
		po.setModifiedUser(modifyUser);
		return resourceTimeLogPOMapper.insert(po);
	}

	public ResourceTimeLogPO readLastActiveRecord(String resourceHandle) throws MultipleRecordsFoundException {
		ResourceTimeLogPOExample example = new ResourceTimeLogPOExample();
		example.createCriteria().andResourceStateEqualTo("P").andResourceBoEqualTo(resourceHandle).andStartDateTimeLessThan(new Date());
		example.setOrderByClause("START_DATE_TIME DESC");
		List<ResourceTimeLogPO> ret = resourceTimeLogPOMapper.selectByExample(example);
		if (Utils.isEmpty(ret)) {
			return null;
		} /*else if (ret.size() > 1) {
			throw new MultipleRecordsFoundException();
		}*/ else {
			return ret.get(0);
		}
	}
	public ResourceTimeLogPO readLastRecord(String resourceBo) {
        ResourceTimeLogPOExample example = new ResourceTimeLogPOExample();
        example.createCriteria().andEndDateTimeIsNull().andResourceBoEqualTo(resourceBo);
        example.setOrderByClause("STRT DESC");
		example.setLimit(1);
        List<ResourceTimeLogPO> ret = resourceTimeLogPOMapper.selectByExample(example);
        if (Utils.isEmpty(ret)) {
            return null;
        } else {
            return ret.get(0);
        }
    }
	
	public List<ResourceTimeLogPO> readTimeLogRecord(String resourceHandle) {
        ResourceTimeLogPOExample example = new ResourceTimeLogPOExample();
        example.createCriteria().andResourceBoEqualTo(resourceHandle);
        example.setOrderByClause("START_DATE_TIME DESC");
        List<ResourceTimeLogPO> ret = resourceTimeLogPOMapper.selectByExample(example);
        if (Utils.isEmpty(ret)) {
            return null;
        } else {
            return ret;
        }
    }
	
	public int createResourceTimeLogEicc(ResourceTimeLogPO po) throws BusinessException {
        ResourceTimeLogBOHandle handle = new ResourceTimeLogBOHandle(po.getSite(), po.getResourceBo(), po.getStrt());
        po.setHandle(handle.getValue());
        po.setModifiedUser(ContextHelper.getContext().getUser());
        return resourceTimeLogPOMapper.insert(po);
    }

	public List<ResourceTimeLogPO> readRecordsWithinTimeRange(Date beginDate, Date finishDate, String resourceHandle)
			throws BusinessException {
		ResourceTimeLogPOExample example = new ResourceTimeLogPOExample();
		example.createCriteria().andStartDateTimeLessThan(finishDate).andEndDateTimeGreaterThanOrEqualTo(beginDate)
				.andResourceBoEqualTo(resourceHandle);
		example.setOrderByClause("START_DATE_TIME");
		List<ResourceTimeLogPO> ret = resourceTimeLogPOMapper.selectByExample(example);
		if (Utils.isEmpty(ret)) {
			return null;
		} else {
			return ret;
		}
	}

	public List<ResourceTimeLogPO> readRecordsStartTimeWithinTimeRang(Date beginDate, Date finishDate, String resourceHandle) {
		ResourceTimeLogPOExample example = new ResourceTimeLogPOExample();
		example.createCriteria().andStartDateTimeBetween(beginDate, finishDate)
				.andStartDateTimeIsNotNull()
				.andEndDateTimeIsNotNull()
				.andResourceBoEqualTo(resourceHandle);
		example.setOrderByClause("START_DATE_TIME");
		List<ResourceTimeLogPO> ret = resourceTimeLogPOMapper.selectByExample(example);
		if (Utils.isEmpty(ret)) {
			return null;
		} else {
			return ret;
		}
	}

	public void updateResourceTimeLog(ResourceTimeLogPO po, String modifyUser) throws BusinessException {
		po.setModifiedUser(modifyUser);
		int ret = resourceTimeLogPOMapper.updateByPrimaryKey(po);
		if (ret != 1) {
			throw new BusinessException(ErrorCodeEnum.BASIC, "updateResourceTimeLog");
		}
	}

	public List<ResourceTimeLogPO> readRecordsProcessingRTL() {
		ResourceTimeLogPOExample example = new ResourceTimeLogPOExample();
		example.createCriteria().andResourceStateEqualTo("P").andEndDateTimeIsNull();
		return resourceTimeLogPOMapper.selectByExample(example);
	}
}
