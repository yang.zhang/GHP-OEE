package mhp.oee.component.production;

import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.ServiceErrorException;
import mhp.oee.common.handle.OperationParamBOHandle;
import mhp.oee.dao.extend.ResourceParamPOMapper;
import mhp.oee.po.extend.ResourceParamMapperPO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by DingLJ on 2017/4/17 017.
 */
@Component
public class ResourceParamComponent {

    @Autowired
    private ResourceParamPOMapper resourceParamPOMapper;

    public int create(String site, String resourceType,String resource, String key, String value, Date uploadDatetime,Date now){
        return this.resourceParamPOMapper.insert(site,resourceType,resource,key,value,uploadDatetime,now);
    }


    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.READ_COMMITTED)
    public void createMapper(String site, String resourceType, String paramName, String paramKey){

        if(StringUtils.isBlank(site)){
            throw new ServiceErrorException(ErrorCodeEnum.COMMON_SITE_NONE,"site为空");
        }
        if(StringUtils.isBlank(resourceType)){
            throw new ServiceErrorException(ErrorCodeEnum.COMMON_RESOURCE_TYPE_NONE,"设备类型为空");
        }
        if(StringUtils.isBlank(paramKey)){
            throw new ServiceErrorException(ErrorCodeEnum.RESOURCE_PARAM_MAPPER_KEY_NONE,"key为空");
        }


        List<ResourceParamMapperPO> existMapperList = this.resourceParamPOMapper.selectMappers(site,resourceType,paramKey);

        if(!CollectionUtils.isEmpty(existMapperList)){
            throw new ServiceErrorException(ErrorCodeEnum.RESOURCE_PARAM_MAPPER_EXIST,"已存在key");
        }

        ResourceParamMapperPO p = new ResourceParamMapperPO();

        p.setSite(site);
        p.setResourceType(resourceType);
        p.setParamName(paramName);
        p.setParamKey(paramKey);

        OperationParamBOHandle handle = new OperationParamBOHandle(site,resourceType,paramKey);
        p.setHandle(handle.getValue());


        int updates = this.resourceParamPOMapper.insertMapper(p);

        if(updates != 1){
            throw new ServiceErrorException(ErrorCodeEnum.COMMON_INSERT_FAILED,"添加记录失败");
        }


    }

    public List<ResourceParamMapperPO> readMappers(String site, String resourceType, String key){

        return this.resourceParamPOMapper.selectMappers(site,resourceType,key);

    }

}
