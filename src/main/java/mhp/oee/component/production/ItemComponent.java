package mhp.oee.component.production;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.xmlbeans.impl.xb.xmlconfig.NamespaceList.Member2.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.handle.ItemBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.ItemExtendPOMapper;
import mhp.oee.dao.gen.ItemPOMapper;
import mhp.oee.po.extend.ItemExtendPO;
import mhp.oee.po.gen.ItemPO;
import mhp.oee.po.gen.ItemPOExample;
import mhp.oee.po.gen.ItemPOExample.Criteria;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ItemVO;
import org.springframework.util.CollectionUtils;

@Component
public class ItemComponent {

    public static final String DEFAULT_ITEM_REVISION = "#";

    @Autowired
    private ItemPOMapper itemPOMapper;

    @Autowired
    private ItemExtendPOMapper itemPOExtendMapper;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    public boolean existItem(String site, String item, String revision) {
        if (revision == null) {
            revision = DEFAULT_ITEM_REVISION;
        }
        String handle = new ItemBOHandle(site, item, revision).getValue();
        ItemPO po = itemPOMapper.selectByPrimaryKey(handle);
        return (po != null) ? true : false;
    }

    public String createItem(ItemVO itemVO) throws BusinessException {
        String model[]=itemVO.getItem().split("-");
        if (itemVO.getRevision() == null) {
            // TODO: check with business consultant for handling null revision
            // case
            itemVO.setRevision(DEFAULT_ITEM_REVISION);
        }
        if (!StringUtils.isNotBlank(itemVO.getModel())) {
            if (model.length > 2) {
                itemVO.setModel(model[2]);
            } else {
                itemVO.setModel(model[0]);
            }
        }

        String handle = new ItemBOHandle(itemVO.getSite(), itemVO.getItem(), itemVO.getRevision()).getValue();
        ItemPO existingPO = itemPOMapper.selectByPrimaryKey(handle);
        if (existingPO != null) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "Existing ITEM record : " + gson.toJson(existingPO));
        }

        ItemPO po = Utils.copyObjectProperties(itemVO, ItemPO.class);
        po.setHandle(handle); // this is create action, view does not provide
                              // HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = itemPOMapper.insert(po);
        if (ret != 1) {
            existingPO = itemPOMapper.selectByPrimaryKey(handle);
            throw new BusinessException(ErrorCodeEnum.BASIC, "Existing ITEM record : " + gson.toJson(existingPO));
        }
        return handle;
    }

    public List<ItemPO> readItems(String site, String item, String revision, String model, String enabled) {
        ItemPOExample example = new ItemPOExample();
        Criteria criteria = example.createCriteria();
        if (!Utils.isEmpty(site)) {
            criteria.andSiteEqualTo(site);
        }
        if (!Utils.isEmpty(item)) {
            criteria.andItemEqualTo(item);
        }
        if (!Utils.isEmpty(revision)) {
            criteria.andRevisionEqualTo(revision);
        }
        if (!Utils.isEmpty(model)) {
            criteria.andModelEqualTo(model);
        }
        if (!Utils.isEmpty(enabled)) {
            criteria.andEnabledEqualTo(enabled);
        }
        return itemPOMapper.selectByExample(example);
    }

    public List<ItemPO> readItems(String site, String model, String enabled) {
        ItemPOExample example = new ItemPOExample();
        Criteria criteria = example.createCriteria();
        if (!Utils.isEmpty(site)) {
            criteria.andSiteEqualTo(site);
        }
        if (!Utils.isEmpty(model)) {
            criteria.andModelEqualTo(model);
        }
        if (!Utils.isEmpty(enabled)) {
            criteria.andEnabledEqualTo(enabled);
        }
        return itemPOMapper.selectByExample(example);
    }

    public List<ItemPO> readItemsByModelInItem(String site, String model, String enabled) {
        ItemPOExample example = new ItemPOExample();
        Criteria criteria = example.createCriteria();
        criteria.andSiteEqualTo(site);
        criteria.andItemLike("%"+model+"%");
        criteria.andEnabledEqualTo(enabled);

        return itemPOMapper.selectByExample(example);
    }

    public List<ItemPO> readItemsByEnabledItem(String site, String item, String enabled) {
        ItemPOExample example = new ItemPOExample();
        Criteria criteria = example.createCriteria();
        criteria.andSiteEqualTo(site);
        criteria.andItemEqualTo(item);
        criteria.andEnabledEqualTo(enabled);

        return itemPOMapper.selectByExample(example);
    }

    public String getModelByEnabledItem(String site, String item) {
        ItemPOExample example = new ItemPOExample();
        Criteria criteria = example.createCriteria();
        criteria.andSiteEqualTo(site);
        criteria.andItemEqualTo(item);
        criteria.andEnabledEqualTo("true");

        List<ItemPO> selectByExample = itemPOMapper.selectByExample(example);
        if(selectByExample != null && selectByExample.size() != 0){
            return selectByExample.get(0).getModel();
        } else {
            return "";
        }
    }


    public void updateItem(ItemVO ItemVO) throws BusinessException {
        ItemPO po = Utils.copyObjectProperties(ItemVO, ItemPO.class);
        String model[]=po.getItem().split("-");
        if (!StringUtils.isNotBlank(po.getModel())) {
            if (model.length > 2) {
                po.setModel(model[2]);
            } else {
                po.setModel(model[0]);
            }
        }

        if (po.getRevision() == null) {
            po.setRevision(DEFAULT_ITEM_REVISION);
        }
        po.setHandle(new ItemBOHandle(po.getSite(), po.getItem(), po.getRevision()).getValue());
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = itemPOMapper.updateByPrimaryKey(po);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "updateItem");
        }
    }

    public List<ItemVO> readModelHelp(String site, String model) {
        List<ItemExtendPO> itemPOs = itemPOExtendMapper.selectModel(site,Utils.ConvertStrtoArrayLike(model));
        return Utils.copyListProperties(itemPOs, ItemVO.class);
    }

	public List<ItemVO> readItemHelp(String site, List<String> modelList, String item) {
        ItemPOExample example = new ItemPOExample();
        Criteria criteria = example.createCriteria();
        if (!Utils.isEmpty(site)) {
            criteria.andSiteEqualTo(site);
        }
        if (modelList != null && modelList.size() > 0) {
            criteria.andModelIn(modelList);
        }
        if (!Utils.isEmpty(item)) {
			criteria.andItemLikeMore(Utils.ConvertStrtoArrayLike(item));
        }
        example.setOrderByClause("item");
        List<ItemPO> pos = itemPOMapper.selectByExample(example);
        return Utils.copyListProperties(pos, ItemVO.class);
    }

    public ItemPO getItem(String item) {

        if(StringUtils.isBlank(item)){
            return null;
        }

        ItemPOExample example = new ItemPOExample();
        Criteria criteria = example.createCriteria();
        criteria.andItemEqualTo(item);

        List<ItemPO> itemList = itemPOMapper.selectByExample(example);

        if(CollectionUtils.isEmpty(itemList)){
            return null;
        }

        return itemList.get(0);

    }
}
