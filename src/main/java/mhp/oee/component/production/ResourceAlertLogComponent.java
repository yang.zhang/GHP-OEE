package mhp.oee.component.production;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.InvalidResourceException;
import mhp.oee.common.exception.InvalidResourceStateException;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.component.plant.ResourcePlcComponent;
import mhp.oee.dao.gen.ResourceAlertLogPOMapper;
import mhp.oee.po.gen.ResourceAlertLogPO;
import mhp.oee.po.gen.ResourceAlertLogPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceAlertLogVO;

@Component
public class ResourceAlertLogComponent {

    @Autowired
    private ResourceAlertLogPOMapper resourceAlertLogPOMapper;
    
    @Autowired
    private ResourceComponent resourceComponent;
    

    public int createResourceAlertLog(ResourceAlertLogVO resourceAlertLogVO) throws BusinessException {
        ResourceAlertLogPO po = Utils.copyObjectProperties(resourceAlertLogVO, ResourceAlertLogPO.class);

        ResrceBOHandle resrceBOHandle = new ResrceBOHandle(po.getSite(), po.getResrce());
        if (resourceComponent.getResourceByHandle(resrceBOHandle.getValue()) == null) {
            throw new InvalidResourceException();
        }

        String resourceState = po.getResourceState();
        if (resourceState == null) {
            throw new InvalidResourceStateException();
        } else if (!"P".equals(resourceState) && !"D".equals(resourceState) && !"U".equals(resourceState)) {
            throw new InvalidResourceStateException();
        }

        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = resourceAlertLogPOMapper.insert(po);
        return ret;
    }

    public List<ResourceAlertLogPO> readRecordsWithinTimeRange(Date beginDate, Date finishDate, String resource) throws BusinessException{
        ResourceAlertLogPOExample example = new ResourceAlertLogPOExample();
        example.createCriteria().andResrceEqualTo(resource).andDateTimeBetween(beginDate, finishDate);
        example.setOrderByClause("DATE_TIME");
        List<ResourceAlertLogPO> ret = resourceAlertLogPOMapper.selectByExample(example);
        if(Utils.isEmpty(ret)){
        	ret = new ArrayList<ResourceAlertLogPO>();
           // throw new BusinessException(ErrorCodeEnum.BASIC, "no records found");
        }
        return ret;
    }

}
