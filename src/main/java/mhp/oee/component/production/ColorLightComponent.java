package mhp.oee.component.production;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.handle.ColorLightBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.ColorLightPOMapper;
import mhp.oee.po.gen.ColorLightPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ColorLightVO;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
@Component
public class ColorLightComponent {

    @Autowired
    private ColorLightPOMapper colorLightPOMapper;

    public boolean existColorLight(String handle) throws BusinessException {
        ColorLightPO po = colorLightPOMapper.selectByPrimaryKey(handle);
        return (po != null) ? true : false;
    }

    public void createColorLight(ColorLightVO vo) throws BusinessException {
        ColorLightPO po = Utils.copyObjectProperties(vo, ColorLightPO.class);
        ColorLightBOHandle colorLightBOHandle = new ColorLightBOHandle(vo.getResourceBo());
        po.setHandle(colorLightBOHandle.getValue());
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = colorLightPOMapper.insert(po);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "createColorLight");
        }
    }

    public void updateColorLight(ColorLightVO vo) throws BusinessException {
        ColorLightPO po = Utils.copyObjectProperties(vo, ColorLightPO.class);
        ColorLightBOHandle colorLightBOHandle = new ColorLightBOHandle(vo.getResourceBo());
        po.setHandle(colorLightBOHandle.getValue());
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = colorLightPOMapper.updateByPrimaryKey(po);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "updateColorLight");
        }
    }

}
