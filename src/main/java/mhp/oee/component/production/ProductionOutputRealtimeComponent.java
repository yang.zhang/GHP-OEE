package mhp.oee.component.production;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.InvalidItemException;
import mhp.oee.common.exception.InvalidResourceException;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.dao.gen.ProductionOutputRealtimePOMapper;
import mhp.oee.po.gen.ProductionOutputRealtimePO;
import mhp.oee.po.gen.ProductionOutputRealtimePOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ProductionOutputRealtimeVO;

@Component
public class ProductionOutputRealtimeComponent {

    @Autowired
    private ProductionOutputRealtimePOMapper productionOutputRealtimePOMapper;
    @Autowired
    private ResourceComponent resourceComponent;
    @Autowired
    private ItemComponent itemComponent;


    public int createProductionOutputRealtime(ProductionOutputRealtimeVO productionOutputRealtimeVO) throws BusinessException {
        ProductionOutputRealtimePO po = Utils.copyObjectProperties(productionOutputRealtimeVO, ProductionOutputRealtimePO.class);

        ResrceBOHandle resrceBOHandle = new ResrceBOHandle(po.getSite(), po.getResrce());
        if (resourceComponent.getResourceByHandle(resrceBOHandle.getValue()) == null) {
            throw new InvalidResourceException();
        }

        if (!itemComponent.existItem(po.getSite(), po.getItem(), null)) {
            // TODO: revision is not passed from request??
            throw new InvalidItemException();
        }

        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = productionOutputRealtimePOMapper.insert(po);
        return ret;
    }

    public ProductionOutputRealtimePO readLastRecord(String site, String resource, String item) {
        ProductionOutputRealtimePOExample example = new ProductionOutputRealtimePOExample();
        example.createCriteria().andSiteEqualTo(site).andResrceEqualTo(resource).andItemEqualTo(item);
        example.setOrderByClause("END_DATE_TIME DESC");
        List<ProductionOutputRealtimePO> pos = productionOutputRealtimePOMapper.selectByExample(example);

        if (Utils.isEmpty(pos)) {
            return null;
        } else {
            return pos.get(0);
        }
    }

    public List<ProductionOutputRealtimePO> readByResrceAndShiftTime(String site, List<String> resrceList, Date shiftStartTime, Date shiftEndTime) {
        ProductionOutputRealtimePOExample example = new ProductionOutputRealtimePOExample();
        example.createCriteria().andSiteEqualTo(site).andResrceIn(resrceList)
               .andStartDateTimeGreaterThanOrEqualTo(shiftStartTime)
               .andStartDateTimeLessThan(shiftEndTime);
        List<ProductionOutputRealtimePO> pos = productionOutputRealtimePOMapper.selectByExample(example);

        if (Utils.isEmpty(pos)) {
            return null;
        } else {
            return pos;
        }
    }

    public List<ProductionOutputRealtimePO> readByResrceAndShiftTime(String site, List<String> resrceList, List<String> itemList, Date shiftStartTime, Date shiftEndTime) {
        ProductionOutputRealtimePOExample example = new ProductionOutputRealtimePOExample();
        example.createCriteria().andSiteEqualTo(site).andResrceIn(resrceList).andItemIn(itemList)
               .andStartDateTimeGreaterThanOrEqualTo(shiftStartTime)
               .andStartDateTimeLessThan(shiftEndTime);
        List<ProductionOutputRealtimePO> pos = productionOutputRealtimePOMapper.selectByExample(example);

        if (Utils.isEmpty(pos)) {
            return null;
        } else {
            return pos;
        }
    }

}
