package mhp.oee.component.production;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import mhp.oee.dao.gen.ProductionOutputRealtimePOMapper;
import mhp.oee.po.gen.ProductionOutputRealtimePO;
import mhp.oee.po.gen.ProductionOutputRealtimePOExample;
import mhp.oee.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.ProductionOutputFirstPOMapper;
import mhp.oee.po.gen.ProductionOutputFirstPO;
import mhp.oee.po.gen.ProductionOutputFirstPOExample;
import mhp.oee.web.angular.yield.maintenance.YieldSearchVO;

/**
 * Created by LinZuK on 2016/7/22.
 */
@Component
public class
ProductionOutputComponent {

    @Autowired
    private ProductionOutputFirstPOMapper productionOutputFirstPOMapper;
    @Autowired
    private ProductionOutputRealtimePOMapper productionOutputRealtimePOMapper;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();




    public List<ProductionOutputFirstPO> readProductionOutputFirst(String site, List<String> resrceList, String shift, Date date) {
        ProductionOutputFirstPOExample example = new ProductionOutputFirstPOExample();
        example.createCriteria().andSiteEqualTo(site).andResrceIn(resrceList).andShiftEqualTo(shift).andStartDateTimeEqualTo(date);
        return productionOutputFirstPOMapper.selectByExample(example);
    }

    public List<ProductionOutputFirstPO> readProductionOutputFirst(String site, List<String> resrceList,List<String> itemList, String shift, Date date) {
        ProductionOutputFirstPOExample example = new ProductionOutputFirstPOExample();
        example.createCriteria().andSiteEqualTo(site).andResrceIn(resrceList).andItemIn(itemList).andShiftEqualTo(shift).andStartDateTimeEqualTo(date);
        return productionOutputFirstPOMapper.selectByExample(example);
    }

    public List<ProductionOutputFirstPO> readProductionOutputFirst(int hours) {
        Date end = new Date();
        Date start = Utils.addHourToDate(end, hours, false);
//        Date start;
//        Date end;
//        try {
//            start = Utils.strToDatetimeMs("20160920.000000.000");
//            end = Utils.strToDatetimeMs("20160921.000000.000");
//        } catch (ParseException e) {
//            e.printStackTrace();
//            return null;
//        }
        ProductionOutputFirstPOExample example = new ProductionOutputFirstPOExample();
        example.createCriteria()
                .andCreatedDateTimeGreaterThanOrEqualTo(start)
                .andCreatedDateTimeLessThan(end);
        return productionOutputFirstPOMapper.selectByExample(example);
    }

    public void updateProductionOutputFirst(YieldSearchVO vo) throws UpdateErrorException {
        ProductionOutputFirstPO po = productionOutputFirstPOMapper.selectByPrimaryKey(vo.getHandle());
        po.setQtyScrap(vo.getBadQuity());
        po.setQtyYield(vo.getGoodQuity());

        ProductionOutputFirstPOExample example = new ProductionOutputFirstPOExample();
        example.createCriteria().andHandleEqualTo(vo.getHandle()).andModifiedDateTimeEqualTo(vo.getModifiedDateTime());
        po.setModifiedUser(ContextHelper.getContext().getUser());

        int ret = productionOutputFirstPOMapper.updateByExampleSelective(po, example);
        if (ret != 1) {
            // TODO: change to dedicated BusinessException
            throw new UpdateErrorException("PRODUCTION_OUTPUT_FIRST", gson.toJson(po));
        }
    }

    public void insertProductionOutputFirst(ProductionOutputFirstPO po) throws ExistingRecordException {
        ProductionOutputFirstPO existingPO = productionOutputFirstPOMapper.selectByPrimaryKey(po.getHandle());
        if (existingPO != null) {
            throw new ExistingRecordException("PRODUCTION_OUTPUT_FIRST", gson.toJson(existingPO));
        }
        po.setModifiedUser(ContextHelper.getContext().getUser());

        int ret = productionOutputFirstPOMapper.insert(po);
        if (ret != 1) {
            throw new ExistingRecordException("PRODUCTION_OUTPUT_FIRST", gson.toJson(po));        }
    }

    public List<ProductionOutputRealtimePO> readProductionOutputRealtimeByCondition(String site, String resrce, String item, Date startDateTime, Date endDateTime) {
        ProductionOutputRealtimePOExample example = new ProductionOutputRealtimePOExample();
        example.createCriteria().andSiteEqualTo(site).andResrceEqualTo(resrce).andItemEqualTo(item)
                .andStartDateTimeGreaterThanOrEqualTo(startDateTime)
                .andStartDateTimeLessThan(endDateTime);
        return productionOutputRealtimePOMapper.selectByExample(example);
    }

    public int updateProductionOutputFirst(ProductionOutputFirstPO po) {
        return productionOutputFirstPOMapper.updateByPrimaryKey(po);
    }
}
