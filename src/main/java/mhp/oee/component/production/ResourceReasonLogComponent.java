package mhp.oee.component.production;

import java.util.Date;
import java.util.List;

import mhp.oee.po.gen.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.InvalidReasonCodeException;
import mhp.oee.common.exception.InvalidResourceException;
import mhp.oee.common.handle.ReasonCodeBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.component.plant.ReasonCodeComponent;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.dao.gen.ResourceReasonLogPOMapper;
import mhp.oee.po.gen.ResourceReasonLogPOExample.Criteria;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceReasonLogVO;

@Component
public class ResourceReasonLogComponent {

	@Autowired
	private ResourceReasonLogPOMapper resourceReasonLogPOMapper;
	@Autowired
	private ResourceComponent resourceComponent;
	@Autowired
	private ReasonCodeComponent reasonCodeComponent;

	public int createResourceReasonLog(ResourceReasonLogVO resourceReasonLogVO) throws BusinessException {
		ResourceReasonLogPO po = Utils.copyObjectProperties(resourceReasonLogVO, ResourceReasonLogPO.class);

		ResrceBOHandle resrceBOHandle = new ResrceBOHandle(po.getSite(), po.getResrce());
		if (resourceComponent.getResourceByHandle(resrceBOHandle.getValue()) == null) {
			throw new InvalidResourceException();
		}

		ReasonCodeBOHandle reasonCodeBOHandle = new ReasonCodeBOHandle(po.getSite(), po.getReasonCode());
		ReasonCodePO reasonCodePO = reasonCodeComponent.getReasonCodeByHandle(reasonCodeBOHandle.getValue());
		if (reasonCodePO == null) {
			throw new InvalidReasonCodeException();
		} else if ("false".equals(reasonCodePO.getEnabled())) {
			throw new InvalidReasonCodeException();
		}

		po.setMeUserId(po.getMeUserId().toUpperCase()); // force to be upper
														// case
		po.setModifiedUser(ContextHelper.getContext().getUser());
		int ret = resourceReasonLogPOMapper.insert(po);
		return ret;
	}

	public List<ResourceReasonLogPO> selectByExample(String resrce, String reasonCode, String actionCode,
			String maintenanceNotification, String maintenanceOrder) {

		ResourceReasonLogPOExample example = new ResourceReasonLogPOExample();
		Criteria criteria = example.createCriteria();
		criteria.andResrceEqualTo(resrce).andReasonCodeEqualTo(reasonCode).andActionCodeEqualTo(actionCode);
		if (!Utils.isEmpty(maintenanceNotification)) {
			criteria.andMaintenanceNotificationEqualTo(maintenanceNotification);
		}
		if (!Utils.isEmpty(maintenanceOrder)) {
			criteria.andMaintenanceOrderEqualTo(maintenanceOrder);
		}
		List<ResourceReasonLogPO> ret = resourceReasonLogPOMapper.selectByExample(example);
		return ret;
	}

	public List<ResourceReasonLogPO> readRecordsByResourceTimeLog(ResourceTimeLogPO resourceTimeLogPO,
			String reasonCode) {
		ResrceBOHandle resrceBOHandle = new ResrceBOHandle(resourceTimeLogPO.getResourceBo());
		ResourceReasonLogPOExample example = new ResourceReasonLogPOExample();
		Criteria criteria = example.createCriteria();
		criteria.andResrceEqualTo(resrceBOHandle.getResrce())
				.andDateTimeBetween(resourceTimeLogPO.getStartDateTime(), resourceTimeLogPO.getEndDateTime())
				.andSiteEqualTo(resourceTimeLogPO.getSite()).andReasonCodeEqualTo(reasonCode)
				.andActionCodeEqualTo("START");
		example.setOrderByClause("DATE_TIME");
		List<ResourceReasonLogPO> ret = resourceReasonLogPOMapper.selectByExample(example);
		return ret;
	}

	public List<ResourceReasonLogPO> readRecordsByStartTime(ResourceTimeLogPO resourceTimeLogPO, String reasonCode) {
		ResrceBOHandle resrceBOHandle = new ResrceBOHandle(resourceTimeLogPO.getResourceBo());
		ResourceReasonLogPOExample example = new ResourceReasonLogPOExample();
		Criteria criteria = example.createCriteria();
		criteria.andResrceEqualTo(resrceBOHandle.getResrce())
				.andDateTimeBetween(resourceTimeLogPO.getStartDateTime(), resourceTimeLogPO.getEndDateTime())
				.andSiteEqualTo(resourceTimeLogPO.getSite()).andReasonCodeEqualTo(reasonCode);
		example.setOrderByClause("DATE_TIME");
		List<ResourceReasonLogPO> ret = resourceReasonLogPOMapper.selectByExample(example);
		return ret;
	}

	public List<ResourceReasonLogPO> readRecordsByStartTime(ResourceTimeLogPO resourceTimeLogPO, String reasonCode, String actionCode) {
		ResrceBOHandle resrceBOHandle = new ResrceBOHandle(resourceTimeLogPO.getResourceBo());
		ResourceReasonLogPOExample example = new ResourceReasonLogPOExample();
		Criteria criteria = example.createCriteria();
		criteria.andResrceEqualTo(resrceBOHandle.getResrce())
				.andDateTimeBetween(resourceTimeLogPO.getStartDateTime(), resourceTimeLogPO.getEndDateTime())
				.andSiteEqualTo(resourceTimeLogPO.getSite()).andReasonCodeEqualTo(reasonCode)
				.andActionCodeEqualTo(actionCode);
		example.setOrderByClause("DATE_TIME");
		List<ResourceReasonLogPO> ret = resourceReasonLogPOMapper.selectByExample(example);
		return ret;
	}

	public List<ResourceReasonLogPO> readRecordsByStartTimeByResourceReasonCodeLog(ResourceReasonCodeLogPO resourceReasonCodeLogPO, String actionCode) {
		ResrceBOHandle resrceBOHandle = new ResrceBOHandle(resourceReasonCodeLogPO.getResourceBo());
		ResourceReasonLogPOExample example = new ResourceReasonLogPOExample();
		Criteria criteria = example.createCriteria();
		criteria.andResrceEqualTo(resrceBOHandle.getResrce())
				.andDateTimeBetween(resourceReasonCodeLogPO.getStartDateTime(), resourceReasonCodeLogPO.getEndDateTime())
				.andSiteEqualTo(resourceReasonCodeLogPO.getSite()).andReasonCodeEqualTo(resourceReasonCodeLogPO.getReasonCode())
				.andActionCodeEqualTo(actionCode);
		example.setOrderByClause("DATE_TIME");
		List<ResourceReasonLogPO> ret = resourceReasonLogPOMapper.selectByExample(example);
		return ret;
	}

	public List<ResourceReasonLogPO> readRecordsByTimeRange(Date startTime, Date endTime, String resrce, String site,
			String reasonCode) {
		ResourceReasonLogPOExample example = new ResourceReasonLogPOExample();
		Criteria criteria = example.createCriteria();
		criteria.andResrceEqualTo(resrce).andDateTimeBetween(startTime, endTime).andSiteEqualTo(site)
				.andReasonCodeEqualTo(reasonCode);
		List<ResourceReasonLogPO> ret = resourceReasonLogPOMapper.selectByExample(example);
		return ret;
	}
}
