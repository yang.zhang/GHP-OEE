package mhp.oee.component.production;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.NotFoundExistingRecordException;
import mhp.oee.common.handle.StdCycleTimeBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.StdCycleTimeAndResrceAndResourceTypeResourcePOMapper;
import mhp.oee.dao.gen.StdCycleTimePOMapper;
import mhp.oee.po.extend.StdCycleTimeAndResrceAndResourceTypeResourcePO;
import mhp.oee.po.gen.StdCycleTimePO;
import mhp.oee.po.gen.StdCycleTimePOExample;
import mhp.oee.po.gen.StdCycleTimePOExample.Criteria;
import mhp.oee.utils.Utils;
import sun.java2d.pipe.SpanShapeRenderer.Simple;

@Component
public class StdCycleTimeComponent {

    public static final String DEFAULT_ITEM_REVISION = "#";

    private static final String STD_CYCLETIME = "std cycle time table";

    @Autowired
    private StdCycleTimePOMapper stdCycleTimeMapper;

    @Autowired
    StdCycleTimeAndResrceAndResourceTypeResourcePOMapper mapper;

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    public List<StdCycleTimePO> readStdCycleTime(String site, String device, String item, Date infiniteDate) {
        StdCycleTimePOExample example = new StdCycleTimePOExample();
        Criteria criteria = example.createCriteria();
        if (!Utils.isEmpty(site)) {
            criteria.andSiteEqualTo(site);
        }
        if (!Utils.isEmpty(device)) {
            criteria.andResrceEqualTo(device);
        }
        if (!Utils.isEmpty(item)) {
            criteria.andItemEqualTo(item);
        }

        if (infiniteDate != null) {
            criteria.andEndDateEqualTo(infiniteDate);
        }

        return stdCycleTimeMapper.selectByExample(example);
    }

    public void createStdCycleTimeList(List<StdCycleTimePO> list) throws NotFoundExistingRecordException, ExistingRecordException  {
        for (StdCycleTimePO vo : list) {
            StdCycleTimePO existingStdCycleTimeRecord = this.getExistingStdCycleTimeRecord(vo);
            if( existingStdCycleTimeRecord != null ) {
                if (existingStdCycleTimeRecord.getPpmTheory().doubleValue() == vo.getPpmTheory().doubleValue()) {
                    continue;
                }
                existingStdCycleTimeRecord.setEndDate(vo.getStartDate());
                updateExistingStdCycleTimeRecord(existingStdCycleTimeRecord);
            }
            this.createStdCycleTimeByVO(vo);
        }
    }

    public void createStdCycleTimeList_insertSql(List<StdCycleTimePO> list,Map<String,String> sql_map) throws NotFoundExistingRecordException, ExistingRecordException  {    
    	for (StdCycleTimePO vo : list) {
            StdCycleTimePO existingStdCycleTimeRecord = this.getExistingStdCycleTimeRecord(vo);
            if( existingStdCycleTimeRecord != null ) {
                if (existingStdCycleTimeRecord.getPpmTheory().doubleValue() == vo.getPpmTheory().doubleValue()) {
                    continue;
                }
                existingStdCycleTimeRecord.setEndDate(vo.getStartDate());
                String update_sql = updateExistingStdCycleTimeRecord_updateSql(existingStdCycleTimeRecord);                             
                //sqllist.add(update_sql);
                sql_map.put("update"+existingStdCycleTimeRecord.getHandle(), update_sql);
            }
           // this.createStdCycleTimeByVO(vo);
            String sql = this.createStdCycleTimeByVO_insertSql(vo);           
            sql_map.put(vo.getHandle(), sql);                                
        }
    	
    }
    
    public void createStdCycleTime(StdCycleTimePO po) throws NotFoundExistingRecordException, ExistingRecordException {
        StdCycleTimePO existingStdCycleTimeRecord = this.getExistingStdCycleTimeRecord(po);
        if( existingStdCycleTimeRecord != null ) {
            if (existingStdCycleTimeRecord.getPpmTheory().doubleValue() == po.getPpmTheory().doubleValue()) {
                return;
            }
            existingStdCycleTimeRecord.setEndDate(po.getStartDate());
            updateExistingStdCycleTimeRecord(existingStdCycleTimeRecord);
        }
        this.createStdCycleTimeByVO(po);
    }
    
    public String createStdCycleTime_insertSql(StdCycleTimePO po) throws NotFoundExistingRecordException, ExistingRecordException {
        StdCycleTimePO existingStdCycleTimeRecord = this.getExistingStdCycleTimeRecord(po);
        if( existingStdCycleTimeRecord != null ) {
            if (existingStdCycleTimeRecord.getPpmTheory().doubleValue() == po.getPpmTheory().doubleValue()) {
                return "";
            }
            existingStdCycleTimeRecord.setEndDate(po.getStartDate());
            updateExistingStdCycleTimeRecord(existingStdCycleTimeRecord);
        }
       return this.createStdCycleTimeByVO_insertSql(po);
    }
    
    public void updateExistingStdCycleTimeRecord(StdCycleTimePO po) throws NotFoundExistingRecordException {
        po.setModifiedUser(ContextHelper.getContext().getUser());
        StdCycleTimePOExample example = new StdCycleTimePOExample();
       // example.createCriteria().andHandleEqualTo(po.getHandle()).andModifiedDateTimeEqualTo(po.getModifiedDateTime());
        example.createCriteria().andHandleEqualTo(po.getHandle());
        int ret = stdCycleTimeMapper.updateByExample(po, example);
        if (ret != 1) {
            logger.error(STD_CYCLETIME + " : Not Found Record: "+gson.toJson(po));
            throw new NotFoundExistingRecordException();
        }
                
    }

    public String updateExistingStdCycleTimeRecord_updateSql(StdCycleTimePO po) throws NotFoundExistingRecordException {               
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	String update_sql = " update std_cycle_time "
				   	 +" set HANDLE ='"+po.getHandle()+"',"
				     +"  SITE = '"+po.getSite()+"',"
				     +"  ITEM = '"+po.getItem()+"',"
				     +"   OPERATION = '"+po.getOperation()+"',"
				     +"  RESRCE = '"+po.getResrce()+"',"
				     +"  STRT = '"+po.getStrt()+"',"
				     +"  PPM_THEORY = "+po.getPpmTheory()+","
				     +"  STD_CYCLE_TIME = "+po.getStdCycleTime()+","
				     +"  START_DATE = TO_DATE('"+sdf.format(po.getStartDate())+"'),"
				     +"  END_DATE = TO_DATE('"+sdf.format(po.getEndDate())+"'),"
				     +"  MODIFIED_DATE_TIME = NOW(),"
				     +"  MODIFIED_USER = '"+po.getModifiedUser()+"'"
				     + " where HANDLE ='"+po.getHandle()+"' ";
				     		/*+ "and MODIFIED_DATE_TIME=TO_SECONDDATE('"+sdf.format(po.getModifiedDateTime())+"') ";*/
        return update_sql;
    }
    
    private void createStdCycleTimeByVO(StdCycleTimePO po) throws ExistingRecordException  {
        //save the new one
        String handle = (new StdCycleTimeBOHandle(po.getSite(), po.getItem(), po.getOperation(), po.getResrce(), po.getStrt())).getValue();
        StdCycleTimePO existingPO = stdCycleTimeMapper.selectByPrimaryKey(handle);
        if (existingPO != null) {
            throw new ExistingRecordException(STD_CYCLETIME, gson.toJson(existingPO));
        }
        po.setHandle(handle);   // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());

        int ret = stdCycleTimeMapper.insert(po);
        if (ret != 1) {
            logger.error(STD_CYCLETIME + " : Existing record: "+gson.toJson(po));
            throw new ExistingRecordException(STD_CYCLETIME, gson.toJson(po));        }
    }
    
    private String createStdCycleTimeByVO_insertSql(StdCycleTimePO po) throws ExistingRecordException  {
        //save the new one
        String handle = (new StdCycleTimeBOHandle(po.getSite(), po.getItem(), po.getOperation(), po.getResrce(), po.getStrt())).getValue();
        StdCycleTimePO existingPO = stdCycleTimeMapper.selectByPrimaryKey(handle);
        if (existingPO != null) {
            throw new ExistingRecordException(STD_CYCLETIME, gson.toJson(existingPO));
        }
        po.setHandle(handle);   // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "insert into STD_CYCLE_TIME (handle,site,item,operation,resrce,strt,ppm_Theory,std_Cycle_Time,start_Date,end_Date,created_Date_Time,modified_Date_Time,modified_User) "
        		+ "values('"+po.getHandle()+"','"+po.getSite()+"','"+po.getItem()+"','"+po.getOperation()+"','"+po.getResrce()+"','"+po.getStrt()+"','"+po.getPpmTheory()+"',"+po.getStdCycleTime()+",TO_DATE('"+sdf.format(po.getStartDate())+"', 'YYYY-MM-DD'),TO_DATE('"+sdf.format(po.getEndDate())+"', 'YYYY-MM-DD'),NOW(),NOW(),'"+po.getModifiedUser()+"')";

        //int ret = stdCycleTimeMapper.insert(po);
        /*if (ret != 1) {
            logger.error(STD_CYCLETIME + " : Existing record: "+gson.toJson(po));
            throw new ExistingRecordException(STD_CYCLETIME, gson.toJson(po));        }*/
        
        return sql;
    }

    public StdCycleTimePO getExistingStdCycleTimeRecord(StdCycleTimePO vo) {
        List<StdCycleTimePO> readStdCycleTime = this.readStdCycleTime(vo.getSite(), vo.getResrce(), vo.getItem(), Utils.getInfiniteDate());
        if(readStdCycleTime.size() != 0) {
            return readStdCycleTime.get(0);
        } else {
            return null;
        }

    }


    public StdCycleTimePO getExistingStdCycleTimeRecordByKey(String handle) {
       return stdCycleTimeMapper.selectByPrimaryKey(handle);
    }

    public List<StdCycleTimePO> search(String site, String item, String resrce, Date date, int index, int count) {
        StdCycleTimePOExample example = new StdCycleTimePOExample();
        Criteria criteria = example.createCriteria();

        criteria.andSiteEqualTo(site);
        criteria.andResrceEqualTo(resrce);
        criteria.andItemEqualTo(item);
        criteria.andStartDateLessThanOrEqualTo(date);
        criteria.andEndDateGreaterThan(date);
        example.setOrderByClause("RESRCE");
        example.setLimit(count);
        example.setOffset(Utils.getOffset(count, index));

        return stdCycleTimeMapper.selectByExample(example);
    }
  //ppm理论维护一键修改
    public List<StdCycleTimePO> searchPPM(String site, String item, String resrce, Date date) {
        StdCycleTimePOExample example = new StdCycleTimePOExample();
        Criteria criteria = example.createCriteria();

        criteria.andSiteEqualTo(site);
        criteria.andResrceEqualTo(resrce);
        criteria.andItemEqualTo(item);
        criteria.andStartDateLessThanOrEqualTo(date);
        criteria.andEndDateGreaterThan(date);
        example.setOrderByClause("RESRCE");

        return stdCycleTimeMapper.selectByExample(example);
    }


    public int searchCycleTimeTotalNumber(String site, String item, String resrce, Date date) {
        StdCycleTimePOExample example = new StdCycleTimePOExample();
        Criteria criteria = example.createCriteria();

        criteria.andSiteEqualTo(site);
        criteria.andResrceEqualTo(resrce);
        criteria.andItemEqualTo(item);
        criteria.andStartDateLessThanOrEqualTo(date);
        criteria.andEndDateGreaterThan(date);
        example.setOrderByClause("RESRCE");

        return stdCycleTimeMapper.countByExample(example);
    }

    public List<StdCycleTimeAndResrceAndResourceTypeResourcePO> search(String site, String item, String resourceTypeBo, String line, String workCenter, Date date,
            int index, int count) {
        //line is empty
        if(Utils.isEmpty(line)){
            return mapper.getStdCycleTimeAndResrceAndResourceTypeResource(site, item, resourceTypeBo, null, workCenter, date, count, Utils.getOffset(count, index));
        //line is multiple choice
        }else{
            String newLine="";
            String arrayMulLine[]=line.split(",");
            for(int i=0;i<arrayMulLine.length;i++){
                newLine+="'"+arrayMulLine[i]+"',";
            }
            newLine=newLine.substring(0, newLine.length()-1);
            return mapper.getStdCycleTimeAndResrceAndResourceTypeResource(site, item, resourceTypeBo, newLine, workCenter, date, count, Utils.getOffset(count, index));
        }
        
    }
    //ppm理论维护一键修改
    public List<StdCycleTimeAndResrceAndResourceTypeResourcePO> searchPPM(String site, String item, String resourceTypeBo, String line, String workCenter, Date date) {
        //line is empty
        if(Utils.isEmpty(line)){
            return mapper.changePPM(site, item, resourceTypeBo, null, workCenter, date);
        //line is multiple choice
        }else{
            String newLine="";
            String arrayMulLine[]=line.split(",");
            for(int i=0;i<arrayMulLine.length;i++){
                newLine+="'"+arrayMulLine[i]+"',";
            }
            newLine=newLine.substring(0, newLine.length()-1);
            return mapper.changePPM(site, item, resourceTypeBo, newLine, workCenter, date);
        }
        
    }

    public int searchCycleTimeTotalNumber(String site, String item, String resourceBOhandle, String line,
            String workCenter, Date date) {
        if(Utils.isEmpty(line)){
            return mapper.getStdCycleTimeAndResrceAndResourceTypeResourceCount(site, item, resourceBOhandle, null, workCenter, date);
        }else{
            String newLine="";
            String arrayMulLine[]=line.split(",");
            for(int i=0;i<arrayMulLine.length;i++){
                newLine+="'"+arrayMulLine[i]+"',";
            }
            newLine=newLine.substring(0, newLine.length()-1);
            return mapper.getStdCycleTimeAndResrceAndResourceTypeResourceCount(site, item, resourceBOhandle,newLine, workCenter, date);            
        }
    }
}