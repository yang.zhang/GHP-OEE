package mhp.oee.component.production;

import mhp.oee.common.handle.ResourceReasonCodeLogBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.dao.gen.ResourceReasonCodeLogPOMapper;
import mhp.oee.dao.gen.ResourceTimeLogPOMapper;
import mhp.oee.po.gen.ResourceReasonCodeLogPO;
import mhp.oee.po.gen.ResourceReasonCodeLogPOExample;
import mhp.oee.po.gen.ResourceTimeLogPO;
import mhp.oee.po.gen.ResourceTimeLogPOExample;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class ResourceReasonCodeLogComponent {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private ResourceReasonCodeLogPOMapper resourceReasonCodeLogPOMapper;
    @Autowired
    private ResourceTimeLogPOMapper resourceTimeLogPOMapper;

    public String createOrUpdateResourceReasonCodeLog(ResourceReasonCodeLogPO resourceReasonCodeLogPO) {
        String handle = new ResourceReasonCodeLogBOHandle(resourceReasonCodeLogPO.getSite(),
                resourceReasonCodeLogPO.getResourceBo(), resourceReasonCodeLogPO.getStrt()).getValue();
        resourceReasonCodeLogPO.setHandle(handle);
//        resourceReasonCodeLogPO.setModifiedUser("JOB");

        int i = 0;
        String sql=null;
        try {
            ResourceReasonCodeLogPO po = resourceReasonCodeLogPOMapper.selectByPrimaryKey(handle);
            if (null == po) {
                //i = resourceReasonCodeLogPOMapper.insert(resourceReasonCodeLogPO);
            	
            	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                sql ="insert into resource_reason_code_log (HANDLE, SITE, RESOURCE_BO, "
                	    +"  STRT, RESOURCE_STATE, START_DATE_TIME, "
                	    +"    END_DATE_TIME, ELAPSED_TIME, REASON_CODE, "
                	     +"   COMMENT, MAINTENANCE_NOTIFICATION, MAINTENANCE_ORDER, "
                	    +"    ME_USER_ID, CREATED_DATE_TIME, MODIFIED_DATE_TIME, "
                	    +"    MODIFIED_USER) "
                	    +"  values ('"+resourceReasonCodeLogPO.getHandle()+"','"+resourceReasonCodeLogPO.getSite()+"','"+resourceReasonCodeLogPO.getResourceBo()+"'," 
                	     +"  '"+resourceReasonCodeLogPO.getStrt()+"','"+resourceReasonCodeLogPO.getResourceState()+"', TO_SECONDDATE('"+sdf.format(resourceReasonCodeLogPO.getStartDateTime())+"'), TO_SECONDDATE('"
                	     +sdf.format(resourceReasonCodeLogPO.getEndDateTime())+"'),"+resourceReasonCodeLogPO.getElapsedTime()+", '"+resourceReasonCodeLogPO.getReasonCode()+"', "
                	     +"  '"+nullOrStr(resourceReasonCodeLogPO.getComment())+"', '"+nullOrStr(resourceReasonCodeLogPO.getMaintenanceNotification())+"','"+nullOrStr(resourceReasonCodeLogPO.getMaintenanceOrder())+"'," 
                	      +" '"+nullOrStr(resourceReasonCodeLogPO.getMeUserId())+"', NOW(), NOW(), "
                	      +"  '"+resourceReasonCodeLogPO.getModifiedUser()+"')";
            } else {
                //i = resourceReasonCodeLogPOMapper.updateByPrimaryKey(resourceReasonCodeLogPO);
            	resourceReasonCodeLogPOMapper.updateByPrimaryKey(resourceReasonCodeLogPO);
            }
        } catch (Exception e) {
            logger.error("job insert resourceReasonCodeLog error: " + e.getMessage());
        }
        return sql;
    }

    private String nullOrStr(String str){
    	return str==null?"":str;
    }
    
    public void deleteResourceReasonCodeLogByRange(Date recognizeStartDateTime, Date recognizeEndDateTime, String resourceBo) {
            ResourceReasonCodeLogPOExample example = new ResourceReasonCodeLogPOExample();
            example.createCriteria()
                    .andStartDateTimeBetween(recognizeStartDateTime, recognizeEndDateTime)
                    .andResourceBoEqualTo(resourceBo);
            resourceReasonCodeLogPOMapper.deleteByExample(example);
    }

    public Date findRecognizeStartTime(Date beginDateTime, String resourceBo) {
        ResourceReasonCodeLogPO rrcl = findRightFirstResourceReasonCodeLog(beginDateTime, resourceBo);
        if (rrcl == null) {
            return beginDateTime;
        }
        beginDateTime = rrcl.getEndDateTime();
        ResourceTimeLogPOExample example = new ResourceTimeLogPOExample();
        example.createCriteria()
                .andStartDateTimeLessThan(beginDateTime)
                .andEndDateTimeGreaterThan(beginDateTime)
                .andResourceBoEqualTo(resourceBo);
        example.setOrderByClause("END_DATE_TIME");
        example.setLimit(1);
        List<ResourceTimeLogPO> rtlList = resourceTimeLogPOMapper.selectByExample(example);
        if (rtlList != null && rtlList.size() > 0) {
            ResourceTimeLogPO rtl = rtlList.get(0);
            return rtl.getEndDateTime();
        } else {
            return beginDateTime;
        }
    }

    public ResourceReasonCodeLogPO findRightFirstResourceReasonCodeLog(Date beginDateTime, String resourceBo) {
        ResourceReasonCodeLogPOExample example = new ResourceReasonCodeLogPOExample();
        example.createCriteria()
                .andStartDateTimeLessThan(beginDateTime)
                .andEndDateTimeGreaterThan(beginDateTime)
                .andResourceBoEqualTo(resourceBo);
        example.setOrderByClause("END_DATE_TIME");
        example.setLimit(1);
        List<ResourceReasonCodeLogPO> rrclList = resourceReasonCodeLogPOMapper.selectByExample(example);
        if (rrclList != null && rrclList.size() > 0) {
            return rrclList.get(0);
        } else {
            return null;
        }
    }

//
//    public int createResourceTimeLog(ResourceTimeLogVO resourceTimeLogVO) throws BusinessException {
//        if (!resourceComponent.isEnabledResource(resourceTimeLogVO.getResourceBo())) {
//            throw new InvalidResourceException();
//        }
//
//        ResourceTimeLogPO po = Utils.copyObjectProperties(resourceTimeLogVO, ResourceTimeLogPO.class);
//        ResourceTimeLogBOHandle handle = new ResourceTimeLogBOHandle(po.getSite(),po.getResourceBo(), po.getStrt());
//        po.setHandle(handle.getValue());
//        po.setModifiedUser(ContextHelper.getContext().getUser());
//        return resourceTimeLogPOMapper.insert(po);
//    }



}
