package mhp.oee.component.production;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.handle.RealtimePpmBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.RealtimePpmPOMapper;
import mhp.oee.po.gen.RealtimePpmPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.RealtimePpmVO;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
@Component
public class RealtimePpmComponent {

    @Autowired
    private RealtimePpmPOMapper realtimePpmPOMapper;

    public boolean existRealtimePpm(String handle) throws BusinessException {
        RealtimePpmPO po = realtimePpmPOMapper.selectByPrimaryKey(handle);
        return (po != null) ? true : false;
    }

    public void createRealtimePpm(RealtimePpmVO vo) throws BusinessException {
        RealtimePpmPO po = Utils.copyObjectProperties(vo, RealtimePpmPO.class);
        String handle = new RealtimePpmBOHandle(vo.getResourceBo()).getValue();
        po.setHandle(handle);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = realtimePpmPOMapper.insert(po);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "createRealtimePpm");
        }
    }

    public void updateRealtimePpm(RealtimePpmVO vo) throws BusinessException {
        RealtimePpmPO po = Utils.copyObjectProperties(vo, RealtimePpmPO.class);
        String handle = new RealtimePpmBOHandle(vo.getResourceBo()).getValue();
        po.setHandle(handle);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = realtimePpmPOMapper.updateByPrimaryKey(po);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "updateRealtimePpm");
        }
    }
}
