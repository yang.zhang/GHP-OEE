package mhp.oee.component.plant;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.DeleteErrorException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.PlcObjectBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.PlcCategoryAndPlcObjectPOMapper;
import mhp.oee.dao.gen.PlcObjectPOMapper;
import mhp.oee.dao.gen.ResourcePlcPOMapper;
import mhp.oee.po.extend.PlcObjectAndPlcCategoryPO;
import mhp.oee.po.gen.PlcObjectPO;
import mhp.oee.po.gen.PlcObjectPOExample;
import mhp.oee.po.gen.ResourcePlcPO;
import mhp.oee.po.gen.ResourcePlcPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.PlcObjectAndPlcCategoryVO;
import mhp.oee.vo.PlcObjectVO;

@Component
public class PlcObjectComponent {

    @InjectableLogger
    private static Logger logger;
    private static final String PLC_OBJECT = "PLC_OBJECT";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    PlcObjectPOMapper plcObjectPOMapper;

    @Autowired
    private ResourcePlcPOMapper resourcePlcPOMapper;

    @Autowired
    private PlcCategoryAndPlcObjectPOMapper plcCategoryAndPlcObjectPOMapper;

    public List<PlcObjectAndPlcCategoryVO> readAllPlcObjectPage(String site, String plcCategory, int limit, int offset) {
        List<PlcObjectAndPlcCategoryPO> pos = plcCategoryAndPlcObjectPOMapper.selectPlcObjectAndPlcCategory(site, plcCategory, limit, offset);
        List<PlcObjectAndPlcCategoryVO> vos = Utils.copyListProperties(pos, PlcObjectAndPlcCategoryVO.class);
        return vos;
    }

    public List<PlcObjectVO> readAllPlcObject(String site, String plcCategory) {
        PlcObjectPOExample example = new PlcObjectPOExample();
        if (Utils.isEmpty(plcCategory)) {
            example.createCriteria().andSiteEqualTo(site);
        } else {
            example.createCriteria().andSiteEqualTo(site).andCategoryEqualTo(plcCategory);
        }
        List<PlcObjectPO> pos = plcObjectPOMapper.selectByExample(example);
        List<PlcObjectVO> vos = Utils.copyListProperties(pos, PlcObjectVO.class);
        return vos;
    }

    public int readAllPlcObjectCount(String site, String plcCategory) {
        int vos = plcCategoryAndPlcObjectPOMapper.selectPlcObjectAndPlcCategoryCount(site, plcCategory);
        return vos;
    }

    public boolean isPlcObjectUsed(String site, String plcObject) {
        ResourcePlcPOExample rpExample = new ResourcePlcPOExample();
        rpExample.createCriteria().andSiteEqualTo(site).andPlcObjectEqualTo(plcObject);

        List<ResourcePlcPO> rpPOs = this.resourcePlcPOMapper.selectByExample(rpExample);
        if (!Utils.isEmpty(rpPOs)) {
            return true;
        }
        return false;
    }

    public PlcObjectPO existPlcObject(String site, String plcObject) {
        String handle = new PlcObjectBOHandle(site, plcObject).getValue();
        PlcObjectPO rpPOs = plcObjectPOMapper.selectByPrimaryKey(handle);
        return rpPOs;
    }

    public PlcObjectPO getPlcObjectBySiteCategoryObject(String site, String category, String object) {
        PlcObjectPOExample example = new PlcObjectPOExample();
        example.createCriteria().andSiteEqualTo(site).andCategoryEqualTo(category).andPlcObjectEqualTo(object);
        List<PlcObjectPO> list = plcObjectPOMapper.selectByExample(example);
        if(list != null && list.size() != 0) {
            return list.get(0);
        } else {
            return null;
        }
    }

    public String createPlcObject(PlcObjectVO vo) throws BusinessException {
        String handle = new PlcObjectBOHandle(vo.getSite(), vo.getPlcObject()).getValue();
        PlcObjectPO existingPO = plcObjectPOMapper.selectByPrimaryKey(handle);
        if (existingPO != null) {
            logger.error("PlcObjectComponent.createPlcObject() : Existing PLC_OBJECT record");
            throw new ExistingRecordException(PLC_OBJECT, gson.toJson(existingPO));
        }
        PlcObjectPO po = Utils.copyObjectProperties(vo, PlcObjectPO.class);
        po.setHandle(handle); // this is create action, view does not provide
                              // HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = plcObjectPOMapper.insert(po);
        if (ret != 1) {
            logger.error("PlcObjectComponent.createPlcObject() : Existing PLC_OBJECT record");
            existingPO = plcObjectPOMapper.selectByPrimaryKey(handle);
            throw new ExistingRecordException(PLC_OBJECT, gson.toJson(existingPO));
        }
        return handle;
    }

    public void updatePlcObject(PlcObjectVO vo) throws BusinessException {
        PlcObjectPO po = Utils.copyObjectProperties(vo, PlcObjectPO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        PlcObjectPOExample example = new PlcObjectPOExample();
        example.createCriteria().andHandleEqualTo(vo.getHandle()).andModifiedDateTimeEqualTo(vo.getModifiedDateTime());
        int ret = plcObjectPOMapper.updateByExample(po, example);
        if (ret != 1) {
            logger.error("PlcObjectComponent.updatePlcObject() : updatePlcObject");
            throw new UpdateErrorException(PLC_OBJECT, gson.toJson(po));
        }
    }

    public int deletePlcObject(PlcObjectVO vo) throws BusinessException {
        if (isPlcObjectUsed(vo.getSite(), vo.getPlcObject())) {
            logger.error("PlcObjectComponent.deletePlcObject() : deletePlcObject");
            throw new DeleteErrorException(PLC_OBJECT, gson.toJson(vo));
        }
        // TODO: add modifiedDateTime as delete criteria
        // if ret==0, check if it was updated by other user in advance.
        // updated by other user => throw exception
        // record not existed => ignore it ??
        return plcObjectPOMapper.deleteByPrimaryKey(vo.getHandle());
    }
}
