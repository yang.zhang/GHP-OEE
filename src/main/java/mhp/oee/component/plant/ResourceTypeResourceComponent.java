package mhp.oee.component.plant;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.RecordCorruptionException;
import mhp.oee.common.handle.ResourceTypeResourceBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.ResourceTypeResourcePOMapper;
import mhp.oee.po.gen.ResourceTypeResourcePO;
import mhp.oee.po.gen.ResourceTypeResourcePOExample;
import mhp.oee.utils.Utils;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
@Component
public class ResourceTypeResourceComponent {

    @Autowired
    private ResourceTypeResourcePOMapper resourceTypeResourcePOMapper;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();


    public boolean existResourceTypeResource(String resourceTypeBo, String resourceBo) throws BusinessException {
        String handle = new ResourceTypeResourceBOHandle(resourceTypeBo, resourceBo).getValue();
        ResourceTypeResourcePO po = resourceTypeResourcePOMapper.selectByPrimaryKey(handle);
        return (po != null) ? true : false;
    }

    public void createResourceTypeResource(String resourceTypeRef, String resourceRef) throws BusinessException {
        String handle = new ResourceTypeResourceBOHandle(resourceTypeRef, resourceRef).getValue();
        ResourceTypeResourcePO po = new ResourceTypeResourcePO();
        po.setHandle(handle);
        po.setResourceTypeBo(resourceTypeRef);
        po.setResourceBo(resourceRef);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = resourceTypeResourcePOMapper.insert(po);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "createResourceTypeResource");
        }
    }

    public void updateResourceTypeResource(String resourceTypeBo, String resourceBo) throws BusinessException {
        String handle = new ResourceTypeResourceBOHandle(resourceTypeBo, resourceBo).getValue();
        ResourceTypeResourcePO po = new ResourceTypeResourcePO();
        po.setHandle(handle);
        po.setResourceTypeBo(resourceTypeBo);
        po.setResourceBo(resourceBo);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = resourceTypeResourcePOMapper.updateByPrimaryKey(po);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "updateResourceTypeResource");
        }
    }

    public int deleteResourceTypeResourceByResourceBo(String resourceRef) {
        ResourceTypeResourcePOExample example = new ResourceTypeResourcePOExample();
        example.createCriteria().andResourceBoEqualTo(resourceRef);
        return resourceTypeResourcePOMapper.deleteByExample(example);
    }

    public List<ResourceTypeResourcePO> readByResourceTypeBo(String resourceTypeRef) throws RecordCorruptionException {
        ResourceTypeResourcePOExample example = new ResourceTypeResourcePOExample();
        example.createCriteria().andResourceTypeBoEqualTo(resourceTypeRef);
        return resourceTypeResourcePOMapper.selectByExample(example);
    }

    public ResourceTypeResourcePO readByResourceBo(String resourceRef) throws RecordCorruptionException {
        ResourceTypeResourcePOExample example = new ResourceTypeResourcePOExample();
        example.createCriteria().andResourceBoEqualTo(resourceRef);
        List<ResourceTypeResourcePO> pos = resourceTypeResourcePOMapper.selectByExample(example);
        if (Utils.isEmpty(pos)) {
            return null;
        } else if (pos.size() > 1) {
            // RESOURCE_TYPE : RESOURCE = 1 : n mapping
            throw new RecordCorruptionException(gson.toJson(pos));
        } else {
            return pos.get(0);
        }
    }

    public List<ResourceTypeResourcePO> readByExample(ResourceTypeResourcePOExample example) {
        return resourceTypeResourcePOMapper.selectByExample(example);
    }

}
