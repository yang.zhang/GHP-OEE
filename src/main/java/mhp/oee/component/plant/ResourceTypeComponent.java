package mhp.oee.component.plant;

import java.util.List;

import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.NoSuchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.ResourceTypePOMapper;
import mhp.oee.po.gen.ResourceTypePO;
import mhp.oee.po.gen.ResourceTypePOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceTypeConditionVO;
import mhp.oee.vo.ResourceTypeVO;

@Component
public class ResourceTypeComponent {

    @Autowired
    private ResourceTypePOMapper resourceTypePOMapper;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();


    public boolean existResourceType(String site, String ResourceType) {
        String handle = new ResourceTypeBOHandle(site, ResourceType).getValue();
        ResourceTypePO po = resourceTypePOMapper.selectByPrimaryKey(handle);
        return (po != null) ? true : false;
    }

    public ResourceTypePO getResourceTypeByPrimaryKey(String site, String ResourceType) {
        String handle = new ResourceTypeBOHandle(site, ResourceType).getValue();
        return resourceTypePOMapper.selectByPrimaryKey(handle);
    }

    public String createResourceType(ResourceTypeVO ResourceTypeVO) throws BusinessException {
        String handle = new ResourceTypeBOHandle(ResourceTypeVO.getSite(), ResourceTypeVO.getResourceType()).getValue();
        ResourceTypePO existingPO = resourceTypePOMapper.selectByPrimaryKey(handle);
        if (existingPO != null) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "Existing RESOURCE_TYPE record : " + gson.toJson(existingPO));
        }

        ResourceTypePO po = Utils.copyObjectProperties(ResourceTypeVO, ResourceTypePO.class);
        po.setHandle(handle);   // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = resourceTypePOMapper.insert(po);
        if (ret != 1) {
            existingPO = resourceTypePOMapper.selectByPrimaryKey(handle);
            throw new BusinessException(ErrorCodeEnum.BASIC, "Existing RESOURCE_TYPE record : " + gson.toJson(existingPO));
        }
        return handle;
    }

    public List<ResourceTypeVO> readEnabledResourceTypeBySite(String site) {
        ResourceTypePOExample example = new ResourceTypePOExample();
        example.createCriteria().andSiteEqualTo(site).andEnabledEqualTo("true");
        example.setOrderByClause("RESOURCE_TYPE");
        List<ResourceTypePO> pos = resourceTypePOMapper.selectByExample(example);
        List<ResourceTypeVO> vos = Utils.copyListProperties(pos, ResourceTypeVO.class);
        return vos;
    }

    public void updateResourceType(ResourceTypeVO ResourceTypeVO) throws BusinessException {
        ResourceTypePO po = Utils.copyObjectProperties(ResourceTypeVO, ResourceTypePO.class);
        po.setHandle(new ResourceTypeBOHandle(po.getSite(), po.getResourceType()).getValue());
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = resourceTypePOMapper.updateByPrimaryKey(po);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "updateResourceType");
        }
    }

    public List<ResourceTypeConditionVO> readAllResourceTypeBySite(String site) {
        ResourceTypePOExample example = new ResourceTypePOExample();
        example.createCriteria().andSiteEqualTo(site).andEnabledEqualTo("true");
        List<ResourceTypePO> pos = resourceTypePOMapper.selectByExample(example);
        List<ResourceTypeConditionVO> vos = Utils.copyListProperties(pos, ResourceTypeConditionVO.class);
        return vos;
    }

    public ResourceTypePO readResourceTypeByPrimaryKey(String handle) {
        return resourceTypePOMapper.selectByPrimaryKey(handle);
    }

    public List<ResourceTypeConditionVO> readAllResourceTypeLikeResrce(String site, String resrceType) {
        ResourceTypePOExample example = new ResourceTypePOExample();
        //多个关键词模糊搜索
        example.createCriteria().andSiteEqualTo(site).andResourceTypeLikeMore(
        		Utils.ConvertStrtoArrayLike(resrceType));
        example.setOrderByClause("RESOURCE_TYPE");
        List<ResourceTypePO> pos = resourceTypePOMapper.selectByExample(example);
        List<ResourceTypeConditionVO> vos = Utils.copyListProperties(pos, ResourceTypeConditionVO.class);
        return vos;
    }

	public List<ResourceTypePO> getResourceTypeByCondition(String site, String enable) throws NoSuchException {
        ResourceTypePOExample example = new ResourceTypePOExample();
        example.createCriteria().andSiteEqualTo(site).andEnabledEqualTo(enable);
        List<ResourceTypePO> resourceTypePOs = resourceTypePOMapper.selectByExample(example);
        if (resourceTypePOs.size() == 0) {
            throw new NoSuchException("resource type");
        }
        return resourceTypePOs;
    }



}
