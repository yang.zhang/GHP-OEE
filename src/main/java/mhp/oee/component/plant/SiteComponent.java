package mhp.oee.component.plant;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.handle.SiteBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.SitePOMapper;
import mhp.oee.po.gen.SitePO;
import mhp.oee.po.gen.SitePOExample;
import mhp.oee.utils.ExceptionHandler;
import mhp.oee.utils.Utils;
import mhp.oee.vo.SiteVO;

@Component
@Transactional(rollbackFor = { BusinessException.class })
public class SiteComponent {

    private static final String SITE = "SITE";

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private SitePOMapper sitePOMapper;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    public void createSite(SiteVO siteVO) throws BusinessException {
        String handle = new SiteBOHandle(siteVO.getSite()).getValue();
        SitePO existPO = sitePOMapper.selectByPrimaryKey(handle);
        if(existPO != null) {
            throw new ExistingRecordException(SITE, gson.toJson(existPO));
        }
        SitePO po = Utils.copyObjectProperties(siteVO, SitePO.class);
        po.setHandle(new SiteBOHandle(po.getSite()).getValue());
        int ret = sitePOMapper.insert(po);
        if (ret != 1) {
            throw new ExistingRecordException(SITE, gson.toJson(existPO));
        }
    }

    public SitePO readSiteByPkFields(String site) throws BusinessException {
        logger.debug(">> SiteComponent.readSiteByPkFields() : " + site);
        SitePO po = null;
        try {
            po = sitePOMapper.selectByPrimaryKey(new SiteBOHandle(site).getValue());
        } catch (RuntimeException ex) {
            BusinessException be = ExceptionHandler.handleRuntimeException(ex);
            logger.error("=> SiteComponent.readSiteByPkFields() : " + be.getMessage());
            throw be;
        }
        logger.debug("<< SiteComponent.readSiteByPkFields() : " + po);
        return po;
    }

    public void updateSiteByVO(SiteVO siteVO) throws BusinessException {
        logger.debug(">> SiteComponent.updateSiteByPkFields() : " + siteVO);

        SitePO po = Utils.copyObjectProperties(siteVO, SitePO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        SitePOExample example = new SitePOExample();
        example.createCriteria().andHandleEqualTo(po.getHandle()).andModifiedDateTimeEqualTo(siteVO.getModifiedDateTime());

        int ret = sitePOMapper.updateByExample(po, example);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "site record can not be found");
        }
    }

    public List<SiteVO> getAllSite() {
        List<SitePO> pos = sitePOMapper.selectByExample(new SitePOExample());
        List<SiteVO> vos = Utils.copyListProperties(pos, SiteVO.class);
        return vos;
    }

    public SiteVO getSiteDetails(String site) {
        SiteBOHandle handle = new SiteBOHandle(site);
        SitePO po = sitePOMapper.selectByPrimaryKey(handle.getValue());
        SiteVO vo = Utils.copyObjectProperties(po, SiteVO.class);
        return vo;
    }

}
