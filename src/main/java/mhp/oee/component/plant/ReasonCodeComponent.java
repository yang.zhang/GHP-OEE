package mhp.oee.component.plant;

import java.text.ParseException;
import java.util.*;

import mhp.oee.common.exception.*;
import mhp.oee.dao.gen.*;
import mhp.oee.po.extend.*;
import mhp.oee.po.gen.*;
import mhp.oee.vo.*;
import mhp.oee.web.angular.reasoncode.recognition.RecognitionVO;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.handle.ReasonCodeBOHandle;
import mhp.oee.common.handle.ReasonCodeRuleBOHandle;
import mhp.oee.common.handle.ReasonCodeRuleParamBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.ActivityAndReasonCodeRuleAndReasonCodeRuleParamPOMapper;
import mhp.oee.dao.extend.ActivityParamAndRuleParamPOMapper;
import mhp.oee.dao.extend.ReasonCodeAndReasonCodeGroupPOMapper;
import mhp.oee.dao.extend.ReasonCodePriorityAndReasonCodePOMapper;
import mhp.oee.dao.extend.ReasonCodeRuleAndReasonCodeRuleParamPOMapper;
import mhp.oee.dao.extend.ReasonCodeRuleParamAndResourceTypeAndActivityParamPOMapper;
import mhp.oee.utils.Utils;

@Component
public class ReasonCodeComponent {
    @InjectableLogger
    private static Logger logger;
    private static final String REASON_CODE = "REASON_CODE";
    private static final String REASON_CODE_RULE = "REASON_CODE_RULE";
    private static final String REASON_CODE_RULE_PARAM = "REASON_CODE_RULE_PARAM";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    private ReasonCodePOMapper reasonCodePOMapper;
    @Autowired
    private ReasonCodePriorityPOMapper reasonCodePriorityPOMapper;
    @Autowired
    private ReasonCodePriorityAndReasonCodePOMapper reasonCodePriorityAndReasonCodePOMapper;
    @Autowired
    private ReasonCodeRuleAndReasonCodeRuleParamPOMapper reasonCodeRuleAndReasonCodeRuleParamPOMapper;
    @Autowired
    private ActivityAndReasonCodeRuleAndReasonCodeRuleParamPOMapper activityAndReasonCodeRuleAndReasonCodeRuleParamPOMapper;
    @Autowired
    private ReasonCodeRulePOMapper reasonCodeRulePOMapper;
    @Autowired
    private ActivityPOMapper activityPOMapper;
    @Autowired
    private ActivityParamAndRuleParamPOMapper activityParamAndRuleParamPOMapper;
    @Autowired
    private ReasonCodeRuleParamAndResourceTypeAndActivityParamPOMapper reasonCodeRuleParamAndResourceTypeAndActivityParamPOMapper;
    @Autowired
    private ReasonCodeRuleParamPOMapper reasonCodeRuleParamPOMapper;
    @Autowired
    private ReasonCodeAndReasonCodeGroupPOMapper reasonCodeAndReasonCodeGroupPOMapper;
    @Autowired
    private ActivityParamPOMapper activityParamPOMapper;

    public ReasonCodePO getReasonCodeByHandle(String handle) {
        return reasonCodePOMapper.selectByPrimaryKey(handle);
    }

    public List<ReasonCodeAndReasonCodeGroupPO> readAllResonCodeAll(String site, String reasonCodeGroup, String enabled) {
        List<ReasonCodeAndReasonCodeGroupPO> vos = reasonCodeAndReasonCodeGroupPOMapper.selectReasonCodeAndReasonCodeGroupAll(site, reasonCodeGroup, enabled);
        return vos;
    }

    public List<ReasonCodeAndReasonCodeGroupPO> readAllResonCode(String site, String reasonCodeGroup, String enabled, int limit, int offset) {
        List<ReasonCodeAndReasonCodeGroupPO> vos = reasonCodeAndReasonCodeGroupPOMapper.selectReasonCodeAndReasonCodeGroup(site, reasonCodeGroup, enabled, limit, offset);
        return vos;
    }

    public int readAllResonCodeCount(String site, String reasonCodeGroup, String enabled) {
        int vos = reasonCodeAndReasonCodeGroupPOMapper.selectReasonCodeAndReasonCodeGroupCount(site, reasonCodeGroup, enabled);
        return vos;
    }

    public ReasonCodePO existResonCode(String site, String resonCode){
        String handle = new ReasonCodeBOHandle(site, resonCode).getValue();
        ReasonCodePO po = reasonCodePOMapper.selectByPrimaryKey(handle);
        return po;
    }

    public String createReasonCode(ReasonCodeVO reasonCodeVO) throws ExistingRecordException {
        String handle = new ReasonCodeBOHandle(reasonCodeVO.getSite(), reasonCodeVO.getReasonCode()).getValue();
        if (existResonCode(reasonCodeVO.getSite(), reasonCodeVO.getReasonCode()) != null) {
            logger.error("ReasonCodeComponent.createReasonCode() : Existing REASON_CODE record");
            throw new ExistingRecordException(REASON_CODE, gson.toJson(reasonCodeVO));
        }
        ReasonCodePO po = Utils.copyObjectProperties(reasonCodeVO, ReasonCodePO.class);
        po.setHandle(handle);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = reasonCodePOMapper.insert(po);
        if (ret != 1) {
            logger.error("ReasonCodeComponent.createReasonCode() : Existing REASON_CODE record");
            throw new ExistingRecordException(REASON_CODE, gson.toJson(reasonCodeVO));
        }
        return handle;
    }

    public void updateReasonCode(ReasonCodeVO reasonCodeVO) throws UpdateErrorException {
        ReasonCodePO po = Utils.copyObjectProperties(reasonCodeVO, ReasonCodePO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        ReasonCodePOExample example = new ReasonCodePOExample();
        example.createCriteria().andHandleEqualTo(reasonCodeVO.getHandle()).andModifiedDateTimeEqualTo(reasonCodeVO.getModifiedDateTime());
        int ret = reasonCodePOMapper.updateByExample(po, example);
        if (ret != 1) {
            logger.error("ReasonCodeComponent.updateReasonCode() : updateReasonCode");
            throw new UpdateErrorException(REASON_CODE, gson.toJson(po));
        }
    }

    public List<ReasonCodeGroupAndReasonCodePO> readReasonCodesLike(String site, String reasonCode) {
        return reasonCodeAndReasonCodeGroupPOMapper.selectReasonCodeLike(site, "%" + reasonCode + "%");
    }


    public List<RecognitionVO.HistoryLogVO> getHistoryChanges(String site, String reasonCode, String priority, String dateTime) throws ParseException {
        String[] priorities = null;
        if (StringUtils.isNotBlank(priority)) {
            priorities = priority.split("\\,");
            if (priorities.length == 0) priorities = null;
        }
        String[] reasonCodes = null;
        if (StringUtils.isNotBlank(reasonCode)) {
            reasonCodes = reasonCode.split("\\,");
            if (reasonCodes.length == 0) reasonCodes = null;
        }
        List<ReasonCodePriorityAndReasonCodePO> reasonCodePOList = reasonCodePriorityAndReasonCodePOMapper.selectHistoryChanges(site, reasonCodes, priorities, Utils.strToDate(dateTime));
        return buildHistoryLogVOList(reasonCodePOList);
    }

    public List<RecognitionVO.HistoryLogVO> getHistoryStatus(String site, String reasonCode, String priority, String dateTime) throws ParseException {
        String[] priorities = null;
        if (StringUtils.isNotBlank(priority)) {
            priorities = priority.split("\\,");
            if (priorities.length == 0) priorities = null;
        }
        String[] reasonCodes = null;
        if (StringUtils.isNotBlank(reasonCode)) {
            reasonCodes = reasonCode.split("\\,");
            if (reasonCodes.length == 0) reasonCodes = null;
        }
        List<ReasonCodePriorityAndReasonCodePO> reasonCodePOList = reasonCodePriorityAndReasonCodePOMapper.selectHistoryStatus(site, reasonCodes, priorities, Utils.strToDate(dateTime));
        return buildHistoryLogVOList(reasonCodePOList);
    }

    private List<RecognitionVO.HistoryLogVO> buildHistoryLogVOList(List<ReasonCodePriorityAndReasonCodePO> reasonCodePOList) {
        List<RecognitionVO.HistoryLogVO> historyChangeLogVOList = new ArrayList<RecognitionVO.HistoryLogVO>(reasonCodePOList.size());
        for (ReasonCodePriorityAndReasonCodePO reasonCodePO : reasonCodePOList) {
            reasonCodePO.setEffDateTimeString(Utils.datetimeToStr(reasonCodePO.getEffDateTime()));
            reasonCodePO.setEffEndDateTimeString(Utils.datetimeToStr(reasonCodePO.getEffEndDateTime()));
            RecognitionVO.HistoryLogVO historyChangeLogVO = new RecognitionVO.HistoryLogVO();
            historyChangeLogVO.setReasonCodePO(reasonCodePO);
            ActivityRPO activityRPO = reasonCodePriorityAndReasonCodePOMapper.selectActivityR(reasonCodePO.getReasonCodePriorityBo());
            if (null != activityRPO) {
                historyChangeLogVO.setActivityRPO(activityRPO);
            }
            ActivityFPO activityFPO = reasonCodePriorityAndReasonCodePOMapper.selectActivityF(reasonCodePO.getReasonCodePriorityBo());
            if (null != activityFPO) {
                historyChangeLogVO.setActivityFPO(activityFPO);
            }
            ModifiedPO modifiedPO = reasonCodePriorityAndReasonCodePOMapper.selectModified(reasonCodePO.getReasonCodePriorityBo());
            modifiedPO.setModifiedDateTimeString(Utils.datetimeToStr(modifiedPO.getModifiedDateTime()));
            if (null != modifiedPO) {
                historyChangeLogVO.setModifiedPO(modifiedPO);
            }
            historyChangeLogVOList.add(historyChangeLogVO);
        }
        return historyChangeLogVOList;
    }

    public ReasonCodePriorityVO getReasonCodePriorityByPrimaryKey(String handle) {
        ReasonCodePriorityPO po=reasonCodePriorityPOMapper.selectByPrimaryKey(handle);
        ReasonCodePriorityVO vo=Utils.copyObjectProperties(po, ReasonCodePriorityVO.class);
        return vo;
    }

    public ReasonCodePriorityPO getReasonCodePriorityByHandle(String reasonCodePriorityBo) {
        return reasonCodePriorityPOMapper.selectByPrimaryKey(reasonCodePriorityBo);
    }

    public void checkReasonCodePriorityPO(ReasonCodePriorityPO po) throws ExistingException, UsedException {
        if (po == null) {
            throw new ExistingException();
        }
        String used = po.getUsed();
        if ("true".equals(used)) {
            throw new UsedException();
        }
    }

    private String getRuleFullName(String rule) throws UnknownRuleException {
        if ("R".equals(rule)) {
            return "RECOGNITION";
        } else if ("F".equals(rule)) {
            return "FILTER";
        } else {
            throw new UnknownRuleException(rule);
        }
    }

    public ReasonCodeRulePO getReasonCodeRuleByCondition(String reasonCodePriorityBo, String rule) throws UnknownRuleException {
        ReasonCodeRulePOExample reasonCodeRulePOExample = new ReasonCodeRulePOExample();
        reasonCodeRulePOExample.createCriteria()
                .andReasonCodePriorityBoEqualTo(reasonCodePriorityBo)
                .andRuleEqualTo(getRuleFullName(rule));
        List<ReasonCodeRulePO> reasonCodeRulePOs = reasonCodeRulePOMapper.selectByExample(reasonCodeRulePOExample);
        if (reasonCodeRulePOs.size() == 0) {
            return null;
        } else {
            return reasonCodeRulePOs.get(0);
        }
    }

    public void updateReasonCodeRule(ReasonCodeRulePO reasonCodeRule, String activity) throws UpdateErrorException {
        if (reasonCodeRule != null) {
            reasonCodeRule.setActivity(activity);
            int ret = reasonCodeRulePOMapper.updateByPrimaryKey(reasonCodeRule);
            if (ret != 1) {
                throw new UpdateErrorException(REASON_CODE_RULE, reasonCodeRule.getHandle());
            }
        }
    }

    public void updateReasonCodeRuleParamOnChangeActivity(String reasonCodeRuleHandle, List<RuleParamVo> ruleParam, String activity) throws ExistingRecordException {
        // delete param
        ReasonCodeRuleParamPOExample example = new ReasonCodeRuleParamPOExample();
        example.createCriteria().andReasonCodeRuleBoEqualTo(reasonCodeRuleHandle);
        reasonCodeRuleParamPOMapper.deleteByExample(example);
//        // insert new param
//        for (ReasonCodeRuleParamPO each : ruleParam) {
//            String paramValue = each.getParamValue();
//            if (StringUtils.isNotBlank(paramValue)) {
//                checkActivityParam(activity, each.getParamId());
//                reasonCodeRuleParamPOMapper.insert(each);
//            }
//        }
    }

    private void checkActivityParam(String activity, String paramId) throws ExistingException {
        List<ReasonCodeRuleParamPO> reasonCodeRuleParamPOList = reasonCodeRuleAndReasonCodeRuleParamPOMapper.select(activity, paramId);
        if (reasonCodeRuleParamPOList.size() == 0) {
            Map<String, Object> errorJson = new HashMap<String, Object>();
            errorJson.put("activity", activity);
            errorJson.put("paramId", paramId);
            throw new ExistingException();
        }
    }

    public void updateReasonCodeRuleParamOnChangeParameter(String site, String reasonCodeRuleBo, List<RuleParamVo> ruleParam, String activity) throws ExistingException, UpdateErrorException, ExistingRecordException {
        // update
        for (RuleParamVo each2 : ruleParam) {
            // 数据转换
            ReasonCodeRuleParamPO each = new ReasonCodeRuleParamPO();
            each.setHandle(each2.getReasonCodeRuleParamBo());
            each.setParamId(each2.getParamId());
            each.setParamValue(each2.getParamValue());
            each.setResourceType(each2.getResourceType());
            each.setSite(site);
            each.setReasonCodeRuleBo(reasonCodeRuleBo);
            each.setModifiedDateTime(each2.getModifiedDateTime());
            each.setModifiedUser(ContextHelper.getContext().getUser());

            int ret;
            String paramValue = each.getParamValue();

            // 只能是*或null
            if (each.getResourceType() != null && !"*".equals(each.getResourceType())) {
                continue;
            }

            if (StringUtils.isNotBlank(each.getHandle()) && StringUtils.isNotBlank(paramValue)) { // update
                // delete
                ReasonCodeRuleParamPOExample example = new ReasonCodeRuleParamPOExample();
                example.createCriteria()
                        .andReasonCodeRuleBoEqualTo(reasonCodeRuleBo)
                        .andParamIdEqualTo(each.getParamId());
                reasonCodeRuleParamPOMapper.deleteByExample(example);
                // insert
                reasonCodeRuleParamPOMapper.insert(each);
//                // update
//                checkActivityParam(activity, each.getParamId());
//                ReasonCodeRuleParamPOExample example = new ReasonCodeRuleParamPOExample();
//                example.createCriteria()
//                        .andHandleEqualTo(each.getHandle())
//                        .andModifiedDateTimeEqualTo(each.getModifiedDateTime());
//                ret = reasonCodeRuleParamPOMapper.updateByExampleSelective(each, example);
//                if (ret != 1) {
//                    throw new UpdateErrorException(REASON_CODE_RULE_PARAM, each.getHandle());
//                }
            } else if (StringUtils.isNotBlank(each.getHandle()) && StringUtils.isBlank(paramValue)) { // delete
                ReasonCodeRuleParamPOExample example = new ReasonCodeRuleParamPOExample();
                example.createCriteria()
                        .andReasonCodeRuleBoEqualTo(reasonCodeRuleBo)
                        .andParamIdEqualTo(each.getParamId());
                reasonCodeRuleParamPOMapper.deleteByExample(example);
            } else if (StringUtils.isBlank(each.getHandle()) && StringUtils.isNotBlank(paramValue)) { // insert
                    if (each.getResourceType() == null) {
                        each.setResourceType("*");
                    }
                    deleteReasonCodeRuleParam(each.getParamId(), reasonCodeRuleBo);
                    String handle = new ReasonCodeRuleParamBOHandle(site, reasonCodeRuleBo, each.getResourceType(), each.getParamId()).getValue();
                    each.setHandle(handle);
                    each.setModifiedUser(ContextHelper.getContext().getUser());
                    try {
                        reasonCodeRuleParamPOMapper.insert(each);
                    } catch (RuntimeException e) {
                        throw new ExistingRecordException(REASON_CODE_RULE_PARAM, handle);
                    }
            }
        }
    }

    public List<ReasonCodeRuleParamPO> getReasonCodeRuleParamByActivityBO(String activityBo) {
        return activityAndReasonCodeRuleAndReasonCodeRuleParamPOMapper.select(activityBo);
    }

    private ActivityPO getActivityPOByActivity(String activity) {
        ActivityPOExample example = new ActivityPOExample();
        example.createCriteria().andActivityEqualTo(activity);
        List<ActivityPO> activityPOs = activityPOMapper.selectByExample(example);
        if (0 != activityPOs.size()) {
            return activityPOs.get(0);
        } else {
            return null;
        }
    }

    public FilterParamVO getFilterParamButton(String site, String reasonCodeRuleBo, String paramId) throws NoSuchException {
        FilterParamVO vo = new FilterParamVO();
        // 默认参数
        ActivityParamPOExample example = new ActivityParamPOExample();
        example.createCriteria().andParamIdEqualTo(paramId);
        List<ActivityParamPO> activityParamList = activityParamPOMapper.selectByExample(example);
        ActivityParamPO activityParam;
        if (activityParamList.size() == 0) {
            throw new NoSuchException("paramId("+paramId+")");
        }
        activityParam = activityParamList.get(0);
        vo.setActivityFParamId(activityParam.getParamId());
        vo.setActivityFParamDesc(activityParam.getParamDescription());
        // 参数列表
        List<FilterParamButtonPO> params = reasonCodeRuleParamAndResourceTypeAndActivityParamPOMapper.select(site, reasonCodeRuleBo, paramId);
        vo.setParams(params);
        return vo;
    }

    public RecognitionVO.ReasonCodeDetailVO selectReasonCodeDetail(String reasonCodePriorityBo) throws ExistingException {
        // 识别优先级
        ReasonCodePriorityPO reasonCodePriorityPO = reasonCodePriorityPOMapper.selectByPrimaryKey(reasonCodePriorityBo);
        if (reasonCodePriorityBo == null) throw new ExistingException();
        // 原因码
        ReasonCodePO reasonCodePO = reasonCodePOMapper.selectByPrimaryKey(reasonCodePriorityPO.getReasonCodeBo());
        if (reasonCodePO == null) throw new ExistingException();
        // 识别方式
        ActivityPO activityR = null;
        ReasonCodeRulePO reasonCodeRuleRPO = null;
        ReasonCodeRulePOExample exampleR = new ReasonCodeRulePOExample();
        exampleR.createCriteria()
                .andReasonCodePriorityBoEqualTo(reasonCodePriorityBo)
                .andRuleEqualTo("RECOGNITION");
        List<ReasonCodeRulePO> reasonCodeRuleR = reasonCodeRulePOMapper.selectByExample(exampleR);
        if (0 != reasonCodeRuleR.size()) {
            reasonCodeRuleRPO = reasonCodeRuleR.get(0);
            activityR = getActivityPOByActivity(reasonCodeRuleRPO.getActivity());
        }
        // 防呆验证
        ActivityPO activityF = null;
        ReasonCodeRulePO reasonCodeRuleFPO = null;
        ReasonCodeRulePOExample exampleF = new ReasonCodeRulePOExample();
        exampleF.createCriteria()
                .andReasonCodePriorityBoEqualTo(reasonCodePriorityBo)
                .andRuleEqualTo("FILTER");
        List<ReasonCodeRulePO> reasonCodeRuleF = reasonCodeRulePOMapper.selectByExample(exampleF);
        if (0 != reasonCodeRuleF.size()) {
            reasonCodeRuleFPO = reasonCodeRuleF.get(0);
            activityF = getActivityPOByActivity(reasonCodeRuleFPO.getActivity());
        }

        RecognitionVO.ReasonCodeDetailVO reasonCodeDetailOut = new RecognitionVO.ReasonCodeDetailVO();
        reasonCodeDetailOut.setReasonCodePriorityBo(reasonCodePriorityBo);
        reasonCodeDetailOut.setReasonCode(reasonCodePO.getReasonCode());
        reasonCodeDetailOut.setReasonCodeDesc(reasonCodePO.getDescription());
        reasonCodeDetailOut.setPriority(reasonCodePriorityPO.getPriority());
        reasonCodeDetailOut.setSubPriority(reasonCodePriorityPO.getSubPriority());
        reasonCodeDetailOut.setEffDateTime(reasonCodePriorityPO.getStartDateTime());
        reasonCodeDetailOut.setUsed(reasonCodePriorityPO.getUsed());
        reasonCodeDetailOut.setModifiedDateTime(reasonCodePriorityPO.getModifiedDateTime());
        reasonCodeDetailOut.setModifiedUser(reasonCodePriorityPO.getModifiedUser());
        reasonCodeDetailOut.setEffEndDateTime(reasonCodePriorityPO.getEndDateTime());
        reasonCodeDetailOut.setEffEndDateTimeString(Utils.datetimeToStr(reasonCodePriorityPO.getEndDateTime()));
        if (null != reasonCodeRuleRPO) {
            reasonCodeDetailOut.setReasonCodeRuleBoR(reasonCodeRuleRPO.getHandle());
            reasonCodeDetailOut.setActivityR(reasonCodeRuleRPO.getActivity());
            if (null != activityR) {
                reasonCodeDetailOut.setActivityRDesc(activityR.getDescription());
                reasonCodeDetailOut.setActivityBoR(activityR.getHandle());
            }
            reasonCodeDetailOut.setModifiedDateTimeR(reasonCodeRuleRPO.getModifiedDateTime());
            reasonCodeDetailOut.setModifiedDateTimeStringR(Utils.datetimeToStr(reasonCodeRuleRPO.getModifiedDateTime()));
            reasonCodeDetailOut.setModifiedUserR(reasonCodeRuleRPO.getModifiedUser());
            // rule_param and activity_param
            List<ActivityParamAndRuleParamPO> activityParamR = activityParamAndRuleParamPOMapper.select(reasonCodeRuleRPO.getHandle(), reasonCodeRuleRPO.getActivity());
            reasonCodeDetailOut.setActivityRParam(activityParamR);
        }
        if (null != reasonCodeRuleFPO) {
            reasonCodeDetailOut.setReasonCodeRuleBoF(reasonCodeRuleFPO.getHandle());
            reasonCodeDetailOut.setActivityF(reasonCodeRuleFPO.getActivity());
            if (null != activityF) {
                reasonCodeDetailOut.setActivityFDesc(activityF.getDescription());
                reasonCodeDetailOut.setActivityBoF(activityF.getHandle());
            }
            reasonCodeDetailOut.setModifiedDateTimeF(reasonCodeRuleFPO.getModifiedDateTime());
            reasonCodeDetailOut.setModifiedDateTimeStringF(Utils.datetimeToStr(reasonCodeRuleFPO.getModifiedDateTime()));
            reasonCodeDetailOut.setModifiedUserF(reasonCodeRuleFPO.getModifiedUser());
            // rule_param and activity_param
            Map<String, ActivityParamAndRuleParamPO> activityParamFMap = new HashMap<String, ActivityParamAndRuleParamPO>();
            Map<String, String> paramIdAndModifiedUser = new HashMap<String, String>();
            Map<String, Date> paramIdAndModifiedDateTime = new HashMap<String, Date>();
            List<ActivityParamAndRuleParamPO> activityParamF = activityParamAndRuleParamPOMapper.select(reasonCodeRuleFPO.getHandle(), reasonCodeRuleFPO.getActivity());
            for (ActivityParamAndRuleParamPO each : activityParamF) {
                String paramId = each.getParamId();
                Date modifiedDateTime = each.getModifiedDateTime();
                String modifiedUser = each.getModifiedUser();
                boolean hasParamId = paramIdAndModifiedUser.containsKey(paramId);
                if (hasParamId) { // 已经有这个记录了
                    Date modifiedDateTimeLast = paramIdAndModifiedDateTime.get(paramId);
                    if (modifiedDateTime.after(modifiedDateTimeLast)) { // 这个时间更新的更晚
                        paramIdAndModifiedUser.put(paramId, modifiedUser);
                        paramIdAndModifiedDateTime.put(paramId, modifiedDateTime);
                        activityParamFMap.put(paramId, each);
                    }
                } else { // 没有这个记录
                    paramIdAndModifiedUser.put(paramId, modifiedUser);
                    paramIdAndModifiedDateTime.put(paramId, modifiedDateTime);
                    activityParamFMap.put(paramId, each);
                }
                ActivityParamAndRuleParamPO activityParam = activityParamFMap.get(paramId);
                if (activityParam.getResourceType() != null && !"*".equals(activityParam.getResourceType())) {
                    activityParam.setReasonCodeRuleParamBo(activityParam.getReasonCodeRuleParamBo()); // fix: 
                    activityParam.setResourceType("by设备类型");
                }
            }
            List<ActivityParamAndRuleParamPO> listValue = new LinkedList<ActivityParamAndRuleParamPO>();
            Iterator it = activityParamFMap.keySet().iterator();
            while (it.hasNext()) {
                String key = it.next().toString();
                listValue.add(activityParamFMap.get(key));
            }
            reasonCodeDetailOut.setActivityFParam(listValue);
        }
        return reasonCodeDetailOut;
    }

//    public void insertFilterByResourceTypeParam(String site, RecognitionVO.ResourceTypeParamVO resourceTypeParamVO) throws BusinessException {
//        List<ResourceTypeAndActivityParamValueVO> resourceTypeAndActivityParamValueVOList = resourceTypeParamVO.getResourceTypeAndActivityParamValue();
//        for (ResourceTypeAndActivityParamValueVO each : resourceTypeAndActivityParamValueVOList) {
//            if (each.getActivityFParamValue() != null) {
//
//            }
//        }
//    }

    private void updateFilterByResourceTypeParam(ResourceTypeAndActivityParamValueVO each) throws UpdateErrorException {
        ReasonCodeRuleParamPO reasonCodeRuleParamPO = new ReasonCodeRuleParamPO();
        reasonCodeRuleParamPO.setParamValue(each.getActivityFParamValue());

        ReasonCodeRuleParamPOExample example = new ReasonCodeRuleParamPOExample();
        example.createCriteria()
                .andHandleEqualTo(each.getReasonCodeRuleParamBo())
                .andModifiedDateTimeEqualTo(each.getReasonCodeRuleParamModifiedDateTime());

        int ret = reasonCodeRuleParamPOMapper.updateByExampleSelective(reasonCodeRuleParamPO, example);
        if (ret != 1) {
            throw new UpdateErrorException(REASON_CODE_RULE_PARAM, gson.toJson(reasonCodeRuleParamPO));
        }
    }

    private void deleteReasonCodeRuleParam(String paramId, String reasonCodeRuleBo) {
        ReasonCodeRuleParamPOExample ruleParamExample = new ReasonCodeRuleParamPOExample();
        ruleParamExample.createCriteria().andReasonCodeRuleBoEqualTo(reasonCodeRuleBo).andParamIdEqualTo(paramId).andResourceTypeEqualTo("*");
        List<ReasonCodeRuleParamPO> ruleParamList = reasonCodeRuleParamPOMapper.selectByExample(ruleParamExample);
        if (ruleParamList.size() > 0) { // delete first
            reasonCodeRuleParamPOMapper.deleteByExample(ruleParamExample);
        }
    }

    private void insertFilterByResourceTypeParam(String site, String paramId, String reasonCodeRuleBo, ResourceTypeAndActivityParamValueVO each) throws ExistingRecordException {
        ReasonCodeRuleParamPOExample ruleParamExample = new ReasonCodeRuleParamPOExample();
        ruleParamExample.createCriteria().andReasonCodeRuleBoEqualTo(reasonCodeRuleBo).andParamIdEqualTo(paramId).andResourceTypeEqualTo("*");
        List<ReasonCodeRuleParamPO> ruleParamList = reasonCodeRuleParamPOMapper.selectByExample(ruleParamExample);
        if (ruleParamList.size() > 0) { // delete first
            reasonCodeRuleParamPOMapper.deleteByExample(ruleParamExample);
        }
        deleteReasonCodeRuleParam(paramId, reasonCodeRuleBo);
        String handle = new ReasonCodeRuleParamBOHandle(
                site, reasonCodeRuleBo, each.getResourceType(), paramId).getValue();
        ReasonCodeRuleParamPO reasonCodeRuleParamPO = new ReasonCodeRuleParamPO();
        reasonCodeRuleParamPO.setHandle(handle);
        reasonCodeRuleParamPO.setSite(site);
        reasonCodeRuleParamPO.setReasonCodeRuleBo(reasonCodeRuleBo);
        reasonCodeRuleParamPO.setResourceType(each.getResourceType());
        reasonCodeRuleParamPO.setParamId(paramId);
        reasonCodeRuleParamPO.setParamValue(each.getActivityFParamValue());
        reasonCodeRuleParamPO.setModifiedUser(ContextHelper.getContext().getUser());
        try {
            reasonCodeRuleParamPOMapper.insert(reasonCodeRuleParamPO);
        } catch (RuntimeException e) {
            throw new ExistingRecordException(REASON_CODE_RULE_PARAM, reasonCodeRuleParamPO.getHandle());
        }
    }


    public void changeFilterByResourceTypeParam(String site, RecognitionVO.ResourceTypeParamVO resourceTypeParamVO) throws InvalidCycleTimeException, ExistingRecordException, UpdateErrorException {
        List<ResourceTypeAndActivityParamValueVO> resourceTypeAndActivityParamValueVOList = resourceTypeParamVO.getResourceTypeAndActivityParamValue();
        for (ResourceTypeAndActivityParamValueVO each : resourceTypeAndActivityParamValueVOList) {
            if (StringUtils.isNotBlank(each.getReasonCodeRuleParamBo())) {
                if (StringUtils.isNotBlank(each.getActivityFParamValue())) { // update
                    updateFilterByResourceTypeParam(each);
                } else { // delete
                    deleteFilterByResourceTypeParam(each);
                }
            } else {
                if (each.getActivityFParamValue() != null) { // insert
                    insertFilterByResourceTypeParam(site, resourceTypeParamVO.getParamId(), resourceTypeParamVO.getReasonCodeRuleBo(), each);
                }
            }
        }

    }

    private void deleteFilterByResourceTypeParam(ResourceTypeAndActivityParamValueVO each) throws InvalidCycleTimeException {
        int ret = reasonCodeRuleParamPOMapper.deleteByPrimaryKey(each.getReasonCodeRuleParamBo());
        if (ret != 1) {
            throw new InvalidCycleTimeException();
        }
    }

    public ReasonCodeRulePO saveFilterActivity(String site, RecognitionVO.FilterActivityVO filterActivityVO) throws NoSuchException, ItemDisableException, DataDescriptionException, InvalidCycleTimeException {
        Date now = new Date();
        String rule = "FILTER";
        ActivityPOExample example = new ActivityPOExample();
        example.createCriteria().andActivityEqualTo(filterActivityVO.getActivityF());
        List<ActivityPO> activityPOs = activityPOMapper.selectByExample(example);
        if (activityPOs.size() == 0) {
            throw new NoSuchException("activity");
        }
        ActivityPO activity = activityPOs.get(0);
        if ("false".equals(activity.getEnabled())) {
            throw new ItemDisableException("Activity");
        }
        if (!"F".equals(activity.getExecutionType())) {
            throw new DataDescriptionException();
        }
        String reasonCodeRuleBOHandle = new ReasonCodeRuleBOHandle(filterActivityVO.getReasonCodePriorityBo(), rule).getValue();
        ReasonCodeRulePO reasonCodeRulePO = new ReasonCodeRulePO();
        reasonCodeRulePO.setHandle(reasonCodeRuleBOHandle);
        reasonCodeRulePO.setReasonCodePriorityBo(filterActivityVO.getReasonCodePriorityBo());
        reasonCodeRulePO.setRule(rule);
        reasonCodeRulePO.setActivity(filterActivityVO.getActivityF());
        reasonCodeRulePO.setModifiedDateTime(now);
        reasonCodeRulePO.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = reasonCodeRulePOMapper.insert(reasonCodeRulePO);
        if (ret != 1) {
            throw new InvalidCycleTimeException();
        }
        // 保存param
        List<ActivityParamIdAndValueVO> activityParam = filterActivityVO.getActivityParam();
        for (ActivityParamIdAndValueVO each : activityParam) {
            if (null != each.getActivityFParamValue()) {
                String reasonCodeRuleParamHandle = new ReasonCodeRuleParamBOHandle(site, reasonCodeRuleBOHandle, "resourceTypeHandle", each.getActivityFParamId()).getValue();
                ReasonCodeRuleParamPO reasonCodeRuleParamPO = new ReasonCodeRuleParamPO();
                reasonCodeRuleParamPO.setHandle(reasonCodeRuleParamHandle);
                reasonCodeRuleParamPO.setReasonCodeRuleBo(reasonCodeRuleBOHandle);
                reasonCodeRuleParamPO.setParamId(each.getActivityFParamId());
                reasonCodeRuleParamPO.setParamValue(each.getActivityFParamValue());
                reasonCodeRuleParamPO.setModifiedUser(ContextHelper.getContext().getUser());
                ret = reasonCodeRuleParamPOMapper.insert(reasonCodeRuleParamPO);
                if (ret != 1) {
                    throw new InvalidCycleTimeException();
                }
            }
        }
        return reasonCodeRulePO;
    }

    public void insertReasonCodeRule(RecognitionVO.ReasonCodeRuleParamVO activityParamIn) throws CreateErrorException {
        String reasonCodeRuleBo = new ReasonCodeRuleBOHandle(activityParamIn.getReasonCodePriorityBo(), "FILTER").getValue();
        ReasonCodeRulePO reasonCodeRulePO = new ReasonCodeRulePO();
        reasonCodeRulePO.setHandle(reasonCodeRuleBo);
        reasonCodeRulePO.setReasonCodePriorityBo(activityParamIn.getReasonCodePriorityBo());
        reasonCodeRulePO.setRule("FILTER");
        reasonCodeRulePO.setActivity(activityParamIn.getActivity());
        reasonCodeRulePO.setModifiedUser(ContextHelper.getContext().getUser());
        try {
            reasonCodeRulePOMapper.insert(reasonCodeRulePO);
        } catch (RuntimeException e) {
            throw new CreateErrorException(REASON_CODE_RULE, gson.toJson(reasonCodeRulePO));
        }
    }
}
