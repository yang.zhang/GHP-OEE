package mhp.oee.component.plant;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.DataReferingException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.NotFoundExistingRecordException;
import mhp.oee.common.handle.PlcAccessTypeBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.PlcAccessTypePOMapper;
import mhp.oee.dao.gen.ResourcePlcInfoPOMapper;
import mhp.oee.po.gen.PlcAccessTypePO;
import mhp.oee.po.gen.PlcAccessTypePOExample;
import mhp.oee.po.gen.ResourcePlcInfoPO;
import mhp.oee.po.gen.ResourcePlcInfoPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.PlcAccessTypeVO;

@Component
public class PlcAccessTypeComponent {


    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    PlcAccessTypePOMapper plcAccessTypePOMapper;

    @Autowired
    private ResourcePlcInfoPOMapper resourcePlcInfoPOMapper;

    public List<PlcAccessTypeVO> readAllPlcAccessType(String site) {
        PlcAccessTypePOExample example = new PlcAccessTypePOExample();
        example.createCriteria().andSiteEqualTo(site);
        example.setOrderByClause("HANDLE");
        List<PlcAccessTypePO> pos = plcAccessTypePOMapper.selectByExample(example);
        List<PlcAccessTypeVO> vos = Utils.copyListProperties(pos, PlcAccessTypeVO.class);
        return vos;
    }

    public PlcAccessTypePO existPlcObject(String site, String plcAccessType) {
        String handle = new PlcAccessTypeBOHandle(site, plcAccessType).getValue();
        PlcAccessTypePO po = plcAccessTypePOMapper.selectByPrimaryKey(handle);
        return po;
    }

    public PlcAccessTypeVO getAccessTypeByCode(PlcAccessTypeVO vo) {
        PlcAccessTypePO po = plcAccessTypePOMapper.selectByPrimaryKey(vo.getHandle());
        return Utils.copyObjectProperties(po, PlcAccessTypeVO.class);
    }


    public void createAccessType(PlcAccessTypeVO vo) throws ExistingRecordException {
        String handle = new PlcAccessTypeBOHandle(vo.getSite(), vo.getPlcAccessType()).getValue();
        PlcAccessTypePO po = Utils.copyObjectProperties(vo, PlcAccessTypePO.class);
        po.setHandle(handle);   // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());

        int ret = plcAccessTypePOMapper.insert(po);
        if (ret != 1) {
            PlcAccessTypePO existingPO = plcAccessTypePOMapper.selectByPrimaryKey(handle);
            throw new ExistingRecordException("AccessType", gson.toJson(existingPO));
        }
    }

    public void updateAccessType(PlcAccessTypeVO vo) throws NotFoundExistingRecordException {
        PlcAccessTypePO po = Utils.copyObjectProperties(vo, PlcAccessTypePO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        PlcAccessTypePOExample example = new PlcAccessTypePOExample();
        example.createCriteria().andHandleEqualTo(vo.getHandle()).andModifiedDateTimeEqualTo(vo.getModifiedDateTime());
        int ret = plcAccessTypePOMapper.updateByExample(po, example );
        if (ret != 1) {
            throw new NotFoundExistingRecordException();
        }
    }


    public List<ResourcePlcInfoPO> getResourcePlcInfoListByAccessType(String site, String type) {
        // cross-check RESOURCE_PLC_info
        ResourcePlcInfoPOExample rpExample = new ResourcePlcInfoPOExample();
        rpExample.createCriteria().andSiteEqualTo(site).andPlcAccessTypeEqualTo(type);

        return this.resourcePlcInfoPOMapper.selectByExample(rpExample);
    }

    public void deleteAccessType(String site, PlcAccessTypeVO vo) throws NotFoundExistingRecordException, DataReferingException  {

        List<ResourcePlcInfoPO> resourcePlcInfoList = this.getResourcePlcInfoListByAccessType(site, vo.getPlcAccessType());
        if(resourcePlcInfoList == null || resourcePlcInfoList.size() ==0 ){
            //delete Access Type
            PlcAccessTypePOExample example = new PlcAccessTypePOExample();
            example.createCriteria().andHandleEqualTo(vo.getHandle()).andModifiedDateTimeEqualTo(vo.getModifiedDateTime());
            int ret = plcAccessTypePOMapper.deleteByExample(example);
            if (ret != 1) {
                throw new NotFoundExistingRecordException();
            }
        } else {
           throw new DataReferingException();
        }

    }

	public List<ResourcePlcInfoPO> getResourcePlcInfoByPcIPAndPlcAccessTypeNull(String pcIp) {
		ResourcePlcInfoPOExample rpExample = new ResourcePlcInfoPOExample();
        rpExample.createCriteria().andPcIpEqualTo(pcIp).andPlcAccessTypeEqualTo("");
        rpExample.or(rpExample.createCriteria().andPcIpEqualTo(pcIp).andPlcAccessTypeIsNull());
        return this.resourcePlcInfoPOMapper.selectByExample(rpExample);
	}
}
