package mhp.oee.component.plant;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.EffectiveDateException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.InvalidEffectiveDateException;

import mhp.oee.dao.extend.ReasonCodePriorityAndReasonCodePOMapper;
import mhp.oee.dao.extend.ReasonCodePriorityRulePOMapper;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.NotFoundExistingRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ReasonCodePriorityBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.ReasonCodePOMapper;
import mhp.oee.dao.gen.ReasonCodePriorityPOMapper;
import mhp.oee.po.extend.ReasonCodePriorityRulePO;
import mhp.oee.po.gen.ReasonCodePO;
import mhp.oee.po.gen.ReasonCodePriorityPO;
import mhp.oee.po.gen.ReasonCodePriorityPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ReasonCodePriorityVO;

@Component
public class ReasonCodePriorityComponent {
	private static String REASON_CODE_PRIORITY = "REASON_CODE_PRIORITY";
	private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

	@Autowired
	private ReasonCodePOMapper reasonCodePOMapper;
	@Autowired
	private ReasonCodePriorityPOMapper reasonCodePriorityPOMapper;
	@Autowired
	private ReasonCodePriorityAndReasonCodePOMapper reasonCodePriorityAndReasonCodePOMapper;
	@Autowired
	private ReasonCodePriorityRulePOMapper reasonCodePriorityRulePOMapper;
	
	
	public void updateReasonCodePriority(ReasonCodePriorityPO record, boolean selective) {
		if (selective) {
			reasonCodePriorityPOMapper.updateByPrimaryKeySelective(record);
		} else {
			reasonCodePriorityPOMapper.updateByPrimaryKey(record);
		}
	}

	public String changeReasonCodePriority(ReasonCodePriorityVO vo) throws ParseException,
			NotFoundExistingRecordException, UpdateErrorException, InvalidEffectiveDateException, EffectiveDateException {
		String ret = "";
		if (Utils.strToDatetime(vo.getInStartDateTime()).before(new Date())) {
            throw new EffectiveDateException();
        }
		ReasonCodePO reasonCodePO = reasonCodePOMapper.selectByPrimaryKey(vo.getReasonCodeBo());
		if (reasonCodePO == null) {
			throw new NotFoundExistingRecordException();
		}
		ReasonCodePriorityPOExample example = new ReasonCodePriorityPOExample();
		example.createCriteria().andReasonCodeBoEqualTo(vo.getReasonCodeBo()).andPriorityEqualTo(vo.getPriority())
				.andSubPriorityEqualTo(vo.getSubPriority());
		example.setOrderByClause("START_DATE_TIME");
		vo.setStartDateTime(Utils.strToDatetime(vo.getInStartDateTime()));
		String strt = DateFormatUtils.format(vo.getStartDateTime(), "yyyyMMdd.HHmmss");
		vo.setStrt(strt);
		List<ReasonCodePriorityPO> list = reasonCodePriorityPOMapper.selectByExample(example);
		if (list.size() == 0) {
			String handle = new ReasonCodePriorityBOHandle(vo.getReasonCodeBo(), vo.getPriority(), vo.getSubPriority(),
					strt).getValue();
			ReasonCodePriorityPO po = Utils.copyObjectProperties(vo, ReasonCodePriorityPO.class);
			po.setHandle(handle);
			po.setUsed("false");
			try {
				po.setEndDateTime(DateUtils.parseDate("9999-12-31 23:59:59", "yyyy-MM-dd HH:mm:ss"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			po.setModifiedUser(ContextHelper.getContext().getUser());
			int result = reasonCodePriorityPOMapper.insert(po);
			if (result == 1) {
				ret = po.getHandle();
			}
		} else if (list.size() > 0) {
			ReasonCodePriorityPO maxStartDatePo = list.get(list.size() - 1);
			Date effectiveDate = vo.getStartDateTime();
			Date startDate = maxStartDatePo.getStartDateTime();
			Date endDate = maxStartDatePo.getEndDateTime();
			if (effectiveDate.before(startDate)) {
				throw new InvalidEffectiveDateException();
			} else if (effectiveDate.after(startDate) && effectiveDate.before(endDate)
					&& !endDate.equals(DateUtils.parseDate("9999-12-31 23:59:59", "yyyy-MM-dd HH:mm:ss"))) {
				throw new InvalidEffectiveDateException();
			} else if (effectiveDate.after(startDate) && effectiveDate.before(endDate)
					&& endDate.equals(DateUtils.parseDate("9999-12-31 23:59:59", "yyyy-MM-dd HH:mm:ss"))) {
				maxStartDatePo.setEndDateTime(vo.getStartDateTime());
				maxStartDatePo.setModifiedUser(ContextHelper.getContext().getUser());
				ReasonCodePriorityPOExample reasonCodePriorityPOExample = new ReasonCodePriorityPOExample();
				reasonCodePriorityPOExample.createCriteria().andHandleEqualTo(maxStartDatePo.getHandle());
				int updateRet = reasonCodePriorityPOMapper.updateByExample(maxStartDatePo, reasonCodePriorityPOExample);
				if (updateRet != 1) {
					throw new UpdateErrorException(REASON_CODE_PRIORITY, gson.toJson(maxStartDatePo));
				}
				ReasonCodePriorityPO record = Utils.copyObjectProperties(vo, ReasonCodePriorityPO.class);
				String recordHandle = new ReasonCodePriorityBOHandle(vo.getReasonCodeBo(), vo.getPriority(),
						vo.getSubPriority(), strt).getValue();
				record.setHandle(recordHandle);
				record.setUsed("false");
				record.setEndDateTime(DateUtils.parseDate("9999-12-31 23:59:59", "yyyy-MM-dd HH:mm:ss"));
				int result = reasonCodePriorityPOMapper.insert(record);
				if (result == 1) {
					ret = record.getHandle();
				}
			} else if (effectiveDate.after(endDate)
					&& !endDate.equals(DateUtils.parseDate("9999-12-31 23:59:59", "yyyy-MM-dd HH:mm:ss"))) {
				ReasonCodePriorityPO record = Utils.copyObjectProperties(vo, ReasonCodePriorityPO.class);
				String recordHandle = new ReasonCodePriorityBOHandle(vo.getReasonCodeBo(), vo.getPriority(),
						vo.getSubPriority(), strt).getValue();
				record.setHandle(recordHandle);
				record.setUsed("false");
				record.setEndDateTime(DateUtils.parseDate("9999-12-31 23:59:59", "yyyy-MM-dd HH:mm:ss"));
				int result = reasonCodePriorityPOMapper.insert(record);
				if (result == 1) {
					ret = record.getHandle();
				}
			}
		}
		return ret;
	}

	public List<ReasonCodePriorityPO> readRecordsByPriority(String priority, String sub_priority) {
		ReasonCodePriorityPOExample example = new ReasonCodePriorityPOExample();
		example.createCriteria().andPriorityEqualTo(priority).andSubPriorityEqualTo(sub_priority);
		List<ReasonCodePriorityPO> ret = reasonCodePriorityPOMapper.selectByExample(example);
		if (Utils.isEmpty(ret)) {
			return null;
		} else {
			return ret;
		}
	}

	public List<ReasonCodePriorityPO> readRecordsByTimeRange(String site, Date endDate) throws BusinessException {
		endDate = endDate == null ? new Date() : endDate;
//		ReasonCodePriorityPOExample example = new ReasonCodePriorityPOExample();
//		example.createCriteria().andStartDateTimeLessThan(endDate).andEndDateTimeGreaterThanOrEqualTo(endDate);
//		example.setOrderByClause("PRIORITY,SUB_PRIORITY");
//		List<ReasonCodePriorityPO> ret = reasonCodePriorityPOMapper.selectByExample(example);
		List<ReasonCodePriorityPO> ret = reasonCodePriorityAndReasonCodePOMapper.selectReasonCodePriorityByCondition(site, endDate);
		if (Utils.isEmpty(ret)) {
			throw new BusinessException(ErrorCodeEnum.BASIC, "could not find the specifed reason code priority");
		} else {
			return ret;
		}
	}
	
	public List<ReasonCodePriorityRulePO> readReasonCodePriorityRule(String site, Date filterDate, String mark){
		return this.reasonCodePriorityRulePOMapper.selectPriorityRuleByCondition(site,filterDate,mark);
	}

}
