package mhp.oee.component.plant;

import java.util.List;

import mhp.oee.dao.extend.ReasonCodeRuleParamByConditionPOMapper;
import mhp.oee.dao.extend.ResourceTypeResourceAndResourceTypePOMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.dao.gen.ReasonCodeRuleParamPOMapper;
import mhp.oee.po.gen.ReasonCodeRuleParamPO;
import mhp.oee.po.gen.ReasonCodeRuleParamPOExample;

@Component
public class ReasonCodeRuleParamComponent {

	@InjectableLogger
	private static Logger logger;

	@Autowired
	private ReasonCodeRuleParamPOMapper reasonCodeRuleParamPOMapper;
	@Autowired
	private ResourceTypeResourceAndResourceTypePOMapper resourceTypeResourceAndResourceTypePOMapper;
	@Autowired
	private ReasonCodeRuleParamByConditionPOMapper reasonCodeRuleParamByConditionPOMapper;

	// *
	public List<ReasonCodeRuleParamPO> getRuleParam(String reasonCodeRuleBO) {
		ReasonCodeRuleParamPOExample example = new ReasonCodeRuleParamPOExample();
		example.setOrderByClause("PARAM_ID");
		example.createCriteria().andReasonCodeRuleBoEqualTo(reasonCodeRuleBO);
		List<ReasonCodeRuleParamPO> ret = reasonCodeRuleParamPOMapper.selectByExample(example);
		return ret;
	}

	public List<ReasonCodeRuleParamPO> getRuleParam(String reasonCodeRuleBO, String resourceType) {
		List<ReasonCodeRuleParamPO> ret = reasonCodeRuleParamByConditionPOMapper.select(reasonCodeRuleBO, resourceType);
		return ret;
	}

	public String getResourceTypeByResourceBo(String resourceBo) {
		return resourceTypeResourceAndResourceTypePOMapper.select(resourceBo);
	}

}
