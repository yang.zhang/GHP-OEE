package mhp.oee.component.plant;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.DataDescriptionException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.PLCRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ResourcePlcBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.PlcCategoryAndPlcObjectPOMapper;
import mhp.oee.dao.extend.ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPOMapper;
import mhp.oee.dao.extend.ResrceAndResourcePlcPOMapper;
import mhp.oee.dao.gen.ResourcePOMapper;
import mhp.oee.dao.gen.ResourcePlcPOMapper;
import mhp.oee.po.extend.PlcCategoryAndPlcObjectPO;
import mhp.oee.po.extend.ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPO;
import mhp.oee.po.extend.ResrceAndResourcePlcPO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourcePOExample;
import mhp.oee.po.gen.ResourcePlcPO;
import mhp.oee.po.gen.ResourcePlcPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourcePlcVO;
import mhp.oee.vo.ResourceVO;
import mhp.oee.web.angular.resource.plc.management.excel.parser.RelationCellValidator.CategoryCode;

@Component
public class ResourcePlcComponent {

    @InjectableLogger
    private static Logger logger;
    private static final String RESOURCE_PLC = "RESOURCE_PLC";

    @Autowired
    private ResourcePlcPOMapper resourcePlcPOMapper;

    @Autowired
    private ResourcePOMapper resourcePOMapper;

    @Autowired
    private PlcCategoryAndPlcObjectPOMapper plcCategoryAndPlcObjectPOMapper;
    @Autowired
    private ResrceAndResourcePlcPOMapper resrceAndResourcePlcPOMapper;

    @Autowired
    private ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPOMapper resrceAndResourcePlcAndPlcCategoryAndPlcObjecPOMapper;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    public List<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPO> selectViewResrceAndCategoryAndObj(String site, String resource ,String plcCategory, Integer limit, Integer offset) {
        return  resrceAndResourcePlcAndPlcCategoryAndPlcObjecPOMapper.selectViewResrceAndCategoryAndObj(site, resource, plcCategory, limit, offset);
    }
    public int selectViewResrceAndCategoryAndObjCount(String site, String resource ,String plcCategory) {
        return  resrceAndResourcePlcAndPlcCategoryAndPlcObjecPOMapper.selectViewResrceAndCategoryAndObjCount(site, resource, plcCategory);
    }

    public PlcCategoryAndPlcObjectPO selectPlcCategoryAndPlcObject(String site, String category) {
        return plcCategoryAndPlcObjectPOMapper.selectPlcCategoryAndPlcObject(site, category);
    }

    public ResrceAndResourcePlcPO selectResrceAndResourcePlc(String site, String resource, String category, String plcObjec) {
        return resrceAndResourcePlcPOMapper.selectResrceAndResourcePlc(site, resource, category, plcObjec);
    }

    public List<ResourcePlcVO> selectByExample(String site, String resource) {
        ResrceBOHandle resrceHandle = new ResrceBOHandle(site, resource);
        ResourcePlcPOExample example = new ResourcePlcPOExample();
        example.createCriteria().andResourceBoEqualTo(resrceHandle.getValue());
        List<ResourcePlcPO> pos = resourcePlcPOMapper.selectByExample(example);
        List<ResourcePlcVO> vos = Utils.copyListProperties(pos, ResourcePlcVO.class);
        for (ResourcePlcVO each : vos) {
            // manually stuff RESOURCE to each element, as it does not exist in PO
            each.setResrce(resource);
        }
        return vos;
    }

    public ResourcePlcPO existResourcePlcByHandle(ResourcePlcVO resourcePlcVO) {
        String resourcePlcHandle = new ResourcePlcBOHandle(resourcePlcVO.getSite(), resourcePlcVO.getResourceBo(),
                resourcePlcVO.getPlcAddress(), resourcePlcVO.getPlcValue()).getValue();
        return resourcePlcPOMapper.selectByPrimaryKey(resourcePlcHandle);
    }

    public List<ResourcePlcPO> existResourcePlcByResrceAndObj(ResourcePlcVO resourcePlcVO) {
        if (!Utils.isEmpty(resourcePlcVO.getPlcObject())) {
            ResourcePlcPOExample example = new ResourcePlcPOExample();
            example.createCriteria().andResourceBoEqualTo(resourcePlcVO.getResourceBo()).andPlcObjectEqualTo(resourcePlcVO.getPlcObject());
            return resourcePlcPOMapper.selectByExample(example);
        }
        return null;
    }

    public boolean existPlcObject(ResourcePlcVO resourcePlcVO) {
        String resourcePlcHandle = new ResourcePlcBOHandle(resourcePlcVO.getSite(), resourcePlcVO.getResourceBo(), resourcePlcVO.getPlcAddress(),
                resourcePlcVO.getPlcValue()).getValue();
        ResourcePlcPOExample example = new ResourcePlcPOExample();
        example.createCriteria().andResourceBoEqualTo(resourcePlcVO.getResourceBo()).andPlcObjectEqualTo(resourcePlcVO.getPlcObject())
                .andHandleNotEqualTo(resourcePlcHandle);
        List<ResourcePlcPO> po = resourcePlcPOMapper.selectByExample(example);
        return (po != null && po.size() > 0) ? true : false;
    }



    public String createResourcePlc(ResourcePlcVO resourcePlcVO) throws ExistingRecordException, PLCRecordException {
        ResourcePlcPO existResourcePlc = existResourcePlcByHandle(resourcePlcVO);
        if (existResourcePlc !=null) {
            logger.error("ResourcePlcComponent.createResourcePlc() : Existing RESOURCE_PLC record");
            throw new ExistingRecordException(RESOURCE_PLC, gson.toJson(resourcePlcVO));
        }
        //数据项已存在PLC地址对应，不允许新增;
        if (!resourcePlcVO.getCategory().equals(CategoryCode.AlertInfo.getCode()) && !resourcePlcVO.getCategory().equals(CategoryCode.PartsLife.getCode())) {
            List<ResourcePlcPO> existResourcePlcList = existResourcePlcByResrceAndObj(resourcePlcVO);
            if (existResourcePlcList != null && existResourcePlcList.size() > 0) {
                logger.error("ResourcePlcComponent.createResourcePlc() : Existing RESOURCE_PLC record");
                throw new PLCRecordException(RESOURCE_PLC, gson.toJson(resourcePlcVO));
            }
        }
        ResourcePlcPO po = Utils.copyObjectProperties(resourcePlcVO, ResourcePlcPO.class);
        if (Utils.isEmpty(resourcePlcVO.getPlcValue())) {
            po.setPlcValue("*");
        }
        if (Utils.isEmpty(resourcePlcVO.getPlcAddress())) {
            po.setPlcAddress("*");
        }
        String resourcePlcHandle = new ResourcePlcBOHandle(resourcePlcVO.getSite(), resourcePlcVO.getResourceBo(), po.getPlcAddress(),
                po.getPlcValue()).getValue();
        po.setHandle(resourcePlcHandle); // this is create action, view does not // provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        if(resourcePlcPOMapper.selectByPrimaryKey(resourcePlcHandle)!=null){
            throw new ExistingRecordException("RESOURCE_PLC", gson.toJson(po));
        }
        int ret = resourcePlcPOMapper.insert(po);
        if (ret != 1) {
            logger.error("ResourcePlcComponent.createResourcePlc() : Existing RESOURCE_PLC record");
            throw new ExistingRecordException(RESOURCE_PLC, gson.toJson(resourcePlcVO));
        }
        return resourcePlcHandle;
    }

    public void updateResourcePlc(ResourcePlcVO resourcePlcVO) throws UpdateErrorException, ExistingRecordException {
        if (!resourcePlcVO.getCategory().equals(CategoryCode.AlertInfo.getCode()) && !resourcePlcVO.getCategory().equals(CategoryCode.PartsLife.getCode())) {
            if (existPlcObject(resourcePlcVO)) {
                logger.error("ResourcePlcComponent.updateResourcePlc() : Existing RESOURCE_PLC record");
                throw new ExistingRecordException(RESOURCE_PLC, gson.toJson(resourcePlcVO));
            }
        }
        ResourcePlcPO po = Utils.copyObjectProperties(resourcePlcVO, ResourcePlcPO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        ResourcePlcPOExample example = new ResourcePlcPOExample();
        example.createCriteria().andHandleEqualTo(resourcePlcVO.getHandle()).andModifiedDateTimeEqualTo(resourcePlcVO.getModifiedDateTime());
        int ret = resourcePlcPOMapper.updateByExample(po, example);
        if (ret != 1) {
            logger.error("ResourcePlcComponent.updateResourcePlc() : updateResourcePlc");
            throw new UpdateErrorException(RESOURCE_PLC, gson.toJson(resourcePlcVO));
        }
    }

    public int deleteAccessType(ResourcePlcVO vo) {
        return resourcePlcPOMapper.deleteByPrimaryKey(vo.getHandle());
    }

    public void updateResrce(ResourceVO vo) throws UpdateErrorException {
        ResourcePO po = Utils.copyObjectProperties(vo, ResourcePO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        ResourcePOExample example = new ResourcePOExample();
        example.createCriteria().andHandleEqualTo(vo.getHandle()).andModifiedDateTimeEqualTo(vo.getModifiedDateTime());
        resourcePOMapper.updateByExample(po, example);
    }

    public boolean existResrce(String resourceBo) {
        boolean result = false;
        ResourcePlcPOExample example = new ResourcePlcPOExample();
        example.createCriteria().andResourceBoEqualTo(resourceBo);
        List<ResourcePlcPO> ret = resourcePlcPOMapper.selectByExample(example);
        if (ret != null && ret.size() > 0) {
            result = true;
        }
        return result;
    }
    
    public String getPlcObject(String resourceBo,String plcAddress,String plcValue  ) {
        String plcObject = "";
        ResourcePlcPOExample example = new ResourcePlcPOExample();
        example.createCriteria().andResourceBoEqualTo(resourceBo).andPlcAddressEqualTo(plcAddress)
        	.andPlcValueEqualTo(plcValue);
        List<ResourcePlcPO> ret = resourcePlcPOMapper.selectByExample(example);
        if (ret != null && ret.size() > 0) {
        	plcObject = ret.get(0).getPlcObject();
        }
        return plcObject;
    }
    
    public String getPlcDescription(String resourceBo,String plcAddress,String plcValue  ) {
        String description = "";
        ResourcePlcPOExample example = new ResourcePlcPOExample();
        example.createCriteria().andResourceBoEqualTo(resourceBo).andPlcAddressEqualTo(plcAddress)
        	.andPlcValueEqualTo(plcValue);
        List<ResourcePlcPO> ret = resourcePlcPOMapper.selectByExample(example);
        if (ret != null && ret.size() > 0) {
        	description = ret.get(0).getDescription();
        }
        return description;
    }
}
