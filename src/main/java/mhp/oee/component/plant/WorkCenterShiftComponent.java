
package mhp.oee.component.plant;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.common.handle.WorkCenterBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.dao.gen.WorkCenterShiftPOMapper;
import mhp.oee.po.gen.WorkCenterShiftPO;
import mhp.oee.po.gen.WorkCenterShiftPOExample;

@Component
public class WorkCenterShiftComponent {

	@InjectableLogger
	private static Logger logger;

	@Autowired
	private WorkCenterShiftPOMapper workCenterShiftPOMapper;

	public List<WorkCenterShiftPO> getWorkCenterShiftByWorkCenterBO(String workCenterBO){
		if (StringUtils.isBlank(workCenterBO)) {
			return new ArrayList<WorkCenterShiftPO>(0);
		}
		WorkCenterShiftPOExample example = new WorkCenterShiftPOExample();
		example.createCriteria().andWorkCenterBoEqualTo(workCenterBO);
		List<WorkCenterShiftPO> ret = workCenterShiftPOMapper.selectByExample(example);
		return ret;
	}

    public List<WorkCenterShiftPO> getShift(String site,String work_center) {
        String handle=new WorkCenterBOHandle(site,work_center).getValue();
        WorkCenterShiftPOExample example = new WorkCenterShiftPOExample();
        example.createCriteria().andWorkCenterBoEqualTo(handle).andEnabledEqualTo("true");
        List<WorkCenterShiftPO> list=workCenterShiftPOMapper.selectByExample(example);
        return list;
    }

}