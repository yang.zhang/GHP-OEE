package mhp.oee.component.plant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mhp.oee.dao.gen.ResourceCurrentDataPOMapper;
import mhp.oee.po.gen.ResourceCurrentDataPO;

@Component
public class ResourceCurrentDataComponent {

    @Autowired
    private ResourceCurrentDataPOMapper resourceCurrentDataPOMapper;
    
    public ResourceCurrentDataPO selectByHandle(String handle) {
        return resourceCurrentDataPOMapper.selectByPrimaryKey(handle);
    }
    
    public void updateByPrimaryKey(ResourceCurrentDataPO po) {
        resourceCurrentDataPOMapper.updateByPrimaryKey(po);
    }
    
    public void insert(ResourceCurrentDataPO po) {
        resourceCurrentDataPOMapper.insert(po);
    }

}
