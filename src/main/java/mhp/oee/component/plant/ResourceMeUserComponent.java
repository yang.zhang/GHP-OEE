package mhp.oee.component.plant;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.InvalidResourceException;
import mhp.oee.common.exception.InvalidWorkCenterException;
import mhp.oee.common.handle.ResourceMeUserBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.ResourceMeUserPOMapper;
import mhp.oee.dao.gen.ResourcePOMapper;
import mhp.oee.po.gen.ResourceMeUserPO;
import mhp.oee.po.gen.ResourceMeUserPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceMeUserVO;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
@Component
public class ResourceMeUserComponent {

    @Autowired
    private ResourceMeUserPOMapper resourceMeUserPOMapper;
    @Autowired
    private ResourcePOMapper resourcePOMapper;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();


    public boolean existResourceMeUser(String handle) throws BusinessException {
        ResourceMeUserPO po = resourceMeUserPOMapper.selectByPrimaryKey(handle);
        return (po != null) ? true : false;
    }

    public String createResourceMeUser(ResourceMeUserVO resourceMeUserVO) throws BusinessException {
        String handle = new ResourceMeUserBOHandle(resourceMeUserVO.getResourceBo(), resourceMeUserVO.getMeUserId(),
                resourceMeUserVO.getShift()).getValue();
        ResourceMeUserPO existingPO = resourceMeUserPOMapper.selectByPrimaryKey(handle);
        if (existingPO != null) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "Existing RESOURCE_ME_USER record : " + gson.toJson(existingPO));
        }

        ResourceMeUserPO po = Utils.copyObjectProperties(resourceMeUserVO, ResourceMeUserPO.class);
        if (resourcePOMapper.selectByPrimaryKey(po.getResourceBo()) == null) {
            throw new InvalidResourceException();
        }

        po.setHandle(handle);
        po.setMeUserId(po.getMeUserId().toUpperCase()); // force to be upper case
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = resourceMeUserPOMapper.insert(po);
        if (ret != 1) {
            existingPO = resourceMeUserPOMapper.selectByPrimaryKey(handle);
            throw new BusinessException(ErrorCodeEnum.BASIC, "Existing RESOURCE_ME_USER record : " + gson.toJson(existingPO));
        }
        return handle;
    }

    public List<ResourceMeUserPO> readResourceMeUsersByExample(ResourceMeUserPOExample example) {
        return resourceMeUserPOMapper.selectByExample(example);
    }

    public void updateResourceMeUser(ResourceMeUserVO resourceMeUserVO) throws BusinessException {
        ResourceMeUserPO po = Utils.copyObjectProperties(resourceMeUserVO, ResourceMeUserPO.class);
        if (resourcePOMapper.selectByPrimaryKey(po.getResourceBo()) == null) {
            throw new InvalidWorkCenterException();
        }

        po.setHandle(new ResourceMeUserBOHandle(po.getResourceBo(), po.getMeUserId(), po.getShift()).getValue());
        po.setMeUserId(po.getMeUserId().toUpperCase()); // force to be upper case
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = resourceMeUserPOMapper.updateByPrimaryKey(po);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "updateResourceMeUser");
        }
    }

}
