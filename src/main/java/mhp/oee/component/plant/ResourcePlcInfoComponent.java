package mhp.oee.component.plant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mhp.oee.utils.ISplitStringFormat;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ResourcePlcInfoBOHandle;
import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.ResourcePlcInfoAndResrceAndPlcAccessTypePOMapper;
import mhp.oee.dao.extend.ResourceTypeResourceAndResourceAndResourceTypePOMapper;
import mhp.oee.dao.extend.WorkCenterMemberAndWorkCenterPOMapper;
import mhp.oee.dao.gen.ResourcePlcInfoPOMapper;
import mhp.oee.dao.gen.ResourceTypePOMapper;
import mhp.oee.dao.gen.WorkCenterPOMapper;
import mhp.oee.po.extend.ResourcePlcInfoAndResrceAndPlcAccessTypePO;
import mhp.oee.po.extend.ResourceTypeResourceAndResourceAndResourceTypePO;
import mhp.oee.po.extend.WorkCenterMemberAndWorkCenterPO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourcePlcInfoPO;
import mhp.oee.po.gen.ResourcePlcInfoPOExample;
import mhp.oee.po.gen.ResourceTypePO;
import mhp.oee.po.gen.ResourceTypePOExample;
import mhp.oee.po.gen.WorkCenterPO;
import mhp.oee.po.gen.WorkCenterPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.PlcInfoConditionVO;
import mhp.oee.vo.ResourcePlcInfoAndResrceAndPlcAccessTypeVO;
import mhp.oee.vo.ResourcePlcInfoVO;
import mhp.oee.vo.ResourceTypeConditionVO;
import mhp.oee.vo.ResourceVO;
import mhp.oee.vo.WorkCenterConditionVO;
import mhp.oee.web.angular.resource.plc.management.WcCategory;

@Component
public class ResourcePlcInfoComponent {
    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static final String ENABLE_Y = "true";
    private static final String ENABLE_N = "false";
    private static final String WORK_CENTER = "WORK_CENTER";
    private static final String RESOURCE_TYPE = "RESOURCE_TYPE";
    private static final String RESOURCE_PLC_INFO = "RESOURCE_PLC_INFO";

    @Autowired
    private ResourceTypePOMapper resourceTypePOMapper;

    @Autowired
    private WorkCenterPOMapper workCenterPOMapper;

    @Autowired
    private WorkCenterMemberAndWorkCenterPOMapper workCenterMemberAndWorkCenterPOMapper;

    @Autowired
    private ResourcePlcInfoPOMapper resourcePlcInfoPOMapper;

    @Autowired
    private ResourceTypeResourceAndResourceAndResourceTypePOMapper resourceTypeResourceAndResourceAndResourceTypePOMapper;

    @Autowired
    private ResourcePlcInfoAndResrceAndPlcAccessTypePOMapper resourcePlcInfoAndResrceAndPlcAccessTypePOMapper;

    public List<WorkCenterConditionVO> readAllWorkCenter(String site) {
        WorkCenterPOExample poExample = new WorkCenterPOExample();
        poExample.createCriteria().andSiteEqualTo(site).andWcCategoryEqualTo(WcCategory.ZLEVEL6.getName()).andEnabledEqualTo(ENABLE_Y);
        poExample.setOrderByClause(WORK_CENTER);
        List<WorkCenterPO> pos = workCenterPOMapper.selectByExample(poExample);
        List<WorkCenterConditionVO> vos = Utils.copyListProperties(pos, WorkCenterConditionVO.class);
        return vos;
    }

    public List<ResourcePlcInfoPO> existPcIp(String site, String pcIp) {
        ResourcePlcInfoPOExample example = new ResourcePlcInfoPOExample();
        example.createCriteria().andPcIpEqualTo(pcIp);
        List<ResourcePlcInfoPO> rpPOs = resourcePlcInfoPOMapper.selectByExample(example);
        return rpPOs;
    }

    public List<ResourcePlcInfoVO> getResourcePlcInfoPOByPcIps(List<String> pcIps) {
        ResourcePlcInfoPOExample example = new ResourcePlcInfoPOExample();
        example.createCriteria().andPcIpIn(pcIps);
        List<ResourcePlcInfoPO> rpPOs = resourcePlcInfoPOMapper.selectByExample(example);
        return Utils.copyListProperties(rpPOs, ResourcePlcInfoVO.class);
    }

    public List<WorkCenterConditionVO> readWorkCenterHelp(String site, String workCenter) {
        WorkCenterPOExample poExample = new WorkCenterPOExample();
        String[]  workCenterArr = Utils.ConvertStrtoArrayLike(workCenter);
        poExample.createCriteria().andSiteEqualTo(site).andWcCategoryEqualTo(WcCategory.ZLEVEL6.getName()).andWorkCenterLikeMore(workCenterArr);
        poExample.setOrderByClause("DESCRIPTION");
        List<WorkCenterPO> pos = workCenterPOMapper.selectByExample(poExample);
        List<WorkCenterConditionVO> vos = Utils.copyListProperties(pos, WorkCenterConditionVO.class);
        return vos;
    }

    public List<WorkCenterConditionVO> readWorkCenter(List<String> siteList,String workCenter){
        String[]  workCenterArr = Utils.ConvertStrtoArrayLike(workCenter);
        List<WorkCenterPO> pos = workCenterPOMapper.selectWorkCenter(siteList,workCenterArr);
        List<WorkCenterConditionVO> vos = Utils.copyListProperties(pos, WorkCenterConditionVO.class);
        return vos;
    }

	public List<WorkCenterConditionVO> readLine(String site, String workCenterBO) {
        List<WorkCenterPO> workCenterPOs = new ArrayList<WorkCenterPO>();
        List<WorkCenterMemberAndWorkCenterPO> workCenterMemberAndWorkCenterPOs = workCenterMemberAndWorkCenterPOMapper.selectWorkCenterMemberAndWorkCenter(site, workCenterBO);
        if (workCenterMemberAndWorkCenterPOs != null) {
            for (WorkCenterMemberAndWorkCenterPO workCenterMembeAndWorkCenterrPO : workCenterMemberAndWorkCenterPOs) {
                workCenterPOs.add(workCenterMembeAndWorkCenterrPO.getWorkCenterPO());
            }
        }
        return Utils.copyListProperties(workCenterPOs, WorkCenterConditionVO.class);
    }

    public List<WorkCenterConditionVO> readLineHelp(String site, List<String> workCenterBO, String line) {
        
    	//多个关键词模糊搜索
    	String[] lines = Utils.ConvertStrtoArrayLike(line);
    	List<WorkCenterPO> workCenterPOs = workCenterMemberAndWorkCenterPOMapper.selectLineByWorkCenter(site, workCenterBO, lines);
        
        /*//单个关键词模糊搜索
        List<WorkCenterPO> workCenterPOs = workCenterMemberAndWorkCenterPOMapper.selectLineByWorkCenter(site, workCenterBO, line);
        */
        return Utils.copyListProperties(workCenterPOs, WorkCenterConditionVO.class);
    }

    public List<WorkCenterConditionVO> readLinesByWorkAreaAndSites(List<String> siteList, List<String> workAreaList, String lineString){
        //多个关键词模糊搜索
        String[] lines = Utils.ConvertStrtoArrayLike(lineString);
        List<WorkCenterPO> workCenterPOs = workCenterMemberAndWorkCenterPOMapper.selectLineByWorkCenterAndSites(siteList, workAreaList, lines);

        return Utils.copyListProperties(workCenterPOs, WorkCenterConditionVO.class);
    }

    public List<ResourceVO> readResourceHelp(String site,String resrceType, String[] resrces, List<String> lineList, List<String> workCenterList) {
        List<ResourceVO> ResourceVOs = resourceTypeResourceAndResourceAndResourceTypePOMapper.select(site, resrceType,resrces, lineList, workCenterList);
        return ResourceVOs;
    }

    public List<ResourceVO> readOeeResource(String site, String resrce, List<String> lineList, List<String> workCenterList) {
        List<ResourceVO> ResourceVOs = resourceTypeResourceAndResourceAndResourceTypePOMapper.selectOeeResrce(site, resrce, lineList, workCenterList);
        return ResourceVOs;
    }
    public List<ResourceTypeConditionVO> readAllResourceType(String site) {
        ResourceTypePOExample poExample = new ResourceTypePOExample();
        poExample.createCriteria().andSiteEqualTo(site).andEnabledEqualTo(ENABLE_Y);
        poExample.setOrderByClause("RESOURCE_TYPE");
        List<ResourceTypePO> pos = resourceTypePOMapper.selectByExample(poExample);
        List<ResourceTypeConditionVO> vos = Utils.copyListProperties(pos, ResourceTypeConditionVO.class);
        return vos;
    }

    public List<ResourceTypeConditionVO> readResourceTypeBySite(List<String> siteList){
        List<ResourceTypePO> pos = resourceTypePOMapper.selectBySites(siteList);
        List<ResourceTypeConditionVO> vos = Utils.copyListProperties(pos, ResourceTypeConditionVO.class);
        return vos;
    }


    public List<ResourceVO> plcReadResource(final String site, String resourceType, String line, String workCenter) {
        // 将类似"resource1,resource2,resource3"这样的字符串转变成"resourceBo1#resourceBo2#resourceBo3"
        String resourceTypeBO = Utils.formatSplitString(resourceType, ",", "#", new ISplitStringFormat() {
            @Override
            public String format(String rt) {
                return new ResourceTypeBOHandle(site, rt).getValue();
            }
        });

        // LINE_AREA
        if (!Utils.isEmpty(line)) {
            String[] lineArray = line.split(",");
            line = Utils.formatInParams2("LINE_AREA", lineArray);
        }

        // WORK_AREA
        if (!Utils.isEmpty(workCenter)) {
            String[] workCenterArray = workCenter.split(",");
            workCenter = Utils.formatInParams2("WORK_AREA", workCenterArray);
        }

        // RESOURCE_TYPE_BO
        if (!Utils.isEmpty(resourceTypeBO)) {
            String[] resourceTypeBOArray = resourceTypeBO.split("#");
            resourceTypeBO = Utils.formatInParams2("RESOURCE_TYPE_BO", resourceTypeBOArray);
        }

        List<ResourcePO> resourcePOs = new ArrayList<ResourcePO>();
        List<ResourceTypeResourceAndResourceAndResourceTypePO> resourceTypeResourceAndResourceAndResourceTypePOs =
                resourceTypeResourceAndResourceAndResourceTypePOMapper.selectPlcResourceTypeResourceAndResource(site, resourceTypeBO, line, workCenter);
        if (resourceTypeResourceAndResourceAndResourceTypePOs != null) {
            for (ResourceTypeResourceAndResourceAndResourceTypePO resourceTypeResourceAndResourceAndResourceTypePO : resourceTypeResourceAndResourceAndResourceTypePOs) {
                resourcePOs.add(resourceTypeResourceAndResourceAndResourceTypePO.getResourcePO());
            }
        }
        return Utils.copyListProperties(resourcePOs, ResourceVO.class);
    }

    public List<ResourceVO> readResource(final String site, String resourceType, String line, String workCenter) {
        // 将类似"resource1,resource2,resource3"这样的字符串转变成"resourceBo1#resourceBo2#resourceBo3"
        String resourceTypeBO = Utils.formatSplitString(resourceType, ",", "#", new ISplitStringFormat() {
            @Override
            public String format(String rt) {
                return new ResourceTypeBOHandle(site, rt).getValue();
            }
        });

        // LINE_AREA
        if (!Utils.isEmpty(line)) {
            String[] lineArray = line.split(",");
            line = Utils.formatInParams2("LINE_AREA", lineArray);
        }

        // WORK_AREA
        if (!Utils.isEmpty(workCenter)) {
            String[] workCenterArray = workCenter.split(",");
            workCenter = Utils.formatInParams2("WORK_AREA", workCenterArray);
        }

        // RESOURCE_TYPE_BO
        if (!Utils.isEmpty(resourceTypeBO)) {
            String[] resourceTypeBOArray = resourceTypeBO.split("#");
            resourceTypeBO = Utils.formatInParams2("RESOURCE_TYPE_BO", resourceTypeBOArray);
        }

        List<ResourcePO> resourcePOs = new ArrayList<ResourcePO>();
        List<ResourceTypeResourceAndResourceAndResourceTypePO> resourceTypeResourceAndResourceAndResourceTypePOs =
                resourceTypeResourceAndResourceAndResourceTypePOMapper.selectResourceTypeResourceAndResource(site, resourceTypeBO, line, workCenter);
        if (resourceTypeResourceAndResourceAndResourceTypePOs != null) {
            for (ResourceTypeResourceAndResourceAndResourceTypePO resourceTypeResourceAndResourceAndResourceTypePO : resourceTypeResourceAndResourceAndResourceTypePOs) {
                resourcePOs.add(resourceTypeResourceAndResourceAndResourceTypePO.getResourcePO());
            }
        }
        return Utils.copyListProperties(resourcePOs, ResourceVO.class);
    }

    public List<ResourcePlcInfoAndResrceAndPlcAccessTypeVO> readResPlcInfoByRes(PlcInfoConditionVO plcInfoConditionVO) {
        plcInfoConditionVO.setHasResource(ENABLE_Y);
        List<ResourcePlcInfoAndResrceAndPlcAccessTypePO> pos = resourcePlcInfoAndResrceAndPlcAccessTypePOMapper.selectResPlcInfoAndResrceAndPlcAccessType(plcInfoConditionVO, new String[0]);
        List<ResourcePlcInfoAndResrceAndPlcAccessTypeVO> vos = Utils.copyListProperties(pos, ResourcePlcInfoAndResrceAndPlcAccessTypeVO.class);
        return vos;
    }
    public int readResPlcInfoByResCount(PlcInfoConditionVO plcInfoConditionVO) {
        plcInfoConditionVO.setHasResource(ENABLE_Y);
        int count = resourcePlcInfoAndResrceAndPlcAccessTypePOMapper.selectResPlcInfoAndResrceAndPlcAccessTypeCount(plcInfoConditionVO, new String[0]);
        return count;
    }

    public List<ResourcePlcInfoAndResrceAndPlcAccessTypeVO> readResPlcInfo(PlcInfoConditionVO plcInfoConditionVO) {
        List<ResourcePlcInfoAndResrceAndPlcAccessTypeVO> vos = new ArrayList<ResourcePlcInfoAndResrceAndPlcAccessTypeVO>();
        String resourceTypeBO = null;
        if (!Utils.isEmpty(plcInfoConditionVO.getResourceType())) {
            resourceTypeBO = new ResourceTypeBOHandle(plcInfoConditionVO.getSite(),
                    plcInfoConditionVO.getResourceType()).getValue();
            resourceTypeBO="RESOURCE_TYPE_BO = '"+resourceTypeBO+"'";
        }
        List<ResourceTypeResourceAndResourceAndResourceTypePO> resourceTypeResourcePO = resourceTypeResourceAndResourceAndResourceTypePOMapper
                .selectResourceTypeResourceAndResource(plcInfoConditionVO.getSite(),resourceTypeBO,
                        "LINE_AREA = '"+plcInfoConditionVO.getLine()+"'", "WORK_AREA = '"+plcInfoConditionVO.getWorkCenter()+"'");
        Set<String> resourceSet = new HashSet<String>();
        for (ResourceTypeResourceAndResourceAndResourceTypePO resourceTypeResourcePO2 : resourceTypeResourcePO) {
            resourceSet.add(resourceTypeResourcePO2.getResourcePO().getResrce());
        }
        if (resourceSet.size() > 0) {
            plcInfoConditionVO.setHasResource(ENABLE_N);
        } else {
            return vos;
        }
        List<ResourcePlcInfoAndResrceAndPlcAccessTypePO> pos = resourcePlcInfoAndResrceAndPlcAccessTypePOMapper
                .selectResPlcInfoAndResrceAndPlcAccessType(plcInfoConditionVO, resourceSet.toArray());
        vos = Utils.copyListProperties(pos, ResourcePlcInfoAndResrceAndPlcAccessTypeVO.class);
        return vos;
    }

    public int readResPlcInfoCount(PlcInfoConditionVO plcInfoConditionVO) {
        int count = 0;
        String resourceTypeBO = null;
        if (!Utils.isEmpty(plcInfoConditionVO.getResourceType())) {
            resourceTypeBO = new ResourceTypeBOHandle(plcInfoConditionVO.getSite(),
                    plcInfoConditionVO.getResourceType()).getValue();
            resourceTypeBO="RESOURCE_TYPE_BO = '"+resourceTypeBO+"'";
        }
        List<ResourceTypeResourceAndResourceAndResourceTypePO> resourceTypeResourcePO = resourceTypeResourceAndResourceAndResourceTypePOMapper
                .selectResourceTypeResourceAndResource(plcInfoConditionVO.getSite(),resourceTypeBO,
                        "LINE_AREA = '"+plcInfoConditionVO.getLine()+"'", "WORK_AREA = '"+plcInfoConditionVO.getWorkCenter()+"'");
        Set<String> resourceSet = new HashSet<String>();
        for (ResourceTypeResourceAndResourceAndResourceTypePO resourceTypeResourcePO2 : resourceTypeResourcePO) {
            resourceSet.add(resourceTypeResourcePO2.getResourcePO().getResrce());
        }
        if (resourceSet.size() > 0) {
            plcInfoConditionVO.setHasResource(ENABLE_N);
        } else {
            return count;
        }
        count = resourcePlcInfoAndResrceAndPlcAccessTypePOMapper
                .selectResPlcInfoAndResrceAndPlcAccessTypeCount(plcInfoConditionVO, resourceSet.toArray());
        return count;
    }

    public boolean existResPlcInfo(String handle) {
        ResourcePlcInfoPO pos = resourcePlcInfoPOMapper.selectByPrimaryKey(handle);
        return (pos != null) ? true : false;
    }

    public String createResPlcInfo(ResourcePlcInfoVO resourcePlcInfoVO) throws BusinessException {
        ResourcePlcInfoBOHandle resourcePlcInfoBOHandle = new ResourcePlcInfoBOHandle(resourcePlcInfoVO.getSite(), resourcePlcInfoVO.getResourceBo());
        if (existResPlcInfo(resourcePlcInfoBOHandle.getValue())) {
            logger.error("ResourcePlcInfoComponent.createResPlcInfo() : Existing RESOURCE_PLC_INFO  record");
            throw new ExistingRecordException(RESOURCE_PLC_INFO, gson.toJson(resourcePlcInfoVO));
        }
        ResourcePlcInfoPO po = Utils.copyObjectProperties(resourcePlcInfoVO, ResourcePlcInfoPO.class);
        po.setHandle(resourcePlcInfoBOHandle.getValue()); // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = resourcePlcInfoPOMapper.insert(po);
        if (ret != 1) {
            logger.error("ResourcePlcInfoComponent.createResPlcInfo() : Existing RESOURCE_PLC_INFO  record");
            throw new ExistingRecordException(RESOURCE_PLC_INFO, gson.toJson(resourcePlcInfoVO));
        }
        return resourcePlcInfoBOHandle.getValue();
    }

    public ResourcePlcInfoPO readResPlcInfo(String handle) {
        return resourcePlcInfoPOMapper.selectByPrimaryKey(handle);
    }

    public void updateResPlcInfo(ResourcePlcInfoVO resourcePlcInfoVO) throws BusinessException {
        ResourcePlcInfoPO po = Utils.copyObjectProperties(resourcePlcInfoVO, ResourcePlcInfoPO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        ResourcePlcInfoPOExample example = new ResourcePlcInfoPOExample();
        example.createCriteria().andHandleEqualTo(resourcePlcInfoVO.getHandle()).andModifiedDateTimeEqualTo(resourcePlcInfoVO.getModifiedDateTime());
        int ret = resourcePlcInfoPOMapper.updateByExample(po, example);
        if (ret != 1) {
            logger.error("ResourcePlcInfoComponent.updateResPlcInfo() : updateResPlcInfo");
            throw new UpdateErrorException(RESOURCE_PLC_INFO, gson.toJson(resourcePlcInfoVO));
        }
    }

    public int deleteResPlcInfo(ResourcePlcInfoVO resourcePlcInfoVO) throws BusinessException {
        return resourcePlcInfoPOMapper.deleteByPrimaryKey(resourcePlcInfoVO.getHandle());
    }

    public List<ResourcePlcInfoVO> getAllPcIp(){
        return resourcePlcInfoAndResrceAndPlcAccessTypePOMapper.selectPcIp();
    }

//    public List<ResourceVO> readResourceWithMultipleLineAddLineArea(String site, String resourceType,
//            String[] lineArray, String workCenter) {
//        List<ResourceVO> resourceVOs = new ArrayList<ResourceVO>();
//        List<String> lineList = Arrays.asList(lineArray);
//        List<ResourceVO> list = resourceTypeResourceAndResourceAndResourceTypePOMapper
//                .selectResourceTypeResourceAndResourceAddLineArea(site, resourceType, "", workCenter);
//        if (list != null) {
//            for (ResourceVO vo : list) {
//                if (lineList.contains(vo.getLineArea())) {
//                    resourceVOs.add(vo);
//                }
//            }
//        }
//        return resourceVOs;
//    }

    public List<ResourceVO> readResourceWithMultipleLine(String site, String resourceType, String[] lineArray,
                                                         String workCenter) {
     // WORK_AREA
        if (!Utils.isEmpty(workCenter)) {
            String[] workCenterArray = workCenter.split(",");
            workCenter = Utils.formatInParams2("WORK_AREA", workCenterArray);
        }
        
        List<ResourcePO> resourcePOs = new ArrayList<ResourcePO>();
        List<String> lineList = Arrays.asList(lineArray);
        resourceType="RESOURCE_TYPE_BO = '"+resourceType+"'";
        List<ResourceTypeResourceAndResourceAndResourceTypePO> list = resourceTypeResourceAndResourceAndResourceTypePOMapper.selectResourceTypeResourceAndResource(site, resourceType, "", workCenter);
        if (list != null) {
            for (ResourceTypeResourceAndResourceAndResourceTypePO po : list) {
                ResourcePO resourcePO = po.getResourcePO();
                if(lineList.contains(resourcePO.getLineArea())) {
                    resourcePOs.add(resourcePO);
                }
            }
        }

        return Utils.copyListProperties(resourcePOs, ResourceVO.class);
    }

    public List<ResourceVO> readResourceAddLineArea(String site, String resourceType, String line, String workCenter) {
        return resourceTypeResourceAndResourceAndResourceTypePOMapper
                .selectResourceTypeResourceAndResourceAddLineArea(site, resourceType, line, workCenter);
    }
    
    public List<ResourcePlcInfoPO> findBySiteAndResource (String site, String resource, String pcId) {
    	
    	ResrceBOHandle resrceBOHandle = new ResrceBOHandle(site, resource);
    	ResourcePlcInfoPOExample example = new ResourcePlcInfoPOExample();
        mhp.oee.po.gen.ResourcePlcInfoPOExample.Criteria criteria = example.createCriteria();
        criteria.andResourceBoEqualTo(resrceBOHandle.getValue());
        return resourcePlcInfoPOMapper.selectByExample(example);
    }
}
