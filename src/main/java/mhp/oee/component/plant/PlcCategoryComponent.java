package mhp.oee.component.plant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.dao.gen.PlcCategoryPOMapper;
import mhp.oee.po.gen.PlcCategoryPO;

@Component
public class PlcCategoryComponent {

    @Autowired
    private PlcCategoryPOMapper categoryPOMapper;

    public PlcCategoryPO getCategoryByHandle(String handle) {
        return this.categoryPOMapper.selectByPrimaryKey(handle);
    }


}
