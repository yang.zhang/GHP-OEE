package mhp.oee.component.plant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mchange.v2.resourcepool.ResourcePool;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.InvalidWorkCenterException;
import mhp.oee.common.handle.WorkCenterBOHandle;
import mhp.oee.common.handle.WorkCenterMemberBOHandle;
import mhp.oee.common.handle.WorkCenterShiftBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.WorkCenterAndWorkCenterMemberPOMapper;
import mhp.oee.dao.gen.ResourcePOMapper;
import mhp.oee.dao.gen.ResourceTypeResourcePOMapper;
import mhp.oee.dao.gen.WorkCenterMemberPOMapper;
import mhp.oee.dao.gen.WorkCenterPOMapper;
import mhp.oee.dao.gen.WorkCenterShiftPOMapper;
import mhp.oee.po.extend.WorkCenterAndWorkCenterMemberPO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourcePOExample;
import mhp.oee.po.gen.ResourceTypeResourcePO;
import mhp.oee.po.gen.ResourceTypeResourcePOExample;
import mhp.oee.po.gen.WorkCenterMemberPO;
import mhp.oee.po.gen.WorkCenterMemberPOExample;
import mhp.oee.po.gen.WorkCenterPO;
import mhp.oee.po.gen.WorkCenterPOExample;
import mhp.oee.po.gen.WorkCenterShiftPO;
import mhp.oee.po.gen.WorkCenterShiftPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.WorkCenterConditionVO;
import mhp.oee.vo.WorkCenterMemberVO;
import mhp.oee.vo.WorkCenterShiftVO;
import mhp.oee.vo.WorkCenterVO;

@Component
public class WorkCenterComponent {

    public static final String WC_CATEGORY_LINE_AREA = "LEVEL1";
    public static final String WC_CATEGORY_WORK_AREA = "ZLEVEL6";

    @Autowired
    private WorkCenterPOMapper workCenterPOMapper;
    @Autowired
    private WorkCenterShiftPOMapper workCenterShiftPOMapper;
    @Autowired
    private WorkCenterMemberPOMapper workCenterMemberPOMapper;
    @Autowired
    private WorkCenterAndWorkCenterMemberPOMapper workCenterAndWorkCenterMemberPOMapper;
    @Autowired
    private ResourcePOMapper resourcePOMapper;
    @Autowired
    private ResourceTypeResourcePOMapper resourceTypeResourcePOMapper;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();


    // ==========================================================================
    // Work_Center
    // ==========================================================================

    public boolean existWorkCenter(String site, String workCenter) {
        String handle = new WorkCenterBOHandle(site, workCenter).getValue();
        WorkCenterPO po = workCenterPOMapper.selectByPrimaryKey(handle);
        return (po != null) ? true : false;
    }

    public String createWorkCenter(WorkCenterVO workCenterVO) throws BusinessException {
        String handle = new WorkCenterBOHandle(workCenterVO.getSite(), workCenterVO.getWorkCenter()).getValue();
        WorkCenterPO existingPO = workCenterPOMapper.selectByPrimaryKey(handle);
        if (existingPO != null) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "Existing WORK_CENTER record : " + gson.toJson(existingPO));
        }

        WorkCenterPO po = Utils.copyObjectProperties(workCenterVO, WorkCenterPO.class);
        po.setHandle(handle);   // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = workCenterPOMapper.insert(po);
        if (ret != 1) {
            existingPO = workCenterPOMapper.selectByPrimaryKey(handle);
            throw new BusinessException(ErrorCodeEnum.BASIC, "Existing WORK_CENTER record : " + gson.toJson(existingPO));
        }
        return handle;
    }

    public WorkCenterVO readWorkCenterByPrimaryKey(String handle) {
        WorkCenterPO po = workCenterPOMapper.selectByPrimaryKey(handle);
        WorkCenterVO vo = Utils.copyObjectProperties(po, WorkCenterVO.class);
        return vo;
    }

    public List<WorkCenterVO> readWorkCenterByExample(WorkCenterPOExample example) {
        List<WorkCenterPO> pos = workCenterPOMapper.selectByExample(example);
        List<WorkCenterVO> vos = Utils.copyListProperties(pos, WorkCenterVO.class);
        return vos;
    }

    public void updateWorkCenter(WorkCenterVO workCenterVO) throws BusinessException {
        WorkCenterPO po = Utils.copyObjectProperties(workCenterVO, WorkCenterPO.class);
        po.setHandle(new WorkCenterBOHandle(po.getSite(), po.getWorkCenter()).getValue());
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = workCenterPOMapper.updateByPrimaryKey(po);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "updateWorkCenter");
        }
    }


    // ==========================================================================
    // Work_Center_Shift
    // ==========================================================================

    public boolean existWorkCenterShift(String site, String workCenter, String shift) {
        String wcHandle = new WorkCenterBOHandle(site, workCenter).getValue();
        String handle = new WorkCenterShiftBOHandle(wcHandle, shift).getValue();
        WorkCenterShiftPO po = workCenterShiftPOMapper.selectByPrimaryKey(handle);
        return (po != null) ? true : false;
    }

    public String createWorkCenterShift(WorkCenterShiftVO wcsVO) throws BusinessException {
        String handle = new WorkCenterShiftBOHandle(wcsVO.getWorkCenterBo(), wcsVO.getShift()).getValue();
        WorkCenterShiftPO existingPO = workCenterShiftPOMapper.selectByPrimaryKey(handle);
        if (existingPO != null) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "Existing WORK_CENTER_SHIFT record : " + gson.toJson(existingPO));
        }

        WorkCenterShiftPO po = Utils.copyObjectProperties(wcsVO, WorkCenterShiftPO.class);
        po.setHandle(handle);   // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = workCenterShiftPOMapper.insert(po);
        if (ret != 1) {
            existingPO = workCenterShiftPOMapper.selectByPrimaryKey(handle);
            throw new BusinessException(ErrorCodeEnum.BASIC, "Existing WORK_CENTER_SHIFT record : " + gson.toJson(existingPO));
        }
        return handle;
    }

    public List<WorkCenterShiftVO> readWorkCenterShiftByExample(WorkCenterShiftPOExample example) {
        List<WorkCenterShiftPO> pos = workCenterShiftPOMapper.selectByExample(example);
        List<WorkCenterShiftVO> vos = Utils.copyListProperties(pos, WorkCenterShiftVO.class);
        return vos;
    }

    public void updateWorkCenterShift(WorkCenterShiftVO wcsVO) throws BusinessException {
        WorkCenterShiftPO po = Utils.copyObjectProperties(wcsVO, WorkCenterShiftPO.class);
        po.setHandle(new WorkCenterShiftBOHandle(po.getWorkCenterBo(), po.getShift()).getValue());
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = workCenterShiftPOMapper.updateByPrimaryKey(po);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "updateWorkCenterShift");
        }
    }


    // ==========================================================================
    // Work_Center_Member
    // ==========================================================================

    public int createWorkCenterMember(WorkCenterMemberVO vo) throws BusinessException {
        WorkCenterPO wcPO = workCenterPOMapper.selectByPrimaryKey(vo.getWorkCenterBo());
        if (wcPO == null) {
            throw new InvalidWorkCenterException();
        }
        WorkCenterPO wcmPO = workCenterPOMapper.selectByPrimaryKey(vo.getWorkCenterGbo());
        if (wcmPO == null) {
            // GBO member should also be an existing record in work_center
            throw new InvalidWorkCenterException();
        }

        WorkCenterMemberPO po = Utils.copyObjectProperties(vo, WorkCenterMemberPO.class);
        po.setHandle(new WorkCenterMemberBOHandle(po.getWorkCenterBo(), po.getWorkCenterGbo()).getValue());
        po.setModifiedUser(ContextHelper.getContext().getUser());
        return workCenterMemberPOMapper.insert(po);
    }

    public int deleteWorkCenterMemberByMemberRef(String memberRef) {
        WorkCenterMemberPOExample example = new WorkCenterMemberPOExample();
        example.createCriteria().andWorkCenterGboEqualTo(memberRef);
        return workCenterMemberPOMapper.deleteByExample(example);
    }

    public List<WorkCenterConditionVO> readLineAreas(String site, String workCenterHandle) {
        List<WorkCenterAndWorkCenterMemberPO> pos = workCenterAndWorkCenterMemberPOMapper.selectWorkCenterAndWorkCenterMember(site, workCenterHandle);
        return Utils.copyListProperties(pos, WorkCenterConditionVO.class);
    }

    public String readWorkCenterBo(String site, String workCenter){
        if (StringUtils.isBlank(site) || StringUtils.isBlank(workCenter)) {
            return "";
        }
    	WorkCenterPOExample example = new WorkCenterPOExample();
        example.createCriteria().andSiteEqualTo(site).andWorkCenterEqualTo(workCenter);
        List<WorkCenterPO> ret = workCenterPOMapper.selectByExample(example);
        if (ret!=null) {
        	return ret.get(0).getHandle();
		}
        return "";
    }

    //根据生产区域获取拉线，返回数组类型
    public Set<String> getLineByWorkArea(String[] workArea) {
        List<String> workAreaList = new ArrayList<String>();
        Collections.addAll(workAreaList, workArea);
        WorkCenterMemberPOExample example = new WorkCenterMemberPOExample();
        example.createCriteria().andWorkCenterBoIn(workAreaList);
        List<WorkCenterMemberPO> list = workCenterMemberPOMapper.selectByExample(example);
        Set<String> set = new HashSet<String>();
        for (int i = 0; i < list.size(); i++) {
            String lineArea = list.get(i).getWorkCenterGbo().split(",")[1];
            set.add(lineArea);
        }
        return set;
    }

    public Set<String> getLineByResourceType(String[] resourceType) {
        List<String> resourceTypeResourceList = new ArrayList<String>();
        Collections.addAll(resourceTypeResourceList,resourceType);
        ResourceTypeResourcePOExample example = new ResourceTypeResourcePOExample();
        example.createCriteria().andResourceTypeBoIn(resourceTypeResourceList);
        List<ResourceTypeResourcePO> list = resourceTypeResourcePOMapper.selectByExample(example);
        Set<String> set = new HashSet<String>();
        for (ResourceTypeResourcePO po : list) {
            ResourcePO resourcePO = resourcePOMapper.selectByPrimaryKey(po.getResourceBo());
            if(!Utils.isEmpty(resourcePO.getLineArea())){
                set.add(resourcePO.getLineArea());
            }
        }
        return set;
    }

    public Set<String> getLineByResrce(String resrce) {
        List<String> resourceList = new ArrayList<String>();
        Collections.addAll(resourceList, resrce.split(","));
        ResourcePOExample example = new ResourcePOExample();
        example.createCriteria().andResrceIn(resourceList);
        List<ResourcePO> list = resourcePOMapper.selectByExample(example);
        Set<String> set = new HashSet<String>();
        for (int i = 0; i < list.size(); i++) {
            String lineArea = list.get(i).getLineArea();
            if(!Utils.isEmpty(lineArea)){
                set.add(lineArea);
            }
        }
        return set;
    }

}
