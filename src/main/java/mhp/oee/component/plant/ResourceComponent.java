package mhp.oee.component.plant;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.ResourceAndResourceTypeAndResrcePOMapper;
import mhp.oee.dao.extend.ResrceAndWorkCenterShiftAndWorkCenterPOMapper;
import mhp.oee.dao.gen.ResourcePOMapper;
import mhp.oee.po.extend.ResourceAndWorkCenterShiftAndWorkCenterPO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourcePOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceConditionVO;
import mhp.oee.vo.ResourceVO;

import org.apache.poi.util.Units;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ResourceComponent {

    @Autowired
    private ResourcePOMapper resourcePOMapper;

    @Autowired
    private ResrceAndWorkCenterShiftAndWorkCenterPOMapper resrceAndWorkCenterShiftAndWorkCenterPOMapper;
    @Autowired
    private ResourceAndResourceTypeAndResrcePOMapper resourceAndResourceTypeAndResrcePOMapper;

    public ResourcePO getResourceByHandle(String handle) {
        return this.resourcePOMapper.selectByPrimaryKey(handle);
    }

    public List<ResourcePO> readResourcesByExample(ResourcePOExample example) {
        return resourcePOMapper.selectByExample(example);
    }

    public ResourcePO getResourceByKey(String site, String resource) {
        String handle = (new ResrceBOHandle(site, resource)).getValue();
        return resourcePOMapper.selectByPrimaryKey(handle);
    }

    public List<ResourceConditionVO> getResourceByCondition(String site, String resrce, String lineArea, String workArea, String resourceType) {
        List<ResourcePO> pos = resourceAndResourceTypeAndResrcePOMapper.select(site, resourceType, resrce, lineArea, workArea);
        return Utils.copyListProperties(pos, ResourceConditionVO.class);
    }

    public boolean isEnabledResource(String handle) {
        ResourcePOExample example = new ResourcePOExample();
        example.createCriteria().andHandleEqualTo(handle).andEnabledEqualTo("true");
        List<ResourcePO> ret = this.resourcePOMapper.selectByExample(example);
        return Utils.isEmpty(ret) ? false : true;
    }

    public boolean existResource(String handle) {
        ResourcePO po = resourcePOMapper.selectByPrimaryKey(handle);
        return (po != null) ? true : false;
    }

    public void createResource(ResourceVO vo) throws BusinessException {
        ResourcePO po = Utils.copyObjectProperties(vo, ResourcePO.class);
        po.setHandle(new ResrceBOHandle(vo.getSite(), vo.getResrce()).getValue());
        if (po.getConfiguredPlcInfo() == null) {
            po.setConfiguredPlcInfo("false");   // default value
        }
        po.setModifiedUser(ContextHelper.getContext().getUser());
        if(!Utils.isEmpty(po.getField())){
            po.setDescription(po.getDescription()+","+po.getField());
        }
        int ret = resourcePOMapper.insert(po);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "createResource");
        }
    }

    public void updateResource(ResourceVO vo) throws BusinessException {
        ResourcePO po = Utils.copyObjectProperties(vo, ResourcePO.class);
        po.setHandle(new ResrceBOHandle(vo.getSite(), vo.getResrce()).getValue());
        if (po.getConfiguredPlcInfo() == null) {
            po.setConfiguredPlcInfo("false");   // default value
        }
        po.setModifiedUser(ContextHelper.getContext().getUser());
        if(!Utils.isEmpty(po.getField())){
            po.setDescription(po.getDescription()+","+po.getField());
        }
        int ret = resourcePOMapper.updateByPrimaryKey(po);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "updateResource");
        }
    }
    
    public List<ResourcePO> readDistinctResource() throws BusinessException{
        ResourcePOExample example = new ResourcePOExample();
//        example.createCriteria().andResrceEqualTo("APAC0009");
        example.setDistinct(true);
        List<ResourcePO> ret = resourcePOMapper.selectByExample(example);
        if(Utils.isEmpty(ret)){
            throw new BusinessException(ErrorCodeEnum.INVALID_RESOURCE,"no resource find");
        }
        return ret;
    }

    public List<ResourceAndWorkCenterShiftAndWorkCenterPO> readResrceAndWorkCenterAndWorkCenterShift(String site, String resrce) {
        return resrceAndWorkCenterShiftAndWorkCenterPOMapper.selectResrceAndWorkCenterAndWorkCenterShift(site, resrce);
//        return Utils.copyListProperties(pos, ResourceAndWorkCenterShiftAndWorkCenterPO.class);
    }

}
