package mhp.oee.component.plant;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.DeleteErrorException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.InvalidReasonCodeException;
import mhp.oee.common.exception.NotFoundExistingRecordException;
import mhp.oee.common.exception.UsedException;
import mhp.oee.common.handle.ReasonCodeAlertBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.ReasonCodeAlertAndResourceTypeAndReasonCodePOMpper;
import mhp.oee.dao.gen.ReasonCodeAlertPOMapper;
import mhp.oee.po.extend.ReasonCodeAlertAndResourceTypeAndReasonCodePO;
import mhp.oee.po.gen.ReasonCodeAlertPO;
import mhp.oee.po.gen.ReasonCodeAlertPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.PlcObjectVO;
import mhp.oee.vo.ReasonCodeAlertVO;

@Component
public class ReasonCodeAlertComponent {

	public static final String DEFAULT_ITEM_REVISION = "#";

	private static final String REASONCODE_ALERT = "Reason Code Alert Table";

	private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

	@InjectableLogger
	private static Logger logger;

	@Autowired
	private ReasonCodeAlertPOMapper reasonCodeAlertPOMapper;

	@Autowired
	private ReasonCodeAlertAndResourceTypeAndReasonCodePOMpper reasonCodeAlertAndResourceTypeAndReasonCodePOMpper;

	public List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> readReasonCodeAlertCurrent(String site,
			String[] resrceTypes) {
		return reasonCodeAlertAndResourceTypeAndReasonCodePOMpper.selectReasonCodeAlertCurent(resrceTypes, new Date(),
				site);
	}

	public List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> readReasonCodeAlertCurrentPage(String site,
			String[] resrceTypes, int limit, int offset) {
		return reasonCodeAlertAndResourceTypeAndReasonCodePOMpper.selectReasonCodeAlertCurentPage(resrceTypes, site,
				new Date(), limit, offset);
	}

	public int readReasonCodeAlertCurrentPageCount(String site, String[] resrceTypes) {
		return reasonCodeAlertAndResourceTypeAndReasonCodePOMpper.selectReasonCodeAlertCurentPageCount(resrceTypes,
				new Date(), site);
	}

	public List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> readReasonCodeAlertHisChange(String site,
			String[] resrceTypes, Date time, int limit, int offset) {
		return reasonCodeAlertAndResourceTypeAndReasonCodePOMpper.selectReasonCodeAlertHisChange(resrceTypes, site,
				time, limit, offset);
	}

	public int readReasonCodeAlertHisChangeCount(String site, String[] resrceTypes, Date time) {
		return reasonCodeAlertAndResourceTypeAndReasonCodePOMpper.selectReasonCodeAlertHisChangeCount(resrceTypes, site,
				time);
	}

	public List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> readReasonCodeAlertHisStatus(String site,
			String[] resrceTypes, Date time, int limit, int offset) {
		return reasonCodeAlertAndResourceTypeAndReasonCodePOMpper.selectReasonCodeAlertHisStatus(resrceTypes, site,
				time, limit, offset);
	}

	public int readReasonCodeAlertHisStatusCount(String site, String[] resrceTypes, Date time) {
		return reasonCodeAlertAndResourceTypeAndReasonCodePOMpper.selectReasonCodeAlertHisStatusCount(resrceTypes, site,
				time);
	}

	public List<ReasonCodeAlertPO> getReasonCodeAlertByDevice(String resourceType) {
		ReasonCodeAlertPOExample example = new ReasonCodeAlertPOExample();
		example.createCriteria().andResourceTypeBoEqualTo(resourceType);
		return reasonCodeAlertPOMapper.selectByExample(example);
	}

	public int countReasonCodeAlertByResourceType(String resourceType) {
		ReasonCodeAlertPOExample example = new ReasonCodeAlertPOExample();
		example.createCriteria().andResourceTypeBoEqualTo(resourceType)
				.andEndDateTimeEqualTo(Utils.getInfiniteDateWithTime());
		return reasonCodeAlertPOMapper.countByExample(example);
	}

	public List<ReasonCodeAlertPO> getReasonCodeAlertByResourceType(String resourceType) {
		ReasonCodeAlertPOExample example = new ReasonCodeAlertPOExample();
		example.createCriteria().andResourceTypeBoEqualTo(resourceType)
				.andEndDateTimeEqualTo(Utils.getInfiniteDateWithTime());
		return reasonCodeAlertPOMapper.selectByExample(example);
	}

	private void insertReasonCodeAlert(ReasonCodeAlertVO vo) throws ExistingRecordException {
		// create new one
		String handle = (new ReasonCodeAlertBOHandle(vo.getResourceTypeBo(), vo.getAlertSequenceId(),
				vo.getReasonCodeBo(), vo.getStrt())).getValue();
		ReasonCodeAlertPO po = Utils.copyObjectProperties(vo, ReasonCodeAlertPO.class);
		po.setHandle(handle); // this is create action, view does not provide
								// HANDLE
		po.setModifiedUser(ContextHelper.getContext().getUser());

		if (searchReasonCodeAlertByHandle(handle) == null) {
			int ret = reasonCodeAlertPOMapper.insert(po);
			if (ret != 1) {
				logger.error(REASONCODE_ALERT + " : Existing record: " + gson.toJson(vo));
				throw new ExistingRecordException(REASONCODE_ALERT, gson.toJson(vo));
			}
		} else {
			logger.error(REASONCODE_ALERT + " : Existing record: " + gson.toJson(vo));
			throw new ExistingRecordException(REASONCODE_ALERT, gson.toJson(vo));
		}
	}

	private ReasonCodeAlertPO searchReasonCodeAlertByHandle(String handle) {
		return reasonCodeAlertPOMapper.selectByPrimaryKey(handle);
	}

	private void updateExistingReasonCodeAlertRecord(ReasonCodeAlertPO po) throws NotFoundExistingRecordException {
		po.setModifiedUser(ContextHelper.getContext().getUser());
		ReasonCodeAlertPOExample example = new ReasonCodeAlertPOExample();
		example.createCriteria().andHandleEqualTo(po.getHandle()).andModifiedDateTimeEqualTo(po.getModifiedDateTime());
		int ret = reasonCodeAlertPOMapper.updateByExample(po, example);
		if (ret != 1) {
			throw new NotFoundExistingRecordException();
		}

	}

	public List<ReasonCodeAlertPO> getExistingReasonCodeAlert(ReasonCodeAlertVO vo) {
		ReasonCodeAlertPOExample example = new ReasonCodeAlertPOExample();
		example.createCriteria().andResourceTypeBoEqualTo(vo.getResourceTypeBo())
				.andEndDateTimeEqualTo(vo.getEndDateTime());
		List<ReasonCodeAlertPO> list = reasonCodeAlertPOMapper.selectByExample(example);
		if (list.size() != 0) {
			return list;
		} else {
			return null;
		}
	}

	private List<ReasonCodeAlertPO> getExistingReasonCodeAlertByResourceType(String resourceType) {
		ReasonCodeAlertPOExample example = new ReasonCodeAlertPOExample();
		example.createCriteria().andResourceTypeBoEqualTo(resourceType)
				.andEndDateTimeEqualTo(Utils.getInfiniteDateWithTime());
		List<ReasonCodeAlertPO> list = reasonCodeAlertPOMapper.selectByExample(example);
		if (list.size() != 0) {
			return list;
		} else {
			return null;
		}
	}

	public void updateReasonCodeAlertByStartTime(Set<String> resourceTypeSet, Date dateFromWeb)
			throws InvalidReasonCodeException, NotFoundExistingRecordException {
		for (String resourceType : resourceTypeSet) {
			List<ReasonCodeAlertPO> list = this.getExistingReasonCodeAlertByResourceType(resourceType);
			if (list != null && list.size() != 0) {
				for (ReasonCodeAlertPO existingReasonCodeAlertRecord : list) {
					if (dateFromWeb.before(existingReasonCodeAlertRecord.getStartDateTime())) {
						throw new InvalidReasonCodeException();
					} else {
						existingReasonCodeAlertRecord.setEndDateTime(dateFromWeb);
						updateExistingReasonCodeAlertRecord(existingReasonCodeAlertRecord);
					}
				}
			}
		}
	}

	public void createReasonCodeAlertListByVO(List<ReasonCodeAlertVO> list) throws ExistingRecordException {
		for (ReasonCodeAlertVO vo : list) {
			insertReasonCodeAlert(vo);
		}
	}

	public boolean isReasonCodeAlertUsed(String handle) {
		ReasonCodeAlertPOExample example = new ReasonCodeAlertPOExample();
		example.createCriteria().andHandleEqualTo(handle).andUsedEqualTo("true");
		List<ReasonCodeAlertPO> pos = reasonCodeAlertPOMapper.selectByExample(example);
		return (pos == null || (pos != null && pos.size() <= 0)) ? false : true;
	}

	public boolean isReasonCodeMatchAlert(String reasonCodeBO, String alertDescription) {
		ReasonCodeAlertPOExample example = new ReasonCodeAlertPOExample();
		example.createCriteria().andReasonCodeBoEqualTo(reasonCodeBO).andDescriptionEqualTo(alertDescription);
		List<ReasonCodeAlertPO> ret = reasonCodeAlertPOMapper.selectByExample(example);
		if (ret.size() != 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isReasonCodeListMatchAlert(List<String> reasonCodeBOList, String alertDescription) {
		ReasonCodeAlertPOExample example = new ReasonCodeAlertPOExample();
		example.createCriteria().andReasonCodeBoIn(reasonCodeBOList).andDescriptionEqualTo(alertDescription);
		List<ReasonCodeAlertPO> ret = reasonCodeAlertPOMapper.selectByExample(example);
		if (ret.size() != 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isReasonCodeExisting(String reasonCodeBO) {
		ReasonCodeAlertPOExample example = new ReasonCodeAlertPOExample();
		example.createCriteria().andReasonCodeBoEqualTo(reasonCodeBO);
		List<ReasonCodeAlertPO> ret = reasonCodeAlertPOMapper.selectByExample(example);
		if (ret.size() != 0) {
			return true;
		} else {
			return false;
		}
	}

	public int deleteReasonCodeAlertUsed(ReasonCodeAlertAndResourceTypeAndReasonCodePO vo) throws BusinessException {
		if (isReasonCodeAlertUsed(vo.getHandle())) {
			logger.error("ReasonCodeAlertComponent.deleteReasonCodeAlertUsed() : deleteReasonCodeAlertUsed");
			throw new UsedException();
		}
		return reasonCodeAlertPOMapper.deleteByPrimaryKey(vo.getHandle());
	}
}