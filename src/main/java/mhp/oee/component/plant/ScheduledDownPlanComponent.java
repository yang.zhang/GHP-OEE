package mhp.oee.component.plant;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mhp.oee.dao.extend.ScheduledDownPlanAndReasonCodePOMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ScheduledDownPlanBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.ResourceTypeResourceAndResourceAndResourceTypePOMapper;
import mhp.oee.dao.extend.ScheduledDownPlanAndResrceAndWorkCenterPOMapper;
import mhp.oee.dao.gen.ReasonCodePOMapper;
import mhp.oee.dao.gen.ScheduledDownPlanPOMapper;
import mhp.oee.po.extend.ResourceTypeResourceAndResourceAndResourceTypePO;
import mhp.oee.po.extend.ScheduledDownPlanAndResrceAndWorkCenterPO;
import mhp.oee.po.gen.ReasonCodePO;
import mhp.oee.po.gen.ReasonCodePOExample;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ScheduledDownPlanPO;
import mhp.oee.po.gen.ScheduledDownPlanPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ReasonCodeVO;
import mhp.oee.vo.ResourceConditionVO;
import mhp.oee.vo.ScheduledDownPlanAndResrceAndWorkCenterVO;

@Component
public class ScheduledDownPlanComponent {

    @InjectableLogger
    private static Logger logger;
    private static final String SCHEDULED_DOWN_PLAN = "SCHEDULED_DOWN_PLAN";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ReasonCodePOMapper reasonCodePOMapper;

    @Autowired
    ScheduledDownPlanPOMapper scheduledDownPlanPOMapper;

    @Autowired
    ScheduledDownPlanAndResrceAndWorkCenterPOMapper scheduledDownPlanAndResrceAndWorkCenterPOMapper;

    @Autowired
    ResourceTypeResourceAndResourceAndResourceTypePOMapper resourceTypeResourceAndResourceAndResourceTypePOMapper;

    @Autowired
    ScheduledDownPlanAndReasonCodePOMapper scheduledDownPlanAndReasonCodePOMapper;

    public List<ReasonCodeVO> readAllReasonCode(String site, String enabled) {
        ReasonCodePOExample poExmaple = new ReasonCodePOExample();
        poExmaple.createCriteria().andSiteEqualTo(site).andEnabledEqualTo(enabled).andReasonCodeGroupEqualTo("SD00");
        poExmaple.setOrderByClause("REASON_CODE");
        List<ReasonCodePO> pos = reasonCodePOMapper.selectByExample(poExmaple);
        List<ReasonCodeVO> vos = Utils.copyListProperties(pos, ReasonCodeVO.class);
        return vos;
    }

    public List<ScheduledDownPlanAndResrceAndWorkCenterPO> readScheduledDownPlanByResrce(String site, List<ResourceConditionVO> resrces, Date dateTime, int limit, int offset) {
        List<ScheduledDownPlanAndResrceAndWorkCenterPO> vos = scheduledDownPlanAndResrceAndWorkCenterPOMapper.selectScheduledDownPlanAndResrceAndWorkCenter(site, resrces, dateTime, limit, offset);
        return vos;
    }

    public int readScheduledDownPlanByResrceCount(String site, List<ResourceConditionVO> resrces, Date dateTime) {
        int vos= scheduledDownPlanAndResrceAndWorkCenterPOMapper.selectScheduledDownPlanAndResrceAndWorkCenterCount(site, resrces, dateTime);
        return vos;
    }

    public List<ResourceConditionVO> readResourceByCondition(String site, String resourceTypeBO, String line,
            String workCenter) {
        List<ResourcePO> resourcePOs = new ArrayList<ResourcePO>();
        List<ResourceTypeResourceAndResourceAndResourceTypePO> resourceTypeResourceAndResourceAndResourceTypePOs = resourceTypeResourceAndResourceAndResourceTypePOMapper
                .selectResourceTypeResourceAndResource(site, resourceTypeBO, line, workCenter);
        if (resourceTypeResourceAndResourceAndResourceTypePOs != null) {
            for (ResourceTypeResourceAndResourceAndResourceTypePO resourceTypeResourceAndResourceAndResourceTypePO : resourceTypeResourceAndResourceAndResourceTypePOs) {
                resourcePOs.add(resourceTypeResourceAndResourceAndResourceTypePO.getResourcePO());
            }
        }
        return Utils.copyListProperties(resourcePOs, ResourceConditionVO.class);
    }

    private Date getDate(String date) {
        try {
            return Utils.getDateTimefromString(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("时间格式错误：yyyy-MM-dd HH:mm:ss");
    }
    
	public List<ScheduledDownPlanPO> readRecordsWithinTimeRange(String reasonCodeBO, String resrceBO, Date beginTime, Date finishTime) {
//		ScheduledDownPlanPOExample example = new ScheduledDownPlanPOExample();
//		example.createCriteria()
//                .andResourceBoEqualTo(resrceBO)
//                .andStartDateTimeLessThanOrEqualTo(finishTime)
//                .andEndDateTimeGreaterThan(beginTime);
//		List<ScheduledDownPlanPO> ret = scheduledDownPlanPOMapper.selectByExample(example);

        List<ScheduledDownPlanPO> ret = scheduledDownPlanAndReasonCodePOMapper.select(resrceBO, finishTime, beginTime, reasonCodeBO);

		if (Utils.isEmpty(ret)) {
			return null;
		} else {
			return ret;
		}
	}

    public List<ScheduledDownPlanPO>  existRepeatTime(String site, String resourceBO, String handle) {
        ScheduledDownPlanPOExample example = new ScheduledDownPlanPOExample();
        if (Utils.isEmpty(handle)) {
            example.createCriteria().andSiteEqualTo(site).andResourceBoEqualTo(resourceBO);
        } else {
            example.createCriteria().andSiteEqualTo(site).andResourceBoEqualTo(resourceBO).andHandleNotEqualTo(handle);
        }
        List<ScheduledDownPlanPO> pos = scheduledDownPlanPOMapper.selectByExample(example);
        return pos;
    }

    public String createScheduledDownPlanByResrce(ScheduledDownPlanAndResrceAndWorkCenterVO vo) throws BusinessException {
        ScheduledDownPlanBOHandle boHandle = new ScheduledDownPlanBOHandle(vo.getSite(), vo.getResourceBo(), vo.getStrt());
        ScheduledDownPlanPO po = Utils.copyObjectProperties(vo, ScheduledDownPlanPO.class);
        po.setHandle(boHandle.getValue());
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = scheduledDownPlanPOMapper.insert(po);
        if (ret != 1) {
            logger.error("ScheduledDownPlanComponent.createScheduledDownPlanByResrce() : Existing SCHEDULED_DOWN_PLAN record");
            throw new ExistingRecordException(SCHEDULED_DOWN_PLAN, gson.toJson(vo));
        }
        return boHandle.getValue();
    }

    public void updateScheduledDownPlanByResrce(ScheduledDownPlanAndResrceAndWorkCenterVO vo) throws BusinessException {
        ScheduledDownPlanPO po = Utils.copyObjectProperties(vo, ScheduledDownPlanPO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        ScheduledDownPlanPOExample example = new ScheduledDownPlanPOExample();
        example.createCriteria().andHandleEqualTo(vo.getHandle()).andModifiedDateTimeEqualTo(vo.getModifiedDateTime());
        int ret = scheduledDownPlanPOMapper.updateByExample(po, example);
        if (ret != 1) {
            logger.error("ScheduledDownPlanComponent.updateScheduledDownPlanByResrce() : updateScheduledDownPlanByResrce");
            throw new UpdateErrorException(SCHEDULED_DOWN_PLAN, gson.toJson(vo));
        }
    }

    public int deleteScheduledDownPlanByResrce(ScheduledDownPlanAndResrceAndWorkCenterVO vo) throws BusinessException {
        return scheduledDownPlanPOMapper.deleteByPrimaryKey(vo.getHandle());
    }

}
