package mhp.oee.component.plant;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.DebuggingClassWriter;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.InvalidTimeException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ReasonCodeResStateBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStatePOMapper;
import mhp.oee.dao.extend.ReasonCodeAndReasonCodeGroupPOMapper;
import mhp.oee.dao.gen.ReasonCodeResStatePOMapper;
import mhp.oee.dao.gen.ResourceStatePOMapper;
import mhp.oee.po.extend.ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStatePO;
import mhp.oee.po.extend.ReasonCodeAndReasonCodeGroupPO;
import mhp.oee.po.gen.ReasonCodeResStatePO;
import mhp.oee.po.gen.ReasonCodeResStatePOExample;
import mhp.oee.po.gen.ResourceStatePO;
import mhp.oee.po.gen.ResourceStatePOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStateVO;
import mhp.oee.vo.ReasonCodeResStateVO;

@Component
public class ReasonCodeResStateComponent {
    @InjectableLogger
    private static Logger logger;
    private static final String REASON_CODE_RES_STATE = "REASON_CODE_RES_STATE";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    private ResourceStatePOMapper resourceStatePOMapper;

    @Autowired
    private ReasonCodeAndReasonCodeGroupPOMapper reasonCodeAndReasonCodeGroupPOMapper;

    @Autowired
    private ReasonCodeResStatePOMapper reasonCodeResStatePOMapper;

    @Autowired
    private ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStatePOMapper reasonCodeResStateAndResStatePOMapper;

    public List<ResourceStatePO> readResourceState(String site, String enabled) {
        ResourceStatePOExample example = new ResourceStatePOExample();
        if (site != null) {
            example.createCriteria().andSiteEqualTo(site).andEnabledEqualTo(enabled);
        }
        return resourceStatePOMapper.selectByExample(example);
    }
    
//	public boolean isReasonCodeBoMatchResourceState(String resourceStateBO, String reasonCodeBO) {
//		ReasonCodeResStatePOExample example = new ReasonCodeResStatePOExample();
//		example.createCriteria().andResourceStateBoEqualTo(resourceStateBO).andReasonCodeBoEqualTo(reasonCodeBO);
//		List<ReasonCodeResStatePO> ret = reasonCodeResStatePOMapper.selectByExample(example);
//		if (Utils.isEmpty(ret)) {
//			return false;
//		} else {
//			return true;
//		}
//	}

    public List<ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStateVO> readReasonCodeResState(String site, String reasonCodeGroup, String reasonCode, Date dateTime, int limit, int offset) {
        List<ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStatePO> pos = reasonCodeResStateAndResStatePOMapper.selectViewReasonCodeAndGroupAndResStates(site, reasonCodeGroup, reasonCode, dateTime, limit, offset);
        return Utils.copyListProperties(pos, ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStateVO.class);
    }

    public int readReasonCodeResStateCount(String site, String reasonCodeGroup, String reasonCode, Date dateTime) {
        return reasonCodeResStateAndResStatePOMapper.selectViewReasonCodeAndGroupAndResStatesCount(site, reasonCodeGroup, reasonCode, dateTime);
    }

    public List<ReasonCodeAndReasonCodeGroupPO> readResourceStateAddUI(String site, String reasonCodeGroup, String reasonCode, String enabled) {
        return reasonCodeAndReasonCodeGroupPOMapper.selectReasonCodeGroup(site, reasonCodeGroup, reasonCode, enabled);
    }

    public List<ReasonCodeResStatePO> existResonCodeResourceState(String reasonCodeBO, String resourceStateBO, Date startDateTime) {
        ReasonCodeResStatePOExample example = new ReasonCodeResStatePOExample();
        if (!Utils.isEmpty(resourceStateBO)) {
            example.createCriteria().andReasonCodeBoEqualTo(reasonCodeBO).andEndDateTimeEqualTo(startDateTime).andResourceStateBoEqualTo(resourceStateBO);
        } else {
            example.createCriteria().andReasonCodeBoEqualTo(reasonCodeBO).andEndDateTimeEqualTo(startDateTime);
        }
        return reasonCodeResStatePOMapper.selectByExample(example);
    }

    public int deleteResonCodeResourceState(ReasonCodeResStateVO vo) throws BusinessException {
        return reasonCodeResStatePOMapper.deleteByPrimaryKey(vo.getHandle());
    }

    public void updateResonCodeResourceState(ReasonCodeResStateVO vo) throws BusinessException, ParseException {
        List<ReasonCodeResStatePO> pos = existResonCodeResourceState(vo.getReasonCodeBo(), null, vo.getEndDateTime());
        if (pos != null && pos.size() > 0) {
            ReasonCodeResStatePO po = pos.get(0);
            if (Utils.getDatefromString(vo.getStartDateTimeIn()).compareTo(po.getStartDateTime()) <= 0) {
                logger.error("ReasonCodeResStateComponent.updateResonCodeResourceState() : time error");
                throw new InvalidTimeException(gson.toJson(vo));
            }
            po.setModifiedUser(ContextHelper.getContext().getUser());
            po.setEndDateTime(vo.getStartDateTime());
            ReasonCodeResStatePOExample example = new ReasonCodeResStatePOExample();
            example.createCriteria().andHandleEqualTo(po.getHandle()).andModifiedDateTimeEqualTo(po.getModifiedDateTime());
            int ret = reasonCodeResStatePOMapper.updateByExample(po, example);
            if (ret != 1) {
                logger.error("ReasonCodeResStateComponent.updateResonCodeResourceState() : updateResonCodeResourceState");
                throw new UpdateErrorException(REASON_CODE_RES_STATE, gson.toJson(vo));
            }
        }
    }

    public String createResonCodeResourceState(ReasonCodeResStateVO vo) throws BusinessException {
        List<ReasonCodeResStatePO> pos = existResonCodeResourceState(vo.getReasonCodeBo(), vo.getResourceStateBo(), vo.getEndDateTime());
        if (pos != null && pos.size() > 0) {
            throw new ExistingRecordException(REASON_CODE_RES_STATE, gson.toJson(vo));
        }
        ReasonCodeResStateBOHandle reasonCodeResStateBOHandle = new ReasonCodeResStateBOHandle(vo.getReasonCodeBo(), vo.getResourceStateBo(), vo.getStrt());
        ReasonCodeResStatePO po = Utils.copyObjectProperties(vo, ReasonCodeResStatePO.class);
        po.setHandle(reasonCodeResStateBOHandle.getValue()); // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = reasonCodeResStatePOMapper.insert(po);
        if (ret != 1) {
            logger.error("ReasonCodeResStateComponent.createResonCodeResourceState() : Existing REASON_CODE_RES_STATE record");
            throw new ExistingRecordException(REASON_CODE_RES_STATE, gson.toJson(vo));
            
        }
        return reasonCodeResStateBOHandle.getValue();
    }
}
