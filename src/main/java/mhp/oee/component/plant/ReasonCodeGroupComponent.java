package mhp.oee.component.plant;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ReasonCodeGroupBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.ReasonCodeGroupPOMapper;
import mhp.oee.po.gen.ReasonCodeGroupPO;
import mhp.oee.po.gen.ReasonCodeGroupPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ReasonCodeGroupVO;

@Component
public class ReasonCodeGroupComponent {
    @InjectableLogger
    private static Logger logger;
    private static String REASON_CODE_GROUP = "REASON_CODE_GROUP";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    private ReasonCodeGroupPOMapper reasonCodeGroupPOMapper;

    public List<ReasonCodeGroupVO> readAllResonCodeGroup(String site, String enabled) {
        ReasonCodeGroupPOExample example = new ReasonCodeGroupPOExample();
        if (!Utils.isEmpty(enabled)) {
            example.createCriteria().andSiteEqualTo(site).andEnabledEqualTo(enabled);
        } else {
            example.createCriteria().andSiteEqualTo(site);
        }
        example.setOrderByClause("HANDLE");
        List<ReasonCodeGroupPO> pos = reasonCodeGroupPOMapper.selectByExample(example);
        List<ReasonCodeGroupVO> vos = Utils.copyListProperties(pos, ReasonCodeGroupVO.class);
        return vos;
    }

    public ReasonCodeGroupPO existResonCodeGroup(String site, String resonCodeGroup) {
        String handle = new ReasonCodeGroupBOHandle(site, resonCodeGroup).getValue();
        ReasonCodeGroupPO po = reasonCodeGroupPOMapper.selectByPrimaryKey(handle);
        return po;
    }

    public String createReasonCodeGroup(ReasonCodeGroupVO reasonCodeGroupVO) throws BusinessException {
        String handle = new ReasonCodeGroupBOHandle(reasonCodeGroupVO.getSite(), reasonCodeGroupVO.getReasonCodeGroup()).getValue();
        if (existResonCodeGroup(reasonCodeGroupVO.getSite(), reasonCodeGroupVO.getReasonCodeGroup()) != null) {
            logger.error("ReasonCodeGroupComponent.createReasonCodeGroup() : Existing REASON_CODE_GROUP record");
            throw new ExistingRecordException(REASON_CODE_GROUP, gson.toJson(reasonCodeGroupVO));
        }
        ReasonCodeGroupPO po = Utils.copyObjectProperties(reasonCodeGroupVO, ReasonCodeGroupPO.class);
        po.setHandle(handle); // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = reasonCodeGroupPOMapper.insert(po);
        if (ret != 1) {
            logger.error("ReasonCodeGroupComponent.createReasonCodeGroup() : Existing REASON_CODE_GROUP record");
            throw new ExistingRecordException(REASON_CODE_GROUP, gson.toJson(reasonCodeGroupVO));
        }
        return handle;
    }

    public void updateReasonCodeGroup(ReasonCodeGroupVO reasonCodeGroupVO) throws BusinessException {
        ReasonCodeGroupPO po = Utils.copyObjectProperties(reasonCodeGroupVO, ReasonCodeGroupPO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        ReasonCodeGroupPOExample example = new ReasonCodeGroupPOExample();
        example.createCriteria().andHandleEqualTo(reasonCodeGroupVO.getHandle()).andModifiedDateTimeEqualTo(reasonCodeGroupVO.getModifiedDateTime());
        int ret = reasonCodeGroupPOMapper.updateByExample(po, example);
        if (ret != 1) {
            logger.error("ReasonCodeGroupComponent.updateReasonCodeGroup() : updateReasonCodeGroup");
            throw new UpdateErrorException(REASON_CODE_GROUP, gson.toJson(po));
        }
    }

}
