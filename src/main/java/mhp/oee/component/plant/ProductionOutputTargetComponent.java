package mhp.oee.component.plant;

import java.util.ArrayList;
import java.util.List;

import mhp.oee.vo.WorkCenterShiftConditionVO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ProductionOutputTargetBOHandle;
import mhp.oee.common.handle.WorkCenterBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.extend.ProductionOutputTargetAndResrcePOMapper;
import mhp.oee.dao.extend.ResourceTypeResourceAndResourceAndResourceTypePOMapper;
import mhp.oee.dao.extend.ResrceAndWorkCenterAndWorkCenterShiftPOMapper;
import mhp.oee.dao.gen.ItemPOMapper;
import mhp.oee.dao.gen.ProductionOutputTargetPOMapper;
import mhp.oee.dao.gen.ResourcePOMapper;
import mhp.oee.dao.gen.WorkCenterShiftPOMapper;
import mhp.oee.po.extend.ProductionOutputTargetAndResrcePO;
import mhp.oee.po.extend.ResourceTypeResourceAndResourceAndResourceTypePO;
import mhp.oee.po.extend.ResrceAndWorkCenterAndWorkCenterShiftPO;
import mhp.oee.po.gen.ItemPO;
import mhp.oee.po.gen.ItemPOExample;
import mhp.oee.po.gen.ProductionOutputTargetPO;
import mhp.oee.po.gen.ProductionOutputTargetPOExample;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.WorkCenterShiftPO;
import mhp.oee.po.gen.WorkCenterShiftPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ItemVO;
import mhp.oee.vo.ProductionOutputTargetAndResrceVO;
import mhp.oee.vo.ResourceConditionVO;

@Component
public class ProductionOutputTargetComponent {
    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static final String PRODUCTION_OUTPUT_TARGET = "PRODUCTION_OUTPUT_TARGET";
    private static String IS_DEFAULT_VALUE_Y = "true";

    @Autowired
    private ItemPOMapper itemPOMapper;

    @Autowired
    private WorkCenterShiftPOMapper workCenterShiftPOMapper;

    @Autowired
    private ResourcePOMapper resourcePOMapper;

    @Autowired
    private ProductionOutputTargetPOMapper productionOutputTargetPOMapper;

    @Autowired
    private ResrceAndWorkCenterAndWorkCenterShiftPOMapper resrceAndWorkCenterAndWorkCenterShiftPOMapper;

    @Autowired
    private ProductionOutputTargetAndResrcePOMapper productionOutputTargetAndResrcePOMapper;

    @Autowired
    private ResourceTypeResourceAndResourceAndResourceTypePOMapper resourceTypeResourceAndResourceAndResourceTypePOMapper;

    public List<ItemVO> readItemByModel(String site, String model, String enabled) {
        ItemPOExample example = new ItemPOExample();
        example.createCriteria().andSiteEqualTo(site).andEnabledEqualTo(enabled).andModelEqualTo(model);
        example.or().andSiteEqualTo(site).andItemLike("%" + model + "%");
        example.setOrderByClause("ITEM");
        List<ItemPO> itemPo = itemPOMapper.selectByExample(example);
        return Utils.copyListProperties(itemPo, ItemVO.class);
    }

    public List<WorkCenterShiftConditionVO> readShiftByResrce(String site, String resrce, String enabled) {
        List<ResrceAndWorkCenterAndWorkCenterShiftPO> vos = resrceAndWorkCenterAndWorkCenterShiftPOMapper
                .selectResrceAndWorkCenterAndWorkCenterShift(site, resrce, enabled);
        return Utils.copyListProperties(vos, WorkCenterShiftConditionVO.class);
    }

    public List<WorkCenterShiftConditionVO> readShift(String site, String workCenter, String enabled) {
        WorkCenterShiftPOExample example = new WorkCenterShiftPOExample();
        if (Utils.isEmpty(workCenter)) {
            example.createCriteria().andEnabledEqualTo(enabled);
        } else {
            WorkCenterBOHandle boHandle = new WorkCenterBOHandle(site, workCenter);
            example.createCriteria().andEnabledEqualTo(enabled).andWorkCenterBoEqualTo(boHandle.getValue());
        }
        List<WorkCenterShiftPO> workCenterShiftPO = workCenterShiftPOMapper.selectByExample(example);
        return Utils.copyListProperties(workCenterShiftPO, WorkCenterShiftConditionVO.class);
    }

    public List<ProductionOutputTargetAndResrceVO> readProductionOutputTargetAndResrce(String site, String item,
            List<ResourceConditionVO> resourceConditionVOs, String shift, String startDateTime, String endDateTime) {
        List<ProductionOutputTargetAndResrcePO> pos = productionOutputTargetAndResrcePOMapper
                .selectProductionOutputTargetAndResrce(site, item, resourceConditionVOs, shift, startDateTime,
                        endDateTime);
        return Utils.copyListProperties(pos, ProductionOutputTargetAndResrceVO.class);
    }

    public List<ResourceConditionVO> readResourceByCondition(String site, String resourceTypeBO, String line,
            String workCenter) {
        List<ResourcePO> resourcePOs = new ArrayList<ResourcePO>();
        List<ResourceTypeResourceAndResourceAndResourceTypePO> resourceTypeResourceAndResourceAndResourceTypePOs = resourceTypeResourceAndResourceAndResourceTypePOMapper
                .selectResourceTypeResourceAndResource(site, resourceTypeBO, line, workCenter);
        if (resourceTypeResourceAndResourceAndResourceTypePOs != null) {
            for (ResourceTypeResourceAndResourceAndResourceTypePO resourceTypeResourceAndResourceAndResourceTypePO : resourceTypeResourceAndResourceAndResourceTypePOs) {
                resourcePOs.add(resourceTypeResourceAndResourceAndResourceTypePO.getResourcePO());
            }
        }
        return Utils.copyListProperties(resourcePOs, ResourceConditionVO.class);
    }

    public ResourcePO readResource(String handle) {
        ResourcePO po = resourcePOMapper.selectByPrimaryKey(handle);
        return po;
    }

    public List<ProductionOutputTargetPO> readProductionOutputTarget(String site, String item, String resrce,
            String shift, String startDateTime, String endDateTime, String isDefaultValue, String targeDate) {
        ProductionOutputTargetPOExample example = new ProductionOutputTargetPOExample();
        if (IS_DEFAULT_VALUE_Y.equals(isDefaultValue)) {
            example.createCriteria().andSiteEqualTo(site).andResrceEqualTo(resrce).andItemEqualTo(item).andShiftEqualTo(shift).andIsDefaultValueEqualTo(IS_DEFAULT_VALUE_Y);
        } else if (!Utils.isEmpty(targeDate)) {
            example.createCriteria().andSiteEqualTo(site).andResrceEqualTo(resrce).andItemEqualTo(item).andShiftEqualTo(shift).andTargetDateEqualTo(targeDate);
        }
        return productionOutputTargetPOMapper.selectByExample(example);
    }

    public String createProductionOutputTarget(ProductionOutputTargetAndResrceVO vo) throws BusinessException {
        List<ProductionOutputTargetPO> existProductionOutputTarget = readProductionOutputTarget(vo.getSite(),
                vo.getItem(), vo.getResrce(), vo.getShift(), null, null, null, vo.getTargetDate());
        if (existProductionOutputTarget != null && existProductionOutputTarget.size() > 0) {
            logger.error("ProductionOutputTargetComponent.createProductionOutputTarget() : Existing PRODUCTION_OUTPUT_TARGET record");
            throw new ExistingRecordException(PRODUCTION_OUTPUT_TARGET, gson.toJson(vo));
        }
        if (Utils.isEmpty(vo.getOperation())) {
            vo.setOperation("*");
        }
        ProductionOutputTargetBOHandle handle = new ProductionOutputTargetBOHandle(vo.getSite(), vo.getItem(),
                vo.getOperation(), vo.getResrce(), vo.getTargetDate(), vo.getShift());
        ProductionOutputTargetPO po = Utils.copyObjectProperties(vo, ProductionOutputTargetPO.class);
        po.setHandle(handle.getValue()); // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = productionOutputTargetPOMapper.insert(po);
        if (ret != 1) {
            logger.error("ProductionOutputTargetComponent.createProductionOutputTarget() : Existing PRODUCTION_OUTPUT_TARGET record");
            throw new ExistingRecordException(PRODUCTION_OUTPUT_TARGET, gson.toJson(vo));
        }
        return handle.getValue();
    }

    public void updateProductionOutputTarget(ProductionOutputTargetAndResrceVO vo) throws BusinessException {
        ProductionOutputTargetPO po = Utils.copyObjectProperties(vo, ProductionOutputTargetPO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = productionOutputTargetPOMapper.updateByPrimaryKey(po);
        if (ret != 1) {
            logger.error("ProductionOutputTargetComponent.updateProductionOutputTarget() : updateProductionOutputTarget");
            throw new UpdateErrorException(PRODUCTION_OUTPUT_TARGET, gson.toJson(vo));
        }
    }

    public int deleteProductionOutputTarget(ProductionOutputTargetAndResrceVO vo) throws BusinessException {
        return productionOutputTargetPOMapper.deleteByPrimaryKey(vo.getHandle());
    }

}
