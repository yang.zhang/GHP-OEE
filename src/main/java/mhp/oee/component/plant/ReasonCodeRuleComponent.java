package mhp.oee.component.plant;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import mhp.oee.common.exception.NoSuchException;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.CreateErrorException;
import mhp.oee.common.exception.DeleteErrorException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.MandatoryException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.exception.UsedException;
import mhp.oee.common.handle.ActivityBOHandle;
import mhp.oee.common.handle.ReasonCodeRuleBOHandle;
import mhp.oee.common.handle.ReasonCodeRuleParamBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.ActivityPOMapper;
import mhp.oee.dao.gen.ActivityParamPOMapper;
import mhp.oee.dao.gen.ReasonCodePriorityPOMapper;
import mhp.oee.dao.gen.ReasonCodeRulePOMapper;
import mhp.oee.dao.gen.ReasonCodeRuleParamPOMapper;
import mhp.oee.po.gen.ActivityPO;
import mhp.oee.po.gen.ReasonCodePriorityPO;
import mhp.oee.po.gen.ReasonCodePriorityPOExample;
import mhp.oee.po.gen.ReasonCodeRulePO;
import mhp.oee.po.gen.ReasonCodeRulePOExample;
import mhp.oee.po.gen.ReasonCodeRuleParamPO;
import mhp.oee.po.gen.ReasonCodeRuleParamPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ActivityParamVO;
import mhp.oee.vo.ReasonCodePriorityVO;
import mhp.oee.web.angular.reasoncode.recognition.ActivityParamComVO;
import mhp.oee.web.angular.reasoncode.recognition.RecognitionVO;

@Component
public class ReasonCodeRuleComponent {

	@InjectableLogger
	private static Logger logger;

	private static final String REASON_CODE_RULE_PARAM = "REASON_CODE_RULE_PARAM";
	private static final String REASON_CODE_PRIORITY = "REASON_CODE_PRIORITY";
	private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	@Autowired
	ActivityPOMapper activityPOMapper;
	@Autowired
	ActivityParamPOMapper activityParamPOMapper;
	@Autowired
	ReasonCodeRulePOMapper reasonCodeRulePOMapper;
	@Autowired
	ReasonCodeRuleParamPOMapper reasonCodeRuleParamPOMapper;
	@Autowired
	ReasonCodePriorityPOMapper reasonCodePriorityPOMapper;

	public void createRecognitionPattern(String reasonCodePriorityBo, String activity,
			List<ActivityParamVO> activityParamVos, String site) throws CreateErrorException {
		String activityBOHandle = new ActivityBOHandle(activity).getValue();
		ActivityPO po = activityPOMapper.selectByPrimaryKey(activityBOHandle);
		if (po != null & "true".equals(po.getEnabled()) && "R".equals(po.getExecutionType())) {
			// create recodeCodeRule
			String reasonCodeRuleHandle = new ReasonCodeRuleBOHandle(reasonCodePriorityBo, "RECOGNITION").getValue();
			if (reasonCodeRulePOMapper.selectByPrimaryKey(reasonCodeRuleHandle) != null) {
				reasonCodeRulePOMapper.deleteByPrimaryKey(reasonCodeRuleHandle);
			}
			ReasonCodeRulePO rcrPo = new ReasonCodeRulePO();
			// rcrPo=reasonCodeRulePOMapper.selectByPrimaryKey(reasonCodeRuleHandle);
			rcrPo.setHandle(reasonCodeRuleHandle);
			rcrPo.setReasonCodePriorityBo(reasonCodePriorityBo);
			rcrPo.setRule("RECOGNITION");
			rcrPo.setActivity(activity);
			rcrPo.setModifiedUser(ContextHelper.getContext().getUser());
			int ret = reasonCodeRulePOMapper.insert(rcrPo);
			if (ret != 1) {
				throw new CreateErrorException(REASON_CODE_RULE_PARAM, gson.toJson(rcrPo));
			}
			//delete reasonCodeRuleParam
			ReasonCodeRuleParamPOExample example = new ReasonCodeRuleParamPOExample();
            example.createCriteria().andReasonCodeRuleBoEqualTo(reasonCodeRuleHandle);
            List<ReasonCodeRuleParamPO> list = reasonCodeRuleParamPOMapper.selectByExample(example);
            if (list.size() > 0) {
                reasonCodeRuleParamPOMapper.deleteByExample(example);
            }
			// create reasonCodeRuleParam
			if (activityParamVos.size() > 0) {
				for (ActivityParamVO vo : activityParamVos) {
					String reasonCodeRuleParamHandle = new ReasonCodeRuleParamBOHandle(site, reasonCodeRuleHandle, "*",
							vo.getParamId()).getValue();
					if (reasonCodeRuleParamPOMapper.selectByPrimaryKey(reasonCodeRuleParamHandle) != null) {
						reasonCodeRuleParamPOMapper.deleteByPrimaryKey(reasonCodeRuleParamHandle);
					}
					if (!Utils.isEmpty(vo.getParamSetValue())) {
						/*
						 * String reasonCodeRuleParamHandle=new
						 * ReasonCodeRuleParamBOHandle(site,
						 * reasonCodeRuleHandle, "*",
						 * vo.getParamId()).getValue();
						 * if(reasonCodeRuleParamPOMapper.selectByPrimaryKey(
						 * reasonCodeRuleParamHandle)!=null){
						 * reasonCodeRuleParamPOMapper.deleteByPrimaryKey(
						 * reasonCodeRuleParamHandle); }
						 */
						ReasonCodeRuleParamPO rcrpPo = new ReasonCodeRuleParamPO();
						rcrpPo.setHandle(reasonCodeRuleParamHandle);
						rcrpPo.setSite(site);
						rcrpPo.setReasonCodeRuleBo(reasonCodeRuleHandle);
						rcrpPo.setResourceType("*");
						rcrpPo.setParamId(vo.getParamId());
						rcrpPo.setParamValue(vo.getParamSetValue());
						rcrpPo.setModifiedUser(ContextHelper.getContext().getUser());
						int reasult = reasonCodeRuleParamPOMapper.insert(rcrpPo);
						if (reasult != 1) {
							throw new CreateErrorException(REASON_CODE_RULE_PARAM, gson.toJson(rcrpPo));
						}
					}
				}
			}
		}
	}

	public void updateRecognitionPattern(ActivityParamComVO activityParamComVos)
			throws UpdateErrorException, MandatoryException {
		String reasonCodePriorityBo = activityParamComVos.getReasonCodePriorityBO();
		String activityCode = activityParamComVos.getActivityCode();
		String activity = activityParamComVos.getActivity();
		String site = activityParamComVos.getSite();
		List<ActivityParamVO> activityParamVos = activityParamComVos.getRuleParams();
		String reasonCodeRuleHandle = new ReasonCodeRuleBOHandle(reasonCodePriorityBo, "RECOGNITION").getValue();
		ReasonCodePriorityPO po = reasonCodePriorityPOMapper.selectByPrimaryKey(reasonCodePriorityBo);
		if (Utils.isEmpty(activity)) {
			throw new MandatoryException();
		}
		if (po != null && "false".equals(po.getUsed())) {
			if ("CHANGE_ACTIVITY".equals(activityCode)) {
				String activityBOHandle = new ActivityBOHandle(activity).getValue();
				ActivityPO activityPo = activityPOMapper.selectByPrimaryKey(activityBOHandle);
				if (activityPo != null & "true".equals(activityPo.getEnabled())
						&& "R".equals(activityPo.getExecutionType())) {
					// update recodeCodeRule
					ReasonCodeRulePO reasonCodeRulePO = reasonCodeRulePOMapper.selectByPrimaryKey(reasonCodeRuleHandle);
					ReasonCodeRulePOExample example = new ReasonCodeRulePOExample();
					if (reasonCodeRulePO != null) {
						reasonCodeRulePO.setActivity(activity);
						reasonCodeRulePO.setModifiedUser(ContextHelper.getContext().getUser());
						example.createCriteria().andHandleEqualTo(reasonCodeRuleHandle);
						int ret = reasonCodeRulePOMapper.updateByExample(reasonCodeRulePO, example);
						if (ret != 1) {
							throw new UpdateErrorException(REASON_CODE_RULE_PARAM, gson.toJson(reasonCodeRulePO));
						}
					} else {
						ReasonCodeRulePO reasonCodeNew = new ReasonCodeRulePO();
						reasonCodeNew.setRule("RECOGNITION");
						reasonCodeNew.setHandle(reasonCodeRuleHandle);
						reasonCodeNew.setActivity(activity);
						reasonCodeNew.setReasonCodePriorityBo(reasonCodePriorityBo);
						reasonCodeNew.setModifiedUser(ContextHelper.getContext().getUser());
						reasonCodeRulePOMapper.insert(reasonCodeNew);
					}
					// update recodeCodeRuleParam
					updaterecodeCodeRuleParam(site, reasonCodeRuleHandle, activityParamVos);
				}
			} else if ("CHANGE_PARAMETER".equals(activityCode)) {
				updaterecodeCodeRuleParam(site, reasonCodeRuleHandle, activityParamVos);
			}
		}
	}

	// Update RecodeCodeRuleParam
	private void updaterecodeCodeRuleParam(String site, String reasonCodeRuleHandle,
			List<ActivityParamVO> activityParamVos) {
		ReasonCodeRuleParamPOExample example = new ReasonCodeRuleParamPOExample();
		example.createCriteria().andReasonCodeRuleBoEqualTo(reasonCodeRuleHandle);
		List<ReasonCodeRuleParamPO> list = reasonCodeRuleParamPOMapper.selectByExample(example);
		if (list.size() > 0) {
			reasonCodeRuleParamPOMapper.deleteByExample(example);
		}
		for (ActivityParamVO vo : activityParamVos) {
			if (!Utils.isEmpty(vo.getParamSetValue())) {
				ReasonCodeRuleParamPO po = new ReasonCodeRuleParamPO();
				String handle = new ReasonCodeRuleParamBOHandle(site, reasonCodeRuleHandle, "*", vo.getParamId())
						.getValue();
				po.setHandle(handle);
				po.setParamId(vo.getParamId());
				po.setResourceType("*");
				po.setSite(site);
				po.setReasonCodeRuleBo(reasonCodeRuleHandle);
				po.setModifiedUser(ContextHelper.getContext().getUser());
				po.setParamValue(vo.getParamSetValue());
				reasonCodeRuleParamPOMapper.insertSelective(po);
			}
		}
	}

	public void delete(List<ReasonCodePriorityVO> reasonCodePriorityVos)
			throws NoSuchException, DeleteErrorException, UsedException {
		for (ReasonCodePriorityVO vo : reasonCodePriorityVos) {
			ReasonCodePriorityPO reasonCodePriorityPO = reasonCodePriorityPOMapper.selectByPrimaryKey(vo.getHandle());
			if (reasonCodePriorityPO == null) {
				throw new NoSuchException("reasonCodePriority");
			}
			if ("true".equals(reasonCodePriorityPO.getUsed())) {
				throw new UsedException();
			}
			// delete reason_code_priority
			ReasonCodePriorityPOExample example = new ReasonCodePriorityPOExample();
			example.createCriteria().andUsedEqualTo("false").andHandleEqualTo(vo.getHandle());
			List<ReasonCodePriorityPO> pos = reasonCodePriorityPOMapper.selectByExample(example);
			if (pos.size() > 0) {
				for (ReasonCodePriorityPO po : pos) {
					reasonCodePriorityPOMapper.deleteByPrimaryKey(po.getHandle());
				}
			}
			// delete reason_code_rule
			ReasonCodeRulePOExample reasonCodeRulePOExample = new ReasonCodeRulePOExample();
			reasonCodeRulePOExample.createCriteria().andReasonCodePriorityBoEqualTo(vo.getHandle());
			List<ReasonCodeRulePO> reasonCodeRulePos = reasonCodeRulePOMapper.selectByExample(reasonCodeRulePOExample);
			if (reasonCodeRulePos.size() > 0) {
				for (ReasonCodeRulePO reasonCodeRulePO : reasonCodeRulePos) {
					// delete reason_code_rule_param
					ReasonCodeRuleParamPOExample paramPOExample = new ReasonCodeRuleParamPOExample();
					paramPOExample.createCriteria().andReasonCodeRuleBoEqualTo(reasonCodeRulePO.getHandle());
					List<ReasonCodeRuleParamPO> params = reasonCodeRuleParamPOMapper.selectByExample(paramPOExample);
					if (params.size() > 0) {
						reasonCodeRuleParamPOMapper.deleteByExample(paramPOExample);
					}
					int ret = reasonCodeRulePOMapper.deleteByPrimaryKey(reasonCodeRulePO.getHandle());
					if (ret != 1) {
						throw new DeleteErrorException(REASON_CODE_RULE_PARAM, reasonCodeRulePO.getHandle());
					}
				}
			}
		}
	}

	// 新增 reason_code_rule 的 activity 字段
	public String insertReasonCodeRuleF(RecognitionVO.ReasonCodeRuleVO reasonCodeRuleVO) throws BusinessException {
		String rule = "FILTER";
		String handle = new ReasonCodeRuleBOHandle(reasonCodeRuleVO.getReasonCodePriorityBo(), rule).getValue();
		ReasonCodeRulePO rulePO = new ReasonCodeRulePO();
		rulePO.setHandle(handle);
		rulePO.setReasonCodePriorityBo(reasonCodeRuleVO.getReasonCodePriorityBo());
		rulePO.setRule(rule);
		rulePO.setActivity(reasonCodeRuleVO.getActivity());
		try {
			reasonCodeRulePOMapper.insert(rulePO);
		} catch (RuntimeException e) {
			throw new BusinessException(ErrorCodeEnum.BASIC, "Insert activity fail");
		}
		return handle;
	}

	// 修改 reason_code_rule 的 activity 字段
	public void updateReasonCodeRuleF(RecognitionVO.ReasonCodeRuleVO reasonCodeRuleVO)
			throws BusinessException, ParseException {
		// should be not used
		ReasonCodePriorityPO priorityPO = reasonCodePriorityPOMapper
				.selectByPrimaryKey(reasonCodeRuleVO.getReasonCodePriorityBo());
		if ("true".equals(priorityPO.getUsed())) {
			throw new BusinessException(ErrorCodeEnum.BASIC, "Rule already used in recognition job");
		}

		// delete all rule param
		ReasonCodeRuleParamPOExample ruleParamExample = new ReasonCodeRuleParamPOExample();
		ruleParamExample.createCriteria().andReasonCodeRuleBoEqualTo(reasonCodeRuleVO.getReasonCodeRuleBo());
		reasonCodeRuleParamPOMapper.deleteByExample(ruleParamExample);

		// update rule
		ReasonCodeRulePOExample example = new ReasonCodeRulePOExample();
		Date modifiedDateTime = Utils.strToDatetime(reasonCodeRuleVO.getReasonCodeRuleModifiedDateTime());
		example.createCriteria().andHandleEqualTo(reasonCodeRuleVO.getReasonCodeRuleBo())
				.andModifiedDateTimeEqualTo(modifiedDateTime);
		ReasonCodeRulePO rulePO = new ReasonCodeRulePO();
		rulePO.setHandle(reasonCodeRuleVO.getReasonCodeRuleBo());
		rulePO.setActivity(reasonCodeRuleVO.getActivity());
		rulePO.setModifiedUser(ContextHelper.getContext().getUser());
		int ret = reasonCodeRulePOMapper.updateByExampleSelective(rulePO, example);
		if (ret != 1) {
			throw new BusinessException(ErrorCodeEnum.BASIC, "Someone update this record after you select");
		}
	}

	public String selectReasonCodeRuleBoByCondition(String reasonCodePriorityBo) {
		String rule = "FILTER";
		ReasonCodeRulePOExample example = new ReasonCodeRulePOExample();
		example.createCriteria().andReasonCodePriorityBoEqualTo(reasonCodePriorityBo).andRuleEqualTo(rule);
		List<ReasonCodeRulePO> reasonCodeRulePOList = reasonCodeRulePOMapper.selectByExample(example);
		if (reasonCodeRulePOList.size() > 0) {
			return reasonCodeRulePOList.get(0).getHandle();
		} else {
			throw null;
		}
	}

	public String getActivityNameByReasonCodePriorityBO(String reasonCodePriorityBO, String rule) {
		ReasonCodeRulePOExample example = new ReasonCodeRulePOExample();
		example.createCriteria()
				.andReasonCodePriorityBoEqualTo(reasonCodePriorityBO)
				.andRuleEqualTo(rule);
		List<ReasonCodeRulePO> ret = reasonCodeRulePOMapper.selectByExample(example);
		if (ret.isEmpty()) {
			logger.debug("cant find the rule for " + reasonCodePriorityBO + " " + rule);
		} else if (ret.size() > 1) {
			logger.debug("more than one rule is found for " + reasonCodePriorityBO + " " + rule);
		}
		if (ret.size() == 1) {
			return ret.get(0).getActivity();
		} else {
			return "";
		}
	}

	public String getReasonCodeRuleByPriorityBO(String reasonCodePriorityBO, String rule) {
		ReasonCodeRulePOExample example = new ReasonCodeRulePOExample();
		example.createCriteria()
				.andReasonCodePriorityBoEqualTo(reasonCodePriorityBO)
				.andRuleEqualTo(rule);
		List<ReasonCodeRulePO> ret = reasonCodeRulePOMapper.selectByExample(example);
		if (ret.isEmpty()) {
			logger.debug("cant find the rule for " + reasonCodePriorityBO + " " + rule);
		} else if (ret.size() > 1) {
			logger.debug("more than one rule is found for " + reasonCodePriorityBO + " " + rule);
		}
		if (ret.size() == 1) {
			return ret.get(0).getHandle();
		} else {
			return "";
		}
	}
	
	public String getReasonCodeRuleByPriorityBOAndActivity(String reasonCodePriorityBO, String rule,String activity) {
		ReasonCodeRulePOExample example = new ReasonCodeRulePOExample();
		example.createCriteria().andReasonCodePriorityBoEqualTo(reasonCodePriorityBO).andRuleEqualTo(rule).andActivityEqualTo(activity);
		List<ReasonCodeRulePO> ret = reasonCodeRulePOMapper.selectByExample(example);
		if (ret.isEmpty()) {
			logger.debug("cant find the rule for " + reasonCodePriorityBO + " " + rule);
		} else if (ret.size() > 1) {
			logger.debug("more than one rule is found for " + reasonCodePriorityBO + " " + rule);
		}
		if (ret.size() == 1) {
			return ret.get(0).getHandle();
		} else {
			return "";
		}
	}

	public void deleteReasonCodeRuleParamByReasonCodeRuleBo(String reasonCodeRuleBo) {
		ReasonCodeRuleParamPOExample example = new ReasonCodeRuleParamPOExample();
		example.createCriteria().andReasonCodeRuleBoEqualTo(reasonCodeRuleBo);
		reasonCodeRuleParamPOMapper.deleteByExample(example);
	}

	public void deleteReasonCodeRuleByHandle(String reasonCodeRuleBo) {
		reasonCodeRulePOMapper.deleteByPrimaryKey(reasonCodeRuleBo);
	}
}
