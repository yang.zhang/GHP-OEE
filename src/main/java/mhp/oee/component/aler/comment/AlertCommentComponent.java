package mhp.oee.component.aler.comment;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.handle.AlertCommentBOHandle;
import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.AlertCommentPOMapper;
import mhp.oee.dao.gen.ResourceAlertLogPOMapper;
import mhp.oee.dao.gen.ResourceTypePOMapper;
import mhp.oee.po.gen.AlertCommentPO;
import mhp.oee.po.gen.AlertCommentPOExample;
import mhp.oee.po.gen.ResourceAlertLogPO;
import mhp.oee.po.gen.ResourceAlertLogPOExample;
import mhp.oee.po.gen.ResourceTypePO;
import mhp.oee.reporting.alertcomment.AlertCommentVO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.AlertComentManagerVO;

@Component
public class AlertCommentComponent {

    @Autowired
    AlertCommentPOMapper alertCommentPOMapper;
    @Autowired
    ResourceAlertLogPOMapper resourceAlertLogPOMapper;
    @Autowired
    ResourceTypePOMapper resourceTypePOMapper;
    
    private static final String ALERT_COMMENT = "ALERT_COMMENT";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    
    public void createAlertComment(AlertCommentVO vo) throws ExistingRecordException {
        AlertCommentPOExample example = new AlertCommentPOExample();
		List<AlertCommentPO> alertCommentPOList = alertCommentPOMapper.selectByExample(example);
		int cCodeMax = 1;
		for(AlertCommentPO alertCommentPO:alertCommentPOList){
			String commentCode = alertCommentPO.getCommentCode();
			if(commentCode==null||"".equals(commentCode)){
				continue;
			}
			int cCode = Integer.parseInt(commentCode.substring(3,commentCode.length()));
			cCodeMax = cCodeMax>cCode?cCodeMax:cCode;
		}
		cCodeMax+=1;
		vo.setCommentCode("COM"+cCodeMax);
		AlertCommentBOHandle boHandle=new AlertCommentBOHandle(vo.getSite(), vo.getCommentCode());
        String handle=boHandle.getValue();
        AlertCommentPO po=alertCommentPOMapper.selectByPrimaryKey(handle);
        if(po!=null){
            throw new ExistingRecordException(ALERT_COMMENT, gson.toJson(po));
        }
        vo.setHandle(handle);
        String desc=vo.getDescription();
        String[] descArray = desc.split("-");
        if(descArray.length==1){
            vo.setDescription(vo.getResourceTypeDesc()+"-"+desc);
        }else{
            String newDesc=desc.replaceFirst(descArray[0],vo.getResourceTypeDesc());
            vo.setDescription(newDesc);
        }
        //vo.setDescription(vo.getResourceTypeDesc()+"-"+vo.getDescription());
        vo.setModifiedUser(ContextHelper.getContext().getUser());
        alertCommentPOMapper.insert(vo);
    }

    public void updateAlertComment(AlertCommentVO vo) throws BusinessException {
        AlertCommentPO po = Utils.copyObjectProperties(vo, AlertCommentPO.class);
        ResourceAlertLogPOExample example = new ResourceAlertLogPOExample();
        example.createCriteria().andSiteEqualTo(vo.getSite()).andCommentCodeEqualTo(vo.getCommentCode());
        List<ResourceAlertLogPO> resourceAlertLogPOList = resourceAlertLogPOMapper.selectByExample(example);
        if(resourceAlertLogPOList.size()>0){
            throw new BusinessException(ErrorCodeEnum.ALERT_COMMENT_USER, "comment_code is userd");
        }
        String desc=vo.getDescription();
        String[] descArray = desc.split("-");
        if(descArray.length==1){
            po.setDescription(vo.getResourceTypeDesc()+"-"+desc);
        }else{
            String newDesc=desc.replaceFirst(descArray[0],vo.getResourceTypeDesc());
            po.setDescription(newDesc);
        }
        po.setModifiedUser(ContextHelper.getContext().getUser());
        alertCommentPOMapper.updateByPrimaryKey(po);
    }

    public void deleteAlertComment(AlertCommentVO alertCommentVo) {
        String handle=alertCommentVo.getHandle();
        alertCommentPOMapper.deleteByPrimaryKey(handle);
    }

    public String getResourceTypeDesc(String site,String resourceType) {
        ResourceTypeBOHandle boHandle = new ResourceTypeBOHandle(site, resourceType);
        ResourceTypePO po = resourceTypePOMapper.selectByPrimaryKey(boHandle.getValue());
        return po.getDescription();
    }
    

}
