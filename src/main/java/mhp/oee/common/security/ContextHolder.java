package mhp.oee.common.security;

public interface ContextHolder {

    ExecutionContext createEmptyContext();

    void clearContext();

    ExecutionContext getContext();

    void setContext(ExecutionContext cxt);

}
