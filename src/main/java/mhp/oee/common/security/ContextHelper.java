package mhp.oee.common.security;

public class ContextHelper {

    private static ContextHolder contextHolder;

    static {
        contextHolder = new ThreadLocalContextHolder();
    }

    public static ExecutionContext getContext() {
        return contextHolder.getContext();
    }

    public static void clearContext() {
        contextHolder.clearContext();
    }

}
