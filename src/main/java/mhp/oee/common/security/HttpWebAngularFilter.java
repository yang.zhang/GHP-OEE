package mhp.oee.common.security;

import java.io.IOException;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.filter.OncePerRequestFilter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.user.PermissionComponent;
import mhp.oee.po.extend.ActivityPermAndActivityAndUserAndUserGroupAndUserGroupMemberPO;

public class HttpWebAngularFilter extends OncePerRequestFilter {

    @Autowired
    PermissionComponent permissionComponent;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @InjectableLogger
    private static Logger logger;

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String contextPath = request.getRequestURI();
        Map<String, String[]> parameterMap = request.getParameterMap();


        String user = ContextHelper.getContext().getUser();
        String site = parameterMap.get("permsite")[0];
        String activity = parameterMap.get("permactivity")[0];
        if(user == null) {
            logger.error("no such user exist");
            sessionTimeout(request, response);
        }

        ActivityPermAndActivityAndUserAndUserGroupAndUserGroupMemberPO readRecord = permissionComponent.getPermissionRecord(site, user, activity, Permission.READ.getValue());
        ActivityPermAndActivityAndUserAndUserGroupAndUserGroupMemberPO writeRecord = permissionComponent.getPermissionRecord(site, user, activity, Permission.RW.getValue());

        logger.debug("HttpWebAngularFilter url:>>>"+contextPath);
        logger.debug("HttpWebAngularFilter param:>>>>" + parameterMap);
        logger.debug("HttpWebAngularFilter method:>>>>>>>>"+request.getMethod());
        logger.debug("HttpWebAngularFilter read:>>>"+ gson.toJson(readRecord));
        logger.debug("HttpWebAngularFilter write:>>>"+ gson.toJson(writeRecord));

        if (HttpMethod.GET.matches(request.getMethod())) {
            if (readRecord != null || writeRecord != null) {
                logger.debug("HttpWebAngularFilter success in get method");
                filterChain.doFilter(request, response);
            } else {
                logger.error("fail in get method");
                unauthorized(request, response);
            }
        }

        if (HttpMethod.POST.matches(request.getMethod())) {
            if (writeRecord != null) {
                logger.debug("HttpWebAngularFilter success in post method");
                filterChain.doFilter(request, response);
            } else {
                logger.error("fail in post method");
                unauthorized(request, response);
            }
        }
    }

    private void unauthorized(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        session.invalidate();
        response.setHeader("WWW-Authenticate", "user's auth has been changed");
        response.sendError(413, "user's auth has been changed");
        return;
    }

    private void sessionTimeout(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        session.invalidate();
        response.setHeader("WWW-Authenticate", "user session timeout");
        response.sendError(403, "user session timeout");
        return;
    }

    enum Permission {
        READ("R"), RW("RW");

        private String value;

        private Permission(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
