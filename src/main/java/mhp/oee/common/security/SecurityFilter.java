package mhp.oee.common.security;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.filter.OncePerRequestFilter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.user.UserComponent;
import mhp.oee.po.gen.ResourcePlcInfoPO;
import mhp.oee.vo.AppServerUserDetailsVO;
import mhp.oee.web.angular.plc.accesstype.management.PlantAccessTypeService;

public class SecurityFilter extends OncePerRequestFilter {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private UserComponent userComponent;


    @Autowired
    private PlantAccessTypeService plantAccessTypeService;


    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Value("${uppc.guest.name}")
    private String guest_name = "client_guest";

    @Value("${properties.name}")
    private String environment;

    @Override
    public void destroy() {
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String requestURI = request.getRequestURI();
        //特殊处理
        if(requestURI.startsWith("/web/public") || requestURI.startsWith("/mhp-oee/web/public")){
            filterChain.doFilter(request,response);
            return;
        }

        String user = null;
        String pcIp = request.getParameter("plcIp");

        if(StringUtils.isNotBlank(pcIp)){
            user = this.guest_name;
        }

        //如果是clientIndex.html 先跳到中间页面获取ip
        if( requestURI.endsWith("clientIndex.html") ){
            request.getRequestDispatcher("/htmls/linkClientIndex.html").forward(request,response);
            return;
        }



        //logger.debug(">> SecurityFilter.doFilterInternal()");
        ExecutionContext executionContext = ContextHelper.getContext();
        // get user info from app server
        if(user == null){
            user = getUser(request,response);
        }

        if (user != null) {
            AppServerUserDetailsVO appServerUserDetailsVO = userComponent.readAppServerUserDetails(user);
            //logger.debug("appServerUserDetailsVO = " + appServerUserDetailsVO.toString());
            BeanUtils.copyProperties(appServerUserDetailsVO, executionContext);
            executionContext.setUser(user);
        } else {
            logger.error(">>>>>> no such user ");

        }

        try {
            filterChain.doFilter(request, response);
        } finally {
            // clean-up thread-local context
            ContextHelper.clearContext();
        }

        //logger.debug("<< SecurityFilter.doFilterInternal()");
    }

    private String getUser(HttpServletRequest request,HttpServletResponse response) {
        String user = null;
        Principal principal = request.getUserPrincipal();
        if (principal != null) {
            user = principal.getName();
            logger.debug(">>>>>>???security filter??? >>>>>");
            logger.debug(">>>>>>???user name in container??? >>>>>" + user);
        } else {
            // try to retrieve the user from HTTP HEADER, treating it as
            // HTTP_BASIC Auth
            String authHeader = request.getHeader("Authorization");
            if (authHeader != null) {
                StringTokenizer st = new StringTokenizer(authHeader);
                if (st.hasMoreTokens()) {
                    String basic = st.nextToken();

                    if (basic.equalsIgnoreCase("Basic")) {
                        try {
                            String credentials = new String(Base64.decodeBase64(st.nextToken()), "UTF-8");
                            int p = credentials.indexOf(":");
                            if (p != -1) {
                                user = credentials.substring(0, p).trim();
                            }
                        } catch (UnsupportedEncodingException e) {
                            // ignore the exception
                            logger.warn(
                                    "SecurityFilter.getUser() : Failed to retrieve authentication, " + e.getMessage());
                        }
                    }
                }

            }
//            else{
//                String pcIp = request.getRemoteHost();
//                List<ResourcePlcInfoPO> ResourcePlcList = plantAccessTypeService.getResourcePlcInfoByPcIPAndPlcAccessTypeNull(pcIp);
//                if(ResourcePlcList.size() == 1 ){
//                    user = "client_guest";
//
//                }else if(request.getRequestURI().contains("clientIndex.html")){
//                    try {
//                        response.sendRedirect("http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/htmls/index.html");
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }else {
//                    logger.error(" Can not find user");
//                    //user="site_admin";
//                }
//            }
            //开发环境
            if("dev".equals(this.environment)){
                user = "site_admin";
            }

        }

        return (user == null) ? null : user.toUpperCase(); // always return
        // upper case
    }

}
