package mhp.oee.common.security;

public class ThreadLocalContextHolder implements ContextHolder {

    private static final ThreadLocal<ExecutionContext> context = new ThreadLocal<ExecutionContext>();

    @Override
    public ExecutionContext createEmptyContext() {
        return new ExecutionContext();
    }

    @Override
    public void clearContext() {
        context.remove();
    }

    @Override
    public ExecutionContext getContext() {
        ExecutionContext cxt = context.get();
        if (cxt == null) {
            cxt = createEmptyContext();
            setContext(cxt);
        }
        return cxt;
    }

    @Override
    public void setContext(ExecutionContext cxt) {
        context.set(cxt);
    }

}
