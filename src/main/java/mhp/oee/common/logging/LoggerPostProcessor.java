package mhp.oee.common.logging;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.FieldCallback;
import org.springframework.util.ReflectionUtils.FieldFilter;

import mhp.oee.appserver.netweaver.NetWeaverLogger;
import mhp.oee.config.AppServerEnvironment;

/**
 * Inject logger to field which has annotation "InjectableLogger"
 * 
 */
@Service
public class LoggerPostProcessor implements BeanPostProcessor {

    @Autowired
    private Environment environment;

    @Override
    public Object postProcessBeforeInitialization(final Object bean, String beanName) throws BeansException {
        FieldCallback loggerFieldSetter = new FieldCallback() {
            @Override
            public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
                field.setAccessible(true);

                Logger logger = null;
                switch (AppServerEnvironment.initAppServerType(environment)) {
                case AppServerEnvironment.AppServerType.NETWEAVER:
                    logger = new NetWeaverLogger(bean.getClass());
                    break;
                case AppServerEnvironment.AppServerType.TOMCAT:
                default:
                    logger = LoggerFactory.getLogger(bean.getClass());
                    break;
                }
                field.set(bean, logger);
            }
        };

        FieldFilter fieldFilter = new FieldFilter() {
            @Override
            public boolean matches(Field field) {
                return field.getAnnotation(InjectableLogger.class) != null;
            }
        };

        ReflectionUtils.doWithFields(bean.getClass(), loggerFieldSetter, fieldFilter);
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

}
