package mhp.oee.common.exception;

public class PLCRecordException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public PLCRecordException(String tableName, String obj) {
        super(ErrorCodeEnum.PLC_RECORD, "Existing " + tableName + " record : " + obj, obj);
    }
}
