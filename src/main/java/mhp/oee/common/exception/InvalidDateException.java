package mhp.oee.common.exception;

public class InvalidDateException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidDateException() {
        super(ErrorCodeEnum.INVALID_DATE, "Invalid StartDate Format");
    }

}
