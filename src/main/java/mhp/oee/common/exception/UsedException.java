package mhp.oee.common.exception;

/**
 * Created by LinZuK on 2016/8/3.
 * 记录 used 为 true
 * Rule already used in recognition job
 */
public class UsedException extends BusinessException {

    public UsedException() {
        super(ErrorCodeEnum.USED_ERROR, "Record is in used.");
    }
}
