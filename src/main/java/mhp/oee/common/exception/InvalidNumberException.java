package mhp.oee.common.exception;

public class InvalidNumberException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidNumberException() {
        super(ErrorCodeEnum.INVALID_NUMBER, "Invalid PPM Number Format.");
    }
}
