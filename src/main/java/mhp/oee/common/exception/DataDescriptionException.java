package mhp.oee.common.exception;

/*
 * 若数据类型是报警信息和关键配件寿命，则检查描述是否填写，
 * 若无值，则将错误记录显示，并在消息列提示“数据项描述没有维护”
 */
public class DataDescriptionException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public DataDescriptionException() {
        super(ErrorCodeEnum.DATA_DESCRIPTION, "Data Description do not exist");
    }

}
