package mhp.oee.common.exception;

public class InvalidUserAuthException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidUserAuthException() {
        super(ErrorCodeEnum.USER_NO_AUTH, "user don't have enough auth.");
    }
}
