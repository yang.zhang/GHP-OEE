package mhp.oee.common.exception;

/**
 * Created by LinZuK on 2016/8/3.
 * enable 为 false
 */
public class ItemDisableException extends BusinessException {
    private static final long serialVersionUID = 1L;

    public ItemDisableException(String obj) {
        super(ErrorCodeEnum.EXISTING_RECORD, obj + " is disable.", obj);
    }
}
