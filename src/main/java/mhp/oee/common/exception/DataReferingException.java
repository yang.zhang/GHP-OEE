package mhp.oee.common.exception;

public class DataReferingException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public DataReferingException() {
        super(ErrorCodeEnum.DATA_REFER, "Record was refer by the other data in database, can not be deleted");
    }

}
