package mhp.oee.common.exception;

public class NotFoundActivityParamException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public NotFoundActivityParamException() {
        super(ErrorCodeEnum.NOT_FOUND_PARAM_VALUE_ALL, "PPM Activity Param Value is not maintained");
    }
}
