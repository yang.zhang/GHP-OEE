package mhp.oee.common.exception;

public class InvalidEffectiveDateException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidEffectiveDateException() {
        super(ErrorCodeEnum.INVALID_EFFECTIVE_DATE, "Invalid Effective Date");
    }

}
