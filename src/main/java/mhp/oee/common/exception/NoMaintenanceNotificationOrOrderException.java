package mhp.oee.common.exception;

public class NoMaintenanceNotificationOrOrderException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public NoMaintenanceNotificationOrOrderException() {
        super(ErrorCodeEnum.NO_MAINTENANCE_NOTIFICATION_OR_ORDER, "No Maintenance Notification or Order specified.");
    }

}
