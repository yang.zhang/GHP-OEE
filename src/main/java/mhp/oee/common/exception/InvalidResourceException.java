package mhp.oee.common.exception;

public class InvalidResourceException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidResourceException() {
        super(ErrorCodeEnum.INVALID_RESOURCE, "Invalid Resource.");
    }

}
