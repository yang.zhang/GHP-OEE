package mhp.oee.common.exception;

public class InvalidResourceStateException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidResourceStateException() {
        super(ErrorCodeEnum.INVALID_RESOURCE_STATE, "Invalid Resource State.");
    }

}
