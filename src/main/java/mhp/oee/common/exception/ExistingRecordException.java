package mhp.oee.common.exception;

public class ExistingRecordException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public ExistingRecordException(String tableName, String obj) {
        super(ErrorCodeEnum.EXISTING_RECORD, "Existing " + tableName + " record : " + obj, obj);
    }
}
