package mhp.oee.common.exception;

public class InvalidCycleTimeException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidCycleTimeException() {
        super(ErrorCodeEnum.INVALID_CYCLE_TIME,
                "The start date of inserting record was early than the start date of the existing record.");
    }

}
