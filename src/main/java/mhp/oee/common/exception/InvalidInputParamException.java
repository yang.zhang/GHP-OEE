package mhp.oee.common.exception;

public class InvalidInputParamException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidInputParamException(String param) {
        super(ErrorCodeEnum.INVALID_INPUT_PARAM, "Invalid Input Parameter: " + param);
    }

}
