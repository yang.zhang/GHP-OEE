package mhp.oee.common.exception;

/*
 * 生效日期必须大于当前日期
 */
public class EffectiveDateException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public EffectiveDateException() {
        super(ErrorCodeEnum.EFFICTIVE_DATE, "effictive must granter than current date");
    }

}
