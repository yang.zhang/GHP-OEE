package mhp.oee.common.exception;

public class InvalidParamException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidParamException(String param, String json) {
        super(ErrorCodeEnum.PARAM_IS_NULL, param + " Is Null : " + json, json);
    }
}
