package mhp.oee.common.exception;

/**
 * Created by LinZuK on 2016/8/3.
 * 未知规则错误
 */
public class UnknownRuleException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public UnknownRuleException(String rule) {
        super(ErrorCodeEnum.UNKNOWN_VIEW_ACTION, "Unknown rule: " + rule);
    }

}
