package mhp.oee.common.exception;

/*
 * 若数据类型是设备状态、原因代码、设备启动防呆、优率数据、实时PPM、三色灯，则检查数据项代码是否填写，若无值，
 * 则将错误记录显示，并在消息列提示“数据项代码没有维护
 *
 */
public class DataCodeException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public DataCodeException() {
        super(ErrorCodeEnum.DATA_CODE, "DataCode not exist");
    }

}
