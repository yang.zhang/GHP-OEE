package mhp.oee.common.exception;

/**
 * Created by LinZuK on 2016/8/3.
 */
public class UnknownViewActionException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public UnknownViewActionException(Character viewActionCode) {
        super(ErrorCodeEnum.UNKNOWN_VIEW_ACTION, "Unknown view action code: " + viewActionCode);
    }

}
