package mhp.oee.common.exception;

public class ExistingException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public ExistingException() {
        super(ErrorCodeEnum.EXISTING, "record does not exist in database");
    }

}
