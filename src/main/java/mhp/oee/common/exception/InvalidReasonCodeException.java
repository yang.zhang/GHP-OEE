package mhp.oee.common.exception;

public class InvalidReasonCodeException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidReasonCodeException() {
        super(ErrorCodeEnum.INVALID_REASON_CODE, "Invalid Reason Code.");
    }

}
