package mhp.oee.common.exception;

public class InvalidItemException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidItemException() {
        super(ErrorCodeEnum.INVALID_ITEM, "Invalid Item.");
    }

}
