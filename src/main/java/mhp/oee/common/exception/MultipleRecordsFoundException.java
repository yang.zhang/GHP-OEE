package mhp.oee.common.exception;

public class MultipleRecordsFoundException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public MultipleRecordsFoundException() {
        super(ErrorCodeEnum.MULTIPLE_RECORDS_FOUND, "Multiple records found.");
    }

}
