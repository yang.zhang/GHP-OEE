package mhp.oee.common.exception;

public class InvalidReasonCodeAlertUploadException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidReasonCodeAlertUploadException() {
        super(ErrorCodeEnum.INVALID_REASON_CODE_ALERT_UPLOAD, "Insert date was early than record's start date");
    }

}
