package mhp.oee.common.exception;

public class InvalidTimeException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidTimeException(String obj) {
        super(ErrorCodeEnum.INVALID_TIME, "Time error :" + obj, obj);
    }
}
