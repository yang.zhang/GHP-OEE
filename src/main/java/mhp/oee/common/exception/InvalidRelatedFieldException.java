package mhp.oee.common.exception;

public class InvalidRelatedFieldException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidRelatedFieldException() {
        super(ErrorCodeEnum.INVALID_RELATED_FIELD, "Invalid Related Field.");
    }

}
