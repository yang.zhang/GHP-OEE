package mhp.oee.common.exception;

public class CreateErrorException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public CreateErrorException(String tableName, String obj) {
        super(ErrorCodeEnum.CREATE_ERROR, "Create " + tableName + " record error: " + obj, obj);
    }
}
