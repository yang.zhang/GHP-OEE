package mhp.oee.common.exception;

public class DeleteErrorException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public DeleteErrorException(String tableName, String obj) {
        super(ErrorCodeEnum.DELETE_ERROR, "Delete " + tableName + " record error: " + obj, obj);
    }
}
