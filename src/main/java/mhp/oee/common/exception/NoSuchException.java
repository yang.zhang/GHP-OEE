package mhp.oee.common.exception;

/**
 * Created by LinZuK on 2016/8/3.
 */
public class NoSuchException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public NoSuchException(String obj) {
        super(ErrorCodeEnum.NO_SUCH_ERROR, "No such " + obj + " error.", obj);
    }

}
