package mhp.oee.common.exception;

public class BusinessException extends Exception {

    private static final long serialVersionUID = 1L;

    private ErrorCodeEnum errorCode;
    private String errorJson;

    public BusinessException(ErrorCodeEnum errorCode) {
        super();
        this.errorCode = errorCode;
    }

    public BusinessException(ErrorCodeEnum errorCode, Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
    }

    public BusinessException(ErrorCodeEnum errorCode, String traceMessage) {
        super(traceMessage);
        this.errorCode = errorCode;
    }

    public BusinessException(ErrorCodeEnum errorCode, String traceMessage, String errorJson) {
        super(traceMessage);
        this.errorCode = errorCode;
        this.errorJson = errorJson;
    }

    public BusinessException(ErrorCodeEnum errorCode, String traceMessage, Throwable cause) {
        super(traceMessage, cause);
        this.errorCode = errorCode;
    }

    public ErrorCodeEnum getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCodeEnum errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorJson() {
        return errorJson;
    }

    public void setErrorJson(String errorJson) {
        this.errorJson = errorJson;
    }

    @Override
    public String getMessage() {
        String message = String.format("[%d] %s", this.errorCode.getErrorCode(), super.getMessage());
        return message;
    }

}
