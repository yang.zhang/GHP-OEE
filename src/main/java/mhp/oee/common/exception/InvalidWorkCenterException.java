package mhp.oee.common.exception;

public class InvalidWorkCenterException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidWorkCenterException() {
        super(ErrorCodeEnum.INVALID_WORK_CENTER, "Invalid Work Center.");
    }

}
