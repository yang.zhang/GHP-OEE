package mhp.oee.common.exception;

/**
 * Created by DingLJ on 2017/4/17 017.
 */
public class ServiceErrorException extends RuntimeException{

    private int code;


    public ServiceErrorException(ErrorCodeEnum codeEnum, String msg){
        super(msg);
        this.code = codeEnum.getErrorCode();
    }


    public ServiceErrorException(int code, String msg){
        super(msg);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "{\"code\":" + this.code + ",\"msg\":\"" + super.getLocalizedMessage() + "\"}";
    }


    public static void main(String[] args){

        ServiceErrorException e = new ServiceErrorException(ErrorCodeEnum.DATA_CODE,"数据异常");


        System.out.println(e);

    }
}
