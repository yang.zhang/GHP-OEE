package mhp.oee.common.exception;

public class InvalidInputDateException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidInputDateException() {
        super(ErrorCodeEnum.INVALID_INPUTDATE, "Input time should be later than current time");
    }

}
