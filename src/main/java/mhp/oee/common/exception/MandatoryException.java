package mhp.oee.common.exception;

public class MandatoryException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public MandatoryException() {
        super(ErrorCodeEnum.MANDATORY, "field can not be blank");
    }

}
