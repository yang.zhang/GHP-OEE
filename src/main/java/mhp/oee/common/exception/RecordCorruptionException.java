package mhp.oee.common.exception;

public class RecordCorruptionException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public RecordCorruptionException(String record) {
        super(ErrorCodeEnum.RECORD_CORRUPTION, "Record from DB is corrupted : " + record);
    }

}
