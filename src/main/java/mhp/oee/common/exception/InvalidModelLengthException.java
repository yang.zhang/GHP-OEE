package mhp.oee.common.exception;

public class InvalidModelLengthException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidModelLengthException() {
        super(ErrorCodeEnum.INVALID_MODEL_LENGTH, "model length should be 6/7");
    }

}
