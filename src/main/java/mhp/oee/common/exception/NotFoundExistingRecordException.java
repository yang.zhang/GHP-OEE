package mhp.oee.common.exception;

public class NotFoundExistingRecordException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public NotFoundExistingRecordException() {
        super(ErrorCodeEnum.NOT_FOUND_RECORD, "Record can not be found.");
    }

}