package mhp.oee.common.exception;

public class ActivityParamErrorException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public ActivityParamErrorException() {
        super(ErrorCodeEnum.NOT_FOUND_PARAM_VALUE, "PPM Activity Param Value is error");
    }
}
