package mhp.oee.common.exception;

public class UploadFilenameException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public UploadFilenameException() {
        super(ErrorCodeEnum.UPLOAD_FILENAME, "upload file was not end with .xlsx");
    }

}
