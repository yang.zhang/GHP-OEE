package mhp.oee.common.exception;

public class ExistingRepeatTimeException  extends BusinessException {
    private static final long serialVersionUID = 1L;

    public ExistingRepeatTimeException(String obj) {
        super(ErrorCodeEnum.REPEAT_TIME, "Existing repeat time :" + obj, obj);
    }
}
