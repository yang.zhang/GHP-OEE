package mhp.oee.common.exception;

public class UpdateErrorException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public UpdateErrorException(String tableName, String obj) {
        super(ErrorCodeEnum.UPDATE_ERROR, "Update " + tableName + " record error: " + obj, obj);
    }
}
