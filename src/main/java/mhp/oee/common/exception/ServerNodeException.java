package mhp.oee.common.exception;

/**
 * Created by i069533 on 13/04/2017.
 */
public class ServerNodeException extends BusinessException {
    private static final long serialVersionUID = 1L;

    /*
     * 若数据类型是设备状态、原因代码、设备启动防呆、报警信息、三色灯，
     * 则检查寄存器值是否填写，若无值，则将错误记录显示，并在消息列提示“寄存器值没有维护”
     */
    public ServerNodeException() {
        super(ErrorCodeEnum.SERVER_NODE, "server node can not be null or empty");
    }

}
