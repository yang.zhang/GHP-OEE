package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ReasonCodeAlertBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ReasonCodeAlertBO";

    public ReasonCodeAlertBOHandle(String handleString) {
        super(handleString);
    }

    public ReasonCodeAlertBOHandle(String resourceTypeHandle, String alertSequenceId, String reasonCodeHandle, String strt) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(resourceTypeHandle).append(',').append(alertSequenceId).append(',')
                .append(reasonCodeHandle).append(',').append(strt);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(2);
    }

    public String getResourceType() {
        return getComponent(3);
    }

    public String getAlertSequenceId() {
        return getComponent(4);
    }

    public String getReasonCode() {
        return getComponent(7);
    }

    public String getStrt() {
        return getComponent(8);
    }

    public static ReasonCodeAlertBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ReasonCodeAlertBOHandle newHandle = new ReasonCodeAlertBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
