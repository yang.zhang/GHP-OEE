package mhp.oee.common.handle;

public class ActivityGroupBOHandle extends BOHandle {

    public static final String ActivityGroupBOHandle$ = "ActivityGroupBO";
    public static final String ActivityGroupActivityBOHandle$ = "ActivityGroupActivityBO";
    public static final String Handle_Prefix_Delimiter = ":";
    public static final String ActivityGroupBOHandle_Prefix = ActivityGroupBOHandle$ + Handle_Prefix_Delimiter;
    public static final String ActivityGroupActivityBOHandle_Prefix = ActivityGroupActivityBOHandle$ + Handle_Prefix_Delimiter;

    public ActivityGroupBOHandle() {
        // Empty constructor for access to static declarations
    }

    public ActivityGroupBOHandle(String activitygroup) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activitygroup.indexOf(ActivityGroupBOHandle_Prefix) >= 0) {
            setValue(activitygroup);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(ActivityGroupBOHandle_Prefix).append(activitygroup);
            setValue(sb.toString());
        }
    }

    public String getActivityGroup() {
        return getComponent(1);
    }

    public static ActivityGroupBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ActivityGroupBOHandle newHandle = new ActivityGroupBOHandle(handle.getValue());
        if (!ActivityGroupBOHandle$.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }

    public static BOHandle buildActivityGroupActivityHandle(BOHandle activityGroupHandle, BOHandle activityHandle) {
        StringBuilder sb = new StringBuilder();
        sb.append(ActivityGroupActivityBOHandle_Prefix).append(activityGroupHandle).append(",").append(activityHandle);
        return new BOHandle(sb.toString());
    }

}
