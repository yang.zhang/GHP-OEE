package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class PlcAccessTypeBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "PlcAccessTypeBO";

    public PlcAccessTypeBOHandle(String handleString) {
        super(handleString);
    }

    public PlcAccessTypeBOHandle(String site, String plcAccessType) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(plcAccessType);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getPlcAccessType() {
        return getComponent(2);
    }

    public static PlcAccessTypeBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        PlcAccessTypeBOHandle newHandle = new PlcAccessTypeBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
