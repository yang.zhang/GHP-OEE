package mhp.oee.common.handle;
/**
* @author zhengxx
* @E-mail: zhengxx@ATLBattery.com
*/
public class ResourceCurrentDataHandle extends BOHandle {
	private static final String HANDLE_PREFIX = "ResourceCurrentDataBO";



    public ResourceCurrentDataHandle( String resourceBo) {
    	super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(resourceBo);
        setValue(sb.toString());
    }

    
}
