package mhp.oee.common.handle;

public class SiteBOHandle extends BOHandle {

    private static final String HANDLE_PREFIX = "SiteBO";

    public SiteBOHandle(String site) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(":").append(site);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public static SiteBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        if (handle instanceof SiteBOHandle) {
            return (SiteBOHandle) handle;
        }
        SiteBOHandle newHandle = new SiteBOHandle(handle.getComponent(1));
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }

}
