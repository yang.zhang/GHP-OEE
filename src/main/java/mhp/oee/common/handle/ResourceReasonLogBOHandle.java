package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ResourceReasonLogBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ResourceReasonLogBO";

    public ResourceReasonLogBOHandle(String activity) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activity.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(activity);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(activity);
            setValue(sb.toString());
        }
    }

    public static ResourceReasonLogBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ResourceReasonLogBOHandle newHandle = new ResourceReasonLogBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
