package mhp.oee.common.handle;

public class ItemBOHandle extends BOHandle {

    private static final String HANDLE_PREFIX = "ItemBO";

    public ItemBOHandle(String site, String item, String revision) {
        super();
        setValue(site, item, revision);
    }

    public ItemBOHandle(String handle) {
        super(handle);
    }

    public void setValue(String site, String item, String revision) {
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',');
        sb.append(item).append(',').append(revision);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getItem() {
        return getComponent(2);
    }

    @Override
    public String getRevision() {
        return getComponent(3);
    }

//    @Override
//    public void parseValue() {
//        if (value == null) {
//            return;
//        }
//
//        // Parse out handle prefix which occurs before first colon
//        int colonPos = value.indexOf(":");
//        // The colon position cannot be the first or last position in the string
//        if (colonPos <= 0 || colonPos == (value.length() - 1)) {
//            super.parseValue();
//            return;
//        }
//        String handlePrefix = value.substring(0, colonPos);
//
//        // Now, the remaining handle values should be separated by commas
//        String handleValueStr = value.substring(colonPos + 1);
//        Vector<String> handleValueList = Utils.tokenize(handleValueStr, ",");
//
//        components = new Vector<String>();
//        components.add(handlePrefix);
//        if (!Utils.isEmpty(handleValueList)) {
//            components.addAll(handleValueList);
//        }
//    }

    public static ItemBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ItemBOHandle newHandle = new ItemBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }

}
