package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class WorkCenterMemberBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "WorkCenterMemberBO";

    public WorkCenterMemberBOHandle(String activity) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activity.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(activity);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(activity);
            setValue(sb.toString());
        }
    }

    public WorkCenterMemberBOHandle(String workCenterHandle, String workCenterGboHandle) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(workCenterHandle).append(',').append(workCenterGboHandle);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(2);
    }

    public String getWorkCenter() {
        return getComponent(3);
    }

    public static WorkCenterMemberBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        WorkCenterMemberBOHandle newHandle = new WorkCenterMemberBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
