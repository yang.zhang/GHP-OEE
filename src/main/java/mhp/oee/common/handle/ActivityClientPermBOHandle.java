package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ActivityClientPermBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ActivityClientPermBO";

    public ActivityClientPermBOHandle(String userGroupHandle, String activityClientHandle) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(userGroupHandle).append(',').append(activityClientHandle);
        setValue(sb.toString());
    }

    public ActivityClientPermBOHandle(String handleString) {
        super(handleString);
    }

    @Override
    public String getSite() {
        return getComponent(2);
    }

    public String getUserGroup() {
        return getComponent(3);
    }

    public String getActivityClient() {
        return getComponent(5);
    }

    public static ActivityClientPermBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ActivityClientPermBOHandle newHandle = new ActivityClientPermBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
