package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class StdCycleTimeBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "StdCycleTimeBO";

    public StdCycleTimeBOHandle(String activity) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activity.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(activity);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(activity);
            setValue(sb.toString());
        }
    }

    public StdCycleTimeBOHandle(String site, String item, String operation, String resrce, String strt) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(item).append(',').append(operation)
                .append(',').append(resrce).append(',').append(strt);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getItem() {
        return getComponent(2);
    }

    public String getOperation() {
        return getComponent(3);
    }

    public String getResrce() {
        return getComponent(4);
    }

    public String getStrt() {
        return getComponent(5);
    }

    public static StdCycleTimeBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        StdCycleTimeBOHandle newHandle = new StdCycleTimeBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
