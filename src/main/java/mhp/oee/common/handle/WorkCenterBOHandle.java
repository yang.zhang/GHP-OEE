package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class WorkCenterBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "WorkCenterBO";

    public WorkCenterBOHandle(String activity) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activity.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(activity);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(activity);
            setValue(sb.toString());
        }
    }

    public WorkCenterBOHandle(String site, String workCenter) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(workCenter);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getWorkCenter() {
        return getComponent(2);
    }

    public static WorkCenterBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        WorkCenterBOHandle newHandle = new WorkCenterBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
