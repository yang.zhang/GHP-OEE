package mhp.oee.common.handle;

public class ActivityBOHandle extends BOHandle {

    private static final String HANDLE_PREFIX = "ActivityBO";

    public ActivityBOHandle() {
        // Empty constructor for access to static declarations
    }

    public ActivityBOHandle(String activity) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activity.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(activity);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(activity);
            setValue(sb.toString());
        }
    }

    public String getActivity() {
        return getComponent(1);
    }

    public static ActivityBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ActivityBOHandle newHandle = new ActivityBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
