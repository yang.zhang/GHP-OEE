package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ProductionOutputRealtimeBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ProductionOutputRealtimeBO";

    public ProductionOutputRealtimeBOHandle(String productionOutputRealtime) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (productionOutputRealtime.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(productionOutputRealtime);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(productionOutputRealtime);
            setValue(sb.toString());
        }
    }

    public static ProductionOutputRealtimeBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ProductionOutputRealtimeBOHandle newHandle = new ProductionOutputRealtimeBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
