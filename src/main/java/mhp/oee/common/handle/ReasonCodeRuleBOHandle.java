package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ReasonCodeRuleBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ReasonCodeRuleBO";

    public ReasonCodeRuleBOHandle(String handleString) {
        super(handleString);
    }

    public ReasonCodeRuleBOHandle(String reasonCodePriorityHandle, String rule) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(reasonCodePriorityHandle);
        sb.append(',').append(rule);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(3);
    }

    public String getReasonCode() {
        return getComponent(4);
    }

    public String getRule() {
        return getComponent(5);
    }

    public static ReasonCodeRuleBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ReasonCodeRuleBOHandle newHandle = new ReasonCodeRuleBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
