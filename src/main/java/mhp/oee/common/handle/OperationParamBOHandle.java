package mhp.oee.common.handle;
/**
* @author DingLj
*/
public class OperationParamBOHandle extends BOHandle {
	private static final String HANDLE_PREFIX = "OperationParamBO";


    public OperationParamBOHandle(String handleString) {
    	super(handleString);
    }

    public OperationParamBOHandle(String site, String resourceType, String paramKey) {
    	super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(resourceType).append(',').append(paramKey);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getResourceType() {
        return getComponent(2);
    }

    public String getParamKey(){
        return super.getComponent(3);
    }

    public static OperationParamBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        OperationParamBOHandle newHandle = new OperationParamBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
