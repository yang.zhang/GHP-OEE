package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ResourceTypeBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ResourceTypeBO";

    public ResourceTypeBOHandle(String activity) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activity.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(activity);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(activity);
            setValue(sb.toString());
        }
    }

    public ResourceTypeBOHandle(String site, String resourceType) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(resourceType);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getResourceType() {
        return getComponent(2);
    }

    public static ResourceTypeBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ResourceTypeBOHandle newHandle = new ResourceTypeBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
