package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ProductionOutputFollowUpBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ProductionOutputFollowUpBO";

    public ProductionOutputFollowUpBOHandle(String productionOutputFollowUp) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (productionOutputFollowUp.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(productionOutputFollowUp);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(productionOutputFollowUp);
            setValue(sb.toString());
        }
    }

    public ProductionOutputFollowUpBOHandle(String site, String resrce, String operation, String operationRevision,
            String item, String itemRevision, String strt) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(resrce).append(',').append(operation);
        sb.append(',').append(operationRevision).append(',').append(item).append(',').append(itemRevision);
        sb.append(',').append(strt);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getResrce() {
        return getComponent(2);
    }

    public String getOperation() {
        return getComponent(3);
    }

    public String getOperationRevision() {
        return getComponent(4);
    }

    public String getItem() {
        return getComponent(5);
    }

    public String getItemRevision() {
        return getComponent(6);
    }

    public String getStrt() {
        return getComponent(7);
    }

    public static ProductionOutputFollowUpBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ProductionOutputFollowUpBOHandle newHandle = new ProductionOutputFollowUpBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
