package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ReasonCodePriorityBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ReasonCodePriorityBO";

    public ReasonCodePriorityBOHandle(String handleString) {
        super(handleString);
    }

    public ReasonCodePriorityBOHandle(String reasonCodeBO, String priority, String subPriority, String strt) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(reasonCodeBO).append(',').append(priority).append(',').append(subPriority).append(',').append(strt);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(2);
    }

    public String getReasonCode() {
        return getComponent(3);
    }

    public String getPriority() {
        return getComponent(4);
    }

    public String getSubPriority() {
        return getComponent(5);
    }

    public String getStrt() {
        return getComponent(6);
    }

    public static ReasonCodePriorityBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ReasonCodePriorityBOHandle newHandle = new ReasonCodePriorityBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
