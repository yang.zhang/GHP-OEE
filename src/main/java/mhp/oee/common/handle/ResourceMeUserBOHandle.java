package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ResourceMeUserBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ResourceMeUserBO";

    public ResourceMeUserBOHandle(String handleString) {
        super(handleString);
    }

    public ResourceMeUserBOHandle(String resourceHandle, String meUserID, String shift) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(resourceHandle);
        sb.append(',').append(meUserID).append(',').append(shift);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(2);
    }

    public String getResrce() {
        return getComponent(3);
    }

    public String getMeUseId() {
        return getComponent(4);
    }

    public String getShift() {
        return getComponent(5);
    }

    public static ResourceMeUserBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ResourceMeUserBOHandle newHandle = new ResourceMeUserBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
