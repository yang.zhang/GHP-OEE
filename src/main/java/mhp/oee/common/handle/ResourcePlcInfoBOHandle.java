package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ResourcePlcInfoBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ResourcePlcInfoBO";

    public ResourcePlcInfoBOHandle(String handle) {
        if (handle.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(handle);
        }
    }

    public ResourcePlcInfoBOHandle(String site, String resourceRef) {
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(resourceRef);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getResrce() {
        return getComponent(4);
    }

    public static ResourcePlcInfoBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ResourcePlcInfoBOHandle newHandle = new ResourcePlcInfoBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
