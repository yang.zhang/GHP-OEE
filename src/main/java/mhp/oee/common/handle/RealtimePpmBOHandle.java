package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class RealtimePpmBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "RealtimePpmBO";

    public RealtimePpmBOHandle(String resourceHandle) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (resourceHandle.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(resourceHandle);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(resourceHandle);
            setValue(sb.toString());
        }
    }

    @Override
    public String getSite() {
        return getComponent(2);
    }

    public String getResrce() {
        return getComponent(3);
    }

    public static RealtimePpmBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        RealtimePpmBOHandle newHandle = new RealtimePpmBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
