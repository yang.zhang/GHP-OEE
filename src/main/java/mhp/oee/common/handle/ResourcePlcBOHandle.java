package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ResourcePlcBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ResourcePlcBO";

    public ResourcePlcBOHandle(String handleString) {
        super(handleString);
    }

    public ResourcePlcBOHandle(String site, String resourceHandle, String plcAddress, String plcValue) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(resourceHandle).append(',')
                .append(plcAddress).append(',').append(plcValue);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getResrce() {
        return getComponent(4);
    }

    public String getPlcAddress() {
        return getComponent(5);
    }

    public String getPlcValue() {
        return getComponent(6);
    }

    public static ResourcePlcBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ResourcePlcBOHandle newHandle = new ResourcePlcBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
