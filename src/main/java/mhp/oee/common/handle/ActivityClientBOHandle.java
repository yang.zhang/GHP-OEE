package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ActivityClientBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ActivityClientBO";

    public ActivityClientBOHandle(String activityClient) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activityClient.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(activityClient);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(activityClient);
            setValue(sb.toString());
        }
    }

    public String getActivityClient() {
        return getComponent(1);
    }

    public static ActivityClientBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ActivityClientBOHandle newHandle = new ActivityClientBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
