package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class UserGroupMemberBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "UserGroupMemberBO";

    public UserGroupMemberBOHandle(String activity) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activity.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(activity);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(activity);
            setValue(sb.toString());
        }
    }

    public UserGroupMemberBOHandle(String userGroupHandle, String userHandle) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(userGroupHandle).append(',').append(userHandle);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(2);
    }

    public String getUserGroup() {
        return getComponent(3);
    }

    public String getUser() {
        return getComponent(6);
    }

    public static UserGroupMemberBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        UserGroupMemberBOHandle newHandle = new UserGroupMemberBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
