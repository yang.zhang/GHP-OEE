package mhp.oee.common.handle;
/**
* @author zhangyun
* @E-mail: ZhangYun@ATLBattery.com
*/
public class ResrceBOHandle extends BOHandle {
	private static final String HANDLE_PREFIX = "ResourceBO";

    public ResrceBOHandle() {
        // Empty constructor for access to static declarations
    }

    public ResrceBOHandle(String handleString) {
    	super(handleString);
    }

    public ResrceBOHandle(String site, String resrce) {
    	super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(resrce);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getResrce() {
        return getComponent(2);
    }

    public static ResrceBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ResrceBOHandle newHandle = new ResrceBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
