package mhp.oee.common.handle;

public class UserBOHandle extends BOHandle {

    private static final String HANDLE_PREFIX = "UserBO";

    public UserBOHandle(String site, String user) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(user);
        setValue(sb.toString());
    }

    public UserBOHandle(String handle) {
        super(handle);
    }

    public String getSite() {
        return getComponent(1);
    }

    public String getUser() {
        return getComponent(2);
    }

    public static UserBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        if (handle instanceof UserBOHandle) {
            return (UserBOHandle) handle;
        }
        UserBOHandle newHandle = new UserBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
