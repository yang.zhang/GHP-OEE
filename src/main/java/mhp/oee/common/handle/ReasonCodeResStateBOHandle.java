package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ReasonCodeResStateBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ReasonCodeResourceStateBO";

    public ReasonCodeResStateBOHandle(String handleString) {
        super(handleString);
    }

    public ReasonCodeResStateBOHandle(String reasonCodeHandle, String resourceStateHandle, String strt) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(reasonCodeHandle);
        sb.append(',').append(resourceStateHandle);
        sb.append(',').append(strt);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(2);
    }

    public String getReasonCode() {
        return getComponent(3);
    }

    public String getResourceState() {
        return getComponent(6);
    }
    
    public String getStrt() {
        return getComponent(7);
    }

    public static ReasonCodeResStateBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ReasonCodeResStateBOHandle newHandle = new ReasonCodeResStateBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
