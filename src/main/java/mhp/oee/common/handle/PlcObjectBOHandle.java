package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class PlcObjectBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "PlcObjectBO";

    public PlcObjectBOHandle(String handleString) {
        super(handleString);
    }

    public PlcObjectBOHandle(String site, String plcObj) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(plcObj);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getPlcObject() {
        return getComponent(2);
    }

    public static PlcObjectBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        PlcObjectBOHandle newHandle = new PlcObjectBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
