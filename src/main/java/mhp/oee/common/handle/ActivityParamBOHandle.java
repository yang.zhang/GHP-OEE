package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ActivityParamBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ActivityParamBO";

    public ActivityParamBOHandle(String handleString) {
        super(handleString);
    }

    public ActivityParamBOHandle(String activityBOHandle, String paramID) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(activityBOHandle).append(',').append(paramID);
        setValue(sb.toString());
    }

    public String getActivity() {
        return getComponent(2);
    }

    public String getParamID() {
        return getComponent(3);
    }

    public static ActivityParamBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ActivityParamBOHandle newHandle = new ActivityParamBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
