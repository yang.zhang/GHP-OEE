package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class WorkCenterShiftBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "WorkCenterShiftBO";

    public WorkCenterShiftBOHandle(String activity) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activity.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(activity);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(activity);
            setValue(sb.toString());
        }
    }

    public WorkCenterShiftBOHandle(String workCenterHandle, String shift) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(workCenterHandle).append(',').append(shift);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(2);
    }

    public String getWorkCenter() {
        return getComponent(3);
    }

    public String getShift() {
        return getComponent(4);
    }

    public static WorkCenterShiftBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        WorkCenterShiftBOHandle newHandle = new WorkCenterShiftBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
