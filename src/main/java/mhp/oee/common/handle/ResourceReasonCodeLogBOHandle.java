package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ResourceReasonCodeLogBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ResourceReasonCodeLogBO";

    public ResourceReasonCodeLogBOHandle(String activity) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activity.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(activity);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(activity);
            setValue(sb.toString());
        }
    }

    public ResourceReasonCodeLogBOHandle(String site, String resourceHandle, String strt) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(resourceHandle).append(',').append(strt);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getResrce() {
        return getComponent(4);
    }

    public String getStrt() {
        return getComponent(5);
    }

    public static ResourceReasonCodeLogBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ResourceReasonCodeLogBOHandle newHandle = new ResourceReasonCodeLogBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
