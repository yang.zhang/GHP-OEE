package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ProductionOutputTargetBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ProductionOutputTargetBO";

    public ProductionOutputTargetBOHandle(String productionOutputTarget) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (productionOutputTarget.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(productionOutputTarget);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(productionOutputTarget);
            setValue(sb.toString());
        }
    }

    public ProductionOutputTargetBOHandle(String site, String item, String operation, String resrce, String targetDate,
            String shift) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(item).append(',').append(operation);
        sb.append(',').append(resrce).append(',').append(targetDate).append(',').append(shift);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getItem() {
        return getComponent(2);
    }

    public String getOperation() {
        return getComponent(3);
    }

    public String getResrce() {
        return getComponent(4);
    }

    public String getTargetDate() {
        return getComponent(5);
    }

    public String getShift() {
        return getComponent(6);
    }

    public static ProductionOutputTargetBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ProductionOutputTargetBOHandle newHandle = new ProductionOutputTargetBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
