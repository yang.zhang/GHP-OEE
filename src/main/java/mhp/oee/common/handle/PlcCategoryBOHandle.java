package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class PlcCategoryBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "PlcCategoryBO";

    public PlcCategoryBOHandle(String handleString) {
        super(handleString);
    }

    public PlcCategoryBOHandle(String site, String category) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(category);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getCatagory() {
        return getComponent(2);
    }

    public static PlcCategoryBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        PlcCategoryBOHandle newHandle = new PlcCategoryBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
