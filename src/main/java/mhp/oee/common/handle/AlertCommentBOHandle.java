package mhp.oee.common.handle;

public class AlertCommentBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "AlertCommentBO";
    
    public AlertCommentBOHandle(String handle){
        super(handle);
    }

    public AlertCommentBOHandle(String site, String commentCode) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(commentCode);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getCommentCode() {
        return getComponent(2);
    }


    public static AlertCommentBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        AlertCommentBOHandle newHandle = new AlertCommentBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
