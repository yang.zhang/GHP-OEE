package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ScheduledDownPlanBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ScheduledDownPlanBO";

    public ScheduledDownPlanBOHandle(String activity) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activity.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(activity);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(activity);
            setValue(sb.toString());
        }
    }

    public ScheduledDownPlanBOHandle(String site, String resourceHandle, String strt) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(resourceHandle).append(',').append(strt);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getResrce() {
        return getComponent(4);
    }

    public String getStrt() {
        return getComponent(5);
    }

    public static ScheduledDownPlanBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ScheduledDownPlanBOHandle newHandle = new ScheduledDownPlanBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
