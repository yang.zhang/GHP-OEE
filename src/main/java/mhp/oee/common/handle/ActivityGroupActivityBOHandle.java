package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ActivityGroupActivityBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ActivityGroupActivityBO";

    public ActivityGroupActivityBOHandle(String activityGroupBOHandle, String activityBOHandle) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(activityGroupBOHandle).append(',').append(activityBOHandle);
        setValue(sb.toString());
    }

    public ActivityGroupActivityBOHandle(String handleString) {
        super(handleString);
    }

    public String getActivityGroup() {
        return getComponent(2);
    }

    public String getActivity() {
        return getComponent(4);
    }

    public static ActivityGroupActivityBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ActivityGroupActivityBOHandle newHandle = new ActivityGroupActivityBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
