package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ResourceAlertLogBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ResourceAlertLogBO";

    public ResourceAlertLogBOHandle(String handString) {
        super(handString);
    }

    public static ResourceAlertLogBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ResourceAlertLogBOHandle newHandle = new ResourceAlertLogBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
