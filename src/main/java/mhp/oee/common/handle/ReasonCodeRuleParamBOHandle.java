package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ReasonCodeRuleParamBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ReasonCodeRuleParamBO";

    public ReasonCodeRuleParamBOHandle(String handleString) {
        super(handleString);
    }

    public ReasonCodeRuleParamBOHandle(String site, String ReasonCodeRuleHandle, String resourceType, String paramID) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(",").append(ReasonCodeRuleHandle).append(',')
                .append(resourceType).append(',').append(paramID);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getReasonCode() {
        return getComponent(6);
    }

    public String getPriority() {
        return getComponent(7);
    }

    public String getSubPriority() {
        return getComponent(8);
    }

    public String getStrt() {
        return getComponent(9);
    }

    public String getRule() {
        return getComponent(10);
    }

    public String getResourceType() {
        return getComponent(11);
    }

    public String getParamID() {
        return getComponent(12);
    }

    public static ReasonCodeRuleParamBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ReasonCodeRuleParamBOHandle newHandle = new ReasonCodeRuleParamBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
