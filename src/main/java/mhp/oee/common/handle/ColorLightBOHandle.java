package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ColorLightBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ColorLightBO";

    public ColorLightBOHandle(String resourceHandle) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (resourceHandle.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(resourceHandle);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(resourceHandle);
            setValue(sb.toString());
        }
    }

    @Override
    public String getSite() {
        return getComponent(2);
    }

    public String getResource() {
        return getComponent(3);
    }

    public static ColorLightBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ColorLightBOHandle newHandle = new ColorLightBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
