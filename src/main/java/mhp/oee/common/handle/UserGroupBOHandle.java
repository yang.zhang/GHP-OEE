package mhp.oee.common.handle;

public class UserGroupBOHandle extends BOHandle {

    private static final String HANDLE_PREFIX = "UserGroupBO";

    public UserGroupBOHandle(String site, String userGroup) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(userGroup);
        setValue(sb.toString());
    }

    public UserGroupBOHandle(String handle) {
        super(handle);
    }

    public String getSite() {
        return getComponent(1);
    }

    public String getUserGroup() {
        return getComponent(2);
    }

    public static UserGroupBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        if (handle instanceof UserGroupBOHandle) {
            return (UserGroupBOHandle) handle;
        }
        UserGroupBOHandle newHandle = new UserGroupBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
