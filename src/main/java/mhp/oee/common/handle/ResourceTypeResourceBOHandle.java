package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ResourceTypeResourceBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ResourceTypeResourceBO";

    public ResourceTypeResourceBOHandle(String activity) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activity.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(activity);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(activity);
            setValue(sb.toString());
        }
    }

    public ResourceTypeResourceBOHandle(String resourceTypeHandle, String resourceHandle) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(resourceTypeHandle).append(',').append(resourceHandle);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(2);
    }

    public String getResourceType() {
        return getComponent(3);
    }

    public String getResrce() {
        return getComponent(6);
    }

    public static ResourceTypeResourceBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ResourceTypeResourceBOHandle newHandle = new ResourceTypeResourceBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
