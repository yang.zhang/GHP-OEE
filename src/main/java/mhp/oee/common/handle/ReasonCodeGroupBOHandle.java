package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ReasonCodeGroupBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ReasonCodeGroupBO";

    public ReasonCodeGroupBOHandle(String handleString) {
        super(handleString);
    }

    public ReasonCodeGroupBOHandle(String site, String reasonCodeGroup) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site);
        sb.append(',').append(reasonCodeGroup);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getReasonCodeGroup() {
        return getComponent(2);
    }

    public static ReasonCodeGroupBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ReasonCodeGroupBOHandle newHandle = new ReasonCodeGroupBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
