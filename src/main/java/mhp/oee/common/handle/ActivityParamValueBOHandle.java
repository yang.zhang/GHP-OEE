package mhp.oee.common.handle;

/**
 * Created by LinZuK on 2016/7/25.
 */
public class ActivityParamValueBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ActivityParamValueBO";

    public ActivityParamValueBOHandle(String site, String activityBo, String paramId) {
        super();
        setValue(site, activityBo, paramId);
    }

    public ActivityParamValueBOHandle(String handle) {
        super(handle);
    }

    public void setValue(String site, String activityBo, String paramId) {
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',');
        sb.append(activityBo).append(',').append(paramId);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getItem() {
        return getComponent(2);
    }

    @Override
    public String getRevision() {
        return getComponent(3);
    }

    public static ItemBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ItemBOHandle newHandle = new ItemBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
