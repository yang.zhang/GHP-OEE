package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ResourceTimeLogBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ResourceTimeLogBO";

    public ResourceTimeLogBOHandle(String activity) {
        super();
        // If the input is a handle in string form deal will it differently.
        if (activity.indexOf(HANDLE_PREFIX + ":") >= 0) {
            setValue(activity);
        } else {
            // If not already a handle, then build one.
            StringBuilder sb = new StringBuilder();
            sb.append(HANDLE_PREFIX).append(':').append(activity);
            setValue(sb.toString());
        }
    }

    public ResourceTimeLogBOHandle(String site, String resourceRef, String strt) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(resourceRef).append(',').append(strt);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getResrce() {
        return getComponent(4);
    }

    public String getStrt() {
        return getComponent(5);
    }

    public static ResourceTimeLogBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ResourceTimeLogBOHandle newHandle = new ResourceTimeLogBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
