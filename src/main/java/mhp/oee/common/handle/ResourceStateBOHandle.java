package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ResourceStateBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ResourceStateBO";

    public ResourceStateBOHandle(String handleString) {
        super(handleString);
    }

    public ResourceStateBOHandle(String site, String state) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site).append(',').append(state);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getState() {
        return getComponent(2);
    }

    public String getResourceState() {
        return getComponent(1);
    }

    public static ResourceStateBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ResourceStateBOHandle newHandle = new ResourceStateBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
