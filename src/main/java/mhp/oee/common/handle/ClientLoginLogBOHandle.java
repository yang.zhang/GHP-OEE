package mhp.oee.common.handle;

public class ClientLoginLogBOHandle extends BOHandle {

    private static final String HANDLE_PREFIX = "ClientLoginLogBO";

    public ClientLoginLogBOHandle(String userRef, String resrceRef, String strt) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(userRef).append(',').append(resrceRef).append(',').append(strt);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(2);
    }

    public String getUser() {
        return getComponent(3);
    }

    public String getResrce() {
        return getComponent(6);
    }

    public String getStrt() {
        return getComponent(7);
    }

}
