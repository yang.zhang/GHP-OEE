package mhp.oee.common.handle;

/**
 * Created by LinZuK on 2016/9/8.
 */
public class JobLogBOHandle extends BOHandle {

    private static final String HANDLE_PREFIX = "JobLogBO";

    // JobLogBO:<JOB>,<STRT>
    public JobLogBOHandle(String job, String strt, String serverNode) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(":").append(job)
                .append(',').append(strt)
                .append(',').append(serverNode);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return null;
    }

}
