package mhp.oee.common.handle;

public class ActivityPermBOHandle extends BOHandle {

    private static final String HANDLE_PREFIX = "ActivityPermBO";

    public ActivityPermBOHandle(UserGroupBOHandle userGroupBOHandle, ActivityBOHandle activityBOHandle) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(userGroupBOHandle).append(',').append(activityBOHandle);
        setValue(sb.toString());
    }

    public ActivityPermBOHandle(String userGroupHandle, String activityHandle) {
        super();
        StringBuilder sb = new StringBuilder();

        sb.append(HANDLE_PREFIX).append(':').append(userGroupHandle).append(',').append(activityHandle);
        setValue(sb.toString());
    }

    public ActivityPermBOHandle(String handleString) {
        super(handleString);
    }

    @Override
    public String getSite() {
        return getComponent(2);
    }

    public String getUserGroup() {
        return getComponent(3);
    }

    public String getActivity() {
        return getComponent(6);
    }

    public static ActivityPermBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ActivityPermBOHandle newHandle = new ActivityPermBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }

}
