package mhp.oee.common.handle;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ReasonCodeBOHandle extends BOHandle {
    private static final String HANDLE_PREFIX = "ReasonCodeBO";

    public ReasonCodeBOHandle(String handleString) {
        super(handleString);
    }

    public ReasonCodeBOHandle(String site, String reasonCode) {
        super();
        StringBuilder sb = new StringBuilder();
        sb.append(HANDLE_PREFIX).append(':').append(site);
        sb.append(',').append(reasonCode);
        setValue(sb.toString());
    }

    @Override
    public String getSite() {
        return getComponent(1);
    }

    public String getReasonCode() {
        return getComponent(2);
    }

    public static ReasonCodeBOHandle convert(BOHandle handle) {
        if (handle == null) {
            return null;
        }
        ReasonCodeBOHandle newHandle = new ReasonCodeBOHandle(handle.getValue());
        if (!HANDLE_PREFIX.equals(newHandle.getComponent(0))) {
            throw new ClassCastException(handle.getValue());
        }
        return newHandle;
    }
}
