package mhp.oee.common.handle;

import java.util.List;
import java.util.Vector;

import mhp.oee.utils.Utils;

public class BOHandle implements Comparable<BOHandle> {

    /** This is the string value for the handle */
    protected String value;

    /** This vector holds the list of handle components */
    protected Vector<String> components;

    ////////////////////////////////////////////////////////////////////
    // Supported Handle Types
    ////////////////////////////////////////////////////////////////////
    public static final int UNDEFINED_HANDLE = -1;
    public static final int OPERATION_HANDLE = 0;
    public static final int ITEM_HANDLE = 1;
    public static final int RESOURCE_HANDLE = 2;
    public static final int RESOURCE_TYPE_HANDLE = 3;
    public static final int WORK_CENTER_HANDLE = 4;
//    public static final int SHOP_ORDER_HANDLE = 5;
//    public static final int ROUTER_HANDLE = 6;
//    public static final int ROUTER_STEP_HANDLE = 7;
//    public static final int SFC_HANDLE = 8;
//    public static final int INCIDENT_NUMBER_HANDLE = 9;
//    public static final int DISPOSITION_FUNCTION_HANDLE = 10;
//    public static final int ITEM_GROUP_HANDLE = 11;
//    public static final int CUSTOMER_ORDER_HANDLE = 12;
//    public static final int SUBSTEP_HANDLE = 13;

    public static final char DELIM_CHAR = ',';
    public static final char[] ILLEGAL_CHARS = new char[] { ':', ',' };

    /**
     * Basic constructor. Used for serialization purposes mainly.
     *
     */
    public BOHandle() {
    }

    /**
     * Constructor for string version of the handles
     *
     * @param value
     *            The full handle value (e.g. "OperationBO:MAIN,ASSEMBLE,A")
     *
     */
    public BOHandle(String value) {
        this.value = value;
    }

    /**
     * Checks for illegal characters in handles to avoid incorrect handle
     * parsing Should be promoted to BOHandle
     * 
     * @param component
     * @throws Exception
     */
    public static void checkIllegalCharacters(String component) throws Exception {
        for (int i = 0; i < ILLEGAL_CHARS.length; i++) {
            if (component.indexOf(ILLEGAL_CHARS[i]) > -1) {
                throw new Exception(component);
            }
        }
    }

    /**
     * A utility to verify that BOHandle can be converted to our type. If the
     * input inHandle is null, verification does not fail.
     */
    // public void verify() throws BasicBOBeanException {
    // String value = getValue() == null ? "" : getValue();
    // //
    // // The next line is subject to change basic on the structure of handle
    // //
    // if (!value.startsWith(_prefix))
    // throw new BasicBOBeanException(10001, "Invalid inHandle can not be
    // converted to GenericBOHandle");
    // }

    /**
     * Returns the handle's current full string value.
     * 
     * @return The handles current full string value.
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the handle's current full string value.
     * 
     * @param value
     *            The handles current full string value.
     */
    public void setValue(String value) {
        this.value = value;
        components = null; // Get rid of parsed list of handle components
    }

    /**
     * Returns a single component of the handle
     * 
     * @param comp
     *            The component. 0=Handle type (e.g. "OperationBO"), 1= first
     *            component (e.g. site)
     * @return The handle component's value.
     */
    public String getComponent(int comp) {
        if (value == null) // No component if there is no handle
            return null;
        if (components == null) { // has it been parsed into components yet?
            parseValue(); // No, do it now
        }
        if (components.size() > 0 && comp < components.size()) {
            return (String) components.elementAt(comp);
        }
        return null;
    }

    /**
     * Parses the current handle string into its components.
     */
    public void parseValue() {
        if (value == null) {
            return;
        }
        components = Utils.tokenize(value, ":,");
    }

    /**
     * Return the components of this handle.
     * 
     * @return List of component values.
     */
    public List<String> getComponents() {
        if (components == null) {
            parseValue();
        }
        return components;
    }

    /**
     * Returns the handle's current full string value.
     * 
     * @return The handles current full string value.
     */
    @Override
    public String toString() {
        return value;
    }

    /**
     * Compares two handle and returns true if they are the same (value). This
     * is the variant to handle blind compares. If the handle is not a BOHandle,
     * then returns false.
     * 
     * @param handle
     *            The handle to compare to.
     */
    @Override
    public boolean equals(Object handle) {
        if (handle == null || value == null) {
            return false;
        }
        if (handle instanceof BOHandle) {
            return value.equals(((BOHandle) handle).getValue());
        }
        return false;
    }

    /**
     * Compare two entries (for Sorting). returns: +1 if this > o 0 if this == o
     * -1 if this < o Assumes null comes before an non-null value
     */
    @Override
    public int compareTo(BOHandle o) {
        String oValue = ((BOHandle) o).value;
        // handle the null cases
        if (oValue == null && value == null) {
            return 0;
        }
        if (value == null) {
            return -1;
        }
        return value.compareTo(oValue);
    }

    /** Returns a hash code value for the BOHandle. */
    @Override
    public int hashCode() {
        if (value == null) {
            return -1;
        } else {
            return value.hashCode();
        }
    }

    /**
     * Method returns type of entity Handle represents
     * 
     * @return Type of entity Handle represents
     */
    public int getHandleType() {
        return -1;
    }

    /**
     * Method returns main value of an entity handle represents (for example
     * Operation in case of OperationBOHandle)
     * 
     * @return Main value of an entity handle represents
     */
    public String getEntityValue() {
        return null;
    }

    /**
     * Method returns revision of an entity if exists
     * 
     * @return Revision of an entity if exists
     */
    public String getRevision() {
        // initGBO();
        // return _gboHandle.getRevision();
        return null;
    }

    public String getEntityValueWithRevision() {
        // initGBO();
        // return _gboHandle.getEntityValueWithRevision();
        return null;
    }

    /**
     * Method returns Site if it presents in Handle
     * 
     * @return Site if it presents in Handle
     */
    public String getSite() {
        // initGBO();
        // return _gboHandle.getSite();
        return null;
    }

    /**
     * Method returns default label of an entity it represents. For example
     * "operation.default.LABEL" for OperationBOHandle
     * 
     * @return Default label of an entity it represents
     */
    public String getEntityDefaultLabel() {
        // initGBO();
        // return _gboHandle.getEntityDefaultLabel();
        return null;
    }

    /**
     * Method returns default label of an entity with revision it represents.
     * For example "operationRev.default.LABEL" = "Operation/Rev" for
     * OperationBOHandle
     * 
     * @return Default label of an entity it represents
     */
    public String getEntityWithRevisionDefaultLabel() {
        // initGBO();
        // return _gboHandle.getEntityWithRevisionDefaultLabel();
        return null;
    }

    /**
     * BOHandle information
     * 
     * @return information about BOHandle
     */
//    public String info() {
//        String info = "";
//        info += "Handle = " + this.value + "\n";
//        info += "Handle Type = " + getHandleType() + "\n";
//        info += "Site = " + getSite() + "\n";
//        info += "Entity Value = " + getEntityValue() + "\n";
//        info += "Revision = " + getRevision() + "\n";
//        info += "Entity Default Label = " + getEntityDefaultLabel() + "\n";
//        return info;
//    }

}
