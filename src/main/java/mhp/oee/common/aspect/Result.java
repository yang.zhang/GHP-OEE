package mhp.oee.common.aspect;

import mhp.oee.common.exception.ServiceErrorException;

import java.io.Serializable;

/** 规定 返回数据 格式
 * Created by DingLJ on 2017/4/17 017.
 */
public class Result<T> implements Serializable {

    private boolean success = false;
    private Integer code;//错误码
    private String msg;//错误信息
    private T data;//数据

    public Result(){

    }

    public Result(ServiceErrorException e){
        if(e == null){
            return ;
        }
        this.code = e.getCode();
        this.msg = e.getLocalizedMessage();
    }

    public void ok() {
        this.success = true;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
