package mhp.oee.common.cookie;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by DingLJ on 2017/5/17 017.
 */
public class CookieSessionUtil {


    public static final String UPPC_IP_KEY = "qwetyuiop1234567890asdfghjklzxcvbnm";
    public static final String UPPC_RESOURCE_KEY = "asdghjklxcvbnm23456789sdfghjertyuicvbnm";


    /***
     * 获取cookie值
     * @param name
     * @param request
     * @return
     */
    public static String getCookieByName(String name, HttpServletRequest request) {
        String result = "";
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie x : cookies) {
                if (x != null && x.getName().equalsIgnoreCase(name)) {
                    String value = x.getValue();
                    try {
                        result = URLDecoder.decode(value, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        result = "";
                    }
                }
            }
        }
        return result;
    }

    /***
     * 设置当前用户的浏览器的cookie值
     * @param name      cookie的名字
     * @param value     cookies的值
     * @param response  cookies的响应
     * @param path      cookie设置的路劲 默认是根目录
     */
    public static void setCookieByName(String name, String value, HttpServletResponse response, String path) {
        String username = new String();
        try {
            username = URLEncoder.encode("jinyuan", "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Cookie c = new Cookie("username", username);
        c.setMaxAge(60 * 60 * 50);
        if (path == null || path.equalsIgnoreCase("")) {
            path = "/";
        }
        c.setPath(path);
        response.addCookie(c);
    }

    /***
     * 设置当前用户的浏览器的cookie值
     * @param name      cookie的名字
     * @param value     cookies的值
     * @param response  cookies的响应
     */
    public static void setCookieByName(String name, String value, HttpServletResponse response) {
        String username = new String();
        try {
            username = URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Cookie c = new Cookie(name, username);
        c.setMaxAge(60 * 60 * 50);
        c.setPath("/");
        response.addCookie(c);
    }

    /**
     * 清空某个cookie的值
     *
     * @param name
     * @param response
     */
    public static void clearCookieByName(String name, HttpServletResponse response) {
        Cookie c = new Cookie(name, null);
        c.setMaxAge(0);
        c.setPath("/");
        response.addCookie(c);
    }

    public static void clearSession(HttpServletRequest request, String name) {
        if (null != request.getSession()) {
            request.getSession().removeAttribute(name);
            request.getSession().invalidate();
        }
    }

}
