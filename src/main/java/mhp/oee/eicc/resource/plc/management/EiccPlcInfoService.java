package mhp.oee.eicc.resource.plc.management;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mhp.oee.common.security.ContextHelper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ResourcePlcInfoComponent;
import mhp.oee.component.production.ResourceTimeLogComponent;
import mhp.oee.po.gen.ResourceTimeLogPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourcePlcInfoVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class EiccPlcInfoService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private ResourcePlcInfoComponent resourcePlcInfoComponent;
    
    @Autowired
    private ResourceTimeLogComponent resourceTimeLogComponent;

    public List<ResourcePlcInfoVO> readPcIp() {
        return resourcePlcInfoComponent.getAllPcIp();
    }
    
    /**
     * 
     * @param list
     * @throws BusinessException
     * @throws ParseException
     */
    public void changeResourceTimeLog(List<ResourcePlcInfoVO> list) throws BusinessException, ParseException {
        List<String> pcIps = formatPcIps(list);
        // 1 get resource_bo by pc_ip
        List<ResourcePlcInfoVO> pos = resourcePlcInfoComponent.getResourcePlcInfoPOByPcIps(pcIps);
        for (ResourcePlcInfoVO vo : pos) {
            // 2 get resource time log by resource_bo
            List<ResourceTimeLogPO> resourceTimeLogPOs = resourceTimeLogComponent.readTimeLogRecord(vo.getResourceBo());
            // 3 update resource time log
            boolean isAddTimeLog = updateResourceTimeLog(resourceTimeLogPOs);
            if (isAddTimeLog) {
                // 4 add resource time log
                createResourceTimeLog(vo.getSite(), vo.getResourceBo());
            }
        }
    }
    
    /**
     * 
     * @param site
     * @param resourceBo
     * @throws BusinessException
     */
    public void createResourceTimeLog (String site, String resourceBo) throws BusinessException {
        Date now = new Date();
        ResourceTimeLogPO resourceTimeLogPO = new ResourceTimeLogPO();
        resourceTimeLogPO.setSite(site);
        resourceTimeLogPO.setResourceBo(resourceBo);
        resourceTimeLogPO.setStrt(Utils.datetimeMsToStr(now));
        resourceTimeLogPO.setStartDateTime(now);
        resourceTimeLogPO.setResourceState("D");
        resourceTimeLogComponent.createResourceTimeLogEicc(resourceTimeLogPO);
    }
    
    /**
     * 
     * @param resourceTimeLogPOs
     * @return
     * @throws BusinessException
     * @throws ParseException
     */
    public boolean updateResourceTimeLog (List<ResourceTimeLogPO> resourceTimeLogPOs) throws BusinessException, ParseException {
        Date now = new Date();
        boolean flag = false;
        if (Utils.isEmpty(resourceTimeLogPOs)) {
            flag = true;
        } else {
            ResourceTimeLogPO po = resourceTimeLogPOs.get(0);
            if (!po.getResourceState().equals("D") && po.getEndDateTime() == null) {
                po.setEndDateTime(now);
                Date startDt = Utils.strToDatetimeMs(po.getStrt());
                long diff = now.getTime() - startDt.getTime(); 
                po.setElapsedTime(new BigDecimal(diff));
                resourceTimeLogComponent.updateResourceTimeLog(po, ContextHelper.getContext().getUser());
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 
     * @param list
     * @return
     */
    public List<String> formatPcIps (List<ResourcePlcInfoVO> list) {
        if (Utils.isEmpty(list)) {
            return null;
        }
        List<String> pcIps = new ArrayList<String>();
        for (ResourcePlcInfoVO resourcePlcInfoVO : list) {
           pcIps.add(resourcePlcInfoVO.getPcIp());
        }
        return pcIps;
    }
}
