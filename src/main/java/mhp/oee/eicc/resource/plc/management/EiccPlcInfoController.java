package mhp.oee.eicc.resource.plc.management;

import java.text.ParseException;
import java.util.List;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ResourcePlcInfoVO;
import mhp.oee.web.Constants;

@RestController
public class EiccPlcInfoController {

    @InjectableLogger
    private static Logger logger;
    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    EiccPlcInfoService eiccPlcInfoService;

    @RequestMapping(value = "/api/v1/eicc/pc_ip",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourcePlcInfoVO>> readPcIp() {
        String trace = String.format("[EiccPlcInfoController.readPcIp]");
        logger.debug(">> " + trace);
        List<ResourcePlcInfoVO> vos = eiccPlcInfoService.readPcIp();
        logger.debug("<< [EiccPlcInfoController.readPcIp]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/api/v1/eicc/change_resource_time_log",
            method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeResourceTimeLog(@RequestBody List<ResourcePlcInfoVO> pcIps) {
        String trace = String.format("[EiccPlcInfoController.changeResourceTimeLog] request : " + gson.toJson(pcIps) );
        logger.debug(">> " + trace);
        try {
            eiccPlcInfoService.changeResourceTimeLog(pcIps);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("EiccPlcInfoController.changeResourceTimeLog() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("EiccPlcInfoController.changeResourceTimeLog() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
       }
        logger.debug("<< [EiccPlcInfoController.changeResourceTimeLog]  response : void");
        return ResponseEntity.ok(null);
    }
    
}
