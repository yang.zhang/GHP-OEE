package mhp.oee.config;

/**
 * Created by i069533 on 12/04/2017.
 */
public class JobDispatchConfig {

    private Integer serverNodes;


    public JobDispatchConfig() {

    }

    public Integer getServerNodes() {
        return serverNodes;
    }

    public void setServerNodes(Integer serverNodes) {
        this.serverNodes = serverNodes;
    }

    @Override
    public String toString() {
        return "JobDispatchConfig{" +
                "serverNodes=" + serverNodes +
                '}';
    }
}
