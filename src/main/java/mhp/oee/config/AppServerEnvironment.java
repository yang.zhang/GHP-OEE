package mhp.oee.config;

import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertySource;
import org.springframework.web.context.support.StandardServletEnvironment;

public class AppServerEnvironment {

    public static final String SERVLET_CONTEXT_CLASS_NAME_NETWEAVER = "com.sap.engine.services.servlets_jsp.server.application.ServletContextImpl";
    public static final String SERVLET_CONTEXT_CLASS_NAME_TOMCAT = "org.apache.catalina.quartzutils.ApplicationContextFacade";

    private static int appServerType = AppServerType.INIT;

    public static int initAppServerType(Environment environment) {
        if (appServerType == AppServerType.INIT) {
            // determine the app server type
            if (environment instanceof StandardServletEnvironment) {
                StandardServletEnvironment sse = (StandardServletEnvironment) environment;
                MutablePropertySources set = sse.getPropertySources();
                PropertySource<?> ps = set.get(StandardServletEnvironment.SERVLET_CONTEXT_PROPERTY_SOURCE_NAME);
                if (ps != null && ps.getSource() != null) {
                    String source = ps.getSource().toString();
                    if (source.startsWith(SERVLET_CONTEXT_CLASS_NAME_NETWEAVER)) {
                        appServerType = AppServerType.NETWEAVER;
                    } else if (source.startsWith(SERVLET_CONTEXT_CLASS_NAME_TOMCAT)) {
                        appServerType = AppServerType.TOMCAT;
                    } else {
                        appServerType = AppServerType.UNKOWN;
                    }
                } // end-if (ps != null && ps.getSource() != null)
            } // end-if (environment instanceof StandardServletEnvironment)
        }
        return appServerType;
    }

    public static int getAppServerType() {
        return appServerType;
    }

    static public class AppServerType {
        public static final int INIT = -1;
        public static final int UNKOWN = 0;
        public static final int NETWEAVER = 1;
        public static final int TOMCAT = 2;
    }

}
