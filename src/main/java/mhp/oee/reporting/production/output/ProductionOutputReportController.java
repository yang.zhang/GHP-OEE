package mhp.oee.reporting.production.output;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.InvalidInputParamException;
import mhp.oee.common.exception.InvalidParamException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ProductionOutputFirstAndFollowUpConditionVO;
import mhp.oee.vo.ProductionOutputFirstAndFollowUpVO;
import mhp.oee.web.Constants;

@RestController
public class ProductionOutputReportController {
    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ProductionOutputReportService productionOutputReportService;

    @RequestMapping(value = "/report/site/{site}/production_output_report", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ProductionOutputFirstAndFollowUpVO>> readProductionOutputReport(
            @PathVariable("site") String site,
            @RequestBody ProductionOutputReportVO vo) {

        String[] workArea = vo.getWorkArea();
        String[] lineArea = vo.getLineArea();
        String[] resrce = vo.getResrce();
        String[] item = vo.getItem();
        String[] model = vo.getModel();
        Character byTimeType = vo.getByTimeType();
        Character byConditionType = vo.getByConditionType();
        String startDateTime = vo.getStartDateTime();
        String endDateTime = vo.getEndDateTime();

        String trace = String.format("[ProductionOutputReportController.readProductionOutputReport] request : site=%s, workArea=%s, lineArea=%s, resrce=%s, item=%s, model=%s, byTimeType=%s, byConditionType=%s, startDateTime=%s, endDateTime=%s",
                site, workArea, lineArea, resrce, item, model, byTimeType, byConditionType, startDateTime, endDateTime);
        logger.debug(">> " + trace);
        List<ProductionOutputFirstAndFollowUpVO> vos = null;
        try {
            ProductionOutputFirstAndFollowUpConditionVO voObject = getObject(site, workArea, lineArea, resrce, item, model, byTimeType, byConditionType, startDateTime, endDateTime);
            vos = productionOutputReportService.selectProductionOutputReport(voObject);
            DecimalFormat df = new DecimalFormat("0.0");
            for(int i=0;i<vos.size();i++){
                if(!Utils.isEmpty(vos.get(i).getQtySum())){
                    vos.get(i).setGridQtySum(Utils.fmtMicrometer(df.format(Double.valueOf(vos.get(i).getQtySum()))));
                }
                if(!Utils.isEmpty(vos.get(i).getQtyYield())){
                    vos.get(i).setGridQtyYield(Utils.fmtMicrometer(df.format(Double.valueOf(vos.get(i).getQtyYield()))));
                }
                if(!Utils.isEmpty(vos.get(i).getQtyScrap())){
                    vos.get(i).setGridQtyScrap(Utils.fmtMicrometer(df.format(Double.valueOf(vos.get(i).getQtyScrap()))));
                }
            }
        } catch (InvalidParamException e) {
            ExceptionVO exceptionVO = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ProductionOutputReportController.readProductionOutputReport() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        } catch (InvalidInputParamException e) {
            ExceptionVO exceptionVO = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ProductionOutputReportController.readProductionOutputReport() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        } catch (ParseException e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< [ProductionOutputReportController.readProductionOutputReport] response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    /**
     * export Excel
     * @param site
     * @param response
     * @param request
     * @return
     */
    @RequestMapping(value = "/report/site/{site}/export_excel", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ProductionOutputFirstAndFollowUpVO>> exportExcel(
            @PathVariable("site") String site,
            @RequestBody ProductionOutputReportVO vo,
            HttpServletResponse response, HttpServletRequest request) {

        String[] workArea = vo.getWorkArea();
        String[] lineArea = vo.getLineArea();
        String[] resrce = vo.getResrce();
        String[] item = vo.getItem();
        String[] model = vo.getModel();
        Character byTimeType = vo.getByTimeType();
        Character byConditionType = vo.getByConditionType();
        String startDateTime = vo.getStartDateTime();
        String endDateTime = vo.getEndDateTime();


        String trace = String.format("[ProductionOutputReportController.exportExcel] request : site=%s, workArea=%s, lineArea=%s, resrce=%s, item=%s, model=%s, byTimeType=%s, byConditionType=%s, startDateTime=%s, endDateTime=%s",
                site, workArea, lineArea, resrce, item, model, byTimeType, byConditionType, startDateTime, endDateTime);
        logger.debug(">> " + trace);
        try {
            String filePath = "download.xlsx";
            ProductionOutputFirstAndFollowUpConditionVO voObject = getObject(site, workArea, lineArea, resrce, item, model, byTimeType, byConditionType, startDateTime, endDateTime);
            List<ProductionOutputFirstAndFollowUpVO> vos = productionOutputReportService.selectProductionOutputReport(voObject);
            productionOutputReportService.commitExportExcel(vos, response, filePath, byConditionType, byTimeType);
            this.generateDownloadFile(response, filePath);

        } catch (FileNotFoundException e) {
            ExceptionVO exceptionVO = new ExceptionVO(ErrorCodeEnum.FILE_NOT_FOUND.getErrorCode(), e.getMessage(), null);
            logger.error("exportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        } catch (IOException e) {
            ExceptionVO exceptionVO = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
            logger.error("exportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        } catch (InvalidParamException e) {
            ExceptionVO exceptionVO = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ProductionOutputReportController.exportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        } catch (InvalidInputParamException e) {
            ExceptionVO exceptionVO = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ProductionOutputReportController.exportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        } catch (ParseException e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< [ProductionOutputReportController.exportExcel] response : void");
        return ResponseEntity.ok(null);
    }
    /**
     * generate Download File
     * @param response
     * @param filePath
     * @throws IOException
     */
    private void generateDownloadFile(HttpServletResponse response, String filePath) throws IOException {
        File file = new File(filePath);
        OutputStream out = response.getOutputStream();
        response.setHeader("Content-disposition", "attachment; filename=" + "download.xlsx");

        byte[] buffer = new byte[8192]; // use bigger if you want
        int length = 0;

        FileInputStream in = new FileInputStream(file);

        while ((length = in.read(buffer)) > 0) {
            out.write(buffer, 0, length);
        }
        in.close();
        out.close();
    }

    /**
     * get ProductionOutputFirstAndFollowUpConditionVO Object
     * @param site
     * @param workAreas
     * @param lineAreas
     * @param resrces
     * @param items
     * @param models
     * @param byTimeType
     * @param byConditionType
     * @param startDateTime
     * @param endDateTime
     * @return
     * @throws InvalidParamException
     * @throws InvalidInputParamException
     */
    private ProductionOutputFirstAndFollowUpConditionVO getObject(String site, String[] workAreas, String[] lineAreas,
            String[] resrces, String[] items, String[] models, Character byTimeType, Character byConditionType,
            String startDateTime, String endDateTime) throws InvalidParamException, InvalidInputParamException {
        // 1 Join together IN SQL
        String workArea = Utils.formatInParams(ConditionType.WORK_AREA.getName(), workAreas);
        String lineArea = Utils.formatInParams(ConditionType.LINE_AREA.getName(), lineAreas);
        String resrce = Utils.formatInParams(ConditionType.RESRCE.getName(), resrces);
        String item = Utils.formatInParams(ConditionType.ITEM.getName(), items);
        String model = Utils.formatInParams(ConditionType.MODEL.getName(), models);
        String param = " WORK_AREA, WORK_AREA_DES, MODEL, ITEM, ITEM_DES, BY_TIME " ;
        // 2 check condition type
        switch (byConditionType) {
        case 'W':
            if (Utils.isEmpty(workArea)) {
                throw new InvalidParamException("WorkArea", gson.toJson(workArea));
            }
            param = " WORK_AREA, WORK_AREA_DES, MODEL, ITEM, ITEM_DES, BY_TIME " ;
            break;
        case 'L':
            if (Utils.isEmpty(lineArea)) {
                throw new InvalidParamException("LineArea", gson.toJson(lineArea));
            }
            param = " WORK_AREA, WORK_AREA_DES, LINE_AREA, LINE_AREA_DES, MODEL, ITEM, ITEM_DES, BY_TIME " ;
            break;
        case 'R':
            if (Utils.isEmpty(resrce)) {
                throw new InvalidParamException("Resrce", gson.toJson(resrce));
            }
            param = " WORK_AREA, WORK_AREA_DES, LINE_AREA, LINE_AREA_DES, RESRCE, RESRCE_DES, MODEL, ITEM, ITEM_DES, BY_TIME " ;
            break;
        case 'I':
            if (Utils.isEmpty(item)) {
                throw new InvalidParamException("Item", gson.toJson(item));
            }
            param = " MODEL, ITEM, ITEM_DES, BY_TIME " ;
            break;
        default:
            throw new InvalidInputParamException("ByCondtionType");
        }

        // 2 check time type
        if (!byTimeType.equals('M') && !byTimeType.equals('W') && !byTimeType.equals('D') && !byTimeType.equals('S')) {
            throw new InvalidInputParamException("ByTimeType");
        }

        ProductionOutputFirstAndFollowUpConditionVO vo = new ProductionOutputFirstAndFollowUpConditionVO();
        vo.setWorkArea(workArea);
        vo.setLineArea(lineArea);
        vo.setResrce(resrce);
        vo.setModel(model);
        vo.setItem(item);
        vo.setByTimeType(byTimeType);
        vo.setByConditionType(byConditionType);
        vo.setSite(site);
        vo.setStartDateTime(startDateTime);
        vo.setEndDateTime(endDateTime);
        vo.setParam(param);
        return vo;
    }

}
