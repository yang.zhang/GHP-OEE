package mhp.oee.reporting.production.output;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import mhp.oee.utils.Utils;

import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.InvalidInputParamException;
import mhp.oee.common.exception.InvalidParamException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.reporting.component.ProductionOutputReportComponent;
import mhp.oee.vo.ProductionOutputFirstAndFollowUpConditionVO;
import mhp.oee.vo.ProductionOutputFirstAndFollowUpVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class ProductionOutputReportService {
    @InjectableLogger
    private static Logger logger;

    @Autowired
    private ProductionOutputReportComponent productionOutputReportComponent;

    public List<ProductionOutputFirstAndFollowUpVO> selectProductionOutputReport(
            ProductionOutputFirstAndFollowUpConditionVO vo) throws ParseException {
        vo.setStartDateTime(Utils.parseReportDateDay(vo.getStartDateTime()));
        vo.setEndDateTime(Utils.parseReportDateDay(vo.getEndDateTime()));
        return productionOutputReportComponent.selectProductionOutputReport(vo);
    }
    
    public void commitExportExcel(List<ProductionOutputFirstAndFollowUpVO> pos, HttpServletResponse response,
            String filePath, Character byConditionType, Character byTimeType) throws FileNotFoundException, IOException {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFCellStyle style = workbook.createCellStyle();
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%"));
        XSSFSheet sheet = workbook.createSheet(WorkbookCell.SHEET_NAME.getName());
        XSSFRow row = sheet.createRow(0);
        DecimalFormat df = new DecimalFormat("0.0");
        List<String> cellList = new ArrayList<String>();
        if (byConditionType.equals('W') || byConditionType.equals('L') || byConditionType.equals('R')) {
           cellList.add(WorkbookCell.WORK_AREA.getName());
           cellList.add(WorkbookCell.WORK_AREA_DES.getName());
        }
        if (byConditionType.equals('L') || byConditionType.equals('R')) {
            cellList.add(WorkbookCell.LINE_AREA.getName());
            cellList.add(WorkbookCell.LINE_AREA_DES.getName());
        }
        if (byConditionType.equals('R')) {
            cellList.add(WorkbookCell.RESRCE.getName());
            cellList.add(WorkbookCell.RESRCE_DES.getName());
        }
        cellList.add(WorkbookCell.MODEL.getName());
        cellList.add(WorkbookCell.ITEM.getName());
        switch (byTimeType) {
            case 'M':
                cellList.add(WorkbookCell.BY_TIME_TYPE_M.getName());
                break;
            case 'W':
                cellList.add(WorkbookCell.BY_TIME_TYPE_W.getName());
                break;
            case 'D':
                cellList.add(WorkbookCell.BY_TIME_TYPE_D.getName());
                break;
            case 'S':
                cellList.add(WorkbookCell.BY_TIME_TYPE_S.getName());
                break;
        }
        cellList.add(WorkbookCell.QTY_SUM.getName());
        cellList.add(WorkbookCell.QTY_YIELD.getName());
        cellList.add(WorkbookCell.QTY_SCRAP.getName());
        cellList.add(WorkbookCell.FIRST_CAL.getName());
        
        XSSFCell cell = null;
        for (int i = 0; i < cellList.size(); i++) {
            cell = row.createCell((short) i);
            cell.setCellValue(cellList.get(i));
        }

        for (int i = 0; i < pos.size(); i++) {
            XSSFRow addRow = sheet.createRow(i + 1);
            int num = 0;
            if (byConditionType.equals('W') || byConditionType.equals('L') || byConditionType.equals('R')) {
                addRow.createCell(num++).setCellValue(pos.get(i).getWorkArea());
                addRow.createCell(num++).setCellValue(pos.get(i).getWorkAreaDes());
            }
            if (byConditionType.equals('L') || byConditionType.equals('R')) {
                addRow.createCell(num++).setCellValue(pos.get(i).getLineArea());
                addRow.createCell(num++).setCellValue(pos.get(i).getLineAreaDes());
            }
            if (byConditionType.equals('R')) {
                addRow.createCell(num++).setCellValue(pos.get(i).getResrce());
                addRow.createCell(num++).setCellValue(pos.get(i).getResrceDes());
            }
            addRow.createCell(num++).setCellValue(pos.get(i).getModel());
            addRow.createCell(num++).setCellValue(pos.get(i).getItem());
            addRow.createCell(num++).setCellValue(pos.get(i).getByTime());
            addRow.createCell(num++).setCellValue(Double.parseDouble(df.format(Double.parseDouble(pos.get(i).getQtySum()))));
            addRow.createCell(num++).setCellValue(Double.parseDouble(df.format(Double.parseDouble(pos.get(i).getQtyYield()))));
            addRow.createCell(num++).setCellValue(Double.parseDouble(df.format(Double.parseDouble(pos.get(i).getQtyScrap()))));
            XSSFCell cellnum=addRow.createCell(num++);
            if(!Utils.isEmpty(pos.get(i).getFirstCal())){
                cellnum.setCellValue(Double.parseDouble(pos.get(i).getFirstCal().split("%")[0])/100);
            }else{
                cellnum.setCellValue(pos.get(i).getFirstCal());
            }
            cellnum.setCellStyle(style);
        }

        FileOutputStream outputStream = new FileOutputStream(filePath);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }
}
