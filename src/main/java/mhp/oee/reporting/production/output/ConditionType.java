package mhp.oee.reporting.production.output;

public enum ConditionType {

    WORK_AREA('W',"WORK_AREA"), LINE_AREA('L', "LINE_AREA"), RESRCE('R', "RESRCE"), ITEM('I', "ITEM"), MODEL('M', "MODEL");

    private Character code;
    private String name;

    private ConditionType(Character code, String name) {
        this.code = code;
        this.setName(name);
    }
    public Character getCode() {
        return code;
    }

    public void setCode(Character code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
