package mhp.oee.reporting.production.output;

public class ProductionOutputReportVO {

    private String[] workArea;
    private String[] lineArea;
    private String[] resrce;
    private String[] item;
    private String[] model;
    private Character byTimeType;
    private Character byConditionType;
    private String startDateTime;
    private String endDateTime;

    public String[] getWorkArea() {
        return workArea;
    }

    public void setWorkArea(String[] workArea) {
        this.workArea = workArea;
    }

    public String[] getLineArea() {
        return lineArea;
    }

    public void setLineArea(String[] lineArea) {
        this.lineArea = lineArea;
    }

    public String[] getResrce() {
        return resrce;
    }

    public void setResrce(String[] resrce) {
        this.resrce = resrce;
    }

    public String[] getItem() {
        return item;
    }

    public void setItem(String[] item) {
        this.item = item;
    }

    public String[] getModel() {
        return model;
    }

    public void setModel(String[] model) {
        this.model = model;
    }

    public Character getByTimeType() {
        return byTimeType;
    }

    public void setByTimeType(Character byTimeType) {
        this.byTimeType = byTimeType;
    }

    public Character getByConditionType() {
        return byConditionType;
    }

    public void setByConditionType(Character byConditionType) {
        this.byConditionType = byConditionType;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }
}
