package mhp.oee.reporting.production.output;

public enum WorkbookCell {
    SHEET_NAME("产出报表"),
    WORK_AREA("生产区域"),
    WORK_AREA_DES("生产区域描述"),
    LINE_AREA("拉线"),
    LINE_AREA_DES("拉线描述"),
    RESRCE("设备号"),
    RESRCE_DES("设备描述"),
    MODEL("MODEL"),
    ITEM("物料编码"),
    BY_TIME_TYPE_M("月"),
    BY_TIME_TYPE_W("周"),
    BY_TIME_TYPE_D("天"),
    BY_TIME_TYPE_S("班次"),
    QTY_SUM("生产总数"),
    QTY_YIELD("良品数"),
    QTY_SCRAP("人工录入坏品数"),
    FIRST_CAL("优率");
    
    private String name;
    
    private WorkbookCell(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
