package mhp.oee.reporting.oeedetail;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.extend.OeeAlarmLinePO;
import mhp.oee.po.extend.OeeOeePO;
import mhp.oee.po.extend.OeeReasonCodeGroupPO;
import mhp.oee.po.extend.OeeReasonCodePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ProductionOutputFirstAndFollowUpVO;
import mhp.oee.web.Constants;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by SuMingzhi on 2016/11/15.
 */
@RestController
public class OeeDetailReportController {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    OeeDetailReportService oeeReportService;

    @RequestMapping(value = "/web/rest/report/oeedetail/site/{site}/export_excel", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Integer> exportExcel(@PathVariable("site") String site,
            @RequestParam(value = "work_area", required = true) String workCenter,
            @RequestParam(value = "resource_type", required = true) String resourceType,
            @RequestParam(value = "resource_code", required = true) String resourceBo,
            @RequestParam(value = "end_date", required = true) String endDateTime,
            @RequestParam(value = "shift", required = true) String shiftLike,
            @RequestParam(value = "start_date", required = true) String startDateTime,
            @RequestParam(value = "line_area", required = true) String workCenterLink, HttpServletResponse response) {
        String trace = String.format(
                "[OeeReportController.exportExcel] request : site=%s,workCenter=%s,resourceType=%s, resourceBo=%s, endDateTime=%s,shiftLike=%s, startDateTime=%s, workCenterLink=%s",
                site, workCenter, resourceType, resourceBo, endDateTime, shiftLike, startDateTime, workCenterLink);
        logger.debug(">> " + trace);
        List<OeeOeePO> vos = null;
        try {
            File file = new File("download.xlsx");
            vos = oeeReportService.selectOeeOutputReportExcel(site, workCenter, resourceType, resourceBo, endDateTime,
                    shiftLike, startDateTime, workCenterLink);
            oeeReportService.commitExportExcel(vos, file);
            Utils.generateDownloadFile(response, file);
            FileUtils.deleteQuietly(file);
        } catch (IOException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
            logger.error("exportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        } catch (ParseException e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< [OeeReportController.exportExcel] response : void");
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/web/rest/report/oeedetailnum/site/{site}/export_excel", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public Integer oeeNum(@PathVariable("site") String site,
            @RequestParam(value = "work_area", required = true) String workCenter,
            @RequestParam(value = "resource_type", required = true) String resourceType,
            @RequestParam(value = "resource_code", required = true) String resourceBo,
            @RequestParam(value = "end_date", required = true) String endDateTime,
            @RequestParam(value = "shift", required = true) String shiftLike,
            @RequestParam(value = "start_date", required = true) String startDateTime,
            @RequestParam(value = "line_area", required = true) String workCenterLink, HttpServletResponse response) {
        String trace = String.format(
                "[OeeReportController.exportExcel] request : site=%s,workCenter=%s,resourceType=%s, resourceBo=%s, endDateTime=%s,shiftLike=%s, startDateTime=%s, workCenterLink=%s",
                site, workCenter, resourceType, resourceBo, endDateTime, shiftLike, startDateTime, workCenterLink);
        logger.debug(">> " + trace);
        List<OeeOeePO> vos = null;
        try {
            vos = oeeReportService.selectOeeOutputReportExcel(site, workCenter, resourceType, resourceBo, endDateTime,
                    shiftLike, startDateTime, workCenterLink);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        logger.debug("<< [OeeReportController.exportExcel] response : void");
        if(vos==null)
        	return 0;
        else
            return vos.size();
    }

}
