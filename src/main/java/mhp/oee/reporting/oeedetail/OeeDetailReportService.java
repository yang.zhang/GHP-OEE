package mhp.oee.reporting.oeedetail;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.extend.OeeAlarmLinePO;
import mhp.oee.reporting.component.OeeDetailComponent;
import mhp.oee.reporting.oeedetail.OeeDetailWorkbookCell;
import mhp.oee.po.extend.OeeOeePO;
import mhp.oee.utils.Utils;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;

/**
 * Created by SuMingzhi on 2016/11/15.
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class OeeDetailReportService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private OeeDetailComponent oeeComponent;



    public List<OeeOeePO> selectOeeOutputReportExcel(String site, String workCenter,String resourceType, String resourceBo, String endDateTime, String shiftLike,String startDateTime,String workCenterLink) throws ParseException {
        startDateTime = Utils.parseReportDateDay(startDateTime);
        endDateTime = Utils.parseReportDateDay(endDateTime);
        return oeeComponent.selectOeePOList(site, workCenter,resourceType, resourceBo, endDateTime, shiftLike, startDateTime, workCenterLink);
    }



    public void commitExportExcel(List<OeeOeePO> vos, File file) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFCellStyle cellStyle = workbook.createCellStyle();
//        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中  
//        cellStyle.setFillForegroundColor(new XSSFColor(new Color(111)));// 设置背景色  
        
        XSSFCellStyle styleOfOee = workbook.createCellStyle();
        //styleOfOee.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%")); 
        styleOfOee.setDataFormat(workbook.createDataFormat().getFormat("0.000%"));
//        XSSFDataFormat createDataFormat = workbook.createDataFormat();
//        createCellStyle.setDataFormat(createDataFormat.getFormat("#,#0.00%"));
//        HSSFFont font = workbook.createFont();  
//        font.setFontName("黑体");  
//        font.setFontHeightInPoints((short) 16);//设置字体大小  
          
//        XSSFFont font = workbook.createFont();  
//        font.setFontName("微软雅黑");  
//        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//粗体显示  
//        font.setFontHeightInPoints((short) 12);  
          
//        cellStyle.setFont(font);//选择需要用到的字体格式  

        XSSFSheet sheet = workbook.createSheet(OeeDetailWorkbookCell.SHEET_NAME.getName());
       /*
        sheet.setColumnWidth(0, 3500); //设置初始列宽
        sheet.setColumnWidth(1, 3766); //设置初始列宽
        sheet.setColumnWidth(3, 3850); //设置初始列宽
        sheet.setColumnWidth(4, 6000); //设置初始列宽
        sheet.setColumnWidth(7, 8000); //设置初始列宽
        sheet.setColumnWidth(9, 3766); //设置初始列宽
        sheet.setColumnWidth(10, 3766); //设置初始列宽
        */        
        XSSFRow firstRow = sheet.createRow(0);
        XSSFCell createCell0 = firstRow.createCell(0);
        createCell0.setCellStyle(cellStyle);
        createCell0.setCellValue(OeeDetailWorkbookCell.DATE.getName());
        XSSFCell createCell1 = firstRow.createCell(1);
        createCell1.setCellStyle(cellStyle);
        createCell1.setCellValue(OeeDetailWorkbookCell.SHIFT.getName());
        XSSFCell createCell2 = firstRow.createCell(2);
        createCell2.setCellStyle(cellStyle);
        createCell2.setCellValue(OeeDetailWorkbookCell.WORK_AREA.getName());
        XSSFCell createCell3 = firstRow.createCell(3);
        createCell3.setCellStyle(cellStyle);
        createCell3.setCellValue(OeeDetailWorkbookCell.WORK_LINK.getName());
        XSSFCell createCell4 = firstRow.createCell(4);
        createCell4.setCellStyle(cellStyle);
        createCell4.setCellValue(OeeDetailWorkbookCell.RESRCE_TYPE.getName());
        XSSFCell createCell5 = firstRow.createCell(5);
        createCell5.setCellStyle(cellStyle);
        createCell5.setCellValue(OeeDetailWorkbookCell.RESRCE_CODE.getName());
        XSSFCell createCell6 = firstRow.createCell(6);
        createCell6.setCellStyle(cellStyle);
        createCell6.setCellValue(OeeDetailWorkbookCell.ASSET.getName());
        XSSFCell createCell7 = firstRow.createCell(7);
        createCell7.setCellStyle(cellStyle);
        createCell7.setCellValue(OeeDetailWorkbookCell.RESRCE_DESCRIPTION.getName());
        XSSFCell createCell8 = firstRow.createCell(8);
        createCell8.setCellStyle(cellStyle);
        createCell8.setCellValue(OeeDetailWorkbookCell.OEE.getName());
        XSSFCell createCell9 = firstRow.createCell(9);
        createCell9.setCellStyle(cellStyle);
        createCell9.setCellValue(OeeDetailWorkbookCell.AVAILABILITY.getName());
        XSSFCell createCell10 = firstRow.createCell(10);
        createCell10.setCellStyle(cellStyle);
        createCell10.setCellValue(OeeDetailWorkbookCell.PERFORMANCE.getName());
        XSSFCell createCell11 = firstRow.createCell(11);
        createCell11.setCellStyle(cellStyle);
        createCell11.setCellValue(OeeDetailWorkbookCell.QUALITY.getName());
        XSSFCell createCell12 = firstRow.createCell(12);
        createCell12.setCellStyle(cellStyle);
        createCell12.setCellValue(OeeDetailWorkbookCell.MODEL.getName());
        XSSFCell createCell13 = firstRow.createCell(13);
        createCell13.setCellStyle(cellStyle);
        createCell13.setCellValue(OeeDetailWorkbookCell.PPM.getName());
        if(vos!=null){
        	for (int i = 0; i < vos.size(); i++) {
        		XSSFRow addRow = sheet.createRow(i + 1);
        		OeeOeePO oeeDetail = vos.get(i);
        		addRow.createCell(0).setCellValue(oeeDetail.getByTimeChart());
        		addRow.createCell(1).setCellValue(oeeDetail.getByTime());
        		addRow.createCell(2).setCellValue(oeeDetail.getWorkArea());
        		addRow.createCell(3).setCellValue(oeeDetail.getWorkCenter());
        		addRow.createCell(4).setCellValue(oeeDetail.getResrceType());
        		addRow.createCell(5).setCellValue(oeeDetail.getResrce());
        		addRow.createCell(6).setCellValue(oeeDetail.getAsset());
        		addRow.createCell(7).setCellValue(oeeDetail.getResrceDescription());
        		XSSFCell createCell08 = addRow.createCell(8);
        		createCell08.setCellStyle(styleOfOee);
        		if(oeeDetail.getOee()!=null&&oeeDetail.getOee().length()>0){
        			createCell08.setCellValue(Double.parseDouble(oeeDetail.getOee()));
        		}else{
        			createCell08.setCellValue("N/A");
        		}
        		XSSFCell createCell09 = addRow.createCell(9);
        		createCell09.setCellStyle(styleOfOee);
        		if(oeeDetail.getA()!=null&&oeeDetail.getA().length()>0){
        			createCell09.setCellValue(Double.parseDouble(oeeDetail.getA()));
        		}else{
        			createCell09.setCellValue("N/A");
        		}
        		XSSFCell createCell010 = addRow.createCell(10);
        		createCell010.setCellStyle(styleOfOee);
        		if(oeeDetail.getP()!=null&&oeeDetail.getP().length()>0){
        			createCell010.setCellValue(Double.parseDouble(oeeDetail.getP()));
        		}else{
        			createCell010.setCellValue("N/A");
        		}
        		XSSFCell createCell011 = addRow.createCell(11);
        		createCell011.setCellStyle(styleOfOee);
        		if(oeeDetail.getQ()!=null&&oeeDetail.getQ().length()>0){
        			createCell011.setCellValue(Double.parseDouble(oeeDetail.getQ()));
        		}else{
        			createCell011.setCellValue("N/A");
        		}
        		addRow.createCell(12).setCellValue(oeeDetail.getModel());
        		if(oeeDetail.getPpmTherory() != null)
        			addRow.createCell(13).setCellValue(oeeDetail.getPpmTherory().doubleValue());
        	}
        }

        FileOutputStream outputStream = new FileOutputStream(file);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }

    // 查询结果告警线
    public List<OeeAlarmLinePO> selectOeeAlarmLine(String site) {
        return oeeComponent.selectOeeAlarmLine(site);
    }
}
