package mhp.oee.reporting.oeedetail;

/**
 * Created by SuMingzhi on 2016/11/15.
 */
public enum OeeDetailWorkbookCell {
    SHEET_NAME("OEE明细报表"),
    DATE("日期"),
    SHIFT("班次"),
    WORK_AREA("生产区域"),
    WORK_LINK("拉线"),
    RESRCE_TYPE("设备类型"),
    RESRCE_CODE("设备编码"),
    ASSET("资产号"),
    RESRCE_DESCRIPTION("设备描述"),
    OEE("OEE"),
    AVAILABILITY("时间利用率"),
    PERFORMANCE("性能率"),
    QUALITY("优率"),
    MODEL("MODEL"),
    PRD("生产操作员"),
    PPM("理论PPM");

    private String name;

    private OeeDetailWorkbookCell(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
