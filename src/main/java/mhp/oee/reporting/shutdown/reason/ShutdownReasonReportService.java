package mhp.oee.reporting.shutdown.reason;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mhp.oee.dao.extend.ShutdownReasonReportPOMapper;
import mhp.oee.reporting.component.ResourceStatusDetailComponent;
import mhp.oee.reporting.component.ShutdownReasonComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceStatusDetailVO;
import mhp.oee.vo.ShutdownReasonVO;
import mhp.oee.vo.StatVO;

@Service
public class ShutdownReasonReportService {

    @Autowired
    private ShutdownReasonReportPOMapper shutdownReasonReportPOMapper;
    @Autowired
    private ShutdownReasonComponent shutdownReasonComponent;

    public List<ShutdownReasonVO> getShutdonwReasonReport(String site, String workArea, String lineArea,
            String resourceType, String resourceCode, String startDate, String endDate, String byTimetype,String dataType, String byType, String shift) {
    	
    	return shutdownReasonComponent.getShundownReason(site, workArea, lineArea, resourceType, resourceCode,
                startDate, endDate, byTimetype,dataType,byType,shift);
    }
    	
    	
    	
        
    public List<ShutdownReasonVO> getShutdonwReasonForChart(String site, String workArea, String lineArea,
            String resourceType, String resourceCode, String startDate, String endDate, String byTimetype) {
        return shutdownReasonComponent.getShundownReasonForChart(site, workArea, lineArea, resourceType, resourceCode,
                startDate, endDate, byTimetype);
    }




	public ShutdownReasonVO convertDataToChart(List<ShutdownReasonVO> vos) {
		ShutdownReasonVO chartVo = new ShutdownReasonVO();
		if(vos.size() > 0){
			String[] reasonCodeDescriptions = new String[vos.size()];	//原因代码描述（Echarts 数组格式）
			Double[] totaPproportions = new Double[vos.size()];			//停机项/总时长（Echarts 数组格式）
			Double[] dtProportions = new Double[vos.size()];			//DT 时长占比（Echarts 数组格式）
			DecimalFormat df = new DecimalFormat("0.00");
			for(int i = 0 ; i < vos.size(); i++){
				ShutdownReasonVO shutdownReasonVO = vos.get(i);
				reasonCodeDescriptions[i] = shutdownReasonVO.getReasonCodeDescription();
				totaPproportions[i] = Double.parseDouble(df.format(shutdownReasonVO.getTotaPproportionNum()));
				dtProportions[i] = shutdownReasonVO.getDtProportionNum();
				if(i > 0){
					dtProportions[i] = dtProportions[i] + dtProportions[i-1];
				}
				dtProportions[i]=Double.parseDouble(df.format(dtProportions[i]));
			}
			if(reasonCodeDescriptions != null){
				chartVo.setReasonCodeDescriptions(reasonCodeDescriptions);
			}
			chartVo.setTotaPproportions(totaPproportions);
			chartVo.setDtProportions(dtProportions);
		}
		return chartVo;
	}




	public void shutdownReasonExportExcel(List<ShutdownReasonVO> vos, String filePath, String byType) throws IOException {
	    DecimalFormat durationDf = new DecimalFormat("0.00");
	    XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFCellStyle style = workbook.createCellStyle();
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%"));
        XSSFSheet sheet = workbook.createSheet("停机原因柏拉图报表");
        XSSFRow row = sheet.createRow(0);
        XSSFCell cell = row.createCell((short) 0);
        	
        if("W".equals(byType)){
        	 cell.setCellValue("时间");
             cell = row.createCell((short) 1);
             cell.setCellValue("生产区域");
             cell = row.createCell((short) 2);
             cell.setCellValue("停机类型");
             cell = row.createCell((short) 3);
             cell.setCellValue("发生次数");
             cell = row.createCell((short) 4);
             cell.setCellValue("时长（分钟）");
             cell = row.createCell((short) 5);
             cell.setCellValue("停机项/总时长");
             cell = row.createCell((short) 6);
             cell.setCellValue("停机项/总停机时长");
             for (int i = 0; i < vos.size(); i++) {
                 XSSFRow addRow = sheet.createRow(i + 1);
                 addRow.createCell(0).setCellValue(vos.get(i).getByTime());
                 addRow.createCell(1).setCellValue(vos.get(i).getWorkCenterDescription());
                 addRow.createCell(2).setCellValue(vos.get(i).getReasonCodeDescription());
                 addRow.createCell(3).setCellValue(vos.get(i).getShutdownTimes());
                 addRow.createCell(4).setCellValue(Double.parseDouble(durationDf.format(vos.get(i).getTotalTime()/60)));
                 XSSFCell cell5=addRow.createCell(5);
                 cell5.setCellValue(vos.get(i).getTotaPproportionNum()/100);
                 cell5.setCellStyle(style);
                 XSSFCell cell6=addRow.createCell(6);
                 
              	if(vos.get(i).getReasonCodeDescription().equals("正常生产")){
            		 cell6.setCellValue("N/A");
            		 cell6.setCellStyle(style);
            	}else{
            		cell6.setCellValue(vos.get(i).getDtProportionNum()/100);
            		 cell6.setCellStyle(style);
            	}
             }
        }else  if("L".equals(byType)){
        	cell.setCellValue("时间");
        	cell = row.createCell((short) 1);
            cell.setCellValue("生产区域");
            cell = row.createCell((short) 2);
            cell.setCellValue("拉线");
            cell = row.createCell((short) 3);
            cell.setCellValue("停机类型");
            cell = row.createCell((short) 4);
            cell.setCellValue("发生次数");
            cell = row.createCell((short) 5);
            cell.setCellValue("时长（分钟）");
            cell = row.createCell((short) 6);
            cell.setCellValue("停机项/总时长");
            cell = row.createCell((short) 7);
            cell.setCellValue("停机项/总停机时长");
            for (int i = 0; i < vos.size(); i++) {
                XSSFRow addRow = sheet.createRow(i + 1);
                addRow.createCell(0).setCellValue(vos.get(i).getByTime());
                addRow.createCell(1).setCellValue(vos.get(i).getWorkCenterDescription());
                addRow.createCell(2).setCellValue(vos.get(i).getLineAreaDescription());
                addRow.createCell(3).setCellValue(vos.get(i).getReasonCodeDescription());
                addRow.createCell(4).setCellValue(vos.get(i).getShutdownTimes());
                addRow.createCell(5).setCellValue(Double.parseDouble(durationDf.format(vos.get(i).getTotalTime()/60)));
                XSSFCell cell6=addRow.createCell(6);
                cell6.setCellValue(vos.get(i).getTotaPproportionNum()/100);
                cell6.setCellStyle(style);
                XSSFCell cell7=addRow.createCell(7);
             	if(vos.get(i).getReasonCodeDescription().equals("正常生产")){
            		 cell7.setCellValue("N/A");
            		 cell7.setCellStyle(style);
            	}else{
            		cell7.setCellValue(vos.get(i).getDtProportionNum()/100);
            		cell7.setCellStyle(style);
            	}
            }
        }else  if("T".equals(byType)){
        	cell.setCellValue("时间");
            cell = row.createCell((short) 1);
            cell.setCellValue("设备类型");
            cell = row.createCell((short) 2);
            cell.setCellValue("设备类型描述");
            cell = row.createCell((short) 3);
            cell.setCellValue("停机类型");
            cell = row.createCell((short) 4);
            cell.setCellValue("发生次数");
            cell = row.createCell((short) 5);
            cell.setCellValue("时长（分钟）");
            cell = row.createCell((short) 6);
            cell.setCellValue("停机项/总时长");
            cell = row.createCell((short) 7);
            cell.setCellValue("停机项/总停机时长");
            for (int i = 0; i < vos.size(); i++) {
                XSSFRow addRow = sheet.createRow(i + 1);
                addRow.createCell(0).setCellValue(vos.get(i).getByTime());
                addRow.createCell(1).setCellValue(vos.get(i).getResourceTypeCode());
                addRow.createCell(2).setCellValue(vos.get(i).getResourceTypeDescription());
                addRow.createCell(3).setCellValue(vos.get(i).getReasonCodeDescription());
                addRow.createCell(4).setCellValue(vos.get(i).getShutdownTimes());
                addRow.createCell(5).setCellValue(Double.parseDouble(durationDf.format(vos.get(i).getTotalTime()/60)));
                XSSFCell cell6=addRow.createCell(6);
                cell6.setCellValue(vos.get(i).getTotaPproportionNum()/100);
                cell6.setCellStyle(style);
                XSSFCell cell7=addRow.createCell(7);
             	if(vos.get(i).getReasonCodeDescription().equals("正常生产")){
            		 cell7.setCellValue("N/A");
            		 cell7.setCellStyle(style);
            	}else{
            		cell7.setCellValue(vos.get(i).getDtProportionNum()/100);
            		cell7.setCellStyle(style);
            	}
            }
        }else {
        	cell.setCellValue("时间");
        	cell = row.createCell((short) 1);
            cell.setCellValue("生产区域");
            cell = row.createCell((short) 2);
            cell.setCellValue("拉线");
            cell = row.createCell((short) 3);
            cell.setCellValue("设备编码");
            cell = row.createCell((short) 4);
            cell.setCellValue("资产号");
            cell = row.createCell((short) 5);
            cell.setCellValue("设备描述");
            cell = row.createCell((short) 6);
            cell.setCellValue("停机类型");
            cell = row.createCell((short) 7);
            cell.setCellValue("发生次数");
            cell = row.createCell((short) 8);
            cell.setCellValue("时长（分钟）");
            cell = row.createCell((short) 9);
            cell.setCellValue("停机项/总时长");
            cell = row.createCell((short) 10);
            cell.setCellValue("停机项/总停机时长");
            for (int i = 0; i < vos.size(); i++) {
                XSSFRow addRow = sheet.createRow(i + 1);
                addRow.createCell(0).setCellValue(vos.get(i).getByTime());
                addRow.createCell(1).setCellValue(vos.get(i).getWorkCenterDescription());
                addRow.createCell(2).setCellValue(vos.get(i).getLineAreaDescription());
                addRow.createCell(3).setCellValue(vos.get(i).getResrceCode());
                addRow.createCell(4).setCellValue(vos.get(i).getAsset());
                addRow.createCell(5).setCellValue(vos.get(i).getResrceDescription());
                addRow.createCell(6).setCellValue(vos.get(i).getReasonCodeDescription());
                addRow.createCell(7).setCellValue(vos.get(i).getShutdownTimes());
                addRow.createCell(8).setCellValue(Double.parseDouble(durationDf.format(vos.get(i).getTotalTime()/60)));
                XSSFCell cell9=addRow.createCell(9);
                cell9.setCellValue(vos.get(i).getTotaPproportionNum()/100);
                cell9.setCellStyle(style);
                XSSFCell cell10=addRow.createCell(10);
                
             	if(vos.get(i).getReasonCodeDescription().equals("正常生产")){
             		 cell10.setCellValue("N/A");
             		 cell10.setCellStyle(style);
             	}else{
             		cell10.setCellValue(vos.get(i).getDtProportionNum()/100);
             		 cell10.setCellStyle(style);
             	}
            }
        }	
        FileOutputStream outputStream = new FileOutputStream(filePath);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
        
    }
    

}
