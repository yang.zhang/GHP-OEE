package mhp.oee.reporting.shutdown.reason;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.reporting.resource.status.detail.ResourceStatusDetailReportService;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ResourceStatusDetailVO;
import mhp.oee.vo.ShutdownReasonVO;
import mhp.oee.vo.StatVO;
import mhp.oee.web.Constants;

@RestController
public class ShutdownReportController {
    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ShutdownReasonReportService shutdownReasonReportService;

    @RequestMapping(value = "/web/rest/report/site/{site}/shutdown_reason_report", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ShutdownReasonVO>> getShutdownReasonReport(
    		
    		@PathVariable("site") String site,
    		@RequestParam(value = "data_type", required = false) String dataType,
            @RequestParam(value = "work_center", required = false) String workCenter,
            @RequestParam(value = "line_in", required = false) String lineIn,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "resource_code", required = false) String resource_code,
            @RequestParam(value = "start_date", required = true) String startDate,
            @RequestParam(value = "end_date", required = true) String endDate,
            @RequestParam(value = "by_timetype", required = true) String byTimetype,
            @RequestParam(value = "shift", required = false) String shift,
            @RequestParam(value = "by_type", required = true) String byType) {
        try {
            startDate = Utils.parseReportDateDay(startDate);
            endDate = Utils.parseReportDateDay(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<ShutdownReasonVO> vos = null;
        vos = shutdownReasonReportService.getShutdonwReasonReport(site, workCenter, lineIn, resourceType, resource_code,
                startDate, endDate, byTimetype,dataType,byType,shift);
        logger.debug("<< [ResourceStatusDetailReportController.getResourceStatusDetailReport] response : "
                + gson.toJson(vos));
        
        if("CHAR".equals(dataType) && vos.size() > 0){
        	ShutdownReasonVO vo = shutdownReasonReportService.convertDataToChart(vos);
        	List<ShutdownReasonVO> chartvos = new ArrayList<ShutdownReasonVO>() ;
        	chartvos.add(vo);
        	return ResponseEntity.ok(chartvos);
        	
        }
        DecimalFormat DurationDf = new DecimalFormat("0.00");
        DecimalFormat df = new DecimalFormat("0.00");
        for(ShutdownReasonVO vo:vos){
            vo.setStrTotalTime(DurationDf.format(vo.getTotalTime()/60));
            vo.setStrTotaPproportionNum(df.format(vo.getTotaPproportionNum())+"%");
            if(vo.getDtProportionNum() == -1){
            	vo.setStrDtProportionNum("N/A");
            }else {
            	vo.setStrDtProportionNum(df.format(vo.getDtProportionNum())+"%");
            }
        }
        return ResponseEntity.ok(vos);
    }
    @RequestMapping(value = "/web/rest/report/site/{site}/shutdown_reason/export_excel", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceStatusDetailVO>> exportExcel(
    		@PathVariable("site") String site,
    		@RequestParam(value = "data_type", required = false) String dataType,
            @RequestParam(value = "work_center", required = false) String workCenter,
            @RequestParam(value = "line_in", required = false) String lineIn,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "resource_code", required = false) String resource_code,
            @RequestParam(value = "start_date", required = true) String startDate,
            @RequestParam(value = "end_date", required = true) String endDate,
            @RequestParam(value = "by_timetype", required = false) String byTimetype,
            @RequestParam(value = "by_type", required = true) String byType,
            @RequestParam(value = "shift", required = false) String shift,
            HttpServletResponse response) {
    	 String filePath = "download.xlsx";
    	 try {
             startDate = Utils.parseReportDateDay(startDate);
             endDate = Utils.parseReportDateDay(endDate);
         } catch (ParseException e) {
             e.printStackTrace();
         }
         /*
          * String trace = String.format(
          * "[ResourceStatusDetailReportController.getResourceStatusDetailReport] request : site=%s, workArea=%s, lineArea=%s, startDate=%s, endDate=%s, resourceType=%s, resrce=%s, shift=%s"
          * , site, workArea, lineArea, startDate, endDate, resourceType, resrce,
          * shift); logger.debug(">> " + trace);
          */
         List<ShutdownReasonVO> vos = null;
         vos = shutdownReasonReportService.getShutdonwReasonReport(site, workCenter, lineIn, resourceType, resource_code,
                 startDate, endDate, byTimetype,dataType,byType,shift);
         logger.debug("<< [ResourceStatusDetailReportController.getResourceStatusDetailReport] response : "
                 + gson.toJson(vos));
         try {
			shutdownReasonReportService.shutdownReasonExportExcel(vos, filePath,byType);
		} catch (IOException e) {
			e.printStackTrace();
		}
        try {
			this.generateDownloadFile(response, filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
         return ResponseEntity.ok(null);
    	
    }
    private void generateDownloadFile(HttpServletResponse response, String filePath) throws IOException {
        File file = new File(filePath);
        OutputStream out = response.getOutputStream();
        response.setHeader("Content-disposition", "attachment; filename=" + "download.xlsx");
        byte[] buffer = new byte[8192]; // use bigger if you want
        int length = 0;
        FileInputStream in = new FileInputStream(file);
        while ((length = in.read(buffer)) > 0) {
            out.write(buffer, 0, length);
        }
        in.close();
        out.close();
    }
}
