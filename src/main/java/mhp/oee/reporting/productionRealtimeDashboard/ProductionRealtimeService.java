package mhp.oee.reporting.productionRealtimeDashboard;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellFill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mhp.oee.dao.extend.ProductionRealtimeReportPOMapper;
import mhp.oee.dao.extend.ShutdownReasonReportPOMapper;
import mhp.oee.reporting.component.ProductionRealtimeComponent;
import mhp.oee.reporting.component.ResourceStatusDetailComponent;
import mhp.oee.reporting.component.ShutdownReasonComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ProductionRealtimeVo;
import mhp.oee.vo.ResourceStatusDetailVO;
import mhp.oee.vo.ShutdownReasonVO;
import mhp.oee.vo.StatVO;

@Service
public class ProductionRealtimeService {

    @Autowired
    private ProductionRealtimeReportPOMapper productionRealtimeReportPOMapper;
    @Autowired
    private ProductionRealtimeComponent productionRealtimeComponent;

    public List<Map> getProductionRealtimeReport(String site, String workCenter, String lineIn, String resourceType,
			String resource_code, String startDate, String endDate, String shift, String model, String item,
			int colNum,Integer pageSize, Integer currpage) throws ParseException {
    	
    	List<ProductionRealtimeVo> paras = getParas(colNum);
		
		return productionRealtimeComponent.getProductionRealtime(site, workCenter, lineIn, resourceType, resource_code,
                startDate, endDate, shift, model, item, colNum,paras,pageSize,currpage);
	}
    public static void main(String[] args) throws ParseException {
    	getParas(48);
	}	
    private static List<ProductionRealtimeVo> getParas(int colNum) throws ParseException {
    	String initStartTime =  "07:30:00";			//第一个列的开始时间
    	String initEndTime = "08:00:00" ;
    	if(colNum == 12 ){
    		initStartTime =  "08:00:00";			//第一个列的开始时间
        	initEndTime = "10:00:00" ;			//第一个列的结束时间
    	}
    	
		//半小时多传一列
		int addNum = 24*60/colNum;						//列相隔addNum分钟
		int startAddDay = 0 ;						//默认当天日期（查出的数据日期加0）
		int endAddDay = 0 ;							
		String lastStartTime = "";					//标志，若时间刚过晚间0点，则查询日期加1
		String lastEndTime = "";					//标志，若时间刚过晚间0点，则查询日期加1
		
		int vecDeal = colNum ;							//半小时数据多了晚班7：30 - 8：30 一列
		if (colNum == 48){
			vecDeal ++;
		}
		List<ProductionRealtimeVo> paras = new ArrayList<ProductionRealtimeVo>() ;
		for( int i =0 ; i < vecDeal ;i++){
			ProductionRealtimeVo vo = new ProductionRealtimeVo();
			String startTimeForCompare = Utils.addSomeTimeForTime(initStartTime,addNum*i,"MINUTE");
			String endTimeForCompare = Utils.addSomeTimeForTime(initEndTime,addNum*i,"MINUTE");
			if ((!"NO".equals(lastStartTime)) && i > 0 ){
				if(  Utils.strToTime(startTimeForCompare)
						.before(Utils.strToTime(paras.get(i-1).getStartTimeForCompare()))){
					lastStartTime = "NO";
					startAddDay = 1;
				}
			}
			if ((!"NO".equals(lastEndTime)) && i > 0 ){
				if(  Utils.strToTime(endTimeForCompare)
						.before(Utils.strToTime(paras.get(i-1).getEndTimeForCompare()))){
					lastEndTime = "NO";
					endAddDay = 1;
				}
			}
			vo.setColumnTime(getCloumnName(i));					//列名
			vo.setStartDateAddDay(startAddDay);					//开始日期加上addDay
			vo.setEndDateAddDay(endAddDay);						//结束日期加上addDay
			vo.setStartTimeForCompare(" "+startTimeForCompare);
			vo.getStartTimeForCompare();
			vo.getColumnTime();
			vo.getStartDateAddDay();
			vo.getEndTimeForCompare();
			vo.getEndDateAddDay();
			vo.setEndTimeForCompare(" "+endTimeForCompare);
			paras.add(vo);
			
		}
		return paras;
	}

	private static String getCloumnName( int i) {
		char firstT= 'A';							//
		char secondT= 'A';
		int k = i/10;
		int l = i%10;
		firstT = (char) (firstT +k);
		secondT = (char) (secondT + l);
		String  columnName = "TIME_" + firstT + secondT;
		return columnName;
		
	}
	public void productionRealtimeExportExcel(List<Map> vos, HttpServletResponse response,
			String filePath,int colNum) throws ParseException, IOException {
		
		String[] colChNames = {"日期","生产区域描述","拉线描述","设备编码","设备名称","资产号","Model","物料编码","产能目标",
				"分时段目标","早班合计","晚班合计","合计"};
		String[] colEnNames = {"RESULT_DATE","WORK_CENTER_DESCRIPTION","LINE_AREA_DESCRIPTION",
				"RESRCE","RESRCE_DESCRIPTION","ASSET","MODEL","PRODUCT_ITEM","DAY_TARGE_QTY_YIELD",
				"PART_TARGE_QTY_YIELD","M_PRO_SHIFT_TOTAL","E_PRO_SHIFT_TOTAL","DAY_PRO_SHIFT_TOTAL"};
		List<ProductionRealtimeVo> paras = getParas(colNum);
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("生产微计划报表");
        XSSFRow row = sheet.createRow(0);
        XSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("日期");
        for(int i =1 ;i < colChNames.length;i ++){
        	cell = row.createCell((short) i);
            cell.setCellValue(colChNames[i]);
        }
        for(int i = 0 ;i < paras.size() ; i ++){
        	cell = row.createCell((short) i + colChNames.length);
        	String timeColumnName = (String)paras.get(i).getStartTimeForCompare();
        	timeColumnName = timeColumnName.substring(0, timeColumnName.lastIndexOf(":"));
            cell.setCellValue(timeColumnName);
        }
        

        for (int i = 0; i < vos.size(); i++) {
        	XSSFRow addRow = sheet.createRow(i + 1);
        	for(int j = 0 ;j < colEnNames.length ;j ++){
        		try {
        			
//            		addRow.createCell(i*j+j).setCellValue(vos.get(i).get(colEnNames[j]));
        			if((String.class).equals(vos.get(i).get(colEnNames[j]).getClass())){
        				
            			addRow.createCell(j).setCellValue((String)vos.get(i).get(colEnNames[j]));
            			
            		}else if((java.sql.Date.class).equals(vos.get(i).get(colEnNames[j]).getClass())){

            			addRow.createCell(j).setCellValue(((java.sql.Date)vos.get(i).get(colEnNames[j])).toString());
            			
            		}else if((BigDecimal.class).equals(vos.get(i).get(colEnNames[j]).getClass())){
            			
            			Double bd = ((BigDecimal)vos.get(i).get(colEnNames[j])).doubleValue();
            			if(bd == -2){
            				addRow.createCell(j).setCellValue("/");
            			}else if (bd == -1){
            				addRow.createCell(j).setCellValue("-");
            			}else{
            				addRow.createCell(j).setCellValue(bd);
            			}
            			
            			
            		} 
        		
				} catch (Exception e) {
					// TODO: handle exception
				}
        	}
        	//分时段合计，为BigDecimal 格式
        	for(int z = 0 ;z < paras.size() ;z ++){
        		
        		try {
        			Double bd = ((BigDecimal)vos.get(i).get(paras.get(z).getColumnTime())).doubleValue();
        			if(bd == -2){
        				addRow.createCell(z + colEnNames.length).setCellValue("/");
        			}else if (bd == -1){
        				addRow.createCell(z + colEnNames.length).setCellValue("-");
        			}else{
        				addRow.createCell(z + colEnNames.length).setCellValue(bd);
        			}
				} catch (Exception e) {
					// TODO: handle exception
				}
        	}
        }
        FileOutputStream outputStream = new FileOutputStream(filePath);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();

    }

}
