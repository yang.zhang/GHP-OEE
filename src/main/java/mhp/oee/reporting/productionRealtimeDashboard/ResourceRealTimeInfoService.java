package mhp.oee.reporting.productionRealtimeDashboard;

import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.ServiceErrorException;
import mhp.oee.enumClass.LightColor;
import mhp.oee.enumClass.ResourceRealTimeDimension;
import mhp.oee.po.extend.ResourceRealTimeDetailPO;
import mhp.oee.po.extend.ResourceRealTimeInfoPO;
import mhp.oee.reporting.component.ResourceRealTimeInfoComponent;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Created by dinglj on 2017/4/28 028.
 */
@Service
public class ResourceRealTimeInfoService {

    @Autowired
    private ResourceRealTimeInfoComponent resourceRealTimeInfoComponent;

    public List<ResourceRealTimeInfoPO> readRealTimeInfo(Integer dimension,
                                                         String[] siteArr,
                                                         String[] workAreaArr,
                                                         String[] lineAreaArr,
                                                         String[] resourceTypeArr){


        ResourceRealTimeDimension resourceRealTimeDimension = ResourceRealTimeDimension.fromValue(dimension);
        if(resourceRealTimeDimension == null){
            //查询纬度 为空
            throw new ServiceErrorException(ErrorCodeEnum.REAL_TIME_DIMENSION_NONE,"查询纬度为空");
        }
        if(resourceRealTimeDimension == ResourceRealTimeDimension.SITE && this.isEmpty(siteArr)){
            //纬度是工厂 工厂必选
            throw new ServiceErrorException(ErrorCodeEnum.REAL_TIME_SITE_NONE,"工厂条件为空");

        }else if(resourceRealTimeDimension == ResourceRealTimeDimension.WORK_AREA && this.isEmpty(workAreaArr)){
            //纬度生产区域 生产区域必须
            throw new ServiceErrorException(ErrorCodeEnum.REAL_TIME_WORKAREA_NONE,"生产区域条件为空");

        }else if(resourceRealTimeDimension == ResourceRealTimeDimension.LINE_AREA && this.isEmpty(lineAreaArr)){
            //纬度拉线 拉线必须
            throw new ServiceErrorException(ErrorCodeEnum.REAL_TIME_LINEAREA_NONE,"拉线条件为空");

        }else if(resourceRealTimeDimension == ResourceRealTimeDimension.RESOURCE_TYPE && this.isEmpty(resourceTypeArr)){
            //纬度设备类型 设备类型必选
            throw new ServiceErrorException(ErrorCodeEnum.REAL_TIME_SOURCETYPE_NONE,"设备类型条件为空");
        }

        List<String> siteList = null;
        List<String> workAreaList = null;
        List<String> lineAreaList = null;
        List<String> resourceTypeList = null;

        if(!this.isEmpty(siteArr)){
            siteList = Arrays.asList(siteArr);
        }
        if(!this.isEmpty(workAreaArr)){
            workAreaList = Arrays.asList(workAreaArr);
        }
        if(!this.isEmpty(lineAreaArr)){
            lineAreaList = Arrays.asList(lineAreaArr);
        }
        if(!this.isEmpty(resourceTypeArr)){
            resourceTypeList = Arrays.asList(resourceTypeArr);
        }


        return this.resourceRealTimeInfoComponent.readRealTimeInfo(resourceRealTimeDimension,siteList,workAreaList,lineAreaList,resourceTypeList);


    }

    public List<ResourceRealTimeDetailPO> readRealTimeDetail(Integer dimension,
                                                             String dimensionCode,
                                                             Integer colorNum,
                                                             String[] siteArr,
                                                             String[] workAreaArr,
                                                             String[] lineAreaArr,
                                                             String[] resourceTypeArr){

        LightColor color = LightColor.fromValue(colorNum);
        if(color == null){
            //
            throw new ServiceErrorException(ErrorCodeEnum.REAL_TIME_DIMENSION_NONE,"查询三色灯颜色为空");
        }
        ResourceRealTimeDimension resourceRealTimeDimension = ResourceRealTimeDimension.fromValue(dimension);
        if(resourceRealTimeDimension == null){
            //查询纬度 为空
            throw new ServiceErrorException(ErrorCodeEnum.REAL_TIME_DIMENSION_NONE,"查询纬度为空");
        }
        if(StringUtils.isBlank(dimensionCode)){
            //查询纬度 为空
            throw new ServiceErrorException(ErrorCodeEnum.REAL_TIME_DIMENSIONCODE_NONE,"查询纬度代码为空");
        }

        List<String> siteList = null;
        List<String> workAreaList = null;
        List<String> lineAreaList = null;
        List<String> resourceTypeList = null;

        if(!this.isEmpty(siteArr)){
            siteList = Arrays.asList(siteArr);
        }
        if(!this.isEmpty(workAreaArr)){
            workAreaList = Arrays.asList(workAreaArr);
        }
        if(!this.isEmpty(lineAreaArr)){
            lineAreaList = Arrays.asList(lineAreaArr);
        }
        if(!this.isEmpty(resourceTypeArr)){
            resourceTypeList = Arrays.asList(resourceTypeArr);
        }

        if(resourceRealTimeDimension == ResourceRealTimeDimension.SITE){
            siteList = Arrays.asList(dimensionCode);
        }else if(resourceRealTimeDimension == ResourceRealTimeDimension.WORK_AREA){
            workAreaList = Arrays.asList(dimensionCode);
        }else if(resourceRealTimeDimension == ResourceRealTimeDimension.LINE_AREA){
            lineAreaList = Arrays.asList(dimensionCode);
        }else if(resourceRealTimeDimension == ResourceRealTimeDimension.RESOURCE_TYPE){
            resourceTypeList = Arrays.asList(dimensionCode);
        }


        return this.resourceRealTimeInfoComponent.readRealTimeDetail(color,siteList,workAreaList,lineAreaList,resourceTypeList);


    }

    public <T> boolean isEmpty(T[] arr){
        return arr == null || arr.length == 0;
    }


}
