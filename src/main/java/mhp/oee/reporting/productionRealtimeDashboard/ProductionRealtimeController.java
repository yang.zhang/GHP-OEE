package mhp.oee.reporting.productionRealtimeDashboard;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.reporting.resource.status.detail.ResourceStatusDetailReportService;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ResourceStatusDetailVO;
import mhp.oee.vo.ShutdownReasonVO;
import mhp.oee.vo.StatVO;
import mhp.oee.web.Constants;

@RestController
public class ProductionRealtimeController {
	
	 @Autowired
	 ProductionRealtimeService productionRealtimeService;
	 
	 @RequestMapping(value = "/web/rest/report/site/{site}/production_realtime_report", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
	    public ResponseEntity<List<Map<String, String>>> getShutdownReasonReport(
	    		
	    		@PathVariable("site") String site,
	    		@RequestParam(value = "page_size", required = false) Integer pageSize,
	            @RequestParam(value = "curr_page", required = false) Integer currPage,
	            @RequestParam(value = "work_center", required = false) String workCenter,
	            @RequestParam(value = "line_in", required = false) String lineIn,
	            @RequestParam(value = "resource_type", required = false) String resourceType,
	            @RequestParam(value = "resource_code", required = false) String resource_code,
	            @RequestParam(value = "start_date", required = false) String startDate,
	            @RequestParam(value = "end_date", required = false) String endDate,
	            @RequestParam(value = "shift", required = false) String shift ,
	    		@RequestParam(value = "model", required = false) String model ,
				@RequestParam(value = "item", required = false) String item,
				@RequestParam(value = "colNum", required = false) int colNum) throws ParseException{
		
		 startDate = Utils.parseReportDateDay(startDate);
         endDate = Utils.parseReportDateDay(endDate);
		 
         List<Map> vos = new ArrayList<Map>();
         if((currPage == null)){
        	 vos = productionRealtimeService.getProductionRealtimeReport(site, workCenter, lineIn, resourceType, resource_code,
 	                startDate, endDate, shift, model, item, colNum,null,
 	                null); 
         }else {
        	 vos = productionRealtimeService.getProductionRealtimeReport(site, workCenter, lineIn, resourceType, resource_code,
 	                startDate, endDate, shift, model, item, colNum,null,
 	                null); 
         }
         List<Map<String, String>> list=new ArrayList<Map<String,String>>();
         for(int i=0;i<vos.size();i++){
             Map<String,String> retMap=new HashMap<String, String>();
             Map map= new HashMap();
             map=vos.get(i);
             for(Object obj : map.keySet()){
             Object val = map.get(obj );
             String key=obj.toString();
             String value=val.toString();
             if("-1.0".equals(value)){
                 value="-";
             }else if("-2.0".equals(value)){
                 value="/";
             }
             
             if("PART_TARGE_QTY_YIELD".equals(key)&&value.indexOf(".")>0){
                 String valArray[]=value.split("\\.");
                 value=valArray[0]+"."+valArray[1].substring(0, 1);
                 retMap.put(key, value);
             }else{
                 retMap.put(key, value);
             }
             }
             list.add(retMap);
         }
		 
		 return ResponseEntity.ok(list);
	 }
	 
	 @RequestMapping(value = "/web/rest/report/site/{site}/production_realtime/export_excel", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
	    public ResponseEntity<List<ResourceStatusDetailVO>> exportExcel(@PathVariable("site") String site,
	            @RequestParam(value = "work_center", required = false) String workCenter,
	            @RequestParam(value = "line_in", required = false) String lineIn,
	            @RequestParam(value = "resource_type", required = false) String resourceType,
	            @RequestParam(value = "resource_code", required = false) String resource_code,
	            @RequestParam(value = "start_date", required = false) String startDate,
	            @RequestParam(value = "end_date", required = false) String endDate,
	            @RequestParam(value = "shift", required = false) String shift ,
	    		@RequestParam(value = "model", required = false) String model ,
				@RequestParam(value = "item", required = false) String item,
				@RequestParam(value = "colNum", required = false) int colNum, HttpServletResponse response,
	            HttpServletRequest request) throws ParseException, IOException {
		 
		 String filePath = "download.xlsx";
		 
		 startDate = Utils.parseReportDateDay(startDate);
         endDate = Utils.parseReportDateDay(endDate);
         
		 List<Map> vos = productionRealtimeService.getProductionRealtimeReport(site, workCenter, lineIn, resourceType, resource_code,
	                startDate, endDate, shift, model, item, colNum,null, null);
		 
		 productionRealtimeService.productionRealtimeExportExcel(vos, response, filePath,colNum);
		 this.generateDownloadFile(response, filePath);
		 return ResponseEntity.ok(null);
		 
	 }
	 
	 private void generateDownloadFile(HttpServletResponse response, String filePath) throws IOException {
	        File file = new File(filePath);
	        OutputStream out = response.getOutputStream();
	        response.setHeader("Content-disposition", "attachment; filename=" + "download.xlsx");
	        byte[] buffer = new byte[8192]; // use bigger if you want
	        int length = 0;
	        FileInputStream in = new FileInputStream(file);
	        while ((length = in.read(buffer)) > 0) {
	            out.write(buffer, 0, length);
	        }
	        in.close();
	        out.close();
	    }
}
