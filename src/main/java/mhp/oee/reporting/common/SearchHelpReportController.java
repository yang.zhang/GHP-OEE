package mhp.oee.reporting.common;

import java.util.List;

import mhp.oee.vo.ResourceTypeConditionVO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.gen.UserPO;
import mhp.oee.vo.ItemVO;
import mhp.oee.vo.ResourceVO;
import mhp.oee.vo.WorkCenterConditionVO;
import mhp.oee.web.Constants;

@RestController
public class SearchHelpReportController {
    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    SearchHelpReportService searchHelpReportService;


    /**
     * 设备类型查询 支持多site
     * @param siteArr
     * @return
     */
    @RequestMapping(value = "/web/rest/report/help/resource_type", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceTypeConditionVO>> readResourceType(@RequestParam("sites") String[] siteArr) {
        String trace = String.format("[SearchHelpReportController.readResourceType] request : site=%s", siteArr);
        logger.debug(">> " + trace);
        List<ResourceTypeConditionVO> vos = searchHelpReportService.readReourceTypeBySite(siteArr);
        logger.debug("<< [SearchHelpReportController.readResourceType]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    /**
     * 生产区域查询 支持多site
     * @param siteArr
     * @param workCenter
     * @return
     */
    @RequestMapping(value = "/web/rest/report/help/work_center", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<WorkCenterConditionVO>> readWorkCenter(@RequestParam("sites") String[] siteArr,
                                                                      @RequestParam(value = "work_center", required = true) String workCenter) {
        String trace = String.format("[SearchHelpReportController.readWorkCenter] request : site=%s, workCenter=%S", siteArr, workCenter);
        logger.debug(">> " + trace);
        List<WorkCenterConditionVO> vos = searchHelpReportService.readWorkCenter(siteArr, workCenter);
        logger.debug("<< [SearchHelpReportController.readWorkCenter]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    /**
     * 拉线查询 支持多个site
     * @return
     */
    @RequestMapping(value = "/web/rest/report/help/lines",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<WorkCenterConditionVO>> readLines(
            @RequestParam("sites") String[] siteList,
            @RequestParam(value = "work_center", required = false) String[] workCenter,
            @RequestParam(value = "line", required = false) String line) {
        String trace = String.format("[SearchHelpReportController.readLines] request : site=%s, workCenter=%s, line=%s", siteList, workCenter, line);
        logger.debug(">> " + trace);
        List<WorkCenterConditionVO> vos = searchHelpReportService.readLines(siteList, workCenter, line);
        logger.debug("<< [SearchHelpReportController.readLines]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    /**
     * search help : work center
     * @param site
     * @param workCenter
     * @return
     */
    @RequestMapping(value = "/web/rest/report/site/{site}/work_center_help", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<WorkCenterConditionVO>> readWorkCenterHelp(
            @PathVariable("site") String site,
            @RequestParam(value = "work_center", required = true) String workCenter) {
        String trace = String.format("[SearchHelpReportController.readWorkCenterHelp] request : site=%s, workCenter=%S", site, workCenter);
        logger.debug(">> " + trace);
        List<WorkCenterConditionVO> vos = searchHelpReportService.readWorkCenterHelp(site, workCenter);
        logger.debug("<< [SearchHelpReportController.readWorkCenterHelp]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    /**
     * search help : line
     * @param site
     * @param workCenter
     * @param line
     * @return
     */
    @RequestMapping(value = "/web/rest/report/site/{site}/line_help",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<WorkCenterConditionVO>> readLineHelp(
            @PathVariable("site") String site,
            @RequestParam(value = "work_center", required = false) String[] workCenter,
            @RequestParam(value = "line", required = true) String line) {
        String trace = String.format("[SearchHelpReportController.readLineHelp] request : site=%s, workCenter=%s, line=%s", site, workCenter, line);
        logger.debug(">> " + trace);
        List<WorkCenterConditionVO> vos = searchHelpReportService.readLineHelp(site, workCenter, line);
        logger.debug("<< [SearchHelpReportController.readLineHelp]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }


    
    /**
     * search help : resource
     * @param site
     * @param resrce
     * @param lineArea
     * @param workArea
     * @param resourceType
     * @return
     */
    @RequestMapping(value = "/web/rest/report/site/{site}/resource_help", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceVO>> readResourceHelp(
            @PathVariable("site") String site,
            @RequestParam(value = "resrce", required = true) String resrce,
            @RequestParam(value = "resrceType", required = false) String resrceType,
            @RequestParam(value = "lineArea", required = false) String[] lineArea,
            @RequestParam(value = "workArea", required = false) String[] workArea) {
        String trace = String.format("[SearchHelpReportController.readResourceHelp] request : site=%s, resrce=%s, lineArea=%s, workArea=%s", 
                site, resrce, lineArea, workArea);
        logger.debug(">> " + trace);
        List<ResourceVO> vos = searchHelpReportService.readResourceHelp(site, resrceType,resrce, lineArea, workArea);
        logger.debug("<< [SearchHelpReportController.readResourceHelp]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    /**
     * oee报表增加设备接口
     * @param site
     * @param resrce
     * @param lineArea
     * @param workArea
     * @return
     */
    @RequestMapping(value = "/web/rest/report/site/{site}/oee_report_resource_help", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceVO>> readOeeResource(
            @PathVariable("site") String site,
            @RequestParam(value = "resrce", required = true) String resrce,
            @RequestParam(value = "lineArea", required = false) String[] lineArea,
            @RequestParam(value = "workArea", required = false) String[] workArea) {
        String trace = String.format("[SearchHelpReportController.readResourceHelp] request : site=%s, resrce=%s, lineArea=%s, workArea=%s", 
                site, resrce, lineArea, workArea);
        logger.debug(">> " + trace);
        List<ResourceVO> vos = searchHelpReportService.readOeeResource(site, resrce, lineArea, workArea);
        logger.debug("<< [SearchHelpReportController.readResourceHelp]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
   
    /**
     * search help : model
     * @param site
     * @param model
     * @return
     */
    @RequestMapping(value = "/web/rest/report/site/{site}/model_help", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ItemVO>> readModelHelp(
            @PathVariable("site") String site,
            @RequestParam(value = "model", required = true) String model) {
        String trace = String.format("[SearchHelpReportController.readModelHelp] request : site=%s, model=%s", site, model);
        logger.debug(">> " + trace);
        List<ItemVO> vos = searchHelpReportService.readModelHelp(site, model);
        logger.debug("<< [SearchHelpReportController.readModelHelp]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    /**
     * search help : item
     * @param site
     * @param model
     * @param item
     * @return
     */
    @RequestMapping(value = "/web/rest/report/site/{site}/item_help", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ItemVO>> readItemHelp(
            @PathVariable("site") String site,
            @RequestParam(value = "model", required = false) String[] model,
            @RequestParam(value = "item", required = false) String item) {
        String trace = String.format("[SearchHelpReportController.readItemHelp] request : site=%s, model=%s, item=%s", site, model, item);
        logger.debug(">> " + trace);
        List<ItemVO> vos = searchHelpReportService.readItemHelp(site, model, item);
        logger.debug("<< [SearchHelpReportController.readItemHelp]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    
    /**
     * search help : Prd
     * @param site
     * @return
     */
    @RequestMapping(value = "/web/rest/report/site/{site}/prd_help", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<UserPO>> readPrdHelp(
            @PathVariable("site") String site) {
        String trace = String.format("[SearchHelpReportController.readItemHelp] request : site=%s", site);
        logger.debug(">> " + trace);
        List<UserPO> vos = searchHelpReportService.getPrd(site);
        logger.debug("<< [SearchHelpReportController.readItemHelp]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
}
