package mhp.oee.reporting.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mhp.oee.vo.ResourceTypeConditionVO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.common.handle.WorkCenterBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ResourcePlcInfoComponent;
import mhp.oee.component.plant.WorkCenterShiftComponent;
import mhp.oee.component.production.ItemComponent;
import mhp.oee.component.user.UserComponent;
import mhp.oee.po.gen.UserPO;
import mhp.oee.po.gen.WorkCenterShiftPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ItemVO;
import mhp.oee.vo.ResourceVO;
import mhp.oee.vo.WorkCenterConditionVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class SearchHelpReportService {
    @InjectableLogger
    private static Logger logger;
    
    @Autowired
    private ResourcePlcInfoComponent resourcePlcInfoComponent;
    
    @Autowired
    private ItemComponent itemComponent;
    @Autowired
    private UserComponent userComponent;
    @Autowired
    private WorkCenterShiftComponent workCenterShiftComponent;


    public List<ResourceTypeConditionVO> readReourceTypeBySite(String[] siteArr){

        List<String> siteList = null;
        if(siteArr != null && siteArr.length > 0){
            siteList = Arrays.asList(siteArr);
        }
        return this.resourcePlcInfoComponent.readResourceTypeBySite(siteList);
    }

    public List<WorkCenterConditionVO> readWorkCenterHelp(String site, String workCenter) {
        return resourcePlcInfoComponent.readWorkCenterHelp(site, workCenter);
    }

    public List<WorkCenterConditionVO> readWorkCenter(String[] siteArr, String workCenter){

        List<String> siteList = null;
        if(siteArr != null && siteArr.length > 0){
            siteList = Arrays.asList(siteArr);
        }
        return this.resourcePlcInfoComponent.readWorkCenter(siteList,workCenter);
    }

    public List<WorkCenterConditionVO> readLineHelp(String site, String[] workCenters, String line) {
        List<String> workCenterBOList = null;
        if (workCenters != null && workCenters.length > 0) {
            workCenterBOList = new ArrayList<String>();
            for (String workCenter : workCenters) {
                workCenterBOList.add(new WorkCenterBOHandle(site, workCenter).getValue()); 
            }
        }
		/*
		 * ????????? line = "%" + line + "%";
		 */
        return resourcePlcInfoComponent.readLineHelp(site, workCenterBOList, line);
    }

    public List<WorkCenterConditionVO> readLines(String[] siteArr, String[] workAreaArr, String lineString){
        List<String> siteList = null;
        if(siteArr != null && siteArr.length > 0){
            siteList = Arrays.asList(siteArr);
        }
        List<String> workAreaList = null;
        if(siteList != null && workAreaArr != null && workAreaArr.length > 0){
            workAreaList = new ArrayList<String>();
            for (String site : siteList){
                for (String workArea : workAreaArr){
                    workAreaList.add( new WorkCenterBOHandle(site, workArea).getValue() );
                }
            }
        }
        return resourcePlcInfoComponent.readLinesByWorkAreaAndSites(siteList,workAreaList,lineString);
    }
    
    public List<ResourceVO> readResourceHelp(String site,String resrceType, String resrce, String[] lines, String[] workCenters) {
    	String[] resrces = null;
    	if(resrce != null && resrce.length() > 0){
    		resrces =  Utils.ConvertStrtoArrayLike(resrce);
    	}
        List<String> lineList = null;
        List<String> workCenterList = null;
        if (lines != null && lines.length > 0) {
            lineList = new ArrayList<String>();
            for (String line : lines) {
                lineList.add(line);
            }
        }
        if (workCenters != null && workCenters.length > 0) {
            workCenterList = new ArrayList<String>();
            for (String workCenter : workCenters) {
                workCenterList.add(workCenter);
            }
        }
        return resourcePlcInfoComponent.readResourceHelp(site,resrceType, resrces,lineList, workCenterList);
    }
    
    public List<ResourceVO> readOeeResource(String site, String resrce, String[] lines, String[] workCenters) {
        List<String> lineList = null;
        List<String> workCenterList = null;
        if (lines != null && lines.length > 0) {
            lineList = new ArrayList<String>();
            for (String line : lines) {
                lineList.add(line);
            }
        }
        if (workCenters != null && workCenters.length > 0) {
            workCenterList = new ArrayList<String>();
            for (String workCenter : workCenters) {
                workCenterList.add(workCenter);
            }
        }
        return resourcePlcInfoComponent.readOeeResource(site, resrce, lineList, workCenterList);
    }
    
    public List<ItemVO> readModelHelp(String site, String model) {
        model = "%" + model + "%";
        return itemComponent.readModelHelp(site, model);
    }
    
    public List<ItemVO> readItemHelp(String site, String[] models, String item) {
        List<String> modelList = new ArrayList<String>();
        if (models != null && models.length > 0) {
            for (String model : models) {
                modelList.add(model);
            }
        }
        return itemComponent.readItemHelp(site, modelList, item);
    }

    public List<WorkCenterShiftPO> getShift(String site,String work_center) {
        return workCenterShiftComponent.getShift(site,work_center);
    }
    public List<UserPO> getPrd(String site) {
        return userComponent.getAllPrd(site);
    }
    
    
    
}
