package mhp.oee.reporting.ppm;
/**
 * 
 * @author QiongYueXu
 *
 */
public enum PpmActivityParam {
    
    VA01("VA01", "单次最小运行时长(分钟)"),
    VA02("VA02", "单次运行时段内最小产量"),
    VA03("VA03", "单次运行时段内最小PPM"),
    VA04("VA04", "单次运行时段内最大PPM"),
    VA05("VA05", "去除N个最大的单次运行时段内PPM");
    
    private String paramId;
    private String paramDes;
    
    private PpmActivityParam(String paramId, String paramDes) {
        this.setParamId(paramId);
        this.setParamDes(paramDes);
    }

    public String getParamId() {
        return paramId;
    }

    public void setParamId(String paramId) {
        this.paramId = paramId;
    }

    public String getParamDes() {
        return paramDes;
    }

    public void setParamDes(String paramDes) {
        this.paramDes = paramDes;
    }
    
}
