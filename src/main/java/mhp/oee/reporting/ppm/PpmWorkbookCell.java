package mhp.oee.reporting.ppm;

/**
 * Created by LinZuK on 2016/8/17.
 */
public enum PpmWorkbookCell {
    SHEET_NAME("PPM报表"),
    WORK_AREA("生产区域"),
    WORK_AREA_DES("生产区域描述"),
    LINE_AREA("拉线"),
    LINE_AREA_DES("拉线描述"),
    RESRCE("设备号"),
    DESCRIPTION("设备描述"),
    ASSET("资产号"),
    MODEL("Model"),
    ITEM("物料编码"),
    BY_TIME("月/周/天/班次"),
    PPM_THEORY("理论PPM"),
    PPM("实际PPM"),
    FIRST_CAL("达成率");

    private String name;

    private PpmWorkbookCell(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
