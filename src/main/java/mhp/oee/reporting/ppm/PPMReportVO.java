package mhp.oee.reporting.ppm;

public class PPMReportVO {

    private String[] model;
    private String[] itemIn;
    private String[] resrceIn;
    private String startDate;
    private String endDate;
    private String byConditionType;
    private String[] lineIn;
    private String byTimeType;
    private String[] workCenterIn;

    public String[] getModel() {
        return model;
    }

    public void setModel(String[] model) {
        this.model = model;
    }

    public String[] getItemIn() {
        return itemIn;
    }

    public void setItemIn(String[] itemIn) {
        this.itemIn = itemIn;
    }

    public String[] getResrceIn() {
        return resrceIn;
    }

    public void setResrceIn(String[] resrceIn) {
        this.resrceIn = resrceIn;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getByConditionType() {
        return byConditionType;
    }

    public void setByConditionType(String byConditionType) {
        this.byConditionType = byConditionType;
    }

    public String[] getLineIn() {
        return lineIn;
    }

    public void setLineIn(String[] lineIn) {
        this.lineIn = lineIn;
    }

    public String getByTimeType() {
        return byTimeType;
    }

    public void setByTimeType(String byTimeType) {
        this.byTimeType = byTimeType;
    }

    public String[] getWorkCenterIn() {
        return workCenterIn;
    }

    public void setWorkCenterIn(String[] workCenterIn) {
        this.workCenterIn = workCenterIn;
    }

}
