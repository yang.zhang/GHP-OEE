package mhp.oee.reporting.ppm;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.extend.PpmReportPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.web.Constants;

/**
 * Created by LinZuK on 2016/8/17.
 */
@RestController
public class PpmReportController {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    PpmReportService ppmReportService;

    @RequestMapping(value = "/report/ppm/site/{site}/ppm_output_report", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> readPpmOutputReport(@PathVariable("site") String site, @RequestBody PPMReportVO vo) {

        String[] model = vo.getModel();
        String[] itemIn = vo.getItemIn();
        String[] resrceIn = vo.getResrceIn();
        String startDate = vo.getStartDate();
        String endDate = vo.getEndDate();
        String byConditionType = vo.getByConditionType();
        String[] lineIn = vo.getLineIn();
        String byTimeType = vo.getByTimeType();
        String[] workCenterIn = vo.getWorkCenterIn();

        String trace = String.format(
                "[PpmReportController.readPpmOutputReport] request : site=%s, model=%s, item=%s, resrceIn=%s, startDate=%s"
                        + ", endDate=%s, byConditionType=%s, lineIn=%s, byTimeType=%s, workCenterIn=%s",
                site, model, gson.toJson(itemIn), gson.toJson(resrceIn), startDate, endDate, byConditionType,
                gson.toJson(lineIn), byTimeType, gson.toJson(workCenterIn));

        logger.debug(">> " + trace);
        List<PpmReportPO> vos = null;
        try {
            vos = ppmReportService.selectPpmOutputReport(model, itemIn, site, resrceIn, startDate, endDate,
                    byConditionType, lineIn, byTimeType, workCenterIn);
            DecimalFormat df = new DecimalFormat("0.000");
            for(PpmReportPO ppmReportPO:vos){
                if(ppmReportPO.getPpm()==null||"".equals(ppmReportPO.getPpm())){
                    ppmReportPO.setPpm("N/A");
                }else{
                	ppmReportPO.setPpm(df.format(Double.parseDouble(ppmReportPO.getPpm())));
                }
                if(ppmReportPO.getPpmTheory()==null || "".equals(ppmReportPO.getPpmTheory())){
                    ppmReportPO.setPpmTheory("N/A");
                }else{
                	ppmReportPO.setPpmTheory(df.format(Double.parseDouble(ppmReportPO.getPpmTheory())));
                }
            }
        } catch (ParseException e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } catch (BusinessException e) {
            ExceptionVO exceptionVO = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("PpmReportController.readPpmOutputReport() : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        }
        logger.debug("<< [PpmReportController.readPpmOutputReport] response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/report/ppm/site/{site}/export_excel", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> exportExcel(@PathVariable("site") String site,
            @RequestBody PPMReportVO vo,
            HttpServletResponse response) {

        String[] model = vo.getModel();
        String[] itemIn = vo.getItemIn();
        String[] resrceIn = vo.getResrceIn();
        String startDate = vo.getStartDate();
        String endDate = vo.getEndDate();
        String byConditionType = vo.getByConditionType();
        String[] lineIn = vo.getLineIn();
        String byTimeType = vo.getByTimeType();
        String[] workCenterIn = vo.getWorkCenterIn();

        String trace = String.format(
                "[PpmReportController.readPpmOutputReport] request : site=%s, model=%s, item=%s, resrceIn=%s, startDate=%s"
                        + ", endDate=%s, byConditionType=%s, lineIn=%s, byTimeType=%s, workCenterIn=%s",
                site, model, gson.toJson(itemIn), gson.toJson(resrceIn), startDate, endDate, byConditionType,
                gson.toJson(lineIn), byTimeType, gson.toJson(workCenterIn));
        logger.debug(">> " + trace);
        try {
            File file = new File("download.xlsx");
            List<PpmReportPO> vos = ppmReportService.selectPpmOutputReport(model, itemIn, site, resrceIn, startDate,
                    endDate, byConditionType, lineIn, byTimeType, workCenterIn);
            if (StringUtils.isNotBlank(byConditionType) && "ITEM".equalsIgnoreCase(byConditionType.trim())) {
                ppmReportService.commitExportExcelItem(vos, file);
            } else {
                ppmReportService.commitExportExcelDev(vos, file);
            }
            Utils.generateDownloadFile(response, file);
            FileUtils.deleteQuietly(file);
        } catch (IOException e) {
            ExceptionVO exceptionVO = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
            logger.error("exportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        } catch (ParseException e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } catch (BusinessException e) {
            ExceptionVO exceptionVO = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("PpmReportController.readPpmOutputReport() : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        }
        logger.debug("<< [PpmReportController.exportExcel] response : void");
        return ResponseEntity.ok(null);
    }

}
