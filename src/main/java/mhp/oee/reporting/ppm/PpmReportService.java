package mhp.oee.reporting.ppm;

import mhp.oee.common.exception.ActivityParamErrorException;
import mhp.oee.common.exception.NotFoundActivityParamException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.reporting.component.PpmComponent;
import mhp.oee.po.extend.PpmReportPO;
import mhp.oee.po.gen.ActivityParamValuePO;
import mhp.oee.utils.Utils;

import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by LinZuK on 2016/8/17.
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class PpmReportService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private PpmComponent ppmComponent;


    public List<PpmReportPO> selectPpmOutputReport(String[] model, String[] itemIn, String site, String[] resrceIn, String startDate,
                                                   String endDate, String byConditionType, String[] lineIn, String byTimeType, String[] workCenterIn) throws ParseException, NotFoundActivityParamException, ActivityParamErrorException {
        startDate = Utils.parseReportDateDay(startDate);
        endDate = Utils.parseReportDateDay(endDate);
        // 1 SELECT ACTIVITY PARAM VALUE
        List<ActivityParamValuePO> list = ppmComponent.selectParamValue(site, PpmActivityParam.VA01.getParamId());
        int i = 0;
        if (Utils.isEmpty(list)) {
           throw new NotFoundActivityParamException();
        } else {
            for (ActivityParamValuePO activityParamValuePO : list) {
                if (activityParamValuePO.getParamId().equals(PpmActivityParam.VA01.getParamId())
                        || activityParamValuePO.getParamId().equals(PpmActivityParam.VA02.getParamId())
                        || activityParamValuePO.getParamId().equals(PpmActivityParam.VA03.getParamId())
                        || activityParamValuePO.getParamId().equals(PpmActivityParam.VA04.getParamId())
                        || activityParamValuePO.getParamId().equals(PpmActivityParam.VA05.getParamId())){
                    i++;
                }
            }
            if (i != 5 && list.size() >= 5) {
                throw new ActivityParamErrorException();
            }
        }
        return ppmComponent.selectPpmOutputReport(model, itemIn, site, resrceIn, startDate, endDate, byConditionType, lineIn, byTimeType, workCenterIn, list);
    }

    public void commitExportExcelDev(List<PpmReportPO> vos, File file) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFCellStyle style = workbook.createCellStyle();
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%"));
        XSSFSheet sheet = workbook.createSheet(PpmWorkbookCell.SHEET_NAME.getName());
        XSSFRow firstRow = sheet.createRow(0);
        DecimalFormat df = new DecimalFormat("0.000");
        firstRow.createCell(0).setCellValue(PpmWorkbookCell.WORK_AREA.getName());
        firstRow.createCell(1).setCellValue(PpmWorkbookCell.WORK_AREA_DES.getName());
        firstRow.createCell(2).setCellValue(PpmWorkbookCell.LINE_AREA.getName());
        firstRow.createCell(3).setCellValue(PpmWorkbookCell.LINE_AREA_DES.getName());
        firstRow.createCell(4).setCellValue(PpmWorkbookCell.RESRCE.getName());
        firstRow.createCell(5).setCellValue(PpmWorkbookCell.DESCRIPTION.getName());
        firstRow.createCell(6).setCellValue(PpmWorkbookCell.ASSET.getName());
        firstRow.createCell(7).setCellValue(PpmWorkbookCell.MODEL.getName());
        firstRow.createCell(8).setCellValue(PpmWorkbookCell.ITEM.getName());
        firstRow.createCell(9).setCellValue(PpmWorkbookCell.BY_TIME.getName());
        firstRow.createCell(10).setCellValue(PpmWorkbookCell.PPM_THEORY.getName());
        firstRow.createCell(11).setCellValue(PpmWorkbookCell.PPM.getName());
        firstRow.createCell(12).setCellValue(PpmWorkbookCell.FIRST_CAL.getName());

        for (int i = 0; i < vos.size(); i++) {
            XSSFRow addRow = sheet.createRow(i + 1);
            addRow.createCell(0).setCellValue(vos.get(i).getWorkArea());
            addRow.createCell(1).setCellValue(vos.get(i).getWorkAreaDes());
            addRow.createCell(2).setCellValue(vos.get(i).getLineArea());
            addRow.createCell(3).setCellValue(vos.get(i).getLineAreaDes());
            addRow.createCell(4).setCellValue(vos.get(i).getResrce());
            addRow.createCell(5).setCellValue(vos.get(i).getDescription());
            addRow.createCell(6).setCellValue(vos.get(i).getAsset());
            addRow.createCell(7).setCellValue(vos.get(i).getModel());
            addRow.createCell(8).setCellValue(vos.get(i).getItem());
            addRow.createCell(9).setCellValue(vos.get(i).getByTime());
            if(vos.get(i).getPpmTheory()!=null){
                addRow.createCell(10).setCellValue(Double.parseDouble(df.format(Double.valueOf(vos.get(i).getPpmTheory()))));
            }
            if(vos.get(i).getPpm()!=null){
                addRow.createCell(11).setCellValue(Double.parseDouble(df.format(Double.valueOf(vos.get(i).getPpm()))));
            }else
            	addRow.createCell(11).setCellValue("N/A");
            XSSFCell cell=addRow.createCell(12);
            if(!Utils.isEmpty(vos.get(i).getFirstCal())){
                cell.setCellValue(Double.parseDouble(vos.get(i).getFirstCal().split("%")[0])/100);
            }else{
                cell.setCellValue(vos.get(i).getFirstCal());
            }
            cell.setCellStyle(style);
        }

        FileOutputStream outputStream = new FileOutputStream(file);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }

    public void commitExportExcelItem(List<PpmReportPO> vos, File file) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFCellStyle style = workbook.createCellStyle();
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%"));
        XSSFSheet sheet = workbook.createSheet(PpmWorkbookCell.SHEET_NAME.getName());
        XSSFRow firstRow = sheet.createRow(0);
        DecimalFormat df = new DecimalFormat("0.000");

        firstRow.createCell(0).setCellValue(PpmWorkbookCell.MODEL.getName());
        firstRow.createCell(1).setCellValue(PpmWorkbookCell.ITEM.getName());
        firstRow.createCell(2).setCellValue(PpmWorkbookCell.WORK_AREA.getName());
        firstRow.createCell(3).setCellValue(PpmWorkbookCell.WORK_AREA_DES.getName());
        firstRow.createCell(4).setCellValue(PpmWorkbookCell.LINE_AREA.getName());
        firstRow.createCell(5).setCellValue(PpmWorkbookCell.LINE_AREA_DES.getName());
        firstRow.createCell(6).setCellValue(PpmWorkbookCell.RESRCE.getName());
        firstRow.createCell(7).setCellValue(PpmWorkbookCell.DESCRIPTION.getName());
        firstRow.createCell(8).setCellValue(PpmWorkbookCell.ASSET.getName());
        firstRow.createCell(9).setCellValue(PpmWorkbookCell.BY_TIME.getName());
        firstRow.createCell(10).setCellValue(PpmWorkbookCell.PPM_THEORY.getName());
        firstRow.createCell(11).setCellValue(PpmWorkbookCell.PPM.getName());
        firstRow.createCell(12).setCellValue(PpmWorkbookCell.FIRST_CAL.getName());

        for (int i = 0; i < vos.size(); i++) {
            XSSFRow addRow = sheet.createRow(i + 1);
            addRow.createCell(0).setCellValue(vos.get(i).getModel());
            addRow.createCell(1).setCellValue(vos.get(i).getItem());
            addRow.createCell(2).setCellValue(vos.get(i).getWorkArea());
            addRow.createCell(3).setCellValue(vos.get(i).getWorkAreaDes());
            addRow.createCell(4).setCellValue(vos.get(i).getLineArea());
            addRow.createCell(5).setCellValue(vos.get(i).getLineAreaDes());
            addRow.createCell(6).setCellValue(vos.get(i).getResrce());
            addRow.createCell(7).setCellValue(vos.get(i).getDescription());
            addRow.createCell(8).setCellValue(vos.get(i).getAsset());
            addRow.createCell(9).setCellValue(vos.get(i).getByTime());
            if(vos.get(i).getPpmTheory()!=null){
                addRow.createCell(10).setCellValue(df.format(Double.valueOf(vos.get(i).getPpmTheory())));
            }
            if(vos.get(i).getPpm()!=null){
                addRow.createCell(11).setCellValue(df.format(Double.valueOf(vos.get(i).getPpm())));
            }
            XSSFCell cell=addRow.createCell(12);
            if(!Utils.isEmpty(vos.get(i).getFirstCal())){
                cell.setCellValue(Double.parseDouble(vos.get(i).getFirstCal().split("%")[0])/100);
            }else{
                cell.setCellValue(vos.get(i).getFirstCal());
            }
            cell.setCellStyle(style);
        }

        FileOutputStream outputStream = new FileOutputStream(file);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }
}
