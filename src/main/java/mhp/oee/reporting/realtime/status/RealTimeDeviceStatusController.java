package mhp.oee.reporting.realtime.status;

import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.extend.ResourceRealTimeReportPO;
import mhp.oee.utils.Utils;
import mhp.oee.web.Constants;

@RestController
public class RealTimeDeviceStatusController {

    @Autowired
    private RealTimeDeviceService realTimeDeviceService;

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @RequestMapping(value = "/web/rest/report/site/{site}/realtime_device_status_report", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceRealTimeReportPO>> search(@PathVariable("site") String site,
            @RequestParam(value = "work_center", required = true) String workCenter,
            @RequestParam(value = "line", required = false) String line,
            @RequestParam(value = "device_type", required = false) String deviceType,
            @RequestParam(value = "device_code", required = false) String deviceCode,
            HttpServletResponse res, HttpServletRequest request) {

        List<ResourceRealTimeReportPO> list = realTimeDeviceService.search(site, workCenter, line, deviceType, deviceCode);
        //DecimalFormat df = new DecimalFormat("0");
        for(int i=0;i<list.size();i++){
            if(list.get(i).getQtyTotal()!=null){
                String strQtyTotal=String.valueOf(/*df.format*/(list.get(i).getQtyTotal()));
                list.get(i).setStrQtyTotal(Utils.fmtMicrometer(strQtyTotal));
            }
            if(!Utils.isEmpty(list.get(i).getPpm())){
                list.get(i).setIntPPM(Double.valueOf(list.get(i).getPpm()));
            }
            if(!Utils.isEmpty(list.get(i).getHaltCount())){
                list.get(i).setIntHaltCount(Double.valueOf(list.get(i).getHaltCount()));
            }
            if(!Utils.isEmpty(list.get(i).getHaltCount())){
                list.get(i).setIntAlertCount(Double.valueOf(list.get(i).getAlertCount()));
            }
        }
        return ResponseEntity.ok(list);
    }

}