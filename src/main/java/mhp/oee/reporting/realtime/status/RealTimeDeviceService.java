package mhp.oee.reporting.realtime.status;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.po.extend.ResourceRealTimeReportPO;
import mhp.oee.reporting.component.RealTimeDeviceComponent;
import mhp.oee.utils.Utils;


@Service
@Transactional(rollbackFor = { BusinessException.class})
public class RealTimeDeviceService {

    @Autowired
    private RealTimeDeviceComponent realTimeDeviceComponent;

    public List<ResourceRealTimeReportPO> search(String site, String workCenter, String line, String deviceType, String deviceCode) {

        String deviceCodeString = "";
        if(deviceCode != null && deviceCode.length() > 0) {
            String[] deviceCodeArray = deviceCode.split(",");
            deviceCodeString = Utils.formatInParams("RESRCE", deviceCodeArray);
        }
        if(!Utils.isEmpty(line)){
        	String lineAreas = convertStrAddQuotes(line);
        	line="LINE_AREA IN "+lineAreas;
        }
        if (!Utils.isEmpty(deviceType)) {
            String[] deviceTypeArray = deviceType.split(",");
            deviceType = Utils.formatInParams("RESOURCE_TYPE", deviceTypeArray);
        }
        String workArea = null;
        if (!Utils.isEmpty(workCenter)) {
            String[] workAreaArray = workCenter.split(",");
            workArea = Utils.formatInParams("WORK_AREA", workAreaArray);
        }
        if (!Utils.isEmpty(workCenter)) {
            String[] workCenterArray = workCenter.split(",");
            workCenter = Utils.formatInParams("WORK_CENTER", workCenterArray);
        }
        List<ResourceRealTimeReportPO> list = realTimeDeviceComponent.search(site, workArea, workCenter, line, deviceType, deviceCodeString);

        return list;
    }

    private String convertStrAddQuotes(String str) {
    	String array[]=str.split(",");
        String strAddQuotes="(";
        for(int i=0;i<array.length;i++){
        	strAddQuotes+="''"+array[i]+"'',";
        }
        strAddQuotes=strAddQuotes.substring(0, strAddQuotes.length()-1)+")";
		return strAddQuotes;
	}


}
