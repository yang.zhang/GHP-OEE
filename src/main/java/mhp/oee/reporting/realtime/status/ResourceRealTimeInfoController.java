package mhp.oee.reporting.realtime.status;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.ServiceErrorException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.extend.ResourceRealTimeDetailPO;
import mhp.oee.po.extend.ResourceRealTimeInfoPO;
import mhp.oee.reporting.productionRealtimeDashboard.ResourceRealTimeInfoService;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.web.Constants;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by dinglj on 2017/4/28 028.
 */
@RestController
public class ResourceRealTimeInfoController {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private ResourceRealTimeInfoService resourceRealTimeInfoService;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    /**
     * 状态实时看板查询
     * @param request
     * @param dimension 查询维度
     * @param siteArr 工厂
     * @param workAreaArr 生产区域
     * @param lineAreaArr 拉线
     * @param sourceTypeArr 设备类型
     * @return
     */
    @RequestMapping(value = "/web/rest/report/realtime/main", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceRealTimeInfoPO>> readRealTimeInfo(HttpServletRequest request,
                                                                         @RequestParam("dimension") Integer dimension,
                                                                         @RequestParam("site_list") String[] siteArr,
                                                                         @RequestParam("work_area_list") String[] workAreaArr,
                                                                         @RequestParam("line_area_list") String[] lineAreaArr,
                                                                         @RequestParam("source_type_list") String[] sourceTypeArr){

        try {

            List<ResourceRealTimeInfoPO> list = this.resourceRealTimeInfoService.readRealTimeInfo(dimension,siteArr,workAreaArr,lineAreaArr,sourceTypeArr);
            return ResponseEntity.ok(list);

        }catch (ServiceErrorException e){
            //业务异常
            ExceptionVO exceptionVO = new ExceptionVO(e.getCode(),e.getLocalizedMessage(),null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        }catch (Exception e){
            //系统异常
            logger.error("看板查询异常",e);
            ExceptionVO exceptionVO = new ExceptionVO(ErrorCodeEnum.SYSTEM_INNER_ERROR.getErrorCode(),"系统内部异常",null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        }

    }

    /**
     * 设备实时看板 详情
     * @param request
     * @param dimension 查询纬度
     * @param dimensionCode 纬度代码
     * @param colorNum 三色灯颜色
     * @param siteArr 工厂
     * @param workAreaArr 生产区域
     * @param lineAreaArr 拉线
     * @param sourceTypeArr 设备类型
     * @return
     */
    @RequestMapping(value = "/web/rest/report/realtime/detail", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceRealTimeDetailPO>> readRealTimeDetail(HttpServletRequest request,
                                                                         @RequestParam("dimension") Integer dimension,
                                                                         @RequestParam("dimensionCode") String dimensionCode,
                                                                         @RequestParam("color") Integer colorNum,
                                                                         @RequestParam("site_list") String[] siteArr,
                                                                         @RequestParam("work_area_list") String[] workAreaArr,
                                                                         @RequestParam("line_area_list") String[] lineAreaArr,
                                                                         @RequestParam("source_type_list") String[] sourceTypeArr){

        try {

            List<ResourceRealTimeDetailPO> list = this.resourceRealTimeInfoService.readRealTimeDetail(dimension,dimensionCode,colorNum,siteArr,workAreaArr,lineAreaArr,sourceTypeArr);
            return ResponseEntity.ok(list);

        }catch (ServiceErrorException e){
            //业务异常
            ExceptionVO exceptionVO = new ExceptionVO(e.getCode(),e.getLocalizedMessage(),null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        }catch (Exception e){
            //系统异常
            ExceptionVO exceptionVO = new ExceptionVO(ErrorCodeEnum.SYSTEM_INNER_ERROR.getErrorCode(),"系统内部异常",null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        }

    }

}
