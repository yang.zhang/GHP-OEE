package mhp.oee.reporting.oeetrend;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.reporting.component.OeeTrendComponent;
import mhp.oee.reporting.oee.OeeWorkbookCell;
import mhp.oee.reporting.oeedetail.OeeDetailWorkbookCell;
import mhp.oee.po.extend.OeeOeePO;
import mhp.oee.po.extend.OeeOeeTrendPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.OeeOeeTrendVO;

import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Created by SuMingzhi on 2016/12/20.
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class OeeTrendReportService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private OeeTrendComponent oeeTrendComponent;



    public List<Object> selectOeeOutputReportExcel(String site, String workCenter,String resourceType, String resourceBo, String endDateTime, String shiftLike,String startDateTime,String pro,String model,
    		String byTimeType,String byResrceType,String workCenterLink){
    	DecimalFormat df = new DecimalFormat("0.00");
    	List XValues = new ArrayList();
        try {
			startDateTime = Utils.parseReportDateDay(startDateTime);
			endDateTime = Utils.parseReportDateDay(endDateTime);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
        List<Object> returnTotalList = new LinkedList<Object>();
        Map<String,Map<String,Map<String,Double>>> returnMap = new HashMap<String,Map<String,Map<String,Double>>>();
        //<by类型名，Map<oee,Map<X,Y>>>
        Map<String,Map<String,List>> returnMapFormat = new HashMap<String,Map<String,List>>();
        List<OeeOeeTrendPO> returnList = new ArrayList<OeeOeeTrendPO>();
        try{
        	//通过设备维度循环，判断by what 然后高级for
        	if(byResrceType.equals("T")){
        		String[] resTypeEach = resourceType.split(",");
        		for(String res:resTypeEach){
        			List<Object> totalListSplit = oeeTrendComponent.selectOeeTrendList(site, workCenter,res, resourceBo, endDateTime, shiftLike, startDateTime,pro,model,byTimeType, workCenterLink,XValues);
        			if(totalListSplit==null||totalListSplit.size()<1)continue;
        			List<OeeOeeTrendPO> returnListSplit = (List<OeeOeeTrendPO>)totalListSplit.get(0);
        			Map<String,Map<String,Double>> returnMapSplit = (Map<String,Map<String,Double>>)totalListSplit.get(1);
        			if(returnListSplit==null||returnListSplit.size()<1)continue;
        			returnMap.put(returnListSplit.get(0).getResrceType(), returnMapSplit);
        			returnList.addAll(returnListSplit);
        		}
        	}else if(byResrceType.equals("I")){
        		String[] modelEach = model.split(",");
        		for(String res:modelEach){
        			List<Object> totalListSplit = oeeTrendComponent.selectOeeTrendList(site, workCenter,resourceType,resourceBo, endDateTime, shiftLike, startDateTime,pro,res,byTimeType, workCenterLink, XValues);
        			if(totalListSplit==null||totalListSplit.size()<1)continue;
        			List<OeeOeeTrendPO> returnListSplit = (List<OeeOeeTrendPO>)totalListSplit.get(0);
        			Map<String,Map<String,Double>> returnMapSplit = (Map<String,Map<String,Double>>)totalListSplit.get(1);
        			if(returnListSplit==null||returnListSplit.size()<1)continue;
        			returnMap.put(returnListSplit.get(0).getItem(), returnMapSplit);
        			returnList.addAll(returnListSplit);
        		}
        	}else if(byResrceType.equals("L")){
        		String[] workCenterLinkEach = workCenterLink.split(",");
        		for(String res:workCenterLinkEach){
        			List<Object> totalListSplit = oeeTrendComponent.selectOeeTrendList(site, workCenter,resourceType,resourceBo, endDateTime, shiftLike, startDateTime,pro,model,byTimeType, res,XValues);
        			if(totalListSplit==null||totalListSplit.size()<1)continue;
        			List<OeeOeeTrendPO> returnListSplit = (List<OeeOeeTrendPO>)totalListSplit.get(0);
        			Map<String,Map<String,Double>> returnMapSplit = (Map<String,Map<String,Double>>)totalListSplit.get(1);
        			if(returnListSplit==null||returnListSplit.size()<1)continue;
        			returnMap.put(returnListSplit.get(0).getWorkCenter(), returnMapSplit);
        			returnList.addAll(returnListSplit);
        		}
        	}else if(byResrceType.equals("W")){
        		String[] workCenterEach = workCenter.split(",");
        		for(String res:workCenterEach){
        			List<Object> totalListSplit = oeeTrendComponent.selectOeeTrendList(site, res,resourceType,resourceBo, endDateTime, shiftLike, startDateTime,pro,model,byTimeType, workCenterLink,XValues);
        			if(totalListSplit==null||totalListSplit.size()<1)continue;
        			List<OeeOeeTrendPO> returnListSplit = (List<OeeOeeTrendPO>)totalListSplit.get(0);
        			Map<String,Map<String,Double>> returnMapSplit = (Map<String,Map<String,Double>>)totalListSplit.get(1);
        			if(returnListSplit==null||returnListSplit.size()<1)continue;
        			returnMap.put(returnListSplit.get(0).getWorkArea(), returnMapSplit);
        			returnList.addAll(returnListSplit);
        		}
        	}else{ //if(byResrceType.equals("按设备编码"))
        		String[] resEach = resourceBo.split(",");
        		for(String res:resEach){
        			List<Object> totalListSplit = oeeTrendComponent.selectOeeTrendList(site, workCenter,resourceType,res, endDateTime, shiftLike, startDateTime,pro,model,byTimeType, workCenterLink,XValues);
        			if(totalListSplit==null||totalListSplit.size()<1)continue;
        			List<OeeOeeTrendPO> returnListSplit = (List<OeeOeeTrendPO>)totalListSplit.get(0);
        			Map<String,Map<String,Double>> returnMapSplit = (Map<String,Map<String,Double>>)totalListSplit.get(1);
        			if(returnListSplit==null||returnListSplit.size()<1)continue;
        			returnMap.put(returnListSplit.get(0).getResrce(), returnMapSplit);
        			returnList.addAll(returnListSplit);
        		}
        	}
        }catch(Exception e){
            e.printStackTrace();
        	System.out.println("查询出错，请核实输入查询条件是否满足业务需求！");
        	return null;
        }
        
        Map<String,List> OeeValues = new HashMap<String,List>();
        Map<String,List> AValues = new HashMap<String,List>();
        Map<String,List> PValues = new HashMap<String,List>();
        Map<String,List> QValues = new HashMap<String,List>();
        //<by类型名，Map<oee,Map<X,Y>>>
        Set<Entry<String, Map<String, Map<String, Double>>>> entrySet = returnMap.entrySet();
        for(Entry<String, Map<String, Map<String, Double>>> et:returnMap.entrySet()){
        	String byName = et.getKey();
        	OeeValues.put("x", XValues);
        	AValues.put("x", XValues);
        	PValues.put("x", XValues);
        	QValues.put("x", XValues);
        	List YValuesOee = new ArrayList();
        	List YValuesA = new ArrayList();
        	List YValuesP = new ArrayList();
        	List YValuesQ = new ArrayList();
        	for(Object xValue:XValues){
        		Double yValueOee = 0.00;
        		Double yValueA = 0.00;
        		Double yValueP = 0.00;
        		Double yValueQ = 0.00;
        		try{
        			yValueOee = returnMap.get(byName).get((String)xValue).get("oee");
        			yValueA = returnMap.get(byName).get((String)xValue).get("a");
        			yValueP = returnMap.get(byName).get((String)xValue).get("p");
        			yValueQ = returnMap.get(byName).get((String)xValue).get("q");
        		}catch(Exception e){
        			System.out.println("该设备类型在对应月份无纵坐标值");
        		}
        		YValuesOee.add(df.format(yValueOee));
        		YValuesA.add(df.format(yValueA));
        		YValuesP.add(df.format(yValueP));
        		YValuesQ.add(df.format(yValueQ));
        	}
        	OeeValues.put(byName, YValuesOee);
        	AValues.put(byName, YValuesA);
        	PValues.put(byName, YValuesP);
        	QValues.put(byName, YValuesQ);
        }
        returnMapFormat.put("a",AValues);
        returnMapFormat.put("p",PValues);
        returnMapFormat.put("q",QValues);
        returnMapFormat.put("oee",OeeValues );
        
        
        Collections.sort(returnList,new Comparator<OeeOeeTrendPO>(){

			@Override
			public int compare(OeeOeeTrendPO o1, OeeOeeTrendPO o2) {
				int flag = 0;
					flag =  o1.getByTimeChart().compareTo(o2.getByTimeChart());
					if(flag==0){
						flag = o1.getWorkArea().compareTo(o2.getWorkArea());
						if(flag==0){
							flag = o1.getWorkCenter().compareTo(o2.getWorkCenter());
						}
					}
				
				return flag;
			}});
       
        returnTotalList.add(0, returnList);
        returnTotalList.add(1, returnMapFormat);
        return returnTotalList;
    }
    
    
    
    public void commitExportExcel(List<OeeOeeTrendPO> vos, File file, String byTimeType, String byResrceType, String pro) throws IOException {
    	 XSSFWorkbook workbook = new XSSFWorkbook();
         XSSFCellStyle cellStyle = workbook.createCellStyle();
         XSSFCellStyle styleOfOee = workbook.createCellStyle();
         styleOfOee.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%")); 
         XSSFSheet sheet = workbook.createSheet(OeeTrendWorkbookCell.SHEET_NAME.getName());
         XSSFRow firstRow = sheet.createRow(0);
         XSSFCell createCell0 = firstRow.createCell(0);
         createCell0.setCellStyle(cellStyle);
         createCell0.setCellValue(OeeTrendWorkbookCell.DATE.getName());
         XSSFCell createCell1 = firstRow.createCell(1);
         createCell1.setCellStyle(cellStyle);
         createCell1.setCellValue(OeeTrendWorkbookCell.WORK_AREA.getName());
         XSSFCell createCell2 = firstRow.createCell(2);
         createCell2.setCellStyle(cellStyle);
         createCell2.setCellValue(OeeTrendWorkbookCell.WORK_LINK.getName());
         XSSFCell createCell3 = firstRow.createCell(3);
         createCell3.setCellStyle(cellStyle);
         createCell3.setCellValue(OeeTrendWorkbookCell.RESRCE_TYPE.getName());
         XSSFCell createCell4 = firstRow.createCell(4);
         createCell4.setCellStyle(cellStyle);
         createCell4.setCellValue(OeeTrendWorkbookCell.RESRCE_CODE.getName());
         XSSFCell createCell5 = firstRow.createCell(5);
         createCell5.setCellStyle(cellStyle);
         createCell5.setCellValue(OeeTrendWorkbookCell.ASSET.getName());
         XSSFCell createCell6 = firstRow.createCell(6);
         createCell6.setCellStyle(cellStyle);
         createCell6.setCellValue(OeeTrendWorkbookCell.RESRCE_DESCRIPTION.getName());
         XSSFCell createCell7 = firstRow.createCell(7);
         createCell7.setCellStyle(cellStyle);
         createCell7.setCellValue(OeeTrendWorkbookCell.OEE.getName());
         XSSFCell createCell8 = firstRow.createCell(8);
         createCell8.setCellStyle(cellStyle);
         createCell8.setCellValue(OeeTrendWorkbookCell.AVAILABILITY.getName());
         XSSFCell createCell9 = firstRow.createCell(9);
         createCell9.setCellStyle(cellStyle);
         createCell9.setCellValue(OeeTrendWorkbookCell.PERFORMANCE.getName());
         XSSFCell createCell10 = firstRow.createCell(10);
         createCell10.setCellStyle(cellStyle);
         createCell10.setCellValue(OeeTrendWorkbookCell.QUALITY.getName());
         if("S".equals(byTimeType)){
        	 XSSFCell createCell11 = firstRow.createCell(11);
        	 createCell11.setCellStyle(cellStyle);
        	 createCell11.setCellValue(OeeTrendWorkbookCell.ITEM.getName());
        	 XSSFCell createCell12 = firstRow.createCell(12);
        	 createCell12.setCellStyle(cellStyle);
        	 createCell12.setCellValue(OeeTrendWorkbookCell.PRD.getName());
         }else if("I".equals(byResrceType)){
        	 XSSFCell createCell11 = firstRow.createCell(11);
        	 createCell11.setCellStyle(cellStyle);
        	 createCell11.setCellValue(OeeTrendWorkbookCell.ITEM.getName());
         }else if(pro!=null&&!"".equals(pro)){
        	 XSSFCell createCell11 = firstRow.createCell(11);
        	 createCell11.setCellStyle(cellStyle);
        	 createCell11.setCellValue(OeeTrendWorkbookCell.PRD.getName());
         }
         if(vos!=null){
         	for (int i = 0; i < vos.size(); i++) {
         		XSSFRow addRow = sheet.createRow(i + 1);
         		OeeOeeTrendPO oeeDetail = vos.get(i);
         		if("S".equals(byTimeType))
         			addRow.createCell(0).setCellValue(oeeDetail.getByTime());
         		else
         			addRow.createCell(0).setCellValue(oeeDetail.getByTimeChart());
         		addRow.createCell(1).setCellValue(oeeDetail.getWorkArea());
         		addRow.createCell(2).setCellValue(oeeDetail.getWorkCenter());
         		addRow.createCell(3).setCellValue(oeeDetail.getResrceType());
         		addRow.createCell(4).setCellValue(oeeDetail.getResrce());
         		addRow.createCell(5).setCellValue(oeeDetail.getAsset());
         		addRow.createCell(6).setCellValue(oeeDetail.getResrceDescription());
         		XSSFCell createCell07 = addRow.createCell(7);
         		createCell07.setCellStyle(styleOfOee);
         		if(oeeDetail.getOee()!=null&&!oeeDetail.getOee().isNaN())
         			createCell07.setCellValue(oeeDetail.getOee()/100.00);
         		else 
         			createCell07.setCellValue("N/A");
         		XSSFCell createCell08 = addRow.createCell(8);
         		createCell08.setCellStyle(styleOfOee);
         		if(oeeDetail.getA()!=null&&!oeeDetail.getA().isNaN())
         			createCell08.setCellValue(oeeDetail.getA()/100.00);
         		else 
         			createCell08.setCellValue("N/A");
         		XSSFCell createCell09 = addRow.createCell(9);
         		createCell09.setCellStyle(styleOfOee);
         		if(oeeDetail.getP()!=null&&!oeeDetail.getP().isNaN())
         			createCell09.setCellValue(oeeDetail.getP()/100.00);
         		else 
         			createCell09.setCellValue("N/A");
         		XSSFCell createCell010 = addRow.createCell(10);
         		createCell010.setCellStyle(styleOfOee);
         		if(oeeDetail.getQ()!=null&&!oeeDetail.getQ().isNaN())
         			createCell010.setCellValue(oeeDetail.getQ()/100.00);
         		else 
         			createCell010.setCellValue("N/A");
         		if("S".equals(byTimeType)){
         			addRow.createCell(11).setCellValue(oeeDetail.getItem());
         			addRow.createCell(12).setCellValue(oeeDetail.getUserId());
         		}else if("I".equals(byResrceType)){
         			addRow.createCell(11).setCellValue(oeeDetail.getItem());
         		}else if(pro!=null&&!"".equals(pro)){
         			addRow.createCell(11).setCellValue(oeeDetail.getUserId());
         		}
         	}
         }

         FileOutputStream outputStream = new FileOutputStream(file);
         workbook.write(outputStream);
         outputStream.close();
         workbook.close();
    }
}