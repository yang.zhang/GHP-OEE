package mhp.oee.reporting.oeetrend;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.extend.OeeOeePO;
import mhp.oee.po.extend.OeeOeeTrendPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.OeeOeeTrendVO;
import mhp.oee.vo.ProductionOutputFirstAndFollowUpVO;
import mhp.oee.web.Constants;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SuMingzhi on 2016/12/26.
 */
@RestController
public class OeeTrendReportController {

	@InjectableLogger
	private static Logger logger;

	private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

	@Autowired
	OeeTrendReportService oeeTrendReportService;

	@RequestMapping(value = "/web/rest/report/oeetrend/site/{site}/report", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
	public ResponseEntity<?> reportingShow(@PathVariable("site") String site,
			@RequestParam(value = "work_area", required = true) String workCenter,
			@RequestParam(value = "resource_type", required = true) String resourceType,
			@RequestParam(value = "resource_code", required = true) String resourceBo,
			@RequestParam(value = "end_date", required = true) String endDateTime,
			@RequestParam(value = "shift", required = true) String shiftLike,
			@RequestParam(value = "start_date", required = true) String startDateTime,
			@RequestParam(value = "pro", required = true) String pro,
			@RequestParam(value = "model", required = true) String model,
			@RequestParam(value = "byTimeType", required = true) String byTimeType,
			@RequestParam(value = "byResrceType", required = true) String byResrceType,
			@RequestParam(value = "line_area", required = true) String workCenterLink, HttpServletResponse response) {
		String trace = String.format(
				"[OeeTrendReportController.reportingShow] request : site=%s,workCenter=%s,resourceType=%s, resourceBo=%s, endDateTime=%s,shiftLike=%s, startDateTime=%s,pro=%s,model=%s,byTimeType=%s,byResrceType%s,workCenterLink=%s",
				site, workCenter, resourceType, resourceBo, endDateTime, shiftLike, startDateTime, pro, model,
				byTimeType, byResrceType, workCenterLink);
		logger.debug(">> " + trace);
		List<Object> vosBefore = oeeTrendReportService.selectOeeOutputReportExcel(site, workCenter, resourceType, resourceBo,
				endDateTime, shiftLike, startDateTime, pro, model, byTimeType, byResrceType, workCenterLink);
		List<OeeOeeTrendPO> returnTableList = (List<OeeOeeTrendPO>) vosBefore.get(0);
		DecimalFormat df = new DecimalFormat("0.00");
		List<OeeOeeTrendVO> rtList = new ArrayList<OeeOeeTrendVO>();
		for (OeeOeeTrendPO po : returnTableList) {
			OeeOeeTrendVO vo = new OeeOeeTrendVO();
			vo.setA(df.format(po.getA()));
			vo.setAsset(po.getAsset());
			vo.setByTime(po.getByTime());
			vo.setByTimeChart(po.getByTimeChart());
			vo.setByTimeMonth(po.getByTimeMonth());
			vo.setByTimeYear(po.getByTimeYear());
			vo.setItem(po.getItem());
			vo.setModel(po.getModel());
			if(po.getOee() != null){
				vo.setOee(df.format(po.getOee()));
			}
			
			vo.setOeeValue(po.getOeeValue());
			if(po.getP() != null){
				vo.setP(df.format(po.getP()));
			}
			vo.setPpmTherory(po.getPpmTherory());
			if(po.getQ() != null){
				vo.setQ(df.format(po.getQ()));
				
			}
			vo.setQty(po.getQty());
			vo.setResrce(po.getResrce());
			vo.setResrceDescription(po.getResrceDescription());
			vo.setResrceType(po.getResrceType());
			vo.setShiftStartTime(po.getShiftStartTime());
			vo.setUserId(po.getUserId());
			vo.setWorkArea(po.getWorkArea());
			vo.setWorkCenter(po.getWorkCenter());
			rtList.add(vo);
		}
		
		List<Object> vos = new ArrayList<Object>();
		vos.add(rtList);
		vos.add(vosBefore.get(1));
		logger.debug("<< [OeeReportController.reportingShow] response : list");
		return ResponseEntity.ok(vos);
	}

	@RequestMapping(value = "/web/rest/report/oeetrend/site/{site}/export_excel", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
	public ResponseEntity<?> reportingExport(@PathVariable("site") String site,
			@RequestParam(value = "work_area", required = true) String workCenter,
			@RequestParam(value = "resource_type", required = true) String resourceType,
			@RequestParam(value = "resource_code", required = true) String resourceBo,
			@RequestParam(value = "end_date", required = true) String endDateTime,
			@RequestParam(value = "shift", required = true) String shiftLike,
			@RequestParam(value = "start_date", required = true) String startDateTime,
			@RequestParam(value = "pro", required = true) String pro,
			@RequestParam(value = "model", required = true) String model,
			@RequestParam(value = "byTimeType", required = true) String byTimeType,
			@RequestParam(value = "byResrceType", required = true) String byResrceType,
			@RequestParam(value = "line_area", required = true) String workCenterLink, HttpServletResponse response) {
		String trace = String.format(
				"[OeeTrendReportController.reportingShow] request : site=%s,workCenter=%s,resourceType=%s, resourceBo=%s, endDateTime=%s,shiftLike=%s, startDateTime=%s,pro=%s,model=%s,byTimeType=%s,byResrceType%s,workCenterLink=%s",
				site, workCenter, resourceType, resourceBo, endDateTime, shiftLike, startDateTime, pro, model,
				byTimeType, byResrceType, workCenterLink);
		logger.debug(">> " + trace);
		try {
			File file = new File("download.xlsx");
			List<Object> totalList = oeeTrendReportService.selectOeeOutputReportExcel(site, workCenter, resourceType,
					resourceBo, endDateTime, shiftLike, startDateTime, pro, model, byTimeType, byResrceType,
					workCenterLink);
			List<OeeOeeTrendPO> vos = (List<OeeOeeTrendPO>) totalList.get(0);
			oeeTrendReportService.commitExportExcel(vos, file, byTimeType, byResrceType, pro);
			Utils.generateDownloadFile(response, file);
			FileUtils.deleteQuietly(file);
		} catch (Exception e) {
			ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
			logger.error("exportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
		}
		logger.debug("<< [OeeReportController.reportingShow] response : void");
		return ResponseEntity.ok(null);
	}

}