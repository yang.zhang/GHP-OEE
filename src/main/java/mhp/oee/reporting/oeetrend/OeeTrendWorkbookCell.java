package mhp.oee.reporting.oeetrend;

/**
 * Created by SuMingzhi on 2016/01/16.
 */
public enum OeeTrendWorkbookCell {
    SHEET_NAME("OEE趋势报表"),
    DATE("日期"),
    WORK_AREA("生产区域"),
    WORK_LINK("拉线"),
    RESRCE_TYPE("设备类型"),
    RESRCE_CODE("设备编码"),
    ASSET("资产号"),
    RESRCE_DESCRIPTION("设备描述"),
    ITEM("物料编码"),
    OEE("OEE"),
    AVAILABILITY("时间利用率"),
    PERFORMANCE("性能率"),
    QUALITY("优率"),
    PRD("生产操作员");
    private String name;

    private OeeTrendWorkbookCell(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
