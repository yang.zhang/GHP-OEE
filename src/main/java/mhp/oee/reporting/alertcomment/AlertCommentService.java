package mhp.oee.reporting.alertcomment;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.handle.ActivityBOHandle;
import mhp.oee.common.handle.ResourcePlcInfoBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.dao.gen.AlertCommentPOMapper;
import mhp.oee.dao.gen.ResourceAlertLogPOMapper;
import mhp.oee.dao.gen.ResourcePlcInfoPOMapper;
import mhp.oee.po.gen.AlertCommentPO;
import mhp.oee.po.gen.AlertCommentPOExample;
import mhp.oee.po.gen.ResourceAlertLogPO;
import mhp.oee.po.gen.ResourceAlertLogPOExample;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourcePlcInfoPO;
import mhp.oee.po.gen.ResourcePlcInfoPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.AlertCommentVo;
import mhp.oee.vo.ResourceVO;

@Service
@Transactional(rollbackFor = { Exception.class })
public class AlertCommentService {
    
    private static final String RESOURCE_NO_EXIT = "ALERT_COMMENT";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	@InjectableLogger
    private static Logger logger;
	@Autowired
	ResourceAlertLogPOMapper resourceAlertLogPOMapper;
	@Autowired
	AlertCommentPOMapper alertCommentPOMapper;
	@Autowired
	ResourcePlcInfoPOMapper resourcePlcInfoPOMapper;
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public AlertCommentVo commentQuery(String resrce,String site) throws Exception{
		ResourceAlertLogPOExample example = new ResourceAlertLogPOExample();	
		example.setOrderByClause("date_time desc ");
		//example.createCriteria().andResrceEqualTo(resrce).
		//andDateTimeBetween(Utils.addMinuteToDate(sdf.parse("2017-01-13 10:50:08"), 30, false),
				//sdf.parse("2017-01-13 10:50:08"));
		Date now = new Date();
		example.createCriteria().andResrceEqualTo(resrce).andSiteEqualTo(site).andDateTimeBetween(Utils.addMinuteToDate(now, 10, false),now);
		//总条数
		int commentSize = resourceAlertLogPOMapper.countByExample(example); 		
		List<ResourceAlertLogPO> pos = resourceAlertLogPOMapper.selectByExample(example);
		
		//备注信息
		String spit_resrce=resrce.substring(0,4);
		AlertCommentPOExample alert_example = new AlertCommentPOExample();
		alert_example.createCriteria().andResourceTypeEqualTo(spit_resrce);
		List<AlertCommentPO> alert_list = alertCommentPOMapper.selectByExample(alert_example);		
		 AlertCommentVo vo = new AlertCommentVo();
		 vo.setCommentSize(commentSize);
		 vo.setPos(pos);
		 vo.setCommentList(alert_list);
		return vo;
	} 
	
	
	public int updateAlert(String resrce,String site,String date_time,String description,String alert_comment,String comment_code) throws ParseException, UnsupportedEncodingException{
		//description = new String(description.getBytes("iso8859-1"), "utf-8");
		//alert_comment = new String(alert_comment.getBytes("iso8859-1"), "utf-8");
		ResourceAlertLogPOExample example = new ResourceAlertLogPOExample();
		example.createCriteria().andResrceEqualTo(resrce).andSiteEqualTo(site).andDateTimeEqualTo(sdf.parse(date_time)).andDescriptionEqualTo(description);
		List<ResourceAlertLogPO> pos = resourceAlertLogPOMapper.selectByExample(example);
		if(pos!=null&&pos.size()>0){
			ResourceAlertLogPO po = pos.get(0);
			po.setAlertComment(alert_comment);
			po.setCommentCode(comment_code);
			resourceAlertLogPOMapper.updateByExample(po, example);
		}else{
			pos = new ArrayList<ResourceAlertLogPO>();
		}
		
		return pos.size();
	}


    public ResourceVO getResourceByIp(String site,String ip) throws BusinessException {
        ResourcePlcInfoPOExample example = new ResourcePlcInfoPOExample();
        
        String[] ipArray = ip.split(",");
        List<String> ips = new ArrayList<String>();
        Collections.addAll(ips, ipArray);
        
        example.createCriteria().andSiteEqualTo(site).andPcIpIn(ips);
        List<ResourcePlcInfoPO> list = resourcePlcInfoPOMapper.selectByExample(example);
        if(list.size()>0){
            ResourcePlcInfoPO po=list.get(0);
            ResourceVO vo = new ResourceVO();
            vo.setResrce(new ResourcePlcInfoBOHandle(po.getSite(),po.getResourceBo()).getResrce());
            return vo;
        }else{
            throw new BusinessException(ErrorCodeEnum.IP_ERROR,"The host computer IP does not match the device ID");
        }
    }

}
