package mhp.oee.reporting.alertcomment;

import java.util.List;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.gen.AlertCommentPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.AlertComentManagerVO;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.web.Constants;

/**
 * 报警备注管理Controller
 * @author SuMingzhi
 * @Time 2017-3-14
 */
@RestController
public class AlertRemarkManageController {

	@InjectableLogger
	private static Logger logger;
	private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

	@Autowired
	AlertRemarkManageService alertRemarkManageService;

	/**
	 * add delete update
	 * @param List<AlertCommentVO>
	 * @return
	 */
	@RequestMapping(value = "/web/rest/alert/comment/site/{site}/change", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeAlertComment(@PathVariable("site") String site, @RequestBody List<AlertCommentVO> vos) {
		try {
			alertRemarkManageService.change(site,vos);
		} catch (BusinessException e) {
            ExceptionVO exceptionVO = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        }
        return ResponseEntity.ok(null);
    }
	
	/**
	 * delete validate
	 * @param List<AlertCommentVO>
	 * @return
	 */
	@RequestMapping(value = "/web/rest/alert/comment/site/{site}/userd", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> deleteValidate(@PathVariable("site") String site, 
            @RequestParam String comment_code) {
		boolean resultFlag;
		try {
			resultFlag = alertRemarkManageService.validateRemark(site,comment_code);
		} catch (Exception e) {
			logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
		}
        return ResponseEntity.ok(resultFlag);
    }
	
	/**
	 * select
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/web/rest/alert/comment/site/{site}/list", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> list(@PathVariable("site") String site,
            @RequestParam("resource_type") String resourceType,
            @RequestParam("enabled") String enabled) {
		List<AlertCommentVO> list =  null;
		try {
		    list = alertRemarkManageService.list(site,resourceType,enabled);
		} catch (Exception e) {
			logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
		}
        return ResponseEntity.ok(list);
    }
	/*	
	*//**
	 * select addLike
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/web/rest/report/alert/site/{site}/remark/selectlike", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> selectRemarkLike(@PathVariable("site") String site,
    		@RequestParam(value = "description", required = false) String description) {
		List<AlertCommentPO> alertCommentPOList =  null;
		try {
			alertCommentPOList = alertRemarkManageService.selectRemarkLike(site, description);
		} catch (Exception e) {
			logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
		}
        logger.debug("<< [AlertRemarkManageController.selectRemark]  response : "  + gson.toJson(alertCommentPOList));
        return ResponseEntity.ok(alertCommentPOList);
    }
	
	
}
