package mhp.oee.reporting.alertcomment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.aler.comment.AlertCommentComponent;
import mhp.oee.dao.extend.AlertCommentRemarkMapper;
import mhp.oee.dao.gen.AlertCommentPOMapper;
import mhp.oee.dao.gen.ResourceAlertLogPOMapper;
import mhp.oee.po.gen.AlertCommentPO;
import mhp.oee.po.gen.AlertCommentPOExample;
import mhp.oee.po.gen.AlertCommentPOExample.Criteria;
import mhp.oee.utils.Utils;
import mhp.oee.po.gen.ReasonAlertLogPOExample;
import mhp.oee.po.gen.ResourceAlertLogPO;
import mhp.oee.po.gen.ResourceAlertLogPOExample;
import mhp.oee.po.gen.WorkCenterPO;
import mhp.oee.vo.AlertComentManagerVO;

/**
 * 报警备注管理Service
 * 
 * @author SuMingzhi
 * @Time 2017-3-14
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class AlertRemarkManageService {
	@InjectableLogger
	private static Logger logger;
	@Autowired
	AlertCommentPOMapper alertCommentPOMapper;
	@Autowired
	ResourceAlertLogPOMapper resourceAlertLogPOMapper;
	@Autowired
	private AlertCommentComponent alertCommentComponent;
	@Autowired
	private AlertCommentRemarkMapper alertCommentRemarkMapper;
	

	public List<AlertCommentVO> list(String site,String resourceTypes,String enabled) {
	    //数组转成list
	    String[] rt=resourceTypes.split(",");
	    List<String> rts=new LinkedList<String>();
	    Collections.addAll(rts, rt);
		AlertCommentPOExample example = new AlertCommentPOExample();
		example.createCriteria().andSiteEqualTo(site).andEnabledEqualTo(enabled).andResourceTypeIn(rts);
		example.setOrderByClause("COMMENT_CODE");
		List<AlertCommentPO> poList = alertCommentPOMapper.selectByExample(example);
		List<AlertCommentVO> list = new LinkedList<AlertCommentVO>();
		for(AlertCommentPO po:poList){
		    AlertCommentVO vo = Utils.copyObjectProperties(po, AlertCommentVO.class);
		    vo.setResourceTypeDesc(alertCommentComponent.getResourceTypeDesc(site,vo.getResourceType()));
		    list.add(vo);
		}
		return list;
	}
	
	public List<AlertCommentPO> selectRemarkLike(String site, String description) {
		//多个关键词模糊搜索
    	String[] descriptions = Utils.ConvertStrtoArrayLike(description);
		List<AlertCommentPO> resultList = alertCommentRemarkMapper.selectAlertCommentPOLikeMore(site, descriptions);
		
		/*AlertCommentPOExample alertCommentPOExample = new AlertCommentPOExample();
		Criteria createCriteria = alertCommentPOExample.createCriteria();
		createCriteria.andDescriptionLike(description);
		createCriteria.andSiteEqualTo(site);
		 = alertCommentPOMapper.selectByExample(alertCommentPOExample);*/
		return resultList;
	}


	public void change(String site, List<AlertCommentVO> vos) throws BusinessException {
		for (AlertCommentVO vo : vos) {
			if (vo.getViewActionCode().equals('C')) {
			    alertCommentComponent.createAlertComment(vo);
			} else if (vo.getViewActionCode().equals('U')) {
			    alertCommentComponent.updateAlertComment(vo);
			} else if (vo.getViewActionCode().equals('D')) {
			    alertCommentComponent.deleteAlertComment(vo);
			} else {
				logger.error("PlantService.cudPlcCategoryChanges() : Invalid viewActionCode '"
						+ vo.getViewActionCode() + "'.");
			}
		}
	}
	public boolean validateRemark(String site,String commentCode) {
		ResourceAlertLogPOExample example = new ResourceAlertLogPOExample();
		example.createCriteria().andSiteEqualTo(site).andCommentCodeEqualTo(commentCode);
		List<ResourceAlertLogPO> resourceAlertLogPOList = resourceAlertLogPOMapper.selectByExample(example);
		return resourceAlertLogPOList.size()>0? true : false;
	}
	public static void main(String[] args) {
	    String[] userid = {"aa","bb","cc"};
	    List<String> userList = new ArrayList<String>();
	    Collections.addAll(userList, userid);
	    System.err.println(userList.size());
    }

	
}
