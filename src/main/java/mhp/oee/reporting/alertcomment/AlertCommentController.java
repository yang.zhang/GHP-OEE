package mhp.oee.reporting.alertcomment;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.AlertCommentVo;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ResourceVO;
import mhp.oee.web.Constants;
import mhp.oee.web.angular.authorization.management.IPVO;

@RestController
public class AlertCommentController {

	@InjectableLogger
	private static Logger logger;
	private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

	@Autowired
	AlertCommentService alertCommentService;

	/**
	 * 查询报警信息列表
	 * @param resrce
	 * @return
	 */
	
	@RequestMapping(value = "/web/rest/report/alert/site/{site}/comment_query", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<AlertCommentVo> commentQuery(@PathVariable("site") String site,
            @RequestParam(value = "resrce", required = true) String resrce){
        String trace = String.format("[AlertCommentController.commentQuery] request : site=%s,resrce=%s", resrce,site);
        logger.debug(">> " + trace);
        AlertCommentVo vo =null;       
		try {
			vo = alertCommentService.commentQuery(resrce,site);
			
		} catch (Exception e) {
			logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
		}
        logger.debug("<< [AlertCommentController.commentQuery]  response : "  + gson.toJson(vo));       
        return ResponseEntity.ok(vo);
    }
	
	/**
	 * 更新报警信息的备注信息
	 * @param resrce
	 * @return
	 */
	
	@RequestMapping(value = "/web/rest/report/alert/site/{site}/update_alert", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> updateAlert(@PathVariable("site") String site,
            @RequestParam(value = "resrce", required = true) String resrce,           
            @RequestParam(value = "date_time", required = true) String date_time,
            @RequestParam(value = "description", required = true) String description,
            @RequestParam(value = "alert_comment", required = true) String alert_comment,
            @RequestParam(value = "comment_code", required = true) String comment_code){
        String trace = String.format("[AlertCommentController.updateAlert] request : resrce=%s,site=%s,date_time=%s,description=%s,alert_comment=%s,comment_code=%s", resrce,site,date_time,description,alert_comment,comment_code);
        logger.debug(">> " + trace);
		try {//new String(description.getBytes("iso8859-1"), "utf-8")
			alertCommentService.updateAlert(resrce,site,date_time,description ,alert_comment,comment_code);
		} catch (Exception e) {
			logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
		}
        logger.debug("<< [AlertCommentController.commentQuery]  response : "  + gson.toJson(null));
        return ResponseEntity.ok(null);
    }
	
	@RequestMapping(value = "/web/rest/report/alert/site/{site}/get_resource_by_ip", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<ResourceVO> getResourceByIp(@PathVariable("site") String site,HttpServletRequest request,
            @RequestParam(value = "ip", required = true) String ip) throws BusinessException{
        String trace = String.format("[AlertCommentController.getResourceByIp] request : site=%s", site,ip);
        //String ip=request.getRemoteHost();
        logger.debug(">> " + trace);
        ResourceVO vo=null;
        try {
            vo= alertCommentService.getResourceByIp(site,ip);
        } catch (BusinessException e) {
            ExceptionVO exceptionVO = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        }
        logger.debug("<< [AlertCommentController.getResourceByIp]  response : "  + gson.toJson(vo));
        return ResponseEntity.ok(vo);
    }
	@RequestMapping(value = "/web/rest/report/get_request_ip", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> getResourceByIp(HttpServletRequest request) throws BusinessException{
        String ip=request.getRemoteHost();
        IPVO vo = new IPVO(ip);
        return ResponseEntity.ok(vo);
    }
	
}
