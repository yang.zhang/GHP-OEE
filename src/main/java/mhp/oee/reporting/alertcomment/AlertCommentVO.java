package mhp.oee.reporting.alertcomment;

import mhp.oee.po.gen.AlertCommentPO;

public class AlertCommentVO extends AlertCommentPO{
    
    private String resourceTypeDesc;
    private Character viewActionCode;

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

    public String getResourceTypeDesc() {
        return resourceTypeDesc;
    }

    public void setResourceTypeDesc(String resourceTypeDesc) {
        this.resourceTypeDesc = resourceTypeDesc;
    }
    
    
}
