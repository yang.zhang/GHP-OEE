package mhp.oee.reporting.resource.status.detail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;import mhp.oee.po.extend.ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ResourceStatusDetailVO;
import mhp.oee.vo.StatVO;
import mhp.oee.web.Constants;

@RestController
public class ResourceStatusDetailReportController {
    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ResourceStatusDetailReportService resourceStatusDetailReportService;

    @RequestMapping(value = "/web/rest/report/site/{site}/resource_status_detail_report", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceStatusDetailVO>> getResourceStatusDetailReport(@PathVariable("site") String site,
            @RequestParam(value = "page_size", required = true) int pageSize,
            @RequestParam(value = "curr_page", required = true) int currPage,
            @RequestParam(value = "line_area", required = true) String lineArea, // 拉线
            @RequestParam(value = "work_area", required = true) String workArea, // 生产区域
            @RequestParam(value = "start_date", required = true) String startDate,
            @RequestParam(value = "end_date", required = true) String endDate,
            @RequestParam(value = "resource_type", required = false) String resourceType, // 设备类型
            @RequestParam(value = "resource_code", required = false) String resrce, // 设备编码
            @RequestParam(value = "shift", required = false) String shift,
            @RequestParam(value = "type", required = false) String type,
            @RequestParam(value = "commentCode", required = false) String commentCode) {
        String start_date_time = "";
        String end_date_time = "";
        try {
            start_date_time = Utils.reportDateTimeToStr(Utils.strToDatetime(startDate));
            end_date_time = Utils.reportDateTimeToStr(Utils.strToDatetime(endDate));
            startDate = start_date_time.substring(0, 10);
            endDate = end_date_time.substring(0, 10);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String trace = String.format(
                "[ResourceStatusDetailReportController.getResourceStatusDetailReport] request : site=%s, workArea=%s, lineArea=%s, startDate=%s, endDate=%s, resourceType=%s, resrce=%s, shift=%s",
                site, workArea, lineArea, startDate, endDate, resourceType, resrce, shift);
        logger.debug(">> " + trace);
        List<ResourceStatusDetailVO> vos = null;

        vos = resourceStatusDetailReportService.getResourceStatusDetailReport(pageSize,
                Utils.getOffset(pageSize, currPage), site, workArea, lineArea, startDate, endDate, start_date_time,
                end_date_time, resourceType, resrce, shift, type,commentCode);
        logger.debug("<< [ResourceStatusDetailReportController.getResourceStatusDetailReport] response : "
                + gson.toJson(vos));
        DecimalFormat df = new DecimalFormat("0.000");
        for (int i = 0; i < vos.size(); i++) {
            if (vos.get(i).getEndTime() != null && vos.get(i).getStartTime() != null) {
                double dur = vos.get(i).getEndTime().getTime() - vos.get(i).getStartTime().getTime();
                dur = dur / 1000 / 60;
                vos.get(i).setDuration(df.format(dur));
            }
            if (vos.get(i).getQtyInput() != null && vos.get(i).getQtyInput() < 0) {
                vos.get(i).setQtyInput(null);
            }
            if (vos.get(i).getQtyScrap() != null && vos.get(i).getQtyScrap() < 0) {
                vos.get(i).setQtyScrap(null);
            }
            if (vos.get(i).getQtyTotal() != null && vos.get(i).getQtyTotal() < 0) {
                vos.get(i).setQtyTotal(null);
            }
            if (vos.get(i).getQtyYield() != null && vos.get(i).getQtyYield() < 0) {
                vos.get(i).setQtyYield(null);
            }
            if (vos.get(i).getDate() != null) {
                vos.get(i).setStrDate(Utils.reportDateToStr(vos.get(i).getDate()));
            }
            if (vos.get(i).getStartTime() != null) {
                vos.get(i).setStrStartTime(Utils.reportDateTimeToStr(vos.get(i).getStartTime()));
            }
            if (vos.get(i).getEndTime() != null) {
                vos.get(i).setStrEndTime(Utils.reportDateTimeToStr(vos.get(i).getEndTime()));
            }
            if (vos.get(i).getStartDateTime() != null) {
                vos.get(i).setStrStartDateTime(Utils.reportDateTimeToStr(vos.get(i).getStartDateTime()));
            }
            if (vos.get(i).getEndDateTime() != null) {
                vos.get(i).setStrEndDateTime(Utils.reportDateTimeToStr(vos.get(i).getEndDateTime()));
            }
            if (vos.get(i).getReasonCodeTime() != null) {
                vos.get(i).setStrReasonCodeTime(Utils.reportDateTimeToStr(vos.get(i).getReasonCodeTime()));
            }
            if (vos.get(i).getAlertDateTime() != null) {
                vos.get(i).setStrAlertDateTime(Utils.reportDateTimeToStr(vos.get(i).getAlertDateTime()));
            }
        }
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/report/site/{site}/resource_status_detail_count", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<StatVO> getResourceStatusDetailCount(@PathVariable("site") String site,
            @RequestParam(value = "line_area", required = true) String lineArea,
            @RequestParam(value = "work_area", required = true) String workArea,
            @RequestParam(value = "start_date", required = true) String startDate,
            @RequestParam(value = "end_date", required = true) String endDate,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "resource_code", required = false) String resrce,
            @RequestParam(value = "shift", required = false) String shift) {
        String start_date_time = "";
        String end_date_time = "";
        try {
            start_date_time = Utils.reportDateTimeToStr(Utils.strToDatetime(startDate));
            end_date_time = Utils.reportDateTimeToStr(Utils.strToDatetime(endDate));
            startDate = start_date_time.substring(0, 10);
            endDate = end_date_time.substring(0, 10);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String trace = String.format(
                "[ResourceStatusDetailReportController.getResourceStatusDetailCount] request : site=%s, workArea=%s, lineArea=%s, startDate=%s, endDate=%s, resourceType=%s, resrce=%s, shift=%s",
                site, workArea, lineArea, startDate, endDate, start_date_time, end_date_time, resourceType, resrce,
                shift);
        logger.debug(">> " + trace);
        int count = 0;
        StatVO vo = new StatVO();
        vo = resourceStatusDetailReportService.getResourceStatusDetailCount(site, workArea, lineArea, startDate,
                endDate, start_date_time, end_date_time, resourceType, resrce, shift);
        logger.debug("<< [ResourceStatusDetailReportController.getResourceStatusDetailReport] response : "
                + gson.toJson(count));
        DecimalFormat df = new DecimalFormat("0.000");
        if (vo.getDuration() != null) {
            double dur = Double.parseDouble(vo.getDuration());
            dur = dur / 60;
            vo.setDuration(df.format(dur) + "min");
        }
        return ResponseEntity.ok(vo);
    }

    @RequestMapping(value = "/web/rest/report/site/{site}/resource_status_detail_total", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<StatVO> getResourceStatusDetailTotal(@PathVariable("site") String site,
            @RequestParam(value = "type", required = true) String type,
            @RequestParam(value = "line_area", required = true) String lineArea,
            @RequestParam(value = "work_area", required = true) String workArea,
            @RequestParam(value = "start_date", required = true) String startDate,
            @RequestParam(value = "end_date", required = true) String endDate,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "resource_code", required = false) String resrce,
            @RequestParam(value = "shift", required = false) String shift,
            @RequestParam(value = "commentCode", required = false) String commentCode) {
        String start_date_time = "";
        String end_date_time = "";
        try {
            start_date_time = Utils.reportDateTimeToStr(Utils.strToDatetime(startDate));
            end_date_time = Utils.reportDateTimeToStr(Utils.strToDatetime(endDate));
            startDate = start_date_time.substring(0, 10);
            endDate = end_date_time.substring(0, 10);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String trace = String.format(
                "[ResourceStatusDetailReportController.getResourceStatusDetailCount] request : site=%s, workArea=%s, lineArea=%s, startDate=%s, endDate=%s, resourceType=%s, resrce=%s, shift=%s",
                site, type, workArea, lineArea, startDate, endDate, start_date_time, end_date_time, resourceType,
                resrce, shift,commentCode);
        logger.debug(">> " + trace);
        int count = 0;
        StatVO vo = new StatVO();
        vo = resourceStatusDetailReportService.getResourceStatusDetailTotal(site, type, workArea, lineArea, startDate,
                endDate, start_date_time, end_date_time, resourceType, resrce, shift,commentCode);
        logger.debug("<< [ResourceStatusDetailReportController.getResourceStatusDetailReport] response : "
                + gson.toJson(count));
        DecimalFormat df = new DecimalFormat("0.000");
        if (vo.getDuration() != null) {
            double dur = Double.parseDouble(vo.getDuration());
            dur = dur / 60;
            vo.setDuration(df.format(dur) + "min");
        }
        return ResponseEntity.ok(vo);
    }

    @RequestMapping(value = "/web/rest/report/site/{site}/resource_status_detail/export_excel", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceStatusDetailVO>> exportExcel(@PathVariable("site") String site,
            @RequestParam(value = "line_area", required = true) String lineArea,
            @RequestParam(value = "work_area", required = true) String workArea,
            @RequestParam(value = "start_date", required = true) String startDate,
            @RequestParam(value = "end_date", required = true) String endDate,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "resource_code", required = false) String resrce,
            @RequestParam(value = "shift", required = false) String shift,
            @RequestParam(value = "type", required = false) String type, 
            @RequestParam(value = "commentCode", required = false) String commentCode,HttpServletResponse response,
            HttpServletRequest request) {
        String start_date_time = "";
        String end_date_time = "";
        try {
            start_date_time = Utils.reportDateTimeToStr(Utils.strToDatetime(startDate));
            end_date_time = Utils.reportDateTimeToStr(Utils.strToDatetime(endDate));
            startDate = start_date_time.substring(0, 10);
            endDate = end_date_time.substring(0, 10);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String trace = String.format(
                "[ResourceStatusDetailReportController.getResourceStatusDetailCount] request : site=%s, workArea=%s, lineArea=%s, startDate=%s, endDate=%s, resourceType=%s, resrce=%s, shift=%s",
                site, workArea, lineArea, startDate, endDate, resourceType, resrce, shift);
        logger.debug(">> " + trace);
        try {
            String filePath = "download.xlsx";
            List<ResourceStatusDetailVO> vos = resourceStatusDetailReportService.getResourceStatusDetailReport(null,
                    null, site, workArea, lineArea, startDate, endDate, start_date_time, end_date_time, resourceType,
                    resrce, shift, type,commentCode);
            DecimalFormat df = new DecimalFormat("0.000");
            for (int i = 0; i < vos.size(); i++) {
                if (vos.get(i).getEndTime() != null && vos.get(i).getStartTime() != null) {
                    double dur = vos.get(i).getEndTime().getTime() - vos.get(i).getStartTime().getTime();
                    dur = dur / 1000 / 60;
                    vos.get(i).setDuration(df.format(dur));
                }
                if (vos.get(i).getQtyInput() != null && vos.get(i).getQtyInput() < 0) {
                    vos.get(i).setQtyInput(null);
                }
                if (vos.get(i).getQtyScrap() != null && vos.get(i).getQtyScrap() < 0) {
                    vos.get(i).setQtyScrap(null);
                }
                if (vos.get(i).getQtyTotal() != null && vos.get(i).getQtyTotal() < 0) {
                    vos.get(i).setQtyTotal(null);
                }
                if (vos.get(i).getQtyYield() != null && vos.get(i).getQtyYield() < 0) {
                    vos.get(i).setQtyYield(null);
                }
                if (vos.get(i).getDate() != null) {
                    vos.get(i).setStrDate(Utils.reportDateToStr(vos.get(i).getDate()));
                }
                if (vos.get(i).getStartTime() != null) {
                    vos.get(i).setStrStartTime(Utils.reportDateTimeToStr(vos.get(i).getStartTime()));
                }
                if (vos.get(i).getEndTime() != null) {
                    vos.get(i).setStrEndTime(Utils.reportDateTimeToStr(vos.get(i).getEndTime()));
                }
                if (vos.get(i).getStartDateTime() != null) {
                    vos.get(i).setStrStartDateTime(Utils.reportDateTimeToStr(vos.get(i).getStartDateTime()));
                }
                if (vos.get(i).getEndDateTime() != null) {
                    vos.get(i).setStrEndDateTime(Utils.reportDateTimeToStr(vos.get(i).getEndDateTime()));
                }
                if (vos.get(i).getReasonCodeTime() != null) {
                    vos.get(i).setStrReasonCodeTime(Utils.reportDateTimeToStr(vos.get(i).getReasonCodeTime()));
                }
                if (vos.get(i).getAlertDateTime() != null) {
                    vos.get(i).setStrAlertDateTime(Utils.reportDateTimeToStr(vos.get(i).getAlertDateTime()));
                }
            }
            if ("result_item".equals(type)) {
                resourceStatusDetailReportService.resourceStatusExportExcel(vos, response, filePath);
            } else if ("result_alert".equals(type)) {
                resourceStatusDetailReportService.resourceAlertExportExcel(vos, response, filePath);
            } else if ("result_output".equals(type)) {
                resourceStatusDetailReportService.outputExportExcel(vos, response, filePath);
            } else if ("result_rt_reason_code".equals(type)) {
                resourceStatusDetailReportService.rtReasonCodeExportExcel(vos, response, filePath);
            } else {
                resourceStatusDetailReportService.reasonDownExportExcel(vos, response, filePath);
            }
            this.generateDownloadFile(response, filePath);
        } catch (FileNotFoundException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.FILE_NOT_FOUND.getErrorCode(), e.getMessage(), null);
            logger.error("exportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        } catch (IOException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
            logger.error("exportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ProductionOutputReportController.exportExcel] response : void");
        return ResponseEntity.ok(null);
    }
    @RequestMapping(value = "/web/rest/report/site/{site}/resource_status_detail_collect/export_excel", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Integer> exportCollectExcel(@PathVariable("site") String site,
            @RequestParam(value = "line_area", required = true) String lineArea,
            @RequestParam(value = "work_area", required = true) String workArea,
            @RequestParam(value = "start_date", required = true) String startDate,
            @RequestParam(value = "end_date", required = true) String endDate,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "resource_code", required = false) String resrce,
            @RequestParam(value = "shift", required = false) String shift,
            @RequestParam(value = "resource_state", required = false) String resourceState,
            HttpServletResponse response,
            HttpServletRequest request) {
    	 String start_date_time = "";
         String end_date_time = "";
         try {
             start_date_time = Utils.reportDateTimeToStr(Utils.strToDatetime(startDate));
             end_date_time = Utils.reportDateTimeToStr(Utils.strToDatetime(endDate));
             startDate = start_date_time.substring(0, 10);
             endDate = end_date_time.substring(0, 10);
         } catch (ParseException e) {
             e.printStackTrace();
         }
         String trace = String.format(
                 "[ResourceStatusDetailReportController.getResourceStatusDetailCount] request : site=%s, workArea=%s, lineArea=%s, startDate=%s, endDate=%s, resourceType=%s, resrce=%s, shift=%s",
                 site, workArea, lineArea, startDate, endDate, resourceType, resrce, shift);
         logger.debug(">> " + trace);
         String filePath = "download.xlsx";
         List<ResourceStatusDetailVO> list=new LinkedList<ResourceStatusDetailVO>();
         Set<String> set =resourceStatusDetailReportService.getLine(site,workArea,lineArea,resourceType,resrce);
         long startTime = System.currentTimeMillis();
         for(String line:set){
             List<ResourceStatusDetailVO> vos=resourceStatusDetailReportService.getResourceStatusDetailCollect( site, workArea, line, startDate, endDate, start_date_time, end_date_time, resourceType,
                     resrce, shift, resourceState);
             list.addAll(vos);
             vos.clear();
             if(list.size()>300000){
                 break;
             }
         }
         long endTime = System.currentTimeMillis();
         logger.debug("间隔======" + (endTime-startTime));
         DecimalFormat df = new DecimalFormat("0.000");
         for (int i = 0; i < list.size(); i++) {
             if (list.get(i).getEndTime() != null && list.get(i).getStartTime() != null) {
                 double dur = list.get(i).getEndTime().getTime() - list.get(i).getStartTime().getTime();
                 dur = dur / 1000 / 60;
                 list.get(i).setDuration(df.format(dur));
             }
             if (list.get(i).getQtyInput() != null && list.get(i).getQtyInput() < 0) {
                 list.get(i).setQtyInput(null);
             }
             if (list.get(i).getQtyScrap() != null && list.get(i).getQtyScrap() < 0) {
                 list.get(i).setQtyScrap(null);
             }
             if (list.get(i).getQtyTotal() != null && list.get(i).getQtyTotal() < 0) {
                 list.get(i).setQtyTotal(null);
             }
             if (list.get(i).getQtyYield() != null && list.get(i).getQtyYield() < 0) {
                 list.get(i).setQtyYield(null);
             }
             if (list.get(i).getDate() != null) {
                 list.get(i).setStrDate(Utils.reportDateToStr(list.get(i).getDate()));
             }
             if (list.get(i).getStartTime() != null) {
                 list.get(i).setStrStartTime(Utils.reportDateTimeToStr(list.get(i).getStartTime()));
             }
             if (list.get(i).getEndTime() != null) {
                 list.get(i).setStrEndTime(Utils.reportDateTimeToStr(list.get(i).getEndTime()));
             }
             if (list.get(i).getStartDateTime() != null) {
                 list.get(i).setStrStartDateTime(Utils.reportDateTimeToStr(list.get(i).getStartDateTime()));
             }
             if (list.get(i).getEndDateTime() != null) {
                 list.get(i).setStrEndDateTime(Utils.reportDateTimeToStr(list.get(i).getEndDateTime()));
             }
             if (list.get(i).getReasonCodeTime() != null) {
                 list.get(i).setStrReasonCodeTime(Utils.reportDateTimeToStr(list.get(i).getReasonCodeTime()));
             }
         }
         try {
			resourceStatusDetailReportService.ResourceStatusCollectExportExcel(list, response, filePath);
			this.generateDownloadFile(response, filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
         return ResponseEntity.ok(list.size());
    	
    }
    @RequestMapping(value = "/web/rest/report/site/{site}/resource_status_detail_collect_count", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> exportCollectCount(@PathVariable("site") String site,
            @RequestParam(value = "line_area", required = true) String lineArea,
            @RequestParam(value = "work_area", required = true) String workArea,
            @RequestParam(value = "start_date", required = true) String startDate,
            @RequestParam(value = "end_date", required = true) String endDate,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "resource_code", required = false) String resrce,
            @RequestParam(value = "shift", required = false) String shift,
            @RequestParam(value = "resource_state", required = false) String resourceState,
            HttpServletResponse response,
            HttpServletRequest request) {
    	 String start_date_time = "";
         String end_date_time = "";
         try {
             start_date_time = Utils.reportDateTimeToStr(Utils.strToDatetime(startDate));
             end_date_time = Utils.reportDateTimeToStr(Utils.strToDatetime(endDate));
             startDate = start_date_time.substring(0, 10);
             endDate = end_date_time.substring(0, 10);
         } catch (ParseException e) {
             e.printStackTrace();
         }
         String trace = String.format(
                 "[ResourceStatusDetailReportController.getResourceStatusDetailCount] request : site=%s, workArea=%s, lineArea=%s, startDate=%s, endDate=%s, resourceType=%s, resrce=%s, shift=%s",
                 site, workArea, lineArea, startDate, endDate, resourceType, resrce, shift);
         logger.debug(">> " + trace);
         
         Set<String> set =resourceStatusDetailReportService.getLine(site,workArea,lineArea,resourceType,resrce);
         List<ResourceStatusDetailVO> list=new LinkedList<ResourceStatusDetailVO>();
         for(String line:set){
             List<ResourceStatusDetailVO> vos =resourceStatusDetailReportService.getResourceStatusDetailCollect( site, workArea, line, startDate, endDate, start_date_time, end_date_time, resourceType,
                     resrce, shift, resourceState);
             list.addAll(vos);
             if(list.size()>300000){
                 break;
             }
         }
         return ResponseEntity.ok(list.size()); 
    	
    }

    private void generateDownloadFile(HttpServletResponse response, String filePath) throws IOException {
        File file = new File(filePath);
        OutputStream out = response.getOutputStream();
        response.setHeader("Content-disposition", "attachment; filename=" + "download.xlsx");
        byte[] buffer = new byte[8192]; // use bigger if you want
        int length = 0;
        FileInputStream in = new FileInputStream(file);
        while ((length = in.read(buffer)) > 0) {
            out.write(buffer, 0, length);
        }
        in.close();
        out.close();
    }
}
