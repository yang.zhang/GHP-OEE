package mhp.oee.reporting.resource.status.detail;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mhp.oee.component.plant.WorkCenterComponent;
import mhp.oee.reporting.component.ResourceStatusDetailComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceStatusDetailVO;
import mhp.oee.vo.StatVO;

@Service
public class ResourceStatusDetailReportService {

    @Autowired
    private ResourceStatusDetailComponent resourceStatusDetailComponent;
    @Autowired
    private WorkCenterComponent workCenterComponent;

    public List<ResourceStatusDetailVO> getResourceStatusDetailReport(Integer pageSize, Integer currpage, String site,
            String workArea, String lineArea, String startDate, String endDate, String start_date_time,
            String end_date_time, String resourceType, String resourceCode, String shift, String type, String commentCode) {
        return resourceStatusDetailComponent.getResourceStatusDetail(pageSize, currpage, site, workArea, lineArea,
                startDate, endDate, start_date_time, end_date_time, resourceType, resourceCode, shift, type,commentCode);
    }

    public StatVO getResourceStatusDetailCount(String site, String workArea, String lineArea, String startDate,
            String endDate, String start_date_time, String end_date_time, String resourceType, String resourceCode,
            String shift) {
        return resourceStatusDetailComponent.getResourceStatusDetailCount(site, workArea, lineArea, startDate, endDate,
                start_date_time, end_date_time, resourceType, resourceCode, shift);
    }

    public StatVO getResourceStatusDetailTotal(String site, String type, String workArea, String lineArea,
            String startDate, String endDate, String start_date_time, String end_date_time, String resourceType,
            String resourceCode, String shift, String commentCode) {
        return resourceStatusDetailComponent.getResourceStatusDetailTotal(site, type, workArea, lineArea, startDate,
                endDate, start_date_time, end_date_time, resourceType, resourceCode, shift,commentCode);
    }

    public void resourceStatusExportExcel(List<ResourceStatusDetailVO> vos, HttpServletResponse response,
            String filePath) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("设备状态明细报表");
        XSSFRow row = sheet.createRow(0);
        XSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("日期");
        cell = row.createCell((short) 1);
        cell.setCellValue("班次");
        cell = row.createCell((short) 2);
        cell.setCellValue("PRD操作员");
        /*
         * cell = row.createCell((short) 3); cell.setCellValue("ME负责人");
         */
        cell = row.createCell((short) 3);
        cell.setCellValue("设备编码");
        cell = row.createCell((short) 4);
        cell.setCellValue("资产号");
        cell = row.createCell((short) 5);
        cell.setCellValue("设备名称");
        cell = row.createCell((short) 6);
        cell.setCellValue("拉线");
        cell = row.createCell((short) 7);
        cell.setCellValue("开始时间");
        cell = row.createCell((short) 8);
        cell.setCellValue("结束时间");
        cell = row.createCell((short) 9);
        cell.setCellValue("设备状态");
        cell = row.createCell((short) 10);
        cell.setCellValue("时长（min）");
        cell = row.createCell((short) 11);
        cell.setCellValue("MODEL");
        cell = row.createCell((short) 12);
        cell.setCellValue("物料编码");

        for (int i = 0; i < vos.size(); i++) {
            XSSFRow addRow = sheet.createRow(i + 1);
            addRow.createCell(0).setCellValue(vos.get(i).getStrDate());
            addRow.createCell(1).setCellValue(vos.get(i).getShift());
            addRow.createCell(2).setCellValue(vos.get(i).getPrdUserId());
            /* addRow.createCell(3).setCellValue(vos.get(i).getMeUserId()); */
            addRow.createCell(3).setCellValue(vos.get(i).getResource());
            addRow.createCell(4).setCellValue(vos.get(i).getAsset());
            addRow.createCell(5).setCellValue(vos.get(i).getResourceDesc());
            addRow.createCell(6).setCellValue(vos.get(i).getResourceAddr());
            if (vos.get(i).getStrStartTime() != null) {
                addRow.createCell(7).setCellValue(vos.get(i).getStrStartTime());
            }
            if (vos.get(i).getStrEndTime() != null) {
                addRow.createCell(8).setCellValue(vos.get(i).getStrEndTime());
            }
            addRow.createCell(9).setCellValue(vos.get(i).getResourceState());
            addRow.createCell(10).setCellValue(Double.parseDouble(vos.get(i).getDuration()));
            addRow.createCell(11).setCellValue(vos.get(i).getModel());
            addRow.createCell(12).setCellValue(vos.get(i).getItem());
        }
        FileOutputStream outputStream = new FileOutputStream(filePath);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();

    }

    public void resourceAlertExportExcel(List<ResourceStatusDetailVO> vos, HttpServletResponse response,
            String filePath) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("设备报警明细报表");
        XSSFRow row = sheet.createRow(0);

        XSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("日期");
        cell = row.createCell((short) 1);
        cell.setCellValue("班次");
        
        cell = row.createCell((short) 2);
        cell.setCellValue("生产操作员");
        /*
         * cell = row.createCell((short) 3); cell.setCellValue("ME负责人");
         */
        cell = row.createCell((short) 3);
        cell.setCellValue("设备编码");
        cell = row.createCell((short) 4);
        cell.setCellValue("设备名称");
        cell = row.createCell((short) 5);
        cell.setCellValue("开始时间");
        cell = row.createCell((short) 6);
        cell.setCellValue("结束时间");
        cell = row.createCell((short) 7);
        cell.setCellValue("时长（min）");
        cell = row.createCell((short) 8);
        cell.setCellValue("报警信息");
        cell = row.createCell((short) 9);
        cell.setCellValue("报警时间");
        cell = row.createCell((short) 10);
        cell.setCellValue("备注");
        cell = row.createCell((short) 11);
        cell.setCellValue("设备状态");
        cell = row.createCell((short) 12);
        cell.setCellValue("拉线");
        cell = row.createCell((short) 13);
        cell.setCellValue("资产号");

        for (int i = 0; i < vos.size(); i++) {
            XSSFRow addRow = sheet.createRow(i + 1);
            addRow.createCell(0).setCellValue(vos.get(i).getStrDate());
            addRow.createCell(1).setCellValue(vos.get(i).getShift());
            addRow.createCell(2).setCellValue(vos.get(i).getPrdUserId());
            addRow.createCell(3).setCellValue(vos.get(i).getResource());
            addRow.createCell(4).setCellValue(vos.get(i).getResourceDesc());
            if (vos.get(i).getStrStartTime() != null) {
                addRow.createCell(5).setCellValue(vos.get(i).getStrStartTime());
            }
            if (vos.get(i).getStrEndTime() != null) {
                addRow.createCell(6).setCellValue(vos.get(i).getStrEndTime());
            }
            addRow.createCell(7).setCellValue(Double.parseDouble(vos.get(i).getDuration()));
            addRow.createCell(8).setCellValue(vos.get(i).getAlertInfo());
            if(vos.get(i).getAlertDateTime() != null){
           	 addRow.createCell(9).setCellValue(Utils.reportDateTimeToStr(vos.get(i).getAlertDateTime()));
           }
            addRow.createCell(10).setCellValue(vos.get(i).getCommentCode());
            addRow.createCell(11).setCellValue(vos.get(i).getResourceState());
            addRow.createCell(12).setCellValue(vos.get(i).getResourceAddr());
            addRow.createCell(13).setCellValue(vos.get(i).getAsset());
        }
        FileOutputStream outputStream = new FileOutputStream(filePath);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();

    }

    public void reasonDownExportExcel(List<ResourceStatusDetailVO> vos, HttpServletResponse response, String filePath)
            throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("设备最终停机原因");
        XSSFRow row = sheet.createRow(0);

        XSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("日期");
        cell = row.createCell((short) 1);
        cell.setCellValue("班次");
        cell = row.createCell((short) 2);
        cell.setCellValue("PRD操作员");
        /*
         * cell = row.createCell((short) 3); cell.setCellValue("ME负责人");
         */
        cell = row.createCell((short) 3);
        cell.setCellValue("设备编码");
        cell = row.createCell((short) 4);
        cell.setCellValue("资产号");
        cell = row.createCell((short) 5);
        cell.setCellValue("设备名称");
        cell = row.createCell((short) 6);
        cell.setCellValue("拉线");
        cell = row.createCell((short) 7);
        cell.setCellValue("开始时间");
        cell = row.createCell((short) 8);
        cell.setCellValue("结束时间");
        cell = row.createCell((short) 9);
        cell.setCellValue("设备状态");
        cell = row.createCell((short) 10);
        cell.setCellValue("时长（min）");
        cell = row.createCell((short) 11);
        cell.setCellValue("停机原因");
        cell = row.createCell((short) 12);
        cell.setCellValue("匹配异常的停机原因");

        for (int i = 0; i < vos.size(); i++) {
            XSSFRow addRow = sheet.createRow(i + 1);
            addRow.createCell(0).setCellValue(vos.get(i).getStrDate());
            addRow.createCell(1).setCellValue(vos.get(i).getShift());
            addRow.createCell(2).setCellValue(vos.get(i).getPrdUserId());
            /* addRow.createCell(3).setCellValue(vos.get(i).getMeUserId()); */
            addRow.createCell(3).setCellValue(vos.get(i).getResourceCode());
            addRow.createCell(4).setCellValue(vos.get(i).getAsset());
            addRow.createCell(5).setCellValue(vos.get(i).getResourceDesc());
            addRow.createCell(6).setCellValue(vos.get(i).getResourceAddr());
            if (vos.get(i).getStrStartTime() != null) {
                addRow.createCell(7).setCellValue(vos.get(i).getStrStartTime());
            }
            if (vos.get(i).getStrEndTime() != null) {
                addRow.createCell(8).setCellValue(vos.get(i).getStrEndTime());
            }
            addRow.createCell(9).setCellValue(vos.get(i).getResourceState());
            addRow.createCell(10).setCellValue(Double.parseDouble(vos.get(i).getDuration()));
            addRow.createCell(11).setCellValue(vos.get(i).getReasonCodeDesc());
            addRow.createCell(12).setCellValue(vos.get(i).getComment());
        }
        FileOutputStream outputStream = new FileOutputStream(filePath);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }

    public void outputExportExcel(List<ResourceStatusDetailVO> vos, HttpServletResponse response, String filePath)
            throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("设备产量明细报表");
        XSSFRow row = sheet.createRow(0);

        XSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("日期");
        cell = row.createCell((short) 1);
        cell.setCellValue("PRD操作员");
        cell = row.createCell((short) 2);
        cell.setCellValue("设备编码");
        cell = row.createCell((short) 3);
        cell.setCellValue("资产号");
        cell = row.createCell((short) 4);
        cell.setCellValue("设备名称");
        cell = row.createCell((short) 5);
        cell.setCellValue("拉线");
        cell = row.createCell((short) 6);
        cell.setCellValue("开始时间");
        cell = row.createCell((short) 7);
        cell.setCellValue("结束时间");
        cell = row.createCell((short) 8);
        cell.setCellValue("时长（min）");
        cell = row.createCell((short) 9);
        cell.setCellValue("物料编码");
        cell = row.createCell((short) 10);
        cell.setCellValue("总投入");
        cell = row.createCell((short) 11);
        cell.setCellValue("机判良品数");
        cell = row.createCell((short) 12);
        cell.setCellValue("机判坏品数");
        cell = row.createCell((short) 13);
        cell.setCellValue("总产出");

        for (int i = 0; i < vos.size(); i++) {
            XSSFRow addRow = sheet.createRow(i + 1);
            addRow.createCell(0).setCellValue(vos.get(i).getStrDate());
            addRow.createCell(1).setCellValue(vos.get(i).getPrdUserId());
            addRow.createCell(2).setCellValue(vos.get(i).getResource());
            addRow.createCell(3).setCellValue(vos.get(i).getAsset());
            addRow.createCell(4).setCellValue(vos.get(i).getResourceDesc());
            addRow.createCell(5).setCellValue(vos.get(i).getResourceAddr());
            if (vos.get(i).getStrStartTime() != null) {
                addRow.createCell(6).setCellValue(vos.get(i).getStrStartTime());
            }
            if (vos.get(i).getStrEndTime() != null) {
                addRow.createCell(7).setCellValue(vos.get(i).getStrEndTime());
            }
            addRow.createCell(8).setCellValue(Double.parseDouble(vos.get(i).getDuration()));
            addRow.createCell(9).setCellValue(vos.get(i).getItem());
            if (vos.get(i).getQtyInput() != null) {
                addRow.createCell(10).setCellValue(vos.get(i).getQtyInput());
            }
            if (vos.get(i).getQtyInput() != null) {
                addRow.createCell(10).setCellValue(vos.get(i).getQtyInput());
            }
            if (vos.get(i).getQtyYield() != null) {
                addRow.createCell(11).setCellValue(vos.get(i).getQtyYield());
            }
            if (vos.get(i).getQtyScrap() != null) {
                addRow.createCell(12).setCellValue(vos.get(i).getQtyScrap());
            }
            if (vos.get(i).getQtyTotal() != null) {
                addRow.createCell(13).setCellValue(vos.get(i).getQtyTotal());
            }
        }
        FileOutputStream outputStream = new FileOutputStream(filePath);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }

    public void rtReasonCodeExportExcel(List<ResourceStatusDetailVO> vos, HttpServletResponse response, String filePath)
            throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("设备实时停机原因");
        XSSFRow row = sheet.createRow(0);
        XSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("日期");
        cell = row.createCell((short) 1);
        cell.setCellValue("班次");
        cell = row.createCell((short) 2);
        cell.setCellValue("PRD操作员");
        cell = row.createCell((short) 3);
        cell.setCellValue("设备编码");
        cell = row.createCell((short) 4);
        cell.setCellValue("资产号");
        cell = row.createCell((short) 5);
        cell.setCellValue("拉线");
        cell = row.createCell((short) 6);
        cell.setCellValue("设备名称");
        cell = row.createCell((short) 7);
        cell.setCellValue("停机开始时间");
        cell = row.createCell((short) 8);
        cell.setCellValue("停机结束时间");
        cell = row.createCell((short) 9);
        cell.setCellValue("设备状态");
        cell = row.createCell((short) 10);
        cell.setCellValue("停机时长（min）");
        cell = row.createCell((short) 11);
        cell.setCellValue("点选时间");
        cell = row.createCell((short) 12);
        cell.setCellValue("点选原因");

        for (int i = 0; i < vos.size(); i++) {
            XSSFRow addRow = sheet.createRow(i + 1);
            addRow.createCell(0).setCellValue(vos.get(i).getStrDate());
            addRow.createCell(1).setCellValue(vos.get(i).getShift());
            addRow.createCell(2).setCellValue(vos.get(i).getPrdUserId());
            addRow.createCell(3).setCellValue(vos.get(i).getResource());
            addRow.createCell(4).setCellValue(vos.get(i).getAsset());
            addRow.createCell(6).setCellValue(vos.get(i).getResourceDesc());
            addRow.createCell(5).setCellValue(vos.get(i).getResourceAddr());
            if (vos.get(i).getStrStartTime() != null) {
                addRow.createCell(7).setCellValue(vos.get(i).getStrStartTime());
            }
            if (vos.get(i).getStrEndTime() != null) {
                addRow.createCell(8).setCellValue(vos.get(i).getStrEndTime());
            }
            addRow.createCell(9).setCellValue(vos.get(i).getResourceState());
            addRow.createCell(10).setCellValue(Double.parseDouble(vos.get(i).getDuration()));
            addRow.createCell(11).setCellValue(vos.get(i).getStrReasonCodeTime());
            addRow.createCell(12).setCellValue(vos.get(i).getReasonCodeDescription());
        }
        FileOutputStream outputStream = new FileOutputStream(filePath);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();

    }

	public List<ResourceStatusDetailVO> getResourceStatusDetailCollect(String site,
            String workArea, String lineArea, String startDate, String endDate, String start_date_time,
            String end_date_time, String resourceType, String resourceCode, String shift, String resourceState) {
		
		return resourceStatusDetailComponent.getResourceStatusDetailCollect( site, workArea, lineArea,
                startDate, endDate, start_date_time, end_date_time, resourceType, resourceCode, shift,resourceState);
	}

	public void ResourceStatusCollectExportExcel(List<ResourceStatusDetailVO> vos, HttpServletResponse response,
			String filePath) throws IOException {
			 SXSSFWorkbook workbook = new SXSSFWorkbook(100);
			 Sheet sheet = workbook.createSheet("设备基础数据明细汇总报表");
		        Row row = sheet.createRow(0);
		        
		        DecimalFormat df = new DecimalFormat("0.000");
		        
		        Cell cell = row.createCell((short) 0);
		        cell.setCellValue("日期");
		        cell = row.createCell((short) 1);
		        cell.setCellValue("班次");
		        cell = row.createCell((short) 2);
		        cell.setCellValue("PRD操作员");
		        cell = row.createCell((short) 3);
		        cell.setCellValue("ME负责人");
		        cell = row.createCell((short) 4);
		        cell.setCellValue("生产区域");
		        cell = row.createCell((short) 5);
		        cell.setCellValue("拉线");
		        cell = row.createCell((short) 6);
		        cell.setCellValue("设备类型");
		        cell = row.createCell((short) 7);
		        cell.setCellValue("设备编码");
		        cell = row.createCell((short) 8);
		        cell.setCellValue("资产号");
		        cell = row.createCell((short) 9);
		        cell.setCellValue("设备名称");
		        cell = row.createCell((short) 10);
		        cell.setCellValue("开始时间");
		        cell = row.createCell((short) 11);
		        cell.setCellValue("结束时间");
		        cell = row.createCell((short) 12);
		        cell.setCellValue("时长（min）");
		        cell = row.createCell((short) 13);
		        cell.setCellValue("设备状态");
		        /*cell = row.createCell((short) 14);
		        cell.setCellValue("点选原因");*/
		        cell = row.createCell((short) 14);
		        cell.setCellValue("点选时间");
		        cell = row.createCell((short) 15);
		        cell.setCellValue("机判良品数");
		        cell = row.createCell((short) 16);
		        cell.setCellValue("机判坏品数");
		        cell = row.createCell((short) 17);
		        cell.setCellValue("总产出数");
		        cell = row.createCell((short) 18);
		        cell.setCellValue("总投入数");
		        cell = row.createCell((short) 19);
		        cell.setCellValue("物料编码");
		        cell = row.createCell((short) 20);
		        cell.setCellValue("MODEL");
		        cell = row.createCell((short) 21);
		        cell.setCellValue("报警信息");

		        for (int i = 0; i < vos.size() ; i++) {
		        	if(i > 300000) break;
		            Row addRow = sheet.createRow(i + 1);
		            addRow.createCell(0).setCellValue(vos.get(i).getStrDate());				//日期0
		            addRow.createCell(1).setCellValue(vos.get(i).getShift());				//班次
		            addRow.createCell(2).setCellValue(vos.get(i).getPrdUserId());			//PRD操作员
		            addRow.createCell(3).setCellValue(vos.get(i).getMeUserId());			//PRD操作员
		            addRow.createCell(4).setCellValue(vos.get(i).getWorkAreaDesc());		//生产区域
		            addRow.createCell(5).setCellValue(vos.get(i).getResourceAddr());		//拉线
		            addRow.createCell(6).setCellValue(vos.get(i).getResourceTypeDesc());	//设备类型5
		            addRow.createCell(7).setCellValue(vos.get(i).getResource());			//设备编码
		            addRow.createCell(8).setCellValue(vos.get(i).getAsset());				//资产号
		            addRow.createCell(9).setCellValue(vos.get(i).getResourceDesc());		//设备名称
		            if (vos.get(i).getStrStartTime() != null) {
		                addRow.createCell(10).setCellValue(vos.get(i).getStrStartTime());	//开始时间
		            }
		            if (vos.get(i).getStrEndTime() != null) {
		                addRow.createCell(11).setCellValue(vos.get(i).getStrEndTime());		//结束时间
		            }
		            //df.format(Double.parseDouble(
		            addRow.createCell(12).setCellValue(Double.parseDouble(df.format(Double.parseDouble(vos.get(i).getDuration()))));	//时长
		            if("停机".equals(vos.get(i).getResourceState())){
		                addRow.createCell(13).setCellValue(vos.get(i).getReasonCodeDescription());        //设备状态
		            }else {
		                addRow.createCell(13).setCellValue(vos.get(i).getResourceState());        //设备状态
		            }
		            //addRow.createCell(14).setCellValue(vos.get(i).getReasonCodeDescription());//点选原因
		            addRow.createCell(14).setCellValue(vos.get(i).getStrReasonCodeTime());	//点选时间
		            if (vos.get(i).getQtyYield() != null) {
		            	addRow.createCell(15).setCellValue(vos.get(i).getQtyYield());		//良品
		            }
		            
		            if (vos.get(i).getQtyScrap() != null) {
		            	addRow.createCell(16).setCellValue(vos.get(i).getQtyScrap());		//坏品
		            }
		            if (vos.get(i).getQtyTotal() != null) {
		            	addRow.createCell(17).setCellValue(vos.get(i).getQtyTotal());		//总产出
		            }
		            if (vos.get(i).getQtyInput() != null) {
		            	addRow.createCell(18).setCellValue(vos.get(i).getQtyInput());		//总投入
		            }
		            addRow.createCell(19).setCellValue(vos.get(i).getItem());				//物料编码
		            addRow.createCell(20).setCellValue(vos.get(i).getModel());				//model
		            addRow.createCell(21).setCellValue(vos.get(i).getAlertInfo());			//报警
		        }
		        FileOutputStream outputStream = new FileOutputStream(filePath);
		        workbook.write(outputStream);
		        outputStream.close();
		        workbook.close();
		}
    public Set<String> getLine(String site,String workArea, String lineArea, String resourceType, String resrce) {
        Set<String> set = new HashSet<String>();
        if (!Utils.isEmpty(lineArea)) {
           String[] lineArray = lineArea.split(",");
           for(String line:lineArray){
               set.add(line);
           }
           return set;
        } else if (!Utils.isEmpty(workArea)) {
            String[] workArray = workArea.split(",");
            for(int i=0;i<workArray.length;i++){
                workArray[i]="WorkCenterBO:"+site+","+workArray[i];
            }
            set = workCenterComponent.getLineByWorkArea(workArray);
            return set;
        } else if (!Utils.isEmpty(resourceType)) {
            String[] resourceTypeArray = resourceType.split(",");
            for(int i=0;i<resourceTypeArray.length;i++){
                resourceTypeArray[i]="ResourceTypeBO:"+site+","+resourceTypeArray[i];
            }
            set = workCenterComponent.getLineByResourceType(resourceTypeArray);
            return set;
        } else if (!Utils.isEmpty(resrce)) {
            set = workCenterComponent.getLineByResrce(resrce);
            return set;
        }
        return null;
    }
}
