package mhp.oee.reporting.oee;

/**
 * Created by LinZuK on 2016/8/16.
 */
public enum OeeWorkbookCell {
    SHEET_NAME("OEE报表"),
    DATE_SHIFT("日期/班次"),
    AVAILABILITY("可用率"),
    PERFORMANCE("性能"),
    QUALITY("质量优率"),
    OEE("OEE");

    private String name;

    private OeeWorkbookCell(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
