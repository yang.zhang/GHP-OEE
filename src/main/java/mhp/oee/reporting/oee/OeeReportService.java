package mhp.oee.reporting.oee;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.extend.OeeAlarmLinePO;
import mhp.oee.reporting.component.OeeComponent;
import mhp.oee.po.extend.OeeOeePO;
import mhp.oee.po.extend.OeeReasonCodeGroupPO;
import mhp.oee.po.extend.OeeReasonCodePO;
import mhp.oee.utils.Utils;

import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * Created by LinZuK on 2016/8/12.
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class OeeReportService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private OeeComponent oeeComponent;


    public Map<String, Object> selectOeeOutputReportView(String site, String resourceBo, String startDateTime, String endDateTime, String byTimeType) throws ParseException {
        startDateTime = Utils.parseReportDateDay(startDateTime);
        endDateTime = Utils.parseReportDateDay(endDateTime);
        return oeeComponent.selectOeeOutputReport(site, resourceBo, startDateTime, endDateTime, byTimeType);
    }

    public List<OeeOeePO> selectOeeOutputReportExcel(String site, String resourceBo, String startDateTime, String endDateTime, String byTimeType) throws ParseException {
        startDateTime = Utils.parseReportDateDay(startDateTime);
        endDateTime = Utils.parseReportDateDay(endDateTime);
        return oeeComponent.selectOeePOList(site, resourceBo, startDateTime, endDateTime, byTimeType);
    }


    public List<OeeReasonCodePO> selectOeeReasonCodeReport(String site, String resourceBo, String startDateTime, String endDateTime, String byTimeType) throws ParseException {
        startDateTime = Utils.parseReportDateDay(startDateTime);
        endDateTime = Utils.parseReportDateDay(endDateTime);
        return oeeComponent.selectOeeReasonCodeReport(site, resourceBo, startDateTime, endDateTime, byTimeType);
    }

    public List<OeeReasonCodeGroupPO> selectOeeReasonCodeGroupReport(String site, String resourceBo, String startDateTime, String endDateTime, String byTimeType) throws ParseException {
        startDateTime = Utils.parseReportDateDay(startDateTime);
        endDateTime = Utils.parseReportDateDay(endDateTime);
        List<OeeReasonCodeGroupPO>  pos = oeeComponent.selectOeeReasonCodeGroupReport(site, resourceBo, startDateTime, endDateTime, byTimeType);
        // 计算总数
        BigDecimal totalIntersectionTimeDurationSum = new BigDecimal(0);
        for (OeeReasonCodeGroupPO po : pos) {
            totalIntersectionTimeDurationSum = totalIntersectionTimeDurationSum.add(po.getIntersectionTimeDurationSum());
        }
        // 计算百分比
        for (OeeReasonCodeGroupPO po : pos) {
            BigDecimal percent = po.getIntersectionTimeDurationSum()
                    .multiply(new BigDecimal(100))
                    .divide(totalIntersectionTimeDurationSum, RoundingMode.HALF_UP);
            po.setPercent(percent + "%");
        }
        return pos;
    }

    public void commitExportExcel(List<OeeOeePO> vos, File file) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFCellStyle style = workbook.createCellStyle();
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%"));
        XSSFSheet sheet = workbook.createSheet(OeeWorkbookCell.SHEET_NAME.getName());
        XSSFRow firstRow = sheet.createRow(0);
        XSSFCell firstCell = firstRow.createCell(0);
        firstCell.setCellValue(OeeWorkbookCell.DATE_SHIFT.getName());
        XSSFCell secondCell = firstRow.createCell(1);
        secondCell.setCellValue(OeeWorkbookCell.AVAILABILITY.getName());
        XSSFCell thirdCell = firstRow.createCell(2);
        thirdCell.setCellValue(OeeWorkbookCell.PERFORMANCE.getName());
        XSSFCell fourthCell = firstRow.createCell(3);
        fourthCell.setCellValue(OeeWorkbookCell.QUALITY.getName());
        XSSFCell fitthCell = firstRow.createCell(4);
        fitthCell.setCellValue(OeeWorkbookCell.OEE.getName());
        
        for (int i = 0; i < vos.size(); i++) {
            XSSFRow addRow = sheet.createRow(i + 1);
            addRow.createCell(0).setCellValue(vos.get(i).getByTime());
            //时间利用率
            XSSFCell cellA=addRow.createCell(1);
            if(!Utils.isEmpty(vos.get(i).getA())){
                cellA.setCellValue(Double.parseDouble(vos.get(i).getA().split("%")[0])/100);
            }else{
                cellA.setCellValue("N/A");
            }
            cellA.setCellStyle(style);
            
            //性能
            XSSFCell cellP=addRow.createCell(2);
            if(!Utils.isEmpty(vos.get(i).getP())){
                cellP.setCellValue(Double.parseDouble(vos.get(i).getP().split("%")[0])/100);
            }else{
                cellP.setCellValue("N/A");
            }
            cellP.setCellStyle(style);
            //质量优率
            XSSFCell cellQ=addRow.createCell(3);
            if(!Utils.isEmpty(vos.get(i).getQ())){
                cellQ.setCellValue(Double.parseDouble(vos.get(i).getQ().split("%")[0])/100);
            }else{
                cellQ.setCellValue("N/A");
            }
            cellQ.setCellStyle(style);
            //OEE
            XSSFCell cellOEE=addRow.createCell(4);
            if(!Utils.isEmpty(vos.get(i).getOee())){
                cellOEE.setCellValue(Double.parseDouble(vos.get(i).getOee().split("%")[0])/100);
            }else {
                cellOEE.setCellValue("N/A");
            }
            cellOEE.setCellStyle(style);
        }
        FileOutputStream outputStream = new FileOutputStream(file);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }
    // 查询结果告警线
    public List<OeeAlarmLinePO> selectOeeAlarmLine(String site) {
        return oeeComponent.selectOeeAlarmLine(site);
    }
}
