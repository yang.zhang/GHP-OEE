package mhp.oee.reporting.oee;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.extend.OeeAlarmLinePO;
import mhp.oee.po.extend.OeeOeePO;
import mhp.oee.po.extend.OeeReasonCodeGroupPO;
import mhp.oee.po.extend.OeeReasonCodePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ProductionOutputFirstAndFollowUpVO;
import mhp.oee.web.Constants;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by LinZuK on 2016/8/12.
 */
@RestController
public class OeeReportController {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    OeeReportService oeeReportService;


    @RequestMapping(value = "/web/rest/report/oee/site/{site}/oee_output_report", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> readOeeOutputReport(
                        @PathVariable("site") String site,
                        @RequestParam(value = "resourceBo", required = true) String resourceBo,
                        @RequestParam(value = "startDateTime", required = true) String startDateTime,
                        @RequestParam(value = "endDateTime", required = true) String endDateTime,
                        @RequestParam(value = "byTimeType", required = true) String byTimeType) {
        String trace = String.format("[OeeReportController.readOeeOutputReport] request : site=%s, resourceBo=%s, startDateTime=%s, endDateTime=%s, byTimeType=%s",
                site, resourceBo, startDateTime, endDateTime, byTimeType);
        logger.debug(">> " + trace);
        Map<String, Object> vos = null;
        try {
            vos = oeeReportService.selectOeeOutputReportView(site, resourceBo, startDateTime, endDateTime, byTimeType);
        } catch (ParseException e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< [OeeReportController.readOeeOutputReport] response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/report/oee/site/{site}/export_excel", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ProductionOutputFirstAndFollowUpVO>> exportExcel(
                        @PathVariable("site") String site,
                        @RequestParam(value = "resourceBo", required = true) String resourceBo,
                        @RequestParam(value = "startDateTime", required = true) String startDateTime,
                        @RequestParam(value = "endDateTime", required = true) String endDateTime,
                        @RequestParam(value = "byTimeType", required = true) String byTimeType,
                        HttpServletResponse response) {
        String trace = String.format("[OeeReportController.exportExcel] request : site=%s, resourceBo=%s, startDateTime=%s, endDateTime=%s, byTimeType=%s",
                site, resourceBo, startDateTime, endDateTime, byTimeType);
        logger.debug(">> " + trace);
        try {
            File file = new File("download.xlsx");
            List<OeeOeePO> vos = oeeReportService.selectOeeOutputReportExcel(site, resourceBo, startDateTime, endDateTime, byTimeType);
            oeeReportService.commitExportExcel(vos, file);
            Utils.generateDownloadFile(response, file);
            FileUtils.deleteQuietly(file);
        } catch (IOException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
            logger.error("exportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        } catch (ParseException e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< [OeeReportController.exportExcel] response : void");
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/web/rest/report/oee/site/{site}/oee_reason_code_report", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<OeeReasonCodePO>> readOeeReasonCodeReport(
            @PathVariable("site") String site,
            @RequestParam(value = "resourceBo", required = true) String resourceBo,
            @RequestParam(value = "startDateTime", required = true) String startDateTime,
            @RequestParam(value = "endDateTime", required = true) String endDateTime,
            @RequestParam(value = "byTimeType", required = true) String byTimeType) {
        String trace = String.format("[OeeReportController.readOeeReasonCodeReport] request : site=%s, resourceBo=%s, startDateTime=%s, endDateTime=%s, byTimeType=%s",
                site, resourceBo, startDateTime, endDateTime, byTimeType);
        logger.debug(">> " + trace);
        List<OeeReasonCodePO> vos = null;
        try {
            vos = oeeReportService.selectOeeReasonCodeReport(site, resourceBo, startDateTime, endDateTime, byTimeType);
        } catch (ParseException e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< [OeeReportController.readOeeReasonCodeReport] response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/report/oee/site/{site}/oee_reason_code_group_report", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<OeeReasonCodeGroupPO>> readOeeReasonCodeGroupReport(
            @PathVariable("site") String site,
            @RequestParam(value = "resourceBo", required = true) String resourceBo,
            @RequestParam(value = "startDateTime", required = true) String startDateTime,
            @RequestParam(value = "endDateTime", required = true) String endDateTime,
            @RequestParam(value = "byTimeType", required = true) String byTimeType) {
        String trace = String.format("[OeeReportController.readOeeReasonCodeGroupReport] request : site=%s, resourceBo=%s, startDateTime=%s, endDateTime=%s, byTimeType=%s",
                site, resourceBo, startDateTime, endDateTime, byTimeType);
        logger.debug(">> " + trace);
        List<OeeReasonCodeGroupPO> vos = null;
        try {
            vos = oeeReportService.selectOeeReasonCodeGroupReport(site, resourceBo, startDateTime, endDateTime, byTimeType);
        } catch (ParseException e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< [OeeReportController.readOeeReasonCodeGroupReport] response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/report/oee/site/{site}/oee_alarmLine", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> readOeeAlarmLine(@PathVariable("site") String site) {
        String trace = String.format("[OeeReportController.readOeeAlarmLine] request : site=%s", site);
        logger.debug(">> " + trace);
        List<OeeAlarmLinePO> oeeAlarmLine = oeeReportService.selectOeeAlarmLine(site);
        logger.debug("<< [OeeReportController.readOeeAlarmLine] response : " + gson.toJson(oeeAlarmLine));
        return ResponseEntity.ok(oeeAlarmLine);
    }

}
