package mhp.oee.reporting.component;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.dao.extend.ResourceRealTimePOMapper;
import mhp.oee.po.extend.ResourceRealTimeReportPO;

@Component
public class RealTimeDeviceComponent {

	@Autowired
	private ResourceRealTimePOMapper resourceRealTimePOMapper;

	public List<ResourceRealTimeReportPO> search(String site, String workArea,
												 String workCenter, String line, String deviceType, String deviceCode) {
		List<ResourceRealTimeReportPO> basicList = resourceRealTimePOMapper.selectResourceRealTime(deviceType, site, deviceCode, line, workCenter, workArea);
		List<ResourceRealTimeReportPO> returnList = new ArrayList<ResourceRealTimeReportPO>();
		List<String> handleList = getHandleList(basicList);
		for(String handle:handleList){
			List<ResourceRealTimeReportPO> eachResrceReport = getEachResrceReport(handle,basicList);
			ResourceRealTimeReportPO justOneReport = getJustOneReport(eachResrceReport);
			returnList.add(justOneReport);
		}
		return returnList;
	}

	//从基础集合中把所有设备主键去重放入一个集合
	public List<String> getHandleList(List<ResourceRealTimeReportPO> list){
		List<String> handleList = new ArrayList<String>();
		for(ResourceRealTimeReportPO po:list){
			if(!handleList.contains(po.getHandle())){
				handleList.add(po.getHandle());
			}
		}
		return handleList;
	}

	//通过设备主键，在整个基础集合中查出该设备对应的记录集合
	public List<ResourceRealTimeReportPO> getEachResrceReport(String
																	  handle,List<ResourceRealTimeReportPO> list){
		List<ResourceRealTimeReportPO> returnListEach = new ArrayList<ResourceRealTimeReportPO>();
		for(ResourceRealTimeReportPO po:list){
			if(handle.equals(po.getHandle())){
				returnListEach.add(po);
			}
		}
		return returnListEach;
	}

	//将单个设备的记录集合去重合并报警信息
	public ResourceRealTimeReportPO getJustOneReport
	(List<ResourceRealTimeReportPO> list){
		ResourceRealTimeReportPO returnPO = null;
		if(!list.isEmpty()){
			returnPO = list.get(0);
			int i=0;
			String alertDescription="";
			for(ResourceRealTimeReportPO desList : list){
				if(StringUtils.isNotBlank
						(desList.getDescription())){
					i++;
					if(i==5){
						break;
					}else{
						alertDescription +=desList.getDescription()+"|";
					}
				}
			}
			if(i>0){
				alertDescription = alertDescription.substring(0,
						alertDescription.length()-1);
			}
			returnPO.setDescription(alertDescription);
		}
		return returnPO;
	}
}
