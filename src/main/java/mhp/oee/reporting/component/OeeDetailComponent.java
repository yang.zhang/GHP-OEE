package mhp.oee.reporting.component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.dao.extend.OeeAlarmLineMapper;
import mhp.oee.dao.extend.OeeDetailPOMapper;
import mhp.oee.dao.extend.OeePOMapper;
import mhp.oee.po.extend.OeeAlarmLinePO;
import mhp.oee.po.extend.OeeOeePO;
import mhp.oee.po.extend.OeeReasonCodeGroupPO;
import mhp.oee.po.extend.OeeReasonCodePO;
import mhp.oee.reporting.common.SearchHelpReportService;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceVO;

import org.apache.commons.collections.map.HashedMap;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by SuMingzhi on 2016/11/15.
 */
@Component
public class OeeDetailComponent {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    OeeDetailPOMapper oeeDetailPOMapper;
    @Autowired
    OeeAlarmLineMapper oeeAlarmLineMapper;
    @Autowired
    SearchHelpReportService searchHelpReportService;   

    // 查询oee明细报表
    public List<OeeOeePO> selectOeePOList(String site, String workCenter,String resourceType, String resourceBo, String endDate, String shiftLike,String startDate,String workCenterLink) {
    	String[] workCenterArray = null;
    	if(workCenter != null && workCenter.length() > 0) {
    		workCenterArray = workCenter.split(",");
    		workCenter = Utils.formatInParams("WORK_AREA_CODE", workCenterArray);
    	}
    	String[] resourceTypeArray = null;
    	if(resourceType != null && resourceType.length() > 0) {
    		resourceTypeArray = resourceType.split(",");
    		resourceType = Utils.formatInParams("RESRCE_TYPE", resourceTypeArray);
    	}
    	String[] workCenterLinkArray=null;
    	if(workCenterLink != null && workCenterLink.length() > 0) {
    		workCenterLinkArray = workCenterLink.split(",");
    		workCenterLink = Utils.formatInParams("WORK_CENTER_CODE", workCenterLinkArray);
    	}
    	if(shiftLike != null && shiftLike.length() > 0) {
    		shiftLike = "SHIFT like ''"+shiftLike+"%''";
    	}
    	List<OeeOeePO> allOop = new ArrayList<OeeOeePO>();
    	if(resourceBo != null && resourceBo.length() > 0) {
    		String[] resourceBoArray = resourceBo.split(",");
    		for(String resrce:resourceBoArray){
    			String[] oopArr = {resrce};
    			resourceBo = Utils.formatInParams("RESRCE", oopArr);
    			List<OeeOeePO> selectOeeDetail = oeeDetailPOMapper.selectOeeDetail(site, workCenter, resourceBo, endDate, shiftLike, startDate, workCenterLink);
    			for(OeeOeePO e:selectOeeDetail){
    				allOop.add(e);
    			}
    		}
    	}else{
    		String resource = null;
    		List<ResourceVO> vos = new ArrayList<ResourceVO>();
    		if(resourceTypeArray!=null&&resourceTypeArray.length>0){
    			for(String resourceTy:resourceTypeArray){
    				vos.addAll(searchHelpReportService.readResourceHelp(site, resourceTy,resource, workCenterLinkArray, workCenterArray));
    			}
    		}else
    			vos = searchHelpReportService.readResourceHelp(site, resource,resource, workCenterLinkArray, workCenterArray);
            if(vos.isEmpty())
            	return null;
    		for(ResourceVO ro:vos){
    			String[] oopArr = {ro.getResrce()};
    			resourceBo = Utils.formatInParams("RESRCE", oopArr);
    			List<OeeOeePO> selectOeeDetail = oeeDetailPOMapper.selectOeeDetail(site, workCenter, resourceBo, endDate, shiftLike, startDate, workCenterLink);
    			for(OeeOeePO e:selectOeeDetail){
    				allOop.add(e);
    			}
    		}
    		
    	}
    	Collections.sort(allOop,new Comparator<OeeOeePO>(){

			@Override
			public int compare(OeeOeePO o1, OeeOeePO o2) {
				int flag = 0;
				try {
					flag =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(o1.getShiftStartTime()).compareTo(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(o2.getShiftStartTime()));
					if(flag==0){
						flag = o1.getWorkArea().compareTo(o2.getWorkArea());
						if(flag==0){
							flag = o1.getWorkCenter().compareTo(o2.getWorkCenter());
						}
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return flag;
			}});
    	return allOop;
    }
    // 查询oee表格和chart图
    public Map<String, Object> selectOeeOutputReport(String site,String workCenter,String  resourceType,String resourceBo,String endDate,String shiftLike, String startDate,  String workCenterLink) {
        List<OeeOeePO> oeePOList = selectOeePOList(site, workCenter,resourceType, resourceBo, endDate, shiftLike, startDate, workCenterLink);
        Map<String, List<BigDecimal>> valueMap = new LinkedHashMap<String, List<BigDecimal>>();

        // avg oee
        List<Map<String, String>> oeeAvgList = new LinkedList<Map<String, String>>();
        Iterator iterator = valueMap.keySet().iterator();
        while (iterator.hasNext()) {
            String byTime = (String) iterator.next();
            List<BigDecimal> oeeList = valueMap.get(byTime);
            Map<String, String> oeeAvg = new HashMap<String, String>();
            oeeAvg.put("byTime", byTime);
            oeeAvg.put("oeeAvg", calculateOeeAvg(oeeList)); // 将每个分组的oee平均值放入map
            oeeAvgList.add(oeeAvg);
        }

        // pkg data
        Map<String, Object> data = new HashedMap();
        data.put("table", oeePOList);
        data.put("chart", oeeAvgList);
        return data;
    }

    private String calculateOeeAvg(List<BigDecimal> oeeList) {
        double total = 0;
        for (BigDecimal oee : oeeList) {
            total += oee.doubleValue();
        }
        double avg = 100 * total / oeeList.size();
        avg = new BigDecimal(avg).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        return avg + "";
    }

//   查询结果告警线
    public List<OeeAlarmLinePO> selectOeeAlarmLine(String site) {
        return oeeAlarmLineMapper.select(site);
    }
}
