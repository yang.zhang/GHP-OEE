package mhp.oee.reporting.component;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mhp.oee.dao.extend.ResourceStatusDetailReportPOMapper;
import mhp.oee.dao.extend.ShutdownReasonReportPOMapper;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceStatusDetailVO;
import mhp.oee.vo.ShutdownReasonVO;
import mhp.oee.vo.StatVO;

@Component
public class ShutdownReasonComponent {

    @Autowired
    private ShutdownReasonReportPOMapper shutdownReasonReportPOMapper;

    public List<ShutdownReasonVO> getShundownReason( String site,
            String workArea, String lineArea,String resourceType, String resourceCode,String startDate, String endDate, 
             String byTimetype,String dataType, String byType, String shift){
    	
    	 if (!Utils.isEmpty(shift)) {
             shift = "SHIFT like " + "''" +"%"+ shift +"%"+"''";
         } else {
             shift = "";
         }
        if(!Utils.isEmpty(lineArea)){
        	String lineAreas = convertStrAddQuotes(lineArea);
            lineArea="LINE_AREA IN "+lineAreas;
        }else{
            lineArea="";
        }
        if(!Utils.isEmpty(resourceType)){
        	String resourceTypes = convertStrAddQuotes(resourceType);
        	resourceType="RESOURCE_TYPE IN "+resourceTypes;
        }else{
            resourceType="";
        }
        if(!Utils.isEmpty(resourceCode)){
        	String resourceCodes = convertStrAddQuotes(resourceCode);
        	resourceCode="RESRCE IN "+resourceCodes;
        }else{
            resourceCode="";
        }
        if(!Utils.isEmpty(workArea)){
        	String workAreas =  convertStrAddQuotes(workArea);
        	workArea = "WORK_CENTER IN "+ workAreas;
        }else{
        	workArea="";
        }
        if("CHAR".equals(dataType)){
        	return shutdownReasonReportPOMapper.getShundownReasonForChart(site, workArea, lineArea, resourceType, resourceCode,
                    startDate, endDate, byTimetype,shift);
        } else {
        	if("W".equals(byType)){
        		return shutdownReasonReportPOMapper.getShundownReasonByWorkCenter(site, workArea, lineArea, resourceType, resourceCode,
                        startDate, endDate, byTimetype,shift);
        		
        	}else if("L".equals(byType)){
        		return shutdownReasonReportPOMapper.getShundownReasonByWorkArea(site, workArea, lineArea, resourceType, resourceCode,
                        startDate, endDate, byTimetype,shift);
        		
        	}else if("T".equals(byType)){
        		return shutdownReasonReportPOMapper.getShundownReasonByResrceType(site, workArea, lineArea, resourceType, resourceCode,
                        startDate, endDate, byTimetype,shift);
        	}else {
        		return shutdownReasonReportPOMapper.getShundownReasonByResrce(site, workArea, lineArea, resourceType, resourceCode,
                        startDate, endDate, byTimetype,shift);
        	}
        }
    }
       

    private String convertStrAddQuotes(String str) {
    	String array[]=str.split(",");
        String strAddQuotes="(";
        for(int i=0;i<array.length;i++){
        	strAddQuotes+="''"+array[i]+"'',";
        }
        strAddQuotes=strAddQuotes.substring(0, strAddQuotes.length()-1)+")";
		return strAddQuotes;
	}


	public List<ShutdownReasonVO> getShundownReasonForChart(String site, String workArea, String lineArea,
			String resourceType, String resourceCode, String startDate, String endDate, String byTimetype) {
		// TODO Auto-generated method stub
		return null;
	}


    
}
