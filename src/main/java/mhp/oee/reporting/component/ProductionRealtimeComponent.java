package mhp.oee.reporting.component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.dao.extend.ProductionRealtimeReportPOMapper;
import mhp.oee.dao.extend.ResourceStatusDetailReportPOMapper;
import mhp.oee.dao.extend.ShutdownReasonReportPOMapper;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ProductionRealtimeVo;
import mhp.oee.vo.ResourceStatusDetailVO;
import mhp.oee.vo.ShutdownReasonVO;
import mhp.oee.vo.StatVO;

@Component
public class ProductionRealtimeComponent {

    @Autowired
    private ProductionRealtimeReportPOMapper productionRealtimeReportPOMapper;

       

    private String convertStrAddQuotes(String str) {
    	String array[]=str.split(",");
        String strAddQuotes="(";
        for(int i=0;i<array.length;i++){
        	strAddQuotes+="''"+array[i]+"'',";
        }
        strAddQuotes=strAddQuotes.substring(0, strAddQuotes.length()-1)+")";
		return strAddQuotes;
	}




	public List<Map> getProductionRealtime(String siteIn, String workCenter, String lineIn, String resourceType,
			String resrceIn, String startDate, String endDate, String shift, String model, String item,
			int colNum ,List<ProductionRealtimeVo> paras,Integer pageSize, Integer currpage) {
    	
	 	if (!Utils.isEmpty(shift)) {
	        shift = "SHIFT like " + "''" +"%"+ shift +"%"+"''";
	    } else {
	        shift = "";
	    }
   	 	
   	 	if(!Utils.isEmpty(model)){
        	String models = convertStrAddQuotes(model);
        	model="MODEL IN "+models;
        }else{
        	model="";
        }
   	 	
	   	if(!Utils.isEmpty(item)){
	     	String items = convertStrAddQuotes(item);
	     	item="PRODUCT_ITEM IN "+items;
	     }else{
	    	 item="";
	     }
	   	
       if(!Utils.isEmpty(lineIn)){
       	String lineAreas = convertStrAddQuotes(lineIn);
       	lineIn="LINE_AREA IN "+lineAreas;
       }else{
    	   lineIn="";
       }
       if(!Utils.isEmpty(resourceType)){
       	String resourceTypes = convertStrAddQuotes(resourceType);
       	resourceType="RESOURCE_TYPE IN "+resourceTypes;
       }else{
           resourceType="";
       }
       if(!Utils.isEmpty(resrceIn)){
       	String resourceCodes = convertStrAddQuotes(resrceIn);
       	resrceIn="RESRCE IN "+resourceCodes;
       }else{
    	   resrceIn="";
       }
       if(!Utils.isEmpty(workCenter)){
       	String workAreas =  convertStrAddQuotes(workCenter);
       	workCenter = "WORK_CENTER IN "+ workAreas;
       }else{
    	  workCenter="";
       }
       
       List<Map> vos = productionRealtimeReportPOMapper.getProductionRealtime(siteIn, workCenter, lineIn, resourceType, resrceIn,
                startDate, endDate, shift, model, item, colNum,paras,pageSize, currpage);
       	return vos;
       
   }


    
}
