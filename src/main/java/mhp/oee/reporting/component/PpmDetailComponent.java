package mhp.oee.reporting.component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.dao.extend.OeePOMapper;
import mhp.oee.dao.extend.PpmDetailPOMapper;
import mhp.oee.po.extend.OeeOeePO;
import mhp.oee.po.extend.PpmReportPO;
import mhp.oee.po.gen.ActivityParamValuePO;

import mhp.oee.utils.Utils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by SuMingzhi on 2016/11/23.
 */
@Component
public class PpmDetailComponent {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    PpmDetailPOMapper ppmPOMapper;

    // 查询ppm报表
    public List<PpmReportPO> selectPpmOutputReport(String[] model, String[] itemIn, String site, String[] resrceIn,
                                                   String startDate, String endDate, String byConditionType,
                                                   String[] lineIn, String[] workCenterIn, List<ActivityParamValuePO> list,String shift) {
    	if(shift != null && shift.length() > 0) {
    		shift = "SHIFT like ''"+shift+"%''";
    	}
        return ppmPOMapper.selectPpm(Utils.formatInParams("MODEL", model), Utils.formatInParams("ITEM", itemIn), site, Utils.formatInParams("RESRCE", resrceIn), startDate,
                endDate, byConditionType, Utils.formatInParams("LINE_AREA", lineIn), Utils.formatInParams("WORK_AREA", workCenterIn),
                list.get(0).getParamValue(), list.get(1).getParamValue(), list.get(2).getParamValue(), list.get(3).getParamValue(), list.get(4).getParamValue(),shift);
    }

    // 查询ppm报表公式参数
    public List<ActivityParamValuePO> selectParamValue(String site, String paramId) {
        //1. 取activity_param_value 维护值
        List<ActivityParamValuePO> activityParamValueList = ppmPOMapper.selectParamValue(site);
        //2. 如果没有人工维护 取默认值
        if(CollectionUtils.isEmpty(activityParamValueList)){
            activityParamValueList = this.ppmPOMapper.selectDefaultParamValue();
        }
        return activityParamValueList;

    }
}
