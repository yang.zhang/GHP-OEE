package mhp.oee.reporting.component;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.dao.extend.ProductionOutputReportPOMapper;
import mhp.oee.vo.ProductionOutputFirstAndFollowUpConditionVO;
import mhp.oee.vo.ProductionOutputFirstAndFollowUpVO;

@Component
public class ProductionOutputReportComponent {

    @Autowired
    private ProductionOutputReportPOMapper productionOutputReportPOMapper;

    public List<ProductionOutputFirstAndFollowUpVO> selectProductionOutputReport(
            ProductionOutputFirstAndFollowUpConditionVO vo) {
        return productionOutputReportPOMapper.selectProductionOutputReport(vo);
    }

}
