package mhp.oee.reporting.component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.dao.extend.OeeAlarmLineMapper;
import mhp.oee.dao.extend.OeePOMapper;
import mhp.oee.po.extend.OeeAlarmLinePO;
import mhp.oee.po.extend.OeeOeePO;
import mhp.oee.po.extend.OeeReasonCodeGroupPO;
import mhp.oee.po.extend.OeeReasonCodePO;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by LinZuK on 2016/8/12.
 */
@Component
public class OeeComponent {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    OeePOMapper oeePOMapper;
    @Autowired
    OeeAlarmLineMapper oeeAlarmLineMapper;

    // 查询oee报表
    public List<OeeOeePO> selectOeePOList(String site, String resourceBo, String startDateTime, String endDateTime, String byTimeType) {
        return oeePOMapper.selectOee(site, resourceBo, startDateTime, endDateTime, byTimeType);
    }

    // 查询oee表格和chart图
    public Map<String, Object> selectOeeOutputReport(String site, String resourceBo, String startDateTime, String endDateTime, String byTimeType) {
        List<OeeOeePO> oeePOList = selectOeePOList(site, resourceBo, startDateTime, endDateTime, byTimeType);
        Map<String, List<BigDecimal>> valueMap = new LinkedHashMap<String, List<BigDecimal>>();
        // group by byTime
        DecimalFormat df = new DecimalFormat("0.00");
        for (OeeOeePO oeePO : oeePOList) {
        	if(!("".equals(oeePO.getA())||oeePO.getA()==null)){
        		oeePO.setA(df.format(Double.parseDouble(oeePO.getA().split("%")[0]))+"%");
        	}
        	if(!("".equals(oeePO.getP())||oeePO.getP()==null)){
        		oeePO.setP(df.format(Double.parseDouble(oeePO.getP().split("%")[0]))+"%");
        	}
        	if(!("".equals(oeePO.getQ())||oeePO.getQ()==null)){
        		oeePO.setQ(df.format(Double.parseDouble(oeePO.getQ().split("%")[0]))+"%");
        	}
        	if(!("".equals(oeePO.getOee())||oeePO.getOee()==null)){
        		oeePO.setOee(df.format(Double.parseDouble(oeePO.getOee().split("%")[0]))+"%");
        	}
            String byTime;
            if ("X".equalsIgnoreCase(byTimeType)) {
                byTime = oeePO.getByTime();
            } else {
                byTime = oeePO.getByTimeChart();
            }
            if (null != oeePO.getOeeValue()) {
                List<BigDecimal> oeeList = valueMap.get(byTime);
                if (oeeList == null) {
                    oeeList = new LinkedList<BigDecimal>();
                }
                oeeList.add(oeePO.getOeeValue());
                valueMap.put(byTime, oeeList);
            }
        }
        // avg oee
        List<Map<String, String>> oeeAvgList = new LinkedList<Map<String, String>>();
        Iterator iterator = valueMap.keySet().iterator();
        while (iterator.hasNext()) {
            String byTime = (String) iterator.next();
            List<BigDecimal> oeeList = valueMap.get(byTime);
            Map<String, String> oeeAvg = new HashMap<String, String>();
            oeeAvg.put("byTime", byTime);
            oeeAvg.put("oeeAvg", calculateOeeAvg(oeeList)); // 将每个分组的oee平均值放入map
            oeeAvgList.add(oeeAvg);
        }

        // pkg data
        Map<String, Object> data = new HashedMap();
        data.put("table", oeePOList);
        data.put("chart", oeeAvgList);
        return data;
    }

    private String calculateOeeAvg(List<BigDecimal> oeeList) {
        double total = 0;
        for (BigDecimal oee : oeeList) {
            total += oee.doubleValue();
        }
        double avg = 100 * total / oeeList.size();
        avg = new BigDecimal(avg).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        return avg + "";
    }

    // 原因代码报表
    public List<OeeReasonCodePO> selectOeeReasonCodeReport(String site, String resourceBo, String startDateTime, String endDateTime, String byTimeType) {
        return oeePOMapper.selectReasonCode(site, resourceBo, startDateTime, endDateTime, byTimeType);
    }

    // 原因代码组报表
    public List<OeeReasonCodeGroupPO> selectOeeReasonCodeGroupReport(String site, String resourceBo, String startDateTime, String endDateTime, String byTimeType) {
        return oeePOMapper.selectReasonCodeGroup(site, resourceBo, startDateTime, endDateTime, byTimeType);
    }

    // 查询结果告警线
    public List<OeeAlarmLinePO> selectOeeAlarmLine(String site) {
        return oeeAlarmLineMapper.select(site);
    }
}
