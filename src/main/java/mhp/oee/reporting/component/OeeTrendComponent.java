package mhp.oee.reporting.component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.dao.extend.OeeAlarmLineMapper;
import mhp.oee.dao.extend.OeeTrendPOMapper;
import mhp.oee.po.extend.OeeOeeTrendPO;
import mhp.oee.reporting.common.SearchHelpReportService;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceVO;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by SuMingzhi on 2016/12/21.
 */
@Component
public class OeeTrendComponent {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    SimpleDateFormat sDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    @Autowired
    OeeTrendPOMapper oeeTrendPOMapper;
    @Autowired
    OeeAlarmLineMapper oeeAlarmLineMapper;
    @Autowired
    SearchHelpReportService searchHelpReportService; 
    
    DecimalFormat df = new DecimalFormat("0.00");   
    
    // 查询oee明细报表
    public List<Object> selectOeeTrendList(String site, String workCenter,String resourceType, String resourceBo, String endDate, String shiftLike,String startDate,String pro,String model,String byTimeType,String workCenterLink,List XValues) {
    	List<OeeOeeTrendPO> returnOeeList = new ArrayList<OeeOeeTrendPO>();
    	String[] workCenterArray = null;
    	if(workCenter != null && workCenter.length() > 0) {
    		workCenterArray = workCenter.split(",");
    		workCenter = Utils.formatInParams("WORK_AREA_CODE", workCenterArray);
    	}
    	String[] resourceTypeArray = null;
    	if(resourceType != null && resourceType.length() > 0) {
    		resourceTypeArray = resourceType.split(",");
    		resourceType = Utils.formatInParams("RESRCE_TYPE", resourceTypeArray);
    	}
    	String[] workCenterLinkArray=null;
    	if(workCenterLink != null && workCenterLink.length() > 0) {
    		workCenterLinkArray = workCenterLink.split(",");
    		workCenterLink = Utils.formatInParams("WORK_CENTER_CODE", workCenterLinkArray);
    	}
    	if(shiftLike != null && shiftLike.length() > 0) {
    		shiftLike = "SHIFT like ''"+shiftLike+"%''";
    	}
    	if(pro != null && pro.length() > 0) {
    		String[] proArray = pro.split(",");
    		pro = Utils.formatInParams("USER_ID", proArray);
    	}
    	if(model != null && model.length() > 0) {
    		String[] modelArray = model.split(",");
    		model = Utils.formatInParams("MODEL", modelArray);
    	}
    	List<OeeOeeTrendPO> allOop = new ArrayList<OeeOeeTrendPO>();
    	//设备不为空
    	if(resourceBo != null && resourceBo.length() > 0) {
    		String[] resourceBoArray = resourceBo.split(",");
    		for(String resrce:resourceBoArray){
    			String[] oopArr = {resrce};
    			resourceBo = Utils.formatInParams("RESRCE", oopArr);
    			List<OeeOeeTrendPO> selectOeeDetail = oeeTrendPOMapper.selectOeeTrend(site, workCenter, resourceBo, endDate, shiftLike, startDate,pro,model, workCenterLink);
    			
    			if(byTimeType.equals("D")){
    	    		//按日计算均值
    	    		List<String> dayList = new ArrayList<String>();
    	    		String dayTime = null;
    	    		for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){//将所有班次Day去重添加到一个集合
    	    			dayTime = oeeOeePO.getByTimeChart();
    	    			if(!dayList.contains(dayTime)){
    	    				dayList.add(dayTime);
    	    			}
    	    		}
    	    		for(String dayEachTime:dayList){
    	    			OeeOeeTrendPO oeeByResrce = null;
    	    			double oee = 0.00;
    	    			double a = 0.00;
    	    			double p = 0.00;
    	    			double q = 0.00;
    	    			double i = 0.00;
    	    			for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){
    	    				if(oeeOeePO.getByTimeChart().equals(dayEachTime) && oeeOeePO.getOee()!=null && !Double.isNaN(oeeOeePO.getOee()) ){
    	    					oeeByResrce = (OeeOeeTrendPO) oeeOeePO.clone() ;
    	    					oee+= oeeOeePO.getOee().doubleValue();
    	    					a += oeeOeePO.getA().doubleValue();
    	    					p += oeeOeePO.getP().doubleValue();
    	    					q += oeeOeePO.getQ().doubleValue();
    	    					i++;
    	    				}
    	    			}
    	    			if(oeeByResrce==null)continue;
    	    			if(i==0.00)i=1.00;
    	    			oeeByResrce.setByTimeChart(dayEachTime);
    	    			oeeByResrce.setOee(Double.valueOf(df.format(oee/i)));
    	    			oeeByResrce.setA(Double.valueOf(df.format(a/i)));
    	    			oeeByResrce.setP(Double.valueOf(df.format(p/i)));
    	    			oeeByResrce.setQ(Double.valueOf(df.format(q/i)));
    	    			returnOeeList.add(oeeByResrce);
    	    		}
    		    }else if(byTimeType.equals("W")){
    	    		//按周计算均值
    		    	Date parseStart = null;
    	    		Date parseEnd = null;
    				try {
    					parseStart = sDf.parse(startDate+" 00:00:00");
    					parseEnd = sDf.parse(endDate+" 00:00:00");
    				} catch (ParseException e) {
    					e.printStackTrace();
    				}
    	    		while(parseStart.getTime()<=parseEnd.getTime()-604800000){
    	    			OeeOeeTrendPO oeeByResrce = null;
    	    			double oee = 0.00;
    	    			double a = 0.00;
    	    			double p = 0.00;
    	    			double q = 0.00;
    	    			double i = 0.00;
    	    			for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){
    	    				try {
    	    					Calendar cloeeOeePO = Calendar.getInstance();
    	    	    			cloeeOeePO.setTime(sDf.parse(oeeOeePO.getByTimeChart()+" 00:00:00")); 
    	    	    			int weekOeeOeePO = cloeeOeePO.get(Calendar.WEEK_OF_YEAR);
    	    	    			Calendar cloeeOeeP = Calendar.getInstance();
    	    	    			cloeeOeeP.setTime(parseStart); 
    	    	    			int weekStart = cloeeOeeP.get(Calendar.WEEK_OF_YEAR);
    							if(parseStart.getYear()==sDf.parse(oeeOeePO.getByTimeChart()+" 00:00:00").getYear() && weekStart==weekOeeOeePO && oeeOeePO.getOee()!=null && !Double.isNaN(oeeOeePO.getOee())){
    								if(oeeByResrce==null)
    									oeeByResrce = (OeeOeeTrendPO) oeeOeePO.clone() ;
	    								oee+= oeeOeePO.getOee();
    									a += oeeOeePO.getA();
    									p += oeeOeePO.getP();
    									q += oeeOeePO.getQ();
    									i++;
    							}
    						} catch (ParseException e) {
    							e.printStackTrace();
    						}
    	    			}
    	    			Calendar strCleach = Calendar.getInstance();
    	    			strCleach.setTime(parseStart); 
    	    			int strWeek = strCleach.get(Calendar.WEEK_OF_YEAR);
    	    			if(oeeByResrce==null){
    	    				parseStart=new Date(parseStart.getTime()+604800000);
    	    				continue;
    	    			}
    	    			if(i==0.00)i=1.00;
    	    			oeeByResrce.setByTimeChart(parseStart.getYear()+1900+"-W"+strWeek);
    	    			oeeByResrce.setOee(Double.valueOf(df.format(oee/i)));
    	    			oeeByResrce.setA(Double.valueOf(df.format(a/i)));
    	    			oeeByResrce.setP(Double.valueOf(df.format(p/i)));
    	    			oeeByResrce.setQ(Double.valueOf(df.format(q/i)));
    	    			returnOeeList.add(oeeByResrce);
    	    			parseStart=new Date(parseStart.getTime()+604800000);
    	    		}
    		    }else if(byTimeType.equals("M")){
    	    		//按月计算均值
    	    		List<String> monthList = new ArrayList<String>();
    	    		String monthTime = null;
    	    		for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){//将所有班次Day去重添加到一个集合
    	    			monthTime = oeeOeePO.getByTimeMonth();
    	    			if(!monthList.contains(monthTime)){
    	    				monthList.add(monthTime);
    	    			}
    	    		}
    	    		for(String monthEachTime:monthList){
    	    			OeeOeeTrendPO oeeByResrce = null;
    	    			double oee = 0.00;
    	    			double a = 0.00;
    	    			double p = 0.00;
    	    			double q = 0.00;
    	    			double i = 0.00;
    	    			for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){
    	    				if(oeeOeePO.getByTimeMonth().equals(monthEachTime) && oeeOeePO.getOee()!=null && !Double.isNaN(oeeOeePO.getOee())){
    	    					oeeByResrce = (OeeOeeTrendPO) oeeOeePO.clone() ;
    	    					oee+= oeeOeePO.getOee();
    	    					a += oeeOeePO.getA();
    	    					p += oeeOeePO.getP();
    	    					q += oeeOeePO.getQ();
    	    					i++;
    	    				}
    	    			}
    	    			if(oeeByResrce==null)continue;
    	    			if(i==0.00)i=1.00;
    	    			oeeByResrce.setByTimeChart(monthEachTime);
    	    			oeeByResrce.setOee(Double.valueOf(df.format(oee/i)));
    	    			oeeByResrce.setA(Double.valueOf(df.format(a/i)));
    	    			oeeByResrce.setP(Double.valueOf(df.format(p/i)));
    	    			oeeByResrce.setQ(Double.valueOf(df.format(q/i)));
    	    			returnOeeList.add(oeeByResrce);
    	    		}
    		    }else if(byTimeType.equals("Y")){
    	    		//按年计算均值
    	    		List<String> yearList = new ArrayList<String>();
    	    		String yearTime = null;
    	    		for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){//将所有班次Day去重添加到一个集合
    	    			yearTime = oeeOeePO.getByTimeYear();
    	    			if(!yearList.contains(yearTime)){
    	    				yearList.add(yearTime);
    	    			}
    	    		}
    	    		for(String yearEachTime:yearList){
    	    			OeeOeeTrendPO oeeByResrce = null;
    	    			double oee = 0.00;
    	    			double a = 0.00;
    	    			double p = 0.00;
    	    			double q = 0.00;
    	    			double i = 0.00;
    	    			for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){
    	    				if(oeeOeePO.getByTimeYear().equals(yearEachTime) && oeeOeePO.getOee()!=null && !Double.isNaN(oeeOeePO.getOee())){
    	    					oeeByResrce = (OeeOeeTrendPO) oeeOeePO.clone() ;
    	    						oee+= oeeOeePO.getOee();
        	    					a += oeeOeePO.getA();
        	    					p += oeeOeePO.getP();
        	    					q += oeeOeePO.getQ();
        	    					i++;
    	    				}
    	    			}
    	    			if(oeeByResrce==null)continue;
    	    			if(i==0.00)i=1.00;
    	    			oeeByResrce.setByTimeChart(yearEachTime);
    	    			oeeByResrce.setOee(Double.valueOf(df.format(oee/i)));
    	    			oeeByResrce.setA(Double.valueOf(df.format(a/i)));
    	    			oeeByResrce.setP(Double.valueOf(df.format(p/i)));
    	    			oeeByResrce.setQ(Double.valueOf(df.format(q/i)));
    	    			returnOeeList.add(oeeByResrce);
    	    		}
    		    }
    			allOop.addAll(selectOeeDetail);
    			/*for(OeeOeePO e:selectOeeDetail){
    				allOop.add(e);
    			}*/
    		}
    	}else{
    		//设备为空时
    		String resource = null;
    		List<ResourceVO> vos = new ArrayList<ResourceVO>();
    		if(resourceTypeArray!=null&&resourceTypeArray.length>0){
    			for(String resourceTy:resourceTypeArray){
    				vos.addAll(searchHelpReportService.readResourceHelp(site, resourceTy,resource, workCenterLinkArray, workCenterArray));
    			}
    		}else
    			vos = searchHelpReportService.readResourceHelp(site, resource,resource, workCenterLinkArray, workCenterArray);
            if(vos.isEmpty())
            	return null;
    		for(ResourceVO ro:vos){
    			String[] oopArr = {ro.getResrce()};
    			resourceBo = Utils.formatInParams("RESRCE", oopArr);
    			List<OeeOeeTrendPO> selectOeeDetail = oeeTrendPOMapper.selectOeeTrend(site, workCenter, resourceBo, endDate, shiftLike, startDate,pro,model,workCenterLink);
    			
    			if(byTimeType.equals("D")){
    	    		//按日计算均值
    	    		List<String> dayList = new ArrayList<String>();
    	    		String dayTime = null;
    	    		for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){//将所有班次Day去重添加到一个集合
    	    			dayTime = oeeOeePO.getByTimeChart();
    	    			if(!dayList.contains(dayTime)){
    	    				dayList.add(dayTime);
    	    			}
    	    		}
    	    		for(String dayEachTime:dayList){
    	    			OeeOeeTrendPO oeeByResrce = null;
    	    			double oee = 0.00;
    	    			double a = 0.00;
    	    			double p = 0.00;
    	    			double q = 0.00;
    	    			double i = 0.00;
    	    			for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){
    	    				if(oeeOeePO.getByTimeChart().equals(dayEachTime) && oeeOeePO.getOee()!=null && !Double.isNaN(oeeOeePO.getOee())){
    	    					oeeByResrce = (OeeOeeTrendPO) oeeOeePO.clone() ;
    	    					oee+= oeeOeePO.getOee();
    	    					a += oeeOeePO.getA();
    	    					p += oeeOeePO.getP();
    	    					q += oeeOeePO.getQ();
    	    					i++;
    	    				}
    	    			}
    	    			if(oeeByResrce==null)continue;
    	    			if(i==0.00)i=1.00;
    	    			oeeByResrce.setByTimeChart(dayEachTime);
    	    			oeeByResrce.setOee(Double.valueOf(df.format(oee/i)));
    	    			oeeByResrce.setA(Double.valueOf(df.format(a/i)));
    	    			oeeByResrce.setP(Double.valueOf(df.format(p/i)));
    	    			oeeByResrce.setQ(Double.valueOf(df.format(q/i)));
    	    			returnOeeList.add(oeeByResrce);
    	    		}
    		    }else if(byTimeType.equals("W")){
    	    		//按周计算均值
    		    	Date parseStart = null;
    	    		Date parseEnd = null;
    				try {
    					parseStart = sDf.parse(startDate+" 00:00:00");
    					parseEnd = sDf.parse(endDate+" 00:00:00");
    				} catch (ParseException e) {
    					e.printStackTrace();
    				}
    	    		while(parseStart.getTime()<=parseEnd.getTime()-604800000){
    	    			OeeOeeTrendPO oeeByResrce = null;
    	    			double oee = 0.00;
    	    			double a = 0.00;
    	    			double p = 0.00;
    	    			double q = 0.00;
    	    			double i = 0.00;
    	    			for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){
    	    				try {
    	    					Calendar cloeeOeePO = Calendar.getInstance();
    	    	    			cloeeOeePO.setTime(sDf.parse(oeeOeePO.getByTimeChart()+" 00:00:00")); 
    	    	    			int weekOeeOeePO = cloeeOeePO.get(Calendar.WEEK_OF_YEAR);
    	    	    			Calendar cloeeOeeP = Calendar.getInstance();
    	    	    			cloeeOeeP.setTime(parseStart); 
    	    	    			int weekStart = cloeeOeeP.get(Calendar.WEEK_OF_YEAR);
    							if(parseStart.getYear()==sDf.parse(oeeOeePO.getByTimeChart()+" 00:00:00").getYear() && weekStart==weekOeeOeePO && oeeOeePO.getOee()!=null && !Double.isNaN(oeeOeePO.getOee())){
    								if(oeeByResrce==null)
    									oeeByResrce = (OeeOeeTrendPO) oeeOeePO.clone() ;
	    								oee+= oeeOeePO.getOee();
    									a += oeeOeePO.getA();
    									p += oeeOeePO.getP();
    									q += oeeOeePO.getQ();
    									i++;
    							}
    						} catch (ParseException e) {
    							e.printStackTrace();
    						}
    	    			}
    	    			Calendar strCleach = Calendar.getInstance();
    	    			strCleach.setTime(parseStart); 
    	    			int strWeek = strCleach.get(Calendar.WEEK_OF_YEAR);
    	    			if(oeeByResrce==null){
    	    				parseStart=new Date(parseStart.getTime()+604800000);
    	    				continue;
    	    			}
    	    			if(i==0.00)i=1.00;
    	    			oeeByResrce.setByTimeChart(parseStart.getYear()+1900+"-W"+strWeek);
    	    			oeeByResrce.setOee(Double.valueOf(df.format(oee/i)));
    	    			oeeByResrce.setA(Double.valueOf(df.format(a/i)));
    	    			oeeByResrce.setP(Double.valueOf(df.format(p/i)));
    	    			oeeByResrce.setQ(Double.valueOf(df.format(q/i)));
    	    			returnOeeList.add(oeeByResrce);
    	    			parseStart=new Date(parseStart.getTime()+604800000);
    	    		}
    		    }else if(byTimeType.equals("M")){
    	    		//按月计算均值
    	    		List<String> monthList = new ArrayList<String>();
    	    		String monthTime = null;
    	    		for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){//将所有班次Day去重添加到一个集合
    	    			monthTime = oeeOeePO.getByTimeMonth();
    	    			if(!monthList.contains(monthTime)){
    	    				monthList.add(monthTime);
    	    			}
    	    		}
    	    		for(String monthEachTime:monthList){
    	    			OeeOeeTrendPO oeeByResrce = null;
    	    			double oee = 0.00;
    	    			double a = 0.00;
    	    			double p = 0.00;
    	    			double q = 0.00;
    	    			double i = 0.00;
    	    			for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){
    	    				if(oeeOeePO.getByTimeMonth().equals(monthEachTime) && oeeOeePO.getOee()!=null && !Double.isNaN(oeeOeePO.getOee())){
    	    					oeeByResrce = (OeeOeeTrendPO) oeeOeePO.clone() ;
    	    					oee+= oeeOeePO.getOee();
    	    					a += oeeOeePO.getA();
    	    					p += oeeOeePO.getP();
    	    					q += oeeOeePO.getQ();
    	    					i++;
    	    				}
    	    			}
    	    			if(oeeByResrce==null)continue;
    	    			if(i==0.00)i=1.00;
    	    			oeeByResrce.setByTimeChart(monthEachTime);
    	    			oeeByResrce.setOee(Double.valueOf(df.format(oee/i)));
    	    			oeeByResrce.setA(Double.valueOf(df.format(a/i)));
    	    			oeeByResrce.setP(Double.valueOf(df.format(p/i)));
    	    			oeeByResrce.setQ(Double.valueOf(df.format(q/i)));
    	    			returnOeeList.add(oeeByResrce);
    	    		}
    		    }else if(byTimeType.equals("Y")){
    	    		//按年计算均值
    	    		List<String> yearList = new ArrayList<String>();
    	    		String yearTime = null;
    	    		for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){//将所有班次Day去重添加到一个集合
    	    			yearTime = oeeOeePO.getByTimeYear();
    	    			if(!yearList.contains(yearTime)){
    	    				yearList.add(yearTime);
    	    			}
    	    		}
    	    		for(String yearEachTime:yearList){
    	    			OeeOeeTrendPO oeeByResrce = null;
    	    			double oee = 0.00;
    	    			double a = 0.00;
    	    			double p = 0.00;
    	    			double q = 0.00;
    	    			double i = 0.00;
    	    			for(OeeOeeTrendPO oeeOeePO:selectOeeDetail){
    	    				if(oeeOeePO.getByTimeYear().equals(yearEachTime) && oeeOeePO.getOee()!=null && !Double.isNaN(oeeOeePO.getOee())){
    	    					oeeByResrce = (OeeOeeTrendPO) oeeOeePO.clone() ;
    	    					oee+= oeeOeePO.getOee();
    	    					a += oeeOeePO.getA();
    	    					p += oeeOeePO.getP();
    	    					q += oeeOeePO.getQ();
    	    					i++;
    	    				}
    	    			}
    	    			if(oeeByResrce==null)continue;
    	    			if(i==0.00)i=1.00;
    	    			oeeByResrce.setByTimeChart(yearEachTime);
    	    			oeeByResrce.setOee(Double.valueOf(df.format(oee/i)));
    	    			oeeByResrce.setA(Double.valueOf(df.format(a/i)));
    	    			oeeByResrce.setP(Double.valueOf(df.format(p/i)));
    	    			oeeByResrce.setQ(Double.valueOf(df.format(q/i)));
    	    			returnOeeList.add(oeeByResrce);
    	    		}
    		    }
    			allOop.addAll(selectOeeDetail);
    			/*for(OeeOeePO e:selectOeeDetail){
    				allOop.add(e);
    			}*/
    		}
    		
    	}
    	Collections.sort(allOop,new Comparator<OeeOeeTrendPO>(){

			@Override
			public int compare(OeeOeeTrendPO o1, OeeOeeTrendPO o2) {
				int flag = 0;
				try {
					flag =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(o1.getShiftStartTime()).compareTo(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(o2.getShiftStartTime()));
					if(flag==0){
						flag = o1.getWorkArea().compareTo(o2.getWorkArea());
						if(flag==0){
							flag = o1.getWorkCenter().compareTo(o2.getWorkCenter());
						}
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return flag;
			}});
    	
    	Map<String,Map<String,Double>> returnMap = new HashMap<String,Map<String,Double>>();
    	//均值计算逻辑
    	if(byTimeType.equals("S")){
    		returnOeeList = allOop;
    		//按班次计算均值
    		List<String> shiftList = new ArrayList<String>();
    		String shiftTime = null;
    		for(OeeOeeTrendPO oeeOeePO:allOop){//将所有班次时间去重添加到一个集合
    			shiftTime = oeeOeePO.getByTime();
    			if(!shiftList.contains(shiftTime)){
    				shiftList.add(shiftTime);
    			}
    		}
    		if(XValues.isEmpty()||XValues.size()<shiftList.size()){
    			XValues.clear();
    			XValues.addAll(shiftList);
    		}
    		for(String shiftEachTime:shiftList){
    			double oee = 0.00;
    			double a = 0.00;
    			double p = 0.00;
    			double q = 0.00;
    			double i = 0.00;
    			for(OeeOeeTrendPO oeeOeePO:allOop){
    				if(oeeOeePO.getByTime().equals(shiftEachTime) && oeeOeePO.getOee()!=null && !Double.isNaN(oeeOeePO.getOee())){
    					oee+= oeeOeePO.getOee();
    					a += oeeOeePO.getA();
    					p += oeeOeePO.getP();
    					q += oeeOeePO.getQ();
    					i++;
    				}
    			}
    			if(i==0.00)i=1.00;
    			Map<String,Double> nodeValue = new HashMap<String,Double>();
    			nodeValue.put("oee", Double.valueOf(df.format(oee/i)));
    			nodeValue.put("a", Double.valueOf(df.format(a/i)));
    			nodeValue.put("p", Double.valueOf(df.format(p/i)));
    			nodeValue.put("q", Double.valueOf(df.format(q/i)));
    			returnMap.put(shiftEachTime,nodeValue);
    		}
	    	}else if(byTimeType.equals("D")){
	    		//按天计算均值
	    		List<String> dayList = new ArrayList<String>();
	    		String dayTime = null;
	    		for(OeeOeeTrendPO oeeOeePO:allOop){//将所有by day时间去重添加到一个集合
	    			dayTime = oeeOeePO.getByTimeChart();
	    			if(!dayList.contains(dayTime)){
	    				dayList.add(dayTime);
	    			}
	    		}
	    		if(XValues.isEmpty()||XValues.size()<dayList.size()){
	    			XValues.clear();
	    			XValues.addAll(dayList);
	    		}
	    		for(String dayEachTime:dayList){
	    			double oee = 0.00;
	    			double a = 0.00;
	    			double p = 0.00;
	    			double q = 0.00;
	    			double i = 0.00;
	    			for(OeeOeeTrendPO oeeOeePO:allOop){
	    				if(oeeOeePO.getByTimeChart().equals(dayEachTime) && oeeOeePO.getOee()!=null && !Double.isNaN(oeeOeePO.getOee())){
	    					oee+= oeeOeePO.getOee();
	    					a += oeeOeePO.getA();
	    					p += oeeOeePO.getP();
	    					q += oeeOeePO.getQ();
	    					i++;
	    				}
	    			}
	    			if(i==0.00)i=1.00;
	    			Map<String,Double> nodeValue = new HashMap<String,Double>();
	    			nodeValue.put("oee", Double.valueOf(df.format(oee/i)));
	    			nodeValue.put("a", Double.valueOf(df.format(a/i)));
	    			nodeValue.put("p", Double.valueOf(df.format(p/i)));
	    			nodeValue.put("q", Double.valueOf(df.format(q/i)));
	    			returnMap.put(dayEachTime,nodeValue);
	    		}
	    	}else if(byTimeType.equals("M")){
	    		//按月计算均值
	    		List<String> monthList = new ArrayList<String>();
	    		String monthTime = null;
	    		for(OeeOeeTrendPO oeeOeePO:allOop){//将所有by month时间去重添加到一个集合
	    			monthTime = oeeOeePO.getByTimeMonth();
	    			if(!monthList.contains(monthTime)){
	    				monthList.add(monthTime);
	    			}
	    		}
	    		if(XValues.isEmpty()||XValues.size()<monthList.size()){
	    			XValues.clear();
	    			XValues.addAll(monthList);
	    		}
	    		for(String monthEachTime:monthList){
	    			double oee = 0.00;
	    			double a = 0.00;
	    			double p = 0.00;
	    			double q = 0.00;
	    			double i = 0.00;
	    			for(OeeOeeTrendPO oeeOeePO:allOop){
	    				if(oeeOeePO.getByTimeMonth().equals(monthEachTime) && oeeOeePO.getOee()!=null && !Double.isNaN(oeeOeePO.getOee())){
	    						oee+= oeeOeePO.getOee();
    	    					a += oeeOeePO.getA();
    	    					p += oeeOeePO.getP();
    	    					q += oeeOeePO.getQ();
    	    					i++;
	    				}
	    			}
	    			if(i==0.00)i=1.00;
	    			Map<String,Double> nodeValue = new HashMap<String,Double>();
	    			nodeValue.put("oee", Double.valueOf(df.format(oee/i)));
	    			nodeValue.put("a", Double.valueOf(df.format(a/i)));
	    			nodeValue.put("p", Double.valueOf(df.format(p/i)));
	    			nodeValue.put("q", Double.valueOf(df.format(q/i)));
	    			returnMap.put(monthEachTime,nodeValue);
	    		}
	    	}else if(byTimeType.equals("Y")){
	    		//按年计算均值
	    		List<String> yearList = new ArrayList<String>();
	    		String yearTime = null;
	    		for(OeeOeeTrendPO oeeOeePO:allOop){//将所有by year时间去重添加到一个集合
	    			yearTime = oeeOeePO.getByTimeYear();
	    			if(!yearList.contains(yearTime)){
	    				yearList.add(yearTime);
	    			}
	    		}
	    		if(XValues.isEmpty()||XValues.size()<yearList.size()){
	    			XValues.clear();
	    			XValues.addAll(yearList);
	    		}
	    		for(String yearEachTime:yearList){
	    			double oee = 0.00;
	    			double a = 0.00;
	    			double p = 0.00;
	    			double q = 0.00;
	    			double i = 0.00;
	    			for(OeeOeeTrendPO oeeOeePO:allOop){
	    				if(oeeOeePO.getByTimeYear().equals(yearEachTime) && oeeOeePO.getOee()!=null && !Double.isNaN(oeeOeePO.getOee())){
	    					oee+= oeeOeePO.getOee();
	    					a += oeeOeePO.getA();
	    					p += oeeOeePO.getP();
	    					q += oeeOeePO.getQ();
	    					i++;
	    				}
	    			}
	    			if(i==0.00)i=1.00;
	    			Map<String,Double> nodeValue = new HashMap<String,Double>();
	    			nodeValue.put("oee", Double.valueOf(df.format(oee/i)));
	    			nodeValue.put("a", Double.valueOf(df.format(a/i)));
	    			nodeValue.put("p", Double.valueOf(df.format(p/i)));
	    			nodeValue.put("q", Double.valueOf(df.format(q/i)));
	    			returnMap.put(yearEachTime,nodeValue);
	    		}
	    	}else{
	    		List<String> weekList = new ArrayList<String>();
	    		Date parseStart = null;
	    		Date parseEnd = null;
				try {
					parseStart = sDf.parse(startDate+" 00:00:00");
					parseEnd = sDf.parse(endDate+" 00:00:00");
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		while(parseStart.getTime()<=parseEnd.getTime()-604800000){
	    			double oee = 0.00;
	    			double a = 0.00;
	    			double p = 0.00;
	    			double q = 0.00;
	    			double i = 0.00;
	    			for(OeeOeeTrendPO oeeOeePO:allOop){
	    				try{
						if(parseStart.getTime()==sDf.parse(oeeOeePO.getByTimeChart()+" 00:00:00").getTime() && oeeOeePO.getOee()!=null && !Double.isNaN(oeeOeePO.getOee())){
									oee+= oeeOeePO.getOee();
									a += oeeOeePO.getA();
									p += oeeOeePO.getP();
									q += oeeOeePO.getQ();
									i++;
							}
						} catch (ParseException e) {
							e.printStackTrace();
						}
	    			}
	    			if(i==0.00)i=1.00;
	    			Map<String,Double> nodeValue = new HashMap<String,Double>();
	    			nodeValue.put("oee", Double.valueOf(df.format(oee/i)));
	    			nodeValue.put("a", Double.valueOf(df.format(a/i)));
	    			nodeValue.put("p", Double.valueOf(df.format(p/i)));
	    			nodeValue.put("q", Double.valueOf(df.format(q/i)));
	    			
	    			Calendar strCleach = Calendar.getInstance(); 
	    			strCleach.setTime(parseStart); 
	    			int strWeek = strCleach.get(Calendar.WEEK_OF_YEAR);
	    			returnMap.put(parseStart.getYear()+1900+"-W"+strWeek,nodeValue);
	    			weekList.add(parseStart.getYear()+1900+"-W"+strWeek);
	    			parseStart=new Date(parseStart.getTime()+604800000);
	    		}
	    		if(XValues.isEmpty()||XValues.size()<weekList.size()){
	    			XValues.clear();
	    			XValues.addAll(weekList);
	    		}
	    	}


    	List<Object> returnListTotal = new LinkedList<Object>();
    	returnListTotal.add(0, returnOeeList);
    	returnListTotal.add(1,returnMap);
    	return returnListTotal;
    }
}
