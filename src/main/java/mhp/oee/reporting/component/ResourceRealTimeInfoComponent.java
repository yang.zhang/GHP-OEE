package mhp.oee.reporting.component;

import mhp.oee.dao.extend.ResourceRealTimeInfoPOMapper;
import mhp.oee.enumClass.LightColor;
import mhp.oee.enumClass.ResourceRealTimeDimension;
import mhp.oee.po.extend.ResourceRealTimeDetailPO;
import mhp.oee.po.extend.ResourceRealTimeInfoPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by dinglj on 2017/4/28 028.
 */
@Component
public class ResourceRealTimeInfoComponent {


    @Autowired
    private ResourceRealTimeInfoPOMapper resourceRealTimeInfoPOMapper;

    public List<ResourceRealTimeInfoPO> readRealTimeInfo(ResourceRealTimeDimension dimension,
                                                         List<String> siteList,
                                                         List<String> workAreaList,
                                                         List<String> lineAreaList,
                                                         List<String> resourceTypeList){

        return this.resourceRealTimeInfoPOMapper.select(dimension,siteList,workAreaList,lineAreaList,resourceTypeList);
    }

    public List<ResourceRealTimeDetailPO> readRealTimeDetail(LightColor color,
                                                           List<String> siteList,
                                                           List<String> workAreaList,
                                                           List<String> lineAreaList,
                                                           List<String> resourceTypeList){

        return this.resourceRealTimeInfoPOMapper.selectRealTimeDetail(color,siteList,workAreaList,lineAreaList,resourceTypeList);
    }

}
