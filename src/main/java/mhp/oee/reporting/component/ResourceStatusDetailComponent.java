package mhp.oee.reporting.component;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mhp.oee.dao.extend.ResourceStatusDetailReportPOMapper;
import mhp.oee.web.angular.resource.plc.management.PlcInfoService;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceStatusDetailVO;
import mhp.oee.vo.StatVO;

@Component
public class ResourceStatusDetailComponent {

    @Autowired
    private ResourceStatusDetailReportPOMapper resourceStatusDetailReportPOMapper;
    
    @Autowired
    PlcInfoService plcInfoService;

    public List<ResourceStatusDetailVO> getResourceStatusDetail(Integer pageSize, Integer currpage, String site,
            String workArea, String lineArea, String startDate, String endDate, String start_date_time,
            String end_date_time, String resourceType, String resourceCode, String shift, String type, String commentCode) {

        if (!Utils.isEmpty(shift)) {
            shift = "SHIFT like " + "''" +"%"+ shift +"%"+"''";
        } else {
            shift = "";
        }

        // WORK_AREA
        if (!Utils.isEmpty(workArea)) {
            String[] workAreaArray = workArea.split(",");
            workArea = Utils.formatInParams("WORK_AREA", workAreaArray);
        }

        // LINE_AREA
        if (!Utils.isEmpty(lineArea)) {
            String[] lineAreaArray = lineArea.split(",");
            lineArea = Utils.formatInParams("LINE_AREA", lineAreaArray);
        }

        // RESOURCE_TYPE
        if (!Utils.isEmpty(resourceType)) {
            String[] resourceTypeArray = resourceType.split(",");
            resourceType = Utils.formatInParams("RESOURCE_TYPE", resourceTypeArray);
        }

        // RESRCE
        if (!Utils.isEmpty(resourceCode)) {
            String[] resourceCodeArray = resourceCode.split(",");
            resourceCode = Utils.formatInParams("RESRCE", resourceCodeArray);
        }
        
        //commentCode
        if (!Utils.isEmpty(commentCode)) {
            String[] commentCodeArray = commentCode.split(",");
            commentCode = Utils.formatInParams("COMMENT_CODE", commentCodeArray);
        }
       
        
        if ("result_item".equals(type)) {
            return resourceStatusDetailReportPOMapper.resultItem(pageSize, currpage, site, workArea, lineArea,
                    startDate, endDate, start_date_time, end_date_time, resourceType, resourceCode, shift);
        } else if ("result_alert".equals(type)) {
            return resourceStatusDetailReportPOMapper.resultAlert(pageSize, currpage, site, workArea, lineArea,
                    startDate, endDate, start_date_time, end_date_time, resourceType, resourceCode, shift,commentCode);
        } else if ("result_reason_code".equals(type)) {
            return resourceStatusDetailReportPOMapper.resultReasonCode(pageSize, currpage, site, workArea, lineArea,
                    startDate, endDate, start_date_time, end_date_time, resourceType, resourceCode, shift);
        } else if ("result_output".equals(type)) {
            return resourceStatusDetailReportPOMapper.resultOutPut(pageSize, currpage, site, workArea, lineArea,
                    startDate, endDate, start_date_time, end_date_time, resourceType, resourceCode, shift);
        } else if ("result_rt_reason_code".equals(type)) {
            return resourceStatusDetailReportPOMapper.resultRtReasonCode(pageSize, currpage, site, workArea, lineArea,
                    startDate, endDate, start_date_time, end_date_time, resourceType, resourceCode, shift);
        } else {
            return null;
        }
    }

    public StatVO getResourceStatusDetailCount(String site, String workArea, String lineArea, String startDate,
            String endDate, String start_date_time, String end_date_time, String resourceType, String resourceCode,
            String shift) {

        if (!Utils.isEmpty(shift)) {
            shift = "SHIFT like " + "''" +"%"+ shift +"%"+"''";
        } else {
            shift = "";
        }

        // WORK_AREA
        if (!Utils.isEmpty(workArea)) {
            String[] workAreaArray = workArea.split(",");
            workArea = Utils.formatInParams("WORK_AREA", workAreaArray);
        }

        // LINE_AREA
        if (!Utils.isEmpty(lineArea)) {
            String[] lineAreaArray = lineArea.split(",");
            lineArea = Utils.formatInParams("LINE_AREA", lineAreaArray);
        }

        // RESOURCE_TYPE
        if (!Utils.isEmpty(resourceType)) {
            String[] resourceTypeArray = resourceType.split(",");
            resourceType = Utils.formatInParams("RESOURCE_TYPE", resourceTypeArray);
        }

        // RESRCE
        if (!Utils.isEmpty(resourceCode)) {
            String[] resourceCodeArray = resourceCode.split(",");
            resourceCode = Utils.formatInParams("RESRCE", resourceCodeArray);
        }

        StatVO vo = new StatVO();
        vo.setHaltCount(resourceStatusDetailReportPOMapper.resultHaltCount(site, workArea, lineArea, startDate, endDate,
                start_date_time, end_date_time, resourceType, resourceCode, shift));
        vo.setDuration(resourceStatusDetailReportPOMapper.resultDuration(site, workArea, lineArea, startDate, endDate,
                start_date_time, end_date_time, resourceType, resourceCode, shift));
        return vo;
    }

    public StatVO getResourceStatusDetailTotal(String site, String type, String workArea, String lineArea,
            String startDate, String endDate, String start_date_time, String end_date_time, String resourceType,
            String resourceCode, String shift, String commentCode) {
    	
        // WORK_AREA
        if (!Utils.isEmpty(workArea)) {
            String[] workAreaArray = workArea.split(",");
            workArea = Utils.formatInParams("WORK_AREA", workAreaArray);
        }

        // LINE_AREA
        if (!Utils.isEmpty(lineArea)) {
            String[] lineAreaArray = lineArea.split(",");
            lineArea = Utils.formatInParams("LINE_AREA", lineAreaArray);
        }

        // RESOURCE_TYPE
        if (!Utils.isEmpty(resourceType)) {
            String[] resourceTypeArray = resourceType.split(",");
            resourceType = Utils.formatInParams("RESOURCE_TYPE", resourceTypeArray);
        }

        // RESRCE
        if (!Utils.isEmpty(resourceCode)) {
            String[] resourceCodeArray = resourceCode.split(",");
            resourceCode = Utils.formatInParams("RESRCE", resourceCodeArray);
        }

        if (!Utils.isEmpty(shift)) {
            shift = "SHIFT like " + "''" +"%"+ shift +"%"+"''";
        } else {
            shift = "";
        }
        
      //commentCode
        if (!Utils.isEmpty(commentCode)) {
            String[] commentCodeArray = commentCode.split(",");
            commentCode = Utils.formatInParams("COMMENT_CODE", commentCodeArray);
        }
        
        StatVO vo = new StatVO();
        if ("A".equals(type)) {
            vo.setAlertCount(resourceStatusDetailReportPOMapper.resultAlertCount(site, workArea, lineArea, startDate,
                    endDate, start_date_time, end_date_time, resourceType, resourceCode, shift,commentCode));
        } else if ("H".equals(type)) {
            vo.setReasonCodeCount(resourceStatusDetailReportPOMapper.resultReasonCodeCount(site, workArea, lineArea,
                    startDate, endDate,start_date_time, end_date_time, resourceType, resourceCode, shift));
        } else if ("O".equals(type)) {
            vo.setOutputCount(resourceStatusDetailReportPOMapper.resultOutPutCount(site, workArea, lineArea, startDate,
                     endDate,start_date_time, end_date_time, resourceType, resourceCode, shift));
        } else if ("R".equals(type)) {
            vo.setRtReasonCodeCount(resourceStatusDetailReportPOMapper.resultRtReasonCodeCount(site, workArea, lineArea,
                    startDate, endDate, start_date_time, end_date_time, resourceType, resourceCode, shift));
        } else if ("S".equals(type)) {
            vo.setItemCount(resourceStatusDetailReportPOMapper.resultItemCount(site, workArea, lineArea, startDate,
                    endDate, start_date_time, end_date_time, resourceType, resourceCode, shift));
        }
        return vo;
    }

	public List<ResourceStatusDetailVO> getResourceStatusDetailCollect(String site, String workArea, String lineArea,
			String startDate, String endDate, String start_date_time, String end_date_time, String resourceType,
			String resource, String shift , String resourceState) {
		
		//按设备循环
		/*//设备为空时，按其它条件取设备；否则将设备转换成可用数组
		String[] resrces = null;
		if(resource == null || "".equals(resource)){
			List<ResourceVO> resVo = plcInfoService.readResourceOptionalWithLineAddLineArea(site, resourceType, lineArea, workArea);
			if(resVo.size()==0){
			    
			    return new ArrayList<ResourceStatusDetailVO>();
			}
			resVo.get(0).getResrce();
			String[] res = new String[resVo.size()] ;
			String str = "RESRCE = " ;
			for(int i = 0 ;i < resVo.size() ;i++){
				String srt1 = str + "''"+resVo.get(i).getResrce()+"''";
				res[i] = srt1;
			}
			resrces = res;
		}else {
			String str = "RESRCE = " ;
			String[] resourceCodeArray = resource.split(",");
			for(int i=0;i<resourceCodeArray.length;i++){
				resourceCodeArray[i] = str + "''"+resourceCodeArray[i]+"''";
			}
			resrces = resourceCodeArray;
		}*/
		
		String resrce = "";
		// LINE_AREA
        if (!Utils.isEmpty(resource)) {
            String[] resourceArray = resource.split(",");
            resrce = Utils.formatInParams("RESRCE", resourceArray);
        }
		 if (!Utils.isEmpty(shift)) {
	            shift = "SHIFT like " + "''" +"%"+ shift +"%"+"''";
	        } else {
	            shift = "";
	        }

	        // WORK_AREA
	        if (!Utils.isEmpty(workArea)) {
	            String[] workAreaArray = workArea.split(",");
	            workArea = Utils.formatInParams("WORK_AREA", workAreaArray);
	        }

	        // LINE_AREA
	        if (!Utils.isEmpty(lineArea)) {
	            String[] lineAreaArray = lineArea.split(",");
	            lineArea = Utils.formatInParams("LINE_AREA", lineAreaArray);
	        }

	        // RESOURCE_TYPE
	        if (!Utils.isEmpty(resourceType)) {
	            String[] resourceTypeArray = resourceType.split(",");
	            resourceType = Utils.formatInParams("RESOURCE_TYPE", resourceTypeArray);
	        }

	        // RESOURCE_STATE
	        if (!Utils.isEmpty(resourceState)) {
	        	resourceState = "RESOURCE_STATE = ''"+resourceState+"''";
	        }
	        
	        
	        List<ResourceStatusDetailVO> vos = resourceStatusDetailReportPOMapper.resultCollect( site, workArea, lineArea,
                    startDate, endDate, start_date_time, end_date_time, resourceType, resrce, shift,resourceState);
	        return vos;
	}
}
