package mhp.oee.reporting.ppmdetail;

import mhp.oee.common.exception.ActivityParamErrorException;
import mhp.oee.common.exception.NotFoundActivityParamException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.reporting.component.PpmDetailComponent;
import mhp.oee.po.extend.OeeOeePO;
import mhp.oee.po.extend.PpmReportPO;
import mhp.oee.po.gen.ActivityParamValuePO;
import mhp.oee.utils.Utils;

import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by SuMingzhi on 2016/11/23.
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class PpmDetailReportService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private PpmDetailComponent ppmComponent;


    public List<PpmReportPO> selectPpmOutputReport(String[] model, String[] itemIn, String site, String[] resrceIn, String startDate,
                                                   String endDate, String byConditionType, String[] lineIn, String[] workCenterIn,String shift) throws ParseException, NotFoundActivityParamException, ActivityParamErrorException {
        startDate = Utils.parseReportDateDay(startDate);
        endDate = Utils.parseReportDateDay(endDate);
        // 1 SELECT ACTIVITY PARAM VALUE
        List<ActivityParamValuePO> list = ppmComponent.selectParamValue(site, PpmDetailActivityParam.VA01.getParamId());
        int i = 0;
        if (Utils.isEmpty(list)) {
           throw new NotFoundActivityParamException();
        } else {
            for (ActivityParamValuePO activityParamValuePO : list) {
                if (activityParamValuePO.getParamId().equals(PpmDetailActivityParam.VA01.getParamId())
                        || activityParamValuePO.getParamId().equals(PpmDetailActivityParam.VA02.getParamId())
                        || activityParamValuePO.getParamId().equals(PpmDetailActivityParam.VA03.getParamId())
                        || activityParamValuePO.getParamId().equals(PpmDetailActivityParam.VA04.getParamId())
                        || activityParamValuePO.getParamId().equals(PpmDetailActivityParam.VA05.getParamId())){
                    i++;
                }
            }
            if (i != 5 && list.size() >= 5) {
                throw new ActivityParamErrorException();
            }
        }
        return ppmComponent.selectPpmOutputReport(model, itemIn, site, resrceIn, startDate, endDate, byConditionType, lineIn, workCenterIn, list,shift);
    }

    public void commitExportExcelDev(List<PpmReportPO> vos, File file) throws IOException {
    	Collections.sort(vos,new Comparator<PpmReportPO>(){

			@Override
			public int compare(PpmReportPO o1, PpmReportPO o2) {
				int flag = 0;
				try {
					flag =  new SimpleDateFormat("yyyy-MM-dd").parse(o1.getByTime()).compareTo(new SimpleDateFormat("yyyy-MM-dd").parse(o2.getByTime()));
					if(flag == 0){
						flag = (-o1.getShift().charAt(0)+o1.getShift().charAt(1))-( -o2.getShift().charAt(0)+o2.getShift().charAt(1));
						if(flag == 0){
							flag = o1.getWorkAreaDes().compareTo(o2.getWorkAreaDes());
							if(flag == 0){
								flag = o1.getLineAreaDes().compareTo(o2.getLineAreaDes());
							}
						}
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return flag;
			}});
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFCellStyle styleOfDCL = workbook.createCellStyle();
        //styleOfDCL.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%"));
        styleOfDCL.setDataFormat(workbook.createDataFormat().getFormat("0.000%"));
        XSSFSheet sheet = workbook.createSheet(PpmDetailWorkbookCell.SHEET_NAME.getName());
        DecimalFormat df = new DecimalFormat("0.000");
        XSSFRow firstRow = sheet.createRow(0);
        firstRow.createCell(0).setCellValue(PpmDetailWorkbookCell.BY_TIME.getName());
        firstRow.createCell(1).setCellValue(PpmDetailWorkbookCell.SHIFT.getName());
        firstRow.createCell(2).setCellValue(PpmDetailWorkbookCell.SHIFT_DES.getName());
        firstRow.createCell(3).setCellValue(PpmDetailWorkbookCell.WORK_AREA_DES.getName());
        firstRow.createCell(4).setCellValue(PpmDetailWorkbookCell.LINE_AREA_DES.getName());
        firstRow.createCell(5).setCellValue(PpmDetailWorkbookCell.RESRCE.getName());
        firstRow.createCell(6).setCellValue(PpmDetailWorkbookCell.DESCRIPTION.getName());
        firstRow.createCell(7).setCellValue(PpmDetailWorkbookCell.ASSET.getName());
        firstRow.createCell(8).setCellValue(PpmDetailWorkbookCell.MODEL.getName());
        firstRow.createCell(9).setCellValue(PpmDetailWorkbookCell.ITEM.getName());
        firstRow.createCell(10).setCellValue(PpmDetailWorkbookCell.PPM_THEORY.getName());
        firstRow.createCell(11).setCellValue(PpmDetailWorkbookCell.PPM.getName());
        firstRow.createCell(12).setCellValue(PpmDetailWorkbookCell.FIRST_CAL.getName());

        for (int i = 0; i < vos.size(); i++) {
            XSSFRow addRow = sheet.createRow(i + 1);
            addRow.createCell(0).setCellValue(vos.get(i).getByTime());
            addRow.createCell(1).setCellValue(vos.get(i).getShift());
            addRow.createCell(2).setCellValue(vos.get(i).getShiftDes());
            addRow.createCell(3).setCellValue(vos.get(i).getWorkAreaDes()); 
            addRow.createCell(4).setCellValue(vos.get(i).getLineAreaDes());
            addRow.createCell(5).setCellValue(vos.get(i).getResrce());
            addRow.createCell(6).setCellValue(vos.get(i).getDescription());
            addRow.createCell(7).setCellValue(vos.get(i).getAsset());
            addRow.createCell(8).setCellValue(vos.get(i).getModel());
            addRow.createCell(9).setCellValue(vos.get(i).getItem());
            if(vos.get(i).getPpmTheory()!=null){
                addRow.createCell(10).setCellValue(Double.parseDouble(df.format(Double.valueOf(vos.get(i).getPpmTheory()))));
            }
            if(vos.get(i).getPpm()!=null){
                addRow.createCell(11).setCellValue(Double.parseDouble(df.format(Double.valueOf(vos.get(i).getPpm()))));
            }else
            	addRow.createCell(11).setCellValue("N/A");
            if(vos.get(i).getFirstCal()!=null){
            XSSFCell createCell = addRow.createCell(12);
            createCell.setCellStyle(styleOfDCL);
            createCell.setCellValue(Double.valueOf(vos.get(i).getFirstCal()));
            }
        }

        FileOutputStream outputStream = new FileOutputStream(file);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }

    public void commitExportExcelItem(List<PpmReportPO> vos, File file) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(PpmDetailWorkbookCell.SHEET_NAME.getName());
        XSSFRow firstRow = sheet.createRow(0);
        firstRow.createCell(0).setCellValue(PpmDetailWorkbookCell.MODEL.getName());
        firstRow.createCell(1).setCellValue(PpmDetailWorkbookCell.ITEM.getName());

        firstRow.createCell(3).setCellValue(PpmDetailWorkbookCell.WORK_AREA_DES.getName());

        firstRow.createCell(5).setCellValue(PpmDetailWorkbookCell.LINE_AREA_DES.getName());
        firstRow.createCell(6).setCellValue(PpmDetailWorkbookCell.RESRCE.getName());
        firstRow.createCell(7).setCellValue(PpmDetailWorkbookCell.DESCRIPTION.getName());
        firstRow.createCell(8).setCellValue(PpmDetailWorkbookCell.ASSET.getName());
        firstRow.createCell(9).setCellValue(PpmDetailWorkbookCell.BY_TIME.getName());
        firstRow.createCell(10).setCellValue(PpmDetailWorkbookCell.PPM_THEORY.getName());
        firstRow.createCell(11).setCellValue(PpmDetailWorkbookCell.PPM.getName());
        firstRow.createCell(12).setCellValue(PpmDetailWorkbookCell.FIRST_CAL.getName());

        for (int i = 0; i < vos.size(); i++) {
            XSSFRow addRow = sheet.createRow(i + 1);
            addRow.createCell(0).setCellValue(vos.get(i).getModel());
            addRow.createCell(1).setCellValue(vos.get(i).getItem());
            addRow.createCell(2).setCellValue(vos.get(i).getWorkArea());
            addRow.createCell(3).setCellValue(vos.get(i).getWorkAreaDes());
            addRow.createCell(4).setCellValue(vos.get(i).getLineArea());
            addRow.createCell(5).setCellValue(vos.get(i).getLineAreaDes());
            addRow.createCell(6).setCellValue(vos.get(i).getResrce());
            addRow.createCell(7).setCellValue(vos.get(i).getDescription());
            addRow.createCell(8).setCellValue(vos.get(i).getAsset());
            addRow.createCell(9).setCellValue(vos.get(i).getByTime());
            if(vos.get(i).getPpmTheory()!=null){
                addRow.createCell(10).setCellValue(Double.valueOf(vos.get(i).getPpmTheory()));
            }
            if(vos.get(i).getPpm()!=null){
                addRow.createCell(11).setCellValue(Double.valueOf(vos.get(i).getPpm()));
            }
            addRow.createCell(12).setCellValue(vos.get(i).getFirstCal());
        }

        FileOutputStream outputStream = new FileOutputStream(file);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }
}
