package mhp.oee.reporting.ppmdetail;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.extend.PpmReportPO;
import mhp.oee.reporting.common.SearchHelpReportService;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ResourceVO;
import mhp.oee.web.Constants;

/**
 * Created by SuMingzhi on 2016/11/24.
 */
@RestController
public class PpmDetailReportController {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    
    @Autowired
    PpmDetailReportService ppmReportService;
    @Autowired
    SearchHelpReportService searchHelpReportService;   
    @RequestMapping(value = "/web/rest/report/ppm/site/{site}/export_excel_all", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> exportExcel(@PathVariable("site") String site,
            @RequestBody PPMDetailReportVO vo,
            HttpServletResponse response) {

        String[] model = vo.getModel();
        String[] itemIn = vo.getItemIn();
        String[] resrceIn = vo.getResource();
        String startDate = vo.getStartDate();
        String endDate = vo.getEndDate();
        String byConditionType = vo.getByConditionType();
        String[] lineIn = vo.getLineArea();
        String[] workCenterIn = vo.getWorkArea();
        String shift = vo.getShift();
        List<String> resrceList= new ArrayList<String>();
        if(resrceIn==null||resrceIn.length<1){
        	String resource = null;
        	List<ResourceVO> vos = searchHelpReportService.readResourceHelp(site, resource,resource, lineIn, workCenterIn);
        	for(ResourceVO rvo:vos){
        		resrceList.add(rvo.getResrce());
        	}
        }else{
        	for(String resrceOne:resrceIn){
        		resrceList.add(resrceOne);
        	}
        }

        String trace = String.format(
                "[PpmReportController.readPpmOutputReport] request : site=%s, model=%s, item=%s, resrceIn=%s, startDate=%s"
                        + ", endDate=%s, byConditionType=%s, lineIn=%s, workCenterIn=%s",
                site, model, gson.toJson(itemIn), gson.toJson(resrceIn), startDate, endDate, byConditionType,
                gson.toJson(lineIn), gson.toJson(workCenterIn));
        logger.debug(">> " + trace);
        try {
            File file = new File("download.xlsx");
            List<PpmReportPO> vos = new ArrayList<PpmReportPO>();
            //控制每次查询的设备不超过50条
            List<String> resrceListSlice= new ArrayList<String>();
            for(int i=0;i<resrceList.size()/50+1;i++){
            	for(int j=0+i*50;j<50*(i+1);j++){
            		if(j>=resrceList.size())break;
            		resrceListSlice.add(resrceList.get(j));
            	}
            	resrceIn = new String[resrceListSlice.size()];
            	resrceListSlice.toArray(resrceIn);
            	resrceListSlice.clear();
            	vos.addAll(ppmReportService.selectPpmOutputReport(model, itemIn, site, resrceIn, startDate,
            			endDate, byConditionType, lineIn,workCenterIn,shift));
            }
            ppmReportService.commitExportExcelDev(vos, file);
            Utils.generateDownloadFile(response, file);
            FileUtils.deleteQuietly(file);
        } catch (IOException e) {
            ExceptionVO exceptionVO = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
            logger.error("exportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        } catch (ParseException e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } catch (BusinessException e) {
            ExceptionVO exceptionVO = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("PpmReportController.readPpmOutputReport() : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(exceptionVO)).body(null);
        }
        logger.debug("<< [PpmReportController.exportExcel] response : void");
        return ResponseEntity.ok(null);
    }
    
    
    @RequestMapping(value = "/web/rest/report/ppm/site/{site}/export_excel_all_num", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public int exportExcelNum(@PathVariable("site") String site,
            @RequestBody PPMDetailReportVO vo,
            HttpServletResponse response) {
    	String[] model = vo.getModel();
        String[] itemIn = vo.getItemIn();
        String[] resrceIn = vo.getResource();
        String startDate = vo.getStartDate();
        String endDate = vo.getEndDate();
        String byConditionType = vo.getByConditionType();
        String[] lineIn = vo.getLineArea();
        String[] workCenterIn = vo.getWorkArea();
        String shift = vo.getShift();
        List<String> resrceList= new ArrayList<String>();
        if(resrceIn==null||resrceIn.length<1){
        	String resource = null;
        	List<ResourceVO> vos = searchHelpReportService.readResourceHelp(site, resource,resource, lineIn, workCenterIn);
        	for(ResourceVO rvo:vos){
        		resrceList.add(rvo.getResrce());
        	}
        }else{
        	for(String resrceOne:resrceIn){
        		resrceList.add(resrceOne);
        	}
        }

            List<PpmReportPO> vos = new ArrayList<PpmReportPO>();
            List<String> resrceListSlice= new ArrayList<String>();
            for(int i=0;i<resrceList.size()/50+1;i++){
            	for(int j=0+i*50;j<50*(i+1);j++){
            		if(j>=resrceList.size())break;
            		resrceListSlice.add(resrceList.get(j));
            	}
            	resrceIn = new String[resrceListSlice.size()];
            	resrceListSlice.toArray(resrceIn);
            	resrceListSlice.clear();
            	try {
					vos.addAll(ppmReportService.selectPpmOutputReport(model, itemIn, site, resrceIn, startDate,
							endDate, byConditionType, lineIn,workCenterIn,shift));
				} catch (Exception e){
					e.printStackTrace();
				}
            }
		
    	return vos.size();
        }
}
