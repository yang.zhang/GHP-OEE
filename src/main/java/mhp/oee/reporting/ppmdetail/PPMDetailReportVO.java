package mhp.oee.reporting.ppmdetail;

public class PPMDetailReportVO {

    private String[] model;
    private String[] itemIn;
    private String[] resource;
    private String startDate;
    private String endDate;
    private String byConditionType;
    private String[] lineArea;
    private String[] workArea;
    private String shift;
	public String[] getModel() {
		return model;
	}
	public void setModel(String[] model) {
		this.model = model;
	}
	public String[] getItemIn() {
		return itemIn;
	}
	public void setItemIn(String[] itemIn) {
		this.itemIn = itemIn;
	}
	public String[] getResource() {
		return resource;
	}
	public void setResource(String[] resource) {
		this.resource = resource;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getByConditionType() {
		return byConditionType;
	}
	public void setByConditionType(String byConditionType) {
		this.byConditionType = byConditionType;
	}
	public String[] getLineArea() {
		return lineArea;
	}
	public void setLineArea(String[] lineArea) {
		this.lineArea = lineArea;
	}
	public String[] getWorkArea() {
		return workArea;
	}
	public void setWorkArea(String[] workArea) {
		this.workArea = workArea;
	}
	public String getShift() {
		return shift;
	}
	public void setShift(String shift) {
		this.shift = shift;
	}
}