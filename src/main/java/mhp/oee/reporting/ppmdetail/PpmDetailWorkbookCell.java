package mhp.oee.reporting.ppmdetail;

/**
 * Created by SuMingzhi on 2016/11/23.
 */
public enum PpmDetailWorkbookCell {
	SHEET_NAME("PPM报表"),
	BY_TIME("日期"),
    SHIFT("班次代码"),
    SHIFT_DES("班次描述"),
   /* WORK_AREA("生产区域"),*/
    WORK_AREA_DES("生产区域描述"),
    /*LINE_AREA("拉线"),*/
    LINE_AREA_DES("拉线描述"),
    RESRCE("设备编码"),
    DESCRIPTION("设备描述"),
    ASSET("资产号"),
    MODEL("Model"),
    ITEM("物料编码"),
    PPM_THEORY("理论PPM"),
    PPM("实际PPM"),
    FIRST_CAL("达成率");

    private String name;

    private PpmDetailWorkbookCell(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
