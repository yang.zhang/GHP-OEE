package mhp.oee.enumClass;

/** 看板查询维度
 * Created by DingLJ on 2017/4/26 026.
 */
public enum ResourceRealTimeDimension {


    SITE(1,"SITE"),
    WORK_AREA(2,"WORK_AREA"),
    LINE_AREA(3,"LINE_AREA"),
    RESOURCE_TYPE(4,"RESOURCE_TYPE");

    private int value;
    private String filterColumnName;

    private ResourceRealTimeDimension(int value, String filterColumnName){
        this.value = value;
        this.filterColumnName = filterColumnName;
    }

    public int getValue() {
        return value;
    }

    public String getFilterColumnName() {
        return filterColumnName;
    }

    public static ResourceRealTimeDimension fromValue(Integer v){

        if(v != null){
            for (ResourceRealTimeDimension d : values()){
                if(d.value == v.intValue()){
                    return d;
                }
            }
        }

        return null;

    }




}
