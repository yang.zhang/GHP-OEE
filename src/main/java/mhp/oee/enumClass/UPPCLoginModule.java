package mhp.oee.enumClass;

/** 上位机登录模块
 * Created by DingLJ on 2017/5/1 001.
 */
public enum UPPCLoginModule {

    ME(1,"ME_","ME登录模块"),
    PRD(2,"PRD_","PRD登录模块");


    private int key;
    private String value;
    private String desc;


    private UPPCLoginModule(int key, String value, String desc){
        this.key = key;
        this.value = value;
        this.desc = desc;

    }

    public String getValue() {
        return value;
    }

    public static UPPCLoginModule fromValue(Integer key){

        if(key != null){
            for (UPPCLoginModule module : values()){
                if(module.key == key.intValue()){
                    return module;
                }
            }
        }
        return null;

    }


}
