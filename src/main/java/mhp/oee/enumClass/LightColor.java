package mhp.oee.enumClass;

/**
 * Created by DingLJ on 2017/5/1 001.
 */
public enum LightColor {

    RED(1,"R"),
    YELLOW(2,"Y"),
    GREEN(3,"G");


    private int value;
    private String color;

    private LightColor(int value, String color){
        this.value = value;
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public String getColor() {
        return color;
    }

    public static LightColor fromValue(Integer v){

        if(v != null){

            for (LightColor color : values()){
                if(color.value == v.intValue()){
                    return color;
                }
            }

        }
        return null;

    }


}
