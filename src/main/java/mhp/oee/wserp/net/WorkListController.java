package mhp.oee.wserp.net;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceVO;
import mhp.oee.web.angular.resource.plc.management.PlcInfoService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.atlbattery.ECC.PM_notification_simplex.DT_NOTIFICATION_REQ;
import com.atlbattery.ECC.PM_notification_simplex.DT_NOTIFICATION_REQET_EQUNR;
import com.atlbattery.ECC.PM_notification_simplex.DT_NOTIFICATION_RESET_ITEMS;
import com.atlbattery.ECC.PM_notification_simplex.SI_ANDON_NOTIFICATION_SENDBindingStub;
import com.atlbattery.ECC.PM_notification_simplex.SI_ANDON_NOTIFICATION_SENDService;
import com.atlbattery.ECC.PM_notification_simplex.SI_ANDON_NOTIFICATION_SENDServiceLocator;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Attribute;
import mhp.oee.web.Constants;
import mhp.oee.wserp.net.po.TZDGDCXRequest;
import mhp.oee.wserp.net.po.TZDGDCXRequestBo;

/**
 * Created by SuMingzhi on 2017/2/20.
 * *通知单工单查询接口
 */
@RestController
public class WorkListController {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    PlcInfoService plcInfoService;

    @RequestMapping(value = "/web/rest/ws/erp/site/{site}/worklistselect", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> workListSelect(@PathVariable("site") String site, @RequestBody TZDGDCXRequest itemRequest) {
        String username = Attribute.getValue("webservice-address.properties", "username");
        String password = Attribute.getValue("webservice-address.properties", "password");
        String client = Attribute.getValue("webservice-address.properties", "catl-qty");

        SI_ANDON_NOTIFICATION_SENDService service = new SI_ANDON_NOTIFICATION_SENDServiceLocator();
        DT_NOTIFICATION_REQ request = new DT_NOTIFICATION_REQ();
        request.setZSTATE(itemRequest.getZSTATE());
        request.setZCLIENT(client);

        String workCenter = "";
        String resourceType = "";
        String line = "";
        List<ResourceVO> vos;
        List<DT_NOTIFICATION_REQET_EQUNR> etequnrsList = new ArrayList<DT_NOTIFICATION_REQET_EQUNR>();
        if (!Utils.isEmpty(itemRequest.getItems())) {
            for (TZDGDCXRequestBo rb : itemRequest.getItems()) {
                DT_NOTIFICATION_REQET_EQUNR etequnr = new DT_NOTIFICATION_REQET_EQUNR();
                etequnr.setEQUNR(rb.getEQUNR());
                etequnrsList.add(etequnr);
            }

        } else {
            if (!Utils.isEmpty(itemRequest.getWork_center())) {
                for (String stu : itemRequest.getWork_center()) {
                    workCenter = workCenter + stu + ",";
                }
            }

            if (!Utils.isEmpty(itemRequest.getDeviceType())) {
                for (String stu : itemRequest.getDeviceType()) {
                    resourceType = resourceType + stu + ",";
                }
            }
            final String resourceTypeFinal = resourceType;
            if (!Utils.isEmpty(itemRequest.getLine())) {
                for (String stu : itemRequest.getLine()) {
                    line = line + stu + ",";
                }
            }
            final String sites = site;
            vos = plcInfoService.readResourceOptionalWithLine(sites, resourceTypeFinal, line, workCenter);
            for (ResourceVO rv : vos) {
                DT_NOTIFICATION_REQET_EQUNR etequnr = new DT_NOTIFICATION_REQET_EQUNR();
                etequnr.setEQUNR(rv.getResrce());
                etequnrsList.add(etequnr);
            }
        }

        SI_ANDON_NOTIFICATION_SENDBindingStub port = null;
        try {
            port = (SI_ANDON_NOTIFICATION_SENDBindingStub) service.getHTTP_Port();
        } catch (ServiceException e1) {
            e1.printStackTrace();
        }
        port.setUsername(username);
        port.setPassword(password);
        DT_NOTIFICATION_REQET_EQUNR[] etequnrs = new DT_NOTIFICATION_REQET_EQUNR[etequnrsList.size()];
        request.setET_EQUNR(etequnrsList.toArray(etequnrs));

        DT_NOTIFICATION_RESET_ITEMS[] res = null;
        try {
            res = port.SI_ANDON_NOTIFICATION_SEND(request);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(res);
    }
}
