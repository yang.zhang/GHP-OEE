package mhp.oee.wserp.net;

import java.util.ArrayList;
import java.util.List;

import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourcePOExample;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.atlbatter.ANDON.IW31.DT_IW31_REQIT_COMPONENTS;
import com.atlbatter.ANDON.IW31.DT_IW31_REQIT_OPERATIONS;
import com.atlbatter.ANDON.IW31.DT_IW31_REQI_HEAD;

import mhp.oee.wserp.net.po.ExcelPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
@Service
public class WorkListCreateService {


	@Autowired
	private ResourceComponent resourceComponent;

	public boolean checkExcel(XSSFWorkbook workbook) {
		XSSFSheet sheet = workbook.getSheetAt(0);
		List<String> stringCellValues = new ArrayList<String>();
		for (int rowNumber = 4; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
			Row row = sheet.getRow(rowNumber);
			Cell cellB = row.getCell(1);
			Cell cellC = row.getCell(2);
			Cell cellD = row.getCell(3);
			Cell cellE = row.getCell(4);
			Cell cellF = row.getCell(5);
			Cell cellG = row.getCell(6);
			Cell cellH = row.getCell(7);
			Cell cellI = row.getCell(8);
			Cell cellJ = row.getCell(9);
			Cell cellK = row.getCell(10);
			Cell cellL = row.getCell(11);
			if (!cellC.getStringCellValue().equals("ZP03")&!cellC.getStringCellValue().equals("ZP02")){
				cellL.setCellValue("工单类型为ZP02 或ZP03");
				return false;
			}
			if (cellD.getStringCellValue()==null||cellD.getStringCellValue().equals("")){
				cellL.setCellValue("工单描述不为空");
				return false;
			}
			if (StringUtils.isBlank(cellE.getStringCellValue())){
				cellL.setCellValue("设备号为空");
				return false;
			}

			//校验设备
			ResourcePOExample example = new ResourcePOExample();
			ResourcePOExample.Criteria criteria = example.createCriteria();
			criteria.andResrceEqualTo(cellE.getStringCellValue());
			List<ResourcePO> resourcePOList = this.resourceComponent.readResourcesByExample(example);
			if(CollectionUtils.isEmpty(resourcePOList)){
				cellL.setCellValue("设备号在系统中不存在");
				return false;
			}

			if (!cellF.getStringCellValue().matches("\\d{4}[0-1]\\d[0-3]\\d")){
				cellL.setCellValue("计划开始时间格式错误");
				return false;
			}
			if (!cellG.getStringCellValue().matches("\\d+")){
				cellL.setCellValue("工作时间为数字");
				return false;
			}
			if (!cellH.getStringCellValue().equalsIgnoreCase("MIN")){
				cellL.setCellValue("时间单位为MIN");
				return false;
			}
			if (!cellI.getStringCellValue().matches("\\d+")){
				cellL.setCellValue("人数为数字");
				return false;
			}
			if (StringUtils.isBlank(cellJ.getStringCellValue())){
				cellL.setCellValue("配件标识码为空");
				return false;
			}
			if (!cellK.getStringCellValue().matches("\\d+")){
				cellL.setCellValue("数量为数字");
				return false;
			}
			String stringCellValue = cellB.getStringCellValue()+cellC.getStringCellValue()+cellD.getStringCellValue()+cellE.getStringCellValue()
										+cellF.getStringCellValue()+cellH.getStringCellValue()+cellI.getStringCellValue()+cellJ.getStringCellValue();
			if(stringCellValues.contains(stringCellValue)){
				cellL.setCellValue("上传数据含有重复行");
				return false;
			}else{
				stringCellValues.add(stringCellValue);
			}
		}
		return true;
	}

	public List<ExcelPO> readExcel(XSSFWorkbook workbook) {
		XSSFSheet sheet = workbook.getSheetAt(0);
		List<ExcelPO> excelList = new ArrayList<ExcelPO>();
		List<String> AUFNRS = new ArrayList<String>();
		for (int rowNumber = 4; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
			Row row = sheet.getRow(rowNumber);
			Cell cell = row.getCell(1);
			String AUFNR = cell.getStringCellValue();
			if (AUFNR == null || "".equals(AUFNR))
				break;
			if (!AUFNRS.contains(AUFNR))
				AUFNRS.add(AUFNR);
		}
		for (String AUFNR : AUFNRS) {
			ExcelPO excelPO = new ExcelPO();
			DT_IW31_REQI_HEAD ihead = new DT_IW31_REQI_HEAD();
			List<DT_IW31_REQIT_OPERATIONS> itoperations = new ArrayList<DT_IW31_REQIT_OPERATIONS>();
			List<DT_IW31_REQIT_COMPONENTS> itcomponents = new ArrayList<DT_IW31_REQIT_COMPONENTS>();
			DT_IW31_REQIT_OPERATIONS itoperation = new DT_IW31_REQIT_OPERATIONS();
			List<String> checkDescription = new ArrayList<String>();
			ihead.setAUFNR(AUFNR);
			for (int rowNumber = 4; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
				Row row = sheet.getRow(rowNumber);
				Cell cell = row.getCell(1);
				if (!AUFNR.equals(cell.getStringCellValue()))
					continue;
				checkDescription.add(row.getCell(11).getStringCellValue());
				if (ihead.getPM_AUFART() == null || "".equals(ihead.getPM_AUFART())) {
					ihead.setPM_AUFART(row.getCell(2).getStringCellValue());
					ihead.setKTEXT(row.getCell(3).getStringCellValue());
					ihead.setEQUNR(row.getCell(4).getStringCellValue());
					ihead.setGSTRP(row.getCell(5).getStringCellValue());
//					ihead.setGSTRP(String.valueOf(row.getCell(5).getNumericCellValue()));

					itoperation.setARBEIT(row.getCell(6).getStringCellValue());

					String arbeite = row.getCell(7).getStringCellValue();
					arbeite = arbeite != null ? arbeite.toUpperCase() :arbeite;
					itoperation.setARBEITE(arbeite);
					itoperation.setANZZL(row.getCell(8).getStringCellValue());
				}
				DT_IW31_REQIT_COMPONENTS itcomponent = new DT_IW31_REQIT_COMPONENTS();
				itcomponent.setMATNR(row.getCell(9).getStringCellValue());
				itcomponent.setMENGE(row.getCell(10).getStringCellValue());
				itcomponents.add(itcomponent);
			}
			itoperations.add(itoperation);
			excelPO.setIhead(ihead);
			DT_IW31_REQIT_OPERATIONS[] itoperationsArray = new DT_IW31_REQIT_OPERATIONS[itoperations.size()];
			excelPO.setItoperations(itoperations.toArray(itoperationsArray));
			DT_IW31_REQIT_COMPONENTS[] itcomponentsArray = new DT_IW31_REQIT_COMPONENTS[itcomponents.size()];
			excelPO.setItcomponents(itcomponents.toArray(itcomponentsArray));
			excelPO.setCheckDescription(checkDescription);
			excelList.add(excelPO);
		}
		return excelList;
	}
}
