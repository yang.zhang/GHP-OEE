package mhp.oee.wserp.net;


import java.net.Authenticator;
import java.util.HashMap;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import org.slf4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sun.xml.internal.ws.client.BindingProviderProperties;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Attribute;
import mhp.oee.web.Constants;
import com.atlbattery.ANDON.CHANGE_ORDER_STATUS.DT_CHANGE_ORDER_STATUS_REQ;
import com.atlbattery.ANDON.CHANGE_ORDER_STATUS.DT_CHANGE_ORDER_STATUS_RES;
import com.atlbattery.ANDON.CHANGE_ORDER_STATUS.SI_CHANGE_ORDER_STATUS_SENDBindingStub;
import com.atlbattery.ANDON.CHANGE_ORDER_STATUS.SI_CHANGE_ORDER_STATUS_SENDService;
import com.atlbattery.ANDON.CHANGE_ORDER_STATUS.SI_CHANGE_ORDER_STATUS_SENDServiceLocator;


/**
 * Created by SuMingzhi on 2017/2/20.
 * *变更工单状态接口
 */
@RestController
public class ChangeListStatusController {

    @InjectableLogger
    private static Logger logger;

    @RequestMapping(value = "/web/rest/ws/erp/site/{site}/changestatus", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeListStatus(
			    		 @RequestParam(value = "IAUFNR", required = true) String IAUFNR,
			    		 @RequestParam(value = "IANDONUSER", required = true) String IANDONUSER,
                         @RequestParam(value = "ISTATUS", required = true) String ISTATUS) {
    	String username = Attribute.getValue("webservice-address.properties","username");
		String password = Attribute.getValue("webservice-address.properties","password");
		String client = Attribute.getValue("webservice-address.properties","catl-qty");
		
		SI_CHANGE_ORDER_STATUS_SENDService service = new SI_CHANGE_ORDER_STATUS_SENDServiceLocator();
		DT_CHANGE_ORDER_STATUS_REQ request = new DT_CHANGE_ORDER_STATUS_REQ();
		request.setI_CLIENT(client);
		request.setI_AUFNR(IAUFNR);
		request.setI_ANDON_USER(IANDONUSER);
		request.setI_STATUS(ISTATUS);

		SI_CHANGE_ORDER_STATUS_SENDBindingStub port = null;
		try {
			port = (SI_CHANGE_ORDER_STATUS_SENDBindingStub) service.getHTTP_Port();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		port.setUsername(username);
		port.setPassword(password);
		
		DT_CHANGE_ORDER_STATUS_RES response = null;
		try {
			response = port.SI_CHANGE_ORDER_STATUS_SEND(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String emsg = response.getE_MSG();
		String esubrc = response.getE_SUBRC();
		Map<String,String> returnMap = new HashMap<String, String>();
		returnMap.put("emsg", emsg);
		returnMap.put("esubrc", esubrc);
		return ResponseEntity.ok(returnMap);
    }
}
