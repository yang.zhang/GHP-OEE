package mhp.oee.wserp.net;


import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import org.slf4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.atlbatter.ANDON.get_pmurl.DT_GETPMURL_REQ;
import com.atlbatter.ANDON.get_pmurl.DT_GETPMURL_REQIT_PARAMETERS;
import com.atlbatter.ANDON.get_pmurl.DT_GETPMURL_RES;
import com.atlbatter.ANDON.get_pmurl.SI_GETPMURLOUTBindingStub;
import com.atlbatter.ANDON.get_pmurl.SI_GETPMURLOUTService;
import com.atlbatter.ANDON.get_pmurl.SI_GETPMURLOUTServiceLocator;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Attribute;
import mhp.oee.web.Constants;

/**
 * Created by SuMingzhi on 2017/2/20.
 * *工单编辑接口
 */
@RestController
public class EditWorkListController {

    @InjectableLogger
    private static Logger logger;

    @RequestMapping(value = "/web/rest/ws/erp/site/{site}/editworklist", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Map<String, String>> editWorkList(
			    		 @RequestParam(value = "IUSER", required = true) String IUSER,
			    		 @RequestParam(value = "MODE", required = true) String MODE,
                         @RequestParam(value = "AUFNR", required = true) String AUFNR) {
    	String username = Attribute.getValue("webservice-address.properties","username");
		String password = Attribute.getValue("webservice-address.properties","password");
		String client = Attribute.getValue("webservice-address.properties","catl-qty");
		
		SI_GETPMURLOUTService service = new SI_GETPMURLOUTServiceLocator();
		DT_GETPMURL_REQ request = new DT_GETPMURL_REQ();
		request.setI_CLIENT(client);
		request.setI_ACTION(Attribute.getValue("webservice-address.properties","i-action"));
		request.setI_USER(IUSER);
		request.setI_COMMIT(Attribute.getValue("webservice-address.properties","i-commit"));
		DT_GETPMURL_REQIT_PARAMETERS itparameter = new DT_GETPMURL_REQIT_PARAMETERS();
		itparameter.setNAME("MODE");
		itparameter.setVALUE(MODE);
		DT_GETPMURL_REQIT_PARAMETERS itparameter2 = new DT_GETPMURL_REQIT_PARAMETERS();
		itparameter2.setNAME("AUFNR");
		itparameter2.setVALUE(AUFNR);
		DT_GETPMURL_REQIT_PARAMETERS[] itparameters = {itparameter,itparameter2};
		request.setIT_PARAMETERS(itparameters);
		SI_GETPMURLOUTBindingStub port = null;
		try {
			port = (SI_GETPMURLOUTBindingStub) service.getHTTP_Port();
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}
		port.setUsername(username);
		port.setPassword(password);
		DT_GETPMURL_RES response = null;
		try {
			response = port.SI_GETPMURLOUT(request);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		String eurl = response.getE_URL();
		Map<String, String> retMap=new HashMap<String, String>();
		retMap.put("url", eurl);
		return ResponseEntity.ok(retMap);
    }
}
