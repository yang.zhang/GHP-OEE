package mhp.oee.wserp.net;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.rpc.ServiceException;
import org.slf4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.atlbatter.ANDON.ord_2_order.DT_ORD2ORDER_REQ;
import com.atlbatter.ANDON.ord_2_order.DT_ORD2ORDER_REQIT_ORDER;
import com.atlbatter.ANDON.ord_2_order.DT_ORD2ORDER_RES;
import com.atlbatter.ANDON.ord_2_order.SI_ORD_2_ORDER_OUTBindingStub;
import com.atlbatter.ANDON.ord_2_order.SI_ORD_2_ORDER_OUTService;
import com.atlbatter.ANDON.ord_2_order.SI_ORD_2_ORDER_OUTServiceLocator;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Attribute;
import mhp.oee.web.Constants;
import mhp.oee.wserp.net.po.ItemRequest;
import mhp.oee.wserp.net.po.RequestBo;

/**
 * Created by SuMingzhi on 2017/2/20.
 * *ME接单接口
 */
@RestController
public class MECreateListController {

    @InjectableLogger
    private static Logger logger;

    @RequestMapping(value = "/web/rest/ws/erp/site/{site}/mecreatelist", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> createList(@RequestBody ItemRequest itemRequest) {
    	String username = Attribute.getValue("webservice-address.properties","username");
		String password = Attribute.getValue("webservice-address.properties","password");
		String client = Attribute.getValue("webservice-address.properties","catl-qty");
		
		SI_ORD_2_ORDER_OUTService service = new SI_ORD_2_ORDER_OUTServiceLocator();
		DT_ORD2ORDER_REQ request = new DT_ORD2ORDER_REQ();
		request.setI_CLIENT(client);
		request.setI_UTERM(Attribute.getValue("webservice-address.properties","tag-end-name"));
		request.setI_ANDON_USER(itemRequest.getIANDONUSER());
		request.setI_ZSPOT(itemRequest.getIZSPOT());
		request.setI_RELEASE(itemRequest.getIRELEASE());
		List<DT_ORD2ORDER_REQIT_ORDER> itordersList = new ArrayList<DT_ORD2ORDER_REQIT_ORDER>();
		for(RequestBo rb : itemRequest.getItems()){
			DT_ORD2ORDER_REQIT_ORDER itorder = new DT_ORD2ORDER_REQIT_ORDER();
			itorder.setAUFNR(rb.getAUFNR());
			itordersList.add(itorder);
		}
		DT_ORD2ORDER_REQIT_ORDER[] itorders = new DT_ORD2ORDER_REQIT_ORDER[itordersList.size()];
		request.setIT_ORDER(itordersList.toArray(itorders));

		SI_ORD_2_ORDER_OUTBindingStub port = null;
		try {
			port = (SI_ORD_2_ORDER_OUTBindingStub) service.getHTTP_Port();
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}
		port.setUsername(username);
		port.setPassword(password);
		DT_ORD2ORDER_RES response = null;
		try {
			response = port.SI_ORD_2_ORDER_OUT(request);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
    	return ResponseEntity.ok(response);
    }
}
