package mhp.oee.wserp.net.po;

import java.util.List;

public class ItemRequest {
	private String IANDONUSER;
	private String IZSPOT;
	private List<RequestBo> items;
	private String IRELEASE;
	public String getIANDONUSER() {
		return IANDONUSER;
	}
	public void setIANDONUSER(String iANDONUSER) {
		IANDONUSER = iANDONUSER;
	}
	public String getIZSPOT() {
		return IZSPOT;
	}
	public void setIZSPOT(String iZSPOT) {
		IZSPOT = iZSPOT;
	}
	public List<RequestBo> getItems() {
		return items;
	}
	public void setItems(List<RequestBo> items) {
		this.items = items;
	}
    public String getIRELEASE() {
        return IRELEASE;
    }
    public void setIRELEASE(String iRELEASE) {
        IRELEASE = iRELEASE;
    }
	
}
