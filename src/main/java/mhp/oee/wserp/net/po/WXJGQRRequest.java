package mhp.oee.wserp.net.po;

import java.util.List;

public class WXJGQRRequest {
	 private List<WXJGQRRequestBo> items;
  	 private String IANDONUSR;
	 private String IRESULT;
	 public List<WXJGQRRequestBo> getItems() {
		 return items;
	 }
	 public void setItems(List<WXJGQRRequestBo> items) {
		 this.items = items;
	 }
	 public String getIANDONUSR() {
		 return IANDONUSR;
	 }
	 public void setIANDONUSR(String iANDONUSR) {
		 IANDONUSR = iANDONUSR;
	 }
	 public String getIRESULT() {
		 return IRESULT;
	 }
	 public void setIRESULT(String iRESULT) {
		 IRESULT = iRESULT;
	 }

}
