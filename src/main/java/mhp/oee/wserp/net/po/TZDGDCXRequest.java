package mhp.oee.wserp.net.po;

import java.util.List;

public class TZDGDCXRequest {
	private List<TZDGDCXRequestBo> items;
	private String ZSTATE;
	private List<String> line;
	private List<String> deviceType;
	private List<String> work_center;
	private String site;

	public List<TZDGDCXRequestBo> getItems() {
		return items;
	}

	public void setItems(List<TZDGDCXRequestBo> items) {
		this.items = items;
	}

	public List<String> getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(List<String> deviceType) {
		this.deviceType = deviceType;
	}

	public List<String> getWork_center() {
		return work_center;
	}

	public void setWork_center(List<String> work_center) {
		this.work_center = work_center;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public List<String> getLine() {
		return line;
	}

	public void setLine(List<String> line) {
		this.line = line;
	}


	public String getZSTATE() {
		return ZSTATE;
	}

	public void setZSTATE(String zSTATE) {
		ZSTATE = zSTATE;
	}

}
