package mhp.oee.wserp.net.po;

import java.util.List;

import com.atlbatter.ANDON.IW31.DT_IW31_REQIT_COMPONENTS;
import com.atlbatter.ANDON.IW31.DT_IW31_REQIT_OPERATIONS;
import com.atlbatter.ANDON.IW31.DT_IW31_REQI_HEAD;

/**
 * *改造改善工单excel实体
 * @author SuMingzhi
 * 
 */


public class ExcelPO {
    private String iclient;
    private DT_IW31_REQI_HEAD ihead;
    private String imode;
    private String irelease;
    private DT_IW31_REQIT_OPERATIONS[] itoperations;
    private DT_IW31_REQIT_COMPONENTS[] itcomponents;
    private List<String> checkDescription;

    public String getIclient() {
		return iclient;
	}
	public List<String> getCheckDescription() {
		return checkDescription;
	}
	public void setCheckDescription(List<String> checkDescription) {
		this.checkDescription = checkDescription;
	}
	public void setIclient(String iclient) {
		this.iclient = iclient;
	}
	public DT_IW31_REQI_HEAD getIhead() {
		return ihead;
	}
	public void setIhead(DT_IW31_REQI_HEAD ihead) {
		this.ihead = ihead;
	}
	public String getImode() {
		return imode;
	}
	public void setImode(String imode) {
		this.imode = imode;
	}
	public String getIrelease() {
		return irelease;
	}
	public void setIrelease(String irelease) {
		this.irelease = irelease;
	}
	public DT_IW31_REQIT_OPERATIONS[] getItoperations() {
		return itoperations;
	}
	public void setItoperations(DT_IW31_REQIT_OPERATIONS[] itoperations) {
		this.itoperations = itoperations;
	}
	public DT_IW31_REQIT_COMPONENTS[] getItcomponents() {
		return itcomponents;
	}
	public void setItcomponents(DT_IW31_REQIT_COMPONENTS[] itcomponents) {
		this.itcomponents = itcomponents;
	}

}
