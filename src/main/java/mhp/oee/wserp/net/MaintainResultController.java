package mhp.oee.wserp.net;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.slf4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.atlbatter.ANDON.CONFIRM_ORDER.DT_CONFIRM_ORDER_REQ;
import com.atlbatter.ANDON.CONFIRM_ORDER.DT_CONFIRM_ORDER_REQIT_ITEMS;
import com.atlbatter.ANDON.CONFIRM_ORDER.DT_CONFIRM_ORDER_RESET_ITEMS;
import com.atlbatter.ANDON.CONFIRM_ORDER.SI_CONFIRM_ORDER_OUTBindingStub;
import com.atlbatter.ANDON.CONFIRM_ORDER.SI_CONFIRM_ORDER_OUTService;
import com.atlbatter.ANDON.CONFIRM_ORDER.SI_CONFIRM_ORDER_OUTServiceLocator;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Attribute;
import mhp.oee.web.Constants;
import mhp.oee.wserp.net.po.WXJGQRRequest;
import mhp.oee.wserp.net.po.WXJGQRRequestBo;

/**
 * Created by SuMingzhi on 2017/2/20.
 * *维修结果确认接口
 */
@RestController
public class MaintainResultController {

    @InjectableLogger
    private static Logger logger;

    @RequestMapping(value = "/web/rest/ws/erp/site/{site}/maintain", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> maintainResult(@RequestBody WXJGQRRequest itemRequest) {
    	String username = Attribute.getValue("webservice-address.properties","username");
		String password = Attribute.getValue("webservice-address.properties","password");
		String client = Attribute.getValue("webservice-address.properties","catl-qty");
		
		SI_CONFIRM_ORDER_OUTService service = new SI_CONFIRM_ORDER_OUTServiceLocator();
		DT_CONFIRM_ORDER_REQ request = new DT_CONFIRM_ORDER_REQ();
		request.setI_CLIENT(client);
		request.setI_ANDON_USR(itemRequest.getIANDONUSR());
		request.setI_RESULT(itemRequest.getIRESULT());
		List<DT_CONFIRM_ORDER_REQIT_ITEMS> ititemsList = new ArrayList<DT_CONFIRM_ORDER_REQIT_ITEMS>();
		for(WXJGQRRequestBo rb : itemRequest.getItems()){
			DT_CONFIRM_ORDER_REQIT_ITEMS ititem = new DT_CONFIRM_ORDER_REQIT_ITEMS();
			ititem.setAUFNR(rb.getAUFNR());
			ititemsList.add(ititem);
		}
		DT_CONFIRM_ORDER_REQIT_ITEMS[] ititems = new DT_CONFIRM_ORDER_REQIT_ITEMS[ititemsList.size()];
		request.setIT_ITEMS(ititemsList.toArray(ititems));
		SI_CONFIRM_ORDER_OUTBindingStub port = null;
		try {
			port = (SI_CONFIRM_ORDER_OUTBindingStub) service.getHTTP_Port();
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}
		port.setUsername(username);
		port.setPassword(password);
		DT_CONFIRM_ORDER_RESET_ITEMS[] response = null;
		try {
			response = port.SI_CONFIRM_ORDER_OUT(request);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
    	return ResponseEntity.ok(response);
    }
}
