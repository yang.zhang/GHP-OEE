package mhp.oee.wserp.net;


import java.net.Authenticator;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.rpc.ServiceException;
import javax.xml.ws.BindingProvider;

import org.slf4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.atlbatter.ANDON.NOTIF_CREATE.DT_NOTIF_CREATE_REQ;
import com.atlbatter.ANDON.NOTIF_CREATE.DT_NOTIF_CREATE_REQI_ITEM;
import com.atlbatter.ANDON.NOTIF_CREATE.DT_NOTIF_CREATE_RES;
import com.atlbatter.ANDON.NOTIF_CREATE.SI_NOTIF_CREATE_SENDBindingStub;
import com.atlbatter.ANDON.NOTIF_CREATE.SI_NOTIF_CREATE_SENDService;
import com.atlbatter.ANDON.NOTIF_CREATE.SI_NOTIF_CREATE_SENDServiceLocator;
import com.sun.xml.internal.ws.client.BindingProviderProperties;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Attribute;
import mhp.oee.web.Constants;
import mhp.oee.wserp.yhgztzdcj.DTNOTIFCREATEREQ.IITEM;

/**
 * Created by SuMingzhi on 2017/1/11.
 * *隐患、故障通知单创建接口
 */
@RestController
public class FaultListCreateController {

    @InjectableLogger
    private static Logger logger;

    @RequestMapping(value = "/web/rest/ws/erp/site/{site}/faultlistcreate", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> faultListCreate(
			    		 @RequestParam(value = "QMART", required = true) String QMART,
			    		 @RequestParam(value = "QMTXT", required = true) String QMTXT,
			    		 @RequestParam(value = "EQUNR", required = true) String EQUNR,
			    		 @RequestParam(value = "QMNAM", required = true) String QMNAM,
			    		 @RequestParam(value = "QMDAT", required = true) String QMDAT,
			    		 @RequestParam(value = "MZEIT", required = true) String MZEIT,
                         @RequestParam(value = "ZSTATE", required = true) String ZSTATE) {
    	String username = Attribute.getValue("webservice-address.properties","username");
		String password = Attribute.getValue("webservice-address.properties","password");
		String client = Attribute.getValue("webservice-address.properties","catl-qty");
		
		SI_NOTIF_CREATE_SENDService service = new SI_NOTIF_CREATE_SENDServiceLocator();
		DT_NOTIF_CREATE_REQ request = new DT_NOTIF_CREATE_REQ();
		
		request.setI_CLIENT(client);
		DT_NOTIF_CREATE_REQI_ITEM iitem = new DT_NOTIF_CREATE_REQI_ITEM();
		iitem.setEQUNR(EQUNR);
		iitem.setFECOD("");
		iitem.setFEGRP("");
		iitem.setFETXT("");
		iitem.setMZEIT(MZEIT);
		iitem.setOTEIL("");
		iitem.setOTGRP("");
		iitem.setQMART(QMART);
		iitem.setQMDAT(QMDAT);
		iitem.setQMNAM(QMNAM);
		iitem.setQMNUM("");
		iitem.setQMTXT(QMTXT);
		iitem.setTPLNR("");
		request.setI_ITEM(iitem);
		
		SI_NOTIF_CREATE_SENDBindingStub port = null;
		try {
			port = (SI_NOTIF_CREATE_SENDBindingStub) service.getHTTP_Port();
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}
		port.setUsername(username);
		port.setPassword(password);
		
		DT_NOTIF_CREATE_RES response = null;
		try {
			response = port.SI_NOTIF_CREATE_SEND(request);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		String eqmnum = response.getE_QMNUM();
		String emsg = response.getE_MSG();
		String esubrc = response.getE_SUBRC();
		Map<String,String> returnMap = new HashMap<String, String>();
		returnMap.put("eqmnum", eqmnum);
		returnMap.put("emsg", emsg);
		returnMap.put("esubrc", esubrc);
    	return ResponseEntity.ok(returnMap);
    }
}
