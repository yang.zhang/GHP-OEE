package mhp.oee.wserp.net;


import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.datacontract.schemas._2004._07.MXWebService.ParametricData;
import org.datacontract.schemas._2004._07.MXWebService.SetAndonValuesRequest;
import org.datacontract.schemas._2004._07.MXWebService.SetAndonValuesResponse;
import org.slf4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.tempuri.BasicHttpBinding_IServiceCommunicatorStub;
import org.tempuri.ControllerLocator;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.web.Constants;

/**
 * Created by SuMingzhi on 2017/2/20.
 * *对EAP控机接口
 */
@RestController
public class LimitResourceController {

    @InjectableLogger
    private static Logger logger;

    @RequestMapping(value = "/web/rest/ws/erp/site/{site}/limitresource", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> limitResourceController(
			    		 @RequestParam(value = "Resource", required = true) String Resource,
			    		 @RequestParam(value = "limitCode", required = true) String limitCode) {
    	ControllerLocator service = new ControllerLocator();
//    	service.setBasicHttpBinding_IServiceCommunicatorEndpointAddress("XXXXXXXXXXXXXXXXXXXXXXXXS");
		BasicHttpBinding_IServiceCommunicatorStub port = null;
		try {
			port = (BasicHttpBinding_IServiceCommunicatorStub) service.getBasicHttpBinding_IServiceCommunicator();
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}
		
		ParametricData parametricData = new ParametricData("AndonControl", limitCode);
		ParametricData[] parametricDataArray = new ParametricData[]{parametricData};
		SetAndonValuesRequest setAndonValuesRequest = new SetAndonValuesRequest(parametricDataArray, Resource);
		
		SetAndonValuesResponse response = null;
		try {
			response = port.setAndonValues(setAndonValuesRequest);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(response);
    }
}
