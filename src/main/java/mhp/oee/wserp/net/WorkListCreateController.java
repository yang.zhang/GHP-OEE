package mhp.oee.wserp.net;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.atlbatter.ANDON.IW31.DT_IW31_REQ;
import com.atlbatter.ANDON.IW31.DT_IW31_REQIT_COMPONENTS;
import com.atlbatter.ANDON.IW31.DT_IW31_REQIT_OPERATIONS;
import com.atlbatter.ANDON.IW31.DT_IW31_REQI_HEAD;
import com.atlbatter.ANDON.IW31.DT_IW31_RES;
import com.atlbatter.ANDON.IW31.SI_IW31_SENDBindingStub;
import com.atlbatter.ANDON.IW31.SI_IW31_SENDService;
import com.atlbatter.ANDON.IW31.SI_IW31_SENDServiceLocator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Attribute;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.web.Constants;
import mhp.oee.wserp.net.po.ExcelPO;

/**
 * 
 * 改造改善工单接口
 * 
 * @author SuMingzhi
 *
 */
@RestController
public class WorkListCreateController {

	@InjectableLogger
	private static Logger logger;

	private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

	@Autowired
	private WorkListCreateService workListCreateService;

	@RequestMapping(value = "/download/gzgsgd/template/site/{site}/excel", method = RequestMethod.GET)
	public ResponseEntity<?> exportExcel(@PathVariable("site") String site, HttpServletResponse response,
			HttpServletRequest request) {
		try {
			String filePath = request.getSession().getServletContext().getRealPath("/");
			filePath = filePath + "/workOrder.xlsx";
			Utils.generateDownloadFile(response, new File(filePath));
		} catch (IOException e) {
			ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
			logger.error("alertExportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
		}
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/web/rest/ws/erp/site/{site}/excelcheck", method = RequestMethod.POST)
	public ResponseEntity<?> excelCheck(@RequestParam(value = "file") MultipartFile file,
			@PathVariable("site") String site, HttpServletResponse res, HttpServletRequest request) {
		try {
			Utils.checkUploadFilename(file.getOriginalFilename());
			res.setHeader("Content-Type", "application/json");
			res.setStatus(HttpServletResponse.SC_ACCEPTED);
			XSSFWorkbook workbook = null;
			try {
				workbook = new XSSFWorkbook(file.getInputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
			boolean checkExcelFlag = workListCreateService.checkExcel(workbook);
			List<ExcelPO> excelList = workListCreateService.readExcel(workbook);
			Map returnMap = new HashMap<String,Object>();
			returnMap.put("checkResult", checkExcelFlag);
			returnMap.put("poList", excelList);
			return ResponseEntity.ok(returnMap);
		} catch (BusinessException e) {
			return Utils.returnExceptionToFrontEnd(e);
		}
	}

	@RequestMapping(value = "/web/rest/ws/erp/site/{site}/gzlistcreate", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
	public ResponseEntity<?> workListCreate(@RequestBody List<ExcelPO> excelList) {
		String username = Attribute.getValue("webservice-address.properties", "username");
		String password = Attribute.getValue("webservice-address.properties", "password");
		String client = Attribute.getValue("webservice-address.properties", "catl-qty");
		String UTERM = Attribute.getValue("webservice-address.properties", "tag-end-name");
		String VORNR = Attribute.getValue("webservice-address.properties", "vornr");
		String STEUS = Attribute.getValue("webservice-address.properties", "steus");
		String MODE = Attribute.getValue("webservice-address.properties", "mode");
		List<DT_IW31_RES> returnList = new ArrayList<DT_IW31_RES>();
		for (ExcelPO excelPO : excelList) {
			SI_IW31_SENDService service = new SI_IW31_SENDServiceLocator();
			SI_IW31_SENDBindingStub port = null;
			try {
				port = (SI_IW31_SENDBindingStub) service.getHTTP_Port();
			} catch (ServiceException e1) {
				e1.printStackTrace();
			}
			port.setUsername(username);
			port.setPassword(password);

			DT_IW31_REQ request = new DT_IW31_REQ();
			request.setI_CLIENT(client);
			request.setI_MODE(MODE);
			request.setI_RELEASE(excelPO.getIrelease());

			DT_IW31_REQI_HEAD ihead = excelPO.getIhead();
			ihead.setUTERM(UTERM);
			request.setI_HEAD(ihead);

			DT_IW31_REQIT_OPERATIONS[] itoperations = excelPO.getItoperations();
			if (itoperations.length < 1)
				continue;
			DT_IW31_REQIT_OPERATIONS itoperation = itoperations[0];
			itoperation.setVORNR(VORNR);
			itoperation.setMODE(MODE);
			itoperation.setSTEUS(STEUS);
			request.setIT_OPERATIONS(itoperations);

			DT_IW31_REQIT_COMPONENTS[] itcomponents = excelPO.getItcomponents();
			for (DT_IW31_REQIT_COMPONENTS itcomponent : itcomponents) {
				itcomponent.setMODE(MODE);
			}
			request.setIT_COMPONENTS(itcomponents);
			DT_IW31_RES response = null;
			try {
				response = port.SI_IW31_SEND(request);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			returnList.add(response);
		}
		return ResponseEntity.ok(returnList);
	}
}
