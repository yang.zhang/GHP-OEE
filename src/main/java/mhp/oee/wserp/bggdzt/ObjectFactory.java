
package mhp.oee.wserp.bggdzt;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mhp.oee.wserp4 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MTCHANGEORDERSTATUSREQ_QNAME = new QName("http://atlbattery.com/ANDON/CHANGE_ORDER_STATUS", "MT_CHANGE_ORDER_STATUS_REQ");
    private final static QName _MTCHANGEORDERSTATUSRES_QNAME = new QName("http://atlbattery.com/ANDON/CHANGE_ORDER_STATUS", "MT_CHANGE_ORDER_STATUS_RES");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mhp.oee.wserp4
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DTCHANGEORDERSTATUSREQ }
     * 
     */
    public DTCHANGEORDERSTATUSREQ createDTCHANGEORDERSTATUSREQ() {
        return new DTCHANGEORDERSTATUSREQ();
    }

    /**
     * Create an instance of {@link DTCHANGEORDERSTATUSRES }
     * 
     */
    public DTCHANGEORDERSTATUSRES createDTCHANGEORDERSTATUSRES() {
        return new DTCHANGEORDERSTATUSRES();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTCHANGEORDERSTATUSREQ }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbattery.com/ANDON/CHANGE_ORDER_STATUS", name = "MT_CHANGE_ORDER_STATUS_REQ")
    public JAXBElement<DTCHANGEORDERSTATUSREQ> createMTCHANGEORDERSTATUSREQ(DTCHANGEORDERSTATUSREQ value) {
        return new JAXBElement<DTCHANGEORDERSTATUSREQ>(_MTCHANGEORDERSTATUSREQ_QNAME, DTCHANGEORDERSTATUSREQ.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTCHANGEORDERSTATUSRES }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbattery.com/ANDON/CHANGE_ORDER_STATUS", name = "MT_CHANGE_ORDER_STATUS_RES")
    public JAXBElement<DTCHANGEORDERSTATUSRES> createMTCHANGEORDERSTATUSRES(DTCHANGEORDERSTATUSRES value) {
        return new JAXBElement<DTCHANGEORDERSTATUSRES>(_MTCHANGEORDERSTATUSRES_QNAME, DTCHANGEORDERSTATUSRES.class, null, value);
    }

}
