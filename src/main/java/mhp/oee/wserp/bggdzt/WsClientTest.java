package mhp.oee.wserp.bggdzt;

import java.net.Authenticator;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import com.sun.xml.internal.ws.client.BindingProviderProperties;


/**
 * 
 * 变更工单状态接口
 * 
 * @author SMZ
 *
 */


public class WsClientTest {
	public static void main(String[] args) {
		String username = "ADON_PD3_001";
		String password = "init1234";

		NtlmAuthenticator authenticator = new NtlmAuthenticator(username, password);
		Authenticator.setDefault(authenticator);

		SICHANGEORDERSTATUSSENDService service = new SICHANGEORDERSTATUSSENDService();
		SICHANGEORDERSTATUSSEND port = service.getHTTPPort();
		Map<String, Object> ctxt = ((BindingProvider) port).getRequestContext();
		ctxt.put(BindingProviderProperties.REQUEST_TIMEOUT, 10 * 60 * 1000);
		ctxt.put(BindingProviderProperties.CONNECT_TIMEOUT, 10 * 60 * 1000);

		DTCHANGEORDERSTATUSREQ request = new DTCHANGEORDERSTATUSREQ();
		request.setICLIENT("340");
		request.setIAUFNR("000010000102");
		request.setIANDONUSER("通知单");
		request.setISTATUS("D");

		DTCHANGEORDERSTATUSRES response = port.siCHANGEORDERSTATUSSEND(request);
		response.getEMSG();
		response.getESUBRC();
	}
}
