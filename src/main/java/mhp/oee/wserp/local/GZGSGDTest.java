package mhp.oee.wserp.local;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.atlbatter.ANDON.IW31.DT_IW31_REQ;
import com.atlbatter.ANDON.IW31.DT_IW31_REQIT_COMPONENTS;
import com.atlbatter.ANDON.IW31.DT_IW31_REQIT_OPERATIONS;
import com.atlbatter.ANDON.IW31.DT_IW31_REQI_HEAD;
import com.atlbatter.ANDON.IW31.DT_IW31_RES;
import com.atlbatter.ANDON.IW31.SI_IW31_SENDBindingStub;
import com.atlbatter.ANDON.IW31.SI_IW31_SENDService;
import com.atlbatter.ANDON.IW31.SI_IW31_SENDServiceLocator;



/*
 *  改造改善工单接口
 *  Created By SuMingzhi
 */

public class GZGSGDTest {


	public static void main(String[] args) throws ServiceException, RemoteException {
		SI_IW31_SENDService service = new SI_IW31_SENDServiceLocator();
		DT_IW31_REQ request = new DT_IW31_REQ();
		request.setI_CLIENT("340");
		request.setI_MODE("I");
		request.setI_RELEASE("");
		
		DT_IW31_REQI_HEAD ihead = new DT_IW31_REQI_HEAD();
		ihead.setAUFNR("N0001");
		ihead.setPM_AUFART("ZP03");
		ihead.setKTEXT("");
		ihead.setEQUNR("WXXX0001");
		ihead.setGSTRP("20170120");
		ihead.setUTERM("AndonWeb");
		ihead.setQMNAM("");
		request.setI_HEAD(ihead);
		
		DT_IW31_REQIT_OPERATIONS itoperation = new DT_IW31_REQIT_OPERATIONS();
		itoperation.setARBEIT("100");
		itoperation.setARBEITE("MIN");
		itoperation.setANZZL("1");
		itoperation.setVORNR("0010");
		itoperation.setMODE("I");
		itoperation.setSTEUS("PM01");
		DT_IW31_REQIT_OPERATIONS[] itOPERATIONS = {itoperation};
		request.setIT_OPERATIONS(itOPERATIONS);
		
		DT_IW31_REQIT_COMPONENTS itcomponent = new DT_IW31_REQIT_COMPONENTS();
		itcomponent.setMATNR("PT-212-008-0012");
		itcomponent.setMENGE("1");
		itcomponent.setMODE("I");
		DT_IW31_REQIT_COMPONENTS[] itcomponents = {itcomponent};
		request.setIT_COMPONENTS(itcomponents);
		

		SI_IW31_SENDBindingStub port = (SI_IW31_SENDBindingStub) service.getHTTP_Port();
		port.setUsername("ADON_PD3_001");
		port.setPassword("init1234");
		DT_IW31_RES response = port.SI_IW31_SEND(request);
		String eaufnr = response.getE_AUFNR();
		String eqmnum = response.getE_QMNUM();
		String esubrc = response.getE_SUBRC();
		String emsg = response.getE_MSG();
		System.out.println(eaufnr);
		System.out.println(eqmnum);
		System.out.println(esubrc);
		System.out.println(emsg);

    }
}
