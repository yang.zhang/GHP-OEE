package mhp.oee.wserp.local;

import java.rmi.RemoteException;

import org.datacontract.schemas._2004._07.MXWebService.ParametricData;
import org.datacontract.schemas._2004._07.MXWebService.SetAndonValuesRequest;
import org.datacontract.schemas._2004._07.MXWebService.SetAndonValuesResponse;
import org.tempuri.BasicHttpBinding_IServiceCommunicatorStub;
import org.tempuri.ControllerLocator;

/*
 * 对EAP控机接口
 * Created by SuMingzhi on 2017/1/13.
 */
public class EAPTest {
	public static void main(String[] args) throws Exception{
//		service.setBasicHttpBinding_IServiceCommunicatorEndpointAddress("XXXXXXXXXXXXXXXXXXXXXXXXS");
		ControllerLocator service = new ControllerLocator();
		BasicHttpBinding_IServiceCommunicatorStub port = 
				(BasicHttpBinding_IServiceCommunicatorStub) service.getBasicHttpBinding_IServiceCommunicator();
		
		ParametricData parametricData = new ParametricData("AndonControl", "1");
		ParametricData[] parametricDataArray = new ParametricData[]{parametricData};
		SetAndonValuesRequest setAndonValuesRequest = new SetAndonValuesRequest(parametricDataArray, "WXXX0001");
		
		SetAndonValuesResponse response = null;
		try {
			response = port.setAndonValues(setAndonValuesRequest);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		String message = response.getMessage();
		Integer code = response.getCode();
		System.out.println(message);
		System.out.println("-----------------");
		System.out.println(code);
		
	}
}
