package mhp.oee.wserp.local;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.atlbatter.ANDON.ord_2_order.DT_ORD2ORDER_REQ;
import com.atlbatter.ANDON.ord_2_order.DT_ORD2ORDER_REQIT_ORDER;
import com.atlbatter.ANDON.ord_2_order.DT_ORD2ORDER_RES;
import com.atlbatter.ANDON.ord_2_order.DT_ORD2ORDER_RESET_ORDER;
import com.atlbatter.ANDON.ord_2_order.SI_ORD_2_ORDER_OUTBindingStub;
import com.atlbatter.ANDON.ord_2_order.SI_ORD_2_ORDER_OUTService;
import com.atlbatter.ANDON.ord_2_order.SI_ORD_2_ORDER_OUTServiceLocator;



/*
 *    ME接单接口
 *  Created By SuMingzhi
 */
public class MEJDTest {

	public static void main(String[] args) throws ServiceException, RemoteException {
		SI_ORD_2_ORDER_OUTService service = new SI_ORD_2_ORDER_OUTServiceLocator();
		DT_ORD2ORDER_REQ request = new DT_ORD2ORDER_REQ();
		request.setI_CLIENT("340");
//		request.setIUTERM(Attribute.getValue("webservice-address.properties","tag-end-name"));AndonWeb
		request.setI_UTERM("AndonWeb");
		request.setI_ANDON_USER("测试人1");
		request.setI_ZSPOT("0");
		request.setI_RELEASE("X");
		DT_ORD2ORDER_REQIT_ORDER itORDER = new DT_ORD2ORDER_REQIT_ORDER();
		itORDER.setAUFNR("000010000102");
		DT_ORD2ORDER_REQIT_ORDER[] itORDERs = {itORDER};
		request.setIT_ORDER(itORDERs);

		SI_ORD_2_ORDER_OUTBindingStub port = (SI_ORD_2_ORDER_OUTBindingStub) service.getHTTP_Port();
		port.setUsername("ADON_PD3_001");
		port.setPassword("init1234");
		
		DT_ORD2ORDER_RES response = port.SI_ORD_2_ORDER_OUT(request);
		DT_ORD2ORDER_RESET_ORDER[] etORDER = response.getET_ORDER();
		String emsg = response.getE_MSG();
		String esubrc = response.getE_SUBRC();
		System.out.println(emsg);
		System.out.println(esubrc);
		System.out.println(etORDER[0].getAUFNR());

    }

	
}
