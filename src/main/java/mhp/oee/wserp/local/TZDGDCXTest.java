package mhp.oee.wserp.local;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;
import com.atlbattery.ECC.PM_notification_simplex.DT_NOTIFICATION_REQ;
import com.atlbattery.ECC.PM_notification_simplex.DT_NOTIFICATION_REQET_EQUNR;
import com.atlbattery.ECC.PM_notification_simplex.DT_NOTIFICATION_RESET_ITEMS;
import com.atlbattery.ECC.PM_notification_simplex.SI_ANDON_NOTIFICATION_SENDBindingStub;
import com.atlbattery.ECC.PM_notification_simplex.SI_ANDON_NOTIFICATION_SENDService;
import com.atlbattery.ECC.PM_notification_simplex.SI_ANDON_NOTIFICATION_SENDServiceLocator;

/*
 *    通知单工单查询
 *  Created By SuMingzhi
 */
public class TZDGDCXTest {

	public static void main(String[] args) throws ServiceException, RemoteException {
		SI_ANDON_NOTIFICATION_SENDService service = new SI_ANDON_NOTIFICATION_SENDServiceLocator();
		DT_NOTIFICATION_REQ req = new DT_NOTIFICATION_REQ();
		req.setZCLIENT("340");
		req.setZSTATE("F");
		DT_NOTIFICATION_REQET_EQUNR ET_EQUNR = new DT_NOTIFICATION_REQET_EQUNR();
		DT_NOTIFICATION_REQET_EQUNR[] ET_EQUNRs = {ET_EQUNR};
		ET_EQUNR.setEQUNR("WXXX0001");
		
		req.setET_EQUNR(ET_EQUNRs);
		SI_ANDON_NOTIFICATION_SENDBindingStub port = (SI_ANDON_NOTIFICATION_SENDBindingStub) service.getHTTP_Port();
		port.setUsername("ADON_PD3_001");
		port.setPassword("init1234");
		
		DT_NOTIFICATION_RESET_ITEMS[] res = port.SI_ANDON_NOTIFICATION_SEND(req);
		DT_NOTIFICATION_RESET_ITEMS etitems = res[0];
		System.out.println(etitems.getQMNUM());
		System.out.println(etitems.getAUFNR());
		System.out.println(etitems.getQMTXT());
		System.out.println(etitems.getKTEXT());
		System.out.println(etitems.getQMART());
		System.out.println(etitems.getQMARTX());
		System.out.println(etitems.getAUFART());
		System.out.println(etitems.getAUARTTEXT());
		System.out.println(etitems.getGSTRP());
		System.out.println(etitems.getTXT30());
		System.out.println(etitems.getTXT04());
		System.out.println(etitems.getTYPE());
		System.out.println(etitems.getMSG());

    }

	
}
