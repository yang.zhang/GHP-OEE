package mhp.oee.wserp.local;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.atlbatter.ANDON.get_pmurl.DT_GETPMURL_REQ;
import com.atlbatter.ANDON.get_pmurl.DT_GETPMURL_REQIT_PARAMETERS;
import com.atlbatter.ANDON.get_pmurl.DT_GETPMURL_RES;
import com.atlbatter.ANDON.get_pmurl.SI_GETPMURLOUTBindingStub;
import com.atlbatter.ANDON.get_pmurl.SI_GETPMURLOUTService;
import com.atlbatter.ANDON.get_pmurl.SI_GETPMURLOUTServiceLocator;

/*
 *   工单编辑接口
 *  Created By SuMingzhi
 */
public class GDBJTest {

	public static void main(String[] args) throws ServiceException, RemoteException {
		SI_GETPMURLOUTService service = new SI_GETPMURLOUTServiceLocator();
		DT_GETPMURL_REQ request = new DT_GETPMURL_REQ();
		request.setI_CLIENT("340");
//		request.setIACTION(Attribute.getValue("webservice-address.properties","i-action"));ZPM004
		request.setI_ACTION("ZPM004");
		request.setI_USER("测试人1");
//		request.setICOMMIT(Attribute.getValue("webservice-address.properties","i-commit"));X
		request.setI_COMMIT("X");
		DT_GETPMURL_REQIT_PARAMETERS itparameter = new DT_GETPMURL_REQIT_PARAMETERS();
		itparameter.setNAME("MODE");
		itparameter.setVALUE("1");
		DT_GETPMURL_REQIT_PARAMETERS itparameter2 = new DT_GETPMURL_REQIT_PARAMETERS();
		itparameter2.setNAME("AUFNR");
		itparameter2.setVALUE("4000037");
		DT_GETPMURL_REQIT_PARAMETERS[] itparameters = {itparameter,itparameter2};
		request.setIT_PARAMETERS(itparameters);

		SI_GETPMURLOUTBindingStub port = (SI_GETPMURLOUTBindingStub) service.getHTTP_Port();
		port.setUsername("ADON_PD3_001");
		port.setPassword("init1234");
		
		DT_GETPMURL_RES response = port.SI_GETPMURLOUT(request);
		String eurl = response.getE_URL();
		System.out.println(eurl);


    }

	
}
