package mhp.oee.wserp.local;



import com.atlbattery.ANDON.CHANGE_ORDER_STATUS.DT_CHANGE_ORDER_STATUS_REQ;
import com.atlbattery.ANDON.CHANGE_ORDER_STATUS.DT_CHANGE_ORDER_STATUS_RES;
import com.atlbattery.ANDON.CHANGE_ORDER_STATUS.SI_CHANGE_ORDER_STATUS_SENDBindingStub;
import com.atlbattery.ANDON.CHANGE_ORDER_STATUS.SI_CHANGE_ORDER_STATUS_SENDService;
import com.atlbattery.ANDON.CHANGE_ORDER_STATUS.SI_CHANGE_ORDER_STATUS_SENDServiceLocator;




/*
 *   变更工单状态接口
 *  Created By SuMingzhi
 */
public class BGGDZTTest {

	public static void main(String[] args){
		SI_CHANGE_ORDER_STATUS_SENDService service = new SI_CHANGE_ORDER_STATUS_SENDServiceLocator();
		DT_CHANGE_ORDER_STATUS_REQ request = new DT_CHANGE_ORDER_STATUS_REQ();
		request.setI_CLIENT("340");
		request.setI_AUFNR("000010000102");
		request.setI_ANDON_USER("测试人1");
		request.setI_STATUS("D");

		SI_CHANGE_ORDER_STATUS_SENDBindingStub port = null;
		try {
			port = (SI_CHANGE_ORDER_STATUS_SENDBindingStub) service.getHTTP_Port();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		port.setUsername("ADON_PD3_001");
		port.setPassword("init1234");
		
		DT_CHANGE_ORDER_STATUS_RES response = null;
		try {
			response = port.SI_CHANGE_ORDER_STATUS_SEND(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String emsg = response.getE_MSG();
		String esubrc = response.getE_SUBRC();
		System.out.println(emsg);
		System.out.println(esubrc);

    }

	
}
