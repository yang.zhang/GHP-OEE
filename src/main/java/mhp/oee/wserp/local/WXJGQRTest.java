package mhp.oee.wserp.local;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.atlbatter.ANDON.CONFIRM_ORDER.DT_CONFIRM_ORDER_REQ;
import com.atlbatter.ANDON.CONFIRM_ORDER.DT_CONFIRM_ORDER_REQIT_ITEMS;
import com.atlbatter.ANDON.CONFIRM_ORDER.DT_CONFIRM_ORDER_RESET_ITEMS;
import com.atlbatter.ANDON.CONFIRM_ORDER.SI_CONFIRM_ORDER_OUTBindingStub;
import com.atlbatter.ANDON.CONFIRM_ORDER.SI_CONFIRM_ORDER_OUTService;
import com.atlbatter.ANDON.CONFIRM_ORDER.SI_CONFIRM_ORDER_OUTServiceLocator;


/*
 *  维修结果确认接口
 *  Created By SuMingzhi
 */

public class WXJGQRTest {

	public static void main(String[] args) throws ServiceException, RemoteException {
		SI_CONFIRM_ORDER_OUTService service = new SI_CONFIRM_ORDER_OUTServiceLocator();
		DT_CONFIRM_ORDER_REQ request = new DT_CONFIRM_ORDER_REQ();
		request.setI_CLIENT("340");
		request.setI_ANDON_USR("测试人1");
		request.setI_RESULT("0");
		DT_CONFIRM_ORDER_REQIT_ITEMS ititem = new DT_CONFIRM_ORDER_REQIT_ITEMS();
		ititem.setAUFNR("");
		DT_CONFIRM_ORDER_REQIT_ITEMS[] itITEMS = {ititem};
		request.setIT_ITEMS(itITEMS);

		SI_CONFIRM_ORDER_OUTBindingStub port = (SI_CONFIRM_ORDER_OUTBindingStub) service.getHTTP_Port();
		port.setUsername("ADON_PD3_001");
		port.setPassword("init1234");
		DT_CONFIRM_ORDER_RESET_ITEMS[] response = port.SI_CONFIRM_ORDER_OUT(request);
		String msg = response[0].getMSG();
		System.out.println(msg);
    }
}
