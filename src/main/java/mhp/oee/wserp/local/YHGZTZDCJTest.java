package mhp.oee.wserp.local;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.atlbatter.ANDON.NOTIF_CREATE.DT_NOTIF_CREATE_REQ;
import com.atlbatter.ANDON.NOTIF_CREATE.DT_NOTIF_CREATE_REQI_ITEM;
import com.atlbatter.ANDON.NOTIF_CREATE.DT_NOTIF_CREATE_RES;
import com.atlbatter.ANDON.NOTIF_CREATE.SI_NOTIF_CREATE_SENDBindingStub;
import com.atlbatter.ANDON.NOTIF_CREATE.SI_NOTIF_CREATE_SENDService;
import com.atlbatter.ANDON.NOTIF_CREATE.SI_NOTIF_CREATE_SENDServiceLocator;


/*
 * 隐患、故障通知单创建接口
 *  Created By SuMingzhi
 */
public class YHGZTZDCJTest {

	public static void main(String[] args) throws ServiceException, RemoteException {
		SI_NOTIF_CREATE_SENDService service = new SI_NOTIF_CREATE_SENDServiceLocator();
		DT_NOTIF_CREATE_REQ req = new DT_NOTIF_CREATE_REQ();
		req.setI_CLIENT("340");
		DT_NOTIF_CREATE_REQI_ITEM iitem = new DT_NOTIF_CREATE_REQI_ITEM();
		iitem.setEQUNR("WXXX0001");
		iitem.setFECOD("");
		iitem.setFEGRP("");
		iitem.setFETXT("");
		iitem.setMZEIT("");
		iitem.setOTEIL("");
		iitem.setOTGRP("");
		iitem.setQMART("Z5");
		iitem.setQMDAT("");
		iitem.setQMNAM("");
		iitem.setQMNUM("");
		iitem.setQMTXT("");
		iitem.setTPLNR("");
		req.setI_ITEM(iitem);

		SI_NOTIF_CREATE_SENDBindingStub port = (SI_NOTIF_CREATE_SENDBindingStub) service.getHTTP_Port();
		port.setUsername("ADON_PD3_001");
		port.setPassword("init1234");
		
		DT_NOTIF_CREATE_RES response = port.SI_NOTIF_CREATE_SEND(req);
		String eqmnum = response.getE_QMNUM();
		String emsg = response.getE_MSG();
		String esubrc = response.getE_SUBRC();
		System.out.println(eqmnum);
		System.out.println(emsg);
		System.out.println(esubrc);

    }

	
}
