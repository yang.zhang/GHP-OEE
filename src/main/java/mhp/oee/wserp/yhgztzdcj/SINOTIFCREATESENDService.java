
package mhp.oee.wserp.yhgztzdcj;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "SI_NOTIF_CREATE_SENDService", targetNamespace = "http://atlbatter.com/ANDON/NOTIF_CREATE", wsdlLocation = "http://ndsapd111.atlbattery.com:50900/dir/wsdl?p=ic/08015f1fb4eb3e1d82dd868b531d787c")
public class SINOTIFCREATESENDService
    extends Service
{

    private final static URL SINOTIFCREATESENDSERVICE_WSDL_LOCATION;
    private final static WebServiceException SINOTIFCREATESENDSERVICE_EXCEPTION;
    private final static QName SINOTIFCREATESENDSERVICE_QNAME = new QName("http://atlbatter.com/ANDON/NOTIF_CREATE", "SI_NOTIF_CREATE_SENDService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://ndsapd111.atlbattery.com:50900/dir/wsdl?p=ic/08015f1fb4eb3e1d82dd868b531d787c");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SINOTIFCREATESENDSERVICE_WSDL_LOCATION = url;
        SINOTIFCREATESENDSERVICE_EXCEPTION = e;
    }

    public SINOTIFCREATESENDService() {
        super(__getWsdlLocation(), SINOTIFCREATESENDSERVICE_QNAME);
    }

    public SINOTIFCREATESENDService(WebServiceFeature... features) {
        super(__getWsdlLocation(), SINOTIFCREATESENDSERVICE_QNAME, features);
    }

    public SINOTIFCREATESENDService(URL wsdlLocation) {
        super(wsdlLocation, SINOTIFCREATESENDSERVICE_QNAME);
    }

    public SINOTIFCREATESENDService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SINOTIFCREATESENDSERVICE_QNAME, features);
    }

    public SINOTIFCREATESENDService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SINOTIFCREATESENDService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns SINOTIFCREATESEND
     */
    @WebEndpoint(name = "HTTP_Port")
    public SINOTIFCREATESEND getHTTPPort() {
        return super.getPort(new QName("http://atlbatter.com/ANDON/NOTIF_CREATE", "HTTP_Port"), SINOTIFCREATESEND.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns SINOTIFCREATESEND
     */
    @WebEndpoint(name = "HTTP_Port")
    public SINOTIFCREATESEND getHTTPPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://atlbatter.com/ANDON/NOTIF_CREATE", "HTTP_Port"), SINOTIFCREATESEND.class, features);
    }

    /**
     * 
     * @return
     *     returns SINOTIFCREATESEND
     */
    @WebEndpoint(name = "HTTPS_Port")
    public SINOTIFCREATESEND getHTTPSPort() {
        return super.getPort(new QName("http://atlbatter.com/ANDON/NOTIF_CREATE", "HTTPS_Port"), SINOTIFCREATESEND.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns SINOTIFCREATESEND
     */
    @WebEndpoint(name = "HTTPS_Port")
    public SINOTIFCREATESEND getHTTPSPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://atlbatter.com/ANDON/NOTIF_CREATE", "HTTPS_Port"), SINOTIFCREATESEND.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SINOTIFCREATESENDSERVICE_EXCEPTION!= null) {
            throw SINOTIFCREATESENDSERVICE_EXCEPTION;
        }
        return SINOTIFCREATESENDSERVICE_WSDL_LOCATION;
    }

}
