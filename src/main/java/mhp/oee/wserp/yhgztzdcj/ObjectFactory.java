
package mhp.oee.wserp.yhgztzdcj;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mhp.oee.wserp2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MTNOTIFCREATERES_QNAME = new QName("http://atlbatter.com/ANDON/NOTIF_CREATE", "MT_NOTIF_CREATE_RES");
    private final static QName _MTNOTIFCREATEREQ_QNAME = new QName("http://atlbatter.com/ANDON/NOTIF_CREATE", "MT_NOTIF_CREATE_REQ");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mhp.oee.wserp2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DTNOTIFCREATEREQ }
     * 
     */
    public DTNOTIFCREATEREQ createDTNOTIFCREATEREQ() {
        return new DTNOTIFCREATEREQ();
    }

    /**
     * Create an instance of {@link DTNOTIFCREATERES }
     * 
     */
    public DTNOTIFCREATERES createDTNOTIFCREATERES() {
        return new DTNOTIFCREATERES();
    }

    /**
     * Create an instance of {@link DTNOTIFCREATEREQ.IITEM }
     * 
     */
    public DTNOTIFCREATEREQ.IITEM createDTNOTIFCREATEREQIITEM() {
        return new DTNOTIFCREATEREQ.IITEM();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTNOTIFCREATERES }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbatter.com/ANDON/NOTIF_CREATE", name = "MT_NOTIF_CREATE_RES")
    public JAXBElement<DTNOTIFCREATERES> createMTNOTIFCREATERES(DTNOTIFCREATERES value) {
        return new JAXBElement<DTNOTIFCREATERES>(_MTNOTIFCREATERES_QNAME, DTNOTIFCREATERES.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTNOTIFCREATEREQ }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbatter.com/ANDON/NOTIF_CREATE", name = "MT_NOTIF_CREATE_REQ")
    public JAXBElement<DTNOTIFCREATEREQ> createMTNOTIFCREATEREQ(DTNOTIFCREATEREQ value) {
        return new JAXBElement<DTNOTIFCREATEREQ>(_MTNOTIFCREATEREQ_QNAME, DTNOTIFCREATEREQ.class, null, value);
    }

}
