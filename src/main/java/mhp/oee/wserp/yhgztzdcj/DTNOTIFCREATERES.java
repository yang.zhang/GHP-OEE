
package mhp.oee.wserp.yhgztzdcj;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * DATA TYPE NOTIF CREATE RESPONSE
 * 
 * <p>DT_NOTIF_CREATE_RES complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType name="DT_NOTIF_CREATE_RES">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="E_QMNUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="E_MSG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="E_SUBRC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_NOTIF_CREATE_RES", propOrder = {
    "eqmnum",
    "emsg",
    "esubrc"
})
public class DTNOTIFCREATERES {

    @XmlElement(name = "E_QMNUM")
    protected String eqmnum;
    @XmlElement(name = "E_MSG")
    protected String emsg;
    @XmlElement(name = "E_SUBRC")
    protected String esubrc;

    /**
     * ��ȡeqmnum���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEQMNUM() {
        return eqmnum;
    }

    /**
     * ����eqmnum���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEQMNUM(String value) {
        this.eqmnum = value;
    }

    /**
     * ��ȡemsg���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMSG() {
        return emsg;
    }

    /**
     * ����emsg���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMSG(String value) {
        this.emsg = value;
    }

    /**
     * ��ȡesubrc���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getESUBRC() {
        return esubrc;
    }

    /**
     * ����esubrc���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setESUBRC(String value) {
        this.esubrc = value;
    }

}
