package mhp.oee.wserp.yhgztzdcj;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * MESSAGE TYPE NOTIF CREATE REQUEST
 * 
 * <p>DT_NOTIF_CREATE_REQ complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType name="DT_NOTIF_CREATE_REQ">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="I_CLIENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="I_ITEM" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="QMNUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="QMART" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="QMTXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TPLNR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EQUNR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="OTGRP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="OTEIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FEGRP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FECOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FETXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="QMNAM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="QMDAT" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="8"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="MZEIT" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="6"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_NOTIF_CREATE_REQ", propOrder = {
    "iclient",
    "iitem"
})
public class DTNOTIFCREATEREQ {

    @XmlElement(name = "I_CLIENT")
    protected String iclient;
    @XmlElement(name = "I_ITEM")
    protected DTNOTIFCREATEREQ.IITEM iitem;

    /**
     * ��ȡiclient���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getICLIENT() {
        return iclient;
    }

    /**
     * ����iclient���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setICLIENT(String value) {
        this.iclient = value;
    }

    /**
     * ��ȡiitem���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link DTNOTIFCREATEREQ.IITEM }
     *     
     */
    public DTNOTIFCREATEREQ.IITEM getIITEM() {
        return iitem;
    }

    /**
     * ����iitem���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link DTNOTIFCREATEREQ.IITEM }
     *     
     */
    public void setIITEM(DTNOTIFCREATEREQ.IITEM value) {
        this.iitem = value;
    }


    /**
     * <p>anonymous complex type�� Java �ࡣ
     * 
     * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="QMNUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="QMART" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="QMTXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TPLNR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EQUNR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="OTGRP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="OTEIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FEGRP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FECOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FETXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="QMNAM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="QMDAT" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="8"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="MZEIT" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="6"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "qmnum",
        "qmart",
        "qmtxt",
        "tplnr",
        "equnr",
        "otgrp",
        "oteil",
        "fegrp",
        "fecod",
        "fetxt",
        "qmnam",
        "qmdat",
        "mzeit"
    })
    public static class IITEM {

        @XmlElement(name = "QMNUM")
        protected String qmnum;
        @XmlElement(name = "QMART")
        protected String qmart;
        @XmlElement(name = "QMTXT")
        protected String qmtxt;
        @XmlElement(name = "TPLNR")
        protected String tplnr;
        @XmlElement(name = "EQUNR")
        protected String equnr;
        @XmlElement(name = "OTGRP")
        protected String otgrp;
        @XmlElement(name = "OTEIL")
        protected String oteil;
        @XmlElement(name = "FEGRP")
        protected String fegrp;
        @XmlElement(name = "FECOD")
        protected String fecod;
        @XmlElement(name = "FETXT")
        protected String fetxt;
        @XmlElement(name = "QMNAM")
        protected String qmnam;
        @XmlElement(name = "QMDAT")
        protected String qmdat;
        @XmlElement(name = "MZEIT")
        protected String mzeit;

        /**
         * ��ȡqmnum���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQMNUM() {
            return qmnum;
        }

        /**
         * ����qmnum���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQMNUM(String value) {
            this.qmnum = value;
        }

        /**
         * ��ȡqmart���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQMART() {
            return qmart;
        }

        /**
         * ����qmart���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQMART(String value) {
            this.qmart = value;
        }

        /**
         * ��ȡqmtxt���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQMTXT() {
            return qmtxt;
        }

        /**
         * ����qmtxt���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQMTXT(String value) {
            this.qmtxt = value;
        }

        /**
         * ��ȡtplnr���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTPLNR() {
            return tplnr;
        }

        /**
         * ����tplnr���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTPLNR(String value) {
            this.tplnr = value;
        }

        /**
         * ��ȡequnr���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEQUNR() {
            return equnr;
        }

        /**
         * ����equnr���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEQUNR(String value) {
            this.equnr = value;
        }

        /**
         * ��ȡotgrp���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOTGRP() {
            return otgrp;
        }

        /**
         * ����otgrp���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOTGRP(String value) {
            this.otgrp = value;
        }

        /**
         * ��ȡoteil���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOTEIL() {
            return oteil;
        }

        /**
         * ����oteil���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOTEIL(String value) {
            this.oteil = value;
        }

        /**
         * ��ȡfegrp���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFEGRP() {
            return fegrp;
        }

        /**
         * ����fegrp���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFEGRP(String value) {
            this.fegrp = value;
        }

        /**
         * ��ȡfecod���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFECOD() {
            return fecod;
        }

        /**
         * ����fecod���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFECOD(String value) {
            this.fecod = value;
        }

        /**
         * ��ȡfetxt���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFETXT() {
            return fetxt;
        }

        /**
         * ����fetxt���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFETXT(String value) {
            this.fetxt = value;
        }

        /**
         * ��ȡqmnam���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQMNAM() {
            return qmnam;
        }

        /**
         * ����qmnam���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQMNAM(String value) {
            this.qmnam = value;
        }

        /**
         * ��ȡqmdat���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQMDAT() {
            return qmdat;
        }

        /**
         * ����qmdat���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQMDAT(String value) {
            this.qmdat = value;
        }

        /**
         * ��ȡmzeit���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMZEIT() {
            return mzeit;
        }

        /**
         * ����mzeit���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMZEIT(String value) {
            this.mzeit = value;
        }

    }

}
