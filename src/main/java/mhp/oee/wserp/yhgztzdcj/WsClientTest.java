package mhp.oee.wserp.yhgztzdcj;

import java.net.Authenticator;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import com.sun.xml.internal.ws.client.BindingProviderProperties;

import mhp.oee.wserp.yhgztzdcj.DTNOTIFCREATEREQ.IITEM;


/**
 * 
 * 隐患、故障通知单创建接口
 * 
 * @author SMZ
 *
 */


public class WsClientTest {
	public static void main(String[] args) {
		String username = "ADON_PD3_001";
		String password = "init1234";

		NtlmAuthenticator authenticator = new NtlmAuthenticator(username, password);
		Authenticator.setDefault(authenticator);

		SINOTIFCREATESENDService service = new SINOTIFCREATESENDService();
		SINOTIFCREATESEND port = service.getHTTPPort();
		Map<String, Object> ctxt = ((BindingProvider) port).getRequestContext();
		ctxt.put(BindingProviderProperties.REQUEST_TIMEOUT, 10 * 60 * 1000);
		ctxt.put(BindingProviderProperties.CONNECT_TIMEOUT, 10 * 60 * 1000);

		DTNOTIFCREATEREQ request = new DTNOTIFCREATEREQ();
		
		request.setICLIENT("340");
		IITEM iitem = new IITEM();
		iitem.setEQUNR("WXXX0001");
		iitem.setFECOD("");
		iitem.setFEGRP("");
		iitem.setFETXT("");
		iitem.setMZEIT("");
		iitem.setOTEIL("");
		iitem.setOTGRP("");
		iitem.setQMART("Z5");
		iitem.setQMDAT("");
		iitem.setQMNAM("");
		iitem.setQMNUM("");
		iitem.setQMTXT("");
		iitem.setTPLNR("");
		request.setIITEM(iitem);
		
		DTNOTIFCREATERES response = port.siNOTIFCREATESEND(request);
		String eqmnum = response.getEQMNUM();
		String emsg = response.getEMSG();
		String esubrc = response.getESUBRC();
		System.out.println(eqmnum);
		System.out.println(emsg);
		System.out.println(esubrc);
	
	}
}
