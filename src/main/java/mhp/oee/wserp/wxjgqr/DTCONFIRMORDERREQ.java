
package mhp.oee.wserp.wxjgqr;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * DATA TYPE CONFIRM ORDER REQUEST
 * 
 * <p>DT_CONFIRM_ORDER_REQ complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType name="DT_CONFIRM_ORDER_REQ">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="I_CLIENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="I_ANDON_USR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="I_RESULT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IT_ITEMS" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AUFNR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_CONFIRM_ORDER_REQ", propOrder = {
    "iclient",
    "iandonusr",
    "iresult",
    "ititems"
})
public class DTCONFIRMORDERREQ {

    @XmlElement(name = "I_CLIENT", required = true)
    protected String iclient;
    @XmlElement(name = "I_ANDON_USR", required = true)
    protected String iandonusr;
    @XmlElement(name = "I_RESULT", required = true)
    protected String iresult;
    @XmlElement(name = "IT_ITEMS", required = true)
    protected List<DTCONFIRMORDERREQ.ITITEMS> ititems;

    /**
     * ��ȡiclient���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getICLIENT() {
        return iclient;
    }

    /**
     * ����iclient���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setICLIENT(String value) {
        this.iclient = value;
    }

    /**
     * ��ȡiandonusr���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIANDONUSR() {
        return iandonusr;
    }

    /**
     * ����iandonusr���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIANDONUSR(String value) {
        this.iandonusr = value;
    }

    /**
     * ��ȡiresult���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIRESULT() {
        return iresult;
    }

    /**
     * ����iresult���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIRESULT(String value) {
        this.iresult = value;
    }

    /**
     * Gets the value of the ititems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ititems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getITITEMS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DTCONFIRMORDERREQ.ITITEMS }
     * 
     * 
     */
    public List<DTCONFIRMORDERREQ.ITITEMS> getITITEMS() {
        if (ititems == null) {
            ititems = new ArrayList<DTCONFIRMORDERREQ.ITITEMS>();
        }
        return this.ititems;
    }


    /**
     * <p>anonymous complex type�� Java �ࡣ
     * 
     * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AUFNR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "aufnr"
    })
    public static class ITITEMS {

        @XmlElement(name = "AUFNR", required = true)
        protected String aufnr;

        /**
         * ��ȡaufnr���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAUFNR() {
            return aufnr;
        }

        /**
         * ����aufnr���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAUFNR(String value) {
            this.aufnr = value;
        }

    }

}
