
package mhp.oee.wserp.wxjgqr;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * DATA TYPE CONFIRM ORDER RESPONSE
 * 
 * <p>DT_CONFIRM_ORDER_RES complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType name="DT_CONFIRM_ORDER_RES">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ET_ITEMS" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AUFNR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AUART" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="KTEXT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SUBRC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MSG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_CONFIRM_ORDER_RES", propOrder = {
    "etitems"
})
public class DTCONFIRMORDERRES {

    @XmlElement(name = "ET_ITEMS", required = true)
    protected List<DTCONFIRMORDERRES.ETITEMS> etitems;

    /**
     * Gets the value of the etitems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the etitems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getETITEMS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DTCONFIRMORDERRES.ETITEMS }
     * 
     * 
     */
    public List<DTCONFIRMORDERRES.ETITEMS> getETITEMS() {
        if (etitems == null) {
            etitems = new ArrayList<DTCONFIRMORDERRES.ETITEMS>();
        }
        return this.etitems;
    }


    /**
     * <p>anonymous complex type�� Java �ࡣ
     * 
     * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AUFNR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AUART" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="KTEXT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SUBRC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MSG" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "aufnr",
        "auart",
        "ktext",
        "subrc",
        "msg"
    })
    public static class ETITEMS {

        @XmlElement(name = "AUFNR", required = true)
        protected String aufnr;
        @XmlElement(name = "AUART", required = true)
        protected String auart;
        @XmlElement(name = "KTEXT", required = true)
        protected String ktext;
        @XmlElement(name = "SUBRC", required = true)
        protected String subrc;
        @XmlElement(name = "MSG", required = true)
        protected String msg;

        /**
         * ��ȡaufnr���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAUFNR() {
            return aufnr;
        }

        /**
         * ����aufnr���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAUFNR(String value) {
            this.aufnr = value;
        }

        /**
         * ��ȡauart���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAUART() {
            return auart;
        }

        /**
         * ����auart���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAUART(String value) {
            this.auart = value;
        }

        /**
         * ��ȡktext���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getKTEXT() {
            return ktext;
        }

        /**
         * ����ktext���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setKTEXT(String value) {
            this.ktext = value;
        }

        /**
         * ��ȡsubrc���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSUBRC() {
            return subrc;
        }

        /**
         * ����subrc���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSUBRC(String value) {
            this.subrc = value;
        }

        /**
         * ��ȡmsg���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMSG() {
            return msg;
        }

        /**
         * ����msg���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMSG(String value) {
            this.msg = value;
        }

    }

}
