
package mhp.oee.wserp.wxjgqr;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mhp.oee.wserp5 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MTCONFIRMORDERREQ_QNAME = new QName("http://atlbatter.com/ANDON/CONFIRM_ORDER", "MT_CONFIRM_ORDER_REQ");
    private final static QName _MTCONFIRMORDERRES_QNAME = new QName("http://atlbatter.com/ANDON/CONFIRM_ORDER", "MT_CONFIRM_ORDER_RES");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mhp.oee.wserp5
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DTCONFIRMORDERRES }
     * 
     */
    public DTCONFIRMORDERRES createDTCONFIRMORDERRES() {
        return new DTCONFIRMORDERRES();
    }

    /**
     * Create an instance of {@link DTCONFIRMORDERREQ }
     * 
     */
    public DTCONFIRMORDERREQ createDTCONFIRMORDERREQ() {
        return new DTCONFIRMORDERREQ();
    }

    /**
     * Create an instance of {@link DTCONFIRMORDERRES.ETITEMS }
     * 
     */
    public DTCONFIRMORDERRES.ETITEMS createDTCONFIRMORDERRESETITEMS() {
        return new DTCONFIRMORDERRES.ETITEMS();
    }

    /**
     * Create an instance of {@link DTCONFIRMORDERREQ.ITITEMS }
     * 
     */
    public DTCONFIRMORDERREQ.ITITEMS createDTCONFIRMORDERREQITITEMS() {
        return new DTCONFIRMORDERREQ.ITITEMS();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTCONFIRMORDERREQ }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbatter.com/ANDON/CONFIRM_ORDER", name = "MT_CONFIRM_ORDER_REQ")
    public JAXBElement<DTCONFIRMORDERREQ> createMTCONFIRMORDERREQ(DTCONFIRMORDERREQ value) {
        return new JAXBElement<DTCONFIRMORDERREQ>(_MTCONFIRMORDERREQ_QNAME, DTCONFIRMORDERREQ.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTCONFIRMORDERRES }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbatter.com/ANDON/CONFIRM_ORDER", name = "MT_CONFIRM_ORDER_RES")
    public JAXBElement<DTCONFIRMORDERRES> createMTCONFIRMORDERRES(DTCONFIRMORDERRES value) {
        return new JAXBElement<DTCONFIRMORDERRES>(_MTCONFIRMORDERRES_QNAME, DTCONFIRMORDERRES.class, null, value);
    }

}
