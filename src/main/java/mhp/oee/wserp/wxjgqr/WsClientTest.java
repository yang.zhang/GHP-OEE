package mhp.oee.wserp.wxjgqr;

import java.net.Authenticator;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import com.sun.xml.internal.ws.client.BindingProviderProperties;

import mhp.oee.wserp.wxjgqr.DTCONFIRMORDERREQ.ITITEMS;
import mhp.oee.wserp.wxjgqr.DTCONFIRMORDERRES.ETITEMS;

/**
 * 
 *维修结果确认接口
 * 
 * @author SMZ
 *
 */


public class WsClientTest {
	public static void main(String[] args) {
		String username = "ADON_PD3_001";
		String password = "init1234";

		NtlmAuthenticator authenticator = new NtlmAuthenticator(username, password);
		Authenticator.setDefault(authenticator);

		SICONFIRMORDEROUTService service = new SICONFIRMORDEROUTService();
		SICONFIRMORDEROUT port = service.getHTTPPort();
		Map<String, Object> ctxt = ((BindingProvider) port).getRequestContext();
		ctxt.put(BindingProviderProperties.REQUEST_TIMEOUT, 10 * 60 * 1000);
		ctxt.put(BindingProviderProperties.CONNECT_TIMEOUT, 10 * 60 * 1000);

		DTCONFIRMORDERREQ request = new DTCONFIRMORDERREQ();
		request.setIANDONUSR("");
		request.setICLIENT("340");
		request.setIRESULT("");
		List<ITITEMS> ititems = request.getITITEMS();
		ITITEMS ititem = new ITITEMS();
		ititem.setAUFNR("");
		ititems.add(ititem);
		DTCONFIRMORDERRES response = port.siCONFIRMORDEROUT(request);
		List<ETITEMS> etitems = response.getETITEMS();
		
		
		
		System.out.println(etitems.get(0).getMSG());
	}
}
