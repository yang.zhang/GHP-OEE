package mhp.oee.wserp.mejd;

import java.net.Authenticator;
import java.util.List;
import java.util.Map;
import javax.xml.ws.BindingProvider;
import com.sun.xml.internal.ws.client.BindingProviderProperties;
import mhp.oee.utils.Attribute;
import mhp.oee.wserp.mejd.DTORD2ORDERREQ.ITORDER;
import mhp.oee.wserp.mejd.DTORD2ORDERRES.ETORDER;


/**
 * 
 * ME接单接口
 * 
 * @author SMZ
 *
 */


public class WsClientTest {
	public static void main(String[] args) {
		String username = "ADON_PD3_001";
		String password = "init1234";

		NtlmAuthenticator authenticator = new NtlmAuthenticator(username, password);
		Authenticator.setDefault(authenticator);

		SIORD2ORDEROUTService service = new SIORD2ORDEROUTService();
		SIORD2ORDEROUT port = service.getHTTPPort();
		Map<String, Object> ctxt = ((BindingProvider) port).getRequestContext();
		ctxt.put(BindingProviderProperties.REQUEST_TIMEOUT, 10 * 60 * 1000);
		ctxt.put(BindingProviderProperties.CONNECT_TIMEOUT, 10 * 60 * 1000);

		DTORD2ORDERREQ request = new DTORD2ORDERREQ();
		request.setICLIENT("340");
		request.setIUTERM(Attribute.getValue("webservice-address.properties","tag-end-name"));
		request.setIANDONUSER("测试一");
		request.setIZSPOT("0");
		List<ITORDER> itorders = request.getITORDER();
		
		
		ITORDER itorder = new ITORDER();
		itorder.setAUFNR("000010000372");
		itorders.add(itorder);

		
		DTORD2ORDERRES response = port.siORD2ORDEROUT(request);
		
		
		List<ETORDER> etorders = response.getETORDER();
		System.out.println(response.getEMSG());
		System.out.println(response.getESUBRC());
		System.out.println(etorders.get(0).getAUFNR());
	}
}
