
package mhp.oee.wserp.mejd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mhp.oee.wserp3 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MTORD2ORDERREQ_QNAME = new QName("http://atlbatter.com/ANDON/ord_2_order", "MT_ORD2ORDER_REQ");
    private final static QName _MTORD2ORDERRES_QNAME = new QName("http://atlbatter.com/ANDON/ord_2_order", "MT_ORD2ORDER_RES");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mhp.oee.wserp3
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DTORD2ORDERREQ }
     * 
     */
    public DTORD2ORDERREQ createDTORD2ORDERREQ() {
        return new DTORD2ORDERREQ();
    }

    /**
     * Create an instance of {@link DTORD2ORDERRES }
     * 
     */
    public DTORD2ORDERRES createDTORD2ORDERRES() {
        return new DTORD2ORDERRES();
    }

    /**
     * Create an instance of {@link DTORD2ORDERREQ.ITORDER }
     * 
     */
    public DTORD2ORDERREQ.ITORDER createDTORD2ORDERREQITORDER() {
        return new DTORD2ORDERREQ.ITORDER();
    }

    /**
     * Create an instance of {@link DTORD2ORDERRES.ETORDER }
     * 
     */
    public DTORD2ORDERRES.ETORDER createDTORD2ORDERRESETORDER() {
        return new DTORD2ORDERRES.ETORDER();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTORD2ORDERREQ }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbatter.com/ANDON/ord_2_order", name = "MT_ORD2ORDER_REQ")
    public JAXBElement<DTORD2ORDERREQ> createMTORD2ORDERREQ(DTORD2ORDERREQ value) {
        return new JAXBElement<DTORD2ORDERREQ>(_MTORD2ORDERREQ_QNAME, DTORD2ORDERREQ.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTORD2ORDERRES }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbatter.com/ANDON/ord_2_order", name = "MT_ORD2ORDER_RES")
    public JAXBElement<DTORD2ORDERRES> createMTORD2ORDERRES(DTORD2ORDERRES value) {
        return new JAXBElement<DTORD2ORDERRES>(_MTORD2ORDERRES_QNAME, DTORD2ORDERRES.class, null, value);
    }

}
