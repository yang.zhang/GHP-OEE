
package mhp.oee.wserp.gzgsgd;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "SI_IW31_SEND", targetNamespace = "http://atlbatter.com/ANDON/IW31")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface SIIW31SEND {


    /**
     * 
     * @param mtIW31REQ
     * @return
     *     returns mhp.oee.wserp6.DTIW31RES
     */
    @WebMethod(operationName = "SI_IW31_SEND", action = "http://sap.com/xi/WebService/soap1.1")
    @WebResult(name = "MT_IW31_RES", targetNamespace = "http://atlbatter.com/ANDON/IW31", partName = "MT_IW31_RES")
    public DTIW31RES siIW31SEND(
        @WebParam(name = "MT_IW31_REQ", targetNamespace = "http://atlbatter.com/ANDON/IW31", partName = "MT_IW31_REQ")
        DTIW31REQ mtIW31REQ);

}
