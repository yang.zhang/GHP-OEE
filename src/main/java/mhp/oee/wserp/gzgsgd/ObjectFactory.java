
package mhp.oee.wserp.gzgsgd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mhp.oee.wserp6 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MTIW31REQ_QNAME = new QName("http://atlbatter.com/ANDON/IW31", "MT_IW31_REQ");
    private final static QName _MTIW31RES_QNAME = new QName("http://atlbatter.com/ANDON/IW31", "MT_IW31_RES");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mhp.oee.wserp6
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DTIW31REQ }
     * 
     */
    public DTIW31REQ createDTIW31REQ() {
        return new DTIW31REQ();
    }

    /**
     * Create an instance of {@link DTIW31RES }
     * 
     */
    public DTIW31RES createDTIW31RES() {
        return new DTIW31RES();
    }

    /**
     * Create an instance of {@link DTIW31REQ.IHEAD }
     * 
     */
    public DTIW31REQ.IHEAD createDTIW31REQIHEAD() {
        return new DTIW31REQ.IHEAD();
    }

    /**
     * Create an instance of {@link DTIW31REQ.ITOPERATIONS }
     * 
     */
    public DTIW31REQ.ITOPERATIONS createDTIW31REQITOPERATIONS() {
        return new DTIW31REQ.ITOPERATIONS();
    }

    /**
     * Create an instance of {@link DTIW31REQ.ITCOMPONENTS }
     * 
     */
    public DTIW31REQ.ITCOMPONENTS createDTIW31REQITCOMPONENTS() {
        return new DTIW31REQ.ITCOMPONENTS();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTIW31REQ }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbatter.com/ANDON/IW31", name = "MT_IW31_REQ")
    public JAXBElement<DTIW31REQ> createMTIW31REQ(DTIW31REQ value) {
        return new JAXBElement<DTIW31REQ>(_MTIW31REQ_QNAME, DTIW31REQ.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTIW31RES }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbatter.com/ANDON/IW31", name = "MT_IW31_RES")
    public JAXBElement<DTIW31RES> createMTIW31RES(DTIW31RES value) {
        return new JAXBElement<DTIW31RES>(_MTIW31RES_QNAME, DTIW31RES.class, null, value);
    }

}
