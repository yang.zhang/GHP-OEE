package mhp.oee.wserp.gzgsgd;

import java.net.Authenticator;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import com.sun.xml.internal.ws.client.BindingProviderProperties;

import mhp.oee.utils.Attribute;
import mhp.oee.wserp.gzgsgd.DTIW31REQ.IHEAD;
import mhp.oee.wserp.gzgsgd.DTIW31REQ.ITCOMPONENTS;
import mhp.oee.wserp.gzgsgd.DTIW31REQ.ITOPERATIONS;



/**
 * 
 * 改造改善工单接口
 * 
 * @author SMZ
 *
 */


public class WsClientTest {
	public static void main(String[] args) {
		String username = Attribute.getValue("webservice-address.properties","username");
		String password = Attribute.getValue("webservice-address.properties","password");
		String client = Attribute.getValue("webservice-address.properties","catl-qty");
		String UTERM = Attribute.getValue("webservice-address.properties","tag-end-name");
		String VORNR = Attribute.getValue("webservice-address.properties","vornr");
		String STEUS = Attribute.getValue("webservice-address.properties","steus");
		String MODE = Attribute.getValue("webservice-address.properties","mode");

		NtlmAuthenticator authenticator = new NtlmAuthenticator(username, password);
		Authenticator.setDefault(authenticator);

		SIIW31SENDService service = new SIIW31SENDService();
		SIIW31SEND port = service.getHTTPPort();
		Map<String, Object> ctxt = ((BindingProvider) port).getRequestContext();
		ctxt.put(BindingProviderProperties.REQUEST_TIMEOUT, 10 * 60 * 1000);
		ctxt.put(BindingProviderProperties.CONNECT_TIMEOUT, 10 * 60 * 1000);
		
		DTIW31REQ request = new DTIW31REQ();
		request.setICLIENT(client);
		request.setIMODE(MODE);
		request.setIRELEASE("");
		
		IHEAD ihead = new IHEAD();
		ihead.setAUFNR("N0001");
		ihead.setPMAUFART("ZP03");
		ihead.setKTEXT("改造改善工单创建cs");
		ihead.setEQUNR("WXXX0001");
		ihead.setGSTRP("20170120");
		ihead.setUTERM(UTERM);
		ihead.setQMNAM("测试人1");
		request.setIHEAD(ihead);
		
		List<ITOPERATIONS> itoperations = request.getITOPERATIONS();
		ITOPERATIONS itoperation = new ITOPERATIONS();
		itoperation.setARBEIT("100");
		itoperation.setARBEITE("MIN");
		itoperation.setANZZL("1");
		itoperation.setVORNR(VORNR);
		itoperation.setMODE(MODE);
		itoperation.setSTEUS(STEUS);
		itoperations.add(itoperation);
		
		List<ITCOMPONENTS> itcomponents = request.getITCOMPONENTS();
		ITCOMPONENTS itcomponent = new ITCOMPONENTS();
		itcomponent.setMATNR("PT-212-008-0012");
		itcomponent.setMENGE("1");
		itcomponent.setMODE("I");
		
		itcomponents.add(itcomponent);
		
		DTIW31RES response = port.siIW31SEND(request);
		String eaufnr = response.getEAUFNR();
		String eqmnum = response.getEQMNUM();
		String esubrc = response.getESUBRC();
		String emsg = response.getEMSG();
		System.out.println(eaufnr);
		System.out.println(eqmnum);
		System.out.println(esubrc);
		System.out.println(emsg);
	}
}
