
package mhp.oee.wserp.tzdgdcx;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mhp.oee.wserp1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MTNOTIFICATIONRES_QNAME = new QName("http://atlbattery.com/ECC/PM_notification_simplex", "MT_NOTIFICATION_RES");
    private final static QName _MTNOTIFICATIONREQ_QNAME = new QName("http://atlbattery.com/ECC/PM_notification_simplex", "MT_NOTIFICATION_REQ");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mhp.oee.wserp1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DTNOTIFICATIONREQ }
     * 
     */
    public DTNOTIFICATIONREQ createDTNOTIFICATIONREQ() {
        return new DTNOTIFICATIONREQ();
    }

    /**
     * Create an instance of {@link DTNOTIFICATIONRES }
     * 
     */
    public DTNOTIFICATIONRES createDTNOTIFICATIONRES() {
        return new DTNOTIFICATIONRES();
    }

    /**
     * Create an instance of {@link DTNOTIFICATIONREQ.ETEQUNR }
     * 
     */
    public DTNOTIFICATIONREQ.ETEQUNR createDTNOTIFICATIONREQETEQUNR() {
        return new DTNOTIFICATIONREQ.ETEQUNR();
    }

    /**
     * Create an instance of {@link DTNOTIFICATIONRES.ETITEMS }
     * 
     */
    public DTNOTIFICATIONRES.ETITEMS createDTNOTIFICATIONRESETITEMS() {
        return new DTNOTIFICATIONRES.ETITEMS();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTNOTIFICATIONRES }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbattery.com/ECC/PM_notification_simplex", name = "MT_NOTIFICATION_RES")
    public JAXBElement<DTNOTIFICATIONRES> createMTNOTIFICATIONRES(DTNOTIFICATIONRES value) {
        return new JAXBElement<DTNOTIFICATIONRES>(_MTNOTIFICATIONRES_QNAME, DTNOTIFICATIONRES.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTNOTIFICATIONREQ }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbattery.com/ECC/PM_notification_simplex", name = "MT_NOTIFICATION_REQ")
    public JAXBElement<DTNOTIFICATIONREQ> createMTNOTIFICATIONREQ(DTNOTIFICATIONREQ value) {
        return new JAXBElement<DTNOTIFICATIONREQ>(_MTNOTIFICATIONREQ_QNAME, DTNOTIFICATIONREQ.class, null, value);
    }

}
