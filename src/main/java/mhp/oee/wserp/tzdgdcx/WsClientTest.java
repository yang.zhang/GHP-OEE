package mhp.oee.wserp.tzdgdcx;

import java.net.Authenticator;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import com.sun.xml.internal.ws.client.BindingProviderProperties;

import mhp.oee.utils.Attribute;
import mhp.oee.wserp.tzdgdcx.DTNOTIFICATIONREQ.ETEQUNR;
import mhp.oee.wserp.tzdgdcx.DTNOTIFICATIONRES.ETITEMS;

public class WsClientTest {
	public static void main(String[] args) {
		String username = Attribute.getValue("webservice-address.properties","username");
		String password = Attribute.getValue("webservice-address.properties","password");

		NtlmAuthenticator authenticator = new NtlmAuthenticator(username, password);
		Authenticator.setDefault(authenticator);

		SIANDONNOTIFICATIONSENDService service = new SIANDONNOTIFICATIONSENDService();
		SIANDONNOTIFICATIONSEND port = service.getHTTPPort();
		Map<String, Object> ctxt = ((BindingProvider) port).getRequestContext();
		ctxt.put(BindingProviderProperties.REQUEST_TIMEOUT, 10 * 60 * 1000);
		ctxt.put(BindingProviderProperties.CONNECT_TIMEOUT, 10 * 60 * 1000);

		DTNOTIFICATIONREQ request = new DTNOTIFICATIONREQ();
		request.setZSTATE("B");
		request.setZCLIENT(Attribute.getValue("webservice-address.properties","catl-qty"));
		List<ETEQUNR> etequnrs = request.getETEQUNR();
		ETEQUNR etequnr = new ETEQUNR();
		etequnr.setEQUNR("WXXX0001");
		etequnrs.add(etequnr);

		DTNOTIFICATIONRES response = port.siANDONNOTIFICATIONSEND(request);
		ETITEMS etitems = response.getETITEMS().get(0);
		System.out.println(etitems.getQMNUM());
		System.out.println(etitems.getAUFNR());
		System.out.println(etitems.getQMTXT());
		System.out.println(etitems.getKTEXT());
		System.out.println(etitems.getQMART());
		System.out.println(etitems.getQMARTX());
		System.out.println(etitems.getAUFART());
		System.out.println(etitems.getAUARTTEXT());
		System.out.println(etitems.getGSTRP());
		System.out.println(etitems.getTXT30());
		System.out.println(etitems.getTXT04());
		System.out.println(etitems.getTYPE());
		System.out.println(etitems.getMSG());
		String equnr = etitems.getEQUNR();
		System.out.println(equnr);
	}
}
