
package mhp.oee.wserp.tzdgdcx;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>DT_NOTIFICATION_RES complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType name="DT_NOTIFICATION_RES">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ET_ITEMS" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="QMNUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AUFNR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="QMTXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="KTEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="QMART" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="QMARTX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ZQSTATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AUFART" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AUARTTEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="GSTRP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MSG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TXT04" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TXT30" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_NOTIFICATION_RES", propOrder = {
    "etitems"
})
public class DTNOTIFICATIONRES {

    @XmlElement(name = "ET_ITEMS")
    protected List<DTNOTIFICATIONRES.ETITEMS> etitems;

    /**
     * Gets the value of the etitems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the etitems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getETITEMS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DTNOTIFICATIONRES.ETITEMS }
     * 
     * 
     */
    public List<DTNOTIFICATIONRES.ETITEMS> getETITEMS() {
        if (etitems == null) {
            etitems = new ArrayList<DTNOTIFICATIONRES.ETITEMS>();
        }
        return this.etitems;
    }


    /**
     * <p>anonymous complex type�� Java �ࡣ
     * 
     * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="QMNUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AUFNR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="QMTXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="KTEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="QMART" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="QMARTX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ZQSTATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AUFART" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AUARTTEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="GSTRP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MSG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TXT04" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TXT30" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "qmnum",
        "aufnr",
        "qmtxt",
        "ktext",
        "qmart",
        "qmartx",
        "zqstate",
        "aufart",
        "auarttext",
        "gstrp",
        "type",
        "msg",
        "txt04",
        "txt30",
        "equnr"
    })
    public static class ETITEMS {

        @XmlElement(name = "QMNUM")
        protected String qmnum;
        @XmlElement(name = "AUFNR")
        protected String aufnr;
        @XmlElement(name = "QMTXT")
        protected String qmtxt;
        @XmlElement(name = "KTEXT")
        protected String ktext;
        @XmlElement(name = "QMART")
        protected String qmart;
        @XmlElement(name = "QMARTX")
        protected String qmartx;
        @XmlElement(name = "ZQSTATE")
        protected String zqstate;
        @XmlElement(name = "AUFART")
        protected String aufart;
        @XmlElement(name = "AUARTTEXT")
        protected String auarttext;
        @XmlElement(name = "GSTRP")
        protected String gstrp;
        @XmlElement(name = "TYPE")
        protected String type;
        @XmlElement(name = "MSG")
        protected String msg;
        @XmlElement(name = "TXT04")
        protected String txt04;
        @XmlElement(name = "TXT30")
        protected String txt30;
        @XmlElement(name = "EQUNR")
        protected String equnr;

        /**
         * ��ȡqmnum���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQMNUM() {
            return qmnum;
        }

        /**
         * ����qmnum���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQMNUM(String value) {
            this.qmnum = value;
        }

        /**
         * ��ȡaufnr���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAUFNR() {
            return aufnr;
        }

        /**
         * ����aufnr���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAUFNR(String value) {
            this.aufnr = value;
        }

        /**
         * ��ȡqmtxt���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQMTXT() {
            return qmtxt;
        }

        /**
         * ����qmtxt���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQMTXT(String value) {
            this.qmtxt = value;
        }

        /**
         * ��ȡktext���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getKTEXT() {
            return ktext;
        }

        /**
         * ����ktext���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setKTEXT(String value) {
            this.ktext = value;
        }

        /**
         * ��ȡqmart���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQMART() {
            return qmart;
        }

        /**
         * ����qmart���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQMART(String value) {
            this.qmart = value;
        }

        /**
         * ��ȡqmartx���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQMARTX() {
            return qmartx;
        }

        /**
         * ����qmartx���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQMARTX(String value) {
            this.qmartx = value;
        }

        /**
         * ��ȡzqstate���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZQSTATE() {
            return zqstate;
        }

        /**
         * ����zqstate���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZQSTATE(String value) {
            this.zqstate = value;
        }

        /**
         * ��ȡaufart���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAUFART() {
            return aufart;
        }

        /**
         * ����aufart���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAUFART(String value) {
            this.aufart = value;
        }

        /**
         * ��ȡauarttext���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAUARTTEXT() {
            return auarttext;
        }

        /**
         * ����auarttext���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAUARTTEXT(String value) {
            this.auarttext = value;
        }

        /**
         * ��ȡgstrp���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGSTRP() {
            return gstrp;
        }

        /**
         * ����gstrp���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGSTRP(String value) {
            this.gstrp = value;
        }

        /**
         * ��ȡtype���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTYPE() {
            return type;
        }

        /**
         * ����type���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTYPE(String value) {
            this.type = value;
        }

        /**
         * ��ȡmsg���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMSG() {
            return msg;
        }

        /**
         * ����msg���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMSG(String value) {
            this.msg = value;
        }

        /**
         * ��ȡtxt04���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTXT04() {
            return txt04;
        }

        /**
         * ����txt04���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTXT04(String value) {
            this.txt04 = value;
        }

        /**
         * ��ȡtxt30���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTXT30() {
            return txt30;
        }

        /**
         * ����txt30���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTXT30(String value) {
            this.txt30 = value;
        }
        
        
        public String getEQUNR() {
            return equnr;
        }
        public void setEQUNR(String value) {
            this.equnr = value;
        }

    }

}
