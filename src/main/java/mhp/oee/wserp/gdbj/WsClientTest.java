package mhp.oee.wserp.gdbj;

import java.net.Authenticator;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import com.sun.xml.internal.ws.client.BindingProviderProperties;

import mhp.oee.utils.Attribute;
import mhp.oee.wserp.gdbj.DTGETPMURLREQ.ITPARAMETERS;




/**
 *
 * 工单编辑接口
 *
 * @author SMZ
 *
 */


public class WsClientTest {
	public static void main(String[] args) {
		String username = "ADON_PD3_001";
		String password = "init1234";

		NtlmAuthenticator authenticator = new NtlmAuthenticator(username, password);
		Authenticator.setDefault(authenticator);

		SIGETPMURLOUTService service = new SIGETPMURLOUTService();
		SIGETPMURLOUT port = service.getHTTPPort();
		Map<String, Object> ctxt = ((BindingProvider) port).getRequestContext();
		ctxt.put(BindingProviderProperties.REQUEST_TIMEOUT, 10 * 60 * 1000);
		ctxt.put(BindingProviderProperties.CONNECT_TIMEOUT, 10 * 60 * 1000);

		DTGETPMURLREQ request = new DTGETPMURLREQ();
		request.setICLIENT("340");
		request.setIACTION(Attribute.getValue("webservice-address.properties","i-action"));
		request.setIUSER("测试�?");
		request.setICOMMIT(Attribute.getValue("webservice-address.properties","i-commit"));
		List<ITPARAMETERS> itparameters = request.getITPARAMETERS();
		ITPARAMETERS itparameter = new ITPARAMETERS();
		itparameter.setNAME("MODE");
		itparameter.setVALUE("1");
		itparameters.add(itparameter);
		ITPARAMETERS itparameter2 = new ITPARAMETERS();
		itparameter2.setNAME("AUFNR");
		itparameter2.setVALUE("4000037");
		itparameters.add(itparameter2);

		DTGETPMURLRES response = port.siGETPMURLOUT(request);
		String eurl = response.getEURL();
		System.out.println(eurl);
	}
}
