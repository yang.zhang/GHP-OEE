
package mhp.oee.wserp.gdbj;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mhp.oee.wserp7 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MTGETPMURLRES_QNAME = new QName("http://atlbatter.com/ANDON/get_pmurl", "MT_GETPMURL_RES");
    private final static QName _MTGETPMURLREQ_QNAME = new QName("http://atlbatter.com/ANDON/get_pmurl", "MT_GETPMURL_REQ");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mhp.oee.wserp7
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DTGETPMURLREQ }
     * 
     */
    public DTGETPMURLREQ createDTGETPMURLREQ() {
        return new DTGETPMURLREQ();
    }

    /**
     * Create an instance of {@link DTGETPMURLRES }
     * 
     */
    public DTGETPMURLRES createDTGETPMURLRES() {
        return new DTGETPMURLRES();
    }

    /**
     * Create an instance of {@link DTGETPMURLREQ.ITPARAMETERS }
     * 
     */
    public DTGETPMURLREQ.ITPARAMETERS createDTGETPMURLREQITPARAMETERS() {
        return new DTGETPMURLREQ.ITPARAMETERS();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTGETPMURLRES }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbatter.com/ANDON/get_pmurl", name = "MT_GETPMURL_RES")
    public JAXBElement<DTGETPMURLRES> createMTGETPMURLRES(DTGETPMURLRES value) {
        return new JAXBElement<DTGETPMURLRES>(_MTGETPMURLRES_QNAME, DTGETPMURLRES.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTGETPMURLREQ }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://atlbatter.com/ANDON/get_pmurl", name = "MT_GETPMURL_REQ")
    public JAXBElement<DTGETPMURLREQ> createMTGETPMURLREQ(DTGETPMURLREQ value) {
        return new JAXBElement<DTGETPMURLREQ>(_MTGETPMURLREQ_QNAME, DTGETPMURLREQ.class, null, value);
    }

}
