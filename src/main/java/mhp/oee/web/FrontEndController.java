package mhp.oee.web;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import mhp.oee.common.logging.InjectableLogger;

@Controller
public class FrontEndController {

    @InjectableLogger
    private static Logger logger;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String rootIndex(Model model, HttpServletRequest request) {
        logger.debug("FrontEndController.rootIndex()");
        return "redirect:/htmls/index.html";
    }

}
