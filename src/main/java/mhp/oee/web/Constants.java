package mhp.oee.web;

public class Constants {

    public static final String REST_HTTP_GET_CONTENT_TYPE = "application/json;charset=utf-8";
    public static final String RESPONSE_HEADER_ERROR_MESSAGE = "Error-Message";
    public static final String RESPONSE_HEADER_LOGOUT_MESSAGE = "Logout";

    public static final int IGNORED_NUM = 2;
    public static final int REASON_CODE_ALERT_IGNORED_NUM = 1;
    public static final int USERGROUP_IGNORED_NUM = 1;

    public static final int PPM_UPLOAD_LENGTH = 4;
    public static final int PLC_UPLOAD_LENGTH = 7;
    public static final int REASONCODE_UPLOAD_LENGTH = 4;
    public static final int USERGROUP_UPLOAD_LENGTH = 2;

    public static final int MODEL_SIX = 6;
    public static final int MODEL_SEVEN = 7;

}
