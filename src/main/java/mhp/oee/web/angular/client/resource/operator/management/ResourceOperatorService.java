package mhp.oee.web.angular.client.resource.operator.management;

import java.util.Date;
import java.util.List;

import mhp.oee.common.exception.*;
import mhp.oee.common.handle.UserBOHandle;
import mhp.oee.component.user.UserGroupComponent;
import mhp.oee.enumClass.UPPCLoginModule;
import mhp.oee.po.gen.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.api.v1.request.ResourceReasonLogIn;
import mhp.oee.api.v1.request.ResourceReasonLogRequest;
import mhp.oee.appserver.netweaver.NetWeaverUserProvider;
import mhp.oee.common.handle.ResourceCurrentDataHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.component.plant.PlcAccessTypeComponent;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.component.plant.ResourceCurrentDataComponent;
import mhp.oee.component.production.ItemComponent;
import mhp.oee.component.production.ResourceReasonLogComponent;
import mhp.oee.component.production.ResourceTimeLogComponent;
import mhp.oee.config.AppServerEnvironment;
import mhp.oee.utils.Utils;
import mhp.oee.vo.NetWeaverUserAuthVO;
import mhp.oee.vo.ResourceCurrentDataVO;
import mhp.oee.vo.ResourceReasonLogVO;
import org.springframework.util.CollectionUtils;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class ResourceOperatorService {
    
    @Autowired
    PlcAccessTypeComponent accessTypeComponent;
    @Autowired
    ResourceComponent resourceComponent;
    @Autowired
    ResourceCurrentDataComponent resourceCurrentDataComponent;
    @Autowired
    ItemComponent itemComponent;
    @Autowired
    private ResourceTimeLogComponent resourceTimeLogComponent;
    
    @Autowired
    private ResourceReasonLogComponent resourceReasonLogComponent;

	@Autowired
	private UserGroupComponent userGroupComponent;

    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    public ResourceCurrentDataVO getResourceOperatorInfo(String pcIp) {
        List<ResourcePlcInfoPO> ResourcePlcList =accessTypeComponent.getResourcePlcInfoByPcIPAndPlcAccessTypeNull(pcIp);
		if(CollectionUtils.isEmpty(ResourcePlcList)){
			return null;
		}

        String resourceBO = ResourcePlcList.get(0).getResourceBo();
		String handle="ResourceCurrentDataBO:"+resourceBO;
        ResourceCurrentDataPO po = resourceCurrentDataComponent.selectByHandle(handle);

        ResourcePO resourcePO=resourceComponent.getResourceByHandle(resourceBO);
        ItemPO itemPO = itemComponent.getItem(po.getItem());
        ResourceCurrentDataVO vo=Utils.copyObjectProperties(po, ResourceCurrentDataVO.class);
        vo.setResource(resourcePO.getResrce());
        vo.setResourceName(resourcePO.getDescription());
        if(itemPO!=null){
            vo.setItem(itemPO.getItem());
            vo.setItemName(itemPO.getDescription());
        }
        return vo;
    }

	public ResourceCurrentDataVO getResourceOperatorInfoByPCIp(String pcIp) {
		List<ResourcePlcInfoPO> resourcePlcList =accessTypeComponent.getResourcePlcInfoByPcIPAndPlcAccessTypeNull(pcIp);

		if(CollectionUtils.isEmpty(resourcePlcList)){
			return null;
		}

		String resourceBO=resourcePlcList.get(0).getResourceBo();
		String handle="ResourceCurrentDataBO:"+resourceBO;
		ResourceCurrentDataPO po = resourceCurrentDataComponent.selectByHandle(handle);

		//如果没有resource_current_data没有数据 初始化一条数据
		if(po == null){

			ResourceCurrentDataHandle resourceCurrentDataHandle = new ResourceCurrentDataHandle(resourceBO);
			po = new ResourceCurrentDataPO();

			po.setHandle(resourceCurrentDataHandle.getValue());
			po.setResourceBo(resourceBO);

			this.resourceCurrentDataComponent.insert(po);
		}

		ResourcePO resourcePO=resourceComponent.getResourceByHandle(resourceBO);
		ItemPO itemPO = itemComponent.getItem(po.getItem());
		ResourceCurrentDataVO vo=Utils.copyObjectProperties(po, ResourceCurrentDataVO.class);
		vo.setResource(resourcePO.getResrce());
		vo.setResourceName(resourcePO.getDescription());
		if(itemPO!=null){
			vo.setItem(itemPO.getItem());
			vo.setItemName(itemPO.getDescription());
		}
		return vo;
	}

    
    public void changeItem(ResourceCurrentDataVO vo) throws BusinessException{
	    ResourceCurrentDataHandle handle = new ResourceCurrentDataHandle(vo.getResourceBo());
        ResourceCurrentDataPO po = resourceCurrentDataComponent.selectByHandle(handle.getValue());
        if(po == null ){
        	ResourceCurrentDataPO poNew = Utils.copyObjectProperties(vo, ResourceCurrentDataPO.class);
        	poNew.setHandle(handle.getValue());
        	poNew.setOperatorLocked("false");
        	poNew.setMeStaffLocked("false");
        	poNew.setModifiedUser(ContextHelper.getContext().getUser());
        	resourceCurrentDataComponent.insert(poNew);
        }else{
        	if( !vo.getItem().equals(po.getItem()) ){
	        	po.setItem(vo.getItem());
	        	resourceCurrentDataComponent.updateByPrimaryKey(po);
	        }
        }
	}

	public ResourceCurrentDataVO changeOperatorOrMeUser(ResourceCurrentDataVO voIn) {
		boolean isAuth =  getUserAuth(voIn);
		ResourceCurrentDataVO voOut = new ResourceCurrentDataVO();
		//用户密码错误
		if(!isAuth){
			voOut.setCode(ErrorCodeEnum.UPPC_LOGIN_ERROR.getErrorCode() + "");
			voOut.setMsg("用户密码错误");
			return voOut;
		}
		//权限判断
		UPPCLoginModule uppcLoginModule = UPPCLoginModule.fromValue(voIn.getModule());
		String site = voIn.getSite();
		if(uppcLoginModule == null || StringUtils.isBlank(site)){
			//参数错误
			voOut.setCode(ErrorCodeEnum.UPPC_PARAM_ERROR.getErrorCode() + "");
			voOut.setMsg("参数错误");
			return voOut;
		}
		UserBOHandle userBOHandle = new UserBOHandle(site,voIn.getUserName().toUpperCase());
		List<UserGroupMemberPO> userGroupMemberPOList = this.userGroupComponent.getUserGroupsByUser(userBOHandle.getValue());
		boolean isHavePermission = false;
		if(userGroupMemberPOList != null){
			for (UserGroupMemberPO userGroupMemberPO : userGroupMemberPOList){
				if(userGroupMemberPO.getUserGroupBo().contains(uppcLoginModule.getValue())){
					isHavePermission = true;
					break;
				}
			}
		}

		//验证用户名密码，通过则修改操作表用户，返回代码0，否则失败，返回代码 1
		if (isHavePermission == true ) {
			ResourceCurrentDataHandle handle = new ResourceCurrentDataHandle(voIn.getResourceBo());
			 ResourceCurrentDataPO po = resourceCurrentDataComponent.selectByHandle(handle.getValue());
			 if("meUser".equals(voIn.getType())){
				 po.setResourceMeStaff(voIn.getUserName());
				 voOut.setResourceMeStaff(voIn.getUserName());
			 }else{
				 po.setResourceOperator(voIn.getUserName());
				 voOut.setOperatorName(voIn.getUserName());
			 }
			 resourceCurrentDataComponent.updateByPrimaryKey(po);
			 voOut.setUserName(voIn.getUserName());
			 voOut.setCode("0");
		}else{
			voOut.setCode("1");
			voOut.setMsg("当前用户无操作权限");
		}
		return voOut;
	}

	public ResourceCurrentDataVO changeLocked(ResourceCurrentDataVO voIn) throws InvalidParamException {
		ResourceCurrentDataVO voOut = new ResourceCurrentDataVO();
		boolean isAuth = getUserAuth(voIn);
		// 验证用户名密码，通过则修改操作表用户，返回代码0，否则1代表用户名或密码错误，2状态无变化
		if ( isAuth == true || "meUser".equals(voIn.getType()) && "true".equals(voIn.getMeStaffLocked()) 
				|| "operator".equals(voIn.getType())&& "true".equals(voIn.getOperatorLocked())) {
			ResourceCurrentDataHandle handle = new ResourceCurrentDataHandle(voIn.getResourceBo());
			ResourceCurrentDataPO po = resourceCurrentDataComponent.selectByHandle(handle.getValue());
			if("meUser".equals(voIn.getType())){
				po.setMeStaffLocked(voIn.getMeStaffLocked()); 
				voOut.setMeStaffLocked(voIn.getMeStaffLocked());
			}else{
				po.setOperatorLocked(voIn.getOperatorLocked());
				voOut.setOperatorLocked(voIn.getOperatorLocked());
			}
			voOut.setCode("0");
			resourceCurrentDataComponent.updateByPrimaryKey(po);
		} else {
			voOut.setCode("1");
			voOut.setMsg("用户名或密码错误");
		}
		return voOut;
	}

	private boolean getUserAuth(ResourceCurrentDataVO voIn) {
		boolean isAuth = false;
		// 用户名密码不为空
		if (voIn.getUserName() != null && voIn.getPassWord() != null) {
			// 判断容器
			if (AppServerEnvironment.getAppServerType() == AppServerEnvironment.AppServerType.NETWEAVER) {
				NetWeaverUserAuthVO authVO = NetWeaverUserProvider.getUserAuth(voIn.getUserName(), voIn.getPassWord());
				if (authVO.isAuthenticated()) { // 通过验证返回true,否则false
					isAuth = true;
				}
			} else { // tomcat容器
				ResourceCurrentDataVO voLocal = ResourceCurrentDataVO.getResourceCurrentDataVO();
				// 通过验证返回true,否则false
				if (voIn.getUserName().equals(voLocal.getUserName())
						&& voIn.getPassWord().equals(voLocal.getPassWord())) {
					isAuth = true;
				}
			}
		}
		return isAuth;
	}

	public ResourceCurrentDataVO operatorClean(ResourceCurrentDataVO voIn) {
		ResourceCurrentDataVO voOut = new ResourceCurrentDataVO();
		ResourceCurrentDataHandle handle = new ResourceCurrentDataHandle(voIn.getResourceBo());
		ResourceTimeLogPO res = resourceTimeLogComponent.readLastRecord(voIn.getResourceBo());
		if( res != null){
			if("D".equals(res.getResourceState())){
				 ResourceCurrentDataPO po = resourceCurrentDataComponent.selectByHandle(handle.getValue());
				 if(po != null)
				 po.setItem("");
				 po.setResourceOperator("");
				 po.setOperatorLocked("false");
				 resourceCurrentDataComponent.updateByPrimaryKey(po);
				 voOut.setCode("0");
			}else {
				voOut.setCode("1");
				voOut.setMsg("非停机状态不允许注销");
			}
		}else{
		    voOut.setCode("3");
		    voOut.setMsg("该设备无活动日志记录");
		}
		return voOut;
	}

	public ResourceCurrentDataVO meUserClean(ResourceCurrentDataVO voIn) {
		ResourceCurrentDataVO voOut = new ResourceCurrentDataVO();
		ResourceCurrentDataHandle handle = new ResourceCurrentDataHandle(voIn.getResourceBo());
		ResourceTimeLogPO res = resourceTimeLogComponent.readLastRecord(voIn.getResourceBo());
		if( res != null && !"D".equals(res.getResourceState()) ){
			voOut.setCode("1");
			voOut.setMsg("非停机状态不允许注销");
			return voOut;
		}

		ResourceCurrentDataPO po = resourceCurrentDataComponent.selectByHandle(handle.getValue());
		if(po == null){
			voOut.setCode("1");
			voOut.setMsg("找不到设备操作日志");
			return voOut;
		}

		po.setResourceMeStaff("");
		po.setMeStaffLocked("false");
		resourceCurrentDataComponent.updateByPrimaryKey(po);
		voOut.setCode("0");

		return voOut;
	}

	 public int postResourceReasonLog(ResourceReasonLogRequest resourceReasonLogRequest) throws BusinessException {
	        int ret = 0;

	        for (ResourceReasonLogIn resourceReasonLogIn : resourceReasonLogRequest.getItems()) {
	            ResourceReasonLogVO vo = Utils.copyObjectProperties(resourceReasonLogIn, ResourceReasonLogVO.class);
	            if (Utils.isEmpty(resourceReasonLogIn.getActionCode())) {
	                throw new InvalidInputParamException("actionCode");
	            }

	            ResrceBOHandle resrceBOHandle = new ResrceBOHandle(resourceReasonLogIn.getSite(), resourceReasonLogIn.getResrce());
	            ResourcePO po = resourceComponent.getResourceByHandle(resrceBOHandle.getValue());

	            if (po == null) {
	                throw new InvalidResourceException();
	            }

	            if (!"true".equals(po.getConfiguredPlcInfo())) {
	                continue;
	            }
	            if(!Utils.isEmpty(resourceReasonLogIn.getMaintenanceNotification())||!Utils.isEmpty(resourceReasonLogIn.getMaintenanceOrder())){
	                List<ResourceReasonLogPO> pos = resourceReasonLogComponent.selectByExample(
	                        resourceReasonLogIn.getResrce(),
	                        resourceReasonLogIn.getReasonCode(),
	                        resourceReasonLogIn.getActionCode(),
	                        resourceReasonLogIn.getMaintenanceNotification(),
	                        resourceReasonLogIn.getMaintenanceOrder());
	                if (!Utils.isEmpty(pos)) {
	                    throw new ExistingRecordException("RESOURCE_REASON_LOG",gson.toJson(pos));
	                }else {
	                    vo.setDateTime(new Date());
	                    ret += resourceReasonLogComponent.createResourceReasonLog(vo);
	                }
	            }else {
	                vo.setDateTime(new Date());
	                ret += resourceReasonLogComponent.createResourceReasonLog(vo);
	            }
	        }

	        return ret;
	    }
		
	
}
