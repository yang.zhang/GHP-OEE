package mhp.oee.web.angular.client.resource.operator.management;

import javax.servlet.http.HttpServletRequest;

import mhp.oee.api.v1.PostClientLoginLogService;
import mhp.oee.api.v1.request.ClientLoginLogIn;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.api.v1.request.ResourceReasonLogRequest;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.InvalidParamException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ResourceCurrentDataComponent;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceCurrentDataVO;
import mhp.oee.web.Constants;

@RestController
public class ResourceOperatorController {

    @InjectableLogger
    private static Logger logger;
    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    
    @Autowired
    ResourceOperatorService resourceOperatorService;
    @Autowired
    ResourceCurrentDataComponent resourceCurrentDataComponent;

    @Autowired
    private PostClientLoginLogService postClientLoginLogService;

    
    @RequestMapping(value = "/web/rest/client/get_resource_operator_info", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<ResourceCurrentDataVO> getResourceOperatorInfo(HttpServletRequest request) {
        String pcIp=request.getRemoteHost();
        String trace = String.format("[ResourceOperatorController.getResourceOperatorInfo]");
        logger.debug(">> " + trace);
        ResourceCurrentDataVO vo = resourceOperatorService.getResourceOperatorInfo(pcIp);
        logger.debug("<< [ResourceOperatorController.getResourceOperatorInfo]  response : " + gson.toJson(vo));
        return ResponseEntity.ok(vo);
    }

    @RequestMapping(value = "/web/rest/client/resource_operator_info", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<ResourceCurrentDataVO> getResourceOperatorInfoByIp(HttpServletRequest request,
                                                                             @RequestParam("ip") String ip) {
        ResourceCurrentDataVO vo = null;
        try {

            if(StringUtils.isBlank(ip)){
                return ResponseEntity.ok(null);
            }

            vo = resourceOperatorService.getResourceOperatorInfoByPCIp(ip);
            return ResponseEntity.ok(vo);

        }catch (Exception e){
            logger.error(e.getMessage(),e);
            return ResponseEntity.ok(null);
        }finally {
            logger.debug("ResourceOperatorController.getResourceOperatorInfoByIp request [ip = " + ip + "]  response [" + gson.toJson(vo) + "]");
        }

    }
    
    //维护更换物料接口
    @RequestMapping(value = "/web/rest/client/site/{site}/change_item", method = RequestMethod.POST)
    public ResponseEntity<?> changeItem(@PathVariable("site") String site,@RequestBody ResourceCurrentDataVO vo) {
        String trace = String.format("[ResourceOperatorController.change_item] request : ResourceCurrentDataVO=%s ", gson.toJson(vo));
        logger.debug(">> " + trace);
        try {
        	resourceOperatorService.changeItem(vo);
        } catch (BusinessException e) {
            logger.error(trace + e.getMessage() + System.lineSeparator()+ Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< " + "[ResourceOperatorController.change_item]");
        return ResponseEntity.ok().body(null);
    }
    
    @RequestMapping(value = "/web/rest/client/change_operator_or_me_user", method = RequestMethod.POST)
    public ResponseEntity<ResourceCurrentDataVO> changeOperatorOrMeUser(@RequestBody ResourceCurrentDataVO voIn, HttpServletRequest request) {
    	
        String trace = String.format("[ResourceOperatorController.get_user_auth] request : " + gson.toJson(voIn));
        logger.debug(">> " + trace);
        ResourceCurrentDataVO voOut = resourceOperatorService.changeOperatorOrMeUser(voIn);
        logger.debug("<< [ResourceOperatorController.getResourceOperatorInfo]  r esponse : " + gson.toJson(voOut));
        return ResponseEntity.ok(voOut);
    }
    
    @RequestMapping(value = "/web/rest/client/change_locked", method = RequestMethod.POST)
    public ResponseEntity<ResourceCurrentDataVO> changeLocked(@RequestBody ResourceCurrentDataVO voIn, HttpServletRequest request) {
    	
        String trace = String.format("[ResourceOperatorController.get_user_auth] request : " + gson.toJson(voIn));
        logger.debug(">> " + trace);
        ResourceCurrentDataVO voOut = new ResourceCurrentDataVO();
		try {
			voOut = resourceOperatorService.changeLocked(voIn);
		} catch (InvalidParamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        logger.debug("<< [ResourceOperatorController.getResourceOperatorInfo]  r esponse : " + gson.toJson(voOut));
        return ResponseEntity.ok(voOut);
    }
    
    @RequestMapping(value = "/web/rest/client/operator_logout", method = RequestMethod.POST)
    public ResponseEntity<ResourceCurrentDataVO> operatorClean(@RequestBody ResourceCurrentDataVO voIn, HttpServletRequest request) {
    	
        String trace = String.format("[ResourceOperatorController.get_user_auth] request : " + gson.toJson(voIn));
        logger.debug(">> " + trace);
        ResourceCurrentDataVO voOut = resourceOperatorService.operatorClean(voIn);
        logger.debug("<< [ResourceOperatorController.getResourceOperatorInfo]  r esponse : " + gson.toJson(voOut));
        return ResponseEntity.ok(voOut);
    }
    
    @RequestMapping(value = "/web/rest/client/me_user_logout", method = RequestMethod.POST)
    public ResponseEntity<ResourceCurrentDataVO> meUserClean(@RequestBody ResourceCurrentDataVO voIn, HttpServletRequest request) {
    	
        String trace = String.format("[ResourceOperatorController.get_user_auth] request : " + gson.toJson(voIn));
        logger.debug(">> " + trace);
        ResourceCurrentDataVO voOut = resourceOperatorService.meUserClean(voIn);
        logger.debug("<< [ResourceOperatorController.getResourceOperatorInfo]  r esponse : " + gson.toJson(voOut));
        return ResponseEntity.ok(voOut);
    }
    
    /**
     * Post Resource Reason Log
     */
    @RequestMapping(value = "/web/rest/client/resource_reason_log", method = RequestMethod.POST)
    public ResponseEntity<?> postResourceReasonLog(@RequestBody ResourceReasonLogRequest resourceReasonLogRequest) {
        String trace = String.format("[web.rest.client][resource_reason_log] : %s", gson.toJson(resourceReasonLogRequest));
        logger.debug(">> " + trace);
        try {
        	resourceOperatorService.postResourceReasonLog(resourceReasonLogRequest);
        } catch (BusinessException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< " + trace);
        }
        return ResponseEntity.ok().body(null);
    }


    /**
     *
     */
    @RequestMapping(value = "/web/rest/client/post_client_login_log", method = RequestMethod.POST)
    public ResponseEntity<?> postClentLoginLog(@RequestBody ClientLoginLogIn clientLoginIn) {
        logger.debug(">> [ResourceOperatorController.postClentLoginLog] request = " + gson.toJson(clientLoginIn));
        try {
            postClientLoginLogService.postClientLoginLog(clientLoginIn);
        } catch (BusinessException e) {
            logger.error("## [ResourceOperatorController.postClentLoginLog] : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        } finally {
            logger.debug("<< [ResourceOperatorController.postClentLoginLog]");
        }
        return ResponseEntity.ok().body(null);
    }

}
