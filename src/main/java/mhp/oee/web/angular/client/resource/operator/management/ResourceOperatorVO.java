package mhp.oee.web.angular.client.resource.operator.management;


public class ResourceOperatorVO {
	
	private String userName;
	private String passWord;
	private String code;
	private String msg;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public static ResourceOperatorVO getResourceOperatorVO(){
		ResourceOperatorVO resourceOperatorVO = new ResourceOperatorVO();
		resourceOperatorVO.setUserName("00071858");
		resourceOperatorVO.setPassWord("111");
		return null;
		
	}
}
