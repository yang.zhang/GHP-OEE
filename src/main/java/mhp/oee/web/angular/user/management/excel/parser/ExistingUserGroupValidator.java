package mhp.oee.web.angular.user.management.excel.parser;

import mhp.oee.common.exception.ExistingException;
import mhp.oee.common.exception.NoSuchException;
import mhp.oee.common.logging.GenericLoggerFactory;
import mhp.oee.common.security.SpringContextHolder;
import mhp.oee.component.user.UserComponent;
import mhp.oee.component.user.UserGroupComponent;
import mhp.oee.po.gen.UserGroupPO;
import mhp.oee.web.angular.excel.parser.api.CellValidator;
import mhp.oee.web.angular.resource.plc.management.excel.parser.ExistingCellValidator;
import mhp.oee.web.angular.user.management.excel.parser.UserGroupCell.UserGroupIndex;

public class ExistingUserGroupValidator extends CellValidator {

    public ExistingUserGroupValidator(UserGroupCell cell) {
        this.cell = cell;
        this.cell.attach(this);
        logger = GenericLoggerFactory.getLogger(ExistingCellValidator.class);
    }

    @Override
    public void validator() {
        if (this.cell.getValue().trim().length() == 0) {
            return;
        }

        if (this.cell.getKey().equals(UserGroupIndex.User)) {
            UserComponent dao = (UserComponent) SpringContextHolder.getBean("userComponent");
            if (!dao.readAppServerUserDetailsWithoutDummy(cell.getValue())) {
                this.cell.addException(new NoSuchException(" netweaver user "));
            }

            return;
        }

        if (this.cell.getKey().equals(UserGroupIndex.UserGroup)) {
            UserGroupComponent dao = (UserGroupComponent) SpringContextHolder.getBean("userGroupComponent");
            UserGroupPO userGroup = dao.existUserGroup(cell.getSite(), cell.getValue());
            if (userGroup == null) {
                this.cell.addException(new ExistingException());
            }
            return;
        }

    }
}