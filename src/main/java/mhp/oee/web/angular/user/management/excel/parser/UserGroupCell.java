package mhp.oee.web.angular.user.management.excel.parser;

import java.util.List;

import mhp.oee.web.angular.excel.parser.api.CustomerCell;
import mhp.oee.web.angular.excel.parser.api.CustomerIndex;
import mhp.oee.web.angular.excel.parser.api.MandatoryCellValidator;

public class UserGroupCell extends CustomerCell {

    public UserGroupCell(UserGroupIndex key, String value, String site) {
        super(key, value, site);
    }

    public static List<CustomerCell> initCellAttribute(List<CustomerCell> list, String site) {

        UserGroupCell userCell = new UserGroupCell(UserGroupIndex.User, "", site);
        new MandatoryCellValidator(userCell);
        new ExistingUserGroupValidator(userCell);
        list.add(UserGroupIndex.User.getIndex(), userCell);

        UserGroupCell userGroupCell = new UserGroupCell(UserGroupIndex.UserGroup, "", site);
        new MandatoryCellValidator(userGroupCell);
        new ExistingUserGroupValidator(userGroupCell);
        list.add(UserGroupIndex.UserGroup.getIndex(), userGroupCell);

        return list;
    }

    public enum UserGroupIndex implements CustomerIndex {
        User(0, "user"),
        UserGroup(1, "userGroup");

        private Integer index;
        private String value;

        private UserGroupIndex(Integer index, String value) {
            this.index = index;
            this.value = value;
        }

        @Override
        public String toString() {
            return this.index.toString();
        }

        @Override
        public Integer getIndex() {
            return this.index;
        }

        @Override
        public String getValue() {
            return this.value;
        }
    }

}