package mhp.oee.web.angular.user.management;

import java.util.ArrayList;
import java.util.List;

import mhp.oee.web.angular.excel.parser.api.JsonRow;
import mhp.oee.web.angular.excel.parser.api.SuccessUpload;

public class UserGroupUploadVO {
    private List<JsonRow> exceptionRowArray = new ArrayList<JsonRow>();
    private SuccessUpload successUpload = new SuccessUpload(null);

    public SuccessUpload getSuccessUpload() {
        return successUpload;
    }

    public void setSuccessUpload(SuccessUpload successUpload) {
        this.successUpload = successUpload;
    }

    public List<JsonRow> getExceptionRowArray() {
        return exceptionRowArray;
    }

    public void setExceptionRowArray(List<JsonRow> exceptionRowArray) {
        this.exceptionRowArray = exceptionRowArray;
    }
}
