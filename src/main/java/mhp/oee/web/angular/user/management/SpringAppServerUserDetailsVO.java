package mhp.oee.web.angular.user.management;

import java.util.List;

import mhp.oee.po.gen.UserGroupMemberPO;
import mhp.oee.po.gen.UserGroupPO;
import mhp.oee.po.gen.UserPO;
import mhp.oee.vo.AppServerUserDetailsVO;

public class SpringAppServerUserDetailsVO {


    private AppServerUserDetailsVO serverUserDetailsVO;

    private String message;
    // user handle
    private UserPO userPO;
    private List<UserGroupMemberPO> originUserGroupMember;
    private List<UserGroupPO> updateUserGroup;

    // user gourp

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public AppServerUserDetailsVO getServerUserDetailsVO() {
        return serverUserDetailsVO;
    }

    public void setServerUserDetailsVO(AppServerUserDetailsVO serverUserDetailsVO) {
        this.serverUserDetailsVO = serverUserDetailsVO;
    }

    public UserPO getUserPO() {
        return userPO;
    }

    public void setUserPO(UserPO userPO) {
        this.userPO = userPO;
    }

    public List<UserGroupMemberPO> getOriginUserGroupMember() {
        return originUserGroupMember;
    }

    public void setOriginUserGroupMember(List<UserGroupMemberPO> originUserGroupMember) {
        this.originUserGroupMember = originUserGroupMember;
    }

    public List<UserGroupPO> getUpdateUserGroup() {
        return updateUserGroup;
    }

    public void setUpdateUserGroup(List<UserGroupPO> updateUserGroup) {
        this.updateUserGroup = updateUserGroup;
    }

}
