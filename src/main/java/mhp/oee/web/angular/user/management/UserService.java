package mhp.oee.web.angular.user.management;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.user.UserComponent;
import mhp.oee.component.user.UserGroupComponent;
import mhp.oee.po.gen.UserGroupMemberPO;
import mhp.oee.po.gen.UserGroupPO;
import mhp.oee.po.gen.UserPO;
import mhp.oee.vo.AppServerUserDetailsVO;
import mhp.oee.web.Constants;
import mhp.oee.web.angular.excel.parser.api.CustomerRow;
import mhp.oee.web.angular.excel.parser.api.JsonRow;
import mhp.oee.web.angular.user.management.excel.parser.UserGroupCell.UserGroupIndex;
import mhp.oee.web.angular.user.management.excel.parser.UserGroupRow;
import mhp.oee.web.angular.user.management.excel.parser.UserGroupRow.UserAndUserGroupVO;

@Service
@Transactional(rollbackFor = {BusinessException.class})
public class UserService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    UserComponent userComponent;

    @Autowired
    UserGroupComponent userGroupComponent;


    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    public SpringAppServerUserDetailsVO searchUserById(String site, String userId) {
        SpringAppServerUserDetailsVO vo = new SpringAppServerUserDetailsVO();
        AppServerUserDetailsVO readAppServerUserDetails = userComponent.readAppServerUserDetails(userId);
        if (readAppServerUserDetails == null) {
            vo.setMessage("No such user");
        } else {
            vo.setMessage("OK");
            vo.setServerUserDetailsVO(readAppServerUserDetails);
            vo.setUserPO(userComponent.getUserDetails(site, userId));
            vo.setOriginUserGroupMember(userComponent.getUserGroupMemberPOList(site, userId));
        }

        return vo;
    }

    public List<UserGroupPO> getUserGroups(String site) {
        return userGroupComponent.getUserGroups(site);
    }

    public void changeUserGroup(SpringAppServerUserDetailsVO vo, String site, String userId) throws BusinessException {
        UserPO user = vo.getUserPO();
        logger.debug(">>>>   " + "start to change user group"  +"     <<<<<");
        if(user == null) {
            user = new UserPO();
            user.setSite(site);
            user.setEnabled("true");
            user.setUserId(userId);
            userComponent.createUserDetails(user);
        } else {
            userComponent.updateUserDetails(user);
        }

        logger.debug(">>>>   " + "user create / update successfull"  +"     <<<<<");


        List<UserGroupMemberPO> originUserGroupMember = vo.getOriginUserGroupMember();
        if(originUserGroupMember != null && originUserGroupMember.size() != 0) {
            userComponent.deleteOriginUserGroups(originUserGroupMember);
        }
        logger.debug(">>>>   " + "user group member delete successfull"  +"     <<<<<");


        List<UserGroupPO> updatedUserGroup = vo.getUpdateUserGroup();
        if(updatedUserGroup != null && updatedUserGroup.size() != 0) {
            userComponent.createUserGroupMember(user, updatedUserGroup);
        }
        logger.debug(">>>>   " + "user group member create successfull"  +"     <<<<<");

    }


    @SuppressWarnings("deprecation")
    public UserGroupUploadVO processUploadFile(MultipartFile multiFile, String site)
            throws IOException, BusinessException, ParseException {
        XSSFWorkbook workbook = new XSSFWorkbook(multiFile.getInputStream());
        XSSFSheet sheet = workbook.getSheetAt(0);

        List<CustomerRow> rowArray = new ArrayList<CustomerRow>();
        for (int rowNumber = Constants.USERGROUP_IGNORED_NUM; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            Row row = sheet.getRow(rowNumber);
            CustomerRow customerRow = new UserGroupRow(site);
            for (int cellNumber = 0; cellNumber < Constants.USERGROUP_UPLOAD_LENGTH; cellNumber++) {
                if (row != null && row.getCell(cellNumber) != null) {
                    Cell cell = row.getCell(cellNumber);
                    switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_NUMERIC:
                        if (cellNumber == UserGroupIndex.User.getIndex()) {
                            customerRow.setCellValue(cellNumber, rowNumber,
                                    Integer.valueOf((int)cell.getNumericCellValue()).toString());
                        }
                        break;
                    case Cell.CELL_TYPE_STRING:
                        customerRow.setCellValue(cellNumber, rowNumber, cell.getStringCellValue().toUpperCase());
                        break;
                    case Cell.CELL_TYPE_BLANK:
                        customerRow.setCellValue(cellNumber, rowNumber, cell.getStringCellValue());
                        break;
                    }
                } else {
                    customerRow.setCellValue(cellNumber, rowNumber, "");
                }

            }
            rowArray.add(customerRow);
        }
        workbook.close();

        List<CustomerRow> notEmptyRowArray = new ArrayList<CustomerRow>();
        for (CustomerRow row : rowArray) {
            if(!row.isEmptyRow()) {
                notEmptyRowArray.add(row);
            }
        }

        for (CustomerRow row : notEmptyRowArray) {
            logger.debug(">>>>>>>>>>> excel rows    "+row.toString());
        }

        UserGroupUploadVO vo = new UserGroupUploadVO();

        for (CustomerRow row : notEmptyRowArray) {
            JsonRow jsonRow = row.generateRowError();
            if (jsonRow != null) {
                vo.getExceptionRowArray().add(row.generateRowError());
            }
        }

        // if no exception, then store data into database
        if (vo.getExceptionRowArray().size() == 0) {
            for (CustomerRow row : notEmptyRowArray) {
                UserAndUserGroupVO uploadVO = (UserAndUserGroupVO) row.getVO(site);
                this.createVO(uploadVO);
            }
            vo.getSuccessUpload().setCommitNumber(notEmptyRowArray.size());
        }

        return vo;
    }

    private void createVO(UserAndUserGroupVO uploadVO) {
        try {
            UserPO userPO = uploadVO.getUserPO();
            userComponent.createUserDetailsByPO(userPO);
        } catch (ExistingRecordException e) {
            logger.error("existing record for upload user:    " + e.getErrorJson());
        }

        try {
            UserGroupMemberPO userGroupMemberPO = uploadVO.getUserGroupMemberPO();
            userGroupComponent.createUserGroupMemberByPO(userGroupMemberPO);
        } catch (ExistingRecordException e) {
            logger.error("existing record for upload userGroup:    " + e.getErrorJson());
        }
    }

}
