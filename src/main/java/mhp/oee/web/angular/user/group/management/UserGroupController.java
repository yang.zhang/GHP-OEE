package mhp.oee.web.angular.user.group.management;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.gen.UserGroupPO;
import mhp.oee.po.gen.UserPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ActivityPermAndActivityVO;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ActivityClientPermAndActivityClientVO;
import mhp.oee.vo.ActivityClientVO;
import mhp.oee.vo.UserGroupMemberVO;
import mhp.oee.vo.UserGroupVO;
import mhp.oee.web.Constants;

@RestController
public class UserGroupController {

    @InjectableLogger
    private static Logger logger;
    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    UserGroupService userGroupService;

    @RequestMapping(value = "/web/rest/plant/site/{site}/user_group_help",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<UserGroupPO>> getUserGroupsLikeGroup(
            @PathVariable("site") String site, 
            @RequestParam(value = "user_group", required = false) String userGroup) {
        String trace = String.format("[UserGroupController.getUserGroupsLikeGroup] request : site=%s, userGroup=%s",
                site, userGroup);
        logger.debug(">> " + trace);
        List<UserGroupPO> vos = userGroupService.getUserGroupsLikeGroup(site, userGroup);
        logger.debug("<< [UserGroupController.getUserGroupsLikeGroup] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/user_group",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<UserGroupPO>> readUserGroupByGroup(
            @PathVariable("site") String site, 
            @RequestParam(value = "user_group", required = true) String userGroup) {
        String trace = String.format("[UserGroupController.readUserGroupByGroup] request : site=%s, userGroup=%s",
                site, userGroup);
        logger.debug(">> " + trace);
        List<UserGroupPO> vos = userGroupService.readUserGroupByGroup(site, userGroup);
        logger.debug("<< [UserGroupController.readUserGroupByGroup] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/site/{site}/user_all",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<UserPO>> readAllUser(@PathVariable("site") String site) {
        String trace = String.format("[UserGroupController.readAllUser] request : site=%s", site);
        logger.debug(">> " + trace);
        List<UserPO> vos = userGroupService.readAllUser(site);
        logger.debug("<< [UserGroupController.readAllUser] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/site/{site}/user_used",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<UserPO>> getUserByUserGroup(
            @PathVariable("site") String site,
            @RequestParam(value = "user_group", required = true) String userGroup) {
        String trace = String.format("[UserGroupController.getUserByUserGroup] request : site=%s, userGroup=%s",
                site, userGroup);
        logger.debug(">> " + trace);
        List<UserPO> vos = userGroupService.getUserByUserGroup(site, userGroup);
        logger.debug("<< [UserGroupController.getUserByUserGroup] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/change_user_group",
            method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeUserGroup(@RequestBody List<UserGroupVO> vos) {
        String trace = String.format("[UserGroupController.changeUserGroup] request : " + gson.toJson(vos));
        logger.debug(">> " + trace);
        try {
            userGroupService.changeUserGroup(vos);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("UserGroupController.changeUserGroup() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [UserGroupController.changeUserGroup] response : void");
        return ResponseEntity.ok().body(null);
    }
    
    @RequestMapping(value = "/web/rest/plant/change_user_group_member",
            method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeUserGroupMember(@RequestBody List<UserGroupMemberVO> vos) {
        String trace = String.format("[UserGroupController.changeUserGroupMember] request : " + gson.toJson(vos));
        logger.debug(">> " + trace);
        try {
            userGroupService.changeUserGroupMember(vos);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("UserGroupController.changeUserGroupMember() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [UserGroupController.changeUserGroupMember] response : void");
        return ResponseEntity.ok().body(null);
    }
    
    @RequestMapping(value = "/web/rest/plant/{site}/activity_perm",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityPermAndActivityVO>> getActivityPermByUserGroup(
            @PathVariable("site") String site,
            @RequestParam(value = "user_group", required = true) String userGroup) {
        String trace = String.format("[UserGroupController.getActivityPermByUserGroup] request : site=%s, userGroup=%s",
                site, userGroup);
        logger.debug(">> " + trace);
        List<ActivityPermAndActivityVO>  vos = userGroupService.getActivityPermByUserGroup(site, userGroup);
        logger.debug("<< [UserGroupController.getActivityPermByUserGroup] response : "  + gson.toJson(vos));
        return ResponseEntity.ok().body(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/{site}/activity_client_perm",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityClientPermAndActivityClientVO>> getActivityClientPermByUserGroup(
            @PathVariable("site") String site,
            @RequestParam(value = "user_group", required = true) String userGroup) {
        String trace = String.format("[UserGroupController.getActivityClientPermByUserGroup] request : site=%s, userGroup=%s",
                site, userGroup);
        logger.debug(">> " + trace);
        List<ActivityClientPermAndActivityClientVO>  vos = userGroupService.getActivityClientPermByUserGroup(site, userGroup);
        logger.debug("<< [UserGroupController.getActivityClientPermByUserGroup] response : "  + gson.toJson(vos));
        return ResponseEntity.ok().body(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/{site}/activity_client_all",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityClientVO> > getAllActivityClient(
            @PathVariable("site") String site) {
        String trace = String.format("[UserGroupController.getAllActivityClient] request : ");
        logger.debug(">> " + trace);
        List<ActivityClientVO> vos = userGroupService.getAllActivityClient();
        logger.debug("<< [UserGroupController.getAllActivityClient] response : "  + gson.toJson(vos));
        return ResponseEntity.ok().body(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/change_activity_perm",
            method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeActivityPerm(@RequestBody List<ActivityPermAndActivityVO> vos) {
        String trace = String.format("[UserGroupController.changeActivityPerm] request : " + gson.toJson(vos));
        logger.debug(">> " + trace);
        try {
            userGroupService.changeActivityPerm(vos);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("UserGroupController.changeActivityPerm() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [UserGroupController.changeActivityPerm] response : void");
        return ResponseEntity.ok().body(null);
    }
    
    @RequestMapping(value = "/web/rest/plant/change_activity_client_perm",
            method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeActivityClientPerm(@RequestBody List<ActivityClientPermAndActivityClientVO> vos) {
        String trace = String.format("[UserGroupController.changeActivityClientPerm] request : " + gson.toJson(vos));
        logger.debug(">> " + trace);
        try {
            userGroupService.changeActivityClientPerm(vos);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("UserGroupController.changeActivityClientPerm() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [UserGroupController.changeActivityClientPerm] response : void");
        return ResponseEntity.ok().body(null);
    }
    
}
