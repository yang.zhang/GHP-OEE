package mhp.oee.web.angular.user.management.excel.parser;

import java.util.Date;

import mhp.oee.common.handle.UserBOHandle;
import mhp.oee.common.handle.UserGroupMemberBOHandle;
import mhp.oee.common.security.SpringContextHolder;
import mhp.oee.component.user.UserGroupComponent;
import mhp.oee.po.gen.UserGroupMemberPO;
import mhp.oee.po.gen.UserGroupPO;
import mhp.oee.po.gen.UserPO;
import mhp.oee.web.angular.excel.parser.api.CustomerRow;
import mhp.oee.web.angular.user.management.excel.parser.UserGroupCell.UserGroupIndex;

public class UserGroupRow extends CustomerRow {

    public UserGroupRow(String site) {
        UserGroupCell.initCellAttribute(this.getCellArray(), site);
    }

    @Override
    public Object getVO(String site) {
        UserAndUserGroupVO vo = new UserAndUserGroupVO();

        //set user
        UserPO userPO = new UserPO();
        userPO.setUserId(this.getValue(UserGroupIndex.User));
        userPO.setSite(site);
        userPO.setEnabled("true");
        userPO.setEnabledModifiedDateTime(new Date());


        String userHandle = (new UserBOHandle(site, userPO.getUserId())).getValue();
        userPO.setHandle(userHandle);
        vo.setUserPO(userPO);

        //set group member
        UserGroupPO userGroup = getUserGroup(site, this.getValue(UserGroupIndex.UserGroup));



        UserGroupMemberPO userGroupMemberPO = new UserGroupMemberPO();
        userGroupMemberPO.setUserBo(userPO.getHandle());
        userGroupMemberPO.setUserGroupBo(userGroup.getHandle());
        String userGroupMemberHandle = new UserGroupMemberBOHandle(userGroup.getHandle(), userPO.getHandle()).getValue();

        userGroupMemberPO.setHandle(userGroupMemberHandle);
        vo.setUserGroupMemberPO(userGroupMemberPO);

        return vo;
    }

    private UserGroupPO getUserGroup(String site, String userGroup){
        UserGroupComponent dao = (UserGroupComponent)SpringContextHolder.getBean("userGroupComponent");
        UserGroupPO userGroupPO = dao.existUserGroup(site, userGroup);

        return userGroupPO;
    }





    public class UserAndUserGroupVO {
        private UserPO  userPO;
        private UserGroupMemberPO userGroupMemberPO;

        public UserPO getUserPO() {
            return userPO;
        }
        public void setUserPO(UserPO userPO) {
            this.userPO = userPO;
        }
        public UserGroupMemberPO getUserGroupMemberPO() {
            return userGroupMemberPO;
        }
        public void setUserGroupMemberPO(UserGroupMemberPO userGroupMemberPO) {
            this.userGroupMemberPO = userGroupMemberPO;
        }
    }



}