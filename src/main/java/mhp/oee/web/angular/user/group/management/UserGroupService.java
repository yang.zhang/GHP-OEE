package mhp.oee.web.angular.user.group.management;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.UserBOHandle;
import mhp.oee.common.handle.UserGroupBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.user.UserGroupComponent;
import mhp.oee.po.extend.ActivityAndActivityGroupAndActivityGroupActivityPO;
import mhp.oee.po.gen.ActivityClientPermPO;
import mhp.oee.po.gen.UserGroupMemberPO;
import mhp.oee.po.gen.UserGroupPO;
import mhp.oee.po.gen.UserPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ActivityPermAndActivityVO;
import mhp.oee.vo.ActivityClientPermAndActivityClientVO;
import mhp.oee.vo.ActivityClientVO;
import mhp.oee.vo.UserGroupMemberVO;
import mhp.oee.vo.UserGroupVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class UserGroupService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    UserGroupComponent userGroupComponent;
    
    public List<UserGroupPO> getUserGroupsLikeGroup(String site, String userGroup){
        List<UserGroupPO> vos = userGroupComponent.getUserGroupsLikeGroup(site, userGroup);
        return vos;
    }
    
    public List<UserGroupPO> readUserGroupByGroup(String site, String userGroup){
        List<UserGroupPO> vos = userGroupComponent.getUserGroupsByGroup(site, userGroup);
        return vos;
    }
    
    public List<UserPO> readAllUser(String site) {
        return userGroupComponent.getAllUser(site);
    }
    
    public List<UserPO> getUserByUserGroup(String site, String userGroup) {
        List<UserPO> vos = new ArrayList<UserPO>();
        List<UserGroupMemberPO> pos = userGroupComponent.getUserByUserGroup(site, userGroup);
        for (UserGroupMemberPO po : pos) {
            UserBOHandle boHandle = new UserBOHandle(po.getUserBo());
            UserPO userPO = new UserPO();
            userPO.setHandle(po.getUserBo());
            userPO.setUserId(boHandle.getUser());
            vos.add(userPO);
        }
        return vos;
    }
    
    public void changeUserGroupMember(List<UserGroupMemberVO> vos) throws BusinessException {
        for (UserGroupMemberVO each : vos) {
            switch (each.getViewActionCode()) {
            case 'C':
                userGroupComponent.createUserGroupMember(each);
                break;
            case 'D':
                userGroupComponent.deleteUserGroupMember(each);
                break;
            default:
                logger.error("UserGroupService.cudchangeUserGroup() : Invalid viewActionCode '" + each.getViewActionCode()
                        + "'.");
                break;
            }
        }
    }
    
    public void changeUserGroup(List<UserGroupVO> vos) throws BusinessException {
        for (UserGroupVO each : vos) {
            switch (each.getViewActionCode()) {
            case 'C':
                userGroupComponent.createUserGroup(each);
                break;
            case 'U':
                userGroupComponent.updateUserGroup(each);
                break;
            default:
                logger.error("UserGroupService.cudchangeUserGroup() : Invalid viewActionCode '" + each.getViewActionCode()
                        + "'.");
                break;
            }
        }
    }
    
    public List<ActivityPermAndActivityVO> getActivityPermByUserGroup(String site, String userGroup) {
        List<ActivityPermAndActivityVO> vos = new ArrayList<ActivityPermAndActivityVO>();
        UserGroupBOHandle handle = new UserGroupBOHandle(site, userGroup);
        List<ActivityAndActivityGroupAndActivityGroupActivityPO> pos = userGroupComponent.getAllActivityGroupAndActivity();
        for (ActivityAndActivityGroupAndActivityGroupActivityPO po : pos) {
            ActivityPermAndActivityVO  vo = userGroupComponent.getActivityPermByUserGroup(handle.getValue(), po.getActivityBo());
            if (vo == null) {
                vo = new ActivityPermAndActivityVO();
            }
            vo.setUserGroupBo(handle.getValue());
            vo.setActivityGroup(po.getActivityGroup());
            vo.setActivityGroupDes(po.getDescription());
            vo.setActivity(po.getActivity());
            vo.setActivityBo(po.getActivityBo());
            vo.setActivityDes(po.getActivityDes());
            vos.add(vo);
        }
        return vos;
    }
    
    public List<ActivityClientPermAndActivityClientVO> getActivityClientPermByUserGroup(String site, String userGroup) {
        UserGroupBOHandle handle = new UserGroupBOHandle(site, userGroup);
        return userGroupComponent.getActivityClientPermByUserGroup(handle.getValue());
    }
    
    public List<ActivityClientVO> getAllActivityClient() {
        List<ActivityClientVO> vos = userGroupComponent.getAllActivityClient();
        if (!Utils.isEmpty(vos)) {
            for (ActivityClientVO vo : vos) {
                vo.setActivityClientBo(vo.getHandle());
            }  
        }
        return vos;
    }
    
    public void changeActivityPerm(List<ActivityPermAndActivityVO> vos) throws BusinessException {
        for (ActivityPermAndActivityVO each : vos) {
            switch (each.getViewActionCode()) {
            case 'C':
                userGroupComponent.createActivityPerm(each);
                break;
            case 'U':
                userGroupComponent.updateActivityPerm(each);
                break;
            default:
                logger.error("UserGroupService.cudChangeActivityPerm() : Invalid viewActionCode '" + each.getViewActionCode() + "'.");
                break;
            }
        }
    }
    
    public void changeActivityClientPerm(List<ActivityClientPermAndActivityClientVO> vos) throws BusinessException {
        for (ActivityClientPermAndActivityClientVO each : vos) {
            switch (each.getViewActionCode()) {
            case 'C':
                ActivityClientPermPO po = userGroupComponent.exsitActivityClientPermPOMapper(each);
                if (po == null) {
                    userGroupComponent.createActivityClientPerm(each);
                } else {
                    po.setPermissionSetting("true");
                    userGroupComponent.updateActivityClientPerm(Utils.copyObjectProperties(po, ActivityClientPermAndActivityClientVO.class));
                }
                break;
            case 'U':
                userGroupComponent.updateActivityClientPerm(each);
                break;
            default:
                logger.error("UserGroupService.cudChangeActivityClientPerm() : Invalid viewActionCode '" + each.getViewActionCode() + "'.");
                break;
            }
        }
    }
    
}
