package mhp.oee.web.angular.user.management;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.gen.UserGroupPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.web.Constants;

@RestController
public class RestUserController {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    UserService userService;

    @RequestMapping(value = "/web/rest/user/management/site/{site}/search", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<SpringAppServerUserDetailsVO> searchUser(@PathVariable("site") String site,
            @RequestParam(value = "userId", required = true) String userId, HttpServletResponse res,
            HttpServletRequest request) {

        String trace = String.format("[search][site/%s/users] : userId=%s", site, userId);
        logger.debug(">> " + trace);
        SpringAppServerUserDetailsVO vo = userService.searchUserById(site, userId);
        logger.debug("<< " + trace + " : " + gson.toJson(vo));
        return ResponseEntity.ok().body(vo);
    }

    @RequestMapping(value = "/web/rest/user/management/site/{site}/usergroups", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<UserGroupPO>> getUserGroups(@PathVariable("site") String site, HttpServletResponse res,
            HttpServletRequest request) {

        String trace = String.format("[search][site/%s/users]", site);
        logger.debug(">> " + trace);
        List<UserGroupPO> list = userService.getUserGroups(site);
        logger.debug("<< " + trace + " : " + gson.toJson(list));
        return ResponseEntity.ok().body(list);
    }

    @RequestMapping(value = "/web/rest/user/management/site/{site}/{userId}/update_usergroup", method = RequestMethod.POST)
    public ResponseEntity<?> changeUserGroup(@PathVariable("site") String site, @RequestBody SpringAppServerUserDetailsVO vObj,
            @PathVariable("userId") String userId) {
        String trace = String.format("[update user group][site/%s/users] : userId=%s", site, userId);
        logger.debug(">> " + trace);
        try {
            this.userService.changeUserGroup(vObj, site, userId);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("RestUserController.changeUserGroup : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(e.getMessage())).body(null);
        }
        return ResponseEntity.ok().body(null);
    }

    @RequestMapping(value = "/web/rest/user/management/site/{site}/upload_file", method = RequestMethod.POST)
    public ResponseEntity<?> processUpload(@RequestParam(value = "file") MultipartFile file,
            @PathVariable("site") String site, HttpServletResponse res, HttpServletRequest request)
            throws ParseException {
        try {
            logger.debug("file upload " + file.getOriginalFilename());

            Utils.checkUploadFilename(file.getOriginalFilename());

            UserGroupUploadVO vo = this.userService.processUploadFile(file, site);
            logger.debug(">>>>>> " + " : " + gson.toJson(vo));

            res.setHeader("Content-Type", "application/json");
            res.setStatus(HttpServletResponse.SC_ACCEPTED);
            return ResponseEntity.ok(vo);
        } catch (IOException e) {
            logger.error("ppmUpload() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            res.setHeader("Content-Type", "application/json");
            res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }  catch (BusinessException e) {
            return Utils.returnExceptionToFrontEnd(e);
        }
    }



    @RequestMapping(value = "/download/user/management/site/{site}/excel", method = RequestMethod.GET)
    public ResponseEntity<?> exportExcel(@PathVariable("site") String site, HttpServletResponse response,
            HttpServletRequest request) {

     try {

            String filePath = request.getSession().getServletContext().getRealPath("/");
            filePath = filePath + "/user.xlsx";
            Utils.generateDownloadFile(response, new File(filePath));

     } catch (IOException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
            logger.error("alertExportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
     }

     return ResponseEntity.ok(null);

    }

}
