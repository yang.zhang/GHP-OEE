package mhp.oee.web.angular.resource.plc.management.excel.parser;

import mhp.oee.vo.ResourcePlcVO;
import mhp.oee.web.angular.excel.parser.api.CustomerRow;
import mhp.oee.web.angular.resource.plc.management.excel.parser.ResourcePlcCell.ResourcePlcIndex;


public class ResourcePlcRow extends CustomerRow {


    public ResourcePlcRow(String site) {
        ResourcePlcCell.initCellAttribute(this.getCellArray(), site);
    }

    @Override
    public Object getVO(String site) {
        ResourcePlcVO vo = new ResourcePlcVO();
        vo.setSite(site);
        vo.setResrce(this.getValue(ResourcePlcIndex.DeviceNo));
        vo.setPlcAddress(this.getValue(ResourcePlcIndex.Address));
        vo.setPlcValue(this.getValue(ResourcePlcIndex.Reg));
        vo.setCategory(this.getValue(ResourcePlcIndex.Category));
        vo.setPlcObject(this.getValue(ResourcePlcIndex.Object));
        vo.setDescription(this.getValue(ResourcePlcIndex.Des));
        vo.setLogPath(this.getValue(ResourcePlcIndex.Path));
        return vo;
    }
}