package mhp.oee.web.angular.resource.plc.management;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mhp.oee.reporting.oee.OeeWorkbookCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.DataCodeException;
import mhp.oee.common.exception.DataDescriptionException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.InvalidParamException;
import mhp.oee.common.exception.NotFoundExistingRecordException;
import mhp.oee.common.exception.PLCRecordException;
import mhp.oee.common.exception.RegisterationException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.component.plant.ResourcePlcComponent;
import mhp.oee.po.extend.ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourcePlcPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourcePlcVO;
import mhp.oee.vo.ResourceVO;
import mhp.oee.vo.ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO;
import mhp.oee.web.Constants;
import mhp.oee.web.angular.excel.parser.api.CustomerRow;
import mhp.oee.web.angular.excel.parser.api.JsonRow;
import mhp.oee.web.angular.plc.accesstype.management.PlcAddressUploadVO;
import mhp.oee.web.angular.resource.plc.management.excel.parser.RelationCellValidator.CategoryCode;
import mhp.oee.web.angular.resource.plc.management.excel.parser.ResourcePlcRow;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class PlcService {

    @InjectableLogger
    private static Logger logger;
    private static final String CONFIGURED_PLC_INFO_Y = "true";
    private static final String CONFIGURED_PLC_INFO_N = "false";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    private ResourcePlcComponent resourcePlcComponent;

    @Autowired
    ResourceComponent resourceComponent;

    public List<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO> readResourcePlc(String site, String resource, String plcCategory, Integer limit, Integer offSet) {
    	List<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPO> po = resourcePlcComponent.selectViewResrceAndCategoryAndObj(site, resource, plcCategory, limit, offSet);
        for (ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPO poc : po) {
        	if (Utils.isEmpty(poc.getPlcObject())){
        		poc.setPlcObjectDes(poc.getDescription());
        	}
		}
    	return Utils.copyListProperties(po, ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO.class);
    }

    public int readResourcePlcCount(String site, String resource, String plcCategory) {
    	int result= resourcePlcComponent.selectViewResrceAndCategoryAndObjCount(site, resource, plcCategory);
    	return result;
    }

    public boolean existResourcePlcByHandle(ResourcePlcVO resourcePlcVO){
        if(resourcePlcComponent.existResourcePlcByHandle(resourcePlcVO) != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean existResourcePlcByResrceAndObj(ResourcePlcVO resourcePlcVO){
         List<ResourcePlcPO> list = resourcePlcComponent.existResourcePlcByResrceAndObj(resourcePlcVO);
         if(list != null && list.size() > 0){
             return true;
         } else {
             return false;
         }
    }

    public boolean existPlcObject(ResourcePlcVO resourcePlcVO){
    	boolean existPlcObject = resourcePlcComponent.existPlcObject(resourcePlcVO);
        return existPlcObject;
    }

    public void changeResourcePlc(List<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO> vos) throws ExistingRecordException, UpdateErrorException, InvalidParamException, RegisterationException, DataCodeException, DataDescriptionException, PLCRecordException {
        Collections.sort(vos);
        Set<ResourceVO> createList = new HashSet<ResourceVO>();
        Set<ResourceVO> deleteList = new HashSet<ResourceVO>();
        /*List<String> sss = new ArrayList<String>();
        for (int i=0;i<vos.size();i++) {
            String handle ="ResourcePlcBO"+ vos.get(i).getSite()+","+vos.get(i).getResourceBo()+","+vos.get(i).getPlcAddress()+","+vos.get(i).getPlcValue();
            if(sss.contains(handle)){
                vos.remove(i);
                continue;
            }
            sss.add(handle);
        }*/
        for (ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO each : vos) {
            checkParam(each);
            switch (each.getViewActionCode()) {
            case 'C':
                createList.add(Utils.copyObjectProperties(each.getResourcePO(), ResourceVO.class));
                resourcePlcComponent.createResourcePlc(Utils.copyObjectProperties(each, ResourcePlcVO.class));
                break;
            case 'U':
                resourcePlcComponent.updateResourcePlc(Utils.copyObjectProperties(each, ResourcePlcVO.class));
                break;
            case 'D':
                deleteList.add(Utils.copyObjectProperties(each.getResourcePO(), ResourceVO.class));
                resourcePlcComponent.deleteAccessType(Utils.copyObjectProperties(each, ResourcePlcVO.class));
                break;
            default:
                logger.error("PlcService.cudChangeResourcePlc() : Invalid viewActionCode '" + each.getViewActionCode()
                        + "'.");
                break;
            }
        }
        // change Resrce
        changeResrce(createList, deleteList);
    }

    private void changeResrce(Set<ResourceVO> createList, Set<ResourceVO> deleteList) throws UpdateErrorException {
        if (createList.size() > 0) {
            for (ResourceVO resourceVo : createList) {
                resourceVo.setConfiguredPlcInfo(CONFIGURED_PLC_INFO_Y);
                resourcePlcComponent.updateResrce(resourceVo);
            }
        }
        if (deleteList.size() > 0) {
            for (ResourceVO resourceVo : deleteList) {
                if (!resourcePlcComponent.existResrce(resourceVo.getHandle())){
                    resourceVo.setConfiguredPlcInfo(CONFIGURED_PLC_INFO_N);
                    resourcePlcComponent.updateResrce(resourceVo);
                }
            }
        }
    }

    private void checkParam(ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO vo) throws InvalidParamException, RegisterationException, DataCodeException, DataDescriptionException {
        // check site, resourceBo, resourcePo, category
        if (Utils.isEmpty(vo.getSite())) {
            throw new InvalidParamException("Site", gson.toJson(vo));
        }
        if (Utils.isEmpty(vo.getResourceBo()) || vo.getResourcePO() == null
                || (vo.getResourcePO() != null && Utils.isEmpty(vo.getResourcePO().getHandle()))) {
            throw new InvalidParamException("ResourceBO Or ResourcePO", gson.toJson(vo));
        }
        if (Utils.isEmpty(vo.getCategory())) {
            throw new InvalidParamException("Category", gson.toJson(vo));
        }
        // check plcValue
        if (vo.getCategory().equals(CategoryCode.DeviceStatus.getCode()) || vo.getCategory().equals(CategoryCode.ReasonCode.getCode())
                || vo.getCategory().equals(CategoryCode.Startup.getCode()) || vo.getCategory().equals(CategoryCode.AlertInfo.getCode())
                || vo.getCategory().equals(CategoryCode.ThreeL.getCode())) {

            if (Utils.isEmpty(vo.getPlcValue())) {
                logger.debug(" >>>>>>>  category :  " +  vo.getCategory() + "    Registeration Exception(reg is empty)");
                throw new RegisterationException();
            }
        }

        // check plcObject
        if (vo.getCategory().equals(CategoryCode.DeviceStatus.getCode()) || vo.getCategory().equals(CategoryCode.ReasonCode.getCode())
                || vo.getCategory().equals(CategoryCode.Startup.getCode()) || vo.getCategory().equals(CategoryCode.PPM.getCode())
                || vo.getCategory().equals(CategoryCode.ThreeL.getCode())
                || vo.getCategory().equals(CategoryCode.ProductionCap.getCode())) {

            if (Utils.isEmpty(vo.getPlcObject())) {
                logger.debug(" >>>>>>>  category :  " +  vo.getCategory() + "    DataCode Exception(object is empty)");
                throw new DataCodeException();
            }
        }

        // check Description
        if (vo.getCategory().equals(CategoryCode.AlertInfo.getCode()) || vo.getCategory().equals(CategoryCode.PartsLife.getCode())) {
            if (Utils.isEmpty(vo.getDescription())) {
                logger.debug(" >>>>>>>  category cell:  " +  vo.getCategory() + "    DataDesciption Exception(des is empty)");
                throw new DataDescriptionException();
            }
        }
    }



    public PlcAddressUploadVO processUploadFile(MultipartFile multiFile, String site) throws IOException, ExistingRecordException, UpdateErrorException, NotFoundExistingRecordException, PLCRecordException {
        XSSFWorkbook workbook = new XSSFWorkbook(multiFile.getInputStream());
        XSSFSheet sheet = workbook.getSheetAt(0);

        List<CustomerRow> rowArray = new ArrayList<CustomerRow>();
        for (int rowNumber = Constants.IGNORED_NUM; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            Row row = sheet.getRow(rowNumber);
            CustomerRow customerRow = new ResourcePlcRow(site);
            for (int cellNumber = 0; cellNumber < Constants.PLC_UPLOAD_LENGTH; cellNumber++) {
                if (row != null && row.getCell(cellNumber) != null) {
                    Cell cell = row.getCell(cellNumber);
                    switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_NUMERIC:
                        customerRow.setCellValue(cellNumber, rowNumber,
                               Integer.valueOf((int)cell.getNumericCellValue()).toString());
                        break;
                    case Cell.CELL_TYPE_STRING:
                        customerRow.setCellValue(cellNumber, rowNumber, cell.getStringCellValue());
                        break;
                    case Cell.CELL_TYPE_BLANK:
                        customerRow.setCellValue(cellNumber, rowNumber, cell.getStringCellValue());
                        break;
                    }
                } else {
                    customerRow.setCellValue(cellNumber, rowNumber, "");
                }
            }
            rowArray.add(customerRow);
        }
        workbook.close();
        List<CustomerRow> notEmptyRowArray = new ArrayList<CustomerRow>();
        for (CustomerRow row : rowArray) {
            if(!row.isEmptyRow()) {
                notEmptyRowArray.add(row);
            }
        }

        for (CustomerRow row : notEmptyRowArray) {
            logger.debug(">>>>>>>>>>> excel rows    "+row.toString());
        }


        PlcAddressUploadVO vo = new PlcAddressUploadVO();
        for (CustomerRow row : notEmptyRowArray) {
            JsonRow jsonRow = row.generateRowError();
            if (jsonRow != null) {
                logger.debug(">>>>>>>>>>> json rows   " + gson.toJson(jsonRow));
                vo.getExceptionRowArray().add(row.generateRowError());
            }
        }

        // if no exception, then store data into database
        // ResourcePlcVO resourcePlcVO
        if (vo.getExceptionRowArray().size() == 0) {
            this.createDataRecord(notEmptyRowArray, site);
            vo.getSuccessUpload().setCommitNumber(notEmptyRowArray.size());
        }

        return vo;
    }

    private void createDataRecord(List<CustomerRow> rowArray, String site) throws ExistingRecordException, UpdateErrorException, NotFoundExistingRecordException, PLCRecordException {
        Set<String> createList = new HashSet<String>();
        for (CustomerRow row : rowArray) {
            ResourcePlcVO vo = (ResourcePlcVO) row.getVO(site);
            String resourceBo = new ResrceBOHandle(vo.getSite(), vo.getResrce()).getValue();
            vo.setResourceBo(resourceBo);
            vo.transferToUnicode();
            resourcePlcComponent.createResourcePlc(vo);
            createList.add(resourceBo);
        }
        for (String resourceBo : createList) {
            ResourcePO po = resourceComponent.getResourceByHandle(resourceBo);
            if (po == null) {
                // TODO: change to dedicated BusinessException
                throw new NotFoundExistingRecordException();
            }
            po.setConfiguredPlcInfo("true");
            resourcePlcComponent.updateResrce(Utils.copyObjectProperties(po, ResourceVO.class));
        }
    }

    public void commitExportExcel(List<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO> vos, String resource, String description, File file) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(PlcResourceCell.SHEET_NAME.getName());
        XSSFRow firstRow = sheet.createRow(0);
        firstRow.createCell(0).setCellValue(PlcResourceCell.RESOURCE.getName());
        firstRow.createCell(1).setCellValue(PlcResourceCell.DESCRIPTION.getName());
        firstRow.createCell(2).setCellValue(PlcResourceCell.CATEGORY_DES.getName());
        firstRow.createCell(3).setCellValue(PlcResourceCell.PLC_OBJECT_DES.getName());
        firstRow.createCell(4).setCellValue(PlcResourceCell.PLC_ADDRESS.getName());
        firstRow.createCell(5).setCellValue(PlcResourceCell.PLC_VALUE.getName());
        firstRow.createCell(6).setCellValue(PlcResourceCell.LOG_PATH.getName());

        for (int i = 0; i < vos.size(); i++) {
            XSSFRow addRow = sheet.createRow(i + 1);
            addRow.createCell(0).setCellValue(resource);
            addRow.createCell(1).setCellValue(description);
            addRow.createCell(2).setCellValue(vos.get(i).getCategoryDes());
            addRow.createCell(3).setCellValue(vos.get(i).getPlcObjectDes());
            addRow.createCell(4).setCellValue(vos.get(i).getPlcAddress());
            addRow.createCell(5).setCellValue(vos.get(i).getPlcValue());
            addRow.createCell(6).setCellValue(vos.get(i).getLogPath());
        }

        FileOutputStream outputStream = new FileOutputStream(file);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }
}
