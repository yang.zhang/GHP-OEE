package mhp.oee.web.angular.resource.plc.management;

/**
 * Created by LinZuK on 2016/10/13.
 */
public enum PlcResourceCell {
    SHEET_NAME("设备PLC地址信息"),
    RESOURCE("设备编码"),
    DESCRIPTION("设备名称"),
    CATEGORY_DES("PLC数据类型"),
    PLC_OBJECT_DES("数据项"),
    PLC_ADDRESS("PLC地址"),
    PLC_VALUE("寄存器值"),
    LOG_PATH("Log路径"),;

    private String name;

    private PlcResourceCell(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
