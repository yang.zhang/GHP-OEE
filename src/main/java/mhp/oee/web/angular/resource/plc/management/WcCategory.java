package mhp.oee.web.angular.resource.plc.management;

public enum WcCategory {

    LEVEL1("LEVEL1", "LEVEL1"),
    LEVEL2("LEVEL2", "LEVEL2"),
    LEVEL3("LEVEL3", "LEVEL3"),
    LEVEL4("LEVEL4", "LEVEL4"),
    ZLEVEL6("ZLEVEL6", "ZLEVEL6");

    private String name;
    private String des;

    private WcCategory(String name, String des) {
        this.name = name;
        this.des = des;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

}
