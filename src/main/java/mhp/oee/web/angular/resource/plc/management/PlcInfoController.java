package mhp.oee.web.angular.resource.plc.management;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.PlcInfoConditionVO;
import mhp.oee.vo.ResourcePlcInfoAndResrceAndPlcAccessTypeVO;
import mhp.oee.vo.ResourcePlcInfoVO;
import mhp.oee.vo.ResourceTypeConditionVO;
import mhp.oee.vo.ResourceVO;
import mhp.oee.vo.WorkCenterConditionVO;
import mhp.oee.web.Constants;

@RestController
public class PlcInfoController {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    PlcInfoService plcInfoService;

    @RequestMapping(value = "/web/rest/plant/site/{site}/work_center_condition",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<WorkCenterConditionVO>> readWorkCenter(@PathVariable("site") String site) {
        String trace = String.format("[PlcInfoController.readWorkCenter] request : site=%s", site);
        logger.debug(">> " + trace);
        List<WorkCenterConditionVO> vos = plcInfoService.readWorkCenter(site);
        logger.debug("<< [PlcInfoController.readWorkCenter]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/line_condition",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<WorkCenterConditionVO>> readLine(
            @PathVariable("site") String site,
            @RequestParam(value = "work_center", required = true) String workCenter) {
        String trace = String.format("[PlcInfoController.readLine] request : site=%s, workCenter=%s", site, workCenter);
        logger.debug(">> " + trace);
        List<WorkCenterConditionVO> vos = plcInfoService.readLine(site, workCenter);
        logger.debug("<< [PlcInfoController.readLine]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/exist_pc_ip",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> existPcIp(
            @PathVariable("site") String site,
            @RequestParam(value = "pc_ip", required = true) String pcIp) {
        String trace = String.format("[PlcInfoController.existPcIp] request : site=%s, pcIp=%s",
                site, pcIp);
        logger.debug(">> " + trace);
        boolean vos = plcInfoService.existPcIp(site, pcIp);
        logger.debug("<< [PlcInfoController.existPcIp] response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/resource_type_condition",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceTypeConditionVO>> readResourceType(@PathVariable("site") String site) {
        String trace = String.format("[PlcInfoController.readResourceType] request : site=%s", site);
        logger.debug(">> " + trace);
        List<ResourceTypeConditionVO> vos = plcInfoService.readResourceType(site);
        logger.debug("<< [PlcInfoController.readResourceType]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/resource_condition",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceVO>> readResource(
            @PathVariable("site") String site,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "line", required = true) String line,
            @RequestParam(value = "work_center", required = true) String workCenter) {
        String trace = String.format("[PlcInfoController.readResource] request : site=%s, resourceType=%s, line=%s, workCenter=%s",
                site, resourceType, line, workCenter);
        logger.debug(">> " + trace);
        List<ResourceVO> vos = plcInfoService.readResource(site, resourceType, line, workCenter);
        logger.debug("<< [PlcInfoController.readResource]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/site/{site}/plc_resource_condition",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceVO>> plcReadResource(
            @PathVariable("site") String site,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "line", required = true) String line,
            @RequestParam(value = "work_center", required = true) String workCenter) {
        String trace = String.format("[PlcInfoController.readResource] request : site=%s, resourceType=%s, line=%s, workCenter=%s",
                site, resourceType, line, workCenter);
        logger.debug(">> " + trace);
        List<ResourceVO> vos = plcInfoService.plcReadResource(site, resourceType, line, workCenter);
        logger.debug("<< [PlcInfoController.readResource]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/resource_condition_opt_with_line",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceVO>> readResourceOptWithLine(
            @PathVariable("site") String site,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "line", required = false) String line,
            @RequestParam(value = "work_center", required = true) String workCenter) {
        String trace = String.format("[PlcInfoController.readResource] request : site=%s, resourceType=%s, line=%s, workCenter=%s",
                site, resourceType, line, workCenter);
        logger.debug(">> " + trace);
        List<ResourceVO> vos = plcInfoService.readResourceOptionalWithLine(site, resourceType, line, workCenter);
        logger.debug("<< [PlcInfoController.readResource]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/site/{site}/resource_condition_opt_with_line_add_linearea",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceVO>> readResourceOptWithLineAddLineArea(
            @PathVariable("site") String site,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "line", required = false) String line,
            @RequestParam(value = "work_center", required = true) String workCenter) {
        String trace = String.format("[PlcInfoController.readResource] request : site=%s, resourceType=%s, line=%s, workCenter=%s",
                site, resourceType, line, workCenter);
        logger.debug(">> " + trace);
        List<ResourceVO> vos = plcInfoService.readResourceOptionalWithLineAddLineArea(site, resourceType, line, workCenter);
        logger.debug("<< [PlcInfoController.readResource]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/plc_info",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourcePlcInfoAndResrceAndPlcAccessTypeVO>> readPlcInfo(
            @PathVariable("site") String site,
            @RequestParam(value = "resource", required = false) String[] resource,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "work_center", required = true) String workCenter,
            @RequestParam(value = "line", required = true) String line,
            @RequestParam(value = "index", required = true) int index,
            @RequestParam(value = "count", required = true) int count) {
        String trace = String.format("[PlcInfoController.readPlcInfo] request : site=%s, resource=%s, resourceType=%s, workCenter=%s, line=%s, index=%s, count=%s",
                site, resource, resourceType, workCenter, line, index, count);
        logger.debug(">> " + trace);
        List<ResourcePlcInfoAndResrceAndPlcAccessTypeVO> vos = null;
        PlcInfoConditionVO plcInfoConditionVO = getPlcInfoConditionVO(site, resource, resourceType, workCenter, line, index, count);
        if (plcInfoConditionVO.getResource() != null && plcInfoConditionVO.getResource().length > 0) {
            vos = plcInfoService.readResPlcInfoByRes(plcInfoConditionVO);
        } else {
            vos = plcInfoService.readResPlcInfo(plcInfoConditionVO);
        }
        logger.debug("<< [PlcInfoController.readPlcInfo]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/plc_info_count",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Integer> readPlcInfoCount(
            @PathVariable("site") String site,
            @RequestParam(value = "resource", required = false) String[] resource,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "work_center", required = true) String workCenter,
            @RequestParam(value = "line", required = true) String line) {
        String trace = String.format("[PlcInfoController.readPlcInfoCount] request : site=%s, resource=%s, resourceType=%s, workCenter=%s, line=%s",
                site, resource, resourceType, workCenter, line);
        logger.debug(">> " + trace);
    	int total = 0;
        PlcInfoConditionVO plcInfoConditionVO = getPlcInfoConditionVO(site, resource, resourceType, workCenter, line, total, total);
    	if (plcInfoConditionVO.getResource() != null && plcInfoConditionVO.getResource().length > 0) {
    	    total = plcInfoService.readResPlcInfoByResCount(plcInfoConditionVO);
        } else {
            total = plcInfoService.readResPlcInfoCount(plcInfoConditionVO);
        }
        logger.debug("<< [PlcInfoController.readPlcInfoCount]  response : "  + gson.toJson(total));
        return ResponseEntity.ok(total);
    }

    @RequestMapping(value = "/web/rest/plant/change_plc_info",
            method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourcePlcInfoVO>> changePlcInfo(@RequestBody List<ResourcePlcInfoVO> resourcePlcInfoVO) {
        String trace = String.format("[PlcInfoController.changePlcInfo] request : " + gson.toJson(resourcePlcInfoVO));
        logger.debug(">> " + trace);
        try {
            plcInfoService.changePlcInfo(resourcePlcInfoVO);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("PlcInfoController.changePlcInfo() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [PlcInfoController.changePlcInfo]  response : void");
        return ResponseEntity.ok().body(null);
    }

    private PlcInfoConditionVO getPlcInfoConditionVO(String site, String[] resource, String resourceType, String workCenter, String line, int index, int count){
        PlcInfoConditionVO plcInfoConditionVO = new PlcInfoConditionVO();
        plcInfoConditionVO.setLimit(count);
        plcInfoConditionVO.setOffset(Utils.getOffset(count, index));
        plcInfoConditionVO.setSite(site);
        plcInfoConditionVO.setResource(resource);
        plcInfoConditionVO.setResourceType(resourceType);
        plcInfoConditionVO.setWorkCenter(workCenter);
        plcInfoConditionVO.setLine(line);
        return plcInfoConditionVO;
    }
}
