package mhp.oee.web.angular.resource.plc.management.excel.parser;

import mhp.oee.common.exception.ExistingException;
import mhp.oee.common.handle.PlcCategoryBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.logging.GenericLoggerFactory;
import mhp.oee.common.security.SpringContextHolder;
import mhp.oee.component.plant.PlcCategoryComponent;
import mhp.oee.component.plant.PlcObjectComponent;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.po.gen.PlcCategoryPO;
import mhp.oee.po.gen.PlcObjectPO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.web.angular.excel.parser.api.CellValidator;
import mhp.oee.web.angular.resource.plc.management.excel.parser.ResourcePlcCell.ResourcePlcIndex;

public class ExistingCellValidator extends CellValidator {


    public ExistingCellValidator(ResourcePlcCell cell){
        this.cell = cell;
        this.cell.attach(this);
        logger = GenericLoggerFactory.getLogger(ExistingCellValidator.class);
     }

    @Override
    public void validator() {
        if(this.cell.getValue().trim().length() == 0) {
            return;
        }

        if(this.cell.getKey().equals(ResourcePlcIndex.DeviceNo)){
            String handle = new ResrceBOHandle(this.cell.getSite(), this.cell.getValue()).getValue();
            ResourceComponent dao = (ResourceComponent)SpringContextHolder.getBean("resourceComponent");
            ResourcePO po = dao.getResourceByHandle(handle);
            if(po == null) {
                this.cell.addException(new ExistingException());
            }

            return;
        }

        if(this.cell.getKey().equals(ResourcePlcIndex.Category)){
            String handle = new PlcCategoryBOHandle(this.cell.getSite(), this.cell.getValue()).getValue();
            PlcCategoryComponent dao = (PlcCategoryComponent)SpringContextHolder.getBean("plcCategoryComponent");
            PlcCategoryPO po = dao.getCategoryByHandle(handle);
            if(po == null) {
                this.cell.addException(new ExistingException());
            }

            return;
        }

        if(this.cell.getKey().equals(ResourcePlcIndex.Object)){
            PlcObjectComponent dao = (PlcObjectComponent)SpringContextHolder.getBean("plcObjectComponent");
            PlcObjectPO po = dao.existPlcObject(this.cell.getSite(), this.cell.getValue());
            if(po == null) {
                this.cell.addException(new ExistingException());
            }

            return;
        }





    }





}
