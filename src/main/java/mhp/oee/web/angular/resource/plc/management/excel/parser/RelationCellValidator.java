package mhp.oee.web.angular.resource.plc.management.excel.parser;

import mhp.oee.common.exception.DataCodeException;
import mhp.oee.common.exception.DataDescriptionException;
import mhp.oee.common.exception.InvalidRelatedFieldException;
import mhp.oee.common.exception.RegisterationException;
import mhp.oee.common.logging.GenericLoggerFactory;
import mhp.oee.common.security.SpringContextHolder;
import mhp.oee.component.plant.PlcObjectComponent;
import mhp.oee.po.gen.PlcObjectPO;
import mhp.oee.web.angular.excel.parser.api.CellValidator;
import mhp.oee.web.angular.excel.parser.api.CustomerCell;
import mhp.oee.web.angular.excel.parser.api.MandatoryCellValidator;
import mhp.oee.web.angular.resource.plc.management.excel.parser.ResourcePlcCell.ResourcePlcIndex;

public class RelationCellValidator extends CellValidator {

    private CustomerCell dependentCell;

    public RelationCellValidator(CustomerCell cell, CustomerCell dependentCell) {
        this.dependentCell = dependentCell;
        this.cell = cell;
        this.cell.attach(this);
        logger = GenericLoggerFactory.getLogger(MandatoryCellValidator.class);
    }

    @Override
    public void validator() {
        String dValue = this.dependentCell.getValue();

        // 若数据类型是设备状态、原因代码、设备启动防呆、报警信息、三色灯，则检查寄存器值是否填写，若无值，则将错误记录显示，
        // 并在消息列提示“寄存器值没有维护”
        if (this.cell.getKey().equals(ResourcePlcIndex.Reg)) {
            if (dValue.equals(CategoryCode.DeviceStatus.getCode()) || dValue.equals(CategoryCode.ReasonCode.getCode())
                    || dValue.equals(CategoryCode.Startup.getCode()) || dValue.equals(CategoryCode.AlertInfo.getCode())
                    || dValue.equals(CategoryCode.ThreeL.getCode())) {

                if (this.cell.getValue().trim().length() == 0) {
                    logger.error(" >>>>>>>  category cell:  " +  dValue + "    Registeration Exception(reg is empty)");
                    this.cell.addException(new RegisterationException());
                }
            }
        }

        // 若数据类型是设备状态、原因代码、设备启动防呆、优率数据、实时PPM、三色灯，
        // 则检查数据项代码是否填写，若无值，则将错误记录显示，并在消息列提示“数据项代码没有维护”
        if (this.cell.getKey().equals(ResourcePlcIndex.Object)) {
            if (dValue.equals(CategoryCode.DeviceStatus.getCode()) || dValue.equals(CategoryCode.ReasonCode.getCode())
                    || dValue.equals(CategoryCode.Startup.getCode()) || dValue.equals(CategoryCode.PPM.getCode())
                    || dValue.equals(CategoryCode.ThreeL.getCode())
                    || dValue.equals(CategoryCode.ProductionCap.getCode())) {

                if (this.cell.getValue().trim().length() == 0) {
                    logger.error(" >>>>>>>  category cell:  " +  dValue + "    DataCode Exception(object is empty)");
                    this.cell.addException(new DataCodeException());
                }
            }
        }



        if (this.cell.getKey().equals(ResourcePlcIndex.Object) && this.cell.getValue().trim().length() != 0) {
            String site = this.cell.getSite();
            String object = this.cell.getValue();
            PlcObjectComponent dao = (PlcObjectComponent)SpringContextHolder.getBean("plcObjectComponent");
            PlcObjectPO po = dao.getPlcObjectBySiteCategoryObject(site, dValue, object);
            if(po == null) {
                logger.error("can not find object code");
                this.cell.addException(new InvalidRelatedFieldException());
            }
        }





        // 若数据类型是报警信息和关键配件寿命，则检查描述是否填写，若无值，
        // 则将错误记录显示，并在消息列提示“数据项描述没有维护”
        if (this.cell.getKey().equals(ResourcePlcIndex.Des)) {
            if (dValue.equals(CategoryCode.AlertInfo.getCode()) || dValue.equals(CategoryCode.PartsLife.getCode())) {
                if (this.cell.getValue().trim().length() == 0) {
                    logger.error(" >>>>>>>  category cell:  " +  dValue + "    DataDesciption Exception(des is empty)");
                    this.cell.addException(new DataDescriptionException());
                }
            }
        }
    }

    /*
     * C01 设备状态 C02 原因代码 C03 产量数据 C04 设备是否允许开机 C05 报警信息 C06 关键配件寿命 C07 实时PPM C08
     * 三色灯
     */
    public enum CategoryCode {
        DeviceStatus("C01"), ReasonCode("C02"), ProductionCap("C03"), Startup("C04"), AlertInfo("C05"), PartsLife(
                "C06"), PPM("C07"), ThreeL("C08");

        private String code;

        CategoryCode(String code) {
            this.setCode(code);
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }

}
