package mhp.oee.web.angular.resource.plc.management;

import java.util.List;

import mhp.oee.utils.ISplitStringFormat;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.common.handle.WorkCenterBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ResourcePlcInfoComponent;
import mhp.oee.po.gen.ResourcePlcInfoPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.PlcInfoConditionVO;
import mhp.oee.vo.ResourcePlcInfoAndResrceAndPlcAccessTypeVO;
import mhp.oee.vo.ResourcePlcInfoVO;
import mhp.oee.vo.ResourceTypeConditionVO;
import mhp.oee.vo.ResourceVO;
import mhp.oee.vo.WorkCenterConditionVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class PlcInfoService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private ResourcePlcInfoComponent resourcePlcInfoComponent;

    public List<WorkCenterConditionVO> readWorkCenter(String site) {
        return resourcePlcInfoComponent.readAllWorkCenter(site);
    }

    public boolean existPcIp(String site, String pcIp) {
        List<ResourcePlcInfoPO> po = resourcePlcInfoComponent.existPcIp(site, pcIp);
        return (po != null && po.size() > 0) ? true : false;
    }

    public List<WorkCenterConditionVO> readLine(String site, String workCenter) {
        WorkCenterBOHandle workCenterBOHandle = new WorkCenterBOHandle(site, workCenter);
        return resourcePlcInfoComponent.readLine(site, workCenterBOHandle.getValue());
    }

    public List<ResourceTypeConditionVO> readResourceType(String site) {
        return resourcePlcInfoComponent.readAllResourceType(site);
    }

    public List<ResourceVO> readResource(String site, String resourceType, String line, String workCenter) {
        String resourceTypeBO = null;
        if (!Utils.isEmpty(resourceType)) {
            resourceTypeBO = new ResourceTypeBOHandle(site, resourceType).getValue();
        }
        return resourcePlcInfoComponent.readResource(site, resourceTypeBO, line, workCenter);
    }
    
    public List<ResourceVO> plcReadResource(String site, String resourceType, String line, String workCenter) {
        String resourceTypeBO = null;
        if (!Utils.isEmpty(resourceType)) {
            resourceTypeBO = new ResourceTypeBOHandle(site, resourceType).getValue();
        }
        return resourcePlcInfoComponent.plcReadResource(site, resourceTypeBO, line, workCenter);
    }

    public List<ResourcePlcInfoAndResrceAndPlcAccessTypeVO> readResPlcInfoByRes(PlcInfoConditionVO plcInfoConditionVO) {
        List<ResourcePlcInfoAndResrceAndPlcAccessTypeVO> vos = resourcePlcInfoComponent.readResPlcInfoByRes(plcInfoConditionVO);
        return vos;
    }
    public int readResPlcInfoByResCount(PlcInfoConditionVO plcInfoConditionVO) {
    	int vos = resourcePlcInfoComponent.readResPlcInfoByResCount(plcInfoConditionVO);
        return vos;
    }

    public List<ResourcePlcInfoAndResrceAndPlcAccessTypeVO> readResPlcInfo(PlcInfoConditionVO plcInfoConditionVO) {
        List<ResourcePlcInfoAndResrceAndPlcAccessTypeVO> vos = resourcePlcInfoComponent.readResPlcInfo(plcInfoConditionVO);
        return vos;
    }
    public int readResPlcInfoCount(PlcInfoConditionVO plcInfoConditionVO) {
    	int vos = resourcePlcInfoComponent.readResPlcInfoCount(plcInfoConditionVO);
        return vos;
    }

    public void changePlcInfo(List<ResourcePlcInfoVO> resourcePlcInfoVO) throws BusinessException {
        for (ResourcePlcInfoVO each : resourcePlcInfoVO) {
        	switch (each.getViewActionCode()) {
            case 'C':
                resourcePlcInfoComponent.createResPlcInfo(each);
                break;
            case 'U':
                resourcePlcInfoComponent.updateResPlcInfo(each);
                break;
            case 'D':
            	resourcePlcInfoComponent.deleteResPlcInfo(each);
                break;
            default:
                logger.error("PlcInfoService.cudChangePlcInfo() : Invalid viewActionCode '" + each.getViewActionCode()
                        + "'.");
                break;
            }
        }
    }

    public List<ResourceVO> readResourceOptionalWithLine(final String site, final String resourceType, String line, String workCenter) {

        return resourcePlcInfoComponent.readResource(site, resourceType, line, workCenter);
    }

    public List<ResourceVO> readResourceOptionalWithLineAddLineArea(final String site, String resourceType, String line,
                                                                    String workCenter) {
        // 将类似"resource1,resource2,resource3"这样的字符串转变成"resourceBo1|resourceBo2|resourceBo3"
        String resourceTypeBO = Utils.formatSplitString(resourceType, ",", "#", new ISplitStringFormat() {
            @Override
            public String format(String rt) {
                return new ResourceTypeBOHandle(site, rt).getValue();
            }
        });

        // LINE_AREA
        if (!Utils.isEmpty(line)) {
            String[] lineArray = line.split(",");
            line = Utils.formatInParams2("LINE_AREA", lineArray);
        }

        // WORK_AREA
        if (!Utils.isEmpty(workCenter)) {
            String[] workCenterArray = workCenter.split(",");
            workCenter = Utils.formatInParams2("WORK_AREA", workCenterArray);
        }

        // RESOURCE_TYPE_BO
        if (!Utils.isEmpty(resourceTypeBO)) {
            String[] resourceTypeBOArray = resourceTypeBO.split("#");
            resourceTypeBO = Utils.formatInParams2("RESOURCE_TYPE_BO", resourceTypeBOArray);
        }
        return resourcePlcInfoComponent.readResourceAddLineArea(site, resourceTypeBO, line, workCenter);
    }
}
