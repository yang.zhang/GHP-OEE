package mhp.oee.web.angular.resource.plc.management.excel.parser;

import java.util.List;

import mhp.oee.web.angular.excel.parser.api.CustomerCell;
import mhp.oee.web.angular.excel.parser.api.CustomerIndex;
import mhp.oee.web.angular.excel.parser.api.MandatoryCellValidator;

public class ResourcePlcCell extends CustomerCell {

    public ResourcePlcCell(ResourcePlcIndex key, String value, String site) {
        super(key, value, site);
    }

    public static List<CustomerCell> initCellAttribute(List<CustomerCell> list, String site) {

        ResourcePlcCell deviceCell = new ResourcePlcCell(ResourcePlcIndex.DeviceNo, "", site);
        new MandatoryCellValidator(deviceCell);
        new ExistingCellValidator(deviceCell);
        list.add(ResourcePlcIndex.DeviceNo.getIndex(), deviceCell);

        ResourcePlcCell addressCell = new ResourcePlcCell(ResourcePlcIndex.Address, "", site);
        new MandatoryCellValidator(addressCell);
        list.add(ResourcePlcIndex.Address.getIndex(), addressCell);

        ResourcePlcCell categoryCell = new ResourcePlcCell(ResourcePlcIndex.Category, "", site);
        new MandatoryCellValidator(categoryCell);
        new ExistingCellValidator(categoryCell);
        list.add(ResourcePlcIndex.Category.getIndex(), categoryCell);

        ResourcePlcCell regCell = new ResourcePlcCell(ResourcePlcIndex.Reg, "", site);
        new RelationCellValidator(regCell, categoryCell);
        list.add(ResourcePlcIndex.Reg.getIndex(), regCell);

        ResourcePlcCell objectCell = new ResourcePlcCell(ResourcePlcIndex.Object, "", site);
        new ExistingCellValidator(objectCell);
        new RelationCellValidator(objectCell, categoryCell);
        list.add(ResourcePlcIndex.Object.getIndex(), objectCell);

        ResourcePlcCell desCell = new ResourcePlcCell(ResourcePlcIndex.Des, "", site);
        new RelationCellValidator(desCell, categoryCell);
        list.add(ResourcePlcIndex.Des.getIndex(), desCell);
        list.add(ResourcePlcIndex.Path.getIndex(), new ResourcePlcCell(ResourcePlcIndex.Path, "", site));

        return list;
    }

    public enum ResourcePlcIndex implements CustomerIndex {
        DeviceNo(0, "resrce"),
        Address(1, "plcAddress"),
        Category(2, "category"),
        Reg(3, "plcValue"),
        Object(4, "plcObject"),
        Des(5, "description"),
        Path(6, "logPath");

        private Integer index;
        private String value;

        private ResourcePlcIndex(Integer index, String value) {
            this.index = index;
            this.value = value;
        }

        @Override
        public String toString() {
            return this.index.toString();
        }

        @Override
        public Integer getIndex() {
            return this.index;
        }

        @Override
        public String getValue() {
            // TODO Auto-generated method stub
            return this.value;
        }
    }

}