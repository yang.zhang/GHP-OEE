package mhp.oee.web.angular.resource.plc.management;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ResourcePlcVO;
import mhp.oee.vo.ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO;
import mhp.oee.web.Constants;
import mhp.oee.web.angular.plc.accesstype.management.PlcAddressUploadVO;

@RestController
public class PlcController {

	@InjectableLogger
	private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

	@Autowired
	PlcService plcService;

	@RequestMapping(value = "/web/rest/plant/site/{site}/resource_plc",
			method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
	public ResponseEntity<List<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO>> readResourcePlc(
	        @PathVariable("site") String site,
			@RequestParam(value = "plc_category", required = true) String plcCategory,
			@RequestParam(value = "resource", required = true) String resource,
			@RequestParam(value = "index", required = true) int pageIndex,
			@RequestParam(value = "count", required = true) int pageCount) {
	    String trace = String.format("[PlcController.readResourcePlc] request : site=%s, plcCategory=%s, resource=%s, index=%s, count=%s",
                site, plcCategory, resource, pageIndex, pageCount);
        logger.debug(">> " + trace);
		List<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO> vos = plcService.readResourcePlc(site, resource,
				plcCategory, pageCount, Utils.getOffset(pageCount, pageIndex));
		logger.debug("<< [PlcController.readResourcePlc]  response : "  + gson.toJson(vos));
		return ResponseEntity.ok(vos);
	}

	@RequestMapping(value = "/web/rest/plant/site/{site}/resource_plc/export_excel",
			method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
	public ResponseEntity<List<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO>> exportExcel(
			@PathVariable("site") String site,
			@RequestParam(value = "plc_category", required = true) String plcCategory,
			@RequestParam(value = "resource", required = true) String resource,
			@RequestParam(value = "description", required = true) String description,
			HttpServletResponse response) {
		String trace = String.format("[PlcController.exportExcel] request : site=%s, plcCategory=%s, resource=%s, description=%s",
				site, plcCategory, resource, description);
		logger.debug(">> " + trace);
		try {
			File file = new File(UUID.randomUUID().toString().replace("-", "") + ".xlsx");
			List<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO> vos = plcService.readResourcePlc(site, resource,
					plcCategory, null, null);
			plcService.commitExportExcel(vos, resource, description, file);
			Utils.generateDownloadFile(response, file);
			FileUtils.deleteQuietly(file);
		} catch (IOException e) {
			ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
			logger.error("exportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
		}
		logger.debug("<< [PlcController.exportExcel]  response : void");
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/web/rest/plant/site/{site}/resource_plc_count",
			method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
	public ResponseEntity<Integer> readResourcePlcCount(
	        @PathVariable("site") String site,
            @RequestParam(value = "plc_category", required = true) String plcCategory,
            @RequestParam(value = "resource", required = true) String resource) {
	    String trace = String.format("[PlcController.readResourcePlcCount] request : site=%s, plcCategory=%s, resource=%s",
                site, plcCategory, resource);
        logger.debug(">> " + trace);
		int vos = plcService.readResourcePlcCount(site, resource, plcCategory);
		logger.debug("<< [PlcController.readResourcePlcCount]  response : "  + gson.toJson(vos));
		return ResponseEntity.ok(vos);
	}

	@RequestMapping(value = "/web/rest/plant/site/{site}/exist_resource_plc",
			method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
	public ResponseEntity<Boolean> existResourcePlcByHandle(
	        @PathVariable("site") String site,
            @RequestParam(value = "plc_address", required = true) String plcAddress,
            @RequestParam(value = "plc_value", required = true) String plcValue,
            @RequestParam(value = "resource_bo", required = true) String resourceBo){
	    String trace = String.format("[PlcController.existResourcePlcByHandle] request : site=%s, plcAddress=%s, plcValue=%s, resourceBo=%s",
                site, plcAddress, plcValue, resourceBo);
        logger.debug(">> " + trace);
		ResourcePlcVO resourcePlcVO = getResourcePlcVO(site, plcAddress, plcValue, null, resourceBo);
		boolean vos = plcService.existResourcePlcByHandle(resourcePlcVO);
		logger.debug("<< [PlcController.existResourcePlcByHandle]  response : "  + gson.toJson(vos));
		return ResponseEntity.ok(vos);
	}

	@RequestMapping(value = "/web/rest/plant/site/{site}/exist_resource_plc_obj",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> existResourcePlcByObj(
            @PathVariable("site") String site,
            @RequestParam(value = "plc_object", required = true) String plcObject,
            @RequestParam(value = "resource_bo", required = true) String resourceBo){
	    String trace = String.format("[PlcController.existResourcePlcByObj] request : site=%s, plcObject=%s, resourceBo=%s",
                site, plcObject, resourceBo);
        logger.debug(">> " + trace);
        ResourcePlcVO resourcePlcVO = getResourcePlcVO(site, null, null, plcObject, resourceBo);
        boolean vos = plcService.existResourcePlcByResrceAndObj(resourcePlcVO);
        logger.debug("<< [PlcController.existResourcePlcByObj]  response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

	@RequestMapping(value = "/web/rest/plant/site/{site}/exist_plc_object",
			method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
	public ResponseEntity<Boolean> existPlcObject(
	        @PathVariable("site") String site,
            @RequestParam(value = "plc_address", required = true) String plcAddress,
            @RequestParam(value = "plc_value", required = true) String plcValue,
            @RequestParam(value = "plc_object", required = true) String plcObject,
            @RequestParam(value = "resource_bo", required = true) String resourceBo) {
	    String trace = String.format("[PlcController.existPlcObject] request : site=%s, plcAddress=%s, plcValue=%s, plcObject=%s,  resourceBo=%s",
	            site, plcAddress, plcValue, plcObject, resourceBo);
        logger.debug(">> " + trace);
	    ResourcePlcVO resourcePlcVO = getResourcePlcVO(site, plcAddress, plcValue, plcObject, resourceBo);
	    boolean vos= plcService.existPlcObject(resourcePlcVO);
        logger.debug("<< [PlcController.existPlcObject]  response : "  + gson.toJson(vos));
		return ResponseEntity.ok(vos);
	}

	@RequestMapping(value = "/web/rest/plant/change_resource_plc",
			method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
	public ResponseEntity<?> changeResourcePlc(@RequestBody List<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO> resourcePlcVOs) {
	    String trace = String.format("[PlcController.changeResourcePlc] request : " + gson.toJson(resourcePlcVOs));
        logger.debug(">> " + trace);
	    try {
			plcService.changeResourcePlc(resourcePlcVOs);
		} catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
			logger.error("PlcController.changeResourcePlc() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
		}
        logger.debug("<< [PlcController.changeResourcePlc] response : void");
		return ResponseEntity.ok().body(null);
	}

	private ResourcePlcVO getResourcePlcVO(String site, String plcAddress, String plcValue, String plcObject, String resourceBO){
	    ResourcePlcVO vo = new ResourcePlcVO();
	    vo.setSite(site);
	    vo.setPlcAddress(plcAddress);
	    vo.setPlcValue(plcValue);
	    vo.setPlcObject(plcObject);
	    vo.setResourceBo(resourceBO);
	    return vo;
	}

    @RequestMapping(value = "/web/rest/plant/site/{site}/resource/upload_file", method = RequestMethod.POST)
    public ResponseEntity<?> processUpload(@RequestParam(value = "file") MultipartFile file,
            @PathVariable("site") String site, HttpServletResponse res, HttpServletRequest request) {
        try {
            logger.debug("file upload " + file.getOriginalFilename());

            Utils.checkUploadFilename(file.getOriginalFilename());

            PlcAddressUploadVO vo = this.plcService.processUploadFile(file, site);
            logger.debug(">>>>>> " + " : " + gson.toJson(vo));

            res.setHeader("Content-Type", "application/json");
            res.setStatus(HttpServletResponse.SC_ACCEPTED);
            return ResponseEntity.ok(vo);
        } catch (IOException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.IO_EXCEPTION.getErrorCode(), e.getMessage(), null);
            logger.error("ReasonCodeAlertController.readReasonCodeAlertHisStatus() : " + e.getMessage()
                    + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        } catch (BusinessException e) {
            return Utils.returnExceptionToFrontEnd(e);
        }
    }

    @RequestMapping(value = "/download/plant/site/{site}/resource/excel", method = RequestMethod.GET)
    public ResponseEntity<?> exportExcel(@PathVariable("site") String site, HttpServletResponse response,
            HttpServletRequest request) {

     try {
            String filePath = request.getSession().getServletContext().getRealPath("/");
            filePath = filePath + "/plc.xlsx";
            Utils.generateDownloadFile(response, new File(filePath));

        } catch (IOException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
            logger.error("alertExportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok(null);
    }

}
