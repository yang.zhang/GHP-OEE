package mhp.oee.web.angular.yield.maintenance;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ProductionOutputFirstBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.component.plant.ResourceTypeComponent;
import mhp.oee.component.plant.WorkCenterComponent;
import mhp.oee.component.production.ItemComponent;
import mhp.oee.component.production.ProductionOutputComponent;
import mhp.oee.component.production.ProductionOutputRealtimeComponent;
import mhp.oee.po.extend.ResourceAndWorkCenterShiftAndWorkCenterPO;
import mhp.oee.po.gen.ItemPO;
import mhp.oee.po.gen.ProductionOutputFirstPO;
import mhp.oee.po.gen.ProductionOutputRealtimePO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceConditionVO;
import mhp.oee.vo.WorkCenterShiftConditionVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class YieldService {

    @Autowired
    ResourceTypeComponent resourceTypeComponent;
    @Autowired
    WorkCenterComponent workCenterComponent;
    @Autowired
    ResourceComponent resourceComponent;
    @Autowired
    ItemComponent itemComponent;
    @Autowired
    ProductionOutputComponent productionOutputComponent;
    @Autowired
    ProductionOutputRealtimeComponent productionOutputRealtimeComponent;

    @InjectableLogger
    private static Logger logger;

    public List<ResourceConditionVO> getResourcesByCondition(String site, String resrce, String lineArea,
            String workArea, String resourceType) {
        return resourceComponent.getResourceByCondition(site, resrce, lineArea, workArea, resourceType);
    }

    public List<WorkCenterShiftConditionVO> getWorkCenterShiftByCondition(String site, String resrce) {
        List<ResourceAndWorkCenterShiftAndWorkCenterPO> pos = resourceComponent
                .readResrceAndWorkCenterAndWorkCenterShift(site, resrce);
        List<WorkCenterShiftConditionVO> vos = new ArrayList<WorkCenterShiftConditionVO>(pos.size());
        for (ResourceAndWorkCenterShiftAndWorkCenterPO po : pos) {
            WorkCenterShiftConditionVO vo = new WorkCenterShiftConditionVO();
            vo.setStartTime(Utils.time2ToStr(po.getStartTime()));
            vo.setEndTime(Utils.time2ToStr(po.getEndTime()));
            vo.setShift(po.getShift());
            vo.setDescription(po.getDescription());
            vos.add(vo);
        }
        return vos;
    }


    public List<YieldSearchVO> searchProductionOutputWithItem(String site, List<String> resrceList,
            List<String> itemList, String productionDate, String startTime, String endTime, String shift,
            String shiftDescription) throws ParseException {

        Date shiftStartTime = Utils.getDateFromWeb(productionDate + "-" + startTime);

        // 第一步：先从表PRODUCTION_OUTPUT_FIRST获取已维护过设备优的数据
        List<ProductionOutputFirstPO> readProductionOutputFirst = this.productionOutputComponent
                .readProductionOutputFirst(site, resrceList, itemList, shift, shiftStartTime);
        List<YieldSearchVO> list = this.getListYieldSearchVOList(readProductionOutputFirst, site, productionDate, shift, shiftDescription);

        // 第二步：从表PRODUCTION_OUTPUT_REALTIME取总产量
        // if endTime < startTime, date + 1
        Date shiftEndTime = getShiftEndTime(productionDate, endTime, startTime);
        List<ProductionOutputRealtimePO> records = productionOutputRealtimeComponent.readByResrceAndShiftTime(site,
                resrceList, itemList, shiftStartTime, shiftEndTime);

        if (records != null && records.size() != 0) {
            list = getExtractOutputFromRealtime(site, productionDate, shift, shiftDescription, list, records, shiftStartTime, shiftEndTime);
        }

        return list;
    }

    public List<YieldSearchVO> searchProductionOutputWithoutItem(String site, List<String> resrceList,
            String productionDate, String startTime, String endTime, String shift, String shiftDescription)
            throws ParseException {
        Date shiftStartTime = Utils.getDateFromWeb(productionDate + "-" + startTime);

        // 第一步：先从表PRODUCTION_OUTPUT_FIRST获取已维护过设备优的数据
        List<ProductionOutputFirstPO> readProductionOutputFirst = this.productionOutputComponent.readProductionOutputFirst(site, resrceList, shift, shiftStartTime);
        List<YieldSearchVO> list = this.getListYieldSearchVOList(readProductionOutputFirst, site, productionDate, shift, shiftDescription);


        // 第二步：从表PRODUCTION_OUTPUT_REALTIME取总产量
        Date shiftEndTime = getShiftEndTime(productionDate, endTime, startTime);

        List<ProductionOutputRealtimePO> records = productionOutputRealtimeComponent.readByResrceAndShiftTime(site,
                resrceList, shiftStartTime, shiftEndTime);

        if (records != null && records.size() != 0) {
            list = getExtractOutputFromRealtime(site, productionDate, shift, shiftDescription, list, records, shiftStartTime, shiftEndTime);
        }

        return list;
    }

    private List<YieldSearchVO> getExtractOutputFromRealtime(String site, String productionDate, String shift, String shiftDescription,
            List<YieldSearchVO> list, List<ProductionOutputRealtimePO> records,
            Date shiftStartTime, Date shiftEndTime) {
        // set qty to zero
        for (ProductionOutputRealtimePO po : records) {
            if (po.getQtyTotal().doubleValue() < 0) {
                po.setQtyTotal(new BigDecimal(0));
            }
        }

        List<ProductionOutputRealtimePO> groupByItemList = new ArrayList<ProductionOutputRealtimePO>();
        // group by item
        for (ProductionOutputRealtimePO po : records) {
            ProductionOutputRealtimePO exist = this.existInGroupByItemList(groupByItemList, po);
            if (exist != null) {
                exist.setQtyTotal(exist.getQtyTotal().add(po.getQtyTotal()));
            } else {
                groupByItemList.add(po);
            }
        }

        // exist is YieldSearchVO
        for (ProductionOutputRealtimePO po : groupByItemList) {
            if (!this.existInYieldSearchList(list, po)) {
                YieldSearchVO vo = new YieldSearchVO();
                vo.setBadQuity(new BigDecimal(0));
                vo.setGoodQuity(new BigDecimal(0));
                vo.setTotalOutput(po.getQtyTotal());
                vo.setShiftDescription(shiftDescription);
                vo.setShift(shift);
                vo.setProductionDate(productionDate);
                vo.setModel(itemComponent.getModelByEnabledItem(site, po.getItem()));
                vo.setItem(po.getItem());
                vo.setResrce(po.getResrce());
                vo.setResrceDescription(resourceComponent.getResourceByKey(site, po.getResrce()).getDescription());
                vo.setStartDateTime(shiftStartTime);
                vo.setEndDateTime(shiftEndTime);
                vo.setSite(site);

                list.add(vo);
            }
        }

        return list;
    }

    private Date getShiftEndTime(String productionDate, String endTime, String startTime) throws ParseException {
        // if endTime < startTime, date + 1
        Date startTimeDate = Utils.getTimeFromWeb(startTime);
        logger.debug("start time:" + startTimeDate);

        Date endTimeDate = Utils.getTimeFromWeb(endTime);
        logger.debug("end time:" + endTimeDate);

        Date shiftEndTime = Utils.getDateFromWeb(productionDate + "-" + endTime);
        logger.debug("shiftEndTime:" + shiftEndTime.toString());

        if (endTimeDate.before(startTimeDate)) {
            Calendar c = Calendar.getInstance();
            c.setTime(shiftEndTime);
            c.add(Calendar.DAY_OF_MONTH, 1);// 今天+1天
            shiftEndTime = c.getTime();
            logger.debug("change shiftEndTime:" + shiftEndTime.toString());
        }
        return shiftEndTime;
    }

    private List<YieldSearchVO> getListYieldSearchVOList(List<ProductionOutputFirstPO> readProductionOutputFirst,
                                                         String site, String productionDate, String shift, String shiftDescription) throws ParseException{
        List<YieldSearchVO> list = new ArrayList<YieldSearchVO>();
        for (ProductionOutputFirstPO po : readProductionOutputFirst) {
            YieldSearchVO vo = new YieldSearchVO();
            vo.setBadQuity(po.getQtyScrap());
            vo.setGoodQuity(po.getQtyYield());
            vo.setTotalOutput(po.getQtyYield().add(po.getQtyScrap()));
            vo.setShift(shift);
            vo.setShiftDescription(shiftDescription);
            vo.setProductionDate(productionDate);
            vo.setModel(itemComponent.getModelByEnabledItem(site, po.getItem()));
            vo.setItem(po.getItem());
            vo.setResrce(po.getResrce());
            vo.setResrceDescription(resourceComponent.getResourceByKey(site, po.getResrce()).getDescription());
            vo.setHandle(po.getHandle());
            vo.setModifiedDateTime(po.getModifiedDateTime());
            vo.setCreatedDateDec(Utils.datetimeToStr(po.getCreatedDateTime()));
            list.add(vo);
        }
        return list;
    }

    private boolean existInYieldSearchList(List<YieldSearchVO> list, ProductionOutputRealtimePO po) {
        for (YieldSearchVO vo : list) {
            if (vo.getResrce().equals(po.getResrce()) && vo.getItem().equals(po.getItem())) {
                return true;
            }
        }
        return false;
    }

    private ProductionOutputRealtimePO existInGroupByItemList(List<ProductionOutputRealtimePO> groupByItemList, ProductionOutputRealtimePO object) {
        for (ProductionOutputRealtimePO po : groupByItemList) {
            if (po.getResrce().equals(object.getResrce()) && po.getItem().equals(object.getItem())) {
                return po;
            }
        }

        return null;
    }

    public List<ExistingObject> checkExistingItemList(String site, List<String> itemlist) {
        List<ExistingObject> objects = new ArrayList<ExistingObject>();
        for (String item : itemlist) {
            ExistingObject object = new ExistingObject();
            object.setKey(item);

            List<ItemPO> readItemsByEnabledItem = itemComponent.readItemsByEnabledItem(site, item, "true");
            if(readItemsByEnabledItem != null && readItemsByEnabledItem.size() != 0) {
                object.setExist(true);
                object.setDescription(readItemsByEnabledItem.get(0).getDescription());
                object.setModel(readItemsByEnabledItem.get(0).getModel());
            } else {
                object.setExist(false);
            }

            objects.add(object);
        }

        return objects;
    }

    public List<ExistingObject> checkExistingResourceList(String site, List<String> resourcelist) {
        List<ExistingObject> objects = new ArrayList<ExistingObject>();
        for (String resource : resourcelist) {
            ExistingObject object = new ExistingObject();
            object.setKey(resource);

            ResourcePO po = resourceComponent.getResourceByKey(site, resource);
            if(po != null) {
                object.setExist(true);
                object.setValue(po.getWorkArea());
                object.setDescription(po.getDescription());
            } else {
                object.setExist(false);
                object.setValue(null);
            }

            objects.add(object);
        }

        return objects;
    }

    public void changeProductionOutput(List<YieldSearchVO> productionOutputVO) throws UpdateErrorException, ExistingRecordException {
        for(YieldSearchVO vo : productionOutputVO) {
            if(vo.getHandle() != null && vo.getHandle().trim().length() != 0 ){
                //change
                this.productionOutputComponent.updateProductionOutputFirst(vo);
            } else {
                //save
                ProductionOutputFirstPO po = new ProductionOutputFirstPO();
                po.setSite(vo.getSite());
                po.setResrce(vo.getResrce());
                po.setOperation("*");
                po.setOperationRevision("*");
                po.setItem(vo.getItem());
                po.setItemRevision("*");
                po.setStartDateTime(vo.getStartDateTime());
                po.setEndDateTime(vo.getEndDateTime());

                po.setStrt(Utils.getStringFromWebByDate(vo.getStartDateTime()));
                po.setShift(vo.getShift());
                po.setShiftDescription(vo.getShiftDescription());
                po.setQtyYield(vo.getGoodQuity());
                po.setQtyScrap(vo.getBadQuity());
                po.setQtyUnconfirm(new BigDecimal(0));

                ProductionOutputFirstBOHandle handle = new ProductionOutputFirstBOHandle(po.getSite(), po.getResrce(), po.getOperation(),
                                                             po.getOperationRevision(), po.getItem(), po.getItemRevision(), po.getStrt());

                po.setHandle(handle.getValue());
                this.productionOutputComponent.insertProductionOutputFirst(po);

            }
        }

    }

}
