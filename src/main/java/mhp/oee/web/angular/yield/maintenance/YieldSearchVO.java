package mhp.oee.web.angular.yield.maintenance;

import java.math.BigDecimal;
import java.util.Date;

public class YieldSearchVO {

    private String handle;
    private String site;
    private String resrce;
    private String resrceDescription;
    private String item;
    private String model;

    private String productionDate;
    private String shift;
    private String createdDateDec;

    private BigDecimal totalOutput;
    private BigDecimal goodQuity;
    private BigDecimal badQuity;

    private Date modifiedDateTime;


    /*used to save*/
    private Date startDateTime;
    private Date endDateTime;
    private String shiftDescription;

    public String getSite() {
        return site;
    }
    public void setSite(String site) {
        this.site = site;
    }
    public String getResrce() {
        return resrce;
    }
    public void setResrce(String resrce) {
        this.resrce = resrce;
    }
    public String getItem() {
        return item;
    }
    public void setItem(String item) {
        this.item = item;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public String getProductionDate() {
        return productionDate;
    }
    public void setProductionDate(String productionDate) {
        this.productionDate = productionDate;
    }
    public String getShift() {
        return shift;
    }
    public void setShift(String shift) {
        this.shift = shift;
    }
    public BigDecimal getTotalOutput() {
        return totalOutput;
    }
    public void setTotalOutput(BigDecimal totalOutput) {
        this.totalOutput = totalOutput;
    }
    public BigDecimal getGoodQuity() {
        return goodQuity;
    }
    public void setGoodQuity(BigDecimal goodQuity) {
        this.goodQuity = goodQuity;
    }
    public BigDecimal getBadQuity() {
        return badQuity;
    }
    public void setBadQuity(BigDecimal badQuity) {
        this.badQuity = badQuity;
    }
    public String getResrceDescription() {
        return resrceDescription;
    }
    public void setResrceDescription(String resrceDescription) {
        this.resrceDescription = resrceDescription;
    }
    public String getHandle() {
        return handle;
    }
    public void setHandle(String handle) {
        this.handle = handle;
    }
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }
    public Date getStartDateTime() {
        return startDateTime;
    }
    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }
    public Date getEndDateTime() {
        return endDateTime;
    }
    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }
    public String getShiftDescription() {
        return shiftDescription;
    }
    public void setShiftDescription(String shiftDescription) {
        this.shiftDescription = shiftDescription;
    }
    public String getCreatedDateDec() {
        return createdDateDec;
    }
    public void setCreatedDateDec(String createdDateDec) {
        this.createdDateDec = createdDateDec;
    }

}
