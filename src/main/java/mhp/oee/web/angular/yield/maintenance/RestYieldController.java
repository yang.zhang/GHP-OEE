package mhp.oee.web.angular.yield.maintenance;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ResourceConditionVO;
import mhp.oee.vo.WorkCenterShiftConditionVO;
import mhp.oee.web.Constants;

@RestController
public class RestYieldController {

    @Autowired
    private YieldService yieldService;

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    /*
     * resource
     */
    @RequestMapping(value = "/web/rest/yield/site/{site}/resource",
            method = RequestMethod.GET,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceConditionVO>> searchResource(@PathVariable("site") String site,
                                                                    @RequestParam(value = "resrce", required = false) String resrce,
                                                                    @RequestParam(value = "lineArea", required = false) String lineArea,
                                                                    @RequestParam(value = "workArea", required = false) String workArea,
                                                                    @RequestParam(value = "resourceType", required = false) String resourceType) {
        List<ResourceConditionVO> vos = yieldService.getResourcesByCondition(site, resrce, lineArea, workArea, resourceType);
        return ResponseEntity.ok(vos);
    }

    /*
     * work center shift from workcenter
     */
    @RequestMapping(value = "/web/rest/yield/site/{site}/workCenterShift",
            method = RequestMethod.GET,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<WorkCenterShiftConditionVO>> readWorkCenterShift(@PathVariable("site") String site,
            @RequestParam(value = "resrce", required = true) String resrce) {
        List<WorkCenterShiftConditionVO> vos = yieldService.getWorkCenterShiftByCondition(site, resrce);
        return ResponseEntity.ok(vos);
    }


    @RequestMapping(value = "/web/rest/yield/site/{site}/check_existing_item",
            method = RequestMethod.GET,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> checkExistingItem(@PathVariable("site") String site,
            @RequestParam(value = "item", required = true) String item) {
       String[] itemArray = item.split(",");
       List<String> itemlist = Arrays.asList(itemArray);
       List<ExistingObject> existingList = this.yieldService.checkExistingItemList(site, itemlist);

       return ResponseEntity.ok().body(gson.toJson(existingList));
    }


    @RequestMapping(value = "/web/rest/yield/site/{site}/check_existing_resource",
            method = RequestMethod.GET,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> checkExistingStdCycleTimeKey(@PathVariable("site") String site,
            @RequestParam(value = "resrce", required = true) String resrce) {
        String[] resourceArray = resrce.split(",");
        List<String> resourcelist = Arrays.asList(resourceArray);
        List<ExistingObject> existingList = this.yieldService.checkExistingResourceList(site, resourcelist);

        return ResponseEntity.ok().body(gson.toJson(existingList));
    }


    /*
     * search function
     * changed by Requirement
     */
    @RequestMapping(value = "/web/rest/yield/site/{site}/production_out",
            method = RequestMethod.GET,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<YieldSearchVO>> searchProductionOutput(@PathVariable("site") String site,
                            @RequestParam(value = "resrce", required = true) String resrce,
                            @RequestParam(value = "item", required = false) String item,
                            @RequestParam(value = "productionDate", required = true) String productionDate,
                            @RequestParam(value = "startTime", required = true) String startTime,
                            @RequestParam(value = "endTime", required = true) String endTime,
                            @RequestParam(value = "shift", required = true) String shift,
                            @RequestParam(value = "shiftDescription", required = true) String shiftDescription) {
        //resrce           设备编码
        //item             物料编码 （可选）
        //productionDate   生产日期
        //startTime, shift 班次及开始时间
        //model            显示
        logger.debug(">>searchProductionOutput     resrce:" + resrce +
                     "  item:" + item +
                     "  productionDate:" + productionDate +
                     "  startTime:" + startTime +
                     "  shift:" + shift);
        List<YieldSearchVO> vos = null;
        String[] resrceArray = resrce.split(",");
        List<String> resrcelist = Arrays.asList(resrceArray);

        try {
            if(item == null || item.trim().length() == 0) {
                vos = yieldService.searchProductionOutputWithoutItem(site, resrcelist, productionDate, startTime, endTime, shift, shiftDescription);
            } else {
                String[] itemArray = item.split(",");
                List<String> itemlist = Arrays.asList(itemArray);

                vos = yieldService.searchProductionOutputWithItem(site, resrcelist, itemlist, productionDate, startTime, endTime, shift, shiftDescription);
            }

            logger.debug(gson.toJson(vos));
            return ResponseEntity.ok(vos);
        } catch (ParseException e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }

    }

    /*
     * save and change
     */
    @RequestMapping(value = "/web/rest/yield/change_production_out",
            method = RequestMethod.POST,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Object> changeProductionOut(@RequestBody List<YieldSearchVO> productionOutputVO) {
        try {
            logger.debug(gson.toJson(productionOutputVO));
            yieldService.changeProductionOutput(productionOutputVO);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok().body(null);
    }

}
