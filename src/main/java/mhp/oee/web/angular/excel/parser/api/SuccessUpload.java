package mhp.oee.web.angular.excel.parser.api;

public class SuccessUpload {

    private Integer commitNumber;

    public SuccessUpload(Integer i) {
        this.commitNumber = i;
    }

    public Integer getCommitNumber() {
        return commitNumber;
    }

    public void setCommitNumber(Integer commitNumber) {
        this.commitNumber = commitNumber;
    }
}
