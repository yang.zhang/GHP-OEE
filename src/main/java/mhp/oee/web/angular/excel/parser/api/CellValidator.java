package mhp.oee.web.angular.excel.parser.api;

import org.slf4j.Logger;

public abstract class CellValidator {

    public CustomerCell cell;

    public static Logger logger;

    public abstract void validator();

}
