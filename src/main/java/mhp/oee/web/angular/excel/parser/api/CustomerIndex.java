package mhp.oee.web.angular.excel.parser.api;

public interface CustomerIndex {
    public Integer getIndex();
    public String getValue();
}
