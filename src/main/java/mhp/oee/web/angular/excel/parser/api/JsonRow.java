package mhp.oee.web.angular.excel.parser.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class
JsonRow {
    private Map<String, String> originDataMap = new HashMap<String, String>();
    private List<Map<String, List<String>>> exceptionList = new ArrayList<Map<String,List<String>>>();
    private Integer rowNumber;

    public Map<String, String> getOriginDataMap() {
        return originDataMap;
    }
    public void setOriginDataMap(Map<String, String> originDataMap) {
        this.originDataMap = originDataMap;
    }
    public List<Map<String, List<String>>> getExceptionList() {
        return exceptionList;
    }
    public void setExceptionList(List<Map<String, List<String>>> exceptionList) {
        this.exceptionList = exceptionList;
    }
    public Integer getRowNumber() {
        return rowNumber;
    }
    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

}