package mhp.oee.web.angular.excel.parser.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mhp.oee.common.exception.BusinessException;

public class CustomerCell {

    private CustomerIndex key;
    private String value;
    private Integer lineNumber;
    private String site;
    private List<BusinessException> exceptionList = new ArrayList<BusinessException>();
    private List<CellValidator> validatorList = new ArrayList<CellValidator>();

    public CustomerCell(CustomerIndex key, String value, String site) {
        this.key = key;
        this.value = value;
        this.site = site;
    }

    public void setValueAndLineNumber(String value, int rowNumber) {
        this.lineNumber = rowNumber;
        this.value = value;
        this.notifyValidator();
    }

    private void notifyValidator() {
        for (CellValidator v : validatorList) {
            v.validator();
        }
    }

    public String getValue() {
        return value;
    }

    public CustomerIndex getKey() {
        return this.key;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void attach(CellValidator CellValidator) {
        this.validatorList.add(CellValidator);
    }

    public List<BusinessException> getExceptionList() {
        return exceptionList;
    }

    public void addException(BusinessException e) {
        this.exceptionList.add(e);
    }

    public String getSite() {
        return site;
    }

    public Map<String, List<String>> getExceptionConvert() {
        Map<String, List<String>> map = new HashMap<String, List<String>>();

        if (exceptionList.size() != 0) {
            List<String> eList = new ArrayList<String>();
            for (BusinessException b : exceptionList) {
                eList.add(b.getMessage());
            }
            map.put(this.getKey().getValue(), eList);
        } else {
            map = null;
        }

        return map;
    }

}
