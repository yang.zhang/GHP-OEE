package mhp.oee.web.angular.excel.parser.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class CustomerRow {

    private List<CustomerCell> cellArray = new ArrayList<CustomerCell>();
    private Integer excelRowNumber;

    public void setCellValue(int index, int rowNumber, String value) {
        this.excelRowNumber = rowNumber;
        cellArray.get(index).setValueAndLineNumber(value, rowNumber);
    }

    public String getValue(CustomerIndex index) {
        for (CustomerCell o : cellArray) {
            if (o.getKey().equals(index)) {
                return o.getValue();
            }
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuffer output = new StringBuffer();
        for (CustomerCell o : cellArray) {
            output.append(o.getValue()).append("    ");
        }

        return output.toString();
    }

    public boolean isEmptyRow() {
        if (toString().trim().length() != 0) {
            return false;
        }
        return true;
    }

    public List<CustomerCell> getCellArray() {
        return cellArray;
    }

    private Map<String, String> getOriginRowData() {
        Map<String, String> map = new HashMap<String, String>();
        for (CustomerCell cell : cellArray) {
            map.put(cell.getKey().getValue(), cell.getValue());
        }
        return map;
    }

    public JsonRow generateRowError() {
        JsonRow jRow = new JsonRow();
        Map<String, String> data = this.getOriginRowData();
        jRow.setRowNumber(this.excelRowNumber);
        jRow.setOriginDataMap(data);
        for (CustomerCell cell : this.cellArray) {
            Map<String, List<String>> exceptionConvert = cell.getExceptionConvert();
            if (cell.getExceptionList().size() != 0) {
                jRow.getExceptionList().add(exceptionConvert);
            }
        }

        if (jRow.getExceptionList().size() == 0) {
            jRow = null;
        }

        return jRow;
    }

    public abstract Object getVO(String site);

}
