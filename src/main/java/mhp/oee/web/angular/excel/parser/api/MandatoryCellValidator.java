package mhp.oee.web.angular.excel.parser.api;

import mhp.oee.common.exception.MandatoryException;
import mhp.oee.common.logging.GenericLoggerFactory;

public class MandatoryCellValidator extends CellValidator {



    public MandatoryCellValidator(CustomerCell cell){
        this.cell = cell;
        this.cell.attach(this);
        logger = GenericLoggerFactory.getLogger(MandatoryCellValidator.class);
     }

    //false -- no issue
    //true -- error
    @Override
    public void validator() {
        if(this.cell.getValue().trim().length() == 0){
           this.cell.addException(new MandatoryException());
           logger.debug("Line: " + this.cell.getLineNumber() + " Column: " + this.cell.getKey().getIndex()
                    + " was empty");
        }
    }









}
