package mhp.oee.web.angular.job;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.dao.gen.ActivityPOMapper;
import mhp.oee.dao.gen.ActivityParamPOMapper;
import mhp.oee.dao.gen.JobLogPOMapper;
import mhp.oee.po.gen.ActivityPO;
import mhp.oee.po.gen.ActivityPOExample;
import mhp.oee.po.gen.ActivityParamPO;
import mhp.oee.po.gen.ActivityParamPOExample;
import mhp.oee.po.gen.JobLogPO;
import mhp.oee.po.gen.JobLogPOExample;

@Service
@Transactional(rollbackFor = { Exception.class })
public class ApiJobService {
	 @Autowired
    private ActivityPOMapper activityPOMapper;
    @Autowired
    private ActivityParamPOMapper activityParamPOMapper;
    @Autowired
    private JobLogPOMapper jobLogPOMapper;
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    public List<ActivityPO> jobSelect(){
    	 ActivityPOExample example = new ActivityPOExample();
         example.createCriteria().andActivityLike("JOB000%");
         List<ActivityPO> vos = activityPOMapper.selectByExample(example); 
         return vos;
    }
    
    public List<ActivityParamPO> jobRunTimeSelect(String activity_bo){
    	ActivityParamPOExample  example = new ActivityParamPOExample();
    	example.createCriteria().andActivityBoEqualTo(activity_bo).andParamIdNotEqualTo("SERVER_NODE");
    	List<ActivityParamPO> vos = activityParamPOMapper.selectByExample(example);
    	return vos;
    }
    
   
	public List<JobLogPO> jobRunStatusList(String job_name,String job_run_time,String job_run_day) throws ParseException{   	
    	
    	if(job_name.equals("JOB0001")){
    		job_name="JOB_REASON_CODE_RECOGNITION";
    	}else if(job_name.equals("JOB0002")){
    		job_name="HEART_BEAT";
    	}else{
    		job_name="PRODUCT_JOB";
    	}  
    	JobLogPOExample example = new JobLogPOExample();
    	if(job_run_time!=null&&!job_run_time.equals("")){
    		String start_date_time = job_run_day+" "+job_run_time;      	
        	example.createCriteria().andStartDateTimeEqualTo(sdf.parse(start_date_time)).andJobEqualTo(job_name);
    	}else{
    		example.createCriteria().andStartDateTimeBetween(sdf.parse(job_run_day+" 00:00:00"), sdf.parse(job_run_day+" 24:00:00")).andJobEqualTo(job_name);
    	}   	
    	List<JobLogPO> vos = jobLogPOMapper.selectByExample(example);
    	return vos;
    }
    
    public void jobServerNodeUpdate(String node_num){   
    	ActivityParamPO po =activityParamPOMapper.selectByPrimaryKey("ActivityParamBO:ActivityBO:JOB0001,SERVER_NODE");
    	po.setParamDefaultValue(node_num);
    	activityParamPOMapper.updateByPrimaryKey(po);
    }
    
}
