package mhp.oee.web.angular.job;

import mhp.oee.api.v1.heartbeat.HeartbeatOverTimeJob;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.dao.gen.ActivityPOMapper;
import mhp.oee.dao.gen.ActivityParamPOMapper;
import mhp.oee.dao.gen.JobLogPOMapper;
import mhp.oee.job.productjob.ProductJob;
import mhp.oee.job.reasoncodereconizejob.RecognizeReasonCodeJob;
import mhp.oee.po.gen.ActivityPO;
import mhp.oee.po.gen.ActivityParamPO;
import mhp.oee.po.gen.JobLogPO;
import mhp.oee.utils.Utils;
import mhp.oee.web.Constants;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by LinZuK on 2016/10/13.
 */
@RestController
public class ApiJobController {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private ProductJob productJob;
    @Autowired
    private RecognizeReasonCodeJob recognizeReasonCodeJob;
    @Autowired
    private HeartbeatOverTimeJob heartbeatOverTimeJob;

    @Autowired
    private ActivityPOMapper activityPOMapper;
    @Autowired
    private ActivityParamPOMapper activityParamPOMapper;
    @Autowired
    private JobLogPOMapper jobLogPOMapper;

    @Autowired
    private ApiJobService apiJobService;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();


    /**
     * 设备心跳超时检测作业（手动执行用）
     * dev_over_time: 设备心跳超时时间
     * */
    @RequestMapping(value = "/api/v1/job/dev_over_time/{devOverTime}/heartbeat_job",
            method = RequestMethod.POST,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> heartbeatJob(@PathVariable("devOverTime") final int devOverTime) {
        heartbeatOverTimeJob.execute(devOverTime);
        return ResponseEntity.ok("设备心跳超时检测作业已启动");
    }

    /**
     * 产出作业（手动执行用）
     * */
    @RequestMapping(value = "/api/v1/job/product_job",
            method = RequestMethod.POST,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> productJob() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    productJob.execute(null);
                } catch (Exception e) {
                    logger.error("产出作业执行错误", e);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
        return ResponseEntity.ok("产出作业已启动");
    }

    /**
     * job管理下拉选项
     */
    @RequestMapping(value = "/web/rest/job/job_select",
            method = RequestMethod.GET,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> jobSelect() {
        List<ActivityPO> vos =null;
        try {
            vos = apiJobService.jobSelect();
        } catch (Exception e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< [ApiJobController.jobSelect]  response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    /**
     * job管理运行时间下拉框
     */
    @RequestMapping(value = "/web/rest/job/job_run_time_select",
            method = RequestMethod.GET,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> jobRunTimeSelect(
            @RequestParam(value = "activity_bo", required = true) String activity_bo ) {
        String trace = String.format("[ApiJobController.jobRunTimeSelect] request : activity_bo=%s", activity_bo);
        logger.debug(">> " + trace);
        List<ActivityParamPO> vos =null;
        try {
            vos = apiJobService.jobRunTimeSelect(activity_bo);
        } catch (Exception e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< [ApiJobController.jobRunTimeSelect]  response : " + gson.toJson(vos));

        return ResponseEntity.ok(vos);
    }

    /**
     * job管理运行查找
     */
    @RequestMapping(value = "/web/rest/job/job_run_status_list",
            method = RequestMethod.GET,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> jobRunStatusList(
            @RequestParam(value = "job_name", required = true) String job_name,
            @RequestParam(value = "job_run_time", required = true) String job_run_time,
            @RequestParam(value = "job_run_day", required = true) String job_run_day) {
        String trace = String.format("[ApiJobController.jobRunStatusList] request : job_name=%s,job_run_time=%s,job_run_day=%s", job_name,job_run_time,job_run_day);
        logger.debug(">> " + trace);
        List<JobLogPO> vos =null;
        try {
            vos = apiJobService.jobRunStatusList(job_name, job_run_time, job_run_day);
        } catch (Exception e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< [ApiJobController.jobRunStatusList]  response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    /**
     * servernode手动修改
     */
    @RequestMapping(value = "/web/rest/job/job_server_node_update",
            method = RequestMethod.GET,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> jobServerNodeUpdate(
            @RequestParam(value = "node_num", required = true) String node_num
    ) {
        String trace = String.format("[ApiJobController.jobRunStatusList] request : node_num=%s", node_num);
        logger.debug(">> " + trace);

        try {
            apiJobService.jobServerNodeUpdate(node_num);
        } catch (Exception e) {
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + " " + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        logger.debug("<< [ApiJobController.jobRunStatusList]  response : " + gson.toJson(null));
        return ResponseEntity.ok(null);
    }


    @RequestMapping(value = "/web/rest/job/reason_code_recognize_job",
            method = RequestMethod.POST,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> reasonCodeRecognizeJob(@RequestBody final String handle) {

        System.out.println(">>>>>>>   "+handle);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    recognizeReasonCodeJob.execute(handle);
                } catch (Exception e) {
                    logger.error("原因规则识别作业执行错误", e);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
        JobVo vo = new JobVo();
        vo.setMessage("原因规则识别作业已启动");
        return ResponseEntity.ok(vo);
    }

    private class JobVo {
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public JobVo() {
            super();
        }


    }

}
