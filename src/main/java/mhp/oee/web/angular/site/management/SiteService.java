package mhp.oee.web.angular.site.management;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.MandatoryException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.SiteComponent;
import mhp.oee.vo.SiteVO;

@Service
@Transactional(rollbackFor = { BusinessException.class})
public class SiteService {

    @Autowired
    SiteComponent siteComponent;

    @InjectableLogger
    private static Logger logger;

    public List<SiteVO> getAllSite() {
        return siteComponent.getAllSite();
    }


    /*
     * site can only be added and update by description
     */
    public void changeSite(List<SiteVO> vos) throws BusinessException{
        for (SiteVO each : vos) {
            String site = each.getSite();
            if(site.trim().length() == 0) {
                throw new MandatoryException();
            }

            if (each.getViewActionCode().equals('C')) {
                this.siteComponent.createSite(each);
            } else if (each.getViewActionCode().equals('U')) {
                this.siteComponent.updateSiteByVO(each);
            } else {
                logger.error("PlantService.cudPlcCategoryChanges() : Invalid viewActionCode '"
                        + each.getViewActionCode() + "'.");
            }
        }

    }


}

