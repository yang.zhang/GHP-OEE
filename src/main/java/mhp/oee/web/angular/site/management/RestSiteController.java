package mhp.oee.web.angular.site.management;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.SiteVO;
import mhp.oee.web.Constants;

@RestController
public class RestSiteController {


    @Autowired
    private SiteService siteSerive;

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();


    @RequestMapping(value = "/web/rest/site/site_list",
            method = RequestMethod.GET,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<SiteVO>> readAllPlcAccessType() {

        List<SiteVO> vos = siteSerive.getAllSite();
        return ResponseEntity.ok(vos);
    }


    @RequestMapping(value = "/web/rest/site/change_site", method = RequestMethod.POST)
    public ResponseEntity<?> changeSite(@RequestBody List<SiteVO> vos) {
        try {
            this.siteSerive.changeSite(vos);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("RestSiteController.changeSite() : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok().body(null);
    }

}
