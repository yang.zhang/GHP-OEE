package mhp.oee.web.angular.reasoncode.recognition;

import java.util.List;

import mhp.oee.vo.ActivityParamVO;

public class ActivityParamComVO {
    private String reasonCodePriorityBO;
    private String site;
    private String activity;
    private String activityCode;
    List<ActivityParamVO> ruleParams;

    public String getReasonCodePriorityBO() {
        return reasonCodePriorityBO;
    }

    public void setReasonCodePriorityBO(String reasonCodePriorityBO) {
        this.reasonCodePriorityBO = reasonCodePriorityBO;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public List<ActivityParamVO> getRuleParams() {
        return ruleParams;
    }

    public void setRuleParams(List<ActivityParamVO> ruleParams) {
        this.ruleParams = ruleParams;
    }

}
