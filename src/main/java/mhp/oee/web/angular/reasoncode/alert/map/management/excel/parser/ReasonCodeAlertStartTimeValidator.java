package mhp.oee.web.angular.reasoncode.alert.map.management.excel.parser;

import java.util.Date;
import java.util.List;

import mhp.oee.common.exception.InvalidReasonCodeAlertUploadException;
import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.common.logging.GenericLoggerFactory;
import mhp.oee.common.security.SpringContextHolder;
import mhp.oee.component.plant.ReasonCodeAlertComponent;
import mhp.oee.po.gen.ReasonCodeAlertPO;
import mhp.oee.web.angular.excel.parser.api.CellValidator;
import mhp.oee.web.angular.reasoncode.alert.map.management.excel.parser.ReasonCodeAlertCell.ReasonCodeAlertIndex;
import mhp.oee.web.angular.resource.plc.management.excel.parser.ExistingCellValidator;

public class ReasonCodeAlertStartTimeValidator extends CellValidator {

    private Date date;

    public ReasonCodeAlertStartTimeValidator(ReasonCodeAlertCell cell, Date date){
            this.date = date;
            this.cell = cell;
            this.cell.attach(this);
            logger = GenericLoggerFactory.getLogger(ExistingCellValidator.class);
         }

    @Override
    public void validator() {
        if (this.cell.getValue().trim().length() == 0) {
            return;
        }

        if (this.cell.getKey().equals(ReasonCodeAlertIndex.ResourceType)) {
            String handle = new ResourceTypeBOHandle(this.cell.getSite(), this.cell.getValue()).getValue();
            ReasonCodeAlertComponent dao = (ReasonCodeAlertComponent) SpringContextHolder.getBean("reasonCodeAlertComponent");
            List<ReasonCodeAlertPO> list = dao.getReasonCodeAlertByResourceType(handle);
            for(ReasonCodeAlertPO po : list) {
                if(!date.after(po.getStartDateTime())) {
                    this.cell.addException(new InvalidReasonCodeAlertUploadException());
                }
            }

            return;
        }

    }
}