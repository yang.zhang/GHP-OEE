package mhp.oee.web.angular.reasoncode.management;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ReasonCodeVO;
import mhp.oee.web.Constants;

@RestController
public class ReasonCodeController {

    @InjectableLogger
    private static Logger logger;
    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ReasonCodeUiService reasonCodeService;

    @RequestMapping(value = "/web/rest/plant/site/{site}/reason_code_page",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ReasonCodeVO>> readAllResonCode(
            @PathVariable("site") String site,
            @RequestParam(value = "reason_code_group", required = true) String reasonCodeGroup,
            @RequestParam(value = "enabled", required = false) String enabled,
            @RequestParam(value = "index", required = true) int index,
            @RequestParam(value = "count", required = true) int count) {
        String trace = String.format("[ReasonCodeController.readAllResonCode] request : site=%s, reasonCodeGroup=%s, enabled=%s, index=%s, count=%s",
                site, reasonCodeGroup, enabled, index, count);
        logger.debug(">> " + trace);
        List<ReasonCodeVO> vos = reasonCodeService.readAllResonCode(site, reasonCodeGroup, enabled, index, count);
        logger.debug("<< [ReasonCodeController.readAllResonCode] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/site/{site}/reason_code_all",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ReasonCodeVO>> readAllResonCodeAll(
            @PathVariable("site") String site,
            @RequestParam(value = "reason_code_group", required = true) String reasonCodeGroup,
            @RequestParam(value = "enabled", required = false) String enabled) {
        String trace = String.format("[ReasonCodeController.readAllResonCode] request : site=%s, reasonCodeGroup=%s, enabled=%s",
                site, reasonCodeGroup, enabled);
        logger.debug(">> " + trace);
        List<ReasonCodeVO> vos = reasonCodeService.readAllResonCodeAll(site, reasonCodeGroup, enabled);
        logger.debug("<< [ReasonCodeController.readAllResonCode] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/site/{site}/reason_code_page_count",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Integer> readAllResonCodeCount(
            @PathVariable("site") String site,
            @RequestParam(value = "reason_code_group", required = true) String reasonCodeGroup,
            @RequestParam(value = "enabled", required = false) String enabled) {
        String trace = String.format("[ReasonCodeController.readAllResonCodeCount] request : site=%s, reasonCodeGroup=%s, enabled=%s",
                site, reasonCodeGroup, enabled);
        logger.debug(">> " + trace);
        int vos = reasonCodeService.readAllResonCodeCount(site, reasonCodeGroup, enabled);
        logger.debug("<< [ReasonCodeController.readAllResonCodeCount] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/exist_reason_code",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> existResonCode(
            @PathVariable("site") String site, 
            @RequestParam(value = "reason_code", required = true) String resonCode) {
        String trace = String.format("[ReasonCodeController.existResonCode] request : site=%s, resonCode=%s",
                site, resonCode);
        logger.debug(">> " + trace);
        boolean vos = reasonCodeService.existResonCode(site, resonCode);
        logger.debug("<< [ReasonCodeController.existResonCode] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/change_reason_code",
            method = RequestMethod.POST)
    public ResponseEntity<?> changeReasonCode(@RequestBody List<ReasonCodeVO> reasonCodeGroupVOs) {
        String trace = String.format("[ReasonCodeController.changeReasonCode] request : " + gson.toJson(reasonCodeGroupVOs));
        logger.debug(">> " + trace);
        try {
            reasonCodeService.changeReasonCode(reasonCodeGroupVOs);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ReasonCodeController.changeReasonCode() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ReasonCodeController.changeReasonCode] response : void");
        return ResponseEntity.ok().body(null);
    }
}
