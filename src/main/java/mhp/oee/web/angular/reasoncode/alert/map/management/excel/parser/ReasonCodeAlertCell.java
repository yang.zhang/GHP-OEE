package mhp.oee.web.angular.reasoncode.alert.map.management.excel.parser;

import java.util.Date;
import java.util.List;

import mhp.oee.web.angular.excel.parser.api.CustomerCell;
import mhp.oee.web.angular.excel.parser.api.CustomerIndex;
import mhp.oee.web.angular.excel.parser.api.MandatoryCellValidator;

public class ReasonCodeAlertCell extends CustomerCell {
    private Date date;

    public ReasonCodeAlertCell(ReasonCodeAlertIndex key, String value, String site, Date date) {
        super(key, value, site);
        this.date = date;
    }

    public static List<CustomerCell> initCellAttribute(List<CustomerCell> list, String site, Date date) {

        ReasonCodeAlertCell deviceCell = new ReasonCodeAlertCell(ReasonCodeAlertIndex.ResourceType, "", site, date);
        new MandatoryCellValidator(deviceCell);
        new ReasonCodeAlertExistingValidator(deviceCell);
        new ReasonCodeAlertStartTimeValidator(deviceCell, date);
        list.add(ReasonCodeAlertIndex.ResourceType.getIndex(), deviceCell);

        ReasonCodeAlertCell alertCodeCell = new ReasonCodeAlertCell(ReasonCodeAlertIndex.AlertCode, "", site, date);
        new MandatoryCellValidator(alertCodeCell);
        list.add(ReasonCodeAlertIndex.AlertCode.getIndex(), alertCodeCell);


        ReasonCodeAlertCell desCell = new ReasonCodeAlertCell(ReasonCodeAlertIndex.Des, "", site, date);
        new MandatoryCellValidator(desCell);
        list.add(ReasonCodeAlertIndex.Des.getIndex(), desCell);


        ReasonCodeAlertCell reasonCodeCell = new ReasonCodeAlertCell(ReasonCodeAlertIndex.ReasonCode, "", site, date);
        new MandatoryCellValidator(reasonCodeCell);
        new ReasonCodeAlertExistingValidator(reasonCodeCell);
        list.add(ReasonCodeAlertIndex.ReasonCode.getIndex(), reasonCodeCell);

        return list;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public enum ReasonCodeAlertIndex implements CustomerIndex {
        ResourceType(0, "resourceTypeBo"),
        AlertCode(1, "alertSequenceId"),
        Des(2, "description"),
        ReasonCode(3, "reasonCodeBo");

        private Integer index;
        private String value;

        private ReasonCodeAlertIndex(Integer index, String value) {
            this.index = index;
            this.value = value;
        }

        @Override
        public String toString() {
            return this.index.toString();
        }

        @Override
        public Integer getIndex() {
            return this.index;
        }

        @Override
        public String getValue() {
            // TODO Auto-generated method stub
            return this.value;
        }
    }

}