package mhp.oee.web.angular.reasoncode.group.management;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ReasonCodeGroupVO;
import mhp.oee.web.Constants;

@RestController
public class ReasonCodeGroupController {

    @InjectableLogger
    private static Logger logger;
    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ReasonCodeGroupService reasonCodeGroupService;

    @RequestMapping(value = "/web/rest/plant/site/{site}/reason_code_group",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ReasonCodeGroupVO>> readAllResonCodeGroup(
            @PathVariable("site") String site,
            @RequestParam(value = "enabled", required = false) String enabled) {
        String trace = String.format("[ReasonCodeGroupController.readAllResonCodeGroup] request : site=%s, enabled=%s",
                site, enabled);
        logger.debug(">> " + trace);
        List<ReasonCodeGroupVO> vos = reasonCodeGroupService.readAllResonCodeGroup(site, enabled);
        logger.debug("<< [ReasonCodeGroupController.readAllResonCodeGroup] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/exist_reason_code_group",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> existResonCodeGroup(
            @PathVariable("site") String site, 
            @RequestParam(value = "reason_code_group", required = true) String resonCodeGroup) {
        String trace = String.format("[ReasonCodeGroupController.existResonCodeGroup] request : site=%s, resonCodeGroup=%s",
                site, resonCodeGroup);
        logger.debug(">> " + trace);
        boolean vos = reasonCodeGroupService.existResonCodeGroup(site, resonCodeGroup);
        logger.debug("<< [ReasonCodeGroupController.existResonCodeGroup] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/change_reason_code_group",
            method = RequestMethod.POST)
    public ResponseEntity<?> changeReasonCodeGroup(@RequestBody List<ReasonCodeGroupVO> reasonCodeGroupVOs) {
        String trace = String.format("[ReasonCodeGroupController.changeReasonCodeGroup] request : " + gson.toJson(reasonCodeGroupVOs));
        logger.debug(">> " + trace);
        try {
            reasonCodeGroupService.changeReasonCodeGroup(reasonCodeGroupVOs);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ReasonCodeGroupController.changeReasonCodeGroup() : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ReasonCodeGroupController.changeReasonCodeGroup] response : void");
        return ResponseEntity.ok().body(null);
    }

}
