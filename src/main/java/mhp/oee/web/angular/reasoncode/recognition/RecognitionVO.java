package mhp.oee.web.angular.reasoncode.recognition;

import mhp.oee.po.extend.*;
import mhp.oee.po.gen.ActivityParamPO;
import mhp.oee.po.gen.ReasonCodeRuleParamPO;
import mhp.oee.po.gen.ResourceTypePO;
import mhp.oee.vo.ActivityParamIdAndValueVO;
import mhp.oee.vo.ResourceTypeAndActivityParamValueVO;
import mhp.oee.vo.RuleParamVo;

import java.util.Date;
import java.util.List;

/**
 * Created by LinZuK on 2016/8/1.
 */
public class RecognitionVO {

    public static class ResourceTypeParamVO {
        private String reasonCodeRuleBo;
//        private String resourceTypeBo;
        private String paramId;
        private List<ResourceTypeAndActivityParamValueVO> resourceTypeAndActivityParamValue;

        public String getReasonCodeRuleBo() {
            return reasonCodeRuleBo;
        }

        public void setReasonCodeRuleBo(String reasonCodeRuleBo) {
            this.reasonCodeRuleBo = reasonCodeRuleBo;
        }
//
//        public String getResourceTypeBo() {
//            return resourceTypeBo;
//        }
//
//        public void setResourceTypeBo(String resourceTypeBo) {
//            this.resourceTypeBo = resourceTypeBo;
//        }

        public String getParamId() {
            return paramId;
        }

        public void setParamId(String paramId) {
            this.paramId = paramId;
        }

        public List<ResourceTypeAndActivityParamValueVO> getResourceTypeAndActivityParamValue() {
            return resourceTypeAndActivityParamValue;
        }

        public void setResourceTypeAndActivityParamValue(List<ResourceTypeAndActivityParamValueVO> resourceTypeAndActivityParamValue) {
            this.resourceTypeAndActivityParamValue = resourceTypeAndActivityParamValue;
        }

    }

    public static class ReasonCodeRuleVO {
        private String reasonCodePriorityBo;
        private String reasonCodeRuleBo;
        private String reasonCodeRuleModifiedDateTime;
        private String activityBo;
        private String activity;
        private Character viewActionCode;

        public String getReasonCodePriorityBo() {
            return reasonCodePriorityBo;
        }

        public void setReasonCodePriorityBo(String reasonCodePriorityBo) {
            this.reasonCodePriorityBo = reasonCodePriorityBo;
        }

        public String getReasonCodeRuleBo() {
            return reasonCodeRuleBo;
        }

        public void setReasonCodeRuleBo(String reasonCodeRuleBo) {
            this.reasonCodeRuleBo = reasonCodeRuleBo;
        }

        public String getActivityBo() {
            return activityBo;
        }

        public void setActivityBo(String activityBo) {
            this.activityBo = activityBo;
        }

        public String getActivity() {
            return activity;
        }

        public void setActivity(String activity) {
            this.activity = activity;
        }

        public Character getViewActionCode() {
            return viewActionCode;
        }

        public void setViewActionCode(Character viewActionCode) {
            this.viewActionCode = viewActionCode;
        }

        public String getReasonCodeRuleModifiedDateTime() {
            return reasonCodeRuleModifiedDateTime;
        }

        public void setReasonCodeRuleModifiedDateTime(String reasonCodeRuleModifiedDateTime) {
            this.reasonCodeRuleModifiedDateTime = reasonCodeRuleModifiedDateTime;
        }
    }

    public static class FilterActivityVO {
        private String reasonCodePriorityBo;
        private String activityF;
        private List<ActivityParamIdAndValueVO> activityParam;

        public String getReasonCodePriorityBo() {
            return reasonCodePriorityBo;
        }

        public void setReasonCodePriorityBo(String reasonCodePriorityBo) {
            this.reasonCodePriorityBo = reasonCodePriorityBo;
        }

        public String getActivityF() {
            return activityF;
        }

        public void setActivityF(String activityF) {
            this.activityF = activityF;
        }

        public List<ActivityParamIdAndValueVO> getActivityParam() {
            return activityParam;
        }

        public void setActivityParam(List<ActivityParamIdAndValueVO> activityParam) {
            this.activityParam = activityParam;
        }

    }


    public static class HistoryLogVO {

        private ReasonCodePriorityAndReasonCodePO reasonCodePO;
        private ActivityRPO activityRPO;
        private ActivityFPO activityFPO;
        private ModifiedPO modifiedPO;

        public ReasonCodePriorityAndReasonCodePO getReasonCodePO() {
            return reasonCodePO;
        }

        public void setReasonCodePO(ReasonCodePriorityAndReasonCodePO reasonCodePO) {
            this.reasonCodePO = reasonCodePO;
        }

        public ActivityRPO getActivityRPO() {
            return activityRPO;
        }

        public void setActivityRPO(ActivityRPO activityRPO) {
            this.activityRPO = activityRPO;
        }

        public ActivityFPO getActivityFPO() {
            return activityFPO;
        }

        public void setActivityFPO(ActivityFPO activityFPO) {
            this.activityFPO = activityFPO;
        }

        public ModifiedPO getModifiedPO() {
            return modifiedPO;
        }

        public void setModifiedPO(ModifiedPO modifiedPO) {
            this.modifiedPO = modifiedPO;
        }
    }


    public static class FilterParamButtonOutForNewVO {
        private ActivityParamPO activityParamPO;
        private List<ResourceTypePO> resourceTypePOList;

        public ActivityParamPO getActivityParamPO() {
            return activityParamPO;
        }

        public void setActivityParamPO(ActivityParamPO activityParamPO) {
            this.activityParamPO = activityParamPO;
        }

        public List<ResourceTypePO> getResourceTypePOList() {
            return resourceTypePOList;
        }

        public void setResourceTypePOList(List<ResourceTypePO> resourceTypePOList) {
            this.resourceTypePOList = resourceTypePOList;
        }
    }



    public static class ReasonCodeDetailVO {
        private String reasonCodePriorityBo;
        private String reasonCode;
        private String reasonCodeDesc;
        // 优先级
        private String priority;
        private String subPriority;
        private Date effDateTime;
        private String effDateTimeString;
        private Date effEndDateTime;
        private String effEndDateTimeString;
        private String used;
        private Date modifiedDateTime;
        private String modifiedDateTimeString;
        private String modifiedUser;
        // 识别方式
        private String reasonCodeRuleBoR;
        private String activityBoR;
        private String activityR;
        private String activityRDesc;
        private Date modifiedDateTimeR;
        private String modifiedDateTimeStringR;
        private String modifiedUserR;
        private List<ActivityParamAndRuleParamPO> activityRParam;
        // 防呆验证
        private String reasonCodeRuleBoF;
        private String activityBoF;
        private String activityF;
        private String activityFDesc;
        private Date modifiedDateTimeF;
        private String modifiedDateTimeStringF;
        private String modifiedUserF;
        private String activityFParamId;
        private String activityFParamDesc;
        private String activityFParamDefaultValue;
        private String activityFParamConType;
        private List<ActivityParamAndRuleParamPO> activityFParam;

        public Date getEffEndDateTime() {
            return effEndDateTime;
        }

        public void setEffEndDateTime(Date effEndDateTime) {
            this.effEndDateTime = effEndDateTime;
        }

        public String getEffEndDateTimeString() {
            return effEndDateTimeString;
        }

        public void setEffEndDateTimeString(String effEndDateTimeString) {
            this.effEndDateTimeString = effEndDateTimeString;
        }

        public String getActivityBoR() {
            return activityBoR;
        }

        public void setActivityBoR(String activityBoR) {
            this.activityBoR = activityBoR;
        }

        public String getActivityBoF() {
            return activityBoF;
        }

        public void setActivityBoF(String activityBoF) {
            this.activityBoF = activityBoF;
        }

        public String getReasonCodePriorityBo() {
            return reasonCodePriorityBo;
        }

        public void setReasonCodePriorityBo(String reasonCodePriorityBo) {
            this.reasonCodePriorityBo = reasonCodePriorityBo;
        }

        public String getReasonCode() {
            return reasonCode;
        }

        public void setReasonCode(String reasonCode) {
            this.reasonCode = reasonCode;
        }

        public String getReasonCodeDesc() {
            return reasonCodeDesc;
        }

        public void setReasonCodeDesc(String reasonCodeDesc) {
            this.reasonCodeDesc = reasonCodeDesc;
        }

        public String getPriority() {
            return priority;
        }

        public void setPriority(String priority) {
            this.priority = priority;
        }

        public String getSubPriority() {
            return subPriority;
        }

        public void setSubPriority(String subPriority) {
            this.subPriority = subPriority;
        }

        public Date getEffDateTime() {
            return effDateTime;
        }

        public void setEffDateTime(Date effDateTime) {
            this.effDateTime = effDateTime;
        }

        public String getEffDateTimeString() {
            return effDateTimeString;
        }

        public void setEffDateTimeString(String effDateTimeString) {
            this.effDateTimeString = effDateTimeString;
        }

        public String getUsed() {
            return used;
        }

        public void setUsed(String used) {
            this.used = used;
        }

        public Date getModifiedDateTime() {
            return modifiedDateTime;
        }

        public void setModifiedDateTime(Date modifiedDateTime) {
            this.modifiedDateTime = modifiedDateTime;
        }

        public String getModifiedDateTimeString() {
            return modifiedDateTimeString;
        }

        public void setModifiedDateTimeString(String modifiedDateTimeString) {
            this.modifiedDateTimeString = modifiedDateTimeString;
        }

        public String getModifiedUser() {
            return modifiedUser;
        }

        public void setModifiedUser(String modifiedUser) {
            this.modifiedUser = modifiedUser;
        }

        public String getReasonCodeRuleBoR() {
            return reasonCodeRuleBoR;
        }

        public void setReasonCodeRuleBoR(String reasonCodeRuleBoR) {
            this.reasonCodeRuleBoR = reasonCodeRuleBoR;
        }

        public String getActivityR() {
            return activityR;
        }

        public void setActivityR(String activityR) {
            this.activityR = activityR;
        }

        public String getActivityRDesc() {
            return activityRDesc;
        }

        public void setActivityRDesc(String activityRDesc) {
            this.activityRDesc = activityRDesc;
        }

        public List<ActivityParamAndRuleParamPO> getActivityRParam() {
            return activityRParam;
        }

        public void setActivityRParam(List<ActivityParamAndRuleParamPO> activityRParam) {
            this.activityRParam = activityRParam;
        }

        public String getReasonCodeRuleBoF() {
            return reasonCodeRuleBoF;
        }

        public void setReasonCodeRuleBoF(String reasonCodeRuleBoF) {
            this.reasonCodeRuleBoF = reasonCodeRuleBoF;
        }

        public String getActivityF() {
            return activityF;
        }

        public void setActivityF(String activityF) {
            this.activityF = activityF;
        }

        public String getActivityFDesc() {
            return activityFDesc;
        }

        public void setActivityFDesc(String activityFDesc) {
            this.activityFDesc = activityFDesc;
        }

        public String getActivityFParamId() {
            return activityFParamId;
        }

        public void setActivityFParamId(String activityFParamId) {
            this.activityFParamId = activityFParamId;
        }

        public String getActivityFParamDesc() {
            return activityFParamDesc;
        }

        public void setActivityFParamDesc(String activityFParamDesc) {
            this.activityFParamDesc = activityFParamDesc;
        }

        public String getActivityFParamDefaultValue() {
            return activityFParamDefaultValue;
        }

        public void setActivityFParamDefaultValue(String activityFParamDefaultValue) {
            this.activityFParamDefaultValue = activityFParamDefaultValue;
        }

        public String getActivityFParamConType() {
            return activityFParamConType;
        }

        public void setActivityFParamConType(String activityFParamConType) {
            this.activityFParamConType = activityFParamConType;
        }

        public List<ActivityParamAndRuleParamPO> getActivityFParam() {
            return activityFParam;
        }

        public void setActivityFParam(List<ActivityParamAndRuleParamPO> activityFParam) {
            this.activityFParam = activityFParam;
        }

        public Date getModifiedDateTimeR() {
            return modifiedDateTimeR;
        }

        public void setModifiedDateTimeR(Date modifiedDateTimeR) {
            this.modifiedDateTimeR = modifiedDateTimeR;
        }

        public String getModifiedDateTimeStringR() {
            return modifiedDateTimeStringR;
        }

        public void setModifiedDateTimeStringR(String modifiedDateTimeStringR) {
            this.modifiedDateTimeStringR = modifiedDateTimeStringR;
        }

        public String getModifiedUserR() {
            return modifiedUserR;
        }

        public void setModifiedUserR(String modifiedUserR) {
            this.modifiedUserR = modifiedUserR;
        }

        public Date getModifiedDateTimeF() {
            return modifiedDateTimeF;
        }

        public void setModifiedDateTimeF(Date modifiedDateTimeF) {
            this.modifiedDateTimeF = modifiedDateTimeF;
        }

        public String getModifiedDateTimeStringF() {
            return modifiedDateTimeStringF;
        }

        public void setModifiedDateTimeStringF(String modifiedDateTimeStringF) {
            this.modifiedDateTimeStringF = modifiedDateTimeStringF;
        }

        public String getModifiedUserF() {
            return modifiedUserF;
        }

        public void setModifiedUserF(String modifiedUserF) {
            this.modifiedUserF = modifiedUserF;
        }
    }


    public static class ReasonCodeRuleParamVO {
        private String actionCode;
        private String reasonCodePriorityBo;
        private String activity;
        private String activityBo;
        private Date modifiedDateTime; // REASON_CODE_RULE 表 MODIFIED_DATE_TIME字段
        private List<RuleParamVo> ruleParam;
        private String reasonCodeRuleBo;

        public String getReasonCodeRuleBo() {
            return reasonCodeRuleBo;
        }

        public void setReasonCodeRuleBo(String reasonCodeRuleBo) {
            this.reasonCodeRuleBo = reasonCodeRuleBo;
        }

        public String getActionCode() {
            return actionCode;
        }

        public void setActionCode(String actionCode) {
            this.actionCode = actionCode;
        }

        public String getReasonCodePriorityBo() {
            return reasonCodePriorityBo;
        }

        public void setReasonCodePriorityBo(String reasonCodePriorityBo) {
            this.reasonCodePriorityBo = reasonCodePriorityBo;
        }

        public String getActivity() {
            return activity;
        }

        public void setActivity(String activity) {
            this.activity = activity;
        }

        public String getActivityBo() {
            return activityBo;
        }

        public void setActivityBo(String activityBo) {
            this.activityBo = activityBo;
        }

        public Date getModifiedDateTime() {
            return modifiedDateTime;
        }

        public void setModifiedDateTime(Date modifiedDateTime) {
            this.modifiedDateTime = modifiedDateTime;
        }

        public List<RuleParamVo> getRuleParam() {
            return ruleParam;
        }

        public void setRuleParam(List<RuleParamVo> ruleParam) {
            this.ruleParam = ruleParam;
        }
    }

}
