package mhp.oee.web.angular.reasoncode.recognition;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import mhp.oee.common.exception.*;
import mhp.oee.po.extend.ReasonCodeGroupAndReasonCodePO;
import mhp.oee.vo.FilterParamVO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.activity.ActivityComponent;
import mhp.oee.component.activity.ActivityParamComponent;
import mhp.oee.component.plant.ReasonCodeComponent;
import mhp.oee.component.plant.ReasonCodePriorityComponent;
import mhp.oee.component.plant.ReasonCodeRuleComponent;
import mhp.oee.component.plant.ResourceTypeComponent;
import mhp.oee.po.extend.ActivityParamAndRuleParamPO;
import mhp.oee.po.gen.ActivityPO;
import mhp.oee.po.gen.ActivityParamPO;
import mhp.oee.po.gen.ReasonCodePriorityPO;
import mhp.oee.po.gen.ReasonCodeRulePO;
import mhp.oee.po.gen.ResourceTypePO;
import mhp.oee.vo.ActivityParamVO;
import mhp.oee.vo.ReasonCodePriorityVO;

/**
 * Created by LinZuK on 2016/7/26.
 */
@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class ReasonCodeRecognitionService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    ReasonCodeComponent reasonCodeComponent;
    @Autowired
    ActivityComponent activityComponent;
    @Autowired
    ReasonCodePriorityComponent reasonCodePriorityComponent;
    @Autowired
    ActivityParamComponent activityParamComponent;
    @Autowired
    ResourceTypeComponent resourceTypeComponent;
    @Autowired
    ReasonCodeRuleComponent reasonCodeRuleComponent;

    public List<ReasonCodeGroupAndReasonCodePO> getReasonCodesLike(String site, String reasonCode) {
        return reasonCodeComponent.readReasonCodesLike(site, reasonCode);
    }

    public List<ActivityPO> getRecognitionInputHelp(String activity) {
        return activityComponent.getActivitiesLikeAndExecutionType(activity, "R");
    }

    public List<ActivityPO> getFilterInputHelp(String activity) {
        return activityComponent.getActivitiesLikeAndExecutionType(activity, "F");
    }

    // 查询历史变更记录
    public List<RecognitionVO.HistoryLogVO> getHistoryChangeLog(String site, String reasonCode, String priority, String dateTime) throws ParseException {
        return reasonCodeComponent.getHistoryChanges(site, reasonCode, priority, dateTime);
    }

    // 查询历史状态记录
    public List<RecognitionVO.HistoryLogVO> getHistoryStatusLog(String site, String reasonCode, String priority, String dateTime) throws ParseException {
        return reasonCodeComponent.getHistoryStatus(site, reasonCode, priority, dateTime);
    }

    public ReasonCodePriorityVO getReasonCodePriorityByPrimaryKey(String reasonCodePriorityBO) {
        return reasonCodeComponent.getReasonCodePriorityByPrimaryKey(reasonCodePriorityBO);
    }

    public String changeReasonCodePriority(ReasonCodePriorityVO reasonCodePriorityVO) throws ParseException, ExistingException, NotFoundExistingRecordException, UpdateErrorException, InvalidEffectiveDateException, EffectiveDateException {
        return reasonCodePriorityComponent.changeReasonCodePriority(reasonCodePriorityVO);
    }

    // “详细信息”- “防呆验证”编辑保存
    public void changeReasonCodeRuleParamF(String site, RecognitionVO.ReasonCodeRuleParamVO activityParamIn) throws ExistingException, UsedException, ItemDisableException, NoSuchException, UnknownRuleException, InvalidResourceException, UpdateErrorException, InvalidItemException, CreateErrorException, InvalidInputParamException, ExistingRecordException {
        String reasonCodePriorityBo = activityParamIn.getReasonCodePriorityBo();
        ReasonCodePriorityPO reasonCodePriority = reasonCodeComponent.getReasonCodePriorityByHandle(reasonCodePriorityBo);
        reasonCodeComponent.checkReasonCodePriorityPO(reasonCodePriority);

        if ("CHANGE_ACTIVITY".equals(activityParamIn.getActionCode())) {
            if (null == activityParamIn.getActivityBo()) {
                String reasonCodeRuleBo = reasonCodeRuleComponent.selectReasonCodeRuleBoByCondition(reasonCodePriorityBo);
                if (reasonCodeRuleBo != null) {
                    reasonCodeRuleComponent.deleteReasonCodeRuleParamByReasonCodeRuleBo(reasonCodeRuleBo);
                    reasonCodeRuleComponent.deleteReasonCodeRuleByHandle(reasonCodeRuleBo);
                }
            } else {
                ActivityPO activity = activityComponent.selectActivityByPrimaryKey(activityParamIn.getActivityBo());
                activityComponent.checkActivityPO(activity, "F");

                ReasonCodeRulePO reasonCodeRule = reasonCodeComponent.getReasonCodeRuleByCondition(activityParamIn.getReasonCodePriorityBo(), "F");
                if (reasonCodeRule == null) {
                    reasonCodeComponent.insertReasonCodeRule(activityParamIn);
                } else {
                    reasonCodeComponent.updateReasonCodeRule(reasonCodeRule, activityParamIn.getActivity());
//                    activityComponent.updateActivityByCondition(activityParamIn.getActivityBo(), activityParamIn.getActivity());
                    reasonCodeComponent.updateReasonCodeRuleParamOnChangeActivity(reasonCodeRule.getHandle(), activityParamIn.getRuleParam(), activityParamIn.getActivity());
                }
            }
        } else if ("CHANGE_PARAMETER".equals(activityParamIn.getActionCode())) {
            if (null == activityParamIn.getActivityBo()) throw new InvalidInputParamException("activityBo");
            reasonCodeComponent.updateReasonCodeRuleParamOnChangeParameter(site, activityParamIn.getReasonCodeRuleBo(), activityParamIn.getRuleParam(), activityParamIn.getActivity());
        }
    }

    // “详细信息”-“防呆验证作业”修改  和  “详细信息”-“识别方式作业”修改
    public List<ActivityParamAndRuleParamPO> getActivityFParam(String activityBo, String executionType) throws ExistingException {
        activityComponent.getActivityByCondition(activityBo, "true", executionType);
        List<ActivityParamPO> activityParamPOs = activityParamComponent.getActivityParamByActivityBO(activityBo);
        List<ActivityParamAndRuleParamPO> pos = new ArrayList<ActivityParamAndRuleParamPO>(activityParamPOs.size());
        for (ActivityParamPO each : activityParamPOs) {
            ActivityParamAndRuleParamPO po = new ActivityParamAndRuleParamPO();
            po.setParamId(each.getParamId());
            po.setParamDescription(each.getParamDescription());
            po.setParamDefaultValue(each.getParamDefaultValue());
            if ("F".equals(executionType)) {
                po.setResourceType("*");
            }
            pos.add(po);
        }
        return pos;
    }

    // “详细信息”– 查询
    public RecognitionVO.ReasonCodeDetailVO getDetailInfo(String reasonCodePriorityBo) throws ExistingException {
        return reasonCodeComponent.selectReasonCodeDetail(reasonCodePriorityBo);
    }

    // “详细信息”-“防呆验证”参数值按钮
    public FilterParamVO getFilterParamButton(String site, String reasonCodeRuleBoF, String activityFParamId) throws NoSuchException {
        return reasonCodeComponent.getFilterParamButton(site, reasonCodeRuleBoF, activityFParamId);
    }

    // “新建”-“防呆验证”参数值按钮
    public RecognitionVO.FilterParamButtonOutForNewVO getFilterParamButtonForNew(String site, String activityBo, String activityFParamId) throws NoSuchException, ExistingException {
        ActivityParamPO activityParamPO = activityComponent.getActivityParamByCondition(activityBo, activityFParamId);
        List<ResourceTypePO> resourceTypePOList = resourceTypeComponent.getResourceTypeByCondition(site, "true");
        RecognitionVO.FilterParamButtonOutForNewVO activityParamAndResourceTypeVO = new RecognitionVO.FilterParamButtonOutForNewVO();
        activityParamAndResourceTypeVO.setActivityParamPO(activityParamPO);
        activityParamAndResourceTypeVO.setResourceTypePOList(resourceTypePOList);
        return activityParamAndResourceTypeVO;
    }

    // “新建”- “防呆验证”By设备类型参数值保存
    public void changeFilterByResourceTypeParam(String site, RecognitionVO.ResourceTypeParamVO resourceTypeParamVO) throws ExistingRecordException, UpdateErrorException, InvalidCycleTimeException {
        reasonCodeComponent.changeFilterByResourceTypeParam(site, resourceTypeParamVO);
    }

    // “新建”-“防呆验证作业”保存
    public ReasonCodeRulePO saveFilterActivity(String site, RecognitionVO.FilterActivityVO filterActivityVO) throws NoSuchException, InvalidCycleTimeException, DataDescriptionException, ItemDisableException {
        return reasonCodeComponent.saveFilterActivity(site, filterActivityVO);
    }
    //新建--识别方式作业
    public void createRecognitionPattern(String reasonCodePriorityBO, String activity, List<ActivityParamVO> activityParamVos, String site) throws BusinessException {
        reasonCodeRuleComponent.createRecognitionPattern(reasonCodePriorityBO,activity,activityParamVos,site);
    }

    public void updateRecognitionPattern(ActivityParamComVO activityParamComVos) throws UpdateErrorException, MandatoryException {
        reasonCodeRuleComponent.updateRecognitionPattern(activityParamComVos);

    }
    //删除
    public void deleteReasonCodeRecognition(List<ReasonCodePriorityVO> reasonCodePriorityVo) throws NoSuchException, DeleteErrorException, UsedException {
        reasonCodeRuleComponent.delete(reasonCodePriorityVo);
    }

    public void changeReasonCodeRuleF(RecognitionVO.ReasonCodeRuleVO reasonCodeRuleVO) throws BusinessException, ParseException {
        switch (reasonCodeRuleVO.getViewActionCode()) {
            case 'U':
                reasonCodeRuleComponent.updateReasonCodeRuleF(reasonCodeRuleVO);
                break;
            case 'C':
                String handle = reasonCodeRuleComponent.insertReasonCodeRuleF(reasonCodeRuleVO);
                logger.debug("handle = " + handle);
                break;
        }
    }
}
