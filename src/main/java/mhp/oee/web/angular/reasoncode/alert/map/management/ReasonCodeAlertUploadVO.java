package mhp.oee.web.angular.reasoncode.alert.map.management;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.component.plant.ReasonCodeAlertComponent;
import mhp.oee.web.angular.excel.parser.api.JsonRow;
import mhp.oee.web.angular.reasoncode.alert.map.management.excel.parser.ExcelRow;

public class ReasonCodeAlertUploadVO {

    private List<JsonRow> exceptionRowArray = new ArrayList<JsonRow>();
    private List<ExcelRow> rows = new ArrayList<ExcelRow>();
    private String date;
    private Set<ResourceTypeCalculation> resTypeCalculation = new HashSet<ResourceTypeCalculation>();

    public List<JsonRow> getExceptionRowArray() {
        return exceptionRowArray;
    }

    public void setExceptionRowArray(List<JsonRow> exceptionRowArray) {
        this.exceptionRowArray = exceptionRowArray;
    }

    public List<ExcelRow> getRows() {
        return rows;
    }

    public void setRows(List<ExcelRow> rows) {
        this.rows = rows;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Set<ResourceTypeCalculation> getResTypeCalculation() {
        return resTypeCalculation;
    }

    public void setResTypeCalculation(Set<ResourceTypeCalculation> resTypeCalculation) {
        this.resTypeCalculation = resTypeCalculation;
    }

    public void calculationExcelResourceType() {
        for(ResourceTypeCalculation calculation : getResTypeCalculation()) {
            calculation.setExcelNumber(Collections.frequency(getRows(), new ExcelRow(calculation.getResourceType()) ));
        }
    }

    public void calculationDataBaseResourceType(String site, ReasonCodeAlertComponent reasonCodeAlertComponent) {
        for(ResourceTypeCalculation calculation : getResTypeCalculation()) {
            String handle = new ResourceTypeBOHandle(site, calculation.getResourceType()).getValue();
            calculation.setDatabaseNumber(reasonCodeAlertComponent.countReasonCodeAlertByResourceType(handle));
        }

    }

}
