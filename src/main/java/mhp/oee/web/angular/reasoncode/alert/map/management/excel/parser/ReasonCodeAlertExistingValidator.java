package mhp.oee.web.angular.reasoncode.alert.map.management.excel.parser;

import mhp.oee.common.exception.ExistingException;
import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.common.logging.GenericLoggerFactory;
import mhp.oee.common.security.SpringContextHolder;
import mhp.oee.component.plant.ReasonCodeComponent;
import mhp.oee.component.plant.ResourceTypeComponent;
import mhp.oee.po.gen.ReasonCodePO;
import mhp.oee.po.gen.ResourceTypePO;
import mhp.oee.web.angular.excel.parser.api.CellValidator;
import mhp.oee.web.angular.reasoncode.alert.map.management.excel.parser.ReasonCodeAlertCell.ReasonCodeAlertIndex;
import mhp.oee.web.angular.resource.plc.management.excel.parser.ExistingCellValidator;

public class ReasonCodeAlertExistingValidator extends CellValidator {

    public ReasonCodeAlertExistingValidator(ReasonCodeAlertCell cell){
            this.cell = cell;
            this.cell.attach(this);
            logger = GenericLoggerFactory.getLogger(ExistingCellValidator.class);
         }

    @Override
    public void validator() {
        if (this.cell.getValue().trim().length() == 0) {
            return;
        }

        if (this.cell.getKey().equals(ReasonCodeAlertIndex.ResourceType)) {
            String handle = new ResourceTypeBOHandle(this.cell.getSite(), this.cell.getValue()).getValue();
            ResourceTypeComponent dao = (ResourceTypeComponent) SpringContextHolder.getBean("resourceTypeComponent");
            ResourceTypePO po = dao.readResourceTypeByPrimaryKey(handle);
            if (po == null || !po.getEnabled().equals("true")) {
                this.cell.addException(new ExistingException());
            }

            return;
        }

        if (this.cell.getKey().equals(ReasonCodeAlertIndex.ReasonCode)) {
            ReasonCodeComponent dao = (ReasonCodeComponent) SpringContextHolder.getBean("reasonCodeComponent");
            ReasonCodePO po = dao.existResonCode(this.cell.getSite(), this.cell.getValue());
            if (po == null || !po.getEnabled().equals("true")) {
                this.cell.addException(new ExistingException());
            }

            return;
        }



    }
}