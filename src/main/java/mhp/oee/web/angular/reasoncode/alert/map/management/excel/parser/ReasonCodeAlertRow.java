package mhp.oee.web.angular.reasoncode.alert.map.management.excel.parser;

import java.util.Date;

import mhp.oee.web.angular.excel.parser.api.CustomerRow;
import mhp.oee.web.angular.reasoncode.alert.map.management.excel.parser.ReasonCodeAlertCell.ReasonCodeAlertIndex;

public class ReasonCodeAlertRow extends CustomerRow {

    public ReasonCodeAlertRow(String site, Date date) {
        ReasonCodeAlertCell.initCellAttribute(this.getCellArray(), site, date);
    }

    @Override
    public Object getVO(String site) {
        ExcelRow row = new ExcelRow();
        row.setResourceType(this.getValue(ReasonCodeAlertIndex.ResourceType));
        row.setAlertCode(this.getValue(ReasonCodeAlertIndex.AlertCode));
        row.setDec(this.getValue(ReasonCodeAlertIndex.Des));
        row.setReasonCode(this.getValue(ReasonCodeAlertIndex.ReasonCode));
        return row;
    }


}