package mhp.oee.web.angular.reasoncode.management;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.ReasonCodeBOHandle;
import mhp.oee.common.handle.ResourceStateBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ReasonCodeResStateComponent;
import mhp.oee.po.extend.ReasonCodeAndReasonCodeGroupPO;
import mhp.oee.po.gen.ReasonCodeResStatePO;
import mhp.oee.po.gen.ResourceStatePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStateVO;
import mhp.oee.vo.ReasonCodeResStateVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class ReasonCodeResStateService {

    @InjectableLogger
    private static Logger logger;
    private SimpleDateFormat simpleDateFormatForDate = new SimpleDateFormat("MM/dd/yyyy-HH/mm/ss");
    
    @Autowired
    ReasonCodeResStateComponent reasonCodeResStateComponent;

    public List<ResourceStatePO> readResourceState(String site, String enabled) {
        return reasonCodeResStateComponent.readResourceState(site, enabled);
    }

    public List<ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStateVO> readReasonCodeResState(String site, String reasonCodeGroup, String reasonCode, String dateTime, int limit, int offset)
            throws ParseException{
        List<ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStateVO> vos = reasonCodeResStateComponent.readReasonCodeResState(site, reasonCodeGroup, reasonCode, Utils.getDatefromString(dateTime), limit, offset);
        for (ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStateVO vo : vos) {
            vo.setStartDateTimeIn(simpleDateFormatForDate.format(vo.getStartDateTime()));
            vo.setEndDateTimeIn(simpleDateFormatForDate.format(vo.getEndDateTime()));
        }
        return vos;
    }

    public int readReasonCodeResStateCount(String site, String reasonCodeGroup, String reasonCode, String dateTime) throws ParseException {
        return reasonCodeResStateComponent.readReasonCodeResStateCount(site, reasonCodeGroup, reasonCode, Utils.getDatefromString(dateTime));
    }

    public List<ReasonCodeAndReasonCodeGroupPO> readResourceStateAddUI(String site, String reasonCodeGroup, String reasonCode, String enabled) {
        return reasonCodeResStateComponent.readResourceStateAddUI(site, reasonCodeGroup, reasonCode, enabled);
    }

    public boolean checkResonCodeResourceState(String site, String reasonCode, String startDateTime, String resourceState) throws ParseException {
        ReasonCodeBOHandle reasonCodeBOHandle = new ReasonCodeBOHandle(site, reasonCode);
        ResourceStateBOHandle resourceStateBOHandle = new ResourceStateBOHandle(site, resourceState);
        List<ReasonCodeResStatePO> pos = reasonCodeResStateComponent.existResonCodeResourceState(reasonCodeBOHandle.getValue(), resourceStateBOHandle.getValue(), getInfiniteDate());
        if (pos != null && pos.size() > 0) {
            return false;
        }
        List<ReasonCodeResStatePO> po = reasonCodeResStateComponent.existResonCodeResourceState(reasonCodeBOHandle.getValue(), null, getInfiniteDate());
        if (po != null && po.size() > 0) {
            ReasonCodeResStatePO reasonCodeResStatePO = po.get(0);
            if (Utils.getDatefromString(startDateTime).compareTo(reasonCodeResStatePO.getStartDateTime()) <= 0) {
                return false;
            }
        }
        return true;
    }

    public void changeResonCodeResourceState(List<ReasonCodeResStateVO> reasonCodeResStateVOs) throws BusinessException, ParseException {
        for (ReasonCodeResStateVO each : reasonCodeResStateVOs) {
            switch (each.getViewActionCode()) {
            case 'C':
                String strt = each.getStartDateTimeIn();
                each.setStartDateTime(Utils.getDateFromWeb(strt));
                each.setEndDateTime(getInfiniteDate());
                each.setStrt(Utils.getStrt(strt));
                reasonCodeResStateComponent.updateResonCodeResourceState(each);
                reasonCodeResStateComponent.createResonCodeResourceState(each);
                break;
            case 'D':
                reasonCodeResStateComponent.deleteResonCodeResourceState(each);
                break;
            default:
                logger.error("ReasonCodeResStateService.changeResonCodeResourceState() : Invalid viewActionCode '" + each.getViewActionCode() + "'.");
                break;
            }
        }
    }
    
    private static Date getInfiniteDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(9999, 11, 31, 23, 59, 59);
        return calendar.getTime();
    }
}
