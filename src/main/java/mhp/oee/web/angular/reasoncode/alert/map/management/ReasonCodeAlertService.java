package mhp.oee.web.angular.reasoncode.alert.map.management;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.InvalidInputDateException;
import mhp.oee.common.handle.ReasonCodeBOHandle;
import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ReasonCodeAlertComponent;
import mhp.oee.component.plant.ReasonCodeComponent;
import mhp.oee.component.plant.ResourceTypeComponent;
import mhp.oee.po.extend.ReasonCodeAlertAndResourceTypeAndReasonCodePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ReasonCodeAlertVO;
import mhp.oee.vo.ResourceTypeConditionVO;
import mhp.oee.web.Constants;
import mhp.oee.web.angular.excel.parser.api.CustomerRow;
import mhp.oee.web.angular.excel.parser.api.JsonRow;
import mhp.oee.web.angular.excel.parser.api.SuccessUpload;
import mhp.oee.web.angular.reasoncode.alert.map.management.excel.parser.ExcelRow;
import mhp.oee.web.angular.reasoncode.alert.map.management.excel.parser.ReasonCodeAlertRow;

@Service
@Transactional(rollbackFor = { BusinessException.class })
public class ReasonCodeAlertService {

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @InjectableLogger
    private static Logger logger;

    @Autowired
    ReasonCodeAlertComponent reasonCodeAlertComponent;

    @Autowired
    ResourceTypeComponent resourceTypeComponent;

    @Autowired
    ReasonCodeComponent reasonCodeComponent;

    public List<ResourceTypeConditionVO> readAllResrceType(String site, String resrceType) {
        return resourceTypeComponent.readAllResourceTypeLikeResrce(site, resrceType);
    }

    public boolean isReasonCodeAlertUsed(String site, String handle) {
        return reasonCodeAlertComponent.isReasonCodeAlertUsed(handle);
    }

    public void changeReasonCodeAlert(List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> pos) throws BusinessException {
        for (ReasonCodeAlertAndResourceTypeAndReasonCodePO each : pos) {
            switch (each.getViewActionCode()) {
            case 'D':
                reasonCodeAlertComponent.deleteReasonCodeAlertUsed(each);
                break;
            default:
                logger.error("ReasonCodeAlertService.changeReasonCodeAlert() : Invalid viewActionCode '"
                        + each.getViewActionCode() + "'.");
                break;
            }
        }
    }

    public List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> readReasonCodeAlertCurrent(String site,
            String[] resrceType) {
        return reasonCodeAlertComponent.readReasonCodeAlertCurrent(site, resrceType);
    }

    public List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> readReasonCodeAlertCurrentPage(String site,
            String[] resrceType, int index, int count) throws ParseException {
        List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> vos = reasonCodeAlertComponent.readReasonCodeAlertCurrentPage(site, resrceType, count, Utils.getOffset(count, index));
        if (vos != null && vos.size() > 0) {
            for (ReasonCodeAlertAndResourceTypeAndReasonCodePO vo : vos) {
                vo.setStartDateTimeIn(Utils.datetimeToStr(vo.getStartDateTime()));
                vo.setEndDateTimeIn(Utils.datetimeToStr(vo.getEndDateTime()));
            }
        }
        return vos;
    }

    public int readReasonCodeAlertCurrentPageCount(String site, String[] resrceType) {
        return reasonCodeAlertComponent.readReasonCodeAlertCurrentPageCount(site, resrceType);
    }

    public List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> readReasonCodeAlertHisChange(String site,
            String[] resrceType, String time, int index, int count) throws ParseException {
        List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> vos = reasonCodeAlertComponent.readReasonCodeAlertHisChange(site, resrceType, Utils.getDatefromString(time),
                count, Utils.getOffset(count, index));
        if (vos != null && vos.size() > 0) {
            for (ReasonCodeAlertAndResourceTypeAndReasonCodePO vo : vos) {
                vo.setStartDateTimeIn(Utils.datetimeToStr(vo.getStartDateTime()));
                vo.setEndDateTimeIn(Utils.datetimeToStr(vo.getEndDateTime()));
            }
        }
        return vos;
    }

    public int readReasonCodeAlertHisChangeCount(String site, String[] resrceType, String time) throws ParseException {
        return reasonCodeAlertComponent.readReasonCodeAlertHisChangeCount(site, resrceType,
                Utils.getDatefromString(time));
    }

    public List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> readReasonCodeAlertHisStatus(String site,
            String[] resrceType, String time, int index, int count) throws ParseException {
        List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> vos = reasonCodeAlertComponent.readReasonCodeAlertHisStatus(site, resrceType, Utils.getDatefromString(time),
                count, Utils.getOffset(count, index));
        if (vos != null && vos.size() > 0) {
            for (ReasonCodeAlertAndResourceTypeAndReasonCodePO vo : vos) {
                vo.setStartDateTimeIn(Utils.datetimeToStr(vo.getStartDateTime()));
                vo.setEndDateTimeIn(Utils.datetimeToStr(vo.getEndDateTime()));
            }
        }
        return vos;
    }

    public int readReasonCodeAlertHisStatusCount(String site, String[] resrceType, String time) throws ParseException {
        return reasonCodeAlertComponent.readReasonCodeAlertHisStatusCount(site, resrceType,
                Utils.getDatefromString(time));
    }

    public void commitExportExcel(List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> pos, HttpServletResponse response,
            String filePath) throws FileNotFoundException, IOException {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("报警信息与原因代码对应关系维护表");
        XSSFRow row = sheet.createRow(0);

        XSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("设备类型");
        cell = row.createCell((short) 1);
        cell.setCellValue("报警信息虚拟编码");
        cell = row.createCell((short) 2);
        cell.setCellValue("报警信息描述");
        cell = row.createCell((short) 3);
        cell.setCellValue("原因代码");
        cell = row.createCell((short) 4);

        for (int i = 0; i < pos.size(); i++) {
            XSSFRow addRow = sheet.createRow(i + 1);
            addRow.createCell(0).setCellValue(pos.get(i).getResourceTypePO().getResourceType());
            addRow.createCell(1).setCellValue(pos.get(i).getAlertSequenceId());
            addRow.createCell(2).setCellValue(pos.get(i).getDescription());
            addRow.createCell(3).setCellValue(pos.get(i).getReasonCodePO().getReasonCode());
        }

        FileOutputStream outputStream = new FileOutputStream(filePath);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }

    public ReasonCodeAlertUploadVO processUploadFile(MultipartFile multiFile, String site, String dateString)
            throws IOException, BusinessException, ParseException {

        Date date = Utils.getDateFromWeb(dateString);
        if(!date.after(new Date())){
            throw new InvalidInputDateException();
        }


        XSSFWorkbook workbook = new XSSFWorkbook(multiFile.getInputStream());
        XSSFSheet sheet = workbook.getSheetAt(0);

        List<CustomerRow> rowArray = new ArrayList<CustomerRow>();
        for (int rowNumber = Constants.REASON_CODE_ALERT_IGNORED_NUM; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            Row row = sheet.getRow(rowNumber);
            CustomerRow customerRow = new ReasonCodeAlertRow(site, date);

            for (int cellNumber = 0; cellNumber < Constants.REASONCODE_UPLOAD_LENGTH; cellNumber++) {
                if (row != null && row.getCell(cellNumber) != null) {
                    Cell cell = row.getCell(cellNumber);
                    switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_NUMERIC:
                        customerRow.setCellValue(cellNumber, rowNumber,
                                Double.valueOf(cell.getNumericCellValue()).toString());
                        break;
                    case Cell.CELL_TYPE_STRING:
                        customerRow.setCellValue(cellNumber, rowNumber, cell.getStringCellValue());
                        break;
                    case Cell.CELL_TYPE_BLANK:
                        customerRow.setCellValue(cellNumber, rowNumber, cell.getStringCellValue());
                        break;
                    }
                } else {
                    customerRow.setCellValue(cellNumber, rowNumber, "");
                }
            }
            rowArray.add(customerRow);
        }
        workbook.close();
        List<CustomerRow> notEmptyRowArray = new ArrayList<CustomerRow>();
        for (CustomerRow row : rowArray) {
            if(!row.isEmptyRow()) {
                notEmptyRowArray.add(row);
            }
        }

        for (CustomerRow row : notEmptyRowArray) {
            logger.debug(">>>>>>>>>>> excel rows    "+row.toString());
        }

        ReasonCodeAlertUploadVO vo = new ReasonCodeAlertUploadVO();

        for (CustomerRow row : notEmptyRowArray) {
            JsonRow jsonRow = row.generateRowError();
            if (jsonRow != null) {
                vo.getExceptionRowArray().add(row.generateRowError());
            }
        }

        if (vo.getExceptionRowArray().size() == 0) {
            for (CustomerRow row : notEmptyRowArray) {
                ExcelRow r = (ExcelRow) row.getVO(site);
                vo.getRows().add(r);

                ResourceTypeCalculation calculation = new ResourceTypeCalculation(r.getResourceType());
                vo.getResTypeCalculation().add(calculation);
            }
            vo.calculationExcelResourceType();
            vo.calculationDataBaseResourceType(site, reasonCodeAlertComponent);
        }

        vo.setDate(dateString);
        return vo;
    }

    public SuccessUpload commitUploadExcel(ReasonCodeAlertUploadVO uploadVO, String site)
            throws BusinessException, ParseException {
        List<ExcelRow> excelRows = uploadVO.getRows();
        Set<String> resourceTypeSet = new HashSet<String>();
        List<ReasonCodeAlertVO> list = new ArrayList<ReasonCodeAlertVO>();
        for (ExcelRow row : excelRows) {
            ReasonCodeAlertVO vo = this.getReasonCodeAlertVOByExcelRow(row, site, uploadVO.getDate());
            logger.debug("create reason code ........");
            logger.debug(gson.toJson(vo));
            resourceTypeSet.add(vo.getResourceTypeBo());
            vo.transferToUnicode();
            list.add(vo);
        }
        reasonCodeAlertComponent.updateReasonCodeAlertByStartTime(resourceTypeSet, Utils.getDateFromWeb(uploadVO.getDate()));
        reasonCodeAlertComponent.createReasonCodeAlertListByVO(list);

        return new SuccessUpload(excelRows.size());
    }

    private ReasonCodeAlertVO getReasonCodeAlertVOByExcelRow(ExcelRow row, String site, String date)
            throws ParseException {
        ReasonCodeAlertVO vo = new ReasonCodeAlertVO();

        String resourceTypeBo = new ResourceTypeBOHandle(site, row.getResourceType()).getValue();
        String reasonCodeBo = new ReasonCodeBOHandle(site, row.getReasonCode()).getValue();

        vo.setAlertSequenceId(row.getAlertCode());
        vo.setDescription(row.getDec());
        vo.setEndDateTime(Utils.getInfiniteDateWithTime());
        vo.setReasonCodeBo(reasonCodeBo);
        vo.setResourceTypeBo(resourceTypeBo);
        vo.setStartDateTime(Utils.getDateFromWeb(date));
        vo.setStrt(Utils.getStrt(date));
        vo.setUsed("false");

        return vo;
    }

}
