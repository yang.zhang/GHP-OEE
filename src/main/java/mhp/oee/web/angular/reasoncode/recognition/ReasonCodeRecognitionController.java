package mhp.oee.web.angular.reasoncode.recognition;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ExistingException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.extend.ActivityParamAndRuleParamPO;
import mhp.oee.po.extend.ReasonCodeGroupAndReasonCodePO;
import mhp.oee.po.gen.ActivityPO;
import mhp.oee.po.gen.ReasonCodePriorityPO;
import mhp.oee.po.gen.ReasonCodeRulePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ActivityParamVO;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.FilterParamVO;
import mhp.oee.vo.ReasonCodePriorityVO;
import mhp.oee.web.Constants;
import mhp.oee.web.angular.activity.management.ActivityService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

/**
 * Created by LinZuK on 2016/7/26.
 */
@RestController
public class ReasonCodeRecognitionController {
    @InjectableLogger
    private static Logger logger;

    @Autowired
    ReasonCodeRecognitionService reasonCodeRecognitionService;
    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ActivityService activityService;

    // 查询 -“原因代码”输入帮助 ok
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/site/{site}/reason_code",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ReasonCodeGroupAndReasonCodePO>> searchReasonCode(@PathVariable("site") String site,
                                                                                 @RequestParam(value = "reasonCode") String reasonCode) {
        List<ReasonCodeGroupAndReasonCodePO> pos = reasonCodeRecognitionService.getReasonCodesLike(site, reasonCode);
        return ResponseEntity.ok(pos);
    }

    // 查询 – 历史变更记录   原因代码、优先级、规则生效日期大于 ok
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/site/{site}/history_change_log",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<RecognitionVO.HistoryLogVO>> searchHistoryChangeLog(@PathVariable("site") String site,
                                                                     @RequestParam(value = "reasonCode", required = true) String reasonCode,
                                                                     @RequestParam(value = "priority", required = true) String priority,
                                                                     @RequestParam(value = "dateTime", required = true) String dateTime) throws ParseException {
        List<RecognitionVO.HistoryLogVO> vos = reasonCodeRecognitionService.getHistoryChangeLog(site, reasonCode, priority, dateTime);
        return ResponseEntity.ok(vos);
    }

    // 查询 – 历史状态记录
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/site/{site}/history_status_log",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<RecognitionVO.HistoryLogVO>> searchHistoryStatusLog(@PathVariable("site") String site,
                                                                     @RequestParam(value = "reasonCode", required = true) String reasonCode,
                                                                     @RequestParam(value = "priority", required = true) String priority,
                                                                     @RequestParam(value = "dateTime", required = true) String dateTime) throws ParseException {
        List<RecognitionVO.HistoryLogVO> vos = reasonCodeRecognitionService.getHistoryStatusLog(site, reasonCode, priority, dateTime);
        return ResponseEntity.ok(vos);
    }

    // “识别方式作业”输入帮助 ok
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/recognition_input_help",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityPO>> getRecognitionInputHelp(@RequestParam(value = "activity") String activity) {
        List<ActivityPO> pos = reasonCodeRecognitionService.getRecognitionInputHelp(activity);
        return ResponseEntity.ok(pos);
    }

    // “防呆验证作业”输入帮助 ok
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/filter_input_help",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityPO>> getFilterInputHelp(@RequestParam(value = "activity") String activity) {
        List<ActivityPO> pos = reasonCodeRecognitionService.getFilterInputHelp(activity);
        return ResponseEntity.ok(pos);
    }



//    // Distinguish Priority Detail
//    //识别优先级详情
//    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/ReasonCodePriorityDetail", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
//    public ResponseEntity<ReasonCodePriorityVO> getReasonCodePriorityByPrimaryKey(
//            @RequestParam(value = "reasonCodePriorityBO") String reasonCodePriorityBO) {
//        ReasonCodePriorityVO vo = reasonCodeRecognitionService.getReasonCodePriorityByPrimaryKey(reasonCodePriorityBO);
//        return ResponseEntity.ok(vo);
//    }

//    // Distinguish pattern Detail
//    //识别方式详情 “详细信息”-“识别方式作业”修改
//    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/recognitionPatternDetail", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
//    public ResponseEntity<List<ActivityParamPO>> recognitionPatternDetail(
//            @RequestParam(value = "activity") String activity,
//            @RequestParam(value = "type") String type) {
//        List<ActivityParamPO> pos = activityService.recognitionPpatternDetail(activity,type);
//        return ResponseEntity.ok(pos);
//    }
    //识别优先级增删改接口 “新建”-“识别优先级”保存 修改
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/changeReasonCodePriority", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeReasonCodePriority(@RequestBody ReasonCodePriorityVO reasonCodePriorityVO) {
        String trace = String.format("[ReasonCodeRecognitionController.changeReasonCodePriority] request : reasonCodePriorityVO=%s ",reasonCodePriorityVO);
        logger.debug(">> " + trace);
        ReasonCodePriorityPO po=new ReasonCodePriorityPO();
        String reasonCodePriorityBO="";
        try {
            reasonCodePriorityBO = reasonCodeRecognitionService.changeReasonCodePriority(reasonCodePriorityVO);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        } catch(ParseException parseException){
            logger.error(Utils.getCurrentMethodName(0) + " : " + parseException.getMessage() + System.lineSeparator() + Utils.getStackTrace(parseException));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(parseException)).body(null);
        }
        po.setHandle(reasonCodePriorityBO);
        return ResponseEntity.ok((Object)("".equals(po.getHandle())? "":po));
    }

    // “详细信息”-“防呆验证作业”修改  和  “详细信息”-“识别方式作业”修改
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/activity_param",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityParamAndRuleParamPO>> getActivityParam(
            @RequestParam(value = "activityBo", required = true) String activityBo,
            @RequestParam(value = "executionType", required = true) String executionType) throws ExistingException {
        List<ActivityParamAndRuleParamPO> pos = reasonCodeRecognitionService.getActivityFParam(activityBo, executionType);
        return ResponseEntity.ok(pos);
    }

    // “详细信息”- “防呆验证”编辑保存
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/site/{site}/change_rule_param_f",
            method = RequestMethod.POST,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Object> changeRuleParamF(
            @PathVariable("site") String site,
            @RequestBody RecognitionVO.ReasonCodeRuleParamVO activityParamIn) {
        try {
            reasonCodeRecognitionService.changeReasonCodeRuleParamF(site, activityParamIn);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok().body(null);
    }

    // “详细信息”– 查询
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/detail_info",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<RecognitionVO.ReasonCodeDetailVO> getDetailInfo(
            @RequestParam(value = "reasonCodePriorityBo", required = true) String reasonCodePriorityBo) {
        RecognitionVO.ReasonCodeDetailVO out = null;
        try {
            out = reasonCodeRecognitionService.getDetailInfo(reasonCodePriorityBo);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok(out);
    }


    // “详细信息”-“防呆验证”参数值按钮
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/site/{site}/filter_param_button",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<FilterParamVO> getFilterParamButton(
            @PathVariable("site") String site,
            @RequestParam(value = "reasonCodeRuleBoF", required = true) String reasonCodeRuleBoF,
            @RequestParam(value = "activityFParamId", required = true) String activityFParamId) {
        FilterParamVO out = null;
        try {
            out = reasonCodeRecognitionService.getFilterParamButton(site, reasonCodeRuleBoF, activityFParamId);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok(out);
    }

    // “新建”-“防呆验证”参数值按钮
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/site/{site}/filter_param_button_for_new",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<RecognitionVO.FilterParamButtonOutForNewVO> getFilterParamButtonForNew(
            @PathVariable("site") String site,
            @RequestParam(value = "activityBo", required = true) String activityBo,
            @RequestParam(value = "activityFParamId", required = true) String activityFParamId) {
        RecognitionVO.FilterParamButtonOutForNewVO out = null;
        try {
            out = reasonCodeRecognitionService.getFilterParamButtonForNew(site, activityBo, activityFParamId);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok(out);
    }

    // 修改防呆验证的rule值 ok
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/change_reason_code_rule_f",
            method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<RecognitionVO.FilterParamButtonOutForNewVO> changeReasonCodeRuleF(@RequestBody RecognitionVO.ReasonCodeRuleVO reasonCodeRuleVO) throws ParseException {
        try {
            reasonCodeRecognitionService.changeReasonCodeRuleF(reasonCodeRuleVO);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok().body(null);
    }

    // “新建”- “防呆验证”By设备类型参数值保存
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/site/{site}/change_resource_type_param",
            method = RequestMethod.POST,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Object> changeFilterByResourceTypeParam(@PathVariable("site") String site,
                                                                @RequestBody RecognitionVO.ResourceTypeParamVO resourceTypeParamVO) {
        try {
            reasonCodeRecognitionService.changeFilterByResourceTypeParam(site, resourceTypeParamVO);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok().body(null);
    }

    // “新建”-“防呆验证作业”保存
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/site/{site}/save_filter_activity",
            method = RequestMethod.POST,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<ReasonCodeRulePO> saveFilterActivity(
            @PathVariable("site") String site,
            @RequestBody RecognitionVO.FilterActivityVO filterActivityVO) {
        ReasonCodeRulePO po = null;
        try {
            po = reasonCodeRecognitionService.saveFilterActivity(site, filterActivityVO);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok().body(po);
    }

    // 识别方式新增接口
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/createRecognitionPattern", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeRecognitionPattern(@RequestBody List<ActivityParamVO> activityParamVos,
            @RequestParam(value = "activity") String activity,
            @RequestParam(value = "reasonCodePriorityBO") String reasonCodePriorityBO,
            @RequestParam(value = "site") String site) {
        String trace = String.format(
                "[ReasonCodeRecognitionController.createRecognitionPattern] request : activityParamVos=%s,activity=%s,reasonCodePriorityBO=%s,site=%s ",
                activityParamVos, activity, reasonCodePriorityBO, site);
        logger.debug(">> " + trace);
        try {
            reasonCodeRecognitionService.createRecognitionPattern(reasonCodePriorityBO, activity, activityParamVos,
                    site);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ReasonCodeRecognitionController.createRecognitionPattern() : " + e.getMessage() + " "
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ReasonCodeRecognitionController.createRecognitionPattern]");
        return ResponseEntity.ok(null);
    }
    //识别方式修改接口
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/updateRecognitionPattern", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> updateRecognitionPattern(@RequestBody ActivityParamComVO activityParamComVos) {
        String trace = String.format(
                "[ReasonCodeRecognitionController.updateRecognitionPattern] request : activityParamComVos=%s ", gson.toJson(activityParamComVos));
        logger.debug(">> " + trace);    
        try {
                reasonCodeRecognitionService.updateRecognitionPattern(activityParamComVos);
            } catch (BusinessException e) {
                ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
                logger.error("ReasonCodeRecognitionController.updateRecognitionPattern() : " + e.getMessage() + " " + Utils.getStackTrace(e));
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
            }
        logger.debug("<< [ReasonCodeRecognitionController.updateRecognitionPattern]");    
        return ResponseEntity.ok(null);
    }
    //删除
    @RequestMapping(value = "/web/rest/plant/reason_code/recognition/deleteReasonCodeRecognition", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> deleteReasonCodeRecognition(@RequestBody List<ReasonCodePriorityVO> reasonCodePriorityVo) {
        String trace = String.format(
                "[ReasonCodeRecognitionController.deleteReasonCodeRecognition] request : reasonCodePriorityVo=%s ", gson.toJson(reasonCodePriorityVo));
        logger.debug(">> " + trace);
            try {
                reasonCodeRecognitionService.deleteReasonCodeRecognition(reasonCodePriorityVo);
            } catch (BusinessException e) {
                ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
                logger.error("ReasonCodeRecognitionController.deleteReasonCodeRecognition() : " + e.getMessage() + " " + Utils.getStackTrace(e));
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
            }
        logger.debug("<< [ReasonCodeRecognitionController.deleteReasonCodeRecognition]");    
        return ResponseEntity.ok(null);
    }
}
