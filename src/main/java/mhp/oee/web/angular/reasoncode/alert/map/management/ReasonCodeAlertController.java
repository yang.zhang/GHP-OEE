package mhp.oee.web.angular.reasoncode.alert.map.management;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.extend.ReasonCodeAlertAndResourceTypeAndReasonCodePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ResourceTypeConditionVO;
import mhp.oee.web.Constants;
import mhp.oee.web.angular.excel.parser.api.SuccessUpload;

@RestController
public class ReasonCodeAlertController {

    @Autowired
    ReasonCodeAlertService reasoncodeAlertService;

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @RequestMapping(value = "/web/rest/reasoncode/site/{site}/alert/commit/resrce_type_all", method = RequestMethod.GET)
    public ResponseEntity<List<ResourceTypeConditionVO>> readAllResrceType(@PathVariable("site") String site,
            @RequestParam(value = "resrce_type", required = false) String resrceType) {
        List<ResourceTypeConditionVO> vos = reasoncodeAlertService.readAllResrceType(site, resrceType);
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/reasoncode/site/{site}/alert/commit/reason_code_alert_current", method = RequestMethod.GET)
    public ResponseEntity<List<ReasonCodeAlertAndResourceTypeAndReasonCodePO>> readReasonCodeAlertCurrentPage(
            @PathVariable("site") String site,
            @RequestParam(value = "resrce_type", required = true) String[] resrceType,
            @RequestParam(value = "index", required = true) int index,
            @RequestParam(value = "count", required = true) int count) {
        List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> vos;
        try {
            vos = reasoncodeAlertService.readReasonCodeAlertCurrentPage(site, resrceType, index, count);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ReasonCodeAlertController.readReasonCodeAlertCurrentPage() : " + e.getMessage()
                    + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/reasoncode/site/{site}/alert/commit/reason_code_alert_current_count", method = RequestMethod.GET)
    public ResponseEntity<Integer> readReasonCodeAlertCurrentPageCount(@PathVariable("site") String site,
            @RequestParam(value = "resrce_type", required = true) String[] resrceType) {
        int vos = reasoncodeAlertService.readReasonCodeAlertCurrentPageCount(site, resrceType);
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/reasoncode/site/{site}/alert/commit/reason_code_alert_his_change", method = RequestMethod.GET)
    public ResponseEntity<List<ReasonCodeAlertAndResourceTypeAndReasonCodePO>> readReasonCodeAlertHisChange(
            @PathVariable("site") String site,
            @RequestParam(value = "resrce_type", required = true) String[] resrceType,
            @RequestParam(value = "time", required = true) String time,
            @RequestParam(value = "index", required = true) int index,
            @RequestParam(value = "count", required = true) int count) {
        List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> vos = null;
        try {
            vos = reasoncodeAlertService.readReasonCodeAlertHisChange(site, resrceType, time, index, count);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ReasonCodeAlertController.readReasonCodeAlertHisChange() : " + e.getMessage()
                    + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/reasoncode/site/{site}/alert/commit/reason_code_alert_his_change_count", method = RequestMethod.GET)
    public ResponseEntity<Integer> readReasonCodeAlertHisChangeCount(@PathVariable("site") String site,
            @RequestParam(value = "resrce_type", required = true) String[] resrceType,
            @RequestParam(value = "time", required = true) String time) {
        int vos = 0;
        try {
            vos = reasoncodeAlertService.readReasonCodeAlertHisChangeCount(site, resrceType, time);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ReasonCodeAlertController.readReasonCodeAlertHisChange() : " + e.getMessage()
                    + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/reasoncode/site/{site}/alert/commit/reason_code_alert_his_status", method = RequestMethod.GET)
    public ResponseEntity<List<ReasonCodeAlertAndResourceTypeAndReasonCodePO>> readReasonCodeAlertHisStatus(
            @PathVariable("site") String site,
            @RequestParam(value = "resrce_type", required = true) String[] resrceType,
            @RequestParam(value = "time", required = true) String time,
            @RequestParam(value = "index", required = true) int index,
            @RequestParam(value = "count", required = true) int count) {
        List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> vos = null;
        try {
            vos = reasoncodeAlertService.readReasonCodeAlertHisStatus(site, resrceType, time, index, count);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ReasonCodeAlertController.readReasonCodeAlertHisStatus() : " + e.getMessage()
                    + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/reasoncode/site/{site}/alert/commit/reason_code_alert_his_status_count", method = RequestMethod.GET)
    public ResponseEntity<Integer> readReasonCodeAlertHisStatusCount(@PathVariable("site") String site,
            @RequestParam(value = "resrce_type", required = true) String[] resrceType,
            @RequestParam(value = "time", required = true) String time) {
        int vos = 0;
        try {
            vos = reasoncodeAlertService.readReasonCodeAlertHisStatusCount(site, resrceType, time);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ReasonCodeAlertController.readReasonCodeAlertHisStatus() : " + e.getMessage()
                    + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/reasoncode/alert/site/{site}/export_excel", method = RequestMethod.GET)
    public ResponseEntity<?> exportExcel(@PathVariable("site") String site,
            @RequestParam(value = "resrce_type", required = true) String[] resrceType, HttpServletResponse response,
            HttpServletRequest request) {

        try {
            String filePath = "download.xlsx";
            List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> po = reasoncodeAlertService
                    .readReasonCodeAlertCurrent(site, resrceType);

            reasoncodeAlertService.commitExportExcel(po, response, filePath);
            this.generateDownloadFile(response, filePath);

        } catch (FileNotFoundException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.FILE_NOT_FOUND.getErrorCode(), e.getMessage(), null);
            logger.error("alertExportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        } catch (IOException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
            logger.error("alertExportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok(null);
    }

    private void generateDownloadFile(HttpServletResponse response, String filePath) throws IOException {
        File file = new File(filePath);
        OutputStream out = response.getOutputStream();
        response.setHeader("Content-disposition", "attachment; filename=" + "download.xlsx");

        byte[] buffer = new byte[8192]; // use bigger if you want
        int length = 0;

        FileInputStream in = new FileInputStream(file);

        while ((length = in.read(buffer)) > 0) {
            out.write(buffer, 0, length);
        }
        in.close();
        out.close();
    }

    @RequestMapping(value = "/web/rest/reasoncode/site/{site}/alert/commit/upload_file", method = RequestMethod.POST)
    public ResponseEntity<?> commitUploadExcel(@RequestBody ReasonCodeAlertUploadVO vo,
            @PathVariable("site") String site, HttpServletResponse res, HttpServletRequest request) {
        try {
            logger.debug(">>>>>  commit to database  <<<<<");
            logger.debug(gson.toJson(vo));
            SuccessUpload successUpload = reasoncodeAlertService.commitUploadExcel(vo, site);
            return ResponseEntity.ok(successUpload);
        } catch (ParseException e) {
            ExceptionVO ex = new ExceptionVO(ErrorCodeEnum.INVALID_DATE.getErrorCode(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(ex)).body(null);
        } catch (BusinessException e) {
            return Utils.returnExceptionToFrontEnd(e);
        }
    }

    @RequestMapping(value = "/web/rest/reasoncode/site/{site}/alert/verify/upload_file", method = RequestMethod.POST)
    public ResponseEntity<?> processUpload(@RequestParam(value = "file") MultipartFile file,
            @PathVariable("site") String site, HttpServletResponse res, HttpServletRequest request,
            @RequestParam(value = "startDate", required = true) String startDate) {
        try {
            logger.debug("file upload " + file.getOriginalFilename());

            Utils.checkUploadFilename(file.getOriginalFilename());
            ReasonCodeAlertUploadVO vo = this.reasoncodeAlertService.processUploadFile(file, site, startDate);
            logger.debug(">>>>>> " + " : " + gson.toJson(vo));

            res.setHeader("Content-Type", "application/json");
            res.setStatus(HttpServletResponse.SC_ACCEPTED);
            return ResponseEntity.ok(vo);
        } catch (IOException e) {
            ExceptionVO ex = new ExceptionVO(ErrorCodeEnum.UPLOAD_FILENAME.getErrorCode(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(ex)).body(null);
        } catch (ParseException e) {
            ExceptionVO ex = new ExceptionVO(ErrorCodeEnum.INVALID_DATE.getErrorCode(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(ex)).body(null);

        } catch (BusinessException e) {
            return Utils.returnExceptionToFrontEnd(e);
        }
    }

    @RequestMapping(value = "/web/rest/reasoncode/site/{site}/alert/commit/is_reason_code_alert_used",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> isReasonCodeAlertUsed(
            @PathVariable("site") String site,
            @RequestParam(value = "handle", required = true) String handle) {
        String trace = String.format("[ReasonCodeAlertController.isReasonCodeAlertUsed] request : site=%s, handle=%s",
                site, handle);
        logger.debug(">> " + trace);
        boolean vos = reasoncodeAlertService.isReasonCodeAlertUsed(site, handle);
        logger.debug("<< [ReasonCodeAlertController.isReasonCodeAlertUsed] response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/reasoncode/alert/commit/change_reason_code_alert",
            method = RequestMethod.POST)
    public ResponseEntity<?> changeReasonCodeAlert(@RequestBody List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> pos) {
        String trace = String.format("[ReasonCodeAlertController.changeReasonCodeAlert] request : " + gson.toJson(pos));
        logger.debug(">> " + trace);
        try {
            reasoncodeAlertService.changeReasonCodeAlert(pos);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ReasonCodeAlertController.changeReasonCodeAlert() : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ReasonCodeAlertController.changeReasonCodeAlert] response : void");
        return ResponseEntity.ok().body(null);
    }

    @RequestMapping(value = "/download/reasoncode/site/{site}/alert/excel", method = RequestMethod.GET)
    public ResponseEntity<?> exportExcel(@PathVariable("site") String site, HttpServletResponse response,
            HttpServletRequest request) {

     try {
            String filePath = request.getSession().getServletContext().getRealPath("/");
            filePath = filePath + "/alert.xlsx";
            Utils.generateDownloadFile(response, new File(filePath));

        } catch (IOException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
            logger.error("alertExportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok(null);
    }

}
