package mhp.oee.web.angular.reasoncode.management;

import java.util.List;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ReasonCodeComponent;
import mhp.oee.po.extend.ReasonCodeAndReasonCodeGroupPO;
import mhp.oee.po.gen.ReasonCodePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ReasonCodeVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class ReasonCodeUiService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    ReasonCodeComponent reasonCodeComponent;

    public List<ReasonCodeVO> readAllResonCode(String site, String reasonCodeGroup, String enabled, int index, int count){
        List<ReasonCodeAndReasonCodeGroupPO> vos = reasonCodeComponent.readAllResonCode(site, reasonCodeGroup, enabled, count, Utils.getOffset(count, index));
        return Utils.copyListProperties(vos, ReasonCodeVO.class);
    }
    
    public List<ReasonCodeVO> readAllResonCodeAll(String site, String reasonCodeGroup, String enabled){
        List<ReasonCodeAndReasonCodeGroupPO> vos = reasonCodeComponent.readAllResonCodeAll(site, reasonCodeGroup, enabled);
        return Utils.copyListProperties(vos, ReasonCodeVO.class);
    }
    
    public int readAllResonCodeCount(String site, String reasonCodeGroup, String enabled){
        int vos = reasonCodeComponent.readAllResonCodeCount(site, reasonCodeGroup, enabled);
        return vos;
    }

    public boolean existResonCode(String site, String resonCode) {
        ReasonCodePO po = reasonCodeComponent.existResonCode(site, resonCode);
        return (po != null) ? true : false;
    }

    public void changeReasonCode(List<ReasonCodeVO> reasonCodeVOs) throws BusinessException {
        for (ReasonCodeVO each : reasonCodeVOs) {
            switch (each.getViewActionCode()) {
            case 'C':
                reasonCodeComponent.createReasonCode(each);
                break;
            case 'U':
                reasonCodeComponent.updateReasonCode(each);
                break;
            default:
                logger.error("ReasonCodeUiService.changeReasonCode() : Invalid viewActionCode '" + each.getViewActionCode() + "'.");
                break;
            }
        }
    }
}
