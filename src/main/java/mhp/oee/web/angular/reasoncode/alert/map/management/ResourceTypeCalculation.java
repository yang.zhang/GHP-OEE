package mhp.oee.web.angular.reasoncode.alert.map.management;

public class ResourceTypeCalculation {
    private String resourceType;
    private Integer excelNumber;
    private Integer databaseNumber;

    public ResourceTypeCalculation() {
    }

    public ResourceTypeCalculation(String resourceType) {
        this.resourceType = resourceType;
    }

    public Integer getDatabaseNumber() {
        return databaseNumber;
    }

    public void setDatabaseNumber(Integer databaseNumber) {
        this.databaseNumber = databaseNumber;
    }

    public Integer getExcelNumber() {
        return excelNumber;
    }

    public void setExcelNumber(Integer excelNumber) {
        this.excelNumber = excelNumber;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((resourceType == null) ? 0 : resourceType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ResourceTypeCalculation other = (ResourceTypeCalculation) obj;
        if (resourceType == null) {
            if (other.resourceType != null)
                return false;
        } else if (!resourceType.equals(other.resourceType))
            return false;
        return true;
    }

}
