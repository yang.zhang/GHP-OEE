package mhp.oee.web.angular.reasoncode.management;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.po.extend.ReasonCodeAndReasonCodeGroupPO;
import mhp.oee.po.gen.ResourceStatePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStateVO;
import mhp.oee.vo.ReasonCodeResStateVO;
import mhp.oee.web.Constants;

@RestController
public class ReasonCodeResStateController {

    @InjectableLogger
    private static Logger logger;
    private static String ENABLED_Y = "true";

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ReasonCodeResStateService reasonCodeResStateService;

    @RequestMapping(value = "/web/rest/plant/site/{site}/resource_state"
            , method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ResourceStatePO>> readResourceState(
            @PathVariable("site") String site,
            @RequestParam(value = "enabled", required = true) String enabled) {
        String trace = String.format("[ReasonCodeResStateController.readResourceState] request : site=%s, enabled%s",
                site, enabled);
        logger.debug(">> " + trace);
        List<ResourceStatePO> vos = reasonCodeResStateService.readResourceState(site, enabled);
        logger.debug("<< [ReasonCodeResStateController.readResourceState] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/reason_code_res_state", 
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStateVO>> readReasonCodeResState(
            @PathVariable("site") String site,
            @RequestParam(value = "reason_code_group", required = true) String reasonCodeGroup,
            @RequestParam(value = "reaon_code", required = false) String reaonCode,
            @RequestParam(value = "date_time", required = true) String dateTime,
            @RequestParam(value = "index", required = true) int index,
            @RequestParam(value = "count", required = true) int count){
        String trace = String.format("[ReasonCodeResStateController.readResourceState] request : site=%s, reasonCodeGroup=%s, reaonCode=%s, dateTime=%s, index=%s, count=%s",
                site, reasonCodeGroup, reaonCode, dateTime, index, count);
        logger.debug(">> " + trace);
        int limit = count;
        int offset = Utils.getOffset(count, index);
        List<ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStateVO> vos = null;
        try {
            vos = reasonCodeResStateService.readReasonCodeResState(site, reasonCodeGroup, reaonCode, dateTime, limit, offset);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ReasonCodeResStateController.readReasonCodeResState() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        } 
        logger.debug("<< [ReasonCodeResStateController.readReasonCodeResState] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/reason_code_res_state_count", 
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Integer> readReasonCodeResStateCount(
            @PathVariable("site") String site,
            @RequestParam(value = "reason_code_group", required = true) String reasonCodeGroup,
            @RequestParam(value = "reaon_code", required = false) String reaonCode,
            @RequestParam(value = "date_time", required = true) String dateTime) {
        String trace = String.format("[ReasonCodeResStateController.readReasonCodeResStateCount] request : site=%s, reasonCodeGroup=%s, reaonCode=%s, dateTime=%s",
                site, reasonCodeGroup, reaonCode, dateTime);
        logger.debug(">> " + trace);
        int vos = 0;
        try {
            vos = reasonCodeResStateService.readReasonCodeResStateCount(site, reasonCodeGroup, reaonCode, dateTime);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ReasonCodeResStateController.readReasonCodeResStateCount() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ReasonCodeResStateController.readReasonCodeResStateCount] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/reason_code_res_state_add", 
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ReasonCodeAndReasonCodeGroupPO>> readResourceStateAddUI(
            @PathVariable("site") String site,
            @RequestParam(value = "reason_code_group", required = true) String reasonCodeGroup,
            @RequestParam(value = "reaon_code", required = false) String reaonCode) {
        String trace = String.format("[ReasonCodeResStateController.readResourceStateAddUI] request : site=%s, reasonCodeGroup=%s, reaonCode=%s",
                site, reasonCodeGroup, reaonCode);
        logger.debug(">> " + trace);
        List<ReasonCodeAndReasonCodeGroupPO> vos = reasonCodeResStateService.readResourceStateAddUI(site, reasonCodeGroup, reaonCode, ENABLED_Y);
        logger.debug("<< [ReasonCodeResStateController.readResourceStateAddUI] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/check_reason_code_res_state", 
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> checkResonCodeResourceState(
            @PathVariable("site") String site,
            @RequestParam(value = "reaon_code", required = true) String reaonCode,
            @RequestParam(value = "date_time", required = true) String dateTime,
            @RequestParam(value = "state", required = true) String state) {
        String trace = String.format("[ReasonCodeResStateController.checkResonCodeResourceState] request : site=%s, reaonCode=%s, dateTime=%s, state=%s",
                site, reaonCode, dateTime, state);
        logger.debug(">> " + trace);
        boolean result = false;
        try {
            result = reasonCodeResStateService.checkResonCodeResourceState(site, reaonCode, dateTime, state);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ReasonCodeResStateController.checkResonCodeResourceState() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ReasonCodeResStateController.checkResonCodeResourceState] response : "  + gson.toJson(result));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/web/rest/plant/change_reason_code_res_state", 
            method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeResonCodeResourceState(
            @RequestBody List<ReasonCodeResStateVO> reasonCodeResStateVO) {
        String trace = String.format("[ReasonCodeResStateController.changeResonCodeResourceState] request : " + gson.toJson(reasonCodeResStateVO));
        logger.debug(">> " + trace);
        try {
            reasonCodeResStateService.changeResonCodeResourceState(reasonCodeResStateVO);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ReasonCodeResStateController.changeResonCodeResourceState() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ReasonCodeResStateController.changeResonCodeResourceState() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ReasonCodeResStateController.changeResonCodeResourceState] response : void");
        return ResponseEntity.ok().body(null);
    }

}
