package mhp.oee.web.angular.reasoncode.group.management;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ReasonCodeGroupComponent;
import mhp.oee.po.gen.ReasonCodeGroupPO;
import mhp.oee.vo.ReasonCodeGroupVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class ReasonCodeGroupService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    ReasonCodeGroupComponent reasonCodeGroupComponent;

    public List<ReasonCodeGroupVO> readAllResonCodeGroup(String site, String enabled) {
        List<ReasonCodeGroupVO> vos = reasonCodeGroupComponent.readAllResonCodeGroup(site, enabled);
        return vos;
    }

    public boolean existResonCodeGroup(String site, String resonCodeGroup) {
        ReasonCodeGroupPO po = reasonCodeGroupComponent.existResonCodeGroup(site, resonCodeGroup);
        return (po != null) ? true : false;
    }

    public void changeReasonCodeGroup(List<ReasonCodeGroupVO> reasonCodeGroupVOs) throws BusinessException {
        for (ReasonCodeGroupVO each : reasonCodeGroupVOs) {
            switch (each.getViewActionCode()) {
            case 'C':
                reasonCodeGroupComponent.createReasonCodeGroup(each);
                break;
            case 'U':
                reasonCodeGroupComponent.updateReasonCodeGroup(each);
                break;
            default:
                logger.error("ReasonCodeGroupService.changeReasonCodeGroup() : Invalid viewActionCode '"
                        + each.getViewActionCode() + "'.");
                break;
            }
        }
    }
}
