package mhp.oee.web.angular.std.cycletime.management.excel.parser;

import java.util.List;

import mhp.oee.common.exception.ExistingException;
import mhp.oee.common.exception.InvalidModelLengthException;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.logging.GenericLoggerFactory;
import mhp.oee.common.security.SpringContextHolder;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.component.production.ItemComponent;
import mhp.oee.po.gen.ItemPO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.web.Constants;
import mhp.oee.web.angular.excel.parser.api.CellValidator;
import mhp.oee.web.angular.resource.plc.management.excel.parser.ExistingCellValidator;
import mhp.oee.web.angular.std.cycletime.management.excel.parser.CycleTimerCell.CycleTimerIndex;

public class ExistingCycleTimerValidator extends CellValidator {


    public ExistingCycleTimerValidator(CycleTimerCell cell){
        this.cell = cell;
        this.cell.attach(this);
        logger = GenericLoggerFactory.getLogger(ExistingCellValidator.class);
     }

    @Override
    public void validator() {
        if(this.cell.getValue().trim().length() == 0) {
            return;
        }

        if(this.cell.getKey().equals(CycleTimerIndex.DeviceNo)){
            String handle = new ResrceBOHandle(this.cell.getSite(), this.cell.getValue()).getValue();
            ResourceComponent dao = (ResourceComponent)SpringContextHolder.getBean("resourceComponent");
            ResourcePO po = dao.getResourceByHandle(handle);
            if(po == null || !po.getEnabled().equals("true") ) {
                this.cell.addException(new ExistingException());
            }

            return;
        }

        //遍历EXCEL上每一条记录获取Model，从表ITEM取ITEM ，条件是SITE = 全局变量中的工厂 and
        //（ MODEL = MODEL编码 ）and ENABLED = ‘true’
        /*
        if(this.cell.getKey().equals(CycleTimerIndex.Model)){
            ItemComponent dao = (ItemComponent)SpringContextHolder.getBean("itemComponent");
            List<ItemPO> readItems = dao.readItems(this.cell.getSite(), this.cell.getValue(), "true");
            if(readItems.size() == 0) {
                this.cell.addException(new ExistingException());
            }

            return;
        }
        */

        //校验EXCEL里填写的Model必须为6或7位，数字和字母不限制
        //遍历EXCEL上每一条记录获取Model，从表ITEM取ITEM ，条件是SITE = 全局变量中的工厂 and （
        //MODEL = MODEL编码 or ITEM 包含 MODEL编码）and ENABLED = ‘true’。
        //将获取的单条或多条item与每行的设备编码、理论PPM和有效开始日期组合成新的批量上载数据集。若没有从ITEM获取到物料编码，则提示“Model无对应的物料编码”
        if(this.cell.getKey().equals(CycleTimerIndex.Model)){
            String model = this.cell.getValue();
            if(!(model.length() == Constants.MODEL_SIX || model.length() == Constants.MODEL_SEVEN)){
                this.cell.addException(new InvalidModelLengthException());
                return;
            }

            ItemComponent dao = (ItemComponent)SpringContextHolder.getBean("itemComponent");
            List<ItemPO> readItems = dao.readItems(this.cell.getSite(), this.cell.getValue(), "true");
            List<ItemPO> readItemsList = dao.readItemsByModelInItem(this.cell.getSite(), this.cell.getValue(), "true");
            if(readItems.size() == 0 && readItemsList.size() == 0) {
                this.cell.addException(new ExistingException());
            }

            return;
        }




    }
}