package mhp.oee.web.angular.std.cycletime.management.excel.parser;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mhp.oee.common.security.SpringContextHolder;
import mhp.oee.component.production.ItemComponent;
import mhp.oee.po.gen.ItemPO;
import mhp.oee.po.gen.StdCycleTimePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ItemVO;
import mhp.oee.web.angular.excel.parser.api.CustomerRow;
import mhp.oee.web.angular.std.cycletime.management.excel.parser.CycleTimerCell.CycleTimerIndex;

public class CycleTimerRow extends CustomerRow {

    public CycleTimerRow(String site) {
        CycleTimerCell.initCellAttribute(this.getCellArray(), site);
    }

    @Override
    public Object getVO(String site) {
        List<StdCycleTimePO> list = new ArrayList<StdCycleTimePO>();

        String deviceNo = this.getValue(CycleTimerIndex.DeviceNo);
        String model = this.getValue(CycleTimerIndex.Model);
        String ppm = this.getValue(CycleTimerIndex.PPM);
        String startDate = this.getValue(CycleTimerIndex.StartDate);

        ItemComponent dao = (ItemComponent)SpringContextHolder.getBean("itemComponent");
        List<ItemPO> readItems = dao.readItems(site, model, "true");
        List<ItemPO> readItemsList = dao.readItemsByModelInItem(site, model, "true");

        Set<ItemVO> itemSet = new HashSet<ItemVO>();
        itemSet.addAll(Utils.copyListProperties(readItems, ItemVO.class));
        itemSet.addAll(Utils.copyListProperties(readItemsList, ItemVO.class));



        for(ItemVO itemVO : itemSet) {
            StdCycleTimePO vo = new StdCycleTimePO();
            vo.setSite(site);
            vo.setItem(itemVO.getItem());
            vo.setOperation("*");
            vo.setResrce(deviceNo);

            vo.setPpmTheory(new BigDecimal(ppm));
            vo.setStdCycleTime(new BigDecimal(Utils.getCycleTime(ppm)));
            vo.setEndDate(Utils.getInfiniteDate());


            try {
                vo.setStrt(Utils.getFormattedDateStringfromExcel(startDate));
                vo.setStartDate(Utils.getDatefromExcel(startDate));
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            list.add(vo);
        }
        return list;
    }
}