package mhp.oee.web.angular.std.cycletime.management;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.web.Constants;
import mhp.oee.web.angular.resource.plc.management.PlcInfoService;

@RestController
public class RestStdCycleTimeController {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    StdCycleTimeService cycleTimeService;

    @Autowired
    PlcInfoService plcInfoService;

    @RequestMapping(value = "/web/rest/std/cycletime/management/site/{site}/search", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<StdCycleTimeAndResourceVO>> searchCycleTimes(@PathVariable("site") String site,
            @RequestParam(value = "item", required = true) String item,
            @RequestParam(value = "resrce", required = false) String resrce,
            @RequestParam(value = "date", required = true) String dateString,
            @RequestParam(value = "index", required = true) String index,
            @RequestParam(value = "count", required = true) String count,
            @RequestParam(value = "model", required = true) String model,
            @RequestParam(value = "resourceType", required = true) String resourceType,
            @RequestParam(value = "line", required = true) String line,
            @RequestParam(value = "workCenter", required = true) String workCenter,
            HttpServletResponse res, HttpServletRequest request) {

        String trace = String.format("[search][site/%s/ppm] : item=%s, resrce=%s, date=%s, model=%s, resourceType=%s, "
                                   + "lint=%s workCenter=%s index=%s count=%s",
                                   site, item, resrce, dateString, model, resourceType, line, workCenter, index, count);
        logger.debug(">> " + trace);
        List<StdCycleTimeAndResourceVO> list;
        try {
            Date date = Utils.getDatefromString(dateString);
            if (resrce != null && resrce.trim().length() != 0) {
                list = cycleTimeService.searchCycleTime(site, item, resrce, date, model, index, count);
            } else {
                list = cycleTimeService.searchCycleTime(site, item, resourceType, line, workCenter, date, model, index, count);
            }
            logger.debug("<< " + trace + " : " + gson.toJson(list));
            return ResponseEntity.ok().body(list);
        } catch (ParseException e) {
            logger.error("searchCycleTimes() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            res.setHeader("Content-Type", "application/json");
            res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
    }

    @RequestMapping(value = "/web/rest/std/cycletime/management/site/{site}/total_number/search", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Integer> searchCycleTimesTotalNumber(@PathVariable("site") String site,
            @RequestParam(value = "item", required = false) String item,
            @RequestParam(value = "resrce", required = false) String resrce,
            @RequestParam(value = "date", required = true) String dateString,
            @RequestParam(value = "model", required = false) String model,
            @RequestParam(value = "resourceType", required = true) String resourceType,
            @RequestParam(value = "line", required = true) String line,
            @RequestParam(value = "workCenter", required = false) String workCenter,
            HttpServletResponse res, HttpServletRequest request) {

        String trace = String.format("[search][site/%s/ppm] : item=%s, resrce=%s, date=%s, model=%s, resourceType=%s, "
                + "line=%s workCenter=%s",
                site, item, resrce, dateString, model, resourceType, line, workCenter);
        logger.debug(">> " + trace);
        int number;
        try {
            Date date = Utils.getDatefromString(dateString);
            if (resrce != null && resrce.trim().length() != 0) {
                number = cycleTimeService.searchCycleTimeTotalNumber(site, item, resrce, date);
            } else {
                number = cycleTimeService.searchCycleTimeTotalNumber(site, item, resourceType, line, workCenter, date);
            }
            logger.debug("<< " + trace + " : " + gson.toJson(number));
            return ResponseEntity.ok().body(number);
        } catch (ParseException e) {
            logger.error("searchCycleTimes() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            res.setHeader("Content-Type", "application/json");
            res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
    }

    @RequestMapping(value = "/web/rest/std/cycletime/management/site/{site}/change_std_cycle_time", method = RequestMethod.POST)
    public ResponseEntity<?> changeStdCycleTime(@PathVariable("site") String site,
    		@RequestBody StdCycleTimeChangeVO vos,
            HttpServletResponse res, HttpServletRequest request
    		) {
  
        try {
        		Date date = Utils.getDatefromString(vos.getDate());
        		cycleTimeService.changeCycleTimeList(vos.getEditList(), site,vos.getCheckAll(),vos.getPpm(),vos.getItem(),vos.getResrce(),date,vos.getModel(),vos.getResourceType(),vos.getLine(),vos.getWorkCenter());
        	
        	
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.INVALID_DATE.getErrorCode(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok().body(null);
    }

    @RequestMapping(value = "/web/rest/std/cycletime/management/site/{site}/upload_file", method = RequestMethod.POST)
    public ResponseEntity<?> processUpload(@RequestParam(value = "file") MultipartFile file,
            @PathVariable("site") String site, HttpServletResponse res, HttpServletRequest request)
            throws ParseException {
        try {
            logger.debug("file upload " + file.getOriginalFilename());
            Utils.checkUploadFilename(file.getOriginalFilename());
            StdCycleTimeUploadVO vo = this.cycleTimeService.processUploadFile(file, site);
            logger.debug(">>>>>> " + " : " + gson.toJson(vo));
            res.setHeader("Content-Type", "application/json");
            res.setStatus(HttpServletResponse.SC_ACCEPTED);
            return ResponseEntity.ok(vo);
        } catch (IOException e) {
            logger.error("ppmUpload() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            res.setHeader("Content-Type", "application/json");
            res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }  catch (BusinessException e) {
            return Utils.returnExceptionToFrontEnd(e);
        }
    }



    @RequestMapping(value = "/web/rest/std/cycletime/management/site/{site}/check_existing_cycletime", method = RequestMethod.POST)
    public ResponseEntity<?> checkExistingStdCycleTimeKey(@PathVariable("site") String site,
            @RequestBody StdCycleTimeKey key) {

       ExistingObject object = this.cycleTimeService.checkExistingStdCycleTime(site, key);
       return ResponseEntity.ok().body(gson.toJson(object));

    }

    @RequestMapping(value = "/download/std/cycletime/management/site/{site}/excel", method = RequestMethod.GET)
    public ResponseEntity<?> exportExcel(@PathVariable("site") String site, HttpServletResponse response,
            HttpServletRequest request) {

     try {

            String filePath = request.getSession().getServletContext().getRealPath("/");
            filePath = filePath + "/ppm.xlsx";
            Utils.generateDownloadFile(response, new File(filePath));

        } catch (IOException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.IO.getErrorCode(), e.getMessage(), null);
            logger.error("alertExportExcel() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok(null);
    }

}
