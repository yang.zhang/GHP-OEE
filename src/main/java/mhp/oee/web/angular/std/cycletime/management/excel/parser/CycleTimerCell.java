package mhp.oee.web.angular.std.cycletime.management.excel.parser;

import java.util.List;

import mhp.oee.web.angular.excel.parser.api.CustomerCell;
import mhp.oee.web.angular.excel.parser.api.CustomerIndex;
import mhp.oee.web.angular.excel.parser.api.MandatoryCellValidator;

public class CycleTimerCell extends CustomerCell {

    public CycleTimerCell(CycleTimerIndex key, String value, String site) {
        super(key, value, site);
    }

    public static List<CustomerCell> initCellAttribute(List<CustomerCell> list, String site) {

        CycleTimerCell deviceCell = new CycleTimerCell(CycleTimerIndex.DeviceNo, "", site);
        new MandatoryCellValidator(deviceCell);
        new ExistingCycleTimerValidator(deviceCell);
        list.add(CycleTimerIndex.DeviceNo.getIndex(), deviceCell);

        CycleTimerCell modelCell = new CycleTimerCell(CycleTimerIndex.Model, "", site);
        new MandatoryCellValidator(modelCell);
        new ExistingCycleTimerValidator(modelCell);
        list.add(CycleTimerIndex.Model.getIndex(), modelCell);


        CycleTimerCell ppmCell = new CycleTimerCell(CycleTimerIndex.PPM, "", site);
        new MandatoryCellValidator(ppmCell);
        new PPMValidator(ppmCell);
        list.add(CycleTimerIndex.PPM.getIndex(), ppmCell);


        CycleTimerCell startDateCell = new CycleTimerCell(CycleTimerIndex.StartDate, "", site);
        new MandatoryCellValidator(startDateCell);
        new StartTimeValidator(startDateCell);
        new RelationCycleTimerValidation(startDateCell, deviceCell, modelCell);
        list.add(CycleTimerIndex.StartDate.getIndex(), startDateCell);

        return list;
    }

    public enum CycleTimerIndex implements CustomerIndex {
        DeviceNo(0, "resrce"),
        Model(1, "model"),
        PPM(2, "ppmTheory"),
        StartDate(3, "startDate");

        private Integer index;
        private String value;

        private CycleTimerIndex(Integer index, String value) {
            this.index = index;
            this.value = value;
        }

        @Override
        public String toString() {
            return this.index.toString();
        }

        @Override
        public Integer getIndex() {
            return this.index;
        }

        @Override
        public String getValue() {
            return this.value;
        }
    }

}