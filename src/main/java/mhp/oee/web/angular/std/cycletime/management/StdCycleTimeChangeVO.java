package mhp.oee.web.angular.std.cycletime.management;

import java.util.List;

import mhp.oee.po.gen.StdCycleTimePO;

public class StdCycleTimeChangeVO{
	private String checkAll;
	private String ppm;
	private String item;
	private String resrce;
	private String date;
	private String model;
	private String resourceType;
	private String line;
	private String workCenter;
	
	private List<StdCycleTimeAndResourceVO> editList;
	
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getResrce() {
		return resrce;
	}
	public void setResrce(String resrce) {
		this.resrce = resrce;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public String getWorkCenter() {
		return workCenter;
	}
	public void setWorkCenter(String workCenter) {
		this.workCenter = workCenter;
	}
	public List<StdCycleTimeAndResourceVO> getEditList() {
		return editList;
	}
	public void setEditList(List<StdCycleTimeAndResourceVO> editList) {
		this.editList = editList;
	}
	public String getCheckAll() {
		return checkAll;
	}
	public void setCheckAll(String checkAll) {
		this.checkAll = checkAll;
	}
	public String getPpm() {
		return ppm;
	}
	public void setPpm(String ppm) {
		this.ppm = ppm;
	}
		
	


}
