package mhp.oee.web.angular.std.cycletime.management.excel.parser;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import mhp.oee.common.exception.InvalidCycleTimeException;
import mhp.oee.common.logging.GenericLoggerFactory;
import mhp.oee.common.security.SpringContextHolder;
import mhp.oee.component.production.ItemComponent;
import mhp.oee.component.production.StdCycleTimeComponent;
import mhp.oee.po.gen.ItemPO;
import mhp.oee.po.gen.StdCycleTimePO;
import mhp.oee.utils.Utils;
import mhp.oee.web.angular.excel.parser.api.CellValidator;
import mhp.oee.web.angular.excel.parser.api.CustomerCell;

public class RelationCycleTimerValidation extends CellValidator {

    private CustomerCell deviceCell;
    private CustomerCell modelCell;

    public RelationCycleTimerValidation(CustomerCell cell, CustomerCell deviceCell, CustomerCell modelCell) {
        this.cell = cell;
        this.deviceCell = deviceCell;
        this.modelCell = modelCell;
        this.cell.attach(this);
        logger = GenericLoggerFactory.getLogger(RelationCycleTimerValidation.class);
    }

    @Override
    public void validator() {
        // 校验上载数据的有效开始日期必须晚于
        // 表STD_CYCLE_TIME中设备编码+物料编码+工厂对应的有效结束日期为9999.12.31这条记录的有效开始日期
        String device = deviceCell.getValue();
        String model = modelCell.getValue();
        String site = this.cell.getSite();
        String startDate = this.cell.getValue();
        Date infiniteDate = Utils.getInfiniteDate();

        if (device.trim().length() == 0 || model.trim().length() == 0 || model.trim().length() == 0
                || startDate.trim().length() == 0) {
            return;
        }

        try {
            Date date = Utils.getDatefromExcel(this.cell.getValue());

            StdCycleTimeComponent subDao = (StdCycleTimeComponent) SpringContextHolder.getBean("stdCycleTimeComponent");

            ItemComponent dao = (ItemComponent) SpringContextHolder.getBean("itemComponent");
            List<ItemPO> readItems = dao.readItems(site, model, "true");
            for (ItemPO po : readItems) {
                String item = po.getItem();
                List<StdCycleTimePO> readStdCycleTime = subDao.readStdCycleTime(site, device, item, infiniteDate);
                for (StdCycleTimePO stdCycleTimePO : readStdCycleTime) {
                    if (!date.after(stdCycleTimePO.getStartDate())) {
                        logger.debug(">>>>>>>>insert date   " + date + "    existing date    "
                                + stdCycleTimePO.getStartDate());
                        this.cell.addException(new InvalidCycleTimeException());
                        return;
                    }
                }
            }
        } catch (ParseException e) {
            return;
        }
    }

}
