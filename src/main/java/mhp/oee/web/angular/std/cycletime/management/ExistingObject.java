package mhp.oee.web.angular.std.cycletime.management;

public class ExistingObject {
    private boolean existing;

    public ExistingObject() {

    }

    public boolean isExisting() {
        return existing;
    }

    public void setExisting(boolean existing) {
        this.existing = existing;
    }
}