package mhp.oee.web.angular.std.cycletime.management.excel.parser;

import java.text.DecimalFormat;

import mhp.oee.common.exception.InvalidNumberException;
import mhp.oee.common.logging.GenericLoggerFactory;
import mhp.oee.web.angular.excel.parser.api.CellValidator;
import mhp.oee.web.angular.std.cycletime.management.excel.parser.CycleTimerCell.CycleTimerIndex;

public class PPMValidator extends CellValidator {


    public PPMValidator(CycleTimerCell cell){
        this.cell = cell;
        this.cell.attach(this);
        logger = GenericLoggerFactory.getLogger(PPMValidator.class);
     }

    @Override
    public void validator() {
        if(this.cell.getValue().trim().length() == 0) {
            return;
        }

        if(this.cell.getKey().equals(CycleTimerIndex.PPM)){
            DecimalFormat df = new DecimalFormat();
            double data = Double.parseDouble(this.cell.getValue());

            String pattern = "0.00";
            df.applyPattern(pattern);
            double formateData = Double.parseDouble(df.format(data));
            if(data != formateData || data <= 0 ) {
                logger.debug(">>>>>>>>data error   " + data + "   " + formateData);
                this.cell.getExceptionList().add(new InvalidNumberException());
            }

            return;
        }


    }
}