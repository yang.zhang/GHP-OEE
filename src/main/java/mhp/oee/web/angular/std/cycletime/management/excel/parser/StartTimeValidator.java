package mhp.oee.web.angular.std.cycletime.management.excel.parser;

import java.text.ParseException;

import mhp.oee.common.exception.InvalidDateException;
import mhp.oee.common.logging.GenericLoggerFactory;
import mhp.oee.utils.Utils;
import mhp.oee.web.angular.excel.parser.api.CellValidator;
import mhp.oee.web.angular.std.cycletime.management.excel.parser.CycleTimerCell.CycleTimerIndex;

public class StartTimeValidator extends CellValidator {

    public StartTimeValidator(CycleTimerCell cell) {
        this.cell = cell;
        this.cell.attach(this);
        logger = GenericLoggerFactory.getLogger(StartTimeValidator.class);
    }

    @Override
    public void validator() {
        if (this.cell.getValue().trim().length() == 0) {
            return;
        }

        if (this.cell.getKey().equals(CycleTimerIndex.StartDate)) {
            try {
                Utils.getDatefromExcel(this.cell.getValue());
            } catch (ParseException e) {
                logger.debug(">>>>>>>>date formate error   " + this.cell.getValue());
                this.cell.addException(new InvalidDateException());
            }

            return;
        }

    }
}