package mhp.oee.web.angular.std.cycletime.management;

import mhp.oee.po.gen.StdCycleTimePO;

public class StdCycleTimeAndResourceVO extends StdCycleTimePO {


    private String startDateDec;
    private String endDateDec;


    private String resrceDes;
    private String asset;
    private String model;

    private Character viewActionCode;



    public String getResrceDes() {
        return resrceDes;
    }
    public void setResrceDes(String resrceDes) {
        this.resrceDes = resrceDes;
    }
    public String getAsset() {
        return asset;
    }
    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }




    public Character getViewActionCode() {
        return viewActionCode;
    }
    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }
    public String getEndDateDec() {
        return endDateDec;
    }
    public void setEndDateDec(String endDateDec) {
        this.endDateDec = endDateDec;
    }
    public String getStartDateDec() {
        return startDateDec;
    }
    public void setStartDateDec(String startDateDec) {
        this.startDateDec = startDateDec;
    }




}
