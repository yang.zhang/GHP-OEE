package mhp.oee.web.angular.std.cycletime.management;

public class StdCycleTimeKey {
    private String item;
    private String resrce;
    private String dateDec;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public String getDateDec() {
        return dateDec;
    }

    public void setDateDec(String dateDec) {
        this.dateDec = dateDec;
    }

}
