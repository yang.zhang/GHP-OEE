package mhp.oee.web.angular.std.cycletime.management;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ExistingException;
import mhp.oee.common.exception.InvalidCycleTimeException;
import mhp.oee.common.exception.InvalidNumberException;
import mhp.oee.common.exception.MandatoryException;
import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.handle.StdCycleTimeBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.component.production.ItemComponent;
import mhp.oee.component.production.StdCycleTimeComponent;
import mhp.oee.dao.extend.StdCycleTimeAndResrceAndResourceTypeResourcePOMapper;
import mhp.oee.dao.gen.StdCycleTimePOMapper;
import mhp.oee.po.extend.StdCycleTimeAndResrceAndResourceTypeResourcePO;
import mhp.oee.po.gen.ItemPO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.StdCycleTimePO;
import mhp.oee.po.gen.StdCycleTimePOExample;
import mhp.oee.utils.JdbcHelper;
import mhp.oee.utils.Utils;
import mhp.oee.web.Constants;
import mhp.oee.web.angular.excel.parser.api.CustomerRow;
import mhp.oee.web.angular.excel.parser.api.JsonRow;
import mhp.oee.web.angular.std.cycletime.management.excel.parser.CycleTimerCell.CycleTimerIndex;
import mhp.oee.web.angular.std.cycletime.management.excel.parser.CycleTimerRow;

@Service
@Transactional(rollbackFor = { BusinessException.class, IOException.class })
public class StdCycleTimeService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    StdCycleTimeComponent stdCycleTimeComponent;

    @Autowired
    ResourceComponent resourceComponent;

    @Autowired
    ItemComponent itemComponent;
    
    @Autowired
    StdCycleTimePOMapper stdCycleTimePOMapper;
    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    public ExistingObject checkExistingStdCycleTime(String site, StdCycleTimeKey key) {

        logger.debug(">>>>>> check exist stdCycleTime >>>>>>");
        logger.debug(site);
        logger.debug(gson.toJson(key));
        StdCycleTimeBOHandle handle = new StdCycleTimeBOHandle(site, key.getItem(), "*", key.getResrce(), key.getDateDec());
        StdCycleTimePO po = stdCycleTimeComponent.getExistingStdCycleTimeRecordByKey(handle.getValue());

        ExistingObject object = new ExistingObject();
        if(po == null) {
            object.setExisting(false);
        } else {
            object.setExisting(true);
        }

        return object;
    }


    @SuppressWarnings("deprecation")
    public StdCycleTimeUploadVO processUploadFile(MultipartFile multiFile, String site)
            throws IOException, BusinessException, ParseException {
        XSSFWorkbook workbook = new XSSFWorkbook(multiFile.getInputStream());
        XSSFSheet sheet = workbook.getSheetAt(0);

        List<CustomerRow> rowArray = new ArrayList<CustomerRow>();
        for (int rowNumber = Constants.IGNORED_NUM; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            Row row = sheet.getRow(rowNumber);
            CustomerRow customerRow = new CycleTimerRow(site);
            for (int cellNumber = 0; cellNumber < Constants.PPM_UPLOAD_LENGTH; cellNumber++) {
                if (row != null && row.getCell(cellNumber) != null) {
                    Cell cell = row.getCell(cellNumber);
                    switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_NUMERIC:
                        if (cellNumber == CycleTimerIndex.StartDate.getIndex() && HSSFDateUtil.isCellDateFormatted(cell)) {
                            customerRow.setCellValue(cellNumber, rowNumber, cell.toString());
                        } else if (cellNumber == CycleTimerIndex.Model.getIndex()) {
                            customerRow.setCellValue(cellNumber, rowNumber,
                                    Integer.valueOf((int)cell.getNumericCellValue()).toString());
                        } else {
                            customerRow.setCellValue(cellNumber, rowNumber,
                                    Double.valueOf(cell.getNumericCellValue()).toString());
                        }
                        break;
                    case Cell.CELL_TYPE_STRING:
                        customerRow.setCellValue(cellNumber, rowNumber, cell.getStringCellValue());
                        break;
                    case Cell.CELL_TYPE_BLANK:
                        customerRow.setCellValue(cellNumber, rowNumber, cell.getStringCellValue());
                        break;
                    }
                } else {
                    customerRow.setCellValue(cellNumber, rowNumber, "");
                }

            }
            rowArray.add(customerRow);
        }
        workbook.close();

        List<CustomerRow> notEmptyRowArray = new ArrayList<CustomerRow>();
        for (CustomerRow row : rowArray) {
            if(!row.isEmptyRow()) {
                notEmptyRowArray.add(row);
            }
        }

        for (CustomerRow row : notEmptyRowArray) {
            logger.debug(">>>>>>>>>>> excel rows    "+row.toString());
        }

        StdCycleTimeUploadVO vo = new StdCycleTimeUploadVO();

        for (CustomerRow row : notEmptyRowArray) {
            JsonRow jsonRow = row.generateRowError();
            if (jsonRow != null) {
                vo.getExceptionRowArray().add(row.generateRowError());
            }
        }

        // if no exception, then store data into database
        // ResourcePlcVO resourcePlcVO       
      //List<String> sqllist = new ArrayList<String>();
       Map<String,String> map = new HashMap<String,String>();
        if (vo.getExceptionRowArray().size() == 0) {
            for (CustomerRow row : notEmptyRowArray) {
                List<StdCycleTimePO> list = (List<StdCycleTimePO>) row.getVO(site);
//                stdCycleTimeComponent.createStdCycleTimeList(list);
                stdCycleTimeComponent.createStdCycleTimeList_insertSql(list,map); 
                if(map.size()>20000){
                	JdbcHelper.batchInsert(map.values(),5000); 
                	map.clear();
                }
               
            }
            
           //sqllist.addAll(map.values());            
            JdbcHelper.batchInsert(map.values(),5000);          
            vo.getSuccessUpload().setCommitNumber(notEmptyRowArray.size());
        }

        return vo;
    }

    //TODO Unit Test
    public List<StdCycleTimeAndResourceVO> searchCycleTime(String site, String item, String resourceType, String line,
            String workCenter, Date date, String model, String index, String count) {
        String resourceBOhandle =new ResourceTypeBOHandle(site, resourceType).getValue();
        List<StdCycleTimeAndResrceAndResourceTypeResourcePO> viewList = stdCycleTimeComponent.search(site, item, resourceBOhandle, line, workCenter, date, new Integer(index), new Integer(count));

        List<StdCycleTimeAndResourceVO> voList = new ArrayList<StdCycleTimeAndResourceVO>();
        for(StdCycleTimeAndResrceAndResourceTypeResourcePO viewObj : viewList ) {
            StdCycleTimeAndResourceVO vo = Utils.copyObjectProperties(viewObj, StdCycleTimeAndResourceVO.class);
            vo.setAsset(viewObj.getAsset());
            vo.setResrceDes(viewObj.getDescription());
            vo.setModel(model);
            voList.add(vo);
        }

        return voList;
    }



    public List<StdCycleTimeAndResourceVO> searchCycleTime(String site, String item, String resrce, Date date,
            String model, String index, String count) {
        List<StdCycleTimePO> list = stdCycleTimeComponent.search(site, item, resrce, date, Integer.parseInt(index),
                Integer.parseInt(count));
        ResourcePO resource = resourceComponent.getResourceByKey(site, resrce);
        logger.debug(">>>>>>>  " + gson.toJson(resource));

        List<StdCycleTimeAndResourceVO> vos = new ArrayList<StdCycleTimeAndResourceVO>();

        for (StdCycleTimePO po : list) {

            StdCycleTimeAndResourceVO vo = Utils.copyObjectProperties(po, StdCycleTimeAndResourceVO.class);
            vo.setResrceDes(resource.getDescription());
            vo.setAsset(resource.getAsset());
            vo.setModel(model);

            vos.add(vo);
        }

        return vos;
    }

    public int searchCycleTimeTotalNumber(String site, String item, String resrce, Date date) {
        return stdCycleTimeComponent.searchCycleTimeTotalNumber(site, item, resrce, date);
    }

    //@TODO UnitTest
    public int searchCycleTimeTotalNumber(String site, String item, String resourceType, String line, String workCenter,
            Date date) {
        String resourceBOhandle =new ResourceTypeBOHandle(site, resourceType).getValue();
        return stdCycleTimeComponent.searchCycleTimeTotalNumber(site, item, resourceBOhandle, line, workCenter, date);
    }

    public void changeCycleTimeList(List<StdCycleTimeAndResourceVO> list, String site, String checkAll, String ppm, String item,
    		String resrce, Date date, String model, String resourceType, String line, String workCenter)
            throws BusinessException, ParseException {
    	if(checkAll.equals("1")){
    		if(resrce != null && resrce.trim().length() != 0){
    			List<StdCycleTimePO> listPpm = stdCycleTimeComponent.searchPPM(site, item, resrce, date);
    			 List<String> updateSqlList = new ArrayList<String>();
        		for(StdCycleTimePO stdCycleTimePO : listPpm){
        			stdCycleTimePO.setPpmTheory(new BigDecimal(ppm));
        			
        			String update_sql = stdCycleTimeComponent.updateExistingStdCycleTimeRecord_updateSql(stdCycleTimePO);
        			updateSqlList.add(update_sql);
        		}
        		JdbcHelper.batchInsert(updateSqlList,10000);
    		}else {
    			String resourceBOhandle =new ResourceTypeBOHandle(site, resourceType).getValue();
    			List<StdCycleTimeAndResrceAndResourceTypeResourcePO> viewList = stdCycleTimeComponent.searchPPM(site, item, resourceBOhandle, line, workCenter, date);
    				
    			  List<StdCycleTimeAndResourceVO> voList = new ArrayList<StdCycleTimeAndResourceVO>();
    			  List<String> updateSqlList = new ArrayList<String>();
    		        for(StdCycleTimeAndResrceAndResourceTypeResourcePO viewObj : viewList ) {
    		        	StdCycleTimePO vo = Utils.copyObjectProperties(viewObj, StdCycleTimeAndResourceVO.class);
    		            
    		            vo.setPpmTheory(new BigDecimal(ppm));
            			String update_sql = stdCycleTimeComponent.updateExistingStdCycleTimeRecord_updateSql(vo);
            			
            			updateSqlList.add(update_sql);
    		        }
    		        JdbcHelper.batchInsert(updateSqlList,10000);
            }
    		
    	}
    		for (StdCycleTimeAndResourceVO each : list) {
	            if (each.getStartDateDec() != null && each.getStartDateDec().trim().length() != 0) {
	                each.setStartDate(Utils.getDatefromString(each.getStartDateDec()));
	            }
	            if (each.getEndDateDec() != null && each.getEndDateDec().trim().length() != 0) {
	                each.setEndDate(Utils.getDatefromString(each.getEndDateDec()));
	            }
	            
	            StdCycleTimePO po = Utils.copyObjectProperties(each, StdCycleTimePO.class);

	            if (each.getViewActionCode().equals('C')) {
	                po.setSite(site);
	                po.setStrt(each.getStartDateDec());

	                this.validationCreateStdCycleTime(po);

	                po.setStdCycleTime(new BigDecimal(Utils.getCycleTime(po.getPpmTheory().toString())));
	                po.setOperation("*");
	                po.setEndDate(Utils.getInfiniteDate());

	                this.stdCycleTimeComponent.createStdCycleTime(po);
	            } else if (each.getViewActionCode().equals('U')) {
	                this.checkPPM(po);

	                po.setStdCycleTime(new BigDecimal(Utils.getCycleTime(po.getPpmTheory().toString())));
	                this.stdCycleTimeComponent.updateExistingStdCycleTimeRecord(po);
	            } else {
	                logger.error("PlantService.cudPlcCategoryChanges() : Invalid viewActionCode '"
	                        + each.getViewActionCode() + "'.");
	            }
	        }
    
    }

    private void validationCreateStdCycleTime(StdCycleTimePO po)
            throws MandatoryException, ExistingException, InvalidNumberException, InvalidCycleTimeException {
        logger.debug("create cycle time po..........    " + gson.toJson(po));
        // validation start time
        if (po.getStrt() == null || po.getStrt().trim().length() == 0) {
            logger.error(">>>>>>>> no start date string");
            throw new MandatoryException();
        }

        // validation resource
        if (po.getResrce() == null || po.getResrce().trim().length() == 0) {
            logger.error(">>>>>>>> no resource");
            throw new MandatoryException();
        } else {
            String handle = new ResrceBOHandle(po.getSite(), po.getResrce()).getValue();
            ResourcePO o = resourceComponent.getResourceByHandle(handle);
            if (o == null || !o.getEnabled().equals("true")) {
                logger.error(">>>>>>>> resource was not exist or resource was disabled");
                throw new ExistingException();
            }
        }

        // validation item
        if (po.getItem() == null || po.getItem().trim().length() == 0) {
            logger.error(">>>>>>>> no item");
            throw new MandatoryException();
        } else {
            List<ItemPO> readItems = itemComponent.readItemsByEnabledItem(po.getSite(), po.getItem(), "true");
            if (readItems.size() == 0) {
                logger.error(">>>>>>>> item was not exist or item was disabled");
                throw new ExistingException();
            }
        }

        // validation ppm
        checkPPM(po);

        // validation start date
        List<StdCycleTimePO> readStdCycleTime = stdCycleTimeComponent.readStdCycleTime(po.getSite(), po.getResrce(),
                po.getItem(), Utils.getInfiniteDate());
        for (StdCycleTimePO stdCycleTimePO : readStdCycleTime) {
            if (po.getStartDate().before(stdCycleTimePO.getStartDate())) {
                logger.error(">>>>>>>>insert date   " + po.getStartDate() + "    existing date    "
                        + stdCycleTimePO.getStartDate());
                throw new InvalidCycleTimeException();
            }
        }

    }

    // update operation only change the ppm value
    private void checkPPM(StdCycleTimePO po) throws MandatoryException, InvalidNumberException {
        if (po.getPpmTheory() == null) {
            logger.error(">>>>>>>> no ppm");
            throw new MandatoryException();
        } else {
            DecimalFormat df = new DecimalFormat();
            double data = Double.parseDouble(po.getPpmTheory().toString());
            String pattern = "0.0";
            df.applyPattern(pattern);
            double formateData = Double.parseDouble(df.format(data));
            if (data != formateData || data <= 0) {
                logger.error(">>>>>>>>ppm error   " + data + "   " + formateData);
                throw new InvalidNumberException();
            }
        }
    }

}