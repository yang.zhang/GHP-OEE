package mhp.oee.web.angular.activity.management;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.web.Constants;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by LinZuK on 2016/7/25.
 */
@RestController
public class ActivityParamController {

    @Autowired
    ActivityParamValueService activityParamValueService;
    @Autowired
    ActivityService activityService;

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // “作业”输入帮助
    @RequestMapping(value = "/web/rest/plant/activity/param/activities",
            method = RequestMethod.GET,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityManageVO.ActivityConditionVO>> searchActivities(@RequestParam(value = "activity", required = true) String activity) {
        List<ActivityManageVO.ActivityConditionVO> vos = activityService.getActivitiesLike(activity);
        return ResponseEntity.ok(vos);
    }

    // 查询
    @RequestMapping(value = "/web/rest/plant/activity/param/site/{site}/activity_param_values",
            method = RequestMethod.GET,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Map<String, Object>> searchActivityParamValues(@PathVariable("site") String site,
                                                                         @RequestParam(value = "activity", required = true) String activity) {
        List<ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO> vos = activityParamValueService.getActivityParamValues(site, activity);
        boolean hasActivity = activityParamValueService.hasActivity(activity);
        Map<String, Object> ret = new HashedMap();
        ret.put("list", vos);
        ret.put("hasActivity", hasActivity);
        return ResponseEntity.ok(ret);
    }

    // 编辑保存
    @RequestMapping(value = "/web/rest/plant/activity/param/site/{site}/change_activity_param_values",
            method = RequestMethod.POST,
            produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Object> changeActivityParamValues(@PathVariable("site") String site,
                                                            @RequestBody List<ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO> activityAndActivityParamAndActivityParamValueVO) {
        try {
            activityParamValueService.changeActivityParamValues(site, activityAndActivityParamAndActivityParamValueVO);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error(Utils.getCurrentMethodName(0) + " : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        return ResponseEntity.ok().body(null);
    }

}
