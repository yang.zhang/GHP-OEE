package mhp.oee.web.angular.activity.management;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.activity.ActivityParamValueComponent;
import mhp.oee.po.gen.ActivityPO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by LinZuK on 2016/7/25.
 */
@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class ActivityParamValueService {

    @Autowired
    private ActivityParamValueComponent activityParamValueComponent;

    @InjectableLogger
    private static Logger logger;

    public List<ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO> getActivityParamValues(String site, String activity) {
        return activityParamValueComponent.readActivityParams(site, activity);
    }

    public boolean hasActivity(String activity) {
        List<ActivityPO> activityList = activityParamValueComponent.findActivity(activity);
        return activityList.size() > 0;
    }

    public void changeActivityParamValues(String site, List<ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO> activityAndActivityParamAndActivityParamValueVO) throws BusinessException {
        for (ActivityManageVO.ActivityAndActivityParamAndActivityParamValueVO each : activityAndActivityParamAndActivityParamValueVO) {
            switch (each.getViewActionCode()) {
                case 'C':
                    String handle = activityParamValueComponent.createParamValueObject(site, each);
                    logger.debug("handle = " + handle);
                    break;
                case 'U':
                    activityParamValueComponent.updateParamValueObject(site, each);
                    break;
                default:
                    logger.error("ActivityParamValueService.changeActivityParamValues() : Invalid viewActionCode '"
                            + each.getViewActionCode() + "'.");
                    break;
            }
        }
    }
}
