package mhp.oee.web.angular.activity.management;

import java.util.List;
import mhp.oee.vo.ActivityParamVO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.activity.ActivityParamComponent;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class ActivityParamService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private ActivityParamComponent activityParamCompent;

    public List<ActivityParamVO> getActivityParam(String activityBOHandle) {
        return activityParamCompent.getActivityParam(activityBOHandle);
    }

    public void changeActivityParam(List<ActivityParamVO> activityParamList) throws BusinessException {
        for(ActivityParamVO each:activityParamList){
            switch (each.getViewActionCode()) {
            case 'C':
                activityParamCompent.createActivityParam(each);
                break;
            case 'U':
                activityParamCompent.updateActivityParam(each);
                break;
            case 'D':
                activityParamCompent.deleteActivityParam(each);
                break;
            default:
                logger.error("ActivityParamService.changeActivityParam() : Invalid viewActionCode '"
                        + each.getViewActionCode() + "'.");
                break;
            }
        }
    }

    public Boolean idParamUsed(String paramID) {
        return activityParamCompent.idParamUsed(paramID);
    }

}
