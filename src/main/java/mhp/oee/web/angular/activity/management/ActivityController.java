package mhp.oee.web.angular.activity.management;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mhp.oee.vo.ActivityGroupVO;
import mhp.oee.vo.ActivityParamVO;
import mhp.oee.vo.ActivityVO;
import mhp.oee.vo.ExceptionVO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.ActivityBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.web.Constants;

@RestController
public class ActivityController {

    @InjectableLogger
    private static Logger logger;
    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    
    @Autowired
    ActivityService activityService;

    @Autowired
    ActivityParamService activityParamService;

    @Autowired
    ActivityGroupService activityGroupService;

    @RequestMapping(value = "/web/rest/plant/activity/getActivityInputAssist", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityManageVO.ActivityForInputAssitVO>> getActivityInputAssist(
            @RequestParam("activity") String activity) {
        String trace = String.format("[ActivityController.getActivityInputAssist] request : activity=%s ", activity);
        logger.debug(">> " + trace);
        List<ActivityManageVO.ActivityForInputAssitVO> vos = activityService.getActivityForInputAssist(activity);
        logger.debug("<< [ActivityController.getActivityInputAssist]  response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/activity/getActivityBasicInfo", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Map<String, List<Object>>> getActivityBasicInfo(@RequestParam("activity") String activity) {
        String trace = String.format("[ActivityController.getActivityBasicInfo] request : activity=%s ", activity);
        logger.debug(">> " + trace);
        String activityBOHandle = new ActivityBOHandle(activity).getValue();
        List<ActivityManageVO.ActivityForBasicInfoVO> activityVos = activityService.getActivityForBasicInfo(activity);
        List<ActivityParamVO> activityParamVos = activityParamService.getActivityParam(activityBOHandle);
        Map<String, List<Object>> map = new HashMap<String, List<Object>>();
        map.put("basicInfo", (List<Object>) (List<? extends Object>) activityVos);
        map.put("params", (List<Object>) (List<? extends Object>) activityParamVos);
        logger.debug("<< [ActivityController.getActivityBasicInfo]  response : " + gson.toJson(map));
        return ResponseEntity.ok(map);
    }

    @RequestMapping(value = "/web/rest/plant/activity/getAssigendActivityGroup", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityGroupVO>> getAssigendActivityGroup(@RequestParam("activity") String activity) {
        String trace = String.format("[ActivityController.getAssigendActivityGroup] request : activity=%s ", activity);
        logger.debug(">> " + trace);
        List<ActivityGroupVO> vos = activityGroupService.getAssigendActivityGroup(activity);
        logger.debug("<< [ActivityController.getAssigendActivityGroup]  response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/activity/getAvailableActivityGroup", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityGroupVO>> getAvailableActivityGroup(@RequestParam("activity") String activity) {
        String trace = String.format("[ActivityController.getAvailableActivityGroup] request : activity=%s ", activity);
        logger.debug(">> " + trace);
        List<ActivityGroupVO> vos = activityGroupService.getAvailableActivityGroup(activity);
        logger.debug("<< [ActivityController.getAvailableActivityGroup]  response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/activity/change_activity", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeActivity(@RequestBody ActivityVO activityVo){
        String trace = String.format("[ActivityController.changeActivity] request : activityVo=%s ", activityVo);
        logger.debug(">> " + trace);
        try {
            activityService.changeActivity(activityVo);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ActivityController.changeActivity() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ActivityController.changeActivity]");
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/web/rest/plant/activity/change_activityParam", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeActivityParam(@RequestBody List<ActivityParamVO> ActivityParamList) {
        String trace = String.format("[ActivityController.changeActivityParam] request : ActivityParamList=%s ", ActivityParamList);
        logger.debug(">> " + trace);
        try {
            activityParamService.changeActivityParam(ActivityParamList);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ActivityController.changeActivityParam() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ActivityController.changeActivityParam]");
        return ResponseEntity.ok(null);
    }
    @RequestMapping(value = "/web/rest/plant/activity/is_param_used", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> idParamUsed(@RequestParam("paramID") String paramID ) {
        String trace = String.format("[ActivityController.is_param_used] request : paramID=%s ", paramID);
        logger.debug(">> " + trace);
        Boolean flag=activityParamService.idParamUsed(paramID);
        logger.debug("<< [ActivityController.idParamUsed]");
        return ResponseEntity.ok(flag);
    }

    @RequestMapping(value = "/web/rest/plant/activity/move_activityGroup", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> moveActivityGroup(@RequestBody ActivityManageVO.ActivityGroupCombVO activityGroupCombVo) {
        String trace = String.format("[ActivityController.moveActivityGroup] request : activityGroupCombVo=%s ", activityGroupCombVo);
        logger.debug(">> " + trace);
        try {
            activityGroupService.move(activityGroupCombVo);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ActivityController.moveActivityGroup() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ActivityController.moveActivityGroup]");
        return ResponseEntity.ok(null);
    }
}
