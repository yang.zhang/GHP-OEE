package mhp.oee.web.angular.activity.management;

import java.util.List;

import mhp.oee.vo.ActivityGroupVO;
import mhp.oee.vo.ActivityVO;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.CreateErrorException;
import mhp.oee.common.exception.DeleteErrorException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.UpdateErrorException;
import mhp.oee.common.handle.ActivityBOHandle;
import mhp.oee.common.handle.ActivityGroupActivityBOHandle;
import mhp.oee.common.handle.ActivityGroupBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.activity.ActivityGroupActivityComponent;
import mhp.oee.component.activity.ActivityGroupComponent;
import mhp.oee.vo.ActivityComVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class ActivityGroupService {

    @InjectableLogger
    private static Logger logger;
    @Autowired
    private ActivityGroupComponent activityGroupCompent;
    @Autowired
    private ActivityGroupActivityComponent activityGroupActivityCompent;

    public List<ActivityGroupVO> getAssigendActivityGroup(String activity) {
        return activityGroupCompent.getAssigendActivityGroup(activity);
    }

    public List<ActivityGroupVO> getAvailableActivityGroup(String activity) {
        return activityGroupCompent.getAvailableActivityGroup(activity);
    }

    public void move(ActivityManageVO.ActivityGroupCombVO activityGroupCombVo) throws BusinessException {
        List<ActivityGroupVO> avaGrps = activityGroupCombVo.getAvaliableActivityGroupVos();
        List<ActivityGroupVO> assGrps = activityGroupCombVo.getAssigendActivityGroupVos();
        String activityBOHandle = new ActivityBOHandle(activityGroupCombVo.getActivity()).getValue();
        // delete record from activityGroupActivity
        for (ActivityGroupVO vo : avaGrps) {
            String activityGroupBOHandle = new ActivityGroupBOHandle(vo.getActivityGroup()).getValue();
            String activityGroupActivityBOhandle = new ActivityGroupActivityBOHandle(activityGroupBOHandle,activityBOHandle).getValue();
            if (activityGroupActivityCompent.existActivityGroupActivity(activityGroupActivityBOhandle)) {
                activityGroupActivityCompent.deleteActivityGroupActivity(activityGroupActivityBOhandle);
            }
        }
        // insert recode to activityGroupActivity
        for (ActivityGroupVO vo : assGrps) {
            String activityGroupBOHandle = new ActivityGroupBOHandle(vo.getActivityGroup()).getValue();
            String activityGroupActivityBOhandle = new ActivityGroupActivityBOHandle(activityGroupBOHandle,activityBOHandle).getValue();
            if (!activityGroupActivityCompent.existActivityGroupActivity(activityGroupActivityBOhandle)) {
                activityGroupActivityCompent.createActivityGroupActivity(activityGroupBOHandle, activityBOHandle);
            }
        }

    }

    public List<ActivityGroupVO> getActivityGroupForInputAssist(String activityGroup) {
        return activityGroupCompent.getActivityGroupForInputAssist(activityGroup);
    }

    public void changeActivityGroup(ActivityGroupVO activityGroupVO) throws ExistingRecordException, CreateErrorException, UpdateErrorException {
        switch (activityGroupVO.getViewActionCode()) {
        case 'C':
            activityGroupCompent.createActivityGroup(activityGroupVO);
            break;
        case 'U':
            activityGroupCompent.updateActivityGroup(activityGroupVO);
            break;
        default:
            logger.error("ActivityGroupService.changeActivityGroup() : Invalid viewActionCode '"
                    + activityGroupVO.getViewActionCode() + "'.");
            break;
        }

    }

    public List<ActivityVO> getAvaliableActivity(String activityGroup) {
        return activityGroupCompent.getAvaliableActivity(activityGroup);
    }

    public List<ActivityVO> getAssigendActivity(String activityGroup) {
        return activityGroupCompent.getAssigendActivity(activityGroup);
    }

    public void moveActivity(ActivityComVO activityComVo) throws DeleteErrorException, CreateErrorException {
        List<ActivityVO> avaVos = activityComVo.getAvaliableActivityVos();
        List<ActivityVO> assVos = activityComVo.getAssigendActivityVos();

        for (ActivityVO vo : avaVos) {
            String activityBOHandle = new ActivityBOHandle(vo.getActivity()).getValue();
            String activityGroupBOHandle = new ActivityGroupBOHandle(activityComVo.getActivityGroup()).getValue();
            String handle = new ActivityGroupActivityBOHandle(activityGroupBOHandle, activityBOHandle).getValue();
            if (activityGroupActivityCompent.existActivityGroupActivity(handle)) {
                activityGroupActivityCompent.deleteActivityGroupActivity(handle);
            }
        }
        for (ActivityVO vo : assVos) {
            String activityBOHandle = new ActivityBOHandle(vo.getActivity()).getValue();
            String activityGroupBOHandle = new ActivityGroupBOHandle(activityComVo.getActivityGroup()).getValue();
            String handle = new ActivityGroupActivityBOHandle(activityGroupBOHandle, activityBOHandle).getValue();
            if (!activityGroupActivityCompent.existActivityGroupActivity(handle)) {
                activityGroupActivityCompent.createActivityGroupActivity(activityGroupBOHandle,activityBOHandle);
            }
        }
    }

    public ActivityGroupVO getActivityGroupByPrimaryKey(String activityGroup) {
        String handle = new ActivityGroupBOHandle(activityGroup).getValue();
        return activityGroupCompent.getActivityGroupByPrimaryKey(handle);
    }

}
