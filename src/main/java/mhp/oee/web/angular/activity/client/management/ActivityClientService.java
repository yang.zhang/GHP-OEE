package mhp.oee.web.angular.activity.client.management;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.ActivityClientBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.activity.ActivityClientComponent;
import mhp.oee.vo.ActivityClientVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class ActivityClientService {

    @InjectableLogger
    private static Logger logger;
    @Autowired
    ActivityClientComponent activityClientComponent;

    public List<ActivityClientVO> getActivityClientForInputAssist(String activityClient) {
        return activityClientComponent.getActivityClientForInputAssist(activityClient);
    }

    public ActivityClientVO getActivityClientByPrimaryKey(String activityClient) {
        String handle = new ActivityClientBOHandle(activityClient).getValue();
        return activityClientComponent.getActivityClientByPrimaryKey(handle);
    }

    public void changeActivityClient(ActivityClientVO activityClientVO) throws BusinessException {
        switch (activityClientVO.getViewActionCode()) {
        case 'C':
            activityClientComponent.createActivityClient(activityClientVO);
            break;
        case 'U':
            activityClientComponent.updateActivityClient(activityClientVO);
            break;
        default:
            logger.error("ActivityClientService.changeActivityClient() : Invalid viewActionCode '"
                    + activityClientVO.getViewActionCode() + "'.");
            break;
        }
    }

}
