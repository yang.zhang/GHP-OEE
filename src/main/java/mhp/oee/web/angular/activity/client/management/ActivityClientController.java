package mhp.oee.web.angular.activity.client.management;

import java.util.List;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ActivityClientVO;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.web.Constants;

@RestController
public class ActivityClientController {

    @InjectableLogger
    private static Logger logger;
    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ActivityClientService activityClientService;

    // get activityClient for InputAssist
    @RequestMapping(value = "/web/rest/plant/activityClient/getActivityClientForInputAssist", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityClientVO>> getActivityClientForInputAssist(
            @RequestParam("activityClient") String activityClient) {
        String trace = String.format(
                "[ActivityClientController.getActivityClientForInputAssist] request : activityClient=%s ", activityClient);
        logger.debug(">> " + trace);
        List<ActivityClientVO> vos = activityClientService.getActivityClientForInputAssist(activityClient);
        logger.debug("<< [ActivityClientController.getActivityClientForInputAssist]  response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    // get activityClient by primarykey
    @RequestMapping(value = "/web/rest/plant/activityClient/getActivityClientByPrimaryKey", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<ActivityClientVO> getActivityClientById(
            @RequestParam("activityClient") String activityClient) {
        String trace = String.format(
                "[ActivityClientController.getActivityClientById] request : activityClient=%s ", activityClient);
        logger.debug(">> " + trace);
        ActivityClientVO vo = activityClientService.getActivityClientByPrimaryKey(activityClient);
        logger.debug("<< [ActivityClientController.getActivityClientByPrimaryKey]  response : " + gson.toJson(vo));
        return ResponseEntity.ok(vo);
    }

    // change activityClient
    @RequestMapping(value = "/web/rest/plant/activityClient/change_activityClient", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeActivityClient(@RequestBody ActivityClientVO activityClientVO) {
        String trace = String.format(
                "[ActivityClientController.getActivityClientById] request : activityClient=%s ", gson.toJson(activityClientVO));
        logger.debug(">> " + trace);
        try {
            activityClientService.changeActivityClient(activityClientVO);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ActivityClientController.changeActivityClient() : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ActivityClientController.getActivityClientByPrimaryKey]");
        return ResponseEntity.ok(null);
    }
}
