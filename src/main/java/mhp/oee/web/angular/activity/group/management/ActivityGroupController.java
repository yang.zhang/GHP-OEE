package mhp.oee.web.angular.activity.group.management;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ActivityComVO;
import mhp.oee.vo.ActivityGroupVO;
import mhp.oee.vo.ActivityVO;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.web.Constants;
import mhp.oee.web.angular.activity.management.ActivityGroupService;

@RestController
public class ActivityGroupController {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ActivityGroupService activityGroupService;

    @RequestMapping(value = "/web/rest/plant/activityGroup/getActivityGroupInputAssist", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityGroupVO>> getActivityGroupForInputAssist(
            @RequestParam("activityGroup") String activityGroup) {
        String trace = String.format(
                "[ActivityGroupController.getActivityGroupForInputAssist] request : activityGroup=%s ", activityGroup);
        logger.debug(">> " + trace);
        List<ActivityGroupVO> vos = activityGroupService.getActivityGroupForInputAssist(activityGroup);
        logger.debug("<< [ActivityGroupController.getActivityGroupForInputAssist]  response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/activityGroup/getActivityGroupByPrimaryKey", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Object> getActivityGroupByPrimaryKey(@RequestParam("activityGroup") String activityGroup) {
        String trace = String.format(
                "[ActivityGroupController.getActivityGroupByPrimaryKey] request : activityGroup=%s ", activityGroup);
        logger.debug(">> " + trace);
        ActivityGroupVO vo = activityGroupService.getActivityGroupByPrimaryKey(activityGroup);
        logger.debug("<< [ActivityGroupController.getActivityGroupByPrimaryKey]  response : " + gson.toJson(vo));
        return ResponseEntity.ok(vo == null ? "null" : vo);
    }

    // get avaliabled Activity
    @RequestMapping(value = "/web/rest/plant/activityGroup/getAvaliableActivity", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityVO>> getAvaliableActivity(@RequestParam("activityGroup") String activityGroup) {
        String trace = String.format("[ActivityGroupController.getAvaliableActivity] request : activityGroup=%s ",
                activityGroup);
        logger.debug(">> " + trace);
        List<ActivityVO> vos = activityGroupService.getAvaliableActivity(activityGroup);
        logger.debug("<< [ActivityGroupController.getAvaliableActivity]  response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    // get assigend Activity
    @RequestMapping(value = "/web/rest/plant/activityGroup/getAssigendActivity", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ActivityVO>> getAssigendActivity(@RequestParam("activityGroup") String activityGroup) {
        String trace = String.format("[ActivityGroupController.getAssigendActivity] request : activityGroup=%s ",
                activityGroup);
        logger.debug(">> " + trace);
        List<ActivityVO> vos = activityGroupService.getAssigendActivity(activityGroup);
        logger.debug("<< [ActivityGroupController.getAssigendActivity]  response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    // change activityGroup
    @RequestMapping(value = "/web/rest/plant/activityGroup/changeActivityGroup", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeActivityGroup(@RequestBody ActivityGroupVO activityGroupVO) {
        String trace = String.format("[ActivityGroupController.changeActivityGroup] request : activityGroupVO=%s ",gson.toJson(activityGroupVO));
        logger.debug(">> " + trace);
        try {
            activityGroupService.changeActivityGroup(activityGroupVO);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ActivityGroupController.changeActivityGroup() : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ActivityGroupController.changeActivityGroup]");
        return ResponseEntity.ok(null);
    }

    // move activity from activityGroup
    @RequestMapping(value = "/web/rest/plant/activityGroup/moveActivity", method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> moveActivity(@RequestBody ActivityComVO activityComVO) {
        String trace = String.format("[ActivityGroupController.moveActivity] request : activityGroupVO=%s ",
                gson.toJson(activityComVO));
        logger.debug(">> " + trace);
        try {
            activityGroupService.moveActivity(activityComVO);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ActivityGroupController.moveActivity() : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ActivityGroupController.moveActivity]");
        return ResponseEntity.ok(null);
    }
}
