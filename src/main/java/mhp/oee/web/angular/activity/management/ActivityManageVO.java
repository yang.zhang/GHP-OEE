package mhp.oee.web.angular.activity.management;

import mhp.oee.vo.ActivityGroupVO;

import java.util.Date;
import java.util.List;

/**
 * Created by LinZuK on 2016/8/1.
 */
public class ActivityManageVO {

    public static class ActivityAndActivityParamAndActivityParamValueVO {

        private String activityParamValueHandle;
        private String activityBo;
        private String activityDesc;
        private String paramId;
        private String paramDescription;
        private String paramDefaultValue;
        private String paramValue;
        private Date activityParamValueModifiedDateTime;

        private Character viewActionCode;

        public String getActivityParamValueHandle() {
            return activityParamValueHandle;
        }

        public void setActivityParamValueHandle(String activityParamValueHandle) {
            this.activityParamValueHandle = activityParamValueHandle;
        }

        public String getActivityBo() {
            return activityBo;
        }

        public void setActivityBo(String activityBo) {
            this.activityBo = activityBo;
        }

        public String getActivityDesc() {
            return activityDesc;
        }

        public void setActivityDesc(String activityDesc) {
            this.activityDesc = activityDesc;
        }

        public String getParamId() {
            return paramId;
        }

        public void setParamId(String paramId) {
            this.paramId = paramId;
        }

        public String getParamDescription() {
            return paramDescription;
        }

        public void setParamDescription(String paramDescription) {
            this.paramDescription = paramDescription;
        }

        public String getParamDefaultValue() {
            return paramDefaultValue;
        }

        public void setParamDefaultValue(String paramDefaultValue) {
            this.paramDefaultValue = paramDefaultValue;
        }

        public String getParamValue() {
            return paramValue;
        }

        public void setParamValue(String paramValue) {
            this.paramValue = paramValue;
        }

        public Date getActivityParamValueModifiedDateTime() {
            return activityParamValueModifiedDateTime;
        }

        public void setActivityParamValueModifiedDateTime(Date activityParamValueModifiedDateTime) {
            this.activityParamValueModifiedDateTime = activityParamValueModifiedDateTime;
        }

        public Character getViewActionCode() {
            return viewActionCode;
        }

        public void setViewActionCode(Character viewActionCode) {
            this.viewActionCode = viewActionCode;
        }
    }


    public static class ActivityGroupCombVO {
        private String activity;
        private List<ActivityGroupVO> avaliableActivityGroupVos;
        private List<ActivityGroupVO> assigendActivityGroupVos;

        public String getActivity() {
            return activity;
        }

        public void setActivity(String activity) {
            this.activity = activity;
        }

        public List<ActivityGroupVO> getAvaliableActivityGroupVos() {
            return avaliableActivityGroupVos;
        }

        public void setAvaliableActivityGroupVos(List<ActivityGroupVO> avaliableActivityGroupVos) {
            this.avaliableActivityGroupVos = avaliableActivityGroupVos;
        }

        public List<ActivityGroupVO> getAssigendActivityGroupVos() {
            return assigendActivityGroupVos;
        }

        public void setAssigendActivityGroupVos(List<ActivityGroupVO> assigendActivityGroupVos) {
            this.assigendActivityGroupVos = assigendActivityGroupVos;
        }

    }

    public static class ActivityForInputAssitVO {
        private String activity;
        private String description;
        private String executionType;

        public String getActivity() {
            return activity;
        }

        public void setActivity(String activity) {
            this.activity = activity;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getExecutionType() {
            return executionType;
        }

        public void setExecutionType(String executionType) {
            this.executionType = executionType;
        }

    }


    public static class ActivityConditionVO {

        private String activity;
        private String description;
        private String executionType;

        public String getActivity() {
            return activity;
        }

        public void setActivity(String activity) {
            this.activity = activity;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getExecutionType() {
            return executionType;
        }

        public void setExecutionType(String executionType) {
            this.executionType = executionType;
        }


    }


    public static class ActivityForBasicInfoVO {

        private String description;
        private String enabled;
        private String visible;
        private Integer sequenceId;
        private String executionType;
        private String classOrProgram;
        private Date modifiedDateTime;
        private Character viewActionCode;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getEnabled() {
            return enabled;
        }

        public void setEnabled(String enabled) {
            this.enabled = enabled;
        }

        public String getVisible() {
            return visible;
        }

        public void setVisible(String visible) {
            this.visible = visible;
        }

        public Integer getSequenceId() {
            return sequenceId;
        }

        public void setSequenceId(Integer sequenceId) {
            this.sequenceId = sequenceId;
        }

        public String getExecutionType() {
            return executionType;
        }

        public void setExecutionType(String executionType) {
            this.executionType = executionType;
        }

        public String getClassOrProgram() {
            return classOrProgram;
        }

        public void setClassOrProgram(String classOrProgram) {
            this.classOrProgram = classOrProgram;
        }

        public Date getModifiedDateTime() {
            return modifiedDateTime;
        }

        public void setModifiedDateTime(Date modifiedDateTime) {
            this.modifiedDateTime = modifiedDateTime;
        }

        public Character getViewActionCode() {
            return viewActionCode;
        }

        public void setViewActionCode(Character viewActionCode) {
            this.viewActionCode = viewActionCode;
        }


    }



}
