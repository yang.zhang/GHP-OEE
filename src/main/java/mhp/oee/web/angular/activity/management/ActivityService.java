package mhp.oee.web.angular.activity.management;

import java.util.List;

import mhp.oee.vo.ActivityVO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.activity.ActivityComponent;
import mhp.oee.component.activity.ActivityParamComponent;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class ActivityService {

    @InjectableLogger
    private static Logger logger;
    @Autowired
    ActivityComponent activityComponent;
    @Autowired
    ActivityParamComponent activityParamComponent;

    public List<ActivityManageVO.ActivityForInputAssitVO> getActivityForInputAssist(String activity) {
        List<ActivityManageVO.ActivityForInputAssitVO> list = activityComponent.getActivityForInputAssist(activity);
        return list;
    }

    public List<ActivityManageVO.ActivityForBasicInfoVO> getActivityForBasicInfo(String activity) {
        return activityComponent.getActivityForBasicInfo(activity);
    }
    public void changeActivity(ActivityVO activityVo) throws BusinessException {
        switch (activityVo.getViewActionCode()) {
            case 'C':
                activityComponent.createActivity(activityVo);
                break;
            case 'U':
                activityComponent.updateActivity(activityVo);
                break;
            default:
                logger.error("ActivityService.changeActivity() : Invalid viewActionCode '"
                        + activityVo.getViewActionCode() + "'.");
                break;
        }
    }

    public List<ActivityManageVO.ActivityConditionVO> getActivitiesLike(String activity) {
        return activityComponent.getActivitiesLike(activity);
    }

//    public List<ActivityParamPO> recognitionPpatternDetail(String activity, String type) {
//        String handle = new ActivityBOHandle(activity).getValue();
//        ActivityPO po = activityComponent.selectActivityByPrimaryKey(handle);
//        List<ActivityParamPO> pos = new LinkedList<ActivityParamPO>();
//        if (po != null && "true".equals(po.getEnabled()) && type.equals(po.getExecutionType())) {
//            pos = activityParamComponent.getActivityParamByActivityBO(po.getHandle());
//        }
//        return pos;
//    }

}
