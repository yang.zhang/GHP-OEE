package mhp.oee.web.angular.planned.downtime.maintenance;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.ExistingRepeatTimeException;
import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ResourcePlcInfoComponent;
import mhp.oee.component.plant.ScheduledDownPlanComponent;
import mhp.oee.po.extend.ScheduledDownPlanAndResrceAndWorkCenterPO;
import mhp.oee.po.gen.ScheduledDownPlanPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ReasonCodeVO;
import mhp.oee.vo.ResourceConditionVO;
import mhp.oee.vo.ScheduledDownPlanAndResrceAndWorkCenterVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class ScheduledDownPlanService {

    @InjectableLogger
    private static Logger logger;
    private static String ENABLED_Y = "true";
    SimpleDateFormat simpleDateFormatForDate = new SimpleDateFormat("MM/dd/yyyy-HH/mm/ss");
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ScheduledDownPlanComponent scheduledDownPlanComponent;
    
    @Autowired
    private ResourcePlcInfoComponent resourcePlcInfoComponent;

    public List<ReasonCodeVO> readAllResonCode(String site){
        List<ReasonCodeVO> vos = scheduledDownPlanComponent.readAllReasonCode(site, ENABLED_Y);
        return vos;
    }

    public List<ScheduledDownPlanAndResrceAndWorkCenterVO> readScheduledDownPlanByResrce(String site, String resrce, String dateTime,String workCenter, String line, String resourceType, int limit, int offset) throws ParseException {
        List<ScheduledDownPlanAndResrceAndWorkCenterVO> vos = new ArrayList<ScheduledDownPlanAndResrceAndWorkCenterVO>();
        // 1 GET RESRCE LIST
        List<ResourceConditionVO> resourceConditionVOs = new ArrayList<ResourceConditionVO>();
        if (Utils.isEmpty(resrce)) {
            String resourceTypeBO = null;
            if (!Utils.isEmpty(resourceType)) {
                resourceTypeBO = new ResourceTypeBOHandle(site, resourceType).getValue();
            }
            if(line != null && line.length() > 0) {
                String[] lineArray = line.split(",");
                resourceConditionVOs = Utils.copyListProperties(resourcePlcInfoComponent.readResourceWithMultipleLine(site, resourceTypeBO, lineArray, workCenter), ResourceConditionVO.class);
            } else {
                resourceConditionVOs = Utils.copyListProperties(resourcePlcInfoComponent.readResource(site, resourceTypeBO, line, workCenter),ResourceConditionVO.class);
            }
        } else {
            String[] resrceArray = resrce.split(",");
            for(int i=0;i<resrceArray.length;i++){
                ResourceConditionVO vo = new ResourceConditionVO();
                vo.setResrce(resrceArray[i]);
                resourceConditionVOs.add(vo);
            }
        }
        if (resourceConditionVOs.size() <= 0) {
            return vos;
        }
        Date time = Utils.getDatefromString(dateTime);
        List<ScheduledDownPlanAndResrceAndWorkCenterPO> pos = scheduledDownPlanComponent.readScheduledDownPlanByResrce(site, resourceConditionVOs, time, limit, offset);
        vos = Utils.copyListProperties(pos, ScheduledDownPlanAndResrceAndWorkCenterVO.class);
        for (ScheduledDownPlanAndResrceAndWorkCenterVO vo : vos) {
            vo.setStartDateTimeIn(simpleDateFormatForDate.format(vo.getStartDateTime()));
            vo.setEndDateTimeIn(simpleDateFormatForDate.format(vo.getEndDateTime()));
        }
        return vos;
    }

    public int readScheduledDownPlanByResrceCount(String site, String resrce, String dateTime,String workCenter, String line, String resourceType) throws ParseException {
        int vos = 0;
        // 1 GET RESRCE LIST
        List<ResourceConditionVO> resourceConditionVOs = new ArrayList<ResourceConditionVO>();
        if (Utils.isEmpty(resrce)) {
            String resourceTypeBO = null;
            if (!Utils.isEmpty(resourceType)) {
                resourceTypeBO = new ResourceTypeBOHandle(site, resourceType).getValue();
            }
            if(line != null && line.length() > 0) {
                String[] lineArray = line.split(",");
                resourceConditionVOs = Utils.copyListProperties(resourcePlcInfoComponent.readResourceWithMultipleLine(site, resourceTypeBO, lineArray, workCenter), ResourceConditionVO.class);
            } else {
                resourceConditionVOs = Utils.copyListProperties(resourcePlcInfoComponent.readResource(site, resourceTypeBO, line, workCenter),ResourceConditionVO.class);
            }
            //resourceConditionVOs = scheduledDownPlanComponent.readResourceByCondition(site, resourceTypeBO, line, workCenter);
        } else {
            ResourceConditionVO vo = new ResourceConditionVO();
            vo.setResrce(resrce);
            resourceConditionVOs.add(vo);
        }
        if (resourceConditionVOs.size() <= 0) {
            return vos;
        }
        Date time = Utils.getDatefromString(dateTime);
        vos = scheduledDownPlanComponent.readScheduledDownPlanByResrceCount(site, resourceConditionVOs, time);
        return vos;
    }

    public boolean existRepeatTime(String site, String resrce, String startDateTime, String endDateTime, String handle) throws ParseException {
        ResrceBOHandle boHandle = new ResrceBOHandle(site, resrce);
        Date startDt = simpleDateFormatForDate.parse(startDateTime);
        Date endDt = simpleDateFormatForDate.parse(endDateTime);
        List<ScheduledDownPlanPO> scheduledDownPlanPOs = scheduledDownPlanComponent.existRepeatTime(site, boHandle.getValue(), handle);
        boolean result = false;
        for (ScheduledDownPlanPO po : scheduledDownPlanPOs) {
            if (startDt.compareTo(po.getStartDateTime()) < 0 && endDt.compareTo(po.getStartDateTime()) > 0) {
                result = true;
                break;
            } else if (startDt.compareTo(po.getStartDateTime()) >= 0 && startDt.compareTo(po.getEndDateTime()) < 0) {
                result = true;
                break;
            }
        }
        return result;
    }

    public void changeScheduledDownPlanByResrce(List<ScheduledDownPlanAndResrceAndWorkCenterVO> scheduledDownPlanAndResrceAndWorkCenterVO)
            throws BusinessException, ParseException {
        Collections.sort(scheduledDownPlanAndResrceAndWorkCenterVO);
        for (ScheduledDownPlanAndResrceAndWorkCenterVO each : scheduledDownPlanAndResrceAndWorkCenterVO) {
            setStartAndEndTime(each);
            switch (each.getViewActionCode()) {
            case 'C':
                boolean checkCreate = existRepeatTime(each.getSite(), each.getResrce(), each.getStartDateTimeIn(), each.getEndDateTimeIn(), each.getHandle());
                if (checkCreate) {
                    logger.error("ScheduledDownPlanService.changeScheduledDownPlanByResrce() : existRepeatTime");
                    throw new ExistingRepeatTimeException(gson.toJson(each));
                }
                String strt = each.getStartDateTimeIn();
                each.setStrt(Utils.getStrt(strt));
                each.setStartDateTime(Utils.getDateFromWeb(strt));
                each.setEndDateTime(Utils.getDateFromWeb(each.getEndDateTimeIn()));
                scheduledDownPlanComponent.createScheduledDownPlanByResrce(each);
                break;
            case 'U':
                boolean checkUpdate = existRepeatTime(each.getSite(), each.getResrce(), each.getStartDateTimeIn(), each.getEndDateTimeIn(), each.getHandle());
                if (checkUpdate) {
                    logger.error("ScheduledDownPlanService.changeScheduledDownPlanByResrce() : existRepeatTime");
                    throw new ExistingRepeatTimeException(gson.toJson(each));
                }
                scheduledDownPlanComponent.updateScheduledDownPlanByResrce(each);
                break;
            case 'D':
                scheduledDownPlanComponent.deleteScheduledDownPlanByResrce(each);
                break;
            default:
                logger.error("ProductionOutputTargetService.cudchangeProductionOutputTarget() : Invalid viewActionCode '" + each.getViewActionCode() + "'.");
                break;
            }
        }
    }

    private void setStartAndEndTime(ScheduledDownPlanAndResrceAndWorkCenterVO vo) throws ParseException {
        vo.setStartDateTime(simpleDateFormatForDate.parse(vo.getStartDateTimeIn()));
        vo.setEndDateTime(simpleDateFormatForDate.parse(vo.getEndDateTimeIn()));
    }
}
