package mhp.oee.web.angular.planned.downtime.maintenance;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ReasonCodeVO;
import mhp.oee.vo.ScheduledDownPlanAndResrceAndWorkCenterVO;
import mhp.oee.web.Constants;

@RestController
public class ScheduledDownPlanController {

    @InjectableLogger
    private static Logger logger;
    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ScheduledDownPlanService scheduledDownPlanService;
    
    @RequestMapping(value = "/web/rest/plant/site/{site}/reason_code",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ReasonCodeVO>> readAllResonCode(@PathVariable("site") String site) {
        String trace = String.format("[ScheduledDownPlanController.readAllResonCode] request : site=%s", site);
        logger.debug(">> " + trace);
        List<ReasonCodeVO> vos = scheduledDownPlanService.readAllResonCode(site);
        logger.debug("<< [ScheduledDownPlanController.readAllResonCode] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/site/{site}/scheduled_down_plan",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ScheduledDownPlanAndResrceAndWorkCenterVO>> readScheduledDownPlanByResrce(
            @PathVariable("site") String site,
            @RequestParam(value = "resrce", required = false) String resrce,
            @RequestParam(value = "time", required = true) String time,
            @RequestParam(value = "work_center", required = true) String workCenter,
            @RequestParam(value = "line", required = true) String line,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "index", required = true) int index,
            @RequestParam(value = "count", required = true) int count) {
        String trace = String.format("[ScheduledDownPlanController.readScheduledDownPlanByResrce] request : site=%s, resrce=%s, time=%s, workCenter=%s, line=%s, resourceType=%s, index=%s, count=%s", 
                site, resrce, time, workCenter, line, resourceType, index, count);
        logger.debug(">> " + trace);
        List<ScheduledDownPlanAndResrceAndWorkCenterVO> vos = null;
        try {
            vos = scheduledDownPlanService.readScheduledDownPlanByResrce(site, resrce, time, workCenter, line, resourceType, count, Utils.getOffset(count, index));
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ScheduledDownPlanController.readScheduledDownPlanByResrce() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ScheduledDownPlanController.readScheduledDownPlanByResrce] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/site/{site}/scheduled_down_plan_count",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Integer> readScheduledDownPlanByResrceCount(
            @PathVariable("site") String site,
            @RequestParam(value = "resrce", required = false) String resrce,
            @RequestParam(value = "time", required = true) String time,
            @RequestParam(value = "work_center", required = true) String workCenter,
            @RequestParam(value = "line", required = true) String line,
            @RequestParam(value = "resource_type", required = false) String resourceType) {
        String trace = String.format("[ScheduledDownPlanController.readScheduledDownPlanByResrceCount] request : site=%s, resrce=%s, time=%s, workCenter=%s, line=%s, resourceType=%s", 
                site, resrce, time, workCenter, line, resourceType);
        logger.debug(">> " + trace);
        int vos = 0;
        try {
            vos = scheduledDownPlanService.readScheduledDownPlanByResrceCount(site, resrce, time, workCenter, line, resourceType);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ScheduledDownPlanController.readScheduledDownPlanByResrce() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ScheduledDownPlanController.readScheduledDownPlanByResrceCount] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/site/{site}/exist_repeat_time",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> existRepeatTime(
            @PathVariable("site") String site,
            @RequestParam(value = "resrce", required = true) String resrce,
            @RequestParam(value = "start_date_time", required = true) String startDateTime,
            @RequestParam(value = "handle", required = true) String handle,
            @RequestParam(value = "end_date_time", required = true) String endDateTime
            ) {
        String trace = String.format("[ScheduledDownPlanController.existRepeatTime] request : site=%s, resrce=%s, startDateTime=%s, endDateTime=%s, handle=%s", 
                site, resrce, startDateTime, endDateTime, handle);
        logger.debug(">> " + trace);
        boolean result = false ;
        try {
            result = scheduledDownPlanService.existRepeatTime(site, resrce, startDateTime, endDateTime, handle);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ScheduledDownPlanController.existRepeatTime() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ScheduledDownPlanController.existRepeatTime] response : "  + gson.toJson(result));
        return ResponseEntity.ok(result);
    }
    
    @RequestMapping(value = "/web/rest/plant/scheduled_down_plan", 
            method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeScheduledDownPlanByResrce(@RequestBody List<ScheduledDownPlanAndResrceAndWorkCenterVO> scheduledDownPlanAndResrceAndWorkCenterVO) {
        String trace = String.format("[ScheduledDownPlanController.changeScheduledDownPlanByResrce] request : " + gson.toJson(scheduledDownPlanAndResrceAndWorkCenterVO));
        logger.debug(">> " + trace);
        try {
            scheduledDownPlanService.changeScheduledDownPlanByResrce(scheduledDownPlanAndResrceAndWorkCenterVO);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ScheduledDownPlanController.changeScheduledDownPlanByResrce() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ScheduledDownPlanController.changeScheduledDownPlanByResrce() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ScheduledDownPlanController.changeScheduledDownPlanByResrce] response : void");
        return ResponseEntity.ok(null);
    }
    
}
