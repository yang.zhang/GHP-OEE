package mhp.oee.web.angular.plc.accesstype.management;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.PlcAccessTypeVO;
import mhp.oee.web.Constants;

@RestController
public class RestAccessTypeController {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    PlantAccessTypeService plantAccessTypeSerive;

    @RequestMapping(value = "/web/rest/plant/site/{site}/plc_access_type", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<PlcAccessTypeVO>> readAllPlcAccessType(@PathVariable("site") String site) {
        List<PlcAccessTypeVO> vos = plantAccessTypeSerive.readAllPlcAccessType(site);
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/is_plc_access_type", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> isPlcAccessTypeUsed(
            @PathVariable("site") String site,
            @RequestParam(value = "plc_access_type", required = true) String PlcAccessType) {
        boolean vos = plantAccessTypeSerive.isPlcAccessTypeUsed(site, PlcAccessType);
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/exist_plc_access_type",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> existPlcObject(
            @PathVariable("site") String site,
            @RequestParam(value = "plc_access_type", required = true) String PlcAccessType) {
        boolean vos = plantAccessTypeSerive.existPlcAccessType(site, PlcAccessType);
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/change_plc_access_type", method = RequestMethod.POST)
    public ResponseEntity<?> changePlcCategories(@RequestBody List<PlcAccessTypeVO> vos, @PathVariable("site") String site) {
        try {
            this.plantAccessTypeSerive.changePlcAccess(site, vos);
        } catch (BusinessException e) {
            logger.error("RestPlantController.changePlcCategories() : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        return ResponseEntity.ok().body(null);
    }

}
