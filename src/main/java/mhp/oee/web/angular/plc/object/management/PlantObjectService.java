package mhp.oee.web.angular.plc.object.management;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.PlcObjectComponent;
import mhp.oee.po.gen.PlcObjectPO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.PlcObjectAndPlcCategoryVO;
import mhp.oee.vo.PlcObjectVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class PlantObjectService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    PlcObjectComponent plcObjectComponent;

    public List<PlcObjectAndPlcCategoryVO> readAllPlcObjectPage(String site, String plcCategory, int index, int count) {
        List<PlcObjectAndPlcCategoryVO> poPOs = plcObjectComponent.readAllPlcObjectPage(site, plcCategory, count, Utils.getOffset(count, index));
        return poPOs;
    }
    
    public List<PlcObjectVO> readAllPlcObject(String site, String plcCategory) {
        List<PlcObjectVO> pos = plcObjectComponent.readAllPlcObject(site, plcCategory);
        return pos;
    }
    
    public int readAllPlcObjectCount(String site, String plcCategory) {
        int poPOs = plcObjectComponent.readAllPlcObjectCount(site, plcCategory);
        return poPOs;
    }

    public boolean isPlcObjectUsed(String site, String plcObject) {
        return plcObjectComponent.isPlcObjectUsed(site, plcObject);
    }

    public boolean existPlcObject(String site, String plcObject) {
        PlcObjectPO po = plcObjectComponent.existPlcObject(site, plcObject);
        return (po != null) ? true : false;
    }

    public void changePlcObjects(List<PlcObjectVO> plcObjectVOs) throws BusinessException {
        for (PlcObjectVO each : plcObjectVOs) {
            switch (each.getViewActionCode()) {
            case 'C':
                plcObjectComponent.createPlcObject(each);
                break;
            case 'U':
                plcObjectComponent.updatePlcObject(each);
                break;
            case 'D':
                plcObjectComponent.deletePlcObject(each);
                break;
            default:
                logger.error("PlantObjectService.PlantObjectService() : Invalid viewActionCode '"
                        + each.getViewActionCode() + "'.");
                break;
            }
        }
    }
}
