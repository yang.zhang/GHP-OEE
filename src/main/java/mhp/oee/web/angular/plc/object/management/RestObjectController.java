package mhp.oee.web.angular.plc.object.management;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.PlcObjectAndPlcCategoryVO;
import mhp.oee.vo.PlcObjectVO;
import mhp.oee.web.Constants;

@RestController
public class RestObjectController {

    @InjectableLogger
    private static Logger logger;
    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    PlantObjectService plcObjectService;

    @RequestMapping(value = "/web/rest/plant/site/{site}/plc_object",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<PlcObjectAndPlcCategoryVO>> readAllPlcObjects(
            @PathVariable("site") String site, 
            @RequestParam(value = "plc_category", required = true) String plcCategory,
            @RequestParam(value = "index", required = true) int index,
            @RequestParam(value = "count", required = true) int count) {
        String trace = String.format("[RestObjectController.readAllPlcObjects] request : site=%s, plcCategory=%s, index=%s, count=%s",
                site, plcCategory, index, count);
        logger.debug(">> " + trace);
        List<PlcObjectAndPlcCategoryVO> vos = plcObjectService.readAllPlcObjectPage(site, plcCategory, index, count);
        logger.debug("<< [RestObjectController.readAllPlcObjects] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/site/{site}/plc_object_all",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<PlcObjectVO>> readAllPlcObjects(
            @PathVariable("site") String site, 
            @RequestParam(value = "plc_category", required = false) String plcCategory) {
        String trace = String.format("[RestObjectController.readAllPlcObjects] request : site=%s, plcCategory=%s",
                site, plcCategory);
        logger.debug(">> " + trace);
        List<PlcObjectVO> vos = plcObjectService.readAllPlcObject(site, plcCategory);
        logger.debug("<< [RestObjectController.readAllPlcObjects] response : "  +  gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/site/{site}/plc_object_count",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Integer> readAllPlcObjectsCount(
            @PathVariable("site") String site, 
            @RequestParam(value = "plc_category", required = true) String plcCategory) {
        String trace = String.format("[RestObjectController.readAllPlcObjectsCount] request : site=%s, plcCategory=%s",
                site, plcCategory);
        logger.debug(">> " + trace);
        int vos = plcObjectService.readAllPlcObjectCount(site, plcCategory);
        logger.debug("<< [RestObjectController.readAllPlcObjectsCount] response :" + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/is_plc_object_used",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> isPlcObjectUsed(
            @PathVariable("site") String site, 
            @RequestParam(value = "plc_object", required = true) String plcObject) {
        String trace = String.format("[RestObjectController.isPlcObjectUsed] request : site=%s, plcObject=%s",
                site, plcObject);
        logger.debug(">> " + trace);
        boolean vos = plcObjectService.isPlcObjectUsed(site, plcObject);
        logger.debug("<< [RestObjectController.isPlcObjectUsed] response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/exist_plc_object_obj_controller",
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> existPlcObject(
            @PathVariable("site") String site, 
            @RequestParam(value = "plc_object", required = true) String plcObject) {
        String trace = String.format("[RestObjectController.existPlcObject] request : site=%s, plcObject=%s",
                site, plcObject);
        logger.debug(">> " + trace);
        boolean vos = plcObjectService.existPlcObject(site, plcObject);
        logger.debug("<< [RestObjectController.existPlcObject] response : " + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    } 

    @RequestMapping(value = "/web/rest/plant/change_plc_object",
            method = RequestMethod.POST)
    public ResponseEntity<?> changePlcObject(@RequestBody List<PlcObjectVO> plcObjectVOs) {
        String trace = String.format("[RestObjectController.changePlcObject] request : " + gson.toJson(plcObjectVOs));
        logger.debug(">> " + trace);
        try {
            plcObjectService.changePlcObjects(plcObjectVOs);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("RestObjectController.changePlcObject() : " + e.getMessage() + System.lineSeparator()
                    + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [RestObjectController.changePlcObject] response : void");
        return ResponseEntity.ok().body(null);
    }

}
