package mhp.oee.web.angular.plc.accesstype.management;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.DataReferingException;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.MandatoryException;
import mhp.oee.common.exception.NotFoundExistingRecordException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.PlcAccessTypeComponent;
import mhp.oee.component.plant.ResourceComponent;
import mhp.oee.component.plant.ResourcePlcComponent;
import mhp.oee.po.gen.PlcAccessTypePO;
import mhp.oee.po.gen.ResourcePlcInfoPO;
import mhp.oee.vo.PlcAccessTypeVO;

@Service
@Transactional(rollbackFor = { BusinessException.class, IOException.class })
public class PlantAccessTypeService {

    @Autowired
    PlcAccessTypeComponent accessTypeComponent;

    @Autowired
    ResourcePlcComponent resourcePlcComponent;

    @Autowired
    ResourceComponent resourceComponent;

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();


    public boolean isPlcAccessTypeUsed(String site, String plcAccessType) {
        List<ResourcePlcInfoPO> pos = accessTypeComponent.getResourcePlcInfoListByAccessType(site, plcAccessType);
        return (pos != null && pos.size() > 0) ? true : false;
    }

    public boolean existPlcAccessType(String site, String plcObject) {
        PlcAccessTypePO po = accessTypeComponent.existPlcObject(site, plcObject);
        return (po != null) ? true : false;
    }

    public List<PlcAccessTypeVO> readAllPlcAccessType(String site) {
        return this.accessTypeComponent.readAllPlcAccessType(site);
    }

    public void changePlcAccess(String site, List<PlcAccessTypeVO> plcAccessVOs) throws MandatoryException, ExistingRecordException, NotFoundExistingRecordException, DataReferingException  {
        for (PlcAccessTypeVO each : plcAccessVOs) {
            String plcAccessType = each.getPlcAccessType();
            String description = each.getDescription();
            if (plcAccessType.trim().length() == 0 || description.trim().length() == 0) {
                throw new MandatoryException();
            }
            if (each.getViewActionCode().equals('C')) {
                this.accessTypeComponent.createAccessType(each);
            } else if (each.getViewActionCode().equals('U')) {

                this.accessTypeComponent.updateAccessType(each);
            } else if (each.getViewActionCode().equals('D')) {
                this.accessTypeComponent.deleteAccessType(site, each);
            } else {
                logger.error("PlantService.cudPlcCategoryChanges() : Invalid viewActionCode '"
                        + each.getViewActionCode() + "'.");
            }
        }
    }

    public PlcAccessTypeVO getAccessTypeByCode(PlcAccessTypeVO vo) {
        return accessTypeComponent.getAccessTypeByCode(vo);
    }
    public List<ResourcePlcInfoPO> getResourcePlcInfoByPcIPAndPlcAccessTypeNull(String pcIp) {
        return accessTypeComponent.getResourcePlcInfoByPcIPAndPlcAccessTypeNull(pcIp);
    }

}
