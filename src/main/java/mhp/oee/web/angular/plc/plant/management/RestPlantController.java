package mhp.oee.web.angular.plc.plant.management;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.PlcCategoryVO;
import mhp.oee.web.Constants;

@RestController
public class RestPlantController {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    PlantService plantSerive;

    @RequestMapping(value = "/web/rest/plant/site/{site}/plc_category",
                    method = RequestMethod.GET,
                    produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<PlcCategoryVO>> readAllPlcCategories(@PathVariable("site") String site) {
        List<PlcCategoryVO> vos = plantSerive.readAllPlcCategories(site);
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/exist_plc_category/{plc_category}",
                    method = RequestMethod.GET,
                    produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> existPlcCategory(@PathVariable("site") String site, @PathVariable("plc_category") String plcCategory) {
        boolean ret = plantSerive.existPlcCategory(site, plcCategory);
        return ResponseEntity.ok(ret);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/is_plc_category_used/{plc_category}",
                    method = RequestMethod.GET,
                    produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> isPlcCategoryUsed(@PathVariable("site") String site, @PathVariable("plc_category") String plcCategory) {
        boolean ret = plantSerive.isPlcCategoryUsed(site, plcCategory);
        return ResponseEntity.ok(ret);
    }

    @RequestMapping(value = "/web/rest/plant/change_plc_categories",
            method = RequestMethod.POST)
    public ResponseEntity<?> changePlcCategories(@RequestBody List<PlcCategoryVO> plcCategoryVOs) {
        try {
            plantSerive.changePlcCategories(plcCategoryVOs);
        } catch (BusinessException e) {
            logger.error("RestPlantController.changePlcCategories() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(
                    Constants.RESPONSE_HEADER_ERROR_MESSAGE, e.getMessage()).body(null);
        }
        return ResponseEntity.ok().body(null);
    }



}
