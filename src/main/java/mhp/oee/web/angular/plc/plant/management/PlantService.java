package mhp.oee.web.angular.plc.plant.management;


import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.handle.PlcCategoryBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.dao.gen.PlcAccessTypePOMapper;
import mhp.oee.dao.gen.PlcCategoryPOMapper;
import mhp.oee.dao.gen.PlcObjectPOMapper;
import mhp.oee.dao.gen.ResourceMeUserPOMapper;
import mhp.oee.dao.gen.ResourcePOMapper;
import mhp.oee.dao.gen.ResourcePlcInfoPOMapper;
import mhp.oee.dao.gen.ResourcePlcPOMapper;
import mhp.oee.dao.gen.ResourceStatePOMapper;
import mhp.oee.dao.gen.ResourceTypePOMapper;
import mhp.oee.dao.gen.ResourceTypeResourcePOMapper;
import mhp.oee.dao.gen.ScheduledDownPlanPOMapper;
import mhp.oee.dao.gen.StdCycleTimePOMapper;
import mhp.oee.po.gen.PlcCategoryPO;
import mhp.oee.po.gen.PlcCategoryPOExample;
import mhp.oee.po.gen.PlcObjectPO;
import mhp.oee.po.gen.PlcObjectPOExample;
import mhp.oee.po.gen.ResourcePlcPO;
import mhp.oee.po.gen.ResourcePlcPOExample;
import mhp.oee.utils.Utils;
import mhp.oee.vo.PlcCategoryVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class PlantService {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    private ResourcePOMapper resourcePOMapper;
    @Autowired
    private ResourceStatePOMapper resourceStatePOMapper;
    @Autowired
    private ResourcePlcPOMapper resourcePlcPOMapper;
    @Autowired
    private ResourcePlcInfoPOMapper resourcePlcInfoPOMapper;
    @Autowired
    private ResourceMeUserPOMapper resourceMeUserPOMapper;
    @Autowired
    private ResourceTypePOMapper resourceTypePOMapper;
    @Autowired
    private ResourceTypeResourcePOMapper resourceTypeResourcePOMapper;

    @Autowired
    private ScheduledDownPlanPOMapper scheduledDownPlanPOMapper;
    @Autowired
    private StdCycleTimePOMapper stdCycleTimePOMapper;

    @Autowired
    PlcAccessTypePOMapper plcAccessTypePOMapper;
    @Autowired
    PlcCategoryPOMapper plcCategoryPOMapper;
    @Autowired
    PlcObjectPOMapper plcObjectPOMapper;




    // ==========================================================================
    // Plc_Category
    // ==========================================================================

    public List<PlcCategoryVO> readAllPlcCategories(String site) {
        PlcCategoryPOExample example = new PlcCategoryPOExample();
        if (site != null) {
            example.createCriteria().andSiteEqualTo(site);
        }
        example.setOrderByClause("HANDLE");
        List<PlcCategoryPO> pos = plcCategoryPOMapper.selectByExample(example);
        List<PlcCategoryVO> vos = Utils.copyListProperties(pos, PlcCategoryVO.class);
        return vos;
    }

    public boolean existPlcCategory(String site, String plcCategory) {
        String handle = new PlcCategoryBOHandle(site, plcCategory).getValue();
        PlcCategoryPO po = plcCategoryPOMapper.selectByPrimaryKey(handle);
        return (po != null) ? true : false;
    }

    public boolean isPlcCategoryUsed(String site, String plcCategory) {
        // cross-check RESOURCE_PLC
        ResourcePlcPOExample rpExample = new ResourcePlcPOExample();
        rpExample.createCriteria().andSiteEqualTo(site).andCategoryEqualTo(plcCategory);
        List<ResourcePlcPO> rpPOs = resourcePlcPOMapper.selectByExample(rpExample);
        if (!Utils.isEmpty(rpPOs)) {
            return true;
        }

        // cross-check PLC_OBJECT
        PlcObjectPOExample poExmaple = new PlcObjectPOExample();
        poExmaple.createCriteria().andSiteEqualTo(site).andCategoryEqualTo(plcCategory);
        List<PlcObjectPO> poPOs = plcObjectPOMapper.selectByExample(poExmaple);
        if (!Utils.isEmpty(poPOs)) {
            return true;
        }

        return false;
    }

    public void changePlcCategories(List<PlcCategoryVO> plcCategoryVOs) throws BusinessException {
        for (PlcCategoryVO each : plcCategoryVOs) {
            switch (each.getViewActionCode()) {
            case 'C':
                createPlcCategory(each);
                break;
            case 'U':
                updatePlcCategory(each);
                break;
            case 'D':
                deletePlcCategory(each);
                break;
            default:
                logger.error("PlantService.cudPlcCategoryChanges() : Invalid viewActionCode '"
                        + each.getViewActionCode() + "'.");
                break;
            }
        }
    }

    public String createPlcCategory(PlcCategoryVO plcCategoryVO) throws BusinessException {
        String handle = new PlcCategoryBOHandle(plcCategoryVO.getSite(), plcCategoryVO.getCategory()).getValue();
        PlcCategoryPO existingPO = plcCategoryPOMapper.selectByPrimaryKey(handle);
        if (existingPO != null) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "Existing PLC_CATEGORY record : " + gson.toJson(existingPO));
        }

        PlcCategoryPO po = Utils.copyObjectProperties(plcCategoryVO, PlcCategoryPO.class);
        po.setHandle(handle);   // this is create action, view does not provide HANDLE
        po.setModifiedUser(ContextHelper.getContext().getUser());
        int ret = plcCategoryPOMapper.insert(po);
        if (ret != 1) {
            existingPO = plcCategoryPOMapper.selectByPrimaryKey(handle);
            throw new BusinessException(ErrorCodeEnum.BASIC, "Existing PLC_CATEGORY record : " + gson.toJson(existingPO));
        }
        return handle;
    }

    public void updatePlcCategory(PlcCategoryVO plcCategoryVO) throws BusinessException {
        PlcCategoryPO po = Utils.copyObjectProperties(plcCategoryVO, PlcCategoryPO.class);
        po.setModifiedUser(ContextHelper.getContext().getUser());

        PlcCategoryPOExample example = new PlcCategoryPOExample();
        example.createCriteria()
                .andHandleEqualTo(po.getHandle())
                .andModifiedDateTimeEqualTo(po.getModifiedDateTime());

        int ret = plcCategoryPOMapper.updateByExample(po, example);
        if (ret != 1) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "updatePlcCategory");
        }
    }

    private int deletePlcCategory(PlcCategoryVO plcCategoryVO) throws BusinessException {
        if (isPlcCategoryUsed(plcCategoryVO.getSite(), plcCategoryVO.getCategory())) {
            throw new BusinessException(ErrorCodeEnum.BASIC, "deletePlcCategory");
        }
        PlcCategoryPOExample example = new PlcCategoryPOExample();
        example.createCriteria()
                .andHandleEqualTo(plcCategoryVO.getHandle())
                .andModifiedDateTimeEqualTo(plcCategoryVO.getModifiedDateTime());
        // if ret==0, check if it was updated by other user in advance.
        //     updated by other user => throw exception
        //     record not existed    => ignore it ??
        return plcCategoryPOMapper.deleteByExample(example);
    }

}
