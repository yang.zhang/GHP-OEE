package mhp.oee.web.angular.authorization.management;

public class IPVO {
    
    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public IPVO(String ip) {
        super();
        this.ip = ip;
    }
    
    
}
