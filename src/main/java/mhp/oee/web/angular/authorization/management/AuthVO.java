package mhp.oee.web.angular.authorization.management;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mhp.oee.po.gen.UserGroupMemberPO;
import mhp.oee.vo.ActivityGroupVO;
import mhp.oee.vo.ActivityVO;

public class AuthVO {
    private Set<ActivityGroupVO> activityGroupVOs = new HashSet<ActivityGroupVO>();

    /*
     * use to debug
     */
    private List<ActivityVO> activityVOs = new ArrayList<ActivityVO>();
    private List<UserGroupMemberPO> groupMemberPOs = new ArrayList<UserGroupMemberPO>();

    public Set<ActivityGroupVO> getActivityGroupVOs() {
        return activityGroupVOs;
    }

    public void setActivityGroupVOs(Set<ActivityGroupVO> activityGroupVOs) {
        this.activityGroupVOs = activityGroupVOs;
    }

    public ActivityGroupVO searchByGroupHandle(String handle) {
        for (ActivityGroupVO vo : activityGroupVOs) {
            if (vo.getHandle().equals(handle)) {
                return vo;
            }
        }
        return null;
    }

    public List<ActivityVO> getActivityVOs() {
        return activityVOs;
    }

    public void setActivityVOs(List<ActivityVO> activityVOs) {
        this.activityVOs = activityVOs;
    }

    public List<UserGroupMemberPO> getGroupMemberPOs() {
        return groupMemberPOs;
    }

    public void setGroupMemberPOs(List<UserGroupMemberPO> groupMemberPOs) {
        this.groupMemberPOs = groupMemberPOs;
    }

}
