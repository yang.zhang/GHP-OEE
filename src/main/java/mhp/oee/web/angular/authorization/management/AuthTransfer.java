package mhp.oee.web.angular.authorization.management;

import java.util.ArrayList;
import java.util.List;

import mhp.oee.po.gen.UserGroupMemberPO;
import mhp.oee.vo.ActivityGroupVO;
import mhp.oee.vo.ActivityVO;

public class AuthTransfer {

    private List<ActivityVO> activityVOs = new ArrayList<ActivityVO>();
    private List<ActivityGroupVO> activityGroupVOs = new ArrayList<ActivityGroupVO>();
    
    private List<UserGroupMemberPO> groupMemberPOs = new ArrayList<UserGroupMemberPO>();

    public List<ActivityVO> getActivityVOs() {
        return activityVOs;
    }

    public void setActivityVOs(List<ActivityVO> activityVOs) {
        this.activityVOs = activityVOs;
    }

    protected List<ActivityGroupVO> getActivityGroupVOs() {
        return activityGroupVOs;
    }

    protected void setActivityGroupVOs(List<ActivityGroupVO> activityGroupVOs) {
        this.activityGroupVOs = activityGroupVOs;
    }

	public List<UserGroupMemberPO> getGroupMemberPOs() {
		return groupMemberPOs;
	}

	public void setGroupMemberPOs(List<UserGroupMemberPO> groupMemberPOs) {
		this.groupMemberPOs = groupMemberPOs;
	}

}
