package mhp.oee.web.angular.authorization.management;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mhp.oee.config.JobDispatchConfig;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.javafx.collections.MappingChange.Map;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.InvalidUserAuthException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ExecutionContext;
import mhp.oee.config.DevEnvConfig;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ResourceVO;
import mhp.oee.vo.SiteVO;
import mhp.oee.web.Constants;

@Controller
public class AuthController {

    @InjectableLogger
    private static Logger logger;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    private AuthService authService;

    @Autowired
    private DevEnvConfig config;

    @Autowired
    private JobDispatchConfig jobDispatchConfig;

    @RequestMapping(value = "/auth/logon_user", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<ExecutionContext> getDefaultUser(HttpServletResponse res, HttpServletRequest request) {

        ExecutionContext logonUser = this.authService.getLogonUser();
        logger.debug("<<  : " + gson.toJson(logonUser));
        return ResponseEntity.ok().body(logonUser);
    }

    @RequestMapping(value = "/auth/site/{site}/user_sitelist", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<SiteVO>> getSiteByUser(@PathVariable("site") String site,
            @RequestParam(value = "userId", required = true) String userId, HttpServletResponse res,
            HttpServletRequest request) {

        String trace = String.format("[auth, userid][site/%s][user/%s]", site, userId);
        logger.debug(">> " + trace);
        List<SiteVO> list = this.authService.getSiteListByUser(userId);
        logger.debug("<< " + trace + " : " + gson.toJson(list));

        return ResponseEntity.ok(list);
    }

    @RequestMapping(value = "/auth/management/site/{site}", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<AuthTransfer> getAuth(@PathVariable("site") String site, HttpServletResponse res,
            HttpServletRequest request) {
        try {
            String trace = String.format("[auth][site/%s/]", site);
            logger.debug(">> " + trace);

            AuthVO auth = this.authService.getAuth(site);
            AuthTransfer returnTransfer = this.authService.getAuthSort(site);

            logger.debug("<< " + trace + " : " + gson.toJson(auth));
            
            return ResponseEntity.ok().body(returnTransfer);

        } catch (InvalidUserAuthException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ReasonCodeAlertController.readReasonCodeAlertHisStatus() : " + e.getMessage()
                    + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
    }

    @RequestMapping(value = "/auth/version", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<DevEnvConfig> getAuth(HttpServletResponse res, HttpServletRequest request) {
        return ResponseEntity.ok().body(config);
    }

    @RequestMapping(value = "/auth/job", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<JobDispatchConfig> getJobConfig(HttpServletResponse res, HttpServletRequest request) {
        return ResponseEntity.ok().body(jobDispatchConfig);
    }

    @RequestMapping(value = "/auth/logout", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> logout(HttpServletResponse response, HttpServletRequest request) throws IOException {
        HttpSession session = request.getSession();
        session.invalidate();
        return ResponseEntity.status(HttpStatus.OK).body(true);
    }
    
    @RequestMapping(value = "/auth/get_request_ip", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> getResourceByIp(HttpServletRequest request) throws BusinessException{
        String ip=request.getRemoteHost();
        IPVO vo = new IPVO(ip);
        return ResponseEntity.ok(vo);
    }
    @RequestMapping(value = "/auth/test_log_error", method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> test(HttpServletRequest request) throws BusinessException{
        List<String> list = new LinkedList<String> ();
        list.add("zhangsan");
        list.add("lisi");
        String abc =list.get(5);
        return ResponseEntity.ok(abc);
    }


}
