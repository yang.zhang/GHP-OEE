package mhp.oee.web.angular.authorization.management;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.InvalidUserAuthException;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.common.security.ContextHelper;
import mhp.oee.common.security.ExecutionContext;
import mhp.oee.component.activity.ActivityComponent;
import mhp.oee.component.plant.SiteComponent;
import mhp.oee.component.user.UserComponent;
import mhp.oee.po.gen.UserGroupMemberPO;
import mhp.oee.po.gen.UserPO;
import mhp.oee.vo.ActivityGroupVO;
import mhp.oee.vo.ActivityVO;
import mhp.oee.vo.SiteVO;

@Service
@Transactional(rollbackFor = { BusinessException.class })
public class AuthService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    UserComponent userComponent;

    @Autowired
    ActivityComponent activityComponent;

    @Autowired
    SiteComponent siteComponent;

    public AuthVO getAuth(String site) throws InvalidUserAuthException {
        ExecutionContext context = ContextHelper.getContext();
        String user = context.getUser();
        logger.debug(">>>>>>>  current user id:  " + user);
        UserPO userDetails = userComponent.getUserDetails(site, user);

        if (userDetails != null && userDetails.getEnabled().equals("true")) {
            List<UserGroupMemberPO> userGroupMemberPOList = userComponent.getUserGroupMemberPOList(site, user);
            AuthTransfer vo = new AuthTransfer();
            AuthVO returnObject = new AuthVO();
            

            for (UserGroupMemberPO po : userGroupMemberPOList) {
                String userGroupBo = po.getUserGroupBo();
                List<ActivityVO> activityList = activityComponent.getActivityByUserGroup(userGroupBo);
                if (activityList != null) {
                    vo.getActivityVOs().addAll(activityList);
                    returnObject.getActivityVOs().addAll(activityList);
                }
                returnObject.getGroupMemberPOs().add(po);
            }

            this.deleteDuplicateAndOverwrite(vo);

            for (ActivityVO activityVO : vo.getActivityVOs()) {
                List<ActivityGroupVO> activityGroupVOs = activityComponent.getActivityGroupByActivity(activityVO);
                vo.getActivityGroupVOs().addAll(activityGroupVOs);
            }

            for (ActivityGroupVO group : vo.getActivityGroupVOs()) {
                ActivityGroupVO searchResult = returnObject.searchByGroupHandle(group.getHandle());
                if (searchResult == null) {
                    returnObject.getActivityGroupVOs().add(group);
                } else {
                    searchResult.getActivityVOs().addAll(group.getActivityVOs());
                }

            }
            return returnObject;

        }

        throw new InvalidUserAuthException();

    }
    public AuthTransfer getAuthSort(String site) throws InvalidUserAuthException {
    	AuthVO returnObject = getAuth(site);
    	List<ActivityGroupVO> activityGroupVOs = new ArrayList<ActivityGroupVO>();
        for(Iterator<ActivityGroupVO> iterator = returnObject.getActivityGroupVOs().iterator(); iterator.hasNext();){
        	ActivityGroupVO b = iterator.next();
        	Collections.sort(b.getActivityVOs(), new MyIntComparator());
        	activityGroupVOs.add(b);
        }
        Collections.sort(activityGroupVOs, new GroupComparator());
        AuthTransfer returnTransfer = new AuthTransfer();
        returnTransfer.setActivityGroupVOs(activityGroupVOs);
        returnTransfer.setActivityVOs(returnObject.getActivityVOs());
        returnTransfer.setGroupMemberPOs(returnObject.getGroupMemberPOs());
		return returnTransfer;
    	
    }

    public void deleteDuplicateAndOverwrite(AuthTransfer transfer) {
        List<ActivityVO> unDuplicateAndOverwrite = new ArrayList<ActivityVO>();
        for (ActivityVO vo : transfer.getActivityVOs()) {
            if (searchActivity(unDuplicateAndOverwrite, vo)) {
                unDuplicateAndOverwrite.add(vo);
            }
        }
        transfer.setActivityVOs(unDuplicateAndOverwrite);
    }

    private boolean searchActivity(List<ActivityVO> unDuplicateAndOverwrite, ActivityVO vo) {
        for (Iterator<ActivityVO> iterator = unDuplicateAndOverwrite.iterator(); iterator.hasNext(); ) {
            ActivityVO in = iterator.next();
            if (in.getActivity().equals(vo.getActivity()) && vo.getPermPO().getPermissionMode().equals("RW")) {
                iterator.remove();
                return true;
            }

            if(in.getActivity().equals(vo.getActivity())) {
                return false;
            }

        }
        return true;
    }

    public ExecutionContext getLogonUser() {
        return ContextHelper.getContext();
    }

    public List<SiteVO> getSiteListByUser(String userId) {
        List<UserPO> siteList = userComponent.getSiteList(userId);
        List<SiteVO> voList = new ArrayList<SiteVO>();
        for (UserPO po : siteList) {
            SiteVO siteDetails = siteComponent.getSiteDetails(po.getSite());
            voList.add(siteDetails);
        }

        return voList;
    }
}

class MyIntComparator implements Comparator {

	/**
	 * o1比o2大，返回-1；o1比o2小，返回1。
	 */
	public int compare(Object o1, Object o2) {
		ActivityVO i1 = ((ActivityVO) o1);
		ActivityVO i2 = ((ActivityVO) o2);
		if (i1.getDescription().compareTo(i2.getDescription()) > 0) {
			return 1;
		}
		if (i1.getDescription().compareTo(i2.getDescription()) < 0) {
			return -1;
		}
		return 0;
	}
}
class GroupComparator implements Comparator {

	/**
	 * o1比o2大，返回-1；o1比o2小，返回1。
	 */
	public int compare(Object o1, Object o2) {
		
		String[] groupsHead = {"实时看板","D/T报表","PPM报表","产量报表","OEE报表","报警报表","其他报表"};
		String[] groupsEnd = {"信息配置管理","权限配置管理"};
		
		ActivityGroupVO i1 = (ActivityGroupVO) o1;
		ActivityGroupVO i2 = (ActivityGroupVO) o2;
		
		for (String group : groupsHead) {
			if (group.equals(i1.getDescription())) {
				return -1;
			}
			if (group.equals(i2.getDescription())) {
				return 1;
			}
		}
		
		for (String group : groupsEnd) {
			if (group.equals(i2.getDescription())) {
				return -1;
			}
			if (group.equals(i1.getDescription())) {
				return 1;
			}
		}
		
		if (i1.getDescription().compareTo(i2.getDescription()) > 0) {
			return 1;
		}
		if (i1.getDescription().compareTo(i2.getDescription()) < 0) {
			return -1;
		}
		return 0;
	}
}
