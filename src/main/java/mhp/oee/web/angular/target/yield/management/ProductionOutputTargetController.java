package mhp.oee.web.angular.target.yield.management;

import java.text.ParseException;
import java.util.List;
import java.util.Set;

import mhp.oee.vo.WorkCenterShiftConditionVO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.vo.ItemVO;
import mhp.oee.vo.ProductionOutputTargetAndResrceVO;
import mhp.oee.web.Constants;

@RestController
public class ProductionOutputTargetController {

    @InjectableLogger
    private static Logger logger;
    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ProductionOutputTargetService productionOutputTargetService;

    @RequestMapping(value = "/web/rest/plant/site/{site}/item", 
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ItemVO>> readItemByModel(
            @PathVariable("site") String site,
            @RequestParam(value = "model", required = true) String model) {
        String trace = String.format("[ProductionOutputTargetController.readItemByModel] request : site=%s, model=%s",
                site, model);
        logger.debug(">> " + trace);
        List<ItemVO> pos = productionOutputTargetService.readItemByModel(site, model);
        logger.debug("<< [ProductionOutputTargetController.readItemByModel] response : "  + gson.toJson(pos));
        return ResponseEntity.ok(pos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/shift", 
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Set<WorkCenterShiftConditionVO>> readShift(
            @PathVariable("site") String site,
            @RequestParam(value = "resrce", required = false) String resrce,
            @RequestParam(value = "work_center", required = false) String workCenter) {
        String trace = String.format("[ProductionOutputTargetController.readShift] request : site=%s, resrce=%s, workCenter=%s",
                site, resrce, workCenter);
        logger.debug(">> " + trace);
        Set<WorkCenterShiftConditionVO> vos = productionOutputTargetService.readShift(site, resrce, workCenter);
        logger.debug("<< [ProductionOutputTargetController.readShift] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/production_output_target", 
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<List<ProductionOutputTargetAndResrceVO>> readProductionOutputTarget(
            @PathVariable("site") String site, 
            @RequestParam(value = "item", required = true) String item,
            @RequestParam(value = "resrce", required = false) String resrce,
            @RequestParam(value = "shift", required = true) String shift,
            @RequestParam(value = "start_date_time", required = true) String startDateTime,
            @RequestParam(value = "end_date_time", required = true) String endDateTime,
            @RequestParam(value = "work_center", required = true) String workCenter,
            @RequestParam(value = "line", required = true) String line,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "model", required = false) String model,
            @RequestParam(value = "page_index", required = true) int pageIndex,
            @RequestParam(value = "page_count", required = true) int pageCount,
            @RequestParam(value = "shift_desc", required = true) String shiftDes) {
        String trace = String.format("[ProductionOutputTargetController.readProductionOutputTarget] request : site=%s, item=%s, resrce=%s, shift=%s, startDateTime=%s, endDateTime=%s, workCenter=%s, line=%s, resourceType=%s, model=%s, pageIndex=%s, pageCount=%s",
                site, item, resrce, shift, startDateTime, endDateTime, workCenter, line, resourceType, model, pageIndex, pageCount);
        logger.debug(">> " + trace);
        List<ProductionOutputTargetAndResrceVO> vos = null;
        try {
            vos = productionOutputTargetService.readProductionOutputTarget(site, item, resrce, shift, startDateTime
                    , endDateTime, workCenter, line, resourceType, model, pageIndex, pageCount, shiftDes);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ProductionOutputTargetController.readProductionOutputTarget() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ProductionOutputTargetController.readProductionOutputTarget] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }
    
    @RequestMapping(value = "/web/rest/plant/site/{site}/production_output_target_count", 
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Integer> readProductionOutputTargetCount(
            @PathVariable("site") String site, 
            @RequestParam(value = "item", required = true) String item,
            @RequestParam(value = "resrce", required = false) String resrce,
            @RequestParam(value = "shift", required = true) String shift,
            @RequestParam(value = "start_date_time", required = true) String startDateTime,
            @RequestParam(value = "end_date_time", required = true) String endDateTime,
            @RequestParam(value = "work_center", required = true) String workCenter,
            @RequestParam(value = "line", required = true) String line,
            @RequestParam(value = "resource_type", required = false) String resourceType,
            @RequestParam(value = "model", required = false) String model,
            @RequestParam(value = "shift_desc", required = true) String shiftDes) {
        String trace = String.format("[ProductionOutputTargetController.readProductionOutputTargetCount] request : site=%s, item=%s, resrce=%s, shift=%s, startDateTime=%s, endDateTime=%s, workCenter=%s, line=%s, resourceType=%s, model=%s",
                site, item, resrce, shift, startDateTime, endDateTime, workCenter, line, resourceType, model);
        logger.debug(">> " + trace);
        int vos = 0;
        try {
            vos = productionOutputTargetService.readProductionOutputTargetCount(site, item, resrce, shift, startDateTime
                    , endDateTime, workCenter, line, resourceType, model, shiftDes);
        } catch (ParseException e) {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.PARSE_ERROR.getErrorCode(), "Date parse error", null);
            logger.error("ProductionOutputTargetController.readProductionOutputTargetCount() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ProductionOutputTargetController.readProductionOutputTarget] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/site/{site}/exist_production_output_target", 
            method = RequestMethod.GET, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<Boolean> existProductionOutputTarget(
            @PathVariable("site") String site,
            @RequestParam(value = "item", required = true) String item,
            @RequestParam(value = "resrce", required = false) String resrce,
            @RequestParam(value = "shift", required = true) String shift,
            @RequestParam(value = "target_date", required = true) String targetDate) {
        String trace = String.format("[ProductionOutputTargetController.existProductionOutputTarget] request : site=%s, item=%s, resrce=%s, shift=%s, targetDate=%s",
                site, item, resrce, shift, targetDate);
        logger.debug(">> " + trace);
        boolean vos = productionOutputTargetService.existProductionOutputTarget(site, item, resrce, shift, targetDate);
        logger.debug("<< [ProductionOutputTargetController.existProductionOutputTarget] response : "  + gson.toJson(vos));
        return ResponseEntity.ok(vos);
    }

    @RequestMapping(value = "/web/rest/plant/change_production_output_target", 
            method = RequestMethod.POST, produces = Constants.REST_HTTP_GET_CONTENT_TYPE)
    public ResponseEntity<?> changeProductionOutputTarget(@RequestBody List<ProductionOutputTargetAndResrceVO> productionOutputTargetAndResrceVO) {
        String trace = String.format("[ProductionOutputTargetController.changeProductionOutputTarget] request : " + gson.toJson(productionOutputTargetAndResrceVO));
        logger.debug(">> " + trace);
        try {
            productionOutputTargetService.changeProductionOutputTarget(productionOutputTargetAndResrceVO);
        } catch (BusinessException e) {
            ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
            logger.error("ProductionOutputTargetController.changeProductionOutputTarget() : " + e.getMessage() + System.lineSeparator() + Utils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        logger.debug("<< [ProductionOutputTargetController.changeProductionOutputTarget] response : void");
        return ResponseEntity.ok(null);
    }
}
