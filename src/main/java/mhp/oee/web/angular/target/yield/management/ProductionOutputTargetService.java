package mhp.oee.web.angular.target.yield.management;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.ResourceTypeBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ProductionOutputTargetComponent;
import mhp.oee.component.plant.ResourcePlcInfoComponent;
import mhp.oee.po.gen.ProductionOutputTargetPO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.utils.Utils;
import mhp.oee.vo.ItemVO;
import mhp.oee.vo.ProductionOutputTargetAndResrceVO;
import mhp.oee.vo.ResourceConditionVO;
import mhp.oee.vo.WorkCenterShiftConditionVO;

@Service
@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
public class ProductionOutputTargetService {

    @InjectableLogger
    private static Logger logger;
    private static String ENABLED_Y = "true";
    private static String IS_DEFAULT_VALUE = "true";

    @Autowired
    ProductionOutputTargetComponent productionOutputTargetComponent;
    
    @Autowired
    private ResourcePlcInfoComponent resourcePlcInfoComponent;

    public List<ItemVO> readItemByModel(String site, String model) {
        return productionOutputTargetComponent.readItemByModel(site, model, ENABLED_Y);
    }

    public Set<WorkCenterShiftConditionVO> readShift(String site, String resrce, String workCenter) {
        Set<WorkCenterShiftConditionVO> voSet = new HashSet<WorkCenterShiftConditionVO>();
        List<WorkCenterShiftConditionVO> voList = null;
        if (!Utils.isEmpty(resrce)) {
            voList = productionOutputTargetComponent.readShiftByResrce(site, resrce, ENABLED_Y);
        } else {
            voList = productionOutputTargetComponent.readShift(site, workCenter, ENABLED_Y);
        }
        voSet.addAll(voList);
        return voSet;
    }

    public List<ProductionOutputTargetAndResrceVO> readProductionOutputTarget(String site, String item, String resrce,
            String shift, String startDateTime, String endDateTime, String workCenter, String line, String resourceType,
            String model, int pageIndex, int pageCount, String shiftDes) throws ParseException {
        List<ResourceConditionVO> resourceConditionVOs = new ArrayList<ResourceConditionVO>();
        List<ProductionOutputTargetAndResrceVO> resultPageList = new ArrayList<ProductionOutputTargetAndResrceVO>();
        startDateTime = Utils.getTargetDate(startDateTime);
        endDateTime = Utils.getTargetDate(endDateTime);
        // 1 GET RESRCE LIST
        if (Utils.isEmpty(resrce)) {
            String resourceTypeBO = null;
            if (!Utils.isEmpty(resourceType)) {
                resourceTypeBO = new ResourceTypeBOHandle(site, resourceType).getValue();
            }
            if(line != null && line.length() > 0) {
                String[] lineArray = line.split(",");
                resourceConditionVOs = Utils.copyListProperties(resourcePlcInfoComponent.readResourceWithMultipleLine(site, resourceTypeBO, lineArray, workCenter), ResourceConditionVO.class);
            } else {
                resourceConditionVOs = Utils.copyListProperties(resourcePlcInfoComponent.readResource(site, resourceTypeBO, line, workCenter),ResourceConditionVO.class);
            }
            //resourceConditionVOs = productionOutputTargetComponent.readResourceByCondition(site, resourceTypeBO, line, workCenter);
        } else {
            ResourceConditionVO resourceConditionVO = new ResourceConditionVO();
            ResrceBOHandle boHandle = new ResrceBOHandle(site, resrce);
            ResourcePO resourcePO = productionOutputTargetComponent.readResource(boHandle.getValue());
            resourceConditionVO.setResrce(resrce);
            resourceConditionVO.setDescription(resourcePO.getDescription());
            resourceConditionVO.setAsset(resourcePO.getAsset());
            resourceConditionVOs.add(resourceConditionVO);
        }
        if (resourceConditionVOs.size() <= 0) {
            return resultPageList;
        }

        // 2 CHECK IF EXIST DEFAULT VALUE
        List<ProductionOutputTargetAndResrceVO> resultAllList = existDefaultValue(resourceConditionVOs, site, item,
                shift, startDateTime, endDateTime, resrce, model, shiftDes);

        // 3 GET DATE FROM TABLE : PRODUCTION_OUTPUT_TARGET
        List<ProductionOutputTargetAndResrceVO> vos = productionOutputTargetComponent.readProductionOutputTargetAndResrce(site, item, resourceConditionVOs, shift
                , startDateTime, endDateTime);
        resultAllList.addAll(vos);

        // 4 SORT
        Collections.sort(resultAllList);

        // 5 PAGING
        int start = (pageIndex - 1) * pageCount;
        int end = (pageIndex * pageCount) > resultAllList.size() ? resultAllList.size() : (pageIndex * pageCount);
        for (int i = start; i < end; i++) {
            resultPageList.add(resultAllList.get(i));
        }
        return resultPageList;
    }

    public int readProductionOutputTargetCount(String site, String item, String resrce,
            String shift, String startDateTime, String endDateTime, String workCenter, String line, String resourceType,
            String model, String shiftDes) throws ParseException {
        int result = 0;
        List<ResourceConditionVO> resourceConditionVOs = new ArrayList<ResourceConditionVO>();
        startDateTime = Utils.getTargetDate(startDateTime);
        endDateTime = Utils.getTargetDate(endDateTime);
        // 1 GET RESRCE LIST
        if (Utils.isEmpty(resrce)) {
            String resourceTypeBO = null;
            if (!Utils.isEmpty(resourceType)) {
                resourceTypeBO = new ResourceTypeBOHandle(site, resourceType).getValue();
            }
            if(line != null && line.length() > 0) {
                String[] lineArray = line.split(",");
                resourceConditionVOs = Utils.copyListProperties(resourcePlcInfoComponent.readResourceWithMultipleLine(site, resourceTypeBO, lineArray, workCenter), ResourceConditionVO.class);
            } else {
                resourceConditionVOs = Utils.copyListProperties(resourcePlcInfoComponent.readResource(site, resourceTypeBO, line, workCenter),ResourceConditionVO.class);
            }
            //resourceConditionVOs = productionOutputTargetComponent.readResourceByCondition(site, resourceTypeBO, line, workCenter);
        } else {
            ResourceConditionVO resourceConditionVO = new ResourceConditionVO();
            ResrceBOHandle boHandle = new ResrceBOHandle(site, resrce);
            ResourcePO resourcePO = productionOutputTargetComponent.readResource(boHandle.getValue());
            resourceConditionVO.setResrce(resrce);
            resourceConditionVO.setDescription(resourcePO.getDescription());
            resourceConditionVO.setAsset(resourcePO.getAsset());
            resourceConditionVOs.add(resourceConditionVO);
        }
        if (resourceConditionVOs.size() <= 0) {
            return result;
        }

        // 2 CHECK IF EXIST DEFAULT VALUE
        List<ProductionOutputTargetAndResrceVO> resultAllList = existDefaultValue(resourceConditionVOs, site, item,
                shift, startDateTime, endDateTime, resrce, model, shiftDes);

        // 3 GET DATE FROM TABLE : PRODUCTION_OUTPUT_TARGET
        List<ProductionOutputTargetAndResrceVO> vos = productionOutputTargetComponent.readProductionOutputTargetAndResrce(site, item, resourceConditionVOs, shift
                , startDateTime, endDateTime);
        resultAllList.addAll(vos);

        // 4 SORT
        Collections.sort(resultAllList);

        return resultAllList.size();
    }

    private List<ProductionOutputTargetAndResrceVO> existDefaultValue(List<ResourceConditionVO> resourceConditionVOs,
            String site, String item, String shift, String startDateTime, String endDateTime, String resrce,
            String model, String shiftDes) {
        List<ProductionOutputTargetAndResrceVO> resultAllList = new ArrayList<ProductionOutputTargetAndResrceVO>();
        for (ResourceConditionVO resourceConditionVO : resourceConditionVOs) {
            List<ProductionOutputTargetPO> productionOutputTargetPOs = productionOutputTargetComponent
                    .readProductionOutputTarget(site, item, resourceConditionVO.getResrce(), shift, null,
                            null, IS_DEFAULT_VALUE, null);
            if (productionOutputTargetPOs == null || (productionOutputTargetPOs != null && productionOutputTargetPOs.size() == 0)) {
                ProductionOutputTargetAndResrceVO vo = new ProductionOutputTargetAndResrceVO();
                ResourcePO po = new ResourcePO();
                po.setResrce(resourceConditionVO.getResrce());
                po.setDescription(resourceConditionVO.getDescription());
                po.setAsset(resourceConditionVO.getAsset());
                vo.setSite(site);
                vo.setResrce(po.getResrce());
                vo.setResourcePO(po);
                vo.setItem(item);
                vo.setModel(model);
                vo.setShift(shift);
                vo.setShiftDescription(shiftDes);
                vo.setTargetDate("*");
                vo.setIsDefaultValue("true");
                resultAllList.add(vo);
            }
        }
        return resultAllList;
    }

    public boolean existProductionOutputTarget(String site, String item, String resrces, String shift,
            String targetDate) {
        List<ProductionOutputTargetPO> productionOutputTargetPOs = productionOutputTargetComponent.readProductionOutputTarget(site, item, resrces, shift, null, null, null, targetDate);
        if (productionOutputTargetPOs != null && productionOutputTargetPOs.size() > 0) {
            return true;
        }
        return false;
    }

    public void changeProductionOutputTarget(List<ProductionOutputTargetAndResrceVO> productionOutputTargetAndResrceVO)
            throws BusinessException {
        for (ProductionOutputTargetAndResrceVO each : productionOutputTargetAndResrceVO) {
            switch (each.getViewActionCode()) {
            case 'C':
                productionOutputTargetComponent.createProductionOutputTarget(each);
                break;
            case 'U':
                productionOutputTargetComponent.updateProductionOutputTarget(each);
                break;
            case 'D':
                productionOutputTargetComponent.deleteProductionOutputTarget(each);
                break;
            default:
                logger.error("ProductionOutputTargetService.cudchangeProductionOutputTarget() : Invalid viewActionCode '" + each.getViewActionCode() + "'.");
                break;
            }
        }
    }
}
