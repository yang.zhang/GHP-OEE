package mhp.oee.web;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.cookie.CookieSessionUtil;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.po.gen.ResourcePlcInfoPO;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.web.angular.plc.accesstype.management.PlantAccessTypeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/** 这个controller 的接口不做拦截
 * Created by dinglj on 2017/5/16 016.
 */
@RestController
@RequestMapping("/web/public")
public class PublicController {

    @Autowired
    private PlantAccessTypeService plantAccessTypeService;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Value("${version}")
    private String version;

    @RequestMapping(value = "/version")
    public ResponseEntity<String> getCurrentVersion(){

        String versionInfo = "[ANDON_APP_VERSION = " + ( this.version == null ? "" : this.version )+ "]";

        return ResponseEntity.ok(versionInfo);
    }


    /**
     * 根据ip获取绑定设备信息
     * @param request
     * @param ip
     * @return
     */
    @RequestMapping(value = "/resourcePlcInfo")
    public ResponseEntity<String> getReousrcePlcInfo(HttpServletRequest request,
                                             @RequestParam(value = "ip") String ip){

        //如果session中有ip和设备号 不做处理直接返回
        String uppcIp = (String) request.getSession(true).getAttribute(CookieSessionUtil.UPPC_IP_KEY);
        String uppcResource = (String) request.getSession(true).getAttribute(CookieSessionUtil.UPPC_RESOURCE_KEY);
        if(StringUtils.isNotBlank(uppcIp) && StringUtils.isNotBlank(uppcResource)){
            return ResponseEntity.ok(uppcResource);
        }


        List<ResourcePlcInfoPO> resourcePlcList = plantAccessTypeService.getResourcePlcInfoByPcIPAndPlcAccessTypeNull(ip);

        if(CollectionUtils.isEmpty(resourcePlcList)){
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.COMMON_NO_DATA.getErrorCode(), "查询无记录", null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }
        if(resourcePlcList.size() == 1){
            ResourcePlcInfoPO resourcePlcInfoPO = resourcePlcList.get(0);
            //ip和设备编码放在session
            request.getSession(true).setAttribute(CookieSessionUtil.UPPC_IP_KEY,ip);
            request.getSession(true).setAttribute(CookieSessionUtil.UPPC_RESOURCE_KEY,resourcePlcInfoPO.getResourceBo());

            return ResponseEntity.ok(resourcePlcInfoPO.getResourceBo());
        }else {
            ExceptionVO vo = new ExceptionVO(ErrorCodeEnum.COMMON_MORETHAN_ONE.getErrorCode(), "查询多条记录", null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
        }

    }


}
