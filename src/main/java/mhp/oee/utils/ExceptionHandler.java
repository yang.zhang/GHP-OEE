package mhp.oee.utils;

import org.springframework.dao.DataAccessException;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;

public class ExceptionHandler {

    public static BusinessException handleRuntimeException(RuntimeException ex) {
        BusinessException be = null;
        if (ex instanceof DataAccessException) {
            // TODO: change to dedicated BusinessException
            be = new BusinessException(ErrorCodeEnum.BASIC, ex.getMessage(), ex.getCause());
        }

        if (be == null) {
            // TODO: change to dedicated BusinessException
            be = new BusinessException(ErrorCodeEnum.BASIC, "Unconverted Runtime Exception");
        }
        return be;
    }

}
