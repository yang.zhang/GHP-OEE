package mhp.oee.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.UploadFilenameException;
import mhp.oee.common.handle.BOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.vo.ExceptionVO;
import mhp.oee.web.Constants;
import org.slf4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class Utils {

	@InjectableLogger
	private static Logger logger;

	private static Integer index = 1;

	private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

	public static String getNextServerNode() {
		index = index + 1;
		return index.toString();
	}

	public static String getCurrentServerNode() {
		return index.toString();
	}

	public static String getMonoServer() {
		return "Mono";
	}

	public static String time2ToStr(Date date) {
		return new SimpleDateFormat("HH/mm/s").format(date);
	}

	public static Date strToTime2(String str) throws ParseException {
		return new SimpleDateFormat("HH/mm/s").parse(str);
	}

	public static String dateToStr(Date date) {
		return new SimpleDateFormat("MM/dd/yyyy").format(date);
	}

	public static Date strToDate(String str) throws ParseException {
		return new SimpleDateFormat("MM/dd/yyyy").parse(str);
	}

	public static String datetimeMsToStr(Date date) {
		return new SimpleDateFormat("yyyyMMdd.HHmmss.SSS").format(date);
	}

	public static Date strToDatetimeMs(String str) throws ParseException {
		return new SimpleDateFormat("yyyyMMdd.HHmmss.SSS").parse(str);
	}

	public static String reportDateToStr(Date date){
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	public static Date strToReportDate(String str) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd").parse(str);
	}

	public static String reportDateTimeToStr(Date date){
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}

	public static Date strToReportDateTime(String str) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
	}

	public static Date strToDatetime(String str) throws ParseException {
		return new SimpleDateFormat("MM/dd/yyyy-HH/mm/s").parse(str);
	}

	public static String datetimeToStr(Date date) {
		return new SimpleDateFormat("MM/dd/yyyy-HH/mm/s").format(date);
	}

	public static String timeToStr(Date time) {
		return new SimpleDateFormat("HH:mm:ss").format(time);
	}

	public static Date strToTime(String str) throws ParseException {
		return new SimpleDateFormat("HH:mm:ss").parse(str);
	}

	/**
	 * Tokenize the given string into pieces (returned as array of Strings)
	 *
	 * @param input
	 *            The input String
	 * @param delimiters
	 *            A string containing the delimiters
	 * @return An array contain the strings
	 */
	public static Vector<String> tokenize(String input, String delimiters) {
		Vector<String> v = new Vector<String>();
		StringTokenizer t = new StringTokenizer(input, delimiters);

		while (t.hasMoreTokens()) {
			v.addElement(t.nextToken());
		}
		return v;
	}

	/**
	 * Null safe check if string is null or zero-length string
	 *
	 * @param string
	 * @return true if the passed string is either null or ""
	 */
	public static boolean isEmpty(String string) {
		return string == null || "".equals(string);
	}

	/**
	 * Null safe check if collection is null or does not contain any rows
	 *
	 * @param collection
	 *            - Data to check
	 * @return true if the passed string is either null or ""
	 */
	public static boolean isEmpty(Collection<?> collection) {
		return collection == null || collection.size() == 0;
	}

	public static boolean isEmpty(BOHandle handle) {
		return (handle == null) || Utils.isEmpty(handle.getValue());
	}

	public static <T> T copyObjectProperties(Object source, Class<T> targetClass) {
		if (source == null) {
			return null;
		}
		T target = null;
		try {
			target = targetClass.newInstance();
			BeanUtils.copyProperties(source, target);
		} catch (InstantiationException e) {
			logger.error(
					"Utils.copyObjectProperties() : " + e.getMessage() + System.lineSeparator() + getStackTrace(e));
		} catch (IllegalAccessException e) {
			logger.error(
					"Utils.copyObjectProperties() : " + e.getMessage() + System.lineSeparator() + getStackTrace(e));
		}
		return target;
	}

	public static <T> List<T> copyListProperties(List<?> sourceList, Class<T> targetClass) {
		if (CollectionUtils.isEmpty(sourceList)) {
			return Collections.emptyList();
		}
		List<T> targetList = new ArrayList<T>(sourceList.size());
		for (Object source : sourceList) {
			targetList.add(copyObjectProperties(source, targetClass));
		}
		return targetList;
	}

	public static String getStackTrace(Throwable e) {
		ByteArrayOutputStream s = new ByteArrayOutputStream();
		PrintWriter p = new PrintWriter(s);
		e.printStackTrace(p);
		p.close();
		return s.toString();
	}

	public static Date getInfiniteDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(9999, 11, 31);
		return calendar.getTime();
	}

	public static Date getInfiniteDateWithTime() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(9999, 11, 31, 23, 59, 59);
		return calendar.getTime();
	}

	public static Date getDatefromExcel(String value) throws ParseException {
		DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		return df.parse(value);
	}

	public static String getFormattedDateStringfromExcel(String startDate) throws ParseException {
		Date datefromExcel = Utils.getDatefromExcel(startDate);
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		return df.format(datefromExcel);
	}

	public static Date getDatefromString(String value) throws ParseException {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		return df.parse(value);
	}

	public static Date getDateTimefromString(String value) throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.parse(value);
	}

	public static int getOffset(int count, int index) {
		return (index - 1) * count;
	}

	public static Long getDiffTime(Date beginDate, Date endDate) {
		return endDate.getTime() - beginDate.getTime();
	}

	public static Long getDiffMinuteTime(Date beginDate, Date endDate) {
		return (endDate.getTime() - beginDate.getTime()) / (1000 * 60);
	}

	public static boolean compareDate(Date earlyDate, Date lateDate) {
		return earlyDate.getTime() < lateDate.getTime() ? true : false;
	}

	public static double getCycleTime(String ppm) {
		DecimalFormat df = new DecimalFormat();
		double data = 1 / Double.parseDouble(ppm);
		String pattern = "0.000000";
		df.applyPattern(pattern);
		return Double.parseDouble(df.format(data));
	}

	public static void checkUploadFilename(String filename) throws UploadFilenameException {
		String regexp = "^.*xlsx$";
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(filename);
		if (!matcher.find()) {
			throw new UploadFilenameException();
		}
	}

	public static String getStrt(String dateTime) {
		String[] strs = dateTime.split("-");
		String[] date = strs[0].split("/");
		String[] time = strs[1].split("/");
		String result = date[2] + date[0] + date[1] + "." + time[0] + time[1] + time[2];
		return result;
	}

	public static Date getDateFromWeb(String value) throws ParseException {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy-HH/mm/ss");
		return df.parse(value);
	}

	public static String getStringFromWeb(String startDate) throws ParseException {
		Date date = Utils.getDatefromExcel(startDate);
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy-HH/mm/ss");
		return df.format(date);
	}

	public static Date getTimeFromWeb(String value) throws ParseException {
        DateFormat df = new SimpleDateFormat("HH/mm/ss");
        return df.parse(value);
    }

	public static String getCurrentMethodName(int layer) {
		return Thread.currentThread().getStackTrace()[layer + 2].getClassName() + "."
				+ Thread.currentThread().getStackTrace()[layer + 2].getMethodName();
	}

	public static String getTargetDate(String dateTime) throws ParseException {
		new SimpleDateFormat("MM/dd/yyyy").parse(dateTime);
		String[] date = dateTime.split("/");
		String result = date[2] + "-" + date[0] + "-" + date[1];
		return result;
	}

	public static ResponseEntity<?> returnExceptionToFrontEnd(BusinessException e) {
		ExceptionVO vo = new ExceptionVO(e.getErrorCode().getErrorCode(), e.getMessage(), e.getErrorJson());
		logger.error("ReasonCodeAlertController.readReasonCodeAlertHisStatus() : " + e.getMessage()
				+ System.lineSeparator() + Utils.getStackTrace(e));
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.header(Constants.RESPONSE_HEADER_ERROR_MESSAGE, gson.toJson(vo)).body(null);
	}

	public static String string2Unicode(String string) {
		StringBuffer unicode = new StringBuffer();
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			switch (Integer.toHexString(c).length()) {
            case 1:
                unicode.append("\\u" + "000" + Integer.toHexString(c));
                break;
            case 2:
                unicode.append("\\u" + "00" + Integer.toHexString(c));
                break;
            case 3:
                unicode.append("\\u" + "0" + Integer.toHexString(c));
                break;
            default:
                unicode.append("\\u" + Integer.toHexString(c));
                break;
            }
		}
		return unicode.toString();
	}

	public static boolean compareDateToStringWithinSameDay(Date startDate, String startTime, String endTime) throws ParseException{
       String year = (startDate.getYear()+1900)+"";
       String month = (startDate.getMonth()+1)+"";
       String day = (startDate.getDate())+"";
       String startHour = startTime.substring(0,startTime.indexOf(":"));
       String startMinute = startTime.substring(startTime.indexOf(":")+1,startTime.length());
       String endHour = endTime.substring(0,endTime.indexOf(":"));
       String endMinute = endTime.substring(endTime.indexOf(":")+1,endTime.length());
       Date st = getDateTimefromString(year+"-"+month+"-"+day+" "+startHour+":"+startMinute+":00");
       Date et = getDateTimefromString(year+"-"+month+"-"+day+" "+endHour+":"+endMinute+":00");
       if (compareDate(st, startDate)&&compareDate(startDate, et)) {
    	   return true;
       }
       return false;
    }

	public static Date parseDateFromReferenceDate(Date hms,Date referDate) throws ParseException{
	       String year = (referDate.getYear()+1900)+"";
	       String month = (referDate.getMonth()+1)+"";
	       String day = (referDate.getDate())+"";
	       String hour = hms.getHours()+"";
	       String minute = hms.getMinutes()+"";
	       return getDateTimefromString(year+"-"+month+"-"+day+" "+hour+":"+minute+":00");
	}

	public static Date addMinuteToDate(Date originDate, int minute, boolean isAdd){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(originDate);
		if (isAdd) {
			calendar.set(Calendar.MINUTE,calendar.get(Calendar.MINUTE)+minute);
			return calendar.getTime();
		}else {
			calendar.set(Calendar.MINUTE,calendar.get(Calendar.MINUTE)-minute);
			return calendar.getTime();
		}
	}

	public static Date addSecondToDate(Date originDate, double minute, boolean isAdd){
		int sec = (int) (minute*1000);
		if (isAdd) {
			return new Date(originDate.getTime() + sec*60*1000/1000);
		} else {
			return new Date(originDate.getTime() - sec*60*1000/1000);
		}
	}

	public static Date addDayToDate(Date originDate, int day, boolean isAdd){
		if (isAdd) {
			return new Date(originDate.getTime() + day*24*60*60*1000);
		} else {
			return new Date(originDate.getTime() - day*24*60*60*1000);
		}
	}

	public static Date addHourToDate(Date originDate, int hour, boolean isAdd){
		if (isAdd) {
			return new Date(originDate.getTime() + hour*60*60*1000);
		} else {
			return new Date(originDate.getTime() - hour*60*60*1000);
		}
	}


    public static void generateDownloadFile(HttpServletResponse response, File file) throws IOException {
        OutputStream out = response.getOutputStream();
        response.setHeader("Content-disposition", "attachment; filename=" + "download.xlsx");

        byte[] buffer = new byte[8192]; // use bigger if you want
        int length = 0;

        FileInputStream in = new FileInputStream(file);

        while ((length = in.read(buffer)) > 0) {
            out.write(buffer, 0, length);
        }
        in.close();
        out.close();
    }

    public static String parseReportDateDay(String date) throws ParseException {
        try {
            String[] dates = date.split("\\/");
            return dates[2] + "-" + dates[0] + "-" + dates[1];
        } catch (Exception e) {
            throw new ParseException("parse report date from MM/dd/yyyy to yyyy-MM-dd error: " + date, 0);
        }
    }

    public static String getStringFromWebByDate(Date date) {
        DateFormat df = new SimpleDateFormat("yyyyMMdd.HHmmss");
        return df.format(date);
    }

	/**
	 * Join together IN SQL
	 */
	public static String formatInParams(String prefix, String[] params) {
		String inParams = "";
		if (null != params && params.length > 0) {
			inParams = prefix + " IN (%s)";
			StringBuilder sb = new StringBuilder();
			for (String param : params) {
				sb.append("''").append(param).append("'', ");
			}
			String paramString = sb.toString();
			paramString = paramString.substring(0, paramString.length()-2);
			inParams = String.format(inParams, paramString);
		}
		return inParams;
	}

	public static String formatInParams2(String prefix, String[] params) {
		String inParams = "";
		if (null != params && params.length > 0) {
			inParams = prefix + " IN (%s)";
			StringBuilder sb = new StringBuilder();
			for (String param : params) {
				sb.append("'").append(param).append("', ");
			}
			String paramString = sb.toString();
			paramString = paramString.substring(0, paramString.length()-2);
			inParams = String.format(inParams, paramString);
		}
		return inParams;
	}

	// 将类似"resource1,resource2,resource3"这样的字符串转变成"resourceBo1,resourceBo2,resourceBo3"
	public static String formatSplitString(String originalString, String originalSplit, String newSplit, ISplitStringFormat format) {
		String newString = null;
		if (!Utils.isEmpty(originalString)) {
			String[] originalStringArray = originalString.split(originalSplit);
			for (String os : originalStringArray) {
				String ns = format.format(os);
				if (Utils.isEmpty(ns)) continue;
				if (Utils.isEmpty(newString)) newString = "";
				newString += newSplit + ns;
			}
			if (!Utils.isEmpty(newString)) {
				newString = newString.substring(1);
			}
		}
		return newString == null ? (originalString == null ? null : "") : newString;
	}
	 //将数字格式化千分位
	 public static String fmtMicrometer(String text)
	    {
	        DecimalFormat df = null;
	        if(text.indexOf(".") > 0)
	        {
	            if(text.length() - text.indexOf(".")-1 == 0)
	            {
	                df = new DecimalFormat("###,##0.");
	            }else if(text.length() - text.indexOf(".")-1 == 1)
	            {
	                df = new DecimalFormat("###,##0.0");
	            }else
	            {
	                df = new DecimalFormat("###,##0.00");
	            }
	        }else
	        {
	            df = new DecimalFormat("###,##0");
	        }
	        double number = 0.0;
	        try {
	             number = Double.parseDouble(text);
	        } catch (Exception e) {
	            number = 0.0;
	        }
	        return df.format(number);
	    }
	 /**
	  * 字符串以";"分割成数组，前后加上"%"  sql like 语句使用
	  * @author zhengxiaoxia
	  * @param str
	  * @return string[]
	  */
	 public static String[] ConvertStrtoArrayLike(String str) {
		//支持中文“；”
		str = str.replace("；", ";");
		ArrayList a = new ArrayList<String>();
		String[] arrayTmp = str.split(";");
		for (int i = 0; i < arrayTmp.length; i++) {
			String stri = arrayTmp[i];
			if (!"".equals(arrayTmp[i])) {
				stri = "%" + stri + "%";
				a.add(stri);
			}
		}
		//如果为空就查出所有
		if(a.size() == 0) a.add("%%");
		String[] strFinal = (String[]) a.toArray((new String[a.size()]));
		System.out.println(strFinal);
		return strFinal;
	}
	 /**
     * @author zhengxx
     * @param dateStr 字符串的时间格式，如 2016-11-02 09：00：00
     * @param addNum  增加多少时间，减少则为负数
     * @param type	   可选：MINUTE， HOUR，SECOND
     * @return		字符串的时间格式，如 2016-11-02 09：00：00
     * @throws ParseException 时间格式转换错误
     */
     public static String addSomeTimeForTime(String dateStr, int addNum, String type) throws ParseException{
    	Date date =  Utils.strToTime(dateStr);
    	Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if("MINUTE".equals(type)){
        	 cal.add(Calendar.MINUTE,addNum );
        }else if("HOUR".equals(type)){
        	cal.add(Calendar.HOUR_OF_DAY,addNum );
        }else {
        	cal.add(Calendar.SECOND,addNum );

        }
        date = cal.getTime();
		return Utils.timeToStr(date);

     }
     public static String nullOrStr(String str){
     	return str==null?"":str;
     }
}