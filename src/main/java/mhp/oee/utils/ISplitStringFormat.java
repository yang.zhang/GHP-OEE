package mhp.oee.utils;

/**
 * Created by LinZuK on 2016/10/20.
 */
public interface ISplitStringFormat {
    String format(String originalString);
}
