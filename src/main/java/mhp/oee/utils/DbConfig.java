package mhp.oee.utils;

import java.io.IOException;  
import java.io.InputStream;  
import java.util.Properties;  
   
/** 
 * 数据库配置文件读取方法 
 * @author WANGYAN 
 * 
 */  
public class DbConfig {  
       
    private String driver;  
    private String url;  
    private String userName;  
    private String password;  
       
    public DbConfig() {  ///GHP-OEE/src/main/resources/filters/filter-qty.properties
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("config/properties/db-config.properties");  
        Properties p=new Properties();  
        try {  
            p.load(inputStream);  
            this.driver=p.getProperty("db.driver");  
            this.url=p.getProperty("db.url");  
            this.userName=p.getProperty("db.username");  
            this.password=p.getProperty("db.password");  
        } catch (IOException e) {  
            // TODO Auto-generated catch block  
            e.printStackTrace();  
        }  
           
    }  
       
    public String getDriver() {  
        return driver;  
    }  
    public String getUrl() {  
        return url;  
    }  
    public String getUserName() {  
        return userName;  
    }  
    public String getPassword() {  
        return password;  
    }  
       
       
   
}  



