package mhp.oee.dao.extend;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import mhp.oee.po.extend.ResrceAndResourcePlcPO;

public interface ResrceAndResourcePlcPOMapper {
    @Select({ "<script>"
            + "SELECT C.RESOURCE_BO, C.SITE, C.CATEGORY, C.PLC_OBJECT, C.PLC_ADDRESS, C.PLC_VALUE, C.LOG_PATH, C.DESCRIPTION "
            + "FROM RESRCE AS D LEFT JOIN RESOURCE_PLC AS C ON D.HANDLE = C.RESOURCE_BO  WHERE D.RESRCE = #{resource} "
            + "AND D.SITE = #{site} AND C.CATEGORY = #{category}"
            + "<if test=' plcObjec != null '>"
            + "AND C.PLC_OBJECT=#{plcObjec}"
            + "</if>"
            + "</script>"})
    @ResultMap("BaseResultMap")
    ResrceAndResourcePlcPO selectResrceAndResourcePlc(@Param("site")String site
            , @Param("resource")String resource, @Param("category")String category, @Param("plcObjec")String plcObjec);
}
