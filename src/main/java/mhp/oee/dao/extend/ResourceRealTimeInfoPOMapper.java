package mhp.oee.dao.extend;

import mhp.oee.enumClass.LightColor;
import mhp.oee.enumClass.ResourceRealTimeDimension;
import mhp.oee.po.extend.ResourceRealTimeDetailPO;
import mhp.oee.po.extend.ResourceRealTimeInfoPO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by DingLJ on 2017/4/26 026.
 */
public interface ResourceRealTimeInfoPOMapper {



    /**
     * 宏观看板实时查询
     * @param dimension 查询纬度
     * @param siteList 工作列表
     * @param workAreaList 生产区域列表
     * @param lineAreaList 拉线列表
     * @param resourceTypeList 设备类型列表
     * @return
     */
    public List<ResourceRealTimeInfoPO> select(@Param("dimension") ResourceRealTimeDimension dimension,
                                               @Param("siteList") List<String> siteList,
                                               @Param("workAreaList") List<String> workAreaList,
                                               @Param("lineAreaList")  List<String> lineAreaList,
                                               @Param("resourceTypeList") List<String> resourceTypeList);


    /**
     *
     * @param color
     * @param siteList
     * @param workAreaList
     * @param lineAreaList
     * @param resourceTypeList
     * @return
     */
    public List<ResourceRealTimeDetailPO> selectRealTimeDetail(@Param("color") LightColor color,
                                                               @Param("siteList") List<String> siteList,
                                                               @Param("workAreaList") List<String> workAreaList,
                                                               @Param("lineAreaList")  List<String> lineAreaList,
                                                               @Param("resourceTypeList") List<String> resourceTypeList);

}
