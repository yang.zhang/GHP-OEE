package mhp.oee.dao.extend;

import mhp.oee.po.extend.OeeAlarmLinePO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;


/**
 * Created by LinZuK on 2016/9/14.
 */
public interface OeeAlarmLineMapper {

    @Select(" SELECT V.PARAM_ID, V.PARAM_VALUE " +
            " FROM ACTIVITY AS A " +
            " INNER JOIN ACTIVITY_PARAM_VALUE AS V " +
            " ON V.SITE=#{site} " +
            " AND V.ACTIVITY_BO=A.HANDLE " +
            " WHERE A.ACTIVITY='DASHBOARD003' ")
    @Results({
            @Result(column = "PARAM_ID", jdbcType = JdbcType.VARCHAR, property = "paramId"),
            @Result(column = "PARAM_VALUE", jdbcType = JdbcType.VARCHAR, property = "paramValue")
    })
    List<OeeAlarmLinePO> select(@Param("site")String site);

}
