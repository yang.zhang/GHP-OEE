package mhp.oee.dao.extend;

import mhp.oee.po.gen.ReasonCodeRuleParamPO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * Created by LinZuK on 2016/7/28.
 */
public interface ActivityAndReasonCodeRuleAndReasonCodeRuleParamPOMapper {

    @Select("SELECT C.* FROM activity AS A " +
            "INNER JOIN REASON_CODE_RULE AS B ON A.ACTIVITY = B.ACTIVITY " +
            "INNER JOIN REASON_CODE_RULE_PARAM AS C ON C.REASON_CODE_RULE_BO = B.HANDLE " +
            "WHERE A.HANDLE = #{activityBo}")
    @Results({
            @Result(id = true, column = "HANDLE", jdbcType = JdbcType.VARCHAR, property = "handle"),
            @Result(column = "SITE", jdbcType = JdbcType.VARCHAR, property = "site"),
            @Result(column = "REASON_CODE_RULE_BO", jdbcType = JdbcType.VARCHAR, property = "reasonCodeRuleBo"),
            @Result(column = "RESOURCE_TYPE", jdbcType = JdbcType.VARCHAR, property = "resourceType"),
            @Result(column = "PARAM_ID", jdbcType = JdbcType.VARCHAR, property = "paramId"),
            @Result(column = "PARAM_VALUE", jdbcType = JdbcType.VARCHAR, property = "paramValue"),
            @Result(column = "CREATED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "createdDateTime"),
            @Result(column = "MODIFIED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "modifiedDateTime"),
            @Result(column = "MODIFIED_USER", jdbcType = JdbcType.VARCHAR, property = "modifiedUser")
    })
    List<ReasonCodeRuleParamPO> select(@Param("activityBo") String activityBo);

}
