package mhp.oee.dao.extend;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import mhp.oee.po.extend.ActivityClientPermAndActivityClientPO;

public interface ActivityClientPermAndActivityClientPOMapper {
    @Select("SELECT * FROM ACTIVITY_CLIENT_PERM AS A INNER JOIN ACTIVITY_CLIENT AS B ON A.ACTIVITY_CLIENT_BO = B.HANDLE "
            + "WHERE A.USER_GROUP_BO = #{userGroupBO} AND PERMISSION_SETTING='true' ")
    @ResultMap("BaseResultMap")
    public List<ActivityClientPermAndActivityClientPO> selectActivityClientPermAndActivityClientPO(@Param("userGroupBO") String userGroupBO);
}
