package mhp.oee.dao.extend;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import mhp.oee.po.extend.ItemExtendPO;
import mhp.oee.po.gen.AlertCommentPO;
import mhp.oee.po.gen.ItemPO;

public interface AlertCommentRemarkMapper {
   @Select({ "<script>"
            + "SELECT DISTINCT HANDLE, SITE, COMMENT_CODE, DESCRIPTION, CREATED_DATE_TIME, MODIFIED_DATE_TIME, MODIFIED_USER FROM ALERT_COMMENT WHERE"
            + " SITE = #{site} AND "
            + " <foreach close=\" )\" collection=\"descriptions\" item=\"arrayItem\" open=\" ( \" separator=\"or\"> "
            + "  DESCRIPTION LIKE #{arrayItem} "
            + " </foreach> "
            + " ORDER BY CREATED_DATE_TIME"
            + "</script>"})
   @Results({
	    @Result( column="HANDLE",jdbcType=JdbcType.VARCHAR,property="handle"),
	    @Result( column="SITE", jdbcType=JdbcType.VARCHAR,property="site" ), 
	    @Result( column="COMMENT_CODE",jdbcType=JdbcType.VARCHAR, property="commentCode" ),
	    @Result( column="DESCRIPTION",jdbcType=JdbcType.VARCHAR , property="description" ),
	    @Result( column="CREATED_DATE_TIME",jdbcType=JdbcType.TIMESTAMP, property="createdDateTime" ), 
	    @Result( column="MODIFIED_DATE_TIME", jdbcType=JdbcType.TIMESTAMP,property="modifiedDateTime" ), 
	    @Result( column="MODIFIED_USER", jdbcType=JdbcType.VARCHAR , property="modifiedUser")
   })
    public List<AlertCommentPO> selectAlertCommentPOLikeMore(@Param("site") String site, @Param("descriptions") String[] descriptions);
}
