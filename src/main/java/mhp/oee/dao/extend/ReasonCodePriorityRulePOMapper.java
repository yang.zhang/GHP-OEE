package mhp.oee.dao.extend;

import mhp.oee.po.extend.ReasonCodePriorityRulePO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * job优化 做表关联查询
 */
public interface ReasonCodePriorityRulePOMapper {



    List<ReasonCodePriorityRulePO> selectPriorityRuleByCondition(@Param("site") String site,@Param("filterDate") Date filterDate, @Param("mark") String mark);


}