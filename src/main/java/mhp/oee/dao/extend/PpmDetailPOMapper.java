package mhp.oee.dao.extend;

import mhp.oee.po.extend.PpmReportPO;
import mhp.oee.po.gen.ActivityParamValuePO;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * Created by SuMingzhi on 2016/11/23.
 */
public interface PpmDetailPOMapper {

    @Select(" SELECT \"RESRCE\", \"DESCRIPTION\", \"WORK_AREA\", \"WORK_AREA_DES\", \"LINE_AREA\", \"LINE_AREA_DES\", \"SHIFT\", \"SHIFT_DES\", \"SHIFT_START\", \"ITEM\", \"PPM\",SUBSTR(\"BY_TIME\",1,10) AS \"BY_TIME\", \"MODEL\", \"ITEM_DES\", \"FIRST_CAL\", \"ASSET\", sum(\"QTY\") AS \"QTY\", sum(\"PPM_THEORY\") AS \"PPM_THEORY\" " +
            "FROM \"_SYS_BIC\".\"com.andon.report.ppm/PPM_RESULTS_CAL_DOWNLOAD\" " +
            "(" +
            "  'PLACEHOLDER' = ('$$model_in$$', '${modelIn}'), " +
            "  'PLACEHOLDER' = ('$$item_in$$', '${itemIn}'), " +
            "  'PLACEHOLDER' = ('$$site_in$$', '${site}'), " +
            "  'PLACEHOLDER' = ('$$resrce_in$$', '${resrceIn}'), " +
            "  'PLACEHOLDER' = ('$$start_date_in$$', '${startDate}'), " +
            "  'PLACEHOLDER' = ('$$end_date_in$$', '${endDate}'), " +
            "  'PLACEHOLDER' = ('$$by_conditiontype$$', '${byConditionType}'), " +
            "  'PLACEHOLDER' = ('$$line_in$$', '${lineIn}'), " +
            "  'PLACEHOLDER' = ('$$work_center_in$$', '${workCenterIn}'), " +
            "  'PLACEHOLDER' = ('$$activity_param_01_in$$','${va01}'), " +
            "  'PLACEHOLDER' = ('$$activity_param_02_in$$','${va02}'), " +
            "  'PLACEHOLDER' = ('$$activity_param_03_in$$','${va03}'), " +
            "  'PLACEHOLDER' = ('$$activity_param_04_in$$','${va04}'), " +
            "  'PLACEHOLDER' = ('$$activity_param_05_in$$','${va05}'), " +
            "  'PLACEHOLDER' = ('$$shift_in$$','${shift}') " +
            ") GROUP BY \"RESRCE\", \"DESCRIPTION\", \"WORK_AREA\", \"WORK_AREA_DES\", \"LINE_AREA\", \"LINE_AREA_DES\", \"SHIFT\", \"SHIFT_DES\", \"SHIFT_START\", \"ITEM\", \"PPM\", \"BY_TIME\", \"MODEL\", \"ITEM_DES\", \"FIRST_CAL\", \"ASSET\" " +
            " ORDER BY WORK_AREA_DES, LINE_AREA_DES,RESRCE, ITEM, BY_TIME, SHIFT_START ")
    @Results({
            @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resrce"),
            @Result(column = "DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "description"),
            @Result(column = "WORK_AREA", jdbcType = JdbcType.VARCHAR, property = "workArea"),
            @Result(column = "WORK_AREA_DES", jdbcType = JdbcType.VARCHAR, property = "workAreaDes"),
            @Result(column = "LINE_AREA", jdbcType = JdbcType.VARCHAR, property = "lineArea"),
            @Result(column = "LINE_AREA_DES", jdbcType = JdbcType.DECIMAL, property = "lineAreaDes"),
            @Result(column = "SHIFT", jdbcType = JdbcType.DECIMAL, property = "shift"),
            @Result(column = "SHIFT_DES", jdbcType = JdbcType.DECIMAL, property = "shiftDes"),
            @Result(column = "ITEM", jdbcType = JdbcType.DECIMAL, property = "item"),
            @Result(column = "PPM", jdbcType = JdbcType.DECIMAL, property = "ppm"),
            @Result(column = "BY_TIME", jdbcType = JdbcType.DECIMAL, property = "byTime"),
            @Result(column = "MODEL", jdbcType = JdbcType.DECIMAL, property = "model"),
            @Result(column = "ITEM_DES", jdbcType = JdbcType.DECIMAL, property = "itemDes"),
            @Result(column = "FIRST_CAL", jdbcType = JdbcType.DECIMAL, property = "firstCal"),
            @Result(column = "ASSET", jdbcType = JdbcType.DECIMAL, property = "asset"),
            @Result(column = "QTY", jdbcType = JdbcType.DECIMAL, property = "qty"),
            @Result(column = "PPM_THEORY", jdbcType = JdbcType.DECIMAL, property = "ppmTheory")
    })
    List<PpmReportPO> selectPpm(@Param("modelIn") String model,
                                @Param("itemIn") String itemIn,
                                @Param("site") String site,
                                @Param("resrceIn") String resrceIn,
                                @Param("startDate") String startDate,
                                @Param("endDate") String endDate,
                                @Param("byConditionType") String byConditionType,
                                @Param("lineIn") String lineIn,
                                @Param("workCenterIn") String workCenterIn,
                                @Param("va01") String va01,
                                @Param("va02") String va02,
                                @Param("va03") String va03,
                                @Param("va04") String va04,
                                @Param("va05") String va05, 
    							@Param("shift") String shift); 
    @Select("SELECT PARAM_ID,PARAM_VALUE FROM ACTIVITY AS A " +
            "INNER JOIN ACTIVITY_PARAM_VALUE AS B " +
            "ON A.HANDLE = B.ACTIVITY_BO " +
            "WHERE A.HANDLE = 'ActivityBO:EQUATION0001' " +
            "AND A.EXECUTION_TYPE = 'E' " +
            "AND A.ENABLED = 'true' " +
            "AND B.SITE = #{site} " + 
            "ORDER BY B.PARAM_ID")
    @Results({
        @Result(id = true, column = "PARAM_ID", jdbcType = JdbcType.VARCHAR, property = "paramId"),
        @Result(id = true, column = "PARAM_VALUE", jdbcType = JdbcType.VARCHAR, property = "paramValue")
    })       
    List<ActivityParamValuePO> selectParamValue(@Param("site")String site);


    @Select("SELECT " +
            "B.PARAM_ID AS PARAM_ID," +
            "B.PARAM_DEFAULT_VALUE AS PARAM_VALUE " +
            "FROM ACTIVITY AS A " +
            "INNER JOIN ACTIVITY_PARAM AS B " +
            "ON A.HANDLE = B.ACTIVITY_BO WHERE A.HANDLE = 'ActivityBO:EQUATION0001' AND A.EXECUTION_TYPE = 'E' AND A.ENABLED = 'true' ORDER BY B.PARAM_ID ")
    @Results({
            @Result(id = true, column = "PARAM_ID", jdbcType = JdbcType.VARCHAR, property = "paramId"),
            @Result(id = true, column = "PARAM_VALUE", jdbcType = JdbcType.VARCHAR, property = "paramValue")
    })
    List<ActivityParamValuePO> selectDefaultParamValue();

}
