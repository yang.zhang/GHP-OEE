package mhp.oee.dao.extend;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;

import mhp.oee.po.extend.ResrceAndWorkCenterAndWorkCenterShiftPO;

public interface ResrceAndWorkCenterAndWorkCenterShiftPOMapper {
    @ResultMap("BaseResultMap")
    List<ResrceAndWorkCenterAndWorkCenterShiftPO> selectResrceAndWorkCenterAndWorkCenterShift(
            @Param("site") String site, 
            @Param("resrce") String resrce, 
            @Param("enabled") String enabled);
}
