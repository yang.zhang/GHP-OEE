package mhp.oee.dao.extend;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import mhp.oee.po.extend.StdCycleTimeAndResrceAndResourceTypeResourcePO;


public interface StdCycleTimeAndResrceAndResourceTypeResourcePOMapper {

    public List<StdCycleTimeAndResrceAndResourceTypeResourcePO> getStdCycleTimeAndResrceAndResourceTypeResource(
            @Param("site") String site,
            @Param("item") String item,
            @Param("resourceTypeBo") String resourceTypeBo,
            @Param("line") String line,
            @Param("workCenter") String workCenter,
            @Param("date") Date Date,
            @Param("limit") int limit,
            @Param("offset") int offset);
    			   
    int getStdCycleTimeAndResrceAndResourceTypeResourceCount(
            @Param("site") String site,
            @Param("item") String item,
            @Param("resourceTypeBo") String resourceTypeBo,
            @Param("line") String line,
            @Param("workCenter") String workCenter,
            @Param("date") Date Date);

	public List<StdCycleTimeAndResrceAndResourceTypeResourcePO> changePPM(
			@Param("site") String site,
			@Param("item") String item,
			@Param("resourceTypeBo") String resourceTypeBo, 
			@Param("line") String line, 
			@Param("workCenter") String workCenter, 
			@Param("date") Date date);

}
