package mhp.oee.dao.extend;

import mhp.oee.po.extend.ActivityAndActivityParamAndActivityParamValuePO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * Created by LinZuK on 2016/7/25.
 */
public interface ActivityAndActivityParamAndActivityParamValuePOMapper {
    @Select(" SELECT C.HANDLE AS ACTIVITY_PARAM_VALUE_HANDLE, A.HANDLE AS ACTIVITY_BO, A.DESCRIPTION AS ACTIVITY_DESC, " +
            " B.PARAM_ID, B.PARAM_DESCRIPTION, B.PARAM_DEFAULT_VALUE, C.PARAM_VALUE, " +
            " C.MODIFIED_DATE_TIME AS ACTIVITY_PARAM_VALUE_MODIFIED_DATE_TIME " +
            " FROM ACTIVITY AS A " +
            " LEFT JOIN ACTIVITY_PARAM AS B ON B.ACTIVITY_BO = A.HANDLE " +
            " LEFT JOIN ACTIVITY_PARAM_VALUE AS C " +
            " ON C.ACTIVITY_BO = B.ACTIVITY_BO AND C.PARAM_ID = B.PARAM_ID AND C.SITE = #{site} " +
            " WHERE A.ACTIVITY = #{activity} ")
    @Results({
            @Result(id = true, column = "HANDLE", jdbcType = JdbcType.VARCHAR, property = "handle"),
            @Result(column = "ACTIVITY_PARAM_VALUE_HANDLE", jdbcType = JdbcType.VARCHAR, property = "activityParamValueHandle"),
            @Result(column = "ACTIVITY_BO", jdbcType = JdbcType.VARCHAR, property = "activityBo"),
            @Result(column = "ACTIVITY_DESC", jdbcType = JdbcType.VARCHAR, property = "activityDesc"),
            @Result(column = "PARAM_ID", jdbcType = JdbcType.VARCHAR, property = "paramId"),
            @Result(column = "PARAM_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "paramDescription"),
            @Result(column = "PARAM_DEFAULT_VALUE", jdbcType = JdbcType.VARCHAR, property = "paramDefaultValue"),
            @Result(column = "PARAM_VALUE", jdbcType = JdbcType.VARCHAR, property = "paramValue"),
            @Result(column = "ACTIVITY_PARAM_VALUE_MODIFIED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "activityParamValueModifiedDateTime")
    })
    List<ActivityAndActivityParamAndActivityParamValuePO> select(@Param("site")String site, @Param("activity")String activity);
}
