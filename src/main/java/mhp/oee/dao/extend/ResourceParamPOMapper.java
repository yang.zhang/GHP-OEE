package mhp.oee.dao.extend;

import mhp.oee.po.extend.ResourceParamMapperPO;
import mhp.oee.po.extend.ResourceParamPO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by DingLJ on 2017/4/17 017.
 */
public interface ResourceParamPOMapper {


    //1.批量插入设备数据 hana 不支持批量
//    public int batchInsertResourceParams(@Param("resourceParamPOList") List<ResourceParamPO> resourceParamPOList);

    public int insert(@Param("site") String site,
                      @Param("resourceType") String resourceType,
                      @Param("resource") String resource,
                      @Param("key") String key,
                      @Param("value") String value,
                      @Param("uploadDatetime") Date uploadDatetime,
                      @Param("dateTime") Date dateTime);

//    public int countResourceParam(String site,String )

    //2.查询设备数据
    public List<ResourceParamPO> selectResourceParams(String workArea, String lineArea, String resourceType, String resource, Date beginDate, Date endDate, List<String> showParamKeys);

    //3.插入工序-参数key映射
    public int insertMapper(ResourceParamMapperPO resourceParamMapperPO);

    //4.查询映射
    public List<ResourceParamMapperPO> selectMappers(@Param("site") String site, @Param("resourceType") String resourceType, @Param("key") String key);


}
