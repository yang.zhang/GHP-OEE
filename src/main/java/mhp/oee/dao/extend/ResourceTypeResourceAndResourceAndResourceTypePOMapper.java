package mhp.oee.dao.extend;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import mhp.oee.po.extend.ResourceTypeResourceAndResourceAndResourceTypePO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.vo.ResourceVO;

public interface ResourceTypeResourceAndResourceAndResourceTypePOMapper {
    @Select({ "<script>"
            + "SELECT * FROM  RESRCE AS A "
            + "<if test = 'resourceTypeBO != null and resourceTypeBO != \"\" '> "
            + "INNER JOIN RESOURCE_TYPE_RESOURCE AS B ON A.HANDLE = B.RESOURCE_BO "
            + "</if>"
            + "WHERE 1=1 "
            + "AND A.CONFIGURED_PLC_INFO='true' "
            + "<if test='resourceTypeBO != null and resourceTypeBO != \"\" ' > "
            + "AND ${resourceTypeBO}"
            + "</if>"
            + "<if test = 'line != null and line != \"\" '>"
            + "AND ${line} "
            + "</if>"
            + "<if test = 'workCenter != null and workCenter != \"\" '>"
            + "AND ${workCenter} "
            + "</if>"
            + "AND A.ENABLED = 'true' "
            + "AND A.SITE = #{site} "
            + "ORDER BY A.RESRCE"
            + "</script>" })
    @ResultMap("ResourceResultMap")
    List<ResourceTypeResourceAndResourceAndResourceTypePO> selectResourceTypeResourceAndResource(@Param("site") String site, @Param("resourceTypeBO") String resourceTypeBO, @Param("line") String line, @Param("workCenter") String workCenter);
    
    @Select({ "<script>"
            + "SELECT * FROM  RESRCE AS A "
            + "<if test = 'resourceTypeBO != null and resourceTypeBO != \"\" '> "
            + "INNER JOIN RESOURCE_TYPE_RESOURCE AS B ON A.HANDLE = B.RESOURCE_BO "
            + "</if>"
            + "WHERE 1=1 "
            + "<if test='resourceTypeBO != null and resourceTypeBO != \"\" ' > "
            + "AND ${resourceTypeBO}"
            + "</if>"
            + "<if test = 'line != null and line != \"\" '>"
            + "AND ${line} "
            + "</if>"
            + "<if test = 'workCenter != null and workCenter != \"\" '>"
            + "AND ${workCenter} "
            + "</if>"
            + "AND A.ENABLED = 'true' "
            + "AND A.SITE = #{site} "
            + "ORDER BY A.RESRCE"
            + "</script>" })
    @ResultMap("ResourceResultMap")
    List<ResourceTypeResourceAndResourceAndResourceTypePO> selectPlcResourceTypeResourceAndResource(@Param("site") String site, @Param("resourceTypeBO") String resourceTypeBO, @Param("line") String line, @Param("workCenter") String workCenter);

    
    @Select({ "<script>"
            + "SELECT A.*, C.DESCRIPTION AS WORK_AREA_DES, D.DESCRIPTION AS LINE_AREA_DES  FROM RESRCE AS A "
            + "INNER JOIN RESOURCE_TYPE_RESOURCE AS RTR ON RTR.RESOURCE_BO=A.HANDLE "
            + "INNER JOIN RESOURCE_TYPE AS RT ON RT.HANDLE=RTR.RESOURCE_TYPE_BO "
            + "LEFT JOIN WORK_CENTER AS C "
            + "ON A.WORK_AREA = C.WORK_CENTER AND A.SITE = C.SITE "
            + "LEFT JOIN WORK_CENTER AS D "
            + "ON A.LINE_AREA = D.WORK_CENTER AND A.SITE = D.SITE "
            + "WHERE A.SITE = #{site} "
            + "AND A.CONFIGURED_PLC_INFO='true' "
            + "<if test = 'resrceType != null and resrceType != \"\" '>"
            + "AND RT.RESOURCE_TYPE = #{resrceType}"
            + "</if>"
            + "<if test = 'resrces != null and resrces != \"\" '>"
			      + " and "
			      + "<foreach close=' )' collection='resrces' item='arrayItem' open=' ( ' separator='or'>"
			      + "  A.DESCRIPTION  like #{arrayItem} "
		 	      + "</foreach>"
            + "</if>"
            + "<if test = 'lineAreaList != null '>"
            + "AND D.WORK_CENTER IN "
            + "<foreach close=')' collection='lineAreaList' item='list' open='(' separator=','> "
            + "#{list}"
            + "</foreach>"
            + "</if>"
            + "<if test = 'workAreaList != null '>"
            + "AND C.WORK_CENTER IN"
            + "<foreach close=')' collection='workAreaList' item='list' open='(' separator=','> "
            + "#{list}"
            + "</foreach>"
            + "</if>"
            + "ORDER BY A.RESRCE "
            + "</script>"})
    @Results({
            @Result(column = "HANDLE", jdbcType = JdbcType.VARCHAR, property = "handle"),
            @Result(column = "SITE", jdbcType = JdbcType.VARCHAR, property = "site"),
            @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resrce"),
            @Result(column = "DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "description"),
            @Result(column = "ASSET", jdbcType = JdbcType.VARCHAR, property = "asset"),
            @Result(column = "SUB_ASSET", jdbcType = JdbcType.VARCHAR, property = "subAsset"),
            @Result(column = "LINE_AREA", jdbcType = JdbcType.VARCHAR, property = "lineArea"),
            @Result(column = "LINE_AREA_DES", jdbcType = JdbcType.VARCHAR, property = "lineAreaDes"),
            @Result(column = "WORK_AREA", jdbcType = JdbcType.VARCHAR, property = "workArea"),
            @Result(column = "WORK_AREA_DES", jdbcType = JdbcType.VARCHAR, property = "workAreaDes"),
            @Result(column = "WORKSHOP", jdbcType = JdbcType.VARCHAR, property = "workshop"),
            @Result(column = "ENABLED", jdbcType = JdbcType.VARCHAR, property = "enabled"),
            @Result(column = "EQP_USER_ID", jdbcType = JdbcType.VARCHAR, property = "eqpUserId"),
            @Result(column = "CONFIGURED_PLC_INFO", jdbcType = JdbcType.VARCHAR, property = "configuredPlcInfo"),
            @Result(column = "CREATED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "createdDateTime"),
            @Result(column = "MODIFIED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "modifiedDateTime"),
            @Result(column = "MODIFIED_USER", jdbcType = JdbcType.VARCHAR, property = "modifiedUser")
    })
    List<ResourceVO> select(@Param("site") String site,
                            @Param("resrceType") String resrceType, 
                            @Param("resrces") String[] resrces, 
                            @Param("lineAreaList") List<String> lineAreaList,
                            @Param("workAreaList") List<String> workAreaList);

    @Select({ "<script>"
            + "SELECT A.*, C.DESCRIPTION AS WORK_AREA_DES, D.DESCRIPTION AS LINE_AREA_DES  FROM RESRCE AS A "
            + "LEFT JOIN WORK_CENTER AS C "
            + "ON A.WORK_AREA = C.WORK_CENTER AND A.SITE = C.SITE "
            + "LEFT JOIN WORK_CENTER AS D "
            + "ON A.LINE_AREA = D.WORK_CENTER AND A.SITE = D.SITE "
            + "WHERE A.SITE = #{site} "
            + "<if test = 'resrce != null and resrce != \"\" '>"
            + "AND A.RESRCE = #{resrce}"
            + "</if>"
            + "<if test = 'lineAreaList != null '>"
            + "AND D.WORK_CENTER IN "
            + "<foreach close=')' collection='lineAreaList' item='list' open='(' separator=','> "
            + "#{list}"
            + "</foreach>"
            + "</if>"
            + "<if test = 'workAreaList != null '>"
            + "AND C.WORK_CENTER IN"
            + "<foreach close=')' collection='workAreaList' item='list' open='(' separator=','> "
            + "#{list}"
            + "</foreach>"
            + "</if>"
            + "ORDER BY A.DESCRIPTION "
            + "</script>"})
    @Results({
            @Result(column = "HANDLE", jdbcType = JdbcType.VARCHAR, property = "handle"),
            @Result(column = "SITE", jdbcType = JdbcType.VARCHAR, property = "site"),
            @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resrce"),
            @Result(column = "DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "description"),
            @Result(column = "ASSET", jdbcType = JdbcType.VARCHAR, property = "asset"),
            @Result(column = "SUB_ASSET", jdbcType = JdbcType.VARCHAR, property = "subAsset"),
            @Result(column = "LINE_AREA", jdbcType = JdbcType.VARCHAR, property = "lineArea"),
            @Result(column = "LINE_AREA_DES", jdbcType = JdbcType.VARCHAR, property = "lineAreaDes"),
            @Result(column = "WORK_AREA", jdbcType = JdbcType.VARCHAR, property = "workArea"),
            @Result(column = "WORK_AREA_DES", jdbcType = JdbcType.VARCHAR, property = "workAreaDes"),
            @Result(column = "WORKSHOP", jdbcType = JdbcType.VARCHAR, property = "workshop"),
            @Result(column = "ENABLED", jdbcType = JdbcType.VARCHAR, property = "enabled"),
            @Result(column = "EQP_USER_ID", jdbcType = JdbcType.VARCHAR, property = "eqpUserId"),
            @Result(column = "CONFIGURED_PLC_INFO", jdbcType = JdbcType.VARCHAR, property = "configuredPlcInfo"),
            @Result(column = "CREATED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "createdDateTime"),
            @Result(column = "MODIFIED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "modifiedDateTime"),
            @Result(column = "MODIFIED_USER", jdbcType = JdbcType.VARCHAR, property = "modifiedUser")
    })
    List<ResourceVO> selectOeeResrce(@Param("site") String site,
                            @Param("resrce") String resrce, @Param("lineAreaList") List<String> lineAreaList,
                            @Param("workAreaList") List<String> workAreaList);

    @Select({ "<script>"
            + "SELECT A.*,D.DESCRIPTION AS lineAreaDes FROM  RESRCE AS A INNER JOIN WORK_CENTER AS D ON A.LINE_AREA = D.WORK_CENTER AND A.SITE = D.SITE "
            + "<if test = 'resourceTypeBO != null and resourceTypeBO != \"\" '> "
            + "INNER JOIN RESOURCE_TYPE_RESOURCE AS B ON A.HANDLE = B.RESOURCE_BO "
            + "</if>"
            + "WHERE 1=1"
            + "<if test='resourceTypeBO != null and resourceTypeBO != \"\" ' > "
            + "AND ${resourceTypeBO}"
            + "</if>"
            + "<if test = 'line != null and line != \"\" '>"
            + "AND ${line} "
            + "</if>"
            + "<if test = 'workCenter != null and workCenter != \"\" '>"
            + "AND ${workCenter} "
            + "</if>"
            + "AND A.ENABLED = 'true' "
            + "AND A.SITE = #{site} "
            + "ORDER BY A.HANDLE"
            + "</script>" })
    @Results({
        @Result(column = "HANDLE", jdbcType = JdbcType.VARCHAR, property = "handle"),
        @Result(column = "SITE", jdbcType = JdbcType.VARCHAR, property = "site"),
        @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resrce"),
        @Result(column = "DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "description"),
        @Result(column = "ASSET", jdbcType = JdbcType.VARCHAR, property = "asset"),
        @Result(column = "SUB_ASSET", jdbcType = JdbcType.VARCHAR, property = "subAsset"),
        @Result(column = "LINE_AREA", jdbcType = JdbcType.VARCHAR, property = "lineArea"),
        @Result(column = "LINE_AREA_DES", jdbcType = JdbcType.VARCHAR, property = "lineAreaDes"),
        @Result(column = "WORK_AREA", jdbcType = JdbcType.VARCHAR, property = "workArea"),
        @Result(column = "WORKSHOP", jdbcType = JdbcType.VARCHAR, property = "workshop"),
        @Result(column = "ENABLED", jdbcType = JdbcType.VARCHAR, property = "enabled"),
        @Result(column = "EQP_USER_ID", jdbcType = JdbcType.VARCHAR, property = "eqpUserId"),
        @Result(column = "CONFIGURED_PLC_INFO", jdbcType = JdbcType.VARCHAR, property = "configuredPlcInfo"),
        @Result(column = "CREATED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "createdDateTime"),
        @Result(column = "MODIFIED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "modifiedDateTime"),
        @Result(column = "MODIFIED_USER", jdbcType = JdbcType.VARCHAR, property = "modifiedUser")
    })
    List<ResourceVO> selectResourceTypeResourceAndResourceAddLineArea(@Param("site") String site, @Param("resourceTypeBO") String resourceTypeBO, @Param("line") String line, @Param("workCenter") String workCenter);
}
