package mhp.oee.dao.extend;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import mhp.oee.po.extend.ActivityPermAndActivityPO;
public interface ActivityPermAndActivityPOMapper {
    
    @Select("SELECT A.*, B.ACTIVITY, B.DESCRIPTION, B.SEQUENCE_ID FROM  ACTIVITY_PERM AS A INNER JOIN ACTIVITY AS B ON A.ACTIVITY_BO = B.HANDLE WHERE B.EXECUTION_TYPE = 'P' "
            + "AND A.USER_GROUP_BO = #{userGroupBO} "
            + "AND A.ACTIVITY_BO = #{activityBO}")
    @ResultMap("BaseResultMap")
    public ActivityPermAndActivityPO selectActivityPermAndActivity(
            @Param("userGroupBO") String userGroupBO,
            @Param("activityBO") String activityBO);
}
