package mhp.oee.dao.extend;

import mhp.oee.po.extend.ProductionOutputFirstAndItemPO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * Created by LinZuK on 2016/8/18.
 */
public interface ProductionOutputFirstItemPOMapper {

    @Select({"<script>" +
            "SELECT " +
            "F.*, " +
            "I.MODEL " +
            "FROM PRODUCTION_OUTPUT_FIRST AS F " +
            "LEFT JOIN ITEM AS I ON F.ITEM=I.ITEM AND F.SITE=I.SITE " +
            "WHERE F.RESRCE=#{resrce} " +
            "AND F.STRT=#{strt} " +
            "AND F.SHIFT=#{shift} " +
            "AND F.ITEM IN " +
            "<foreach open='(' separator=',' close=')' collection='items' item='item' > " +
            "#{item}" +
            "</foreach> " +
            "</script>"})
    @Results({
            @Result(column = "HANDLE", jdbcType = JdbcType.VARCHAR, property = "handle"),
            @Result(column = "SITE", jdbcType = JdbcType.VARCHAR, property = "site"),
            @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resrce"),
            @Result(column = "OPERATION", jdbcType = JdbcType.VARCHAR, property = "operation"),
            @Result(column = "OPERATION_REVISION", jdbcType = JdbcType.VARCHAR, property = "operationRevision"),
            @Result(column = "ITEM", jdbcType = JdbcType.VARCHAR, property = "item"),
            @Result(column = "ITEM_REVISION", jdbcType = JdbcType.VARCHAR, property = "itemRevision"),
            @Result(column = "STRT", jdbcType = JdbcType.VARCHAR, property = "strt"),
            @Result(column = "START_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "startDateTime"),
            @Result(column = "END_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "endDateTime"),
            @Result(column = "SHIFT", jdbcType = JdbcType.VARCHAR, property = "shift"),
            @Result(column = "SHIFT_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "shiftDescription"),
            @Result(column = "QTY_YIELD", jdbcType = JdbcType.DECIMAL, property = "qtyYield"),
            @Result(column = "QTY_SCRAP", jdbcType = JdbcType.DECIMAL, property = "qtyScrap"),
            @Result(column = "QTY_UNCONFIRM", jdbcType = JdbcType.DECIMAL, property = "qtyUnconfirm"),
            @Result(column = "CREATED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "createdDateTime"),
            @Result(column = "MODIFIED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "modifiedDateTime"),
            @Result(column = "MODIFIED_USER", jdbcType = JdbcType.VARCHAR, property = "modifiedUser"),
            @Result(column = "MODEL", jdbcType = JdbcType.VARCHAR, property = "model")
    })
    List<ProductionOutputFirstAndItemPO> select(@Param("resrce") String resrce, @Param("strt") String strt, @Param("shift") String shift, @Param("items") List<String> items);


}
