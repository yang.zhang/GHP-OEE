package mhp.oee.dao.extend;

import mhp.oee.po.extend.ActivityParamAndRuleParamPO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * Created by LinZuK on 2016/7/28.
 */
public interface ActivityParamAndRuleParamPOMapper {

    @Select(" SELECT " +
            " A.HANDLE AS REASON_CODE_RULE_PARAM_BO, " +
            " B.PARAM_ID, " +
            " B.PARAM_DESCRIPTION, " +
            " B.PARAM_DEFAULT_VALUE, " +
            " A.PARAM_VALUE, " +
            " A.MODIFIED_DATE_TIME, " +
            " A.MODIFIED_USER, " +
            " A.RESOURCE_TYPE " +
            " FROM ACTIVITY_PARAM AS B " +
            " INNER JOIN ACTIVITY AS C " +
            " ON C.HANDLE = B.ACTIVITY_BO and C.ACTIVITY = #{activity}" +
            " LEFT JOIN REASON_CODE_RULE_PARAM AS A " +
            " ON A.PARAM_ID = B.PARAM_ID " +
            " AND A.REASON_CODE_RULE_BO = #{ruleHandle} ")
    @Results({
            @Result(column = "REASON_CODE_RULE_PARAM_BO", jdbcType = JdbcType.VARCHAR, property = "reasonCodeRuleParamBo"),
            @Result(column = "PARAM_ID", jdbcType = JdbcType.VARCHAR, property = "paramId"),
            @Result(column = "PARAM_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "paramDescription"),
            @Result(column = "PARAM_DEFAULT_VALUE", jdbcType = JdbcType.VARCHAR, property = "paramDefaultValue"),
            @Result(column = "PARAM_VALUE", jdbcType = JdbcType.VARCHAR, property = "paramValue"),
            @Result(column = "MODIFIED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "modifiedDateTime"),
            @Result(column = "MODIFIED_USER", jdbcType = JdbcType.VARCHAR, property = "modifiedUser"),
            @Result(column = "RESOURCE_TYPE", jdbcType = JdbcType.VARCHAR, property = "resourceType")
    })
    List<ActivityParamAndRuleParamPO> select(@Param("ruleHandle") String ruleHandle, @Param("activity") String activity);
}
