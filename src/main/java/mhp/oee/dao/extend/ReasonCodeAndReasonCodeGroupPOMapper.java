package mhp.oee.dao.extend;

import java.util.List;

import mhp.oee.po.extend.ReasonCodeGroupAndReasonCodePO;
import org.apache.ibatis.annotations.*;

import mhp.oee.po.extend.ReasonCodeAndReasonCodeGroupPO;
import org.apache.ibatis.type.JdbcType;

public interface ReasonCodeAndReasonCodeGroupPOMapper{
    List<ReasonCodeAndReasonCodeGroupPO> selectReasonCodeAndReasonCodeGroup(
            @Param("site")String site, 
            @Param("reasonCodeGroup")String reasonCodeGroup, 
            @Param("enabled")String enabled,
            @Param("limit")int limit,
            @Param("offset")int offset);
    
    @Select({ "<script>"
            + "SELECT A.*,B.REASON_CODE_GROUP,B.DESCRIPTION AS DESCRIPTION_GROUP FROM REASON_CODE AS A "
            + "LEFT JOIN REASON_CODE_GROUP AS B "
            + "ON A.REASON_CODE_GROUP = B.REASON_CODE_GROUP AND A.SITE = B.SITE "
            + "WHERE A.SITE= #{site}  "
            + "AND A.REASON_CODE_GROUP = #{reasonCodeGroup} "
            + " <if test='enabled != null'> "
            + "AND A.ENABLED = #{enabled} "
            + "ORDER BY B.REASON_CODE_GROUP, A.REASON_CODE"
            + "</if> "
            +"</script>"})
    @ResultMap("BaseResultMap")
    List<ReasonCodeAndReasonCodeGroupPO> selectReasonCodeAndReasonCodeGroupAll(
            @Param("site")String site, 
            @Param("reasonCodeGroup")String reasonCodeGroup, 
            @Param("enabled")String enabled);

    int selectReasonCodeAndReasonCodeGroupCount(
            @Param("site")String site, 
            @Param("reasonCodeGroup")String reasonCodeGroup, 
            @Param("enabled")String enabled);

    @Select({ "<script>"
            + "SELECT A.*,B.REASON_CODE_GROUP,B.DESCRIPTION AS DESCRIPTION_GROUP FROM REASON_CODE AS A "
            + "INNER JOIN REASON_CODE_GROUP AS B "
            + "ON A.REASON_CODE_GROUP = B.REASON_CODE_GROUP AND A.SITE = B.SITE "
            + "WHERE A.SITE = #{site}  "
            + "<if test = 'reasonCode != null'>"
            + "AND A.REASON_CODE = #{reasonCode} "
            + "</if>"
            + "AND B.REASON_CODE_GROUP = #{reasonCodeGroup} "
            + "AND A.ENABLED = #{enabled} "
            + "ORDER BY B.REASON_CODE_GROUP, A.REASON_CODE"
            +"</script>"})
    @ResultMap("BaseResultMap")
    List<ReasonCodeAndReasonCodeGroupPO> selectReasonCodeGroup(@Param("site")String site, @Param("reasonCodeGroup")String reasonCodeGroup, @Param("reasonCode")String reasonCode, @Param("enabled")String enabled);


    @Select(" SELECT " +
            " A.HANDLE AS REASON_CODE_BO," +
            " A.REASON_CODE, " +
            " A.DESCRIPTION AS REASON_CODE_DESC, " +
            " A.ENABLED AS REASON_CODE_ENABLED, " +
            " A.REASON_CODE_GROUP AS REASON_CODE_GROUP, " +
            " B.DESCRIPTION AS REASON_CODE_GROUP_DESC, " +
            " B.ENABLED AS REASON_CODE_GROUP_ENABLED " +
            " FROM REASON_CODE AS A " +
            " LEFT JOIN reason_code_group AS B " +
            " ON B.REASON_CODE_GROUP = A.REASON_CODE_GROUP " +
            " WHERE A.REASON_CODE LIKE #{reasonCode} " +
            " AND A.SITE = #{site} " +
            " AND B.SITE = #{site} " +
            " ORDER BY A.REASON_CODE_GROUP, A.REASON_CODE ")
    @Results({
            @Result(column = "REASON_CODE_BO", jdbcType = JdbcType.VARCHAR, property = "reasonCodeBo"),
            @Result(column = "REASON_CODE", jdbcType = JdbcType.VARCHAR, property = "reasonCode"),
            @Result(column = "REASON_CODE_DESC", jdbcType = JdbcType.VARCHAR, property = "reasonCodeDesc"),
            @Result(column = "REASON_CODE_ENABLED", jdbcType = JdbcType.VARCHAR, property = "reasonCodeEnabled"),
            @Result(column = "REASON_CODE_GROUP", jdbcType = JdbcType.VARCHAR, property = "reasonCodeGroup"),
            @Result(column = "REASON_CODE_GROUP_DESC", jdbcType = JdbcType.VARCHAR, property = "reasonCodeGroupDesc"),
            @Result(column = "REASON_CODE_GROUP_ENABLED", jdbcType = JdbcType.VARCHAR, property = "reasonCodeGroupEnabled")
    })
    List<ReasonCodeGroupAndReasonCodePO> selectReasonCodeLike(@Param("site")String site, @Param("reasonCode")String reasonCode);
}
