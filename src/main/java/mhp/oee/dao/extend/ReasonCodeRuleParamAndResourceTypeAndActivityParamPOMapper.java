package mhp.oee.dao.extend;

import mhp.oee.po.extend.FilterParamButtonPO;
import mhp.oee.po.extend.WorkCenterAndWorkCenterMemberPO;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * Created by LinZuK on 2016/7/29.
 */
public interface ReasonCodeRuleParamAndResourceTypeAndActivityParamPOMapper {

    @Select(" SELECT " +
            " A.HANDLE AS REASON_CODE_RULE_PARAM_BO, " +
            " A.REASON_CODE_RULE_BO AS REASON_CODE_RULE_BO_F, " +
            " A.PARAM_ID AS ACTIVITY_F_PARAM_ID, " +
            " B.RESOURCE_TYPE AS ACTIVITY_F_PARAM_RESOURCE_TYPE, " +
            " B.DESCRIPTION AS ACTIVITY_F_PARAM_RESOURCE_TYPE_DESC, " +
            " B.ENABLED AS ACTIVITY_F_PARAM_RESOURCE_TYPE_ENABLED, " +
            " A.PARAM_VALUE AS ACTIVITY_F_PARAM_VALUE, " +
            " A.MODIFIED_DATE_TIME AS ACTIVITY_F_PARAM_MODIFIED_DATE_TIME, " +
            " A.MODIFIED_USER AS ACTIVITY_F_PARAM_MODIFIED_USER " +
            " FROM RESOURCE_TYPE AS B " +
            " LEFT JOIN REASON_CODE_RULE_PARAM AS A " +
            "   ON A.SITE = B.SITE " +
            "   AND A.RESOURCE_TYPE = B.RESOURCE_TYPE " +
            "   AND A.REASON_CODE_RULE_BO = #{reasonCodeRuleBo} " +
            "   AND A.PARAM_ID = #{paramId} " +
            " WHERE B.SITE = #{site} "
            )
    @Results({
            @Result(column = "REASON_CODE_RULE_PARAM_MODIFIED_DATETIME", jdbcType = JdbcType.VARCHAR, property = "reasonCodeRuleParamModifiedDateTime"),
            @Result(column = "REASON_CODE_RULE_PARAM_BO", jdbcType = JdbcType.VARCHAR, property = "reasonCodeRuleParamBo"),
            @Result(column = "REASON_CODE_RULE_BO_F", jdbcType = JdbcType.VARCHAR, property = "reasonCodeRuleBoF"),
            @Result(column = "ACTIVITY_F_PARAM_DEFAULT_VALUE", jdbcType = JdbcType.VARCHAR, property = "activityFParamDefaultValue"),
            @Result(column = "ACTIVITY_F_PARAM_RESOURCE_TYPE", jdbcType = JdbcType.VARCHAR, property = "activityFParamResourceType"),
            @Result(column = "ACTIVITY_F_PARAM_RESOURCE_TYPE_DESC", jdbcType = JdbcType.VARCHAR, property = "activityFParamResourceTypeDesc"),
            @Result(column = "ACTIVITY_F_PARAM_RESOURCE_TYPE_ENABLED", jdbcType = JdbcType.VARCHAR, property = "activityFParamResourceTypeEnabled"),
            @Result(column = "ACTIVITY_F_PARAM_VALUE", jdbcType = JdbcType.VARCHAR, property = "activityFParamValue"),
            @Result(column = "ACTIVITY_F_PARAM_MODIFIED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "activityFParamModifiedDateTime"),
            @Result(column = "ACTIVITY_F_PARAM_MODIFIED_USER", jdbcType = JdbcType.VARCHAR, property = "activityFParamModifiedUser")
    })
    List<FilterParamButtonPO> select(@Param("site") String site, @Param("reasonCodeRuleBo") String reasonCodeRuleBo, @Param("paramId") String paramId);

}
