package mhp.oee.dao.extend;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import mhp.oee.utils.Utils;
import mhp.oee.vo.ResourceStatusDetailVO;
import mhp.oee.vo.ShutdownReasonVO;

/**
 * 
 * @author ZhengXX
 *停机原因拍拉图报表
 */
public interface ShutdownReasonReportPOMapper {
    @Select({ "<script> SELECT BY_TIME,RESOURCE_TYPE_CODE,RESOURCE_TYPE_DESCRIPTION,REASON_CODE,"
    		+ "REASON_CODE_DESCRIPTION,TOTAL_TIME,TOTAL_PROPORTION_NUM,DT_PROPORTION_NUM,SHUTDOWN_TIMES,QTY "
			+" FROM \"_SYS_BIC\".\"com.andon.report.shutdown_reason/SHUTDOWN_REASON_BY_RESOURCETYPE\"("
			+" 'PLACEHOLDER' = ('$$byTimetype$$','${byTimetype}'), "
			+" 'PLACEHOLDER' = ('$$resrceIn$$','${resrceIn}'), "
			+" 'PLACEHOLDER' = ('$$endDate$$','${endDate}'), "
			+" 'PLACEHOLDER' = ('$$siteIn$$','${siteIn}'), "
			+" 'PLACEHOLDER' = ('$$workCenterIn$$','${workCenterIn}'),"
			+" 'PLACEHOLDER' = ('$$startDate$$','${startDate}'), "
			+" 'PLACEHOLDER' = ('$$lineIn$$','${lineIn}'), "
			+" 'PLACEHOLDER' = ('$$shift$$','${shift}'), "
			+" 'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'))"
            + "</script>" })
    @Results({
	            @Result(column = "BY_TIME", jdbcType = JdbcType.VARCHAR, property = "byTime"),
	            @Result(column = "RESOURCE_TYPE_CODE", jdbcType = JdbcType.VARCHAR, property = "resourceTypeCode"),
	            @Result(column = "RESOURCE_TYPE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "resourceTypeDescription"),
	            @Result(column = "REASON_CODE", jdbcType = JdbcType.VARCHAR, property = "reasonCode"),
	            @Result(column = "REASON_CODE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "reasonCodeDescription"),
	            @Result(column = "TOTAL_TIME", jdbcType = JdbcType.DOUBLE, property = "totalTime"),
	            @Result(column = "TOTAL_PROPORTION_NUM", jdbcType = JdbcType.VARCHAR, property = "totaPproportionNum"),
	            @Result(column = "DT_PROPORTION_NUM", jdbcType = JdbcType.VARCHAR, property = "dtProportionNum"),
	            @Result(column = "SHUTDOWN_TIMES", jdbcType = JdbcType.DOUBLE, property = "shutdownTimes")
	          })
    /**
     * 停机原因柏拉图表格数据by resourceType 
     */
    public List<ShutdownReasonVO> getShundownReasonByResrceType(
            @Param("siteIn") String site, 
    		@Param("workCenterIn") String workCenter, 
    		@Param("lineIn") String  lineIn,
    		@Param("resourceType") String  resourceType,
            @Param("resrceIn") String resrceIn,  
    		@Param("startDate") String startDate,  
    		@Param("endDate")String  endDate,
    		@Param("byTimetype")String byTimetype,
    		@Param("shift")String shift);
    
    
    
    @Select({ "<script> SELECT BY_TIME,WORK_CENTER_CODE,WORK_CENTER_DESCRIPTION,REASON_CODE,"
    		+ "REASON_CODE_DESCRIPTION,TOTAL_TIME,TOTAL_PROPORTION_NUM,DT_PROPORTION_NUM,SHUTDOWN_TIMES,QTY "
			+" FROM \"_SYS_BIC\".\"com.andon.report.shutdown_reason/SHUTDOWN_REASON_BY_WORKCENTER\"("
			+" 'PLACEHOLDER' = ('$$byTimetype$$','${byTimetype}'), "
			+" 'PLACEHOLDER' = ('$$resrceIn$$','${resrceIn}'), "
			+" 'PLACEHOLDER' = ('$$endDate$$','${endDate}'), "
			+" 'PLACEHOLDER' = ('$$siteIn$$','${siteIn}'), "
			+" 'PLACEHOLDER' = ('$$workCenterIn$$','${workCenterIn}'),"
			+" 'PLACEHOLDER' = ('$$startDate$$','${startDate}'), "
			+" 'PLACEHOLDER' = ('$$lineIn$$','${lineIn}'), "
			+" 'PLACEHOLDER' = ('$$shift$$','${shift}'), "
			+" 'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'))"
            + "</script>" })
    @Results({
	            @Result(column = "BY_TIME", jdbcType = JdbcType.VARCHAR, property = "byTime"),
	            @Result(column = "WORK_CENTER_CODE", jdbcType = JdbcType.VARCHAR, property = "workCenterCode"),
	            @Result(column = "WORK_CENTER_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "workCenterDescription"),
	            @Result(column = "REASON_CODE", jdbcType = JdbcType.VARCHAR, property = "reasonCode"),
	            @Result(column = "REASON_CODE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "reasonCodeDescription"),
	            @Result(column = "TOTAL_TIME", jdbcType = JdbcType.DOUBLE, property = "totalTime"),
	            @Result(column = "TOTAL_PROPORTION_NUM", jdbcType = JdbcType.VARCHAR, property = "totaPproportionNum"),
	            @Result(column = "DT_PROPORTION_NUM", jdbcType = JdbcType.VARCHAR, property = "dtProportionNum"),
	            @Result(column = "SHUTDOWN_TIMES", jdbcType = JdbcType.DOUBLE, property = "shutdownTimes")
	          })
    /**
     * 停机原因柏拉图表格数据by WorkCenter
     */
    public List<ShutdownReasonVO> getShundownReasonByWorkCenter(
            @Param("siteIn") String site, 
    		@Param("workCenterIn") String workCenter, 
    		@Param("lineIn") String  lineIn,
    		@Param("resourceType") String  resourceType,
            @Param("resrceIn") String resrceIn,  
    		@Param("startDate") String startDate,  
    		@Param("endDate")String  endDate,
    		@Param("byTimetype")String byTimetype,
    		@Param("shift")String shift);
    
    
    @Select({ "<script> SELECT BY_TIME,WORK_CENTER_CODE,WORK_CENTER_DESCRIPTION,LINE_AREA_CODE,LINE_AREA_DESCRIPTION,REASON_CODE,"
    		+ "REASON_CODE_DESCRIPTION,TOTAL_TIME,TOTAL_PROPORTION_NUM,DT_PROPORTION_NUM,SHUTDOWN_TIMES,QTY "
			+" FROM \"_SYS_BIC\".\"com.andon.report.shutdown_reason/SHUTDOWN_REASON_BY_LINEAREA\"("
			+" 'PLACEHOLDER' = ('$$byTimetype$$','${byTimetype}'), "
			+" 'PLACEHOLDER' = ('$$resrceIn$$','${resrceIn}'), "
			+" 'PLACEHOLDER' = ('$$endDate$$','${endDate}'), "
			+" 'PLACEHOLDER' = ('$$siteIn$$','${siteIn}'), "
			+" 'PLACEHOLDER' = ('$$workCenterIn$$','${workCenterIn}'),"
			+" 'PLACEHOLDER' = ('$$startDate$$','${startDate}'), "
			+" 'PLACEHOLDER' = ('$$lineIn$$','${lineIn}'), "
			+" 'PLACEHOLDER' = ('$$shift$$','${shift}'), "
			+" 'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'))"
            + "</script>" })
    @Results({
	            @Result(column = "BY_TIME", jdbcType = JdbcType.VARCHAR, property = "byTime"),
	            @Result(column = "WORK_CENTER_CODE", jdbcType = JdbcType.VARCHAR, property = "workCenterCode"),
	            @Result(column = "WORK_CENTER_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "workCenterDescription"),
	            @Result(column = "LINE_AREA_CODE", jdbcType = JdbcType.VARCHAR, property = "lineAreaCode"),
	            @Result(column = "LINE_AREA_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "lineAreaDescription"),
	            @Result(column = "REASON_CODE", jdbcType = JdbcType.VARCHAR, property = "reasonCode"),
	            @Result(column = "REASON_CODE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "reasonCodeDescription"),
	            @Result(column = "TOTAL_TIME", jdbcType = JdbcType.DOUBLE, property = "totalTime"),
	            @Result(column = "TOTAL_PROPORTION_NUM", jdbcType = JdbcType.VARCHAR, property = "totaPproportionNum"),
	            @Result(column = "DT_PROPORTION_NUM", jdbcType = JdbcType.VARCHAR, property = "dtProportionNum"),
	            @Result(column = "SHUTDOWN_TIMES", jdbcType = JdbcType.DOUBLE, property = "shutdownTimes")
	          })
    /**
     * 停机原因柏拉图表格数据by LineArea
     */
    public List<ShutdownReasonVO> getShundownReasonByWorkArea(
            @Param("siteIn") String site, 
    		@Param("workCenterIn") String workCenter, 
    		@Param("lineIn") String  lineIn,
    		@Param("resourceType") String  resourceType,
            @Param("resrceIn") String resrceIn,  
    		@Param("startDate") String startDate,  
    		@Param("endDate")String  endDate,
    		@Param("byTimetype")String byTimetype,
    		@Param("shift")String shift);
    
    
    @Select({ "<script> SELECT BY_TIME,WORK_CENTER_CODE,WORK_CENTER_DESCRIPTION,LINE_AREA_CODE,LINE_AREA_DESCRIPTION,ASSET,RESRCE_CODE,RESRCE_DESCRIPTION,REASON_CODE,"
    		+ "REASON_CODE_DESCRIPTION,TOTAL_TIME,TOTAL_PROPORTION_NUM,DT_PROPORTION_NUM,SHUTDOWN_TIMES,QTY "
			+" FROM \"_SYS_BIC\".\"com.andon.report.shutdown_reason/SHUTDOWN_REASON_BY_RESOURCE\"("
			+" 'PLACEHOLDER' = ('$$byTimetype$$','${byTimetype}'), "
			+" 'PLACEHOLDER' = ('$$resrceIn$$','${resrceIn}'), "
			+" 'PLACEHOLDER' = ('$$endDate$$','${endDate}'), "
			+" 'PLACEHOLDER' = ('$$siteIn$$','${siteIn}'), "
			+" 'PLACEHOLDER' = ('$$workCenterIn$$','${workCenterIn}'),"
			+" 'PLACEHOLDER' = ('$$startDate$$','${startDate}'), "
			+" 'PLACEHOLDER' = ('$$lineIn$$','${lineIn}'), "
			+" 'PLACEHOLDER' = ('$$shift$$','${shift}'), "
			+" 'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'))"
            + "</script>" })
    @Results({
	            @Result(column = "BY_TIME", jdbcType = JdbcType.VARCHAR, property = "byTime"),
	            @Result(column = "WORK_CENTER_CODE", jdbcType = JdbcType.VARCHAR, property = "workCenterCode"),
	            @Result(column = "WORK_CENTER_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "workCenterDescription"),
	            @Result(column = "LINE_AREA_CODE", jdbcType = JdbcType.VARCHAR, property = "lineAreaCode"),
	            @Result(column = "LINE_AREA_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "lineAreaDescription"),
	            @Result(column = "ASSET", jdbcType = JdbcType.VARCHAR, property = "asset"),
	            @Result(column = "RESRCE_CODE", jdbcType = JdbcType.VARCHAR, property = "resrceCode"),
	            @Result(column = "RESRCE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "resrceDescription"),
	            @Result(column = "REASON_CODE", jdbcType = JdbcType.VARCHAR, property = "reasonCode"),
	            @Result(column = "REASON_CODE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "reasonCodeDescription"),
	            @Result(column = "TOTAL_TIME", jdbcType = JdbcType.DOUBLE, property = "totalTime"),
	            @Result(column = "TOTAL_PROPORTION_NUM", jdbcType = JdbcType.VARCHAR, property = "totaPproportionNum"),
	            @Result(column = "DT_PROPORTION_NUM", jdbcType = JdbcType.VARCHAR, property = "dtProportionNum"),
	            @Result(column = "SHUTDOWN_TIMES", jdbcType = JdbcType.DOUBLE, property = "shutdownTimes")
	          })
    /**
     * 停机原因柏拉图表格数据by RESRCE
     */
    public List<ShutdownReasonVO> getShundownReasonByResrce(
            @Param("siteIn") String site, 
    		@Param("workCenterIn") String workCenter, 
    		@Param("lineIn") String  lineIn,
    		@Param("resourceType") String  resourceType,
            @Param("resrceIn") String resrceIn,  
    		@Param("startDate") String startDate,  
    		@Param("endDate")String  endDate,
    		@Param("byTimetype")String byTimetype,
    		@Param("shift")String shift);
    
    
    @Select({ "<script> SELECT REASON_CODE,REASON_CODE_DESCRIPTION,"
			+" TOTAL_TIME,TOTAL_PROPORTION_NUM,DT_PROPORTION_NUM,QTY" 
			+" FROM \"_SYS_BIC\".\"com.andon.report.shutdown_reason/SHUTDOWN_REASON_FOR_CHART\"("
			+" 'PLACEHOLDER' = ('$$byTimetype$$','${byTimetype}'), "
			+" 'PLACEHOLDER' = ('$$resrceIn$$','${resrceIn}'), "
			+" 'PLACEHOLDER' = ('$$endDate$$','${endDate}'), "
			+" 'PLACEHOLDER' = ('$$siteIn$$','${siteIn}'), "
			+" 'PLACEHOLDER' = ('$$workCenterIn$$','${workCenterIn}'),"
			+" 'PLACEHOLDER' = ('$$startDate$$','${startDate}'), "
			+" 'PLACEHOLDER' = ('$$lineIn$$','${lineIn}'), "
			+" 'PLACEHOLDER' = ('$$shift$$','${shift}'), "
			+" 'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'))"
			+" ORDER BY TOTAL_TIME DESC"
            + "</script>" })
    @Results({
	            @Result(column = "REASON_CODE", jdbcType = JdbcType.VARCHAR, property = "reasonCode"),
	            @Result(column = "REASON_CODE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "reasonCodeDescription"),
	            @Result(column = "TOTAL_TIME", jdbcType = JdbcType.DOUBLE, property = "totalTime"),
	            @Result(column = "TOTAL_PROPORTION_NUM", jdbcType = JdbcType.DOUBLE, property = "totaPproportionNum"),
	            @Result(column = "DT_PROPORTION_NUM", jdbcType = JdbcType.DOUBLE, property = "dtProportionNum"),
	          })
    /**
     * 停机原因柏拉图图表数据
     */
    public List<ShutdownReasonVO> getShundownReasonForChart(
            @Param("siteIn") String site, 
    		@Param("workCenterIn") String workCenter, 
    		@Param("lineIn") String  lineIn,
    		@Param("resourceType") String  resourceType,
            @Param("resrceIn") String resrceIn,  
    		@Param("startDate") String startDate,  
    		@Param("endDate")String  endDate,
    		@Param("byTimetype")String byTimetype,
    		@Param("shift")String shift);
    
 
}
