package mhp.oee.dao.extend;

import mhp.oee.po.gen.ReasonCodeRuleParamPO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by LinZuK on 2016/7/28.
 */
public interface ReasonCodeRuleAndReasonCodeRuleParamPOMapper {

    @Select(" SELECT A.* FROM REASON_CODE_RULE AS A " +
            " INNER JOIN REASON_CODE_RULE_PARAM AS B " +
            " ON A.HANDLE = B.REASON_CODE_RULE_BO " +
            " WHERE A.ACTIVITY = #{activity} " +
            " AND B.PARAM_ID = #{paramId} ")
    @ResultMap("BaseResultMap")
    List<ReasonCodeRuleParamPO> select(@Param("activity") String activity, @Param("paramId") String paramId);

}
