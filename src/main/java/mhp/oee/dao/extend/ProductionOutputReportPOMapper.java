package mhp.oee.dao.extend;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import mhp.oee.vo.ProductionOutputFirstAndFollowUpConditionVO;
import mhp.oee.vo.ProductionOutputFirstAndFollowUpVO;

public interface ProductionOutputReportPOMapper {
    @Select("SELECT *, (QTY_YIELD + QTY_SCRAP) AS QTY_SUM, "
            + "CASE WHEN (QTY_YIELD + QTY_SCRAP) != 0 THEN ROUND((QTY_YIELD/(QTY_YIELD + QTY_SCRAP))*100,2) || '%' "
            + "ELSE null END AS FIRST_CAL_STRT FROM ("
            + "SELECT ${vo.param},"
            + "sum(QTY_YIELD) AS QTY_YIELD,"
            + "sum(QTY_SCRAP) AS QTY_SCRAP "
            + "FROM \"_SYS_BIC\".\"com.andon.report.production_output/PRODUCTION_OUTPUT_RESULTS_CAL\" "
            + "('PLACEHOLDER' = ('$$model_in$$','${vo.model}'), "
            + "'PLACEHOLDER' = ('$$item_in$$','${vo.item}'), "
            + "'PLACEHOLDER' = ('$$site_in$$','${vo.site}'), "
            + "'PLACEHOLDER' = ('$$resrce_in$$','${vo.resrce}'), "
            + "'PLACEHOLDER' = ('$$start_date_in$$','${vo.startDateTime}'), "
            + "'PLACEHOLDER' = ('$$end_date_in$$', '${vo.endDateTime}'), "
            + "'PLACEHOLDER' = ('$$by_conditiontype$$', '${vo.byConditionType}'), "
            + "'PLACEHOLDER' = ('$$line_in$$', '${vo.lineArea}'), "
            + "'PLACEHOLDER' = ('$$by_timetype$$', '${vo.byTimeType}'), "
            + "'PLACEHOLDER' = ('$$work_center_in$$', '${vo.workArea}')) "
            + "GROUP BY ${vo.param}"
            + "ORDER BY ${vo.param}"
            + ")")
    @Results({
        @Result(column = "WORK_AREA", jdbcType = JdbcType.VARCHAR, property = "workArea"),
        @Result(column = "WORK_AREA_DES", jdbcType = JdbcType.VARCHAR, property = "workAreaDes"),
        @Result(column = "LINE_AREA", jdbcType = JdbcType.VARCHAR, property = "lineArea"),
        @Result(column = "LINE_AREA_DES", jdbcType = JdbcType.VARCHAR, property = "lineAreaDes"),
        @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resrce"),
        @Result(column = "RESRCE_DES", jdbcType = JdbcType.VARCHAR, property = "resrceDes"),
        @Result(column = "MODEL", jdbcType = JdbcType.VARCHAR, property = "model"),
        @Result(column = "ITEM", jdbcType = JdbcType.VARCHAR, property = "item"),
        @Result(column = "ITEM_DES", jdbcType = JdbcType.VARCHAR, property = "itemDes"),
        @Result(column = "BY_TIME", jdbcType = JdbcType.VARCHAR, property = "byTime"),
        @Result(column = "QTY_SUM", jdbcType = JdbcType.DECIMAL, property = "qtySum"),
        @Result(column = "QTY_YIELD", jdbcType = JdbcType.DECIMAL, property = "qtyYield"),
        @Result(column = "QTY_SCRAP", jdbcType = JdbcType.DECIMAL, property = "qtyScrap"),
        @Result(column = "FIRST_CAL_STRT", jdbcType = JdbcType.NVARCHAR, property = "firstCal")
    })
    List<ProductionOutputFirstAndFollowUpVO> selectProductionOutputReport(@Param("vo") ProductionOutputFirstAndFollowUpConditionVO vo);
  
}
