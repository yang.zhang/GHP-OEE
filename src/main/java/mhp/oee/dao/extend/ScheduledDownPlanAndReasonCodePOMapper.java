package mhp.oee.dao.extend;

import mhp.oee.po.extend.ResourceAndWorkCenterShiftAndWorkCenterPO;
import mhp.oee.po.gen.ScheduledDownPlanPO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.Date;
import java.util.List;

/**
 * Created by LinZuK on 2016/8/24.
 */
public interface ScheduledDownPlanAndReasonCodePOMapper {

    @Select(" SELECT S.* FROM SCHEDULED_DOWN_PLAN AS S " +
            " INNER JOIN REASON_CODE AS R " +
            " ON R.REASON_CODE = S.REASON_CODE " +
            " AND R.SITE = S.SITE " +
            " WHERE S.RESOURCE_BO = #{resrceBO} " +
            " AND S.START_DATE_TIME <= #{finishTime} " +
            " AND S.END_DATE_TIME > #{beginTime} " +
            " AND R.HANDLE = #{reasonCodeBO} ")
    @Results({
            @Result(column = "HANDLE", jdbcType = JdbcType.VARCHAR, property = "handle"),
            @Result(column = "SITE", jdbcType = JdbcType.VARCHAR, property = "site"),
            @Result(column = "RESOURCE_BO", jdbcType = JdbcType.VARCHAR, property = "resourceBo"),
            @Result(column = "STRT", jdbcType = JdbcType.VARCHAR, property = "strt"),
            @Result(column = "START_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "startDateTime"),
            @Result(column = "END_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "endDateTime"),
            @Result(column = "REASON_CODE", jdbcType = JdbcType.VARCHAR, property = "reasonCode"),
            @Result(column = "CREATED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "createdDateTime"),
            @Result(column = "MODIFIED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "modifiedDateTime"),
            @Result(column = "MODIFIED_USER", jdbcType = JdbcType.VARCHAR, property = "modifiedUser")
    })
    List<ScheduledDownPlanPO> select(
            @Param("resrceBO") String resrceBO,
            @Param("finishTime") Date finishTime,
            @Param("beginTime") Date beginTime,
            @Param("reasonCodeBO") String reasonCodeBO);

}
