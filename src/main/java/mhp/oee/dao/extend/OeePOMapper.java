package mhp.oee.dao.extend;

import mhp.oee.po.extend.OeeOeePO;
import mhp.oee.po.extend.OeeReasonCodeGroupPO;
import mhp.oee.po.extend.OeeReasonCodePO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * Created by LinZuK on 2016/8/12.
 */
public interface OeePOMapper {

    @Select(" SELECT \"SHIFT_START_DATE_TIME\", \"BY_TIME\", \"BY_TIME_CHART\", \"A\", \"P\", \"Q\", \"OEE\", \"OEE_VALUE\", count(\"QTY\") AS \"QTY\" " +
            " FROM \"_SYS_BIC\".\"com.andon.report.oee/OEE_OEE_CAL\" " +
            " (" +
            "   'PLACEHOLDER' = ('$$resourceBo$$', '${resourceBo}'), " +
            "   'PLACEHOLDER' = ('$$byTimeType$$', '${byTimeType}'), " +
            "   'PLACEHOLDER' = ('$$site$$', '${site}'), " +
            "   'PLACEHOLDER' = ('$$endDate$$', '${endDate}'), " +
            "   'PLACEHOLDER' = ('$$startDate$$', '${startDate}')" +
            " ) " +
            " GROUP BY \"SHIFT_START_DATE_TIME\", \"BY_TIME\", \"BY_TIME_CHART\", \"A\", \"P\", \"Q\", \"OEE\", \"OEE_VALUE\"" +
            " ORDER BY \"SHIFT_START_DATE_TIME\" ")
    @Results({
            @Result(column = "BY_TIME", jdbcType = JdbcType.VARCHAR, property = "byTime"),
            @Result(column = "BY_TIME_CHART", jdbcType = JdbcType.VARCHAR, property = "byTimeChart"),
            @Result(column = "A", jdbcType = JdbcType.VARCHAR, property = "a"),
            @Result(column = "P", jdbcType = JdbcType.VARCHAR, property = "p"),
            @Result(column = "Q", jdbcType = JdbcType.VARCHAR, property = "q"),
            @Result(column = "OEE", jdbcType = JdbcType.VARCHAR, property = "oee"),
            @Result(column = "OEE_VALUE", jdbcType = JdbcType.DECIMAL, property = "oeeValue"),
            @Result(column = "QTY", jdbcType = JdbcType.DECIMAL, property = "qty")
    })
    List<OeeOeePO> selectOee(@Param("site") String site,
            @Param("resourceBo") String resourceBo,
            @Param("startDate") String startDate,
            @Param("endDate") String endDate, // 2015-07-19 11:10:00
            @Param("byTimeType") String byTimeType);

    @Select(" SELECT " +
            " \"REASON_CODE\", " +
            " \"DESCRIPTION\", " +
            " \"INTERSECTION_TIME_DURATION_SUM\", " +
            " sum(\"QTY\") AS \"QTY\" " +
            " FROM \"_SYS_BIC\".\"com.andon.report.oee/OEE_REASON_CODE_CAL\" " +
            " (" +
            " 'PLACEHOLDER' = ('$$resourceBo$$', '${resourceBo}'), " +
            " 'PLACEHOLDER' = ('$$byTimeType$$', '${byTimeType}'), " +
            " 'PLACEHOLDER' = ('$$site$$', '${site}'), " +
            " 'PLACEHOLDER' = ('$$endDate$$', '${endDate}'), " +
            " 'PLACEHOLDER' = ('$$startDate$$', '${startDate}')" +
            " ) " +
            " GROUP BY \"REASON_CODE\", \"DESCRIPTION\", \"INTERSECTION_TIME_DURATION_SUM\" ORDER BY REASON_CODE")
    @Results({
            @Result(column = "REASON_CODE", jdbcType = JdbcType.VARCHAR, property = "reasonCode"),
            @Result(column = "DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "description"),
            @Result(column = "INTERSECTION_TIME_DURATION_SUM", jdbcType = JdbcType.DECIMAL, property = "intersectionTimeDurationSum"),
            @Result(column = "QTY", jdbcType = JdbcType.DECIMAL, property = "qty")
    })
    List<OeeReasonCodePO> selectReasonCode(
            @Param("site") String site,
            @Param("resourceBo") String resourceBo,
            @Param("startDate") String startDate,
            @Param("endDate") String endDate,
            @Param("byTimeType") String byTimeType);

    @Select(" SELECT " +
            " \"REASON_CODE_GROUP\", " +
            " \"DESCRIPTION\", " +
            " \"INTERSECTION_TIME_DURATION_SUM\", " +
            " sum(\"QTY\") AS \"QTY\" " +
            " FROM \"_SYS_BIC\".\"com.andon.report.oee/OEE_REASON_CODE_GROUP_CAL\" " +
            " (" +
            " 'PLACEHOLDER' = ('$$resourceBo$$', '${resourceBo}'), " +
            " 'PLACEHOLDER' = ('$$byTimeType$$', '${byTimeType}'), " +
            " 'PLACEHOLDER' = ('$$site$$', '${site}'), " +
            " 'PLACEHOLDER' = ('$$endDate$$', '${endDate}'), " +
            " 'PLACEHOLDER' = ('$$startDate$$', '${startDate}')" +
            " ) " +
            " GROUP BY \"REASON_CODE_GROUP\", \"DESCRIPTION\", \"INTERSECTION_TIME_DURATION_SUM\" ")
    @Results({
            @Result(column = "REASON_CODE_GROUP", jdbcType = JdbcType.VARCHAR, property = "reasonCodeGroup"),
            @Result(column = "DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "description"),
            @Result(column = "INTERSECTION_TIME_DURATION_SUM", jdbcType = JdbcType.DECIMAL, property = "intersectionTimeDurationSum"),
            @Result(column = "QTY", jdbcType = JdbcType.DECIMAL, property = "qty")
    })
    List<OeeReasonCodeGroupPO> selectReasonCodeGroup(
            @Param("site") String site,
            @Param("resourceBo") String resourceBo,
            @Param("startDate") String startDate,
            @Param("endDate") String endDate,
            @Param("byTimeType") String byTimeType);
}
