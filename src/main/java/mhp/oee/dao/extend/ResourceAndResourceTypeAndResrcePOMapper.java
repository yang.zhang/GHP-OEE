package mhp.oee.dao.extend;

import mhp.oee.po.gen.ResourcePO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * Created by LinZuK on 2016/8/3.
 */
public interface ResourceAndResourceTypeAndResrcePOMapper {

    @Select({"<script>" +
            " SELECT B.* FROM RESRCE AS B " +
            " LEFT JOIN RESOURCE_TYPE_RESOURCE AS A ON A.RESOURCE_BO = B.HANDLE " +
            " LEFT JOIN RESOURCE_TYPE AS C ON C.HANDLE = A.RESOURCE_TYPE_BO " +
            " WHERE B.ENABLED = 'true' " +
            " AND B.SITE = #{site} " +
            "<if test='resourceType != null and resourceType != \"\" ' > " +
            " AND C.RESOURCE_TYPE = #{resourceType} " +
            "</if>" +
            "<if test='resrce != null and resrce != \"\" ' > " +
            " AND B.RESRCE LIKE #{resrce} " +
            "</if>" +
            "<if test='lineArea != null and lineArea != \"\" ' > " +
            " AND B.LINE_AREA = #{lineArea} " +
            "</if>" +
            "<if test='workArea != null and workArea != \"\" ' > " +
            " AND B.WORK_AREA = #{workArea}" +
            "</if>" +
            " ORDER BY RESRCE " +
            "</script>"})
    @Results({
            @Result(column = "HANDLE", jdbcType = JdbcType.VARCHAR, property = "handle"),
            @Result(column = "SITE", jdbcType = JdbcType.VARCHAR, property = "site"),
            @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resrce"),
            @Result(column = "DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "description"),
            @Result(column = "ASSET", jdbcType = JdbcType.VARCHAR, property = "asset"),
            @Result(column = "SUB_ASSET", jdbcType = JdbcType.VARCHAR, property = "subAsset"),
            @Result(column = "LINE_AREA", jdbcType = JdbcType.VARCHAR, property = "lineArea"),
            @Result(column = "WORK_AREA", jdbcType = JdbcType.VARCHAR, property = "workArea"),
            @Result(column = "WORKSHOP", jdbcType = JdbcType.VARCHAR, property = "workshop"),
            @Result(column = "ENABLED", jdbcType = JdbcType.VARCHAR, property = "enabled"),
            @Result(column = "EQP_USER_ID", jdbcType = JdbcType.VARCHAR, property = "eqpUserId"),
            @Result(column = "CONFIGURED_PLC_INFO", jdbcType = JdbcType.VARCHAR, property = "configuredPlcInfo"),
            @Result(column = "CREATED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "createdDateTime"),
            @Result(column = "MODIFIED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "modifiedDateTime"),
            @Result(column = "MODIFIED_USER", jdbcType = JdbcType.VARCHAR, property = "modifiedUser")
    })
    List<ResourcePO> select(@Param("site") String site, @Param("resourceType") String resourceType,
                            @Param("resrce") String resrce, @Param("lineArea") String lineArea,
                            @Param("workArea") String workArea);

}
