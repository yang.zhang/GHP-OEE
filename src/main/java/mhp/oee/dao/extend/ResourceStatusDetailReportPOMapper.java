package mhp.oee.dao.extend;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;
import mhp.oee.vo.ResourceStatusDetailVO;

public interface ResourceStatusDetailReportPOMapper {
    @Select({ "<script> SELECT STARTDATE,RESRCE,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,ASSET,LINE_AREA,LINE_AREA_DESC,SHIFT,SHIFT_DESC,"
            + " SHIFT_START_DATE_TIME,SHIFT_END_DATE_TIME,ME_USER_ID,USER_ID,"
            + " START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,"
            + " RESOURCE_STATE,RESOURCE_STATE_DESC,ELAPSED_TIME,ITEM,MODEL,USER_BO,LOGIN_DATE_TIME"
            + " FROM \"_SYS_BIC\".\"com.andon.report.resource_status_detail/RESULT_ITEM\""
            + "("
            + "'PLACEHOLDER' = ('$$site$$','${site}'),"
            + "'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'),"
            + "'PLACEHOLDER' = ('$$resrce$$','${resrce}'),"
            + "'PLACEHOLDER' = ('$$shift$$','${shift}'),"
            + "'PLACEHOLDER' = ('$$workArea$$','${workArea}'),"
            + "'PLACEHOLDER' = ('$$lineArea$$','${lineArea}'),"
            + "'PLACEHOLDER' = ('$$startDate$$','${startDate}'),"
            + "'PLACEHOLDER' = ('$$endDate$$','${endDate}'),"
            + "'PLACEHOLDER' = ('$$start_date_time$$','${start_date_time}'),"
            + "'PLACEHOLDER' = ('$$end_date_time$$','${end_date_time}')"
            + ")"
            + " GROUP BY STARTDATE,RESRCE,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,ASSET,LINE_AREA,LINE_AREA_DESC,SHIFT,SHIFT_DESC,"
            + " SHIFT_START_DATE_TIME,SHIFT_END_DATE_TIME,ME_USER_ID,USER_ID,"
            + " START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,"
            + " RESOURCE_STATE,RESOURCE_STATE_DESC,ELAPSED_TIME,ITEM,MODEL,USER_BO,LOGIN_DATE_TIME"
            + " ORDER BY STARTDATE,SHIFT DESC,RESRCE,INTERSECTION_START_TIME,INTERSECTION_END_TIME"
            + "<if test=\"limit != null\">"
            + "<if test=\"offset != null\">"
            +    "limit ${limit} offset ${offset}"
            + "</if>"
            + "<if test=\"offset == null\">"
            +    "limit ${limit}"
            + "</if>"
            + "</if>"
            + "</script>" })
    @Results({
            @Result(column = "STARTDATE", jdbcType = JdbcType.VARCHAR, property = "date"),
            @Result(column = "SHIFT_DESC", jdbcType = JdbcType.VARCHAR, property = "shift"),
            @Result(column = "ME_USER_ID", jdbcType = JdbcType.VARCHAR, property = "meUserId"),
            @Result(column = "USER_ID", jdbcType = JdbcType.VARCHAR, property = "prdUserId"),
            @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resource"),
            @Result(column = "RESRCE_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceDesc"),
            @Result(column = "RESOURCE_TYPE", jdbcType = JdbcType.VARCHAR, property = "resourceType"),
            @Result(column = "RESOURCE_TYPE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "resourceTypeDesc"),
            @Result(column = "ASSET", jdbcType = JdbcType.VARCHAR, property = "asset"),
            @Result(column = "LINE_AREA", jdbcType = JdbcType.VARCHAR, property = "lineArea"),
            @Result(column = "LINE_AREA_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceAddr"),
            @Result(column = "INTERSECTION_START_TIME", jdbcType = JdbcType.VARCHAR, property = "startTime"),
            @Result(column = "INTERSECTION_END_TIME", jdbcType = JdbcType.VARCHAR, property = "endTime"),
            @Result(column = "RESOURCE_STATE_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceState"),
            @Result(column = "INTERSECTION_TIME_DURATION", jdbcType = JdbcType.VARCHAR, property = "duration"),
            @Result(column = "MODEL", jdbcType = JdbcType.TIMESTAMP, property = "model"),
            @Result(column = "ITEM", jdbcType = JdbcType.TIMESTAMP, property = "item") })
    public List<ResourceStatusDetailVO> resultItem(
            @Param("limit") Integer pageSize,
            @Param("offset") Integer currPage,
            @Param("site") String site, 
            @Param("workArea") String workArea,
            @Param("lineArea") String lineArea, 
            @Param("startDate") String startDate, 
            @Param("endDate")String endDate,
            @Param("start_date_time") String start_date_time, 
            @Param("end_date_time")String end_date_time,
            @Param("resourceType") String resourceType, 
            @Param("resrce") String resrce, 
            @Param("shift") String shift);
    
    
    @Select({ "<script> SELECT STARTDATE,SHIFT,SHIFT_DESC,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,"
    		+ "USER_ID,ME_USER_ID,RESRCE,ASSET,LINE_AREA_DESC,"
            + " START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,"
            + " RESOURCE_STATE_DESC,ELAPSED_TIME,ALERT_INFO,ALERT_DATE_TIME,USER_ID,USER_BO,LOGIN_DATE_TIME,RESOURCE_BO,COMMENT_CODE,ALERT_COMMENT_DESCRIPTION"
            + " FROM \"_SYS_BIC\".\"com.andon.report.resource_status_detail/RESULT_ALERT\""
            + "("
            + "'PLACEHOLDER' = ('$$site$$','${site}'),"
            + "'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'),"
            + "'PLACEHOLDER' = ('$$resrce$$','${resrce}'),"
            + "'PLACEHOLDER' = ('$$shift$$','${shift}'),"
            + "'PLACEHOLDER' = ('$$comment_code_in$$', '${commentCode}'),"
            + "'PLACEHOLDER' = ('$$workArea$$','${workArea}'),"
            + "'PLACEHOLDER' = ('$$lineArea$$','${lineArea}'),"
            + "'PLACEHOLDER' = ('$$startDate$$','${startDate}'),"
            + "'PLACEHOLDER' = ('$$endDate$$','${endDate}'),"
            + "'PLACEHOLDER' = ('$$start_date_time$$','${start_date_time}'),"
            + "'PLACEHOLDER' = ('$$end_date_time$$','${end_date_time}')"
            + ")"
            + " GROUP BY STARTDATE,SHIFT,SHIFT_DESC,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,USER_ID,ME_USER_ID,RESRCE,ASSET,LINE_AREA_DESC,"
            + " START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,"
            + "RESOURCE_STATE_DESC,ELAPSED_TIME,ALERT_INFO,ALERT_DATE_TIME,USER_ID,USER_BO,LOGIN_DATE_TIME,RESOURCE_BO,COMMENT_CODE,ALERT_COMMENT_DESCRIPTION"
            + " ORDER BY STARTDATE,SHIFT DESC,RESRCE,INTERSECTION_START_TIME,INTERSECTION_END_TIME"
            + "<if test=\"limit != null\">"
            + "<if test=\"offset != null\">"
            +    "limit ${limit} offset ${offset}"
            + "</if>"
            + "<if test=\"offset == null\">"
            +    "limit ${limit}"
            + "</if>"
            + "</if>"
            + "</script>" })
    @Results({ @Result(column = "STARTDATE", jdbcType = JdbcType.DATE, property = "date"),
            @Result(column = "SHIFT_DESC", jdbcType = JdbcType.VARCHAR, property = "shift"),
            @Result(column = "USER_ID", jdbcType = JdbcType.VARCHAR, property = "prdUserId"),
            @Result(column = "ME_USER_ID", jdbcType = JdbcType.VARCHAR, property = "meUserId"),
            @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resource"),
            @Result(column = "RESRCE_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceDesc"),
            @Result(column = "RESOURCE_TYPE", jdbcType = JdbcType.VARCHAR, property = "resourceType"),
            @Result(column = "RESOURCE_TYPE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "resourceTypeDesc"),
            @Result(column = "ASSET", jdbcType = JdbcType.VARCHAR, property = "asset"),
            @Result(column = "LINE_AREA_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceAddr"),
            @Result(column = "RESRCE_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceDesc"),
            @Result(column = "INTERSECTION_START_TIME", jdbcType = JdbcType.DATE, property = "startTime"),
            @Result(column = "INTERSECTION_END_TIME", jdbcType = JdbcType.DATE, property = "endTime"),
            @Result(column = "RESOURCE_STATE_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceState"),
            @Result(column = "INTERSECTION_TIME_DURATION", jdbcType = JdbcType.VARCHAR, property = "duration"),
            @Result(column = "ALERT_INFO", jdbcType = JdbcType.VARCHAR, property = "alertInfo"),
    		@Result(column = "ALERT_DATE_TIME", jdbcType = JdbcType.DATE, property = "alertDateTime"),
		    @Result(column = "COMMENT_CODE", jdbcType = JdbcType.VARCHAR, property = "commentCode"),
		    @Result(column = "ALERT_COMMENT_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "alertCommentDescription")
        })
    public List<ResourceStatusDetailVO> resultAlert(
            @Param("limit") Integer pageSize,
            @Param("offset") Integer currPage,
            @Param("site") String site, 
            @Param("workArea") String workArea,
            @Param("lineArea") String lineArea, 
            @Param("startDate") String startDate, 
            @Param("endDate")String endDate,
            @Param("start_date_time") String start_date_time, 
            @Param("end_date_time")String end_date_time,
            @Param("resourceType") String resourceType, 
            @Param("resrce") String resrce, 
            @Param("shift") String shift, 
            @Param("commentCode")String commentCode
            );
    
    @Select({ "<script> SELECT STARTDATE,SHIFT,SHIFT_DESC,ME_USER_ID,RESOURCE_CODE,ASSET,LINE_AREA_DESC,"
            + " RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,"
            + " INTERSECTION_TIME_DURATION,ELAPSED_TIME,REASON_CODE,RRCL_COMMENT,REASON_CODE_DESC,RESOURCE_STATE_DESC,"
            + " USER_ID,USER_BO,LOGIN_DATE_TIME"
            + " FROM \"_SYS_BIC\".\"com.andon.report.resource_status_detail/RESULT_REASON_CODE\""
            + "("
            + "'PLACEHOLDER' = ('$$site$$','${site}'),"
            + "'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'),"
            + "'PLACEHOLDER' = ('$$resrce$$','${resrce}'),"
            + "'PLACEHOLDER' = ('$$shift$$','${shift}'),"
            + "'PLACEHOLDER' = ('$$workArea$$','${workArea}'),"
            + "'PLACEHOLDER' = ('$$lineArea$$','${lineArea}'),"
            + "'PLACEHOLDER' = ('$$startDate$$','${startDate}'),"
            + "'PLACEHOLDER' = ('$$endDate$$','${endDate}'),"
            + "'PLACEHOLDER' = ('$$start_date_time$$','${start_date_time}'),"
            + "'PLACEHOLDER' = ('$$end_date_time$$','${end_date_time}')"
            + ")"
            + " GROUP BY STARTDATE,SHIFT,SHIFT_DESC,ME_USER_ID,RESOURCE_CODE,ASSET,LINE_AREA_DESC,"
            + " RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,"
            + " INTERSECTION_TIME_DURATION,ELAPSED_TIME,REASON_CODE,RRCL_COMMENT,REASON_CODE_DESC,RESOURCE_STATE_DESC,"
            + " USER_ID,USER_BO,LOGIN_DATE_TIME"
            + " ORDER BY STARTDATE,SHIFT DESC,RESOURCE_CODE,INTERSECTION_START_TIME,INTERSECTION_END_TIME"
            + "<if test=\"limit != null\">"
            + "<if test=\"offset != null\">"
            +    "limit ${limit} offset ${offset}"
            + "</if>"
            + "<if test=\"offset == null\">"
            +    "limit ${limit}"
            + "</if>"
            + "</if>"
            + "</script>" })
    @Results({ @Result(column = "STARTDATE", jdbcType = JdbcType.DATE, property = "date"),
            @Result(column = "SHIFT_DESC", jdbcType = JdbcType.VARCHAR, property = "shift"),
            @Result(column = "USER_ID", jdbcType = JdbcType.VARCHAR, property = "prdUserId"),
            @Result(column = "ME_USER_ID", jdbcType = JdbcType.VARCHAR, property = "meUserId"),
            @Result(column = "RESOURCE_CODE", jdbcType = JdbcType.VARCHAR, property = "resourceCode"),
            @Result(column = "RESRCE_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceDesc"),
            @Result(column = "RESOURCE_TYPE", jdbcType = JdbcType.VARCHAR, property = "resourceType"),
            @Result(column = "RESOURCE_TYPE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "resourceTypeDesc"),
            @Result(column = "ASSET", jdbcType = JdbcType.VARCHAR, property = "asset"),
            @Result(column = "LINE_AREA_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceAddr"),
            @Result(column = "RESRCE_DESC", jdbcType = JdbcType.VARCHAR, property = "resource"),
            @Result(column = "INTERSECTION_START_TIME", jdbcType = JdbcType.DATE, property = "startTime"),
            @Result(column = "INTERSECTION_END_TIME", jdbcType = JdbcType.DATE, property = "endTime"),
            @Result(column = "INTERSECTION_TIME_DURATION", jdbcType = JdbcType.VARCHAR, property = "duration"),
            @Result(column = "REASON_CODE_DESC", jdbcType = JdbcType.VARCHAR, property = "reasonCodeDesc"),
            @Result(column = "RRCL_COMMENT", jdbcType = JdbcType.VARCHAR, property = "comment"),
            @Result(column = "RESOURCE_STATE_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceState")
    })
    public List<ResourceStatusDetailVO> resultReasonCode(
            @Param("limit") Integer pageSize,
            @Param("offset") Integer currPage,
            @Param("site") String site, 
            @Param("workArea") String workArea,
            @Param("lineArea") String lineArea, 
            @Param("startDate") String startDate, 
            @Param("endDate")String endDate,
            @Param("start_date_time") String start_date_time, 
            @Param("end_date_time")String end_date_time,
            @Param("resourceType") String resourceType, 
            @Param("resrce") String resrce, 
            @Param("shift") String shift
            );
    @Select({ "<script> SELECT COUNT(*) FROM  (SELECT STARTDATE,RESRCE,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,ASSET,LINE_AREA,LINE_AREA_DESC,SHIFT,SHIFT_DESC,"
            + " SHIFT_START_DATE_TIME,SHIFT_END_DATE_TIME,ME_USER_ID,USER_ID,"
            + " START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,"
            + "RESOURCE_STATE_DESC,ELAPSED_TIME,ITEM,MODEL,USER_BO,LOGIN_DATE_TIME"
            + " FROM \"_SYS_BIC\".\"com.andon.report.resource_status_detail/RESULT_ITEM\""
            + "("
            + "'PLACEHOLDER' = ('$$site$$','${site}'),"
            + "'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'),"
            + "'PLACEHOLDER' = ('$$resrce$$','${resrce}'),"
            + "'PLACEHOLDER' = ('$$shift$$','${shift}'),"
            + "'PLACEHOLDER' = ('$$workArea$$','${workArea}'),"
            + "'PLACEHOLDER' = ('$$lineArea$$','${lineArea}'),"
            + "'PLACEHOLDER' = ('$$startDate$$','${startDate}'),"
            + "'PLACEHOLDER' = ('$$endDate$$','${endDate}'),"
            + "'PLACEHOLDER' = ('$$start_date_time$$','${start_date_time}'),"
            + "'PLACEHOLDER' = ('$$end_date_time$$','${end_date_time}')"
            + ")"
            + " GROUP BY STARTDATE,RESRCE,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,ASSET,LINE_AREA,LINE_AREA_DESC,SHIFT,SHIFT_DESC,"
            + " SHIFT_START_DATE_TIME,SHIFT_END_DATE_TIME,ME_USER_ID,USER_ID,"
            + " START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,"
            + "INTERSECTION_TIME_DURATION,RESOURCE_STATE_DESC,ELAPSED_TIME,ITEM,MODEL,USER_BO,LOGIN_DATE_TIME)"
            + "</script>" })
    public int resultItemCount(
            @Param("site") String site, 
            @Param("workArea") String workArea,
            @Param("lineArea") String lineArea, 
            @Param("startDate") String startDate, 
            @Param("endDate")String endDate,
            @Param("start_date_time") String start_date_time, 
            @Param("end_date_time")String end_date_time,
            @Param("resourceType") String resourceType, 
            @Param("resrce") String resrce, 
            @Param("shift") String shift
            );    
    @Select({ "<script>SELECT COUNT(*) FROM  (SELECT STARTDATE,RESRCE,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,ASSET,LINE_AREA,LINE_AREA_DESC,SHIFT,SHIFT_DESC,"
            + " SHIFT_START_DATE_TIME,SHIFT_END_DATE_TIME,ME_USER_ID,USER_ID,"
            + " START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,"
            + " RESOURCE_STATE,RESOURCE_STATE_DESC,ELAPSED_TIME,ITEM,MODEL,USER_BO,LOGIN_DATE_TIME"
            + " FROM \"_SYS_BIC\".\"com.andon.report.resource_status_detail/RESULT_ITEM\""
            + "("
            + "'PLACEHOLDER' = ('$$site$$','${site}'),"
            + "'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'),"
            + "'PLACEHOLDER' = ('$$resrce$$','${resrce}'),"
            + "'PLACEHOLDER' = ('$$shift$$','${shift}'),"
            + "'PLACEHOLDER' = ('$$workArea$$','${workArea}'),"
            + "'PLACEHOLDER' = ('$$lineArea$$','${lineArea}'),"
            + "'PLACEHOLDER' = ('$$startDate$$','${startDate}'),"
            + "'PLACEHOLDER' = ('$$endDate$$','${endDate}'),"
            + "'PLACEHOLDER' = ('$$start_date_time$$','${start_date_time}'),"
            + "'PLACEHOLDER' = ('$$end_date_time$$','${end_date_time}')"
            + ")"
            + "WHERE RESOURCE_STATE='D' "
            + " GROUP BY STARTDATE,RESRCE,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,ASSET,LINE_AREA,LINE_AREA_DESC,SHIFT,SHIFT_DESC,"
            + " SHIFT_START_DATE_TIME,SHIFT_END_DATE_TIME,ME_USER_ID,USER_ID,"
            + " START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,"
            + " RESOURCE_STATE,RESOURCE_STATE_DESC,ELAPSED_TIME,ITEM,MODEL,USER_BO,LOGIN_DATE_TIME"
            + " )"
            + "</script>" })
    public int resultHaltCount(
            @Param("site") String site, 
            @Param("workArea") String workArea,
            @Param("lineArea") String lineArea, 
            @Param("startDate") String startDate, 
            @Param("endDate")String endDate,
            @Param("start_date_time") String start_date_time, 
            @Param("end_date_time")String end_date_time,
            @Param("resourceType") String resourceType, 
            @Param("resrce") String resrce, 
            @Param("shift") String shift
            );
    @Select({ "<script> SELECT max(SECONDS_BETWEEN(INTERSECTION_START_TIME,INTERSECTION_END_TIME)) FROM ("
            + "SELECT STARTDATE,INTERSECTION_START_TIME,INTERSECTION_END_TIME"
            + " FROM \"_SYS_BIC\".\"com.andon.report.resource_status_detail/RESULT_ITEM\""
            + "("
            + "'PLACEHOLDER' = ('$$site$$','${site}'),"
            + "'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'),"
            + "'PLACEHOLDER' = ('$$resrce$$','${resrce}'),"
            + "'PLACEHOLDER' = ('$$shift$$','${shift}'),"
            + "'PLACEHOLDER' = ('$$workArea$$','${workArea}'),"
            + "'PLACEHOLDER' = ('$$lineArea$$','${lineArea}'),"
            + "'PLACEHOLDER' = ('$$startDate$$','${startDate}'),"
            + "'PLACEHOLDER' = ('$$endDate$$','${endDate}'),"
            + "'PLACEHOLDER' = ('$$start_date_time$$','${start_date_time}'),"
            + "'PLACEHOLDER' = ('$$end_date_time$$','${end_date_time}')"
            + ")"
            + "WHERE RESOURCE_STATE='D')"
            + "</script>" })
    public String resultDuration(
            @Param("site") String site, 
            @Param("workArea") String workArea,
            @Param("lineArea") String lineArea, 
            @Param("startDate") String startDate, 
            @Param("endDate")String endDate,
            @Param("start_date_time") String start_date_time, 
            @Param("end_date_time")String end_date_time,
            @Param("resourceType") String resourceType, 
            @Param("resrce") String resrce, 
            @Param("shift") String shift
            );    
    @Select({ "<script> SELECT COUNT(*) FROM (SELECT STARTDATE,SHIFT,SHIFT_DESC,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,ME_USER_ID,RESRCE,ASSET,LINE_AREA_DESC,"
            + " START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,ELAPSED_TIME,RESOURCE_STATE_DESC,ALERT_INFO,USER_ID,USER_BO,LOGIN_DATE_TIME,RESOURCE_BO,COMMENT_CODE,ALERT_COMMENT_DESCRIPTION"
            + " FROM \"_SYS_BIC\".\"com.andon.report.resource_status_detail/RESULT_ALERT\""
            + "("
            + "'PLACEHOLDER' = ('$$site$$','${site}'),"
            + "'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'),"
            + "'PLACEHOLDER' = ('$$resrce$$','${resrce}'),"
            + "'PLACEHOLDER' = ('$$shift$$','${shift}'),"
            + "'PLACEHOLDER' = ('$$comment_code_in$$', '${commentCode}'),"
            + "'PLACEHOLDER' = ('$$workArea$$','${workArea}'),"
            + "'PLACEHOLDER' = ('$$lineArea$$','${lineArea}'),"
            + "'PLACEHOLDER' = ('$$startDate$$','${startDate}'),"
            + "'PLACEHOLDER' = ('$$endDate$$','${endDate}'),"
            + "'PLACEHOLDER' = ('$$start_date_time$$','${start_date_time}'),"
            + "'PLACEHOLDER' = ('$$end_date_time$$','${end_date_time}')"
            + ")"
            + " GROUP BY STARTDATE,SHIFT,SHIFT_DESC,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,ME_USER_ID,RESRCE,ASSET,LINE_AREA_DESC,"
            + " START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,ELAPSED_TIME,RESOURCE_STATE_DESC,ALERT_INFO,USER_ID,USER_BO,LOGIN_DATE_TIME,RESOURCE_BO,COMMENT_CODE,ALERT_COMMENT_DESCRIPTION)"
            + "</script>" })
    public int resultAlertCount(
            @Param("site") String site, 
            @Param("workArea") String workArea,
            @Param("lineArea") String lineArea, 
            @Param("startDate") String startDate, 
            @Param("endDate")String endDate,
            @Param("start_date_time") String start_date_time, 
            @Param("end_date_time")String end_date_time,
            @Param("resourceType") String resourceType, 
            @Param("resrce") String resrce,
            @Param("shift") String shift,
            @Param("commentCode") String commentCode
            );    
    @Select({ "<script> SELECT COUNT(*) FROM (SELECT STARTDATE,SHIFT,SHIFT_DESC,USER_ID,ME_USER_ID,RESOURCE_CODE,ASSET,LINE_AREA_DESC,"
            + " RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,"
            + " ELAPSED_TIME,REASON_CODE,RRCL_COMMENT,REASON_CODE_DESC,RESOURCE_STATE_DESC,USER_ID,USER_BO,LOGIN_DATE_TIME"
            + " FROM \"_SYS_BIC\".\"com.andon.report.resource_status_detail/RESULT_REASON_CODE\""
            + "("
            + "'PLACEHOLDER' = ('$$site$$','${site}'),"
            + "'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'),"
            + "'PLACEHOLDER' = ('$$resrce$$','${resrce}'),"
            + "'PLACEHOLDER' = ('$$shift$$','${shift}'),"
            + "'PLACEHOLDER' = ('$$workArea$$','${workArea}'),"
            + "'PLACEHOLDER' = ('$$lineArea$$','${lineArea}'),"
            + "'PLACEHOLDER' = ('$$startDate$$','${startDate}'),"
            + "'PLACEHOLDER' = ('$$endDate$$','${endDate}'),"
            + "'PLACEHOLDER' = ('$$start_date_time$$','${start_date_time}'),"
            + "'PLACEHOLDER' = ('$$end_date_time$$','${end_date_time}')"
            + ")"
            + " GROUP BY STARTDATE,SHIFT,SHIFT_DESC,USER_ID,ME_USER_ID,RESOURCE_CODE,ASSET,LINE_AREA_DESC,"
            + " RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,"
            + " INTERSECTION_TIME_DURATION,ELAPSED_TIME,REASON_CODE,RRCL_COMMENT,REASON_CODE_DESC,RESOURCE_STATE_DESC,"
            + " USER_ID,USER_BO,LOGIN_DATE_TIME)"
            + "</script>" })
    public int resultReasonCodeCount(
            @Param("site") String site, 
            @Param("workArea") String workArea,
            @Param("lineArea") String lineArea, 
            @Param("startDate") String startDate, 
            @Param("endDate")String endDate,
            @Param("start_date_time") String start_date_time, 
            @Param("end_date_time")String end_date_time,
            @Param("resourceType") String resourceType, 
            @Param("resrce") String resrce, 
            @Param("shift") String shift
            );
    @Select({ "<script> SELECT STARTDATE,USER_ID,RESRCE,ASSET,LINE_AREA_DESC,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,"
            + "START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,"
            + "INTERSECTION_TIME_DURATION,ITEM,QTY_INPUT,QTY_YIELD,QTY_SCRAP,QTY_TOTAL,DURATION,USER_ID,USER_BO,LOGIN_DATE_TIME,RESOURCE_BO"
            + " FROM \"_SYS_BIC\".\"com.andon.report.resource_status_detail/RESULT_OUTPUT\""
            + "("
            + "'PLACEHOLDER' = ('$$site$$','${site}'),"
            + "'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'),"
            + "'PLACEHOLDER' = ('$$resrce$$','${resrce}'),"
            + "'PLACEHOLDER' = ('$$shift$$','${shift}'),"
            + "'PLACEHOLDER' = ('$$workArea$$','${workArea}'),"
            + "'PLACEHOLDER' = ('$$lineArea$$','${lineArea}'),"
            + "'PLACEHOLDER' = ('$$startDate$$','${startDate}'),"
            + "'PLACEHOLDER' = ('$$endDate$$','${endDate}'),"
            + "'PLACEHOLDER' = ('$$start_date_time$$','${start_date_time}'),"
            + "'PLACEHOLDER' = ('$$end_date_time$$','${end_date_time}')"
            + ")"
            + " GROUP BY STARTDATE,USER_ID,RESRCE,ASSET,LINE_AREA_DESC,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,"
            + " START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,"
            + " INTERSECTION_TIME_DURATION,ITEM,QTY_INPUT,QTY_YIELD,QTY_SCRAP,QTY_TOTAL,DURATION,USER_ID,USER_BO,LOGIN_DATE_TIME,RESOURCE_BO"
            + " ORDER BY STARTDATE,RESRCE,INTERSECTION_START_TIME,INTERSECTION_END_TIME"
            + "<if test=\"limit != null\">"
            + "<if test=\"offset != null\">"
            +    "limit ${limit} offset ${offset}"
            + "</if>"
            + "<if test=\"offset == null\">"
            +    "limit ${limit}"
            + "</if>"
            + "</if>"
            + "</script>" })
    @Results({
        @Result(column = "STARTDATE", jdbcType = JdbcType.VARCHAR, property = "date"),
        @Result(column = "USER_ID", jdbcType = JdbcType.VARCHAR, property = "prdUserId"),
        @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resource"),
        @Result(column = "ASSET", jdbcType = JdbcType.VARCHAR, property = "asset"),
        @Result(column = "LINE_AREA_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceAddr"),
        @Result(column = "RESRCE_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceDesc"),
        @Result(column = "RESOURCE_TYPE", jdbcType = JdbcType.VARCHAR, property = "resourceType"),
        @Result(column = "RESOURCE_TYPE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "resourceTypeDesc"),
        @Result(column = "INTERSECTION_START_TIME", jdbcType = JdbcType.VARCHAR, property = "startTime"),
        @Result(column = "INTERSECTION_END_TIME", jdbcType = JdbcType.VARCHAR, property = "endTime"),
        @Result(column = "ITEM", jdbcType = JdbcType.VARCHAR, property = "item"),
        @Result(column = "QTY_INPUT", jdbcType = JdbcType.VARCHAR, property = "qtyInput"),
        @Result(column = "QTY_YIELD", jdbcType = JdbcType.TIMESTAMP, property = "qtyYield"),
        @Result(column = "QTY_SCRAP", jdbcType = JdbcType.TIMESTAMP, property = "qtyScrap"),
        @Result(column = "QTY_TOTAL", jdbcType = JdbcType.TIMESTAMP, property = "qtyTotal"),
        @Result(column = "DURATION", jdbcType = JdbcType.TIMESTAMP, property = "duration")
    })
    public List<ResourceStatusDetailVO> resultOutPut(
            @Param("limit") Integer pageSize,
            @Param("offset") Integer currPage,
            @Param("site") String site, 
            @Param("workArea") String workArea,
            @Param("lineArea") String lineArea, 
            @Param("startDate") String startDate, 
            @Param("endDate")String endDate,
            @Param("start_date_time") String start_date_time, 
            @Param("end_date_time")String end_date_time,
            @Param("resourceType") String resourceType, 
            @Param("resrce") String resrce, 
            @Param("shift") String shift
            );
    @Select({ "<script>SELECT COUNT(*) FROM (SELECT STARTDATE,USER_ID,RESRCE,ASSET,LINE_AREA_DESC,RESRCE_DESC,"
            + "START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,"
            + "ITEM,QTY_INPUT,QTY_YIELD,QTY_SCRAP,QTY_TOTAL,DURATION"
            + " FROM \"_SYS_BIC\".\"com.andon.report.resource_status_detail/RESULT_OUTPUT\""
            + "("
            + "'PLACEHOLDER' = ('$$site$$','${site}'),"
            + "'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'),"
            + "'PLACEHOLDER' = ('$$resrce$$','${resrce}'),"
            + "'PLACEHOLDER' = ('$$shift$$','${shift}'),"
            + "'PLACEHOLDER' = ('$$workArea$$','${workArea}'),"
            + "'PLACEHOLDER' = ('$$lineArea$$','${lineArea}'),"
            + "'PLACEHOLDER' = ('$$startDate$$','${startDate}'),"
            + "'PLACEHOLDER' = ('$$endDate$$','${endDate}'),"
            + "'PLACEHOLDER' = ('$$start_date_time$$','${start_date_time}'),"
            + "'PLACEHOLDER' = ('$$end_date_time$$','${end_date_time}')"
            + ")"
            + " GROUP BY STARTDATE,USER_ID,RESRCE,ASSET,LINE_AREA_DESC,RESRCE_DESC,"
            + " START_DATE_TIME,END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,"
            + " INTERSECTION_TIME_DURATION,ITEM,QTY_INPUT,QTY_YIELD,QTY_SCRAP,QTY_TOTAL,DURATION)"
            + "</script>" })
    public int resultOutPutCount(
            @Param("site") String site, 
            @Param("workArea") String workArea,
            @Param("lineArea") String lineArea, 
            @Param("startDate") String startDate, 
            @Param("endDate")String endDate,
            @Param("start_date_time") String start_date_time, 
            @Param("end_date_time")String end_date_time,
            @Param("resourceType") String resourceType, 
            @Param("resrce") String resrce, 
            @Param("shift") String shift
            );
    @Select({ "<script>SELECT STARTDATE,RESRCE,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,ASSET,"	
    		+ "LINE_AREA,LINE_AREA_DESC,SHIFT,SHIFT_DESC,SHIFT_START_DATE_TIME,"	
    		+ "SHIFT_END_DATE_TIME,RESOURCE_BO,ME_USER_ID,USER_ID,START_DATE_TIME,	"
			+ "END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,"
			+ "RESOURCE_STATE,	RESOURCE_STATE_DESC,ELAPSED_TIME,USER_BO,REASON_CODE_TIME,	"
			+ "REASON_CODE,REASON_CODE_DESCRIPTION,LOGIN_DATE_TIME"
			+ " FROM \"_SYS_BIC\".\"com.andon.report.resource_status_detail/RESULT_RT_REASON_CODE\" "
			+ "("
            + "'PLACEHOLDER' = ('$$site$$','${site}'),"
            + "'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'),"
            + "'PLACEHOLDER' = ('$$resrce$$','${resrce}'),"
            + "'PLACEHOLDER' = ('$$shift$$','${shift}'),"
            + "'PLACEHOLDER' = ('$$workArea$$','${workArea}'),"
            + "'PLACEHOLDER' = ('$$lineArea$$','${lineArea}'),"
            + "'PLACEHOLDER' = ('$$startDate$$','${startDate}'),"
            + "'PLACEHOLDER' = ('$$endDate$$','${endDate}'),"
            + "'PLACEHOLDER' = ('$$start_date_time$$','${start_date_time}'),"
            + "'PLACEHOLDER' = ('$$end_date_time$$','${end_date_time}')"
            + ")"
            + " GROUP BY STARTDATE,RESRCE,RESRCE_DESC,RESOURCE_TYPE,RESOURCE_TYPE_DESCRIPTION,ASSET,	"
            + "LINE_AREA,LINE_AREA_DESC,SHIFT,SHIFT_DESC,SHIFT_START_DATE_TIME,"
			+ "SHIFT_END_DATE_TIME,RESOURCE_BO,ME_USER_ID,USER_ID,START_DATE_TIME,"
			+ "END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,"
			+ "RESOURCE_STATE,RESOURCE_STATE_DESC,ELAPSED_TIME,USER_BO,REASON_CODE_TIME,"
			+ "REASON_CODE,REASON_CODE_DESCRIPTION,LOGIN_DATE_TIME	"
			+ " ORDER BY STARTDATE , SHIFT DESC , RESRCE , "
			+ "	INTERSECTION_START_TIME , INTERSECTION_END_TIME,REASON_CODE_TIME "
            + "<if test=\"limit != null\">"
            + "<if test=\"offset != null\">"
            +    "limit ${limit} offset ${offset}"
            + "</if>"
            + "<if test=\"offset == null\">"
            +    "limit ${limit}"
            + "</if>"
            + "</if>"
            + "</script>" })
    @Results({
        @Result(column = "STARTDATE", jdbcType = JdbcType.VARCHAR, property = "date"),
        @Result(column = "SHIFT_DESC", jdbcType = JdbcType.VARCHAR, property = "shift"),
        @Result(column = "ME_USER_ID", jdbcType = JdbcType.VARCHAR, property = "meUserId"),
        @Result(column = "USER_ID", jdbcType = JdbcType.VARCHAR, property = "prdUserId"),
        @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resource"),
        @Result(column = "RESRCE_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceDesc"),
        @Result(column = "RESOURCE_TYPE", jdbcType = JdbcType.VARCHAR, property = "resourceType"),
        @Result(column = "RESOURCE_TYPE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "resourceTypeDesc"),
        @Result(column = "ASSET", jdbcType = JdbcType.VARCHAR, property = "asset"),
        @Result(column = "LINE_AREA", jdbcType = JdbcType.VARCHAR, property = "lineArea"),
        @Result(column = "LINE_AREA_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceAddr"),
        @Result(column = "INTERSECTION_START_TIME", jdbcType = JdbcType.VARCHAR, property = "startTime"),
        @Result(column = "INTERSECTION_END_TIME", jdbcType = JdbcType.VARCHAR, property = "endTime"),
        @Result(column = "RESOURCE_STATE_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceState"),
        @Result(column = "INTERSECTION_TIME_DURATION", jdbcType = JdbcType.VARCHAR, property = "duration"),
        @Result(column = "REASON_CODE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "reasonCodeTime"),
        @Result(column = "REASON_CODE_DESCRIPTION", jdbcType = JdbcType.TIMESTAMP, property = "reasonCodeDescription") })
	public List<ResourceStatusDetailVO> resultRtReasonCode(
            @Param("limit") Integer pageSize,
            @Param("offset") Integer currPage,
            @Param("site") String site, 
            @Param("workArea") String workArea,
            @Param("lineArea") String lineArea, 
            @Param("startDate") String startDate, 
            @Param("endDate")String endDate,
            @Param("start_date_time") String start_date_time, 
            @Param("end_date_time")String end_date_time,
            @Param("resourceType") String resourceType, 
            @Param("resrce") String resrce, 
            @Param("shift") String shift
            );
    @Select({ "<script>SELECT COUNT(*) FROM (SELECT STARTDATE,RESRCE,RESRCE_DESC,ASSET,"	
    		+ "LINE_AREA,LINE_AREA_DESC,SHIFT,SHIFT_DESC,SHIFT_START_DATE_TIME,"	
    		+ "SHIFT_END_DATE_TIME,RESOURCE_BO,ME_USER_ID,USER_ID,START_DATE_TIME,	"
			+ "END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,"
			+ "RESOURCE_STATE,	RESOURCE_STATE_DESC,ELAPSED_TIME,USER_BO,REASON_CODE_TIME,	"
			+ "REASON_CODE,REASON_CODE_DESCRIPTION,LOGIN_DATE_TIME"
			+ " FROM \"_SYS_BIC\".\"com.andon.report.resource_status_detail/RESULT_RT_REASON_CODE\" "
			+ "("
            + "'PLACEHOLDER' = ('$$site$$','${site}'),"
            + "'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'),"
            + "'PLACEHOLDER' = ('$$resrce$$','${resrce}'),"
            + "'PLACEHOLDER' = ('$$shift$$','${shift}'),"
            + "'PLACEHOLDER' = ('$$workArea$$','${workArea}'),"
            + "'PLACEHOLDER' = ('$$lineArea$$','${lineArea}'),"
            + "'PLACEHOLDER' = ('$$startDate$$','${startDate}'),"
            + "'PLACEHOLDER' = ('$$endDate$$','${endDate}'),"
            + "'PLACEHOLDER' = ('$$start_date_time$$','${start_date_time}'),"
            + "'PLACEHOLDER' = ('$$end_date_time$$','${end_date_time}')"
            + ")"
            + " GROUP BY STARTDATE,RESRCE,RESRCE_DESC,ASSET,	"
            + "LINE_AREA,LINE_AREA_DESC,SHIFT,SHIFT_DESC,SHIFT_START_DATE_TIME,"
			+ "SHIFT_END_DATE_TIME,RESOURCE_BO,ME_USER_ID,USER_ID,START_DATE_TIME,"
			+ "END_DATE_TIME,INTERSECTION_START_TIME,INTERSECTION_END_TIME,INTERSECTION_TIME_DURATION,"
			+ "RESOURCE_STATE,RESOURCE_STATE_DESC,ELAPSED_TIME,USER_BO,REASON_CODE_TIME,"
			+ "REASON_CODE,REASON_CODE_DESCRIPTION,LOGIN_DATE_TIME)"
            + "</script>" })
    public int resultRtReasonCodeCount(
            @Param("site") String site, 
            @Param("workArea") String workArea,
            @Param("lineArea") String lineArea, 
            @Param("startDate") String startDate, 
            @Param("endDate")String endDate,
            @Param("start_date_time") String start_date_time, 
            @Param("end_date_time")String end_date_time,
            @Param("resourceType") String resourceType, 
            @Param("resrce") String resrce, 
            @Param("shift") String shift
            );

    @Select({ "<script>"
    		+ "SELECT STARTDATE, RESRCE, RESRCE_DESC, ASSET, WORK_AREA_DESC,LINE_AREA, LINE_AREA_DESC, SHIFT,"
    		+ "   SHIFT_DESC, SHIFT_START_DATE_TIME, SHIFT_END_DATE_TIME, RESOURCE_BO, RT_DESC, ME_USER_ID,"
    		+ "   USER_ID, START_DATE_TIME, END_DATE_TIME, INTERSECTION_START_TIME, INTERSECTION_END_TIME,"
			+ "   INTERSECTION_TIME_DURATION, RESOURCE_STATE, RESOURCE_STATE_DESC, ELAPSED_TIME, ITEM,"
			+ "    MODEL, REASON_CODE_TIME, REASON_CODE, REASON_CODE_DESCRIPTION, QTY_INPUT, QTY_YIELD,"
			+ "    QTY_SCRAP, QTY_TOTAL, ALERT_INFO, USER_BO, LOGIN_DATE_TIME, sum(QTY) AS QTY "
			+ "FROM \"_SYS_BIC\".\"com.andon.report.resource_status_detail/RESULT_TOTAL_ITEM\""
			+ "("
            + "'PLACEHOLDER' = ('$$site$$','${site}'),"
            + "'PLACEHOLDER' = ('$$resourceType$$','${resourceType}'),"
            + "'PLACEHOLDER' = ('$$resrce$$','${resrce}'),"
            + "'PLACEHOLDER' = ('$$shift$$','${shift}'),"
            + "'PLACEHOLDER' = ('$$workArea$$','${workArea}'),"
            + "'PLACEHOLDER' = ('$$lineArea$$','${lineArea}'),"
            + "'PLACEHOLDER' = ('$$startDate$$','${startDate}'),"
            + "'PLACEHOLDER' = ('$$endDate$$','${endDate}'),"
            + "'PLACEHOLDER' = ('$$start_date_time$$','${start_date_time}'),"
            + "'PLACEHOLDER' = ('$$end_date_time$$','${end_date_time}'),"
            + "'PLACEHOLDER' = ('$$resource_state$$','${resourceState}')"
            + ")"
            + " group by "
            + "STARTDATE, RESRCE, RESRCE_DESC, ASSET, WORK_AREA_DESC,LINE_AREA, LINE_AREA_DESC, SHIFT,"   
            + "SHIFT_DESC, SHIFT_START_DATE_TIME, SHIFT_END_DATE_TIME, RESOURCE_BO, RT_DESC, ME_USER_ID,"   
            + "USER_ID, START_DATE_TIME, END_DATE_TIME, INTERSECTION_START_TIME, INTERSECTION_END_TIME,"  
            + "INTERSECTION_TIME_DURATION, RESOURCE_STATE, RESOURCE_STATE_DESC, ELAPSED_TIME, ITEM,"    
            + "MODEL, REASON_CODE_TIME, REASON_CODE, REASON_CODE_DESCRIPTION, QTY_INPUT, QTY_YIELD,"    
            + " QTY_SCRAP, QTY_TOTAL, ALERT_INFO, USER_BO, LOGIN_DATE_TIME"
            + " ORDER BY STARTDATE,WORK_AREA_DESC,LINE_AREA_DESC,RESRCE,INTERSECTION_START_TIME"
            + " LIMIT 300000"
            + "</script>" })
    @Results({
    	
        @Result(column = "STARTDATE", jdbcType = JdbcType.VARCHAR, property = "date"),
        @Result(column = "SHIFT_DESC", jdbcType = JdbcType.VARCHAR, property = "shift"),
        @Result(column = "ME_USER_ID", jdbcType = JdbcType.VARCHAR, property = "meUserId"),
        @Result(column = "USER_ID", jdbcType = JdbcType.VARCHAR, property = "prdUserId"),
        @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resource"),
        @Result(column = "RESRCE_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceDesc"),
        @Result(column = "RT_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceTypeDesc"),
        @Result(column = "RESOURCE_STATE", jdbcType = JdbcType.VARCHAR, property = "resourceState"),
        @Result(column = "ASSET", jdbcType = JdbcType.VARCHAR, property = "asset"),
        @Result(column = "WORK_AREA_DESC", jdbcType = JdbcType.VARCHAR, property = "workAreaDesc"),
        @Result(column = "LINE_AREA", jdbcType = JdbcType.VARCHAR, property = "lineArea"),
        @Result(column = "LINE_AREA_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceAddr"),
        @Result(column = "INTERSECTION_START_TIME", jdbcType = JdbcType.VARCHAR, property = "startTime"),
        @Result(column = "INTERSECTION_END_TIME", jdbcType = JdbcType.VARCHAR, property = "endTime"),
        @Result(column = "RESOURCE_STATE_DESC", jdbcType = JdbcType.VARCHAR, property = "resourceState"),
        @Result(column = "INTERSECTION_TIME_DURATION", jdbcType = JdbcType.VARCHAR, property = "duration"),
        @Result(column = "REASON_CODE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "reasonCodeTime"),
        @Result(column = "QTY_INPUT", jdbcType = JdbcType.VARCHAR, property = "qtyInput"),
        @Result(column = "QTY_YIELD", jdbcType = JdbcType.TIMESTAMP, property = "qtyYield"),
        @Result(column = "QTY_SCRAP", jdbcType = JdbcType.TIMESTAMP, property = "qtyScrap"),
        @Result(column = "QTY_TOTAL", jdbcType = JdbcType.TIMESTAMP, property = "qtyTotal"),
        @Result(column = "MODEL", jdbcType = JdbcType.TIMESTAMP, property = "model"),
        @Result(column = "ITEM", jdbcType = JdbcType.TIMESTAMP, property = "item"),
        @Result(column = "ALERT_INFO", jdbcType = JdbcType.VARCHAR, property = "alertInfo"),
        @Result(column = "REASON_CODE_DESCRIPTION", jdbcType = JdbcType.TIMESTAMP, property = "reasonCodeDescription") })
	public List<ResourceStatusDetailVO> resultCollect(
            @Param("site") String site, 
            @Param("workArea") String workArea,
            @Param("lineArea") String lineArea, 
            @Param("startDate") String startDate, 
            @Param("endDate")String endDate,
            @Param("start_date_time") String start_date_time, 
            @Param("end_date_time")String end_date_time,
            @Param("resourceType") String resourceType, 
            @Param("resrce") String resrce, 
            @Param("shift") String shift,
    		@Param("resourceState") String resourceState);
    }
