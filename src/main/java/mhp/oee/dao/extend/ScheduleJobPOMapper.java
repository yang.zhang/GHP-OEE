package mhp.oee.dao.extend;

import mhp.oee.po.extend.ScheduleJobPO;
import mhp.oee.po.extend.WorkCenterAndWorkCenterMemberPO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * Created by LinZuK on 2016/8/31.
 */
public interface ScheduleJobPOMapper {
    @Select(" SELECT A.ACTIVITY, A.DESCRIPTION, P.PARAM_ID, P.PARAM_DEFAULT_VALUE, A.CLASS_OR_PROGRAM, A.ENABLED " +
            " FROM ACTIVITY AS A " +
            " INNER JOIN ACTIVITY_PARAM AS P " +
            " ON P.ACTIVITY_BO=A.HANDLE " +
            " WHERE A.ACTIVITY LIKE 'JOB%' " +
            " AND A.EXECUTION_TYPE='B' " +
            " AND A.ENABLED='true'" +
            " AND A.CLASS_OR_PROGRAM<>'heartbeatOverTimeJob.run' " + 
            " AND P.PARAM_ID NOT IN ('DATA_TIME_RANGE','BATCH_SIZE','DATA_LIST_SIZE','THREAD_TIMES') ")
    @Results({
            @Result(column = "ACTIVITY", jdbcType = JdbcType.VARCHAR, property = "activity"),
            @Result(column = "DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "description"),
            @Result(column = "PARAM_ID", jdbcType = JdbcType.VARCHAR, property = "paramId"),
            @Result(column = "PARAM_DEFAULT_VALUE", jdbcType = JdbcType.VARCHAR, property = "paramDefaultValue"),
            @Result(column = "CLASS_OR_PROGRAM", jdbcType = JdbcType.VARCHAR, property = "classOrProgram"),
            @Result(column = "ENABLED", jdbcType = JdbcType.VARCHAR, property = "enabled")
    })
    List<ScheduleJobPO> selectAllJob();

    @Select(" SELECT A.ACTIVITY, A.DESCRIPTION, P.PARAM_ID, P.PARAM_DEFAULT_VALUE, A.CLASS_OR_PROGRAM, A.ENABLED\n" +
            " FROM ACTIVITY AS A " +
            " INNER JOIN ACTIVITY_PARAM AS P " +
            " ON P.ACTIVITY_BO=A.HANDLE " +
            " WHERE A.CLASS_OR_PROGRAM='heartbeatOverTimeJob.run' ")
    @Results({
            @Result(column = "ACTIVITY", jdbcType = JdbcType.VARCHAR, property = "activity"),
            @Result(column = "DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "description"),
            @Result(column = "PARAM_ID", jdbcType = JdbcType.VARCHAR, property = "paramId"),
            @Result(column = "PARAM_DEFAULT_VALUE", jdbcType = JdbcType.VARCHAR, property = "paramDefaultValue"),
            @Result(column = "CLASS_OR_PROGRAM", jdbcType = JdbcType.VARCHAR, property = "classOrProgram"),
            @Result(column = "ENABLED", jdbcType = JdbcType.VARCHAR, property = "enabled")
    })
    List<ScheduleJobPO> selectHeartBeatJob();
}
