package mhp.oee.dao.extend;

import java.util.List;

import mhp.oee.vo.ActivityGroupVO;
import mhp.oee.vo.ActivityVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import mhp.oee.po.extend.ActivityAndActivityGroupAndActivityGroupActivityPO;

public interface ActivityAndActivityGroupAndActivityGroupActivityPOMapper {
    @Select("SELECT ACTIVITY_GROUP.ACTIVITY_GROUP "
            + "FROM ACTIVITY,ACTIVITY_GROUP,ACTIVITY_GROUP_ACTIVITY "
            + "WHERE ACTIVITY_GROUP_ACTIVITY.ACTIVITY_BO=ACTIVITY.HANDLE AND ACTIVITY_GROUP_ACTIVITY.ACTIVITY_GROUP_BO=ACTIVITY_GROUP.HANDLE "
            + "AND ACTIVITY.ACTIVITY = #{activity}")
    @ResultMap("BaseResultMap")
    public List<ActivityGroupVO> getAssigendActivityGroup(@Param("activity")String activity);
    
    @Select("SELECT ACTIVITY_GROUP.ACTIVITY_GROUP "
            + "FROM ACTIVITY_GROUP "
            + "WHERE ACTIVITY_GROUP.ACTIVITY_GROUP NOT IN "
            + "("
            + "SELECT ACTIVITY_GROUP.ACTIVITY_GROUP "
            + "FROM ACTIVITY,ACTIVITY_GROUP,ACTIVITY_GROUP_ACTIVITY "
            + "WHERE ACTIVITY_GROUP_ACTIVITY.ACTIVITY_BO=ACTIVITY.HANDLE AND ACTIVITY_GROUP_ACTIVITY.ACTIVITY_GROUP_BO=ACTIVITY_GROUP.HANDLE "
            + "AND ACTIVITY.ACTIVITY = #{activity}"
            + ")")
    @ResultMap("BaseResultMap")
    public List<ActivityGroupVO> getAvailableActivityGroup(@Param("activity")String activity);
    @Select("SELECT ACTIVITY.ACTIVITY " 
            + "FROM ACTIVITY,ACTIVITY_GROUP,ACTIVITY_GROUP_ACTIVITY "
            + "WHERE ACTIVITY_GROUP_ACTIVITY.ACTIVITY_BO=ACTIVITY.HANDLE AND ACTIVITY_GROUP_ACTIVITY.ACTIVITY_GROUP_BO=ACTIVITY_GROUP.HANDLE "
            + "AND ACTIVITY_GROUP.ACTIVITY_GROUP = #{activityGroup}")
    @ResultMap("resultActivity")
    public List<ActivityVO> getAssigendActivity(@Param("activityGroup") String activityGroup);

    @Select("SELECT ACTIVITY.ACTIVITY " 
            + "FROM ACTIVITY " 
            + "WHERE ACTIVITY.ACTIVITY NOT IN " 
            + "("
            + "SELECT ACTIVITY.ACTIVITY " 
            + "FROM ACTIVITY,ACTIVITY_GROUP,ACTIVITY_GROUP_ACTIVITY "
            + "WHERE ACTIVITY_GROUP_ACTIVITY.ACTIVITY_BO=ACTIVITY.HANDLE AND ACTIVITY_GROUP_ACTIVITY.ACTIVITY_GROUP_BO=ACTIVITY_GROUP.HANDLE "
            + "AND ACTIVITY_GROUP.ACTIVITY_GROUP = #{activityGroup}" 
            + ")"
            )
    @ResultMap("resultActivity")
    public List<ActivityVO> getAvailableActivity(@Param("activityGroup") String activityGroup);

    
    @Select("SELECT A.ACTIVITY_GROUP, B.ACTIVITY_BO, A.DESCRIPTION, C.ACTIVITY, C.DESCRIPTION AS  ACTIVITY_DES FROM ACTIVITY_GROUP AS A  LEFT JOIN ACTIVITY_GROUP_ACTIVITY AS B ON A.HANDLE = B.ACTIVITY_GROUP_BO "
            + "LEFT JOIN ACTIVITY AS C ON B.ACTIVITY_BO = C.HANDLE")
    @ResultMap("resultActivityAndGroup")
    public List<ActivityAndActivityGroupAndActivityGroupActivityPO> getAllActivityGroupAndActivity();
 
}
