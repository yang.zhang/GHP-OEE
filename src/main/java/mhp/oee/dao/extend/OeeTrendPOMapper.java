package mhp.oee.dao.extend;

import mhp.oee.po.extend.OeeOeeTrendPO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * Created by SuMingzhi on 2016/12/20.
 */
public interface OeeTrendPOMapper {
	
	@Select("  SELECT \"ITEM\", \"USER_ID\", \"WORK_AREA\", \"WORK_CENTER\", \"RESRCE_TYPE\", \"RESRCE\", \"ASSET\", \"RESRCE_DESCRIPTION\", \"SHIFT_START_DATE_TIME\","+ 
			"	\"BY_TIME\", \"BY_TIME_CHART\", \"BY_TIME_MONTH\",\"BY_TIME_YEAR\", \"A\", \"P\", \"Q\", \"OEE\", \"OEE_VALUE\", \"PPM_THEORY\", count(\"QTY\") AS \"QTY\" "+
			"	FROM \"_SYS_BIC\".\"com.andon.report.oee/OEE_CAL_DETAIL_TREND\" "+
			"	('PLACEHOLDER' = ('$$resourceBo$$', '${resourceBo}'),"+
			"	 'PLACEHOLDER' = ('$$site$$', '${site}'), "+
			"	 'PLACEHOLDER' = ('$$shiftLike$$','${shiftLike}'), "+
			"	 'PLACEHOLDER' = ('$$userIDin$$', '${userIDin}'), "+
			"	 'PLACEHOLDER' = ('$$endDate$$', '${endDate}'),"+
			"	 'PLACEHOLDER' = ('$$workCenterLink$$', '${workCenterLink}'),"+
			"	 'PLACEHOLDER' = ('$$startDate$$', '${startDate}'),"+
			"	 'PLACEHOLDER' = ('$$workCenter$$', '${workCenter}'), "+
			"	 'PLACEHOLDER' = ('$$modelIn$$', '${modelIn}')) "+
			"	GROUP BY \"ITEM\", \"USER_ID\", \"WORK_AREA\", \"WORK_CENTER\", \"RESRCE_TYPE\", \"RESRCE\", \"ASSET\", \"RESRCE_DESCRIPTION\", \"SHIFT_START_DATE_TIME\","+
			"	     \"BY_TIME\", \"BY_TIME_CHART\", \"BY_TIME_MONTH\",\"BY_TIME_YEAR\", \"A\", \"P\", \"Q\", \"OEE\", \"OEE_VALUE\", \"PPM_THEORY\"  ")

    @Results({
    	    @Result(column = "ITEM", jdbcType = JdbcType.VARCHAR, property = "item"),
    	    @Result(column = "USER_ID", jdbcType = JdbcType.VARCHAR, property = "userId"),
    	    @Result(column = "WORK_AREA", jdbcType = JdbcType.VARCHAR, property = "workArea"),
    	    @Result(column = "WORK_CENTER", jdbcType = JdbcType.VARCHAR, property = "workCenter"),
    	    @Result(column = "RESRCE_TYPE", jdbcType = JdbcType.VARCHAR, property = "resrceType"),
    	    @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resrce"),
    	    @Result(column = "ASSET", jdbcType = JdbcType.VARCHAR, property = "asset"),
    	    @Result(column = "RESRCE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "resrceDescription"),
    	    @Result(column = "SHIFT_START_DATE_TIME", jdbcType = JdbcType.VARCHAR, property = "shiftStartTime"),
            @Result(column = "BY_TIME", jdbcType = JdbcType.VARCHAR, property = "byTime"),
            @Result(column = "BY_TIME_CHART", jdbcType = JdbcType.VARCHAR, property = "byTimeChart"),
            @Result(column = "BY_TIME_MONTH", jdbcType = JdbcType.VARCHAR, property = "byTimeMonth"),
            @Result(column = "BY_TIME_YEAR", jdbcType = JdbcType.VARCHAR, property = "byTimeYear"),
            @Result(column = "A", jdbcType = JdbcType.DOUBLE, property = "a"),
            @Result(column = "P", jdbcType = JdbcType.DOUBLE, property = "p"),
            @Result(column = "Q", jdbcType = JdbcType.DOUBLE, property = "q"),
            @Result(column = "OEE", jdbcType = JdbcType.DOUBLE, property = "oee"),
            @Result(column = "OEE_VALUE", jdbcType = JdbcType.DECIMAL, property = "oeeValue"),
            @Result(column = "QTY", jdbcType = JdbcType.DECIMAL, property = "qty"),
            @Result(column = "PPM_THEORY", jdbcType = JdbcType.DECIMAL, property = "ppmTherory")
    })
    List<OeeOeeTrendPO> selectOeeTrend(@Param("site") String site,
            @Param("workCenter") String workCenter,
            @Param("resourceBo") String resourceBo,
            @Param("endDate") String endDate,
            @Param("shiftLike") String shiftLike,
            @Param("startDate") String startDate,
            @Param("userIDin") String userIDin,
            @Param("modelIn") String modelIn,
            @Param("workCenterLink") String workCenterLink);
	
	
}
