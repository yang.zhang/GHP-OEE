package mhp.oee.dao.extend;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

/**
 * Created by LinZuK on 2016/8/29.
 */
public interface ResourceTypeResourceAndResourceTypePOMapper {
    @Select(" SELECT B.RESOURCE_TYPE from RESOURCE_TYPE_RESOURCE AS A " +
            " INNER JOIN RESOURCE_TYPE AS B ON B.HANDLE=A.RESOURCE_TYPE_BO " +
            " WHERE A.RESOURCE_BO=#{resourceBo} ")
    @Results({
            @Result(column = "RESOURCE_TYPE", jdbcType = JdbcType.VARCHAR, property = "resourcetype")
    })
    String select(@Param("resourceBo") String resourceBo);
}
