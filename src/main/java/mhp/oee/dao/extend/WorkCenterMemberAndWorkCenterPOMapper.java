package mhp.oee.dao.extend;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import mhp.oee.po.extend.WorkCenterMemberAndWorkCenterPO;
import mhp.oee.po.gen.WorkCenterPO;

public interface WorkCenterMemberAndWorkCenterPOMapper {
   @Select(" SELECT * FROM WORK_CENTER_MEMBER AS A INNER JOIN WORK_CENTER AS B ON A.WORK_CENTER_GBO = B.HANDLE "
            +" WHERE A.WORK_CENTER_BO = #{workCenter} "
            +" AND B.WC_CATEGORY = 'LEVEL1'"
            +" AND B.ENABLED = 'true' "
            +" AND B.SITE = #{site} "
            + "ORDER BY A.WORK_CENTER_GBO")
    @ResultMap("BaseResultMap")
    List<WorkCenterMemberAndWorkCenterPO> selectWorkCenterMemberAndWorkCenter(@Param("site")String site, @Param("workCenter")String workCenter);
   
    @Select({ "<script>"
            + "SELECT A.WORK_CENTER, A.DESCRIPTION, A.ENABLED FROM WORK_CENTER AS A LEFT JOIN WORK_CENTER_MEMBER AS B "
            + "ON A.HANDLE = B.WORK_CENTER_GBO "
            + "WHERE A.SITE = #{site} "
            + "<if test = 'workCenterBOList != null'>"
            + "AND B.WORK_CENTER_BO IN "
            + "<foreach close=')' collection='workCenterBOList' item='list' open='(' separator=','> "
            + "#{list}"
            + "</foreach>"
            + "</if>"
            + "<if test='lines != null and lines != \"\" ' > "
			+ " and "
			+ "<foreach close=' )' collection='lines' item='arrayItem' open=' ( ' separator='or'>"
			+ "  A.DESCRIPTION  like #{arrayItem} "
			+ "</foreach>"
            + "</if>"
            + "AND (A.WC_CATEGORY = 'LEVEL1' OR A.WC_CATEGORY = 'LEVEL3') "
            + "ORDER BY A.DESCRIPTION  "
            + "</script>"})
    @Results({
        @Result(column = "WORK_CENTER", jdbcType = JdbcType.VARCHAR, property = "workCenter"),
        @Result(column = "DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "description"),
        @Result(column = "ENABLED", jdbcType = JdbcType.VARCHAR, property = "enabled")
    })
    List<WorkCenterPO> selectLineByWorkCenter(
            @Param("site")String site, 
            @Param("workCenterBOList")List<String> workCenterBOList,
            
            //?????????
            @Param("lines")String[] lines);
    
    		//?????????
            /*@Param("lineArea")String lineArea);*/



    List<WorkCenterPO> selectLineByWorkCenterAndSites(@Param("siteList") List<String> siteList,
                                                      @Param("workCenterList") List<String> workCenterList,
                                                      @Param("lines") String[] lines);
}

