package mhp.oee.dao.extend;//package mhp.oee.dao.extend;

import mhp.oee.po.extend.ActivityFPO;
import mhp.oee.po.extend.ActivityRPO;
import mhp.oee.po.extend.ReasonCodePriorityAndReasonCodePO;
import mhp.oee.po.extend.ModifiedPO;
import mhp.oee.po.gen.ReasonCodePriorityPO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;

/**
 * Created by LinZuK on 2016/7/26.
 */
public interface ReasonCodePriorityAndReasonCodePOMapper {

    @Select({ "<script>" +
            " SELECT A.HANDLE AS REASON_CODE_PRIORITY_BO, " +
            " A.REASON_CODE_BO AS REASON_CODE_BO, " +
            " B.REASON_CODE, " +
            " B.DESCRIPTION AS REASON_CODE_DESC, " +
            " A.PRIORITY, " +
            " A.SUB_PRIORITY, " +
            " A.START_DATE_TIME AS EFF_DATE_TIME, " +
            " A.END_DATE_TIME AS EFF_END_DATE_TIME," +
            " A.USED " +
            " FROM REASON_CODE_PRIORITY AS A " +
            " INNER JOIN REASON_CODE AS B ON B.HANDLE = A.REASON_CODE_BO " +
            " AND B.SITE = #{site} " +
            " <if test=' reasonCodes != null '> " +
            "   AND B.REASON_CODE IN " +
            "   <foreach open='(' close=')' separator=',' collection='reasonCodes' item='reasonCode' >" +
            "       #{reasonCode} " +
            "   </foreach>" +
            " </if> " +
            " <if test=' priorities != null '> " +
            "   AND A.PRIORITY IN " +
            "   <foreach open='(' close=')' separator=',' collection='priorities' item='priority' >" +
            "       #{priority} " +
            "   </foreach>" +
            " </if> " +
            " AND A.START_DATE_TIME &gt;= #{dateTime} " +
            " ORDER BY B.REASON_CODE, A.PRIORITY, A.SUB_PRIORITY, A.START_DATE_TIME " +
            "</script>"})
    @Results({
            @Result(column = "REASON_CODE_PRIORITY_BO", jdbcType = JdbcType.VARCHAR, property = "reasonCodePriorityBo"),
            @Result(column = "REASON_CODE_BO", jdbcType = JdbcType.VARCHAR, property = "reasonCodeBo"),
            @Result(column = "REASON_CODE", jdbcType = JdbcType.VARCHAR, property = "reasonCode"),
            @Result(column = "REASON_CODE_DESC", jdbcType = JdbcType.VARCHAR, property = "reasonCodeDesc"),
            @Result(column = "PRIORITY", jdbcType = JdbcType.VARCHAR, property = "priority"),
            @Result(column = "SUB_PRIORITY", jdbcType = JdbcType.VARCHAR, property = "subPriority"),
            @Result(column = "EFF_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "effDateTime"),
            @Result(column = "EFF_END_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "effEndDateTime"),
            @Result(column = "USED", jdbcType = JdbcType.VARCHAR, property = "used")
    })
    List<ReasonCodePriorityAndReasonCodePO> selectHistoryChanges(@Param("site") String site,
                                                                 @Param("reasonCodes") String[] reasonCodes,
                                                                 @Param("priorities") String[] priorities,
                                                                 @Param("dateTime") Date dateTime);


    @Select(" SELECT A.* FROM " +
            " REASON_CODE_PRIORITY AS A " +
            " INNER JOIN REASON_CODE AS B " +
            " ON B.HANDLE=A.REASON_CODE_BO " +
            " WHERE B.SITE=#{site} " +
            " AND A.START_DATE_TIME <= #{endDate} " +
            " AND A.END_DATE_TIME > #{endDate} " +
            " ORDER BY PRIORITY,SUB_PRIORITY ")
    @Results({
            @Result(id = true, column = "HANDLE", jdbcType = JdbcType.VARCHAR, property = "handle"),
            @Result(column = "REASON_CODE_BO", jdbcType = JdbcType.VARCHAR, property = "reasonCodeBo"),
            @Result(column = "PRIORITY", jdbcType = JdbcType.VARCHAR, property = "priority"),
            @Result(column = "SUB_PRIORITY", jdbcType = JdbcType.VARCHAR, property = "subPriority"),
            @Result(column = "STRT", jdbcType = JdbcType.VARCHAR, property = "strt"),
            @Result(column = "START_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "startDateTime"),
            @Result(column = "END_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "endDateTime"),
            @Result(column = "USED", jdbcType = JdbcType.VARCHAR, property = "used"),
            @Result(column = "CREATED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "createdDateTime"),
            @Result(column = "MODIFIED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "modifiedDateTime"),
            @Result(column = "MODIFIED_USER", jdbcType = JdbcType.VARCHAR, property = "modifiedUser")
    })
    List<ReasonCodePriorityPO> selectReasonCodePriorityByCondition(@Param("site") String site,
                                                                   @Param("endDate") Date endDate);

    @Select({"<script>" +
            " SELECT A.HANDLE AS REASON_CODE_PRIORITY_BO, " +
            " A.REASON_CODE_BO AS REASON_CODE_BO, " +
            " B.REASON_CODE, " +
            " B.DESCRIPTION AS REASON_CODE_DESC, " +
            " A.PRIORITY, " +
            " A.SUB_PRIORITY, " +
            " A.START_DATE_TIME AS EFF_DATE_TIME, " +
            " A.END_DATE_TIME AS EFF_END_DATE_TIME," +
            " A.USED " +
            " FROM REASON_CODE_PRIORITY AS A " +
            " INNER JOIN REASON_CODE AS B " +
            " ON B.HANDLE = A.REASON_CODE_BO " +
            " WHERE B.SITE = #{site} " +
            " <if test=' reasonCodes != null '> " +
            "   AND B.REASON_CODE IN " +
            "   <foreach open='(' close=')' separator=',' collection='reasonCodes' item='reasonCode' >" +
            "       #{reasonCode} " +
            "   </foreach>" +
            " </if> " +
            " <if test=' priorities != null and priorities != \"\" '> " +
            "   AND A.PRIORITY IN " +
            "   <foreach open='(' close=')' separator=',' collection='priorities' item='priority' >" +
            "       #{priority} " +
            "   </foreach>" +
            " </if> " +
            " AND A.START_DATE_TIME &lt;= #{dateTime} " +
            " AND A.END_DATE_TIME &gt;= #{dateTime}" +
            " ORDER BY B.REASON_CODE, A.PRIORITY, A.SUB_PRIORITY, A.START_DATE_TIME " +
            "</script>"})
    @Results({
            @Result(column = "REASON_CODE_PRIORITY_BO", jdbcType = JdbcType.VARCHAR, property = "reasonCodePriorityBo"),
            @Result(column = "REASON_CODE_BO", jdbcType = JdbcType.VARCHAR, property = "reasonCodeBo"),
            @Result(column = "REASON_CODE", jdbcType = JdbcType.VARCHAR, property = "reasonCode"),
            @Result(column = "REASON_CODE_DESC", jdbcType = JdbcType.VARCHAR, property = "reasonCodeDesc"),
            @Result(column = "PRIORITY", jdbcType = JdbcType.VARCHAR, property = "priority"),
            @Result(column = "SUB_PRIORITY", jdbcType = JdbcType.VARCHAR, property = "subPriority"),
            @Result(column = "EFF_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "effDateTime"),
            @Result(column = "EFF_END_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "effEndDateTime"),
            @Result(column = "USED", jdbcType = JdbcType.VARCHAR, property = "used")
    })
    List<ReasonCodePriorityAndReasonCodePO> selectHistoryStatus(@Param("site") String site,
                                                                @Param("reasonCodes") String[] reasonCodes,
                                                                @Param("priorities") String[] priorities,
                                                                @Param("dateTime") Date dateTime);


    @Select(" SELECT B.ACTIVITY AS ACTIVITY_R, " +
            " C.DESCRIPTION AS ACTIVITY_R_DESC " +
            " FROM REASON_CODE_PRIORITY AS A " +
            " INNER JOIN REASON_CODE_RULE AS B " +
            " ON A.HANDLE = B.REASON_CODE_PRIORITY_BO " +
            " INNER JOIN ACTIVITY AS C " +
            " ON B.ACTIVITY = C.ACTIVITY " +
            " WHERE A.HANDLE = #{reasonCodePriorityBo} " +
            " AND B.RULE = 'RECOGNITION'")
    @Results({
            @Result(column = "ACTIVITY_R", jdbcType = JdbcType.VARCHAR, property = "activityR"),
            @Result(column = "ACTIVITY_R_DESC", jdbcType = JdbcType.VARCHAR, property = "activityRDesc")
    })
    ActivityRPO selectActivityR(@Param("reasonCodePriorityBo") String reasonCodePriorityBo);

    @Select(" SELECT B.ACTIVITY AS ACTIVITY_F, " +
            " C.DESCRIPTION AS ACTIVITY_F_DESC " +
            " FROM REASON_CODE_PRIORITY AS A " +
            " INNER JOIN REASON_CODE_RULE AS B " +
            " ON A.HANDLE = B.REASON_CODE_PRIORITY_BO " +
            " INNER JOIN ACTIVITY AS C " +
            " ON B.ACTIVITY = C.ACTIVITY " +
            " WHERE A.HANDLE = #{reasonCodePriorityBo} " +
            " AND B.RULE = 'FILTER'")
    @Results({
            @Result(column = "ACTIVITY_F", jdbcType = JdbcType.VARCHAR, property = "activityF"),
            @Result(column = "ACTIVITY_F_DESC", jdbcType = JdbcType.VARCHAR, property = "activityFDesc")
    })
    ActivityFPO selectActivityF(@Param("reasonCodePriorityBo") String reasonCodePriorityBo);

    @Select(" SELECT * FROM ( " +
            " ( SELECT MODIFIED_DATE_TIME, MODIFIED_USER FROM REASON_CODE_PRIORITY WHERE HANDLE = #{reasonCodePriorityBo} ) " +
            " UNION " +
            " ( SELECT MODIFIED_DATE_TIME, MODIFIED_USER FROM REASON_CODE_RULE WHERE REASON_CODE_PRIORITY_BO = #{reasonCodePriorityBo} ) " +
            " UNION " +
            " ( " +
            "   SELECT A.MODIFIED_DATE_TIME, A.MODIFIED_USER FROM REASON_CODE_RULE_PARAM AS A " +
            "   INNER JOIN REASON_CODE_RULE AS B ON A.REASON_CODE_RULE_BO = B.HANDLE " +
            "   WHERE B.REASON_CODE_PRIORITY_BO = #{reasonCodePriorityBo}) " +
            " ) A ORDER BY MODIFIED_DATE_TIME DESC LIMIT 1 ")
    @Results({
            @Result(column = "MODIFIED_DATE_TIME", jdbcType = JdbcType.VARCHAR, property = "modifiedDateTime"),
            @Result(column = "MODIFIED_USER", jdbcType = JdbcType.VARCHAR, property = "modifiedUser")
    })
    ModifiedPO selectModified(@Param("reasonCodePriorityBo") String reasonCodePriorityBo);

}
