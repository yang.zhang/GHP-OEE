package mhp.oee.dao.extend;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import mhp.oee.po.extend.ItemExtendPO;
import mhp.oee.po.gen.ItemPO;

public interface ItemExtendPOMapper {
   @Select({ "<script>"
            + "SELECT MODEL FROM ITEM WHERE SITE=#{site} AND MODEL IS NOT NULL "
            + "<if test = 'model != null and model != \"\" '>"
            +" and "
            +" <foreach close=\" )\" collection=\"model\" item=\"arrayItem\" open=\" ( \" separator=\"or\"> "
            +"  MODEL LIKE #{arrayItem} "
            +" </foreach> "
            + "</if>"
            + "GROUP BY MODEL"
            + " ORDER BY MODEL"
            + "</script>"})
   @Results({
       @Result(column = "MODEL", jdbcType = JdbcType.VARCHAR, property = "model"),
   })
    public List<ItemExtendPO> selectModel(@Param("site") String site, @Param("model") String[] model);
}
