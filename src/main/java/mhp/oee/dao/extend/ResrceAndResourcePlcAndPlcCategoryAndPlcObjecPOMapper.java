package mhp.oee.dao.extend;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;

import mhp.oee.po.extend.ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPO;

public interface ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPOMapper {
	
	@ResultMap("BaseResultMap")
	List<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPO> selectViewResrceAndCategoryAndObj(@Param("site") String site
			   , @Param("resource") String resource
			   , @Param("plcCategory") String plcCategory
			   , @Param("limit") Integer limit
			   , @Param("offset") Integer offset);
	
	@ResultMap("BaseResultMap")
	int selectViewResrceAndCategoryAndObjCount(@Param("site") String site
			   , @Param("resource") String resource
			   , @Param("plcCategory") String plcCategory);
	
}
