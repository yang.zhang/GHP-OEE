package mhp.oee.dao.extend;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import mhp.oee.po.extend.ReasonCodeAlertAndResourceTypeAndReasonCodePO;

public interface ReasonCodeAlertAndResourceTypeAndReasonCodePOMpper {
    @Select({ "<script>"
            + "SELECT A.*, B.REASON_CODE, B.DESCRIPTION AS REASON_CODE_DES, C.RESOURCE_TYPE, C.DESCRIPTION AS RESOURCE_TYPE_DES FROM REASON_CODE_ALERT AS A INNER JOIN REASON_CODE AS B ON A.REASON_CODE_BO = B.HANDLE "
            + "INNER JOIN RESOURCE_TYPE AS C "
            + "ON A.RESOURCE_TYPE_BO = C.HANDLE "            
            + "WHERE C.SITE = #{site} "
            + "AND C.RESOURCE_TYPE IN "
            + "<foreach close=')' collection='resourceTypes' item='resourceType' open='(' separator=','> "
            + " #{resourceType} "
            + "</foreach>"
            + "AND A.START_DATE_TIME &lt;= #{time} "
            + "AND A.END_DATE_TIME &gt; #{time} "
            + "ORDER BY A.RESOURCE_TYPE_BO, A.ALERT_SEQUENCE_ID, A.START_DATE_TIME"
            + "</script>"})
    @ResultMap("BaseResultMap")
    public List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> selectReasonCodeAlertCurent(
            @Param("resourceTypes") String [] resourceTypes,
            @Param("time") Date time,
            @Param("site") String site);
    
    public List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> selectReasonCodeAlertCurentPage(
            @Param("resourceTypes") String [] resourceTypes,
            @Param("site") String site,
            @Param("time") Date time,
            @Param("limit") int limit,
            @Param("offset") int offset);
    
    public int selectReasonCodeAlertCurentPageCount(
            @Param("resourceTypes") String [] resourceTypes,
            @Param("time") Date time,
            @Param("site") String site);
    
    public List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> selectReasonCodeAlertHisChange(
            @Param("resourceTypes") String [] resourceTypes,
            @Param("site") String site,
            @Param("time") Date time,
            @Param("limit") int limit,
            @Param("offset") int offset);
    
    public int selectReasonCodeAlertHisChangeCount(
            @Param("resourceTypes") String [] resourceTypes,
            @Param("site") String site,
            @Param("time") Date time);
    
    public List<ReasonCodeAlertAndResourceTypeAndReasonCodePO> selectReasonCodeAlertHisStatus(
            @Param("resourceTypes") String [] resourceTypes,
            @Param("site") String site,
            @Param("time") Date time,
            @Param("limit") int limit,
            @Param("offset") int offset);
    
    public int selectReasonCodeAlertHisStatusCount(
            @Param("resourceTypes") String [] resourceTypes,
            @Param("site") String site,
            @Param("time") Date time);
    
   
}
