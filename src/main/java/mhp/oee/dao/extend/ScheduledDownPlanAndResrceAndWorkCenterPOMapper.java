package mhp.oee.dao.extend;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import mhp.oee.po.extend.ScheduledDownPlanAndResrceAndWorkCenterPO;
import mhp.oee.vo.ResourceConditionVO;

public interface ScheduledDownPlanAndResrceAndWorkCenterPOMapper {
    
    List<ScheduledDownPlanAndResrceAndWorkCenterPO> selectScheduledDownPlanAndResrceAndWorkCenter(
            @Param("site") String site, 
            @Param("resrces") List<ResourceConditionVO> resrces, 
            @Param("date") Date dateTime,
            @Param("limit") int limit,
            @Param("offset") int offset);
    
    int selectScheduledDownPlanAndResrceAndWorkCenterCount(
            @Param("site")String site, 
            @Param("resrces")List<ResourceConditionVO> resrces, 
            @Param("date")Date dateTime);

}
