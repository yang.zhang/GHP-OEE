package mhp.oee.dao.extend;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;
import mhp.oee.po.extend.ResourcePlcInfoAndResrceAndPlcAccessTypePO;
import mhp.oee.vo.PlcInfoConditionVO;
import mhp.oee.vo.ResourcePlcInfoVO;

public interface ResourcePlcInfoAndResrceAndPlcAccessTypePOMapper {

    List<ResourcePlcInfoAndResrceAndPlcAccessTypePO> selectResPlcInfoAndResrceAndPlcAccessType(@Param("plcInfoConditionVO") PlcInfoConditionVO plcInfoConditionVO, @Param("resSet") Object[] resList);

    int selectResPlcInfoAndResrceAndPlcAccessTypeCount(@Param("plcInfoConditionVO") PlcInfoConditionVO plcInfoConditionVO, @Param("resSet") Object[] resList);
    
    @Select("SELECT DISTINCT PC_IP FROM RESRCE AS A " +
            "INNER JOIN RESOURCE_PLC_INFO AS B " +
            "ON A.HANDLE = B.RESOURCE_BO " +
            "AND A.ENABLED = 'true' " +
            "AND A.CONFIGURED_PLC_INFO = 'true' " +
            "AND PC_IP IS NOT NULL")
    @Results({
        @Result(column = "PC_IP", jdbcType = JdbcType.VARCHAR, property = "pcIp")
    })
    List<ResourcePlcInfoVO> selectPcIp();
}
