package mhp.oee.dao.extend;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import mhp.oee.po.extend.ProductionOutputTargetAndResrcePO;
import mhp.oee.vo.ResourceConditionVO;

public interface ProductionOutputTargetAndResrcePOMapper {
    @Select({ "<script>"
            + "SELECT A.*,B.HANDLE AS RESOUCE_BO, B.RESRCE, B.DESCRIPTION AS RESRCE_DES, B.ASSET, "
            + "C.ITEM, C.DESCRIPTION AS ITEM_DES, C.REVISION, C.MODEL, C.ENABLED "
            + "FROM  RESRCE AS B INNER JOIN PRODUCTION_OUTPUT_TARGET AS A "
            + "ON A.RESRCE = B.RESRCE AND A.SITE = B.SITE "
            + "INNER JOIN ITEM AS C ON A.ITEM = C.ITEM AND A.SITE = C.SITE "
            + "WHERE "
            + "A.SITE = #{site} "
            + "AND A.RESRCE IN "
            + "<foreach close=')' collection='resourceConditionVOs' item='listItem' open='(' separator=','> "
            + "#{listItem.resrce}"
            + "</foreach> "
            + "AND A.ITEM = #{item} "
            + "AND A.SHIFT = #{shift} "
            + "AND (A.TARGET_DATE &gt;= #{startDateTime} "
            + "AND A.TARGET_DATE &lt;= #{endDateTime} "
            + "OR A.TARGET_DATE = '*')"
            + "</script>" })
    @ResultMap("BaseResultMap")
    List<ProductionOutputTargetAndResrcePO> selectProductionOutputTargetAndResrce(
            @Param("site") String site, 
            @Param("item") String item, 
            @Param("resourceConditionVOs") List<ResourceConditionVO> resourceConditionVOs, 
            @Param("shift") String shift,
            @Param("startDateTime") String startDateTime,
            @Param("endDateTime") String endDateTime);
    
}
