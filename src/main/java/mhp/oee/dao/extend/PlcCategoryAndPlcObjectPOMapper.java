package mhp.oee.dao.extend;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import mhp.oee.po.extend.PlcCategoryAndPlcObjectPO;
import mhp.oee.po.extend.PlcObjectAndPlcCategoryPO;

public interface PlcCategoryAndPlcObjectPOMapper {
    @Select("SELECT A.CATEGORY AS CATEGORY, A.DESCRIPTION, B.PLC_OBJECT AS PLC_OBJECT, B.PLC_OBJECT_DESCRIPTION "
            + "FROM PLC_CATEGORY AS A LEFT JOIN PLC_OBJECT AS B "
            + "ON A.CATEGORY = B.CATEGORY AND A.SITE = B.SITE  WHERE  A.CATEGORY = #{category} AND A.SITE= #{site} "
            + "ORDER BY B.PLC_OBJECT")
    @ResultMap("BaseResultMap")
    PlcCategoryAndPlcObjectPO selectPlcCategoryAndPlcObject(@Param("site")String site, @Param("category")String category);

    List<PlcObjectAndPlcCategoryPO> selectPlcObjectAndPlcCategory(
            @Param("site")String site, 
            @Param("category")String category,
            @Param("limit")int limit,
            @Param("offset")int offset);
    
    int selectPlcObjectAndPlcCategoryCount(@Param("site")String site, @Param("category")String category);
    
}
