package mhp.oee.dao.extend;

import mhp.oee.po.extend.OeeOeePO;
import mhp.oee.po.extend.OeeReasonCodeGroupPO;
import mhp.oee.po.extend.OeeReasonCodePO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * Created by SuMingzhi on 2016/11/15.
 */
public interface OeeDetailPOMapper {
	
	@Select(" SELECT \"MODEL\", \"WORK_AREA\", \"WORK_CENTER\", \"RESRCE_TYPE\", \"RESRCE\", \"ASSET\", \"RESRCE_DESCRIPTION\", \"SHIFT_START_DATE_TIME\", SUBSTR(\"BY_TIME\",12,8) AS \"BY_TIME\",  "+
			" \"BY_TIME_CHART\", \"A\", \"P\", \"Q\", \"OEE\", \"OEE_VALUE\", \"PPM_THEORY\", count(\"QTY\") AS \"QTY\"  "+
			" FROM \"_SYS_BIC\".\"com.andon.report.oee/OEE_CAL_DETAIL\"  "+
			" ("+
			"    'PLACEHOLDER' = ('$$site$$', '${site}'),  "+
			"    'PLACEHOLDER' = ('$$workCenter$$', '${workCenter}'),  "+
			"    'PLACEHOLDER' = ('$$resourceBo$$', '${resourceBo}'),  "+
			"    'PLACEHOLDER' = ('$$endDate$$', '${endDate}'),  "+
			"    'PLACEHOLDER' = ('$$shiftLike$$', '${shiftLike}'),  "+
			"    'PLACEHOLDER' = ('$$startDate$$', '${startDate}'),  "+
			"    'PLACEHOLDER' = ('$$workCenterLink$$', '${workCenterLink}'))  "+
			" GROUP BY \"MODEL\", \"WORK_AREA\", \"WORK_CENTER\", \"RESRCE_TYPE\", \"RESRCE\", \"ASSET\", \"RESRCE_DESCRIPTION\", \"SHIFT_START_DATE_TIME\", "+
			" \"BY_TIME\", \"BY_TIME_CHART\", \"A\", \"P\", \"Q\", \"OEE\", \"OEE_VALUE\", \"PPM_THEORY\" ")

    @Results({
    	    @Result(column = "MODEL", jdbcType = JdbcType.VARCHAR, property = "model"),
    	    @Result(column = "WORK_AREA", jdbcType = JdbcType.VARCHAR, property = "workArea"),
    	    @Result(column = "WORK_CENTER", jdbcType = JdbcType.VARCHAR, property = "workCenter"),
    	    @Result(column = "RESRCE_TYPE", jdbcType = JdbcType.VARCHAR, property = "resrceType"),
    	    @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resrce"),
    	    @Result(column = "ASSET", jdbcType = JdbcType.VARCHAR, property = "asset"),
    	    @Result(column = "RESRCE_DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "resrceDescription"),
    	    @Result(column = "SHIFT_START_DATE_TIME", jdbcType = JdbcType.VARCHAR, property = "shiftStartTime"),
            @Result(column = "BY_TIME", jdbcType = JdbcType.VARCHAR, property = "byTime"),
            @Result(column = "BY_TIME_CHART", jdbcType = JdbcType.VARCHAR, property = "byTimeChart"),
            @Result(column = "A", jdbcType = JdbcType.VARCHAR, property = "a"),
            @Result(column = "P", jdbcType = JdbcType.VARCHAR, property = "p"),
            @Result(column = "Q", jdbcType = JdbcType.VARCHAR, property = "q"),
            @Result(column = "OEE", jdbcType = JdbcType.VARCHAR, property = "oee"),
            @Result(column = "OEE_VALUE", jdbcType = JdbcType.DECIMAL, property = "oeeValue"),
            @Result(column = "QTY", jdbcType = JdbcType.DECIMAL, property = "qty"),
            @Result(column = "PPM_THEORY", jdbcType = JdbcType.DECIMAL, property = "ppmTherory")
    })
    List<OeeOeePO> selectOeeDetail(@Param("site") String site,
            @Param("workCenter") String workCenter,
            @Param("resourceBo") String resourceBo,
            @Param("endDate") String endDate,
            @Param("shiftLike") String shiftLike,
            @Param("startDate") String startDate,
            @Param("workCenterLink") String workCenterLink);
	
	
}
