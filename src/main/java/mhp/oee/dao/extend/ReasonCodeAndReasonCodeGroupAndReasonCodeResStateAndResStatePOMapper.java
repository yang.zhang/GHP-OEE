package mhp.oee.dao.extend;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import mhp.oee.po.extend.ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStatePO;

public interface ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStatePOMapper {

    List<ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStatePO> selectViewReasonCodeAndGroupAndResStates(
            @Param("site") String site, @Param("reasonCodeGroup") String reasonCodeGroup,
            @Param("reasonCode") String reasonCode, @Param("dateTime") Date dateTime, @Param("limit") int limit,
            @Param("offset") int offset);

    int selectViewReasonCodeAndGroupAndResStatesCount(@Param("site") String site,
            @Param("reasonCodeGroup") String reasonCodeGroup, @Param("reasonCode") String reasonCode,
            @Param("dateTime") Date dateTime);
}
