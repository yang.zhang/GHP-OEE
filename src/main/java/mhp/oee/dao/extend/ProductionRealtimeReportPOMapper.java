package mhp.oee.dao.extend;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import mhp.oee.utils.Utils;
import mhp.oee.vo.ProductionRealtimeVo;
import mhp.oee.vo.ResourceStatusDetailVO;
import mhp.oee.vo.ShutdownReasonVO;

/**
 * 
 * @author ZhengXX
 *生产微计划看板
 */
public interface ProductionRealtimeReportPOMapper {
   
    
    
    @Select({ "<script> "
    	
    	/*************************第三层 bebig***********************************/

    	+ " select * from ( "

    	+ "	select  RESULT_DATE,	 WORK_CENTER_CODE, WORK_CENTER_DESCRIPTION,  SITE, RESRCE, RESOURCE_BO, RESRCE_DESCRIPTION, ASSET, LINE_AREA_CODE, RESOURCE_TYPE,"
    	+ "		LINE_AREA_DESCRIPTION, CASE WHEN  PRODUCT_ITEM IS NULL THEN '-' ELSE PRODUCT_ITEM END AS PRODUCT_ITEM, "
    	+ " CASE WHEN MODEL IS NULL THEN '-' ELSE MODEL END AS MODEL, MIN(DAY_SHIFT_START_DATE_TIME) AS DAY_SHIFT_START_DATE_TIME ,MAX(DAY_SHIFT_END_DATE_TIME)  AS DAY_SHIFT_END_DATE_TIME, "

    	+ "	 CASE WHEN sum(TARGE_QTY_YIELD) IS NULL THEN -1 ELSE sum(TARGE_QTY_YIELD) END as DAY_TARGE_QTY_YIELD, "
    	+ "	 case when sum(de_num) = 0  AND sum(TARGE_QTY_YIELD) IS  NULL THEN  -1 WHEN sum(de_num) = 0 then sum(TARGE_QTY_YIELD) "
    	+ "	 	else   CAST((sum(TARGE_QTY_YIELD))/sum(de_num) AS  DECIMAL(18,1))  end as PART_TARGE_QTY_YIELD, "
    	+ "	 CASE WHEN sum(case when shift LIKE '%M%'  then PRO_SHIFT_TOTAL end) IS NULL THEN -1 ELSE sum(case when shift LIKE '%M%'  then PRO_SHIFT_TOTAL end) END AS M_PRO_SHIFT_TOTAL, "
    	+ "	 CASE WHEN sum(case when shift LIKE '%E%'  then PRO_SHIFT_TOTAL end) IS NULL THEN -1 ELSE sum(case when shift LIKE '%E%'  then PRO_SHIFT_TOTAL end) END AS E_PRO_SHIFT_TOTAL, "
    	+ "	 CASE WHEN sum( PRO_SHIFT_TOTAL)  IS NULL THEN -1 ELSE sum( PRO_SHIFT_TOTAL) END AS DAY_PRO_SHIFT_TOTAL, "


    	+ "ROW_NUMBER() OVER(PARTITION BY SITE,RESULT_DATE,RESOURCE_BO ORDER BY  PRODUCT_ITEM DESC) AS R_NUM, "

    	+ "<foreach close='' collection='paras' item='vo' open='' separator=','>"
    	+ "   CASE WHEN MIN(DAY_SHIFT_START_DATE_TIME) >= concat(ADD_DAYS(RESULT_DATE,${vo.getEndDateAddDay()}),'${vo.getEndTimeForCompare()}') OR concat(ADD_DAYS(RESULT_DATE,${vo.getStartDateAddDay()}),'${vo.getStartTimeForCompare()}') >= MAX(DAY_SHIFT_END_DATE_TIME)  THEN -2 "
    	+ "	WHEN sum(${vo.getColumnTime()}) IS NULL  THEN -1 ELSE  sum(${vo.getColumnTime()}) END AS  ${vo.getColumnTime()}"
    	+ "</foreach>"
    		 
    	/*************************第二层 bebig***********************************/
    	+ " FROM(SELECT  RESULT_DATE,	 WORK_CENTER_CODE, WORK_CENTER_DESCRIPTION, SHIFT, SHIFT_DES, SHIFT_START_DATE_TIME, "
    	+ " SHIFT_END_DATE_TIME, DAY_SHIFT_START_DATE_TIME,DAY_SHIFT_END_DATE_TIME,SITE, RESRCE, RESOURCE_BO, RESRCE_DESCRIPTION, ASSET, LINE_AREA_CODE, RESOURCE_TYPE, "
    	+ " LINE_AREA_DESCRIPTION,  PRODUCT_ITEM,MODEL, TARGE_QTY_YIELD, "
    	+ " case when TARGE_QTY_YIELD = 0 or TARGE_QTY_YIELD  is null then 0 ELSE ${colNum/2}  END AS de_num, "
    	+ " sum(QTY_TOTAL) as PRO_SHIFT_TOTAL, "

    		
    	+ "<foreach close='' collection='paras' item='vo' open='' separator=','> "
    	+ "  SUM(case when (PRO_START_DATE_TIME >= concat(ADD_DAYS(RESULT_DATE,${vo.getStartDateAddDay()}),'${vo.getStartTimeForCompare()}') "
    	+ "		and (concat(ADD_DAYS(RESULT_DATE,${vo.getEndDateAddDay()}),'${vo.getEndTimeForCompare()}') > PRO_START_DATE_TIME)  ) then  QTY_TOTAL end) as  ${vo.getColumnTime()}"
    	+ "</foreach>"

    	+ " FROM( "
    	/*************************第一层 bebig***********************************/
    	+ " SELECT RESULT_DATE, WORK_CENTER_CODE, WORK_CENTER_DESCRIPTION, SHIFT, SHIFT_DES, SHIFT_START_DATE_TIME, "
    	+ "	 SHIFT_END_DATE_TIME, DAY_SHIFT_START_DATE_TIME,DAY_SHIFT_END_DATE_TIME,SITE, RESRCE, RESOURCE_BO, RESRCE_DESCRIPTION, ASSET, LINE_AREA_CODE, "
    	+ "	 RESOURCE_TYPE, LINE_AREA_DESCRIPTION,TARGE_ITEM ,PRODUCT_ITEM,MODEL, QTY_TOTAL, PRO_START_DATE_TIME, "
    	+ "	 PRO_END_DATE_TIME,	 TARGE_QTY_YIELD, TARGET_DATE,QTY "
    	+ " FROM \"_SYS_BIC\".\"com.andon.report.production_realtime_dashboard/PRODUCTION_REALTIME_DASHBOARD\"  "
    	+ "	('PLACEHOLDER' = ('$$model_in$$', '${model}'),	"
    	+ "	 'PLACEHOLDER' = ('$$byTimetype$$',''), "
    	+ "	 'PLACEHOLDER' = ('$$item_in$$','${item}'), "
    	+ "	 'PLACEHOLDER' = ('$$resrceIn$$','${resrceIn}'), "
    	+ "	 'PLACEHOLDER' = ('$$endDate$$','${endDate}'), "
    	+ "	 'PLACEHOLDER' = ('$$siteIn$$','${siteIn}'), "
    	+ "	 'PLACEHOLDER' = ('$$shift$$','${shift}'), "
    	+ "	 'PLACEHOLDER' = ('$$workCenterIn$$','${workCenter}'), "
    	+ "	 'PLACEHOLDER' = ('$$startDate$$','${startDate}'), "
    	+ "	 'PLACEHOLDER' = ('$$lineIn$$','${lineIn}'),'PLACEHOLDER' = ('$$resourceType$$','${resourceType}' "
    	/*************************第一层 END***********************************/	 
    	+ "	 ))ORDER BY RESULT_DATE,RESOURCE_BO) "
    	+ "	 group by  RESULT_DATE, WORK_CENTER_CODE, WORK_CENTER_DESCRIPTION, SHIFT, SHIFT_DES, " 
    	+ "	 SHIFT_START_DATE_TIME,	 SHIFT_END_DATE_TIME, DAY_SHIFT_START_DATE_TIME,DAY_SHIFT_END_DATE_TIME,SITE, RESRCE, RESOURCE_BO, RESRCE_DESCRIPTION, "
    	+ "	 ASSET, LINE_AREA_CODE, RESOURCE_TYPE, "
    	+ "	 LINE_AREA_DESCRIPTION,  PRODUCT_ITEM,MODEL,TARGE_QTY_YIELD "
    		 
    	+ "	 ORDER BY RESULT_DATE,RESOURCE_BO "
    	+ "	 ) "
    	/*************************第二层 END***********************************/	 

    		 
    	+ "	 group by RESULT_DATE,	 WORK_CENTER_CODE, WORK_CENTER_DESCRIPTION,  SITE, RESRCE, "
    	+ "	 RESOURCE_BO, RESRCE_DESCRIPTION, ASSET, LINE_AREA_CODE, RESOURCE_TYPE, "
    		 
    	+ "	 LINE_AREA_DESCRIPTION,  PRODUCT_ITEM ,MODEL"
    		 
    		 
    	+ "	 ORDER BY RESULT_DATE,RESOURCE_BO "
    	/*************************第三层 END***********************************/
    	+ " ) where PRODUCT_ITEM != '-' or R_NUM = 1 "
    	+ "	 ORDER BY RESULT_DATE,SITE,WORK_CENTER_DESCRIPTION,LINE_AREA_DESCRIPTION,RESOURCE_BO ,MODEL ,PRODUCT_ITEM "




+ "<if test=\"limit != null\">"
+ "<if test=\"offset != null\">"
+    "limit ${limit} offset ${offset}"
+ "</if>"
+ "<if test=\"offset == null\">"
+    "limit ${limit}"
+ "</if>"
+ "</if>"




            + "</script>" })
    /**
     * 生产微计划看板
     */
    public List<Map> getProductionRealtime(
            @Param("siteIn") String siteIn, 
    		@Param("workCenter") String workCenter, 
    		@Param("lineIn") String  lineIn,
    		@Param("resourceType") String  resourceType,
            @Param("resrceIn") String resrceIn,  
    		@Param("startDate") String startDate,  
    		@Param("endDate")String  endDate,
    		@Param("shift")String shift,
    		@Param("model")String model,
    		@Param("item")String item,
    		@Param("colNum")int colNum,
    		@Param("paras")List<ProductionRealtimeVo> paras,
    		@Param("limit") Integer pageSize,
            @Param("offset") Integer currPage);
}
