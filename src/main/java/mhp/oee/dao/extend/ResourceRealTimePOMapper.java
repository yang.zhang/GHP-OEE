package mhp.oee.dao.extend;

import mhp.oee.po.extend.ResourceRealTimeReportPO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;


public interface ResourceRealTimePOMapper {

    @Select("SELECT \"SITE\", \"HANDLE\", \"WORK_AREA\", \"LINE_AREA\", \"RESRCE\", " +
            "\"RESRCE_NAME\",\"LINE_AREA_DES\",\"WORK_AREA_DES\",\"RESOURCE_STATE\", " +
            "\"REALTIME_PPM\", \"COLOR_LIGHT\", \"ALERT_COUNT\",\"HALT_COUNT\",\"DATE_TIME\",\"QTY_TOTAL\",\"ORDER_FIELD\", " +
            " \"ITEM\",\"ALERT_TIME\",\"DESCRIPTION\" " +
            "FROM \"_SYS_BIC\".\"com.andon.report.resource_realtime_dashboard/RESOURCE_REALTIME\" "
            + "("
            + "'PLACEHOLDER' = ('$$resource_type_in$$', '${resourceType}'), "
            + "'PLACEHOLDER' = ('$$site_in$$', '${site}'), "
            + "'PLACEHOLDER' = ('$$resrce_in$$', '${resrce}'), "
            + "'PLACEHOLDER' = ('$$line_in$$', '${line}'), "
            + "'PLACEHOLDER' = ('$$work_center_in$$','${workCenter}'), "
            + "'PLACEHOLDER' = ('$$work_area_in$$','${workArea}') "
            + ")"
            +" GROUP BY COLOR_LIGHT,SITE,HANDLE,WORK_AREA,LINE_AREA,RESRCE,RESRCE_NAME,LINE_AREA_DES,WORK_AREA_DES, "
            + "RESOURCE_STATE,REALTIME_PPM,ALERT_COUNT,HALT_COUNT,DATE_TIME,QTY_TOTAL,ORDER_FIELD,DESCRIPTION,ITEM,ALERT_TIME,DESCRIPTION"
            + " ORDER BY ORDER_FIELD,RESRCE,ALERT_TIME,DESCRIPTION"
            + "")
    @Results({
            @Result(column = "SITE", jdbcType = JdbcType.VARCHAR, property = "site"),
            @Result(column = "HANDLE", jdbcType = JdbcType.VARCHAR, property = "handle"),
            @Result(column = "WORK_AREA", jdbcType = JdbcType.VARCHAR, property = "workArea"),
            @Result(column = "LINE_AREA", jdbcType = JdbcType.VARCHAR, property = "lineArea"),
            @Result(column = "RESRCE", jdbcType = JdbcType.VARCHAR, property = "resrce"),
            @Result(column = "RESRCE_NAME", jdbcType = JdbcType.VARCHAR, property = "resrceDes"),
            @Result(column = "LINE_AREA_DES", jdbcType = JdbcType.VARCHAR, property = "lineAreaDes"),
            @Result(column = "WORK_AREA_DES", jdbcType = JdbcType.VARCHAR, property = "workAreaDes"),
            @Result(column = "RESOURCE_STATE", jdbcType = JdbcType.VARCHAR, property = "resourceState"),
            @Result(column = "REALTIME_PPM", jdbcType = JdbcType.VARCHAR, property = "ppm"),
            @Result(column = "COLOR_LIGHT", jdbcType = JdbcType.VARCHAR, property = "colorLight"),
            @Result(column = "ALERT_COUNT", jdbcType = JdbcType.VARCHAR, property = "alertCount"),//报警次数
            @Result(column = "HALT_COUNT", jdbcType = JdbcType.VARCHAR, property = "haltCount"),//停机次数
            @Result(column = "DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "dateTime"),
            @Result(column = "QTY_TOTAL", jdbcType = JdbcType.INTEGER, property = "qtyTotal"),
            @Result(column = "ITEM", jdbcType = JdbcType.VARCHAR, property = "item"),
            @Result(column = "ALERT_TIME", jdbcType = JdbcType.TIMESTAMP, property = "alertTime"),
            @Result(column = "DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "description")
    })
    List<ResourceRealTimeReportPO> selectResourceRealTime(
                                @Param("resourceType") String resourceType,
                                @Param("site") String site,
                                @Param("resrce") String resrce,
                                @Param("line") String line,
                                @Param("workCenter") String workCenter,
                                @Param("workArea") String workArea);
    
}
