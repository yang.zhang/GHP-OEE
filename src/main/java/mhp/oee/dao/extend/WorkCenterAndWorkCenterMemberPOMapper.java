package mhp.oee.dao.extend;

import mhp.oee.po.extend.WorkCenterAndWorkCenterMemberPO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * Created by LinZuK on 2016/7/21.
 */
public interface WorkCenterAndWorkCenterMemberPOMapper {
    @Select(" SELECT WORK_CENTER, DESCRIPTION FROM WORK_CENTER AS A INNER JOIN WORK_CENTER_MEMBER AS B "
            +" ON A.HANDLE = B.WORK_CENTER_GBO "
            +" WHERE B.WORK_CENTER_BO = #{workCenterHandle} "
            +" AND A.WC_CATEGORY = 'LEVEL1' "
            +" AND A.ENABLED = 'true' "
            +" AND A.SITE = #{site} ")
    @Results({
        @Result(column = "WORK_CENTER", jdbcType = JdbcType.VARCHAR, property = "workCenter"),
        @Result(column = "DESCRIPTION", jdbcType = JdbcType.VARCHAR, property = "description")
    })
    List<WorkCenterAndWorkCenterMemberPO> selectWorkCenterAndWorkCenterMember(@Param("site") String site, @Param("workCenterHandle") String workCenterHandle);
}
