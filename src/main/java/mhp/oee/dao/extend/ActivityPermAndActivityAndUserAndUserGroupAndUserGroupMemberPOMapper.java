package mhp.oee.dao.extend;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import mhp.oee.po.extend.ActivityPermAndActivityAndUserAndUserGroupAndUserGroupMemberPO;

public interface ActivityPermAndActivityAndUserAndUserGroupAndUserGroupMemberPOMapper {
    @Select("SELECT * FROM VIEW___ACT_PERM__ACT__USR__USR_GROUP__USR_GROUP_MEMBER "
            + "WHERE SITE = #{site} "
            + "AND USER_ID = #{userId}"
            + "AND ACTIVITY = #{activity}"
            + "AND PERMISSION_MODE = #{permission}")
    @ResultMap("BaseResultMap")
    public ActivityPermAndActivityAndUserAndUserGroupAndUserGroupMemberPO selectActivityPermByUsr(
            @Param("site") String site,
            @Param("userId") String userId,
            @Param("activity") String activity,
            @Param("permission") String permission);
}
