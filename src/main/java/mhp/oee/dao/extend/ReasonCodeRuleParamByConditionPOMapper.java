package mhp.oee.dao.extend;

import mhp.oee.po.gen.ReasonCodeRulePO;
import mhp.oee.po.gen.ReasonCodeRuleParamPO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * Created by LinZuK on 2016/8/29.
 */
public interface ReasonCodeRuleParamByConditionPOMapper {
    @Select(" SELECT * FROM REASON_CODE_RULE_PARAM " +
            " WHERE REASON_CODE_RULE_BO=#{reasonCodeRuleBo} " +
            " AND (RESOURCE_TYPE='*' OR RESOURCE_TYPE=#{resourceType}) " +
            " ORDER BY PARAM_ID ")
    @Results({
            @Result(id = true, column = "HANDLE", jdbcType = JdbcType.VARCHAR, property = "handle"),
            @Result(column = "SITE", jdbcType = JdbcType.VARCHAR, property = "site"),
            @Result(column = "REASON_CODE_RULE_BO", jdbcType = JdbcType.VARCHAR, property = "reasonCodeRuleBo"),
            @Result(column = "RESOURCE_TYPE", jdbcType = JdbcType.VARCHAR, property = "resourceType"),
            @Result(column = "PARAM_ID", jdbcType = JdbcType.VARCHAR, property = "paramId"),
            @Result(column = "PARAM_VALUE", jdbcType = JdbcType.VARCHAR, property = "paramValue"),
            @Result(column = "CREATED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "createdDateTime"),
            @Result(column = "MODIFIED_DATE_TIME", jdbcType = JdbcType.TIMESTAMP, property = "modifiedDateTime"),
            @Result(column = "MODIFIED_USER", jdbcType = JdbcType.VARCHAR, property = "modifiedUser")
    })
    List<ReasonCodeRuleParamPO> select(@Param("reasonCodeRuleBo") String reasonCodeRuleBo,
                                       @Param("resourceType") String resourceType);

}
