package mhp.oee.dao.extend;

import mhp.oee.po.extend.ResourceAndWorkCenterShiftAndWorkCenterPO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by LinZuK on 2016/7/25.
 */
public interface ResrceAndWorkCenterShiftAndWorkCenterPOMapper {
    @Select(" SELECT C.SHIFT, C.DESCRIPTION, C.START_TIME, C.END_TIME " +
            " FROM RESRCE AS A " +
            " INNER JOIN WORK_CENTER AS B ON B.WORK_CENTER = A.WORK_AREA " +
            " INNER JOIN WORK_CENTER_SHIFT AS C ON C.WORK_CENTER_BO = B.HANDLE " +
            " WHERE A.RESRCE = #{resrce} " +
            " AND A.SITE = #{site} " +
            " AND B.SITE = #{site} " +
            " AND C.ENABLED = 'true' " +
            " ORDER BY C.SHIFT")
    @ResultMap("BaseResultMap")
    List<ResourceAndWorkCenterShiftAndWorkCenterPO> selectResrceAndWorkCenterAndWorkCenterShift(@Param("site") String site,
                                                                                                @Param("resrce") String resrce);
}
