package mhp.oee.dao.gen;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import mhp.oee.po.gen.UserPO;
import mhp.oee.po.gen.UserPOExample;

public interface UserPOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table usr
     *
     * @mbggenerated
     */
    int countByExample(UserPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table usr
     *
     * @mbggenerated
     */
    int deleteByExample(UserPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table usr
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(String handle);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table usr
     *
     * @mbggenerated
     */
    int insert(UserPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table usr
     *
     * @mbggenerated
     */
    int insertSelective(UserPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table usr
     *
     * @mbggenerated
     */
    List<UserPO> selectByExample(UserPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table usr
     *
     * @mbggenerated
     */
    UserPO selectByPrimaryKey(String handle);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table usr
     *
     * @mbggenerated
     */
    int updateByExampleSelective(@Param("record") UserPO record, @Param("example") UserPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table usr
     *
     * @mbggenerated
     */
    int updateByExample(@Param("record") UserPO record, @Param("example") UserPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table usr
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(UserPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table usr
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(UserPO record);
}