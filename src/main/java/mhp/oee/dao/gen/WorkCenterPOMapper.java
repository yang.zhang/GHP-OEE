package mhp.oee.dao.gen;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import mhp.oee.po.gen.WorkCenterPO;
import mhp.oee.po.gen.WorkCenterPOExample;

public interface WorkCenterPOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table work_center
     *
     * @mbggenerated
     */
    int countByExample(WorkCenterPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table work_center
     *
     * @mbggenerated
     */
    int deleteByExample(WorkCenterPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table work_center
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(String handle);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table work_center
     *
     * @mbggenerated
     */
    int insert(WorkCenterPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table work_center
     *
     * @mbggenerated
     */
    int insertSelective(WorkCenterPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table work_center
     *
     * @mbggenerated
     */
    List<WorkCenterPO> selectByExample(WorkCenterPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table work_center
     *
     * @mbggenerated
     */
    WorkCenterPO selectByPrimaryKey(String handle);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table work_center
     *
     * @mbggenerated
     */
    int updateByExampleSelective(@Param("record") WorkCenterPO record, @Param("example") WorkCenterPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table work_center
     *
     * @mbggenerated
     */
    int updateByExample(@Param("record") WorkCenterPO record, @Param("example") WorkCenterPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table work_center
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(WorkCenterPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table work_center
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(WorkCenterPO record);

    //查询生产区域 支持多site
    public List<WorkCenterPO> selectWorkCenter(@Param("siteList") List<String> siteList, @Param("descripitonArr") String[] descripitonArr);
}