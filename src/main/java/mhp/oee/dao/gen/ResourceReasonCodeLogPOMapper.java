package mhp.oee.dao.gen;

import java.util.List;
import mhp.oee.po.gen.ResourceReasonCodeLogPO;
import mhp.oee.po.gen.ResourceReasonCodeLogPOExample;
import org.apache.ibatis.annotations.Param;

public interface ResourceReasonCodeLogPOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_code_log
     *
     * @mbggenerated
     */
    int countByExample(ResourceReasonCodeLogPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_code_log
     *
     * @mbggenerated
     */
    int deleteByExample(ResourceReasonCodeLogPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_code_log
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(String handle);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_code_log
     *
     * @mbggenerated
     */
    int insert(ResourceReasonCodeLogPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_code_log
     *
     * @mbggenerated
     */
    int insertSelective(ResourceReasonCodeLogPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_code_log
     *
     * @mbggenerated
     */
    List<ResourceReasonCodeLogPO> selectByExample(ResourceReasonCodeLogPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_code_log
     *
     * @mbggenerated
     */
    ResourceReasonCodeLogPO selectByPrimaryKey(String handle);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_code_log
     *
     * @mbggenerated
     */
    int updateByExampleSelective(@Param("record") ResourceReasonCodeLogPO record, @Param("example") ResourceReasonCodeLogPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_code_log
     *
     * @mbggenerated
     */
    int updateByExample(@Param("record") ResourceReasonCodeLogPO record, @Param("example") ResourceReasonCodeLogPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_code_log
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(ResourceReasonCodeLogPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_code_log
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(ResourceReasonCodeLogPO record);
}