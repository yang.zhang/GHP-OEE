package mhp.oee.dao.gen;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import mhp.oee.po.gen.ProductionOutputTargetPO;
import mhp.oee.po.gen.ProductionOutputTargetPOExample;

public interface ProductionOutputTargetPOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_output_target
     *
     * @mbggenerated
     */
    int countByExample(ProductionOutputTargetPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_output_target
     *
     * @mbggenerated
     */
    int deleteByExample(ProductionOutputTargetPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_output_target
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(String handle);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_output_target
     *
     * @mbggenerated
     */
    int insert(ProductionOutputTargetPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_output_target
     *
     * @mbggenerated
     */
    int insertSelective(ProductionOutputTargetPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_output_target
     *
     * @mbggenerated
     */
    List<ProductionOutputTargetPO> selectByExample(ProductionOutputTargetPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_output_target
     *
     * @mbggenerated
     */
    ProductionOutputTargetPO selectByPrimaryKey(String handle);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_output_target
     *
     * @mbggenerated
     */
    int updateByExampleSelective(@Param("record") ProductionOutputTargetPO record, @Param("example") ProductionOutputTargetPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_output_target
     *
     * @mbggenerated
     */
    int updateByExample(@Param("record") ProductionOutputTargetPO record, @Param("example") ProductionOutputTargetPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_output_target
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(ProductionOutputTargetPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table production_output_target
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(ProductionOutputTargetPO record);
}