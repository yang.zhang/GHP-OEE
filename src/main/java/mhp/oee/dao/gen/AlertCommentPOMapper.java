package mhp.oee.dao.gen;

import java.util.List;
import mhp.oee.po.gen.AlertCommentPO;
import mhp.oee.po.gen.AlertCommentPOExample;
import org.apache.ibatis.annotations.Param;

public interface AlertCommentPOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alert_comment
     *
     * @mbggenerated
     */
    int countByExample(AlertCommentPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alert_comment
     *
     * @mbggenerated
     */
    int deleteByExample(AlertCommentPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alert_comment
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(String handle);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alert_comment
     *
     * @mbggenerated
     */
    int insert(AlertCommentPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alert_comment
     *
     * @mbggenerated
     */
    int insertSelective(AlertCommentPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alert_comment
     *
     * @mbggenerated
     */
    List<AlertCommentPO> selectByExample(AlertCommentPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alert_comment
     *
     * @mbggenerated
     */
    AlertCommentPO selectByPrimaryKey(String handle);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alert_comment
     *
     * @mbggenerated
     */
    int updateByExampleSelective(@Param("record") AlertCommentPO record, @Param("example") AlertCommentPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alert_comment
     *
     * @mbggenerated
     */
    int updateByExample(@Param("record") AlertCommentPO record, @Param("example") AlertCommentPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alert_comment
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(AlertCommentPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alert_comment
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(AlertCommentPO record);
}