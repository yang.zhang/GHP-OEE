package mhp.oee.dao.gen;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import mhp.oee.po.gen.ReasonCodePriorityPO;
import mhp.oee.po.gen.ReasonCodePriorityPOExample;

public interface ReasonCodePriorityPOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table reason_code_priority
     *
     * @mbggenerated
     */
    int countByExample(ReasonCodePriorityPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table reason_code_priority
     *
     * @mbggenerated
     */
    int deleteByExample(ReasonCodePriorityPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table reason_code_priority
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(String handle);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table reason_code_priority
     *
     * @mbggenerated
     */
    int insert(ReasonCodePriorityPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table reason_code_priority
     *
     * @mbggenerated
     */
    int insertSelective(ReasonCodePriorityPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table reason_code_priority
     *
     * @mbggenerated
     */
    List<ReasonCodePriorityPO> selectByExample(ReasonCodePriorityPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table reason_code_priority
     *
     * @mbggenerated
     */
    ReasonCodePriorityPO selectByPrimaryKey(String handle);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table reason_code_priority
     *
     * @mbggenerated
     */
    int updateByExampleSelective(@Param("record") ReasonCodePriorityPO record, @Param("example") ReasonCodePriorityPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table reason_code_priority
     *
     * @mbggenerated
     */
    int updateByExample(@Param("record") ReasonCodePriorityPO record, @Param("example") ReasonCodePriorityPOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table reason_code_priority
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(ReasonCodePriorityPO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table reason_code_priority
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(ReasonCodePriorityPO record);
}