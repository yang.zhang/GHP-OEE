package mhp.oee.po.gen;

import java.util.Date;

public class WorkCenterPO {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column work_center.HANDLE
     *
     * @mbggenerated
     */
    private String handle;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column work_center.SITE
     *
     * @mbggenerated
     */
    private String site;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column work_center.WORK_CENTER
     *
     * @mbggenerated
     */
    private String workCenter;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column work_center.DESCRIPTION
     *
     * @mbggenerated
     */
    private String description;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column work_center.WC_CATEGORY
     *
     * @mbggenerated
     */
    private String wcCategory;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column work_center.ENABLED
     *
     * @mbggenerated
     */
    private String enabled;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column work_center.CREATED_DATE_TIME
     *
     * @mbggenerated
     */
    private Date createdDateTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column work_center.MODIFIED_DATE_TIME
     *
     * @mbggenerated
     */
    private Date modifiedDateTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column work_center.MODIFIED_USER
     *
     * @mbggenerated
     */
    private String modifiedUser;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column work_center.HANDLE
     *
     * @return the value of work_center.HANDLE
     *
     * @mbggenerated
     */
    public String getHandle() {
        return handle;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column work_center.HANDLE
     *
     * @param handle the value for work_center.HANDLE
     *
     * @mbggenerated
     */
    public void setHandle(String handle) {
        this.handle = handle == null ? null : handle.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column work_center.SITE
     *
     * @return the value of work_center.SITE
     *
     * @mbggenerated
     */
    public String getSite() {
        return site;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column work_center.SITE
     *
     * @param site the value for work_center.SITE
     *
     * @mbggenerated
     */
    public void setSite(String site) {
        this.site = site == null ? null : site.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column work_center.WORK_CENTER
     *
     * @return the value of work_center.WORK_CENTER
     *
     * @mbggenerated
     */
    public String getWorkCenter() {
        return workCenter;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column work_center.WORK_CENTER
     *
     * @param workCenter the value for work_center.WORK_CENTER
     *
     * @mbggenerated
     */
    public void setWorkCenter(String workCenter) {
        this.workCenter = workCenter == null ? null : workCenter.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column work_center.DESCRIPTION
     *
     * @return the value of work_center.DESCRIPTION
     *
     * @mbggenerated
     */
    public String getDescription() {
        return description;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column work_center.DESCRIPTION
     *
     * @param description the value for work_center.DESCRIPTION
     *
     * @mbggenerated
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column work_center.WC_CATEGORY
     *
     * @return the value of work_center.WC_CATEGORY
     *
     * @mbggenerated
     */
    public String getWcCategory() {
        return wcCategory;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column work_center.WC_CATEGORY
     *
     * @param wcCategory the value for work_center.WC_CATEGORY
     *
     * @mbggenerated
     */
    public void setWcCategory(String wcCategory) {
        this.wcCategory = wcCategory == null ? null : wcCategory.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column work_center.ENABLED
     *
     * @return the value of work_center.ENABLED
     *
     * @mbggenerated
     */
    public String getEnabled() {
        return enabled;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column work_center.ENABLED
     *
     * @param enabled the value for work_center.ENABLED
     *
     * @mbggenerated
     */
    public void setEnabled(String enabled) {
        this.enabled = enabled == null ? null : enabled.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column work_center.CREATED_DATE_TIME
     *
     * @return the value of work_center.CREATED_DATE_TIME
     *
     * @mbggenerated
     */
    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column work_center.CREATED_DATE_TIME
     *
     * @param createdDateTime the value for work_center.CREATED_DATE_TIME
     *
     * @mbggenerated
     */
    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column work_center.MODIFIED_DATE_TIME
     *
     * @return the value of work_center.MODIFIED_DATE_TIME
     *
     * @mbggenerated
     */
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column work_center.MODIFIED_DATE_TIME
     *
     * @param modifiedDateTime the value for work_center.MODIFIED_DATE_TIME
     *
     * @mbggenerated
     */
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column work_center.MODIFIED_USER
     *
     * @return the value of work_center.MODIFIED_USER
     *
     * @mbggenerated
     */
    public String getModifiedUser() {
        return modifiedUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column work_center.MODIFIED_USER
     *
     * @param modifiedUser the value for work_center.MODIFIED_USER
     *
     * @mbggenerated
     */
    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }
}