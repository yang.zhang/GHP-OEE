package mhp.oee.po.gen;

import java.util.Date;

public class SitePO {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column site.HANDLE
     *
     * @mbggenerated
     */
    private String handle;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column site.SITE
     *
     * @mbggenerated
     */
    private String site;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column site.DESCRIPTION
     *
     * @mbggenerated
     */
    private String description;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column site.CREATED_DATE_TIME
     *
     * @mbggenerated
     */
    private Date createdDateTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column site.MODIFIED_DATE_TIME
     *
     * @mbggenerated
     */
    private Date modifiedDateTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column site.MODIFIED_USER
     *
     * @mbggenerated
     */
    private String modifiedUser;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column site.HANDLE
     *
     * @return the value of site.HANDLE
     *
     * @mbggenerated
     */
    public String getHandle() {
        return handle;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column site.HANDLE
     *
     * @param handle the value for site.HANDLE
     *
     * @mbggenerated
     */
    public void setHandle(String handle) {
        this.handle = handle == null ? null : handle.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column site.SITE
     *
     * @return the value of site.SITE
     *
     * @mbggenerated
     */
    public String getSite() {
        return site;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column site.SITE
     *
     * @param site the value for site.SITE
     *
     * @mbggenerated
     */
    public void setSite(String site) {
        this.site = site == null ? null : site.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column site.DESCRIPTION
     *
     * @return the value of site.DESCRIPTION
     *
     * @mbggenerated
     */
    public String getDescription() {
        return description;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column site.DESCRIPTION
     *
     * @param description the value for site.DESCRIPTION
     *
     * @mbggenerated
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column site.CREATED_DATE_TIME
     *
     * @return the value of site.CREATED_DATE_TIME
     *
     * @mbggenerated
     */
    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column site.CREATED_DATE_TIME
     *
     * @param createdDateTime the value for site.CREATED_DATE_TIME
     *
     * @mbggenerated
     */
    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column site.MODIFIED_DATE_TIME
     *
     * @return the value of site.MODIFIED_DATE_TIME
     *
     * @mbggenerated
     */
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column site.MODIFIED_DATE_TIME
     *
     * @param modifiedDateTime the value for site.MODIFIED_DATE_TIME
     *
     * @mbggenerated
     */
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column site.MODIFIED_USER
     *
     * @return the value of site.MODIFIED_USER
     *
     * @mbggenerated
     */
    public String getModifiedUser() {
        return modifiedUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column site.MODIFIED_USER
     *
     * @param modifiedUser the value for site.MODIFIED_USER
     *
     * @mbggenerated
     */
    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }
}