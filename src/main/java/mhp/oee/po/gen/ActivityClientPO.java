package mhp.oee.po.gen;

import java.util.Date;

public class ActivityClientPO {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column activity_client.HANDLE
     *
     * @mbggenerated
     */
    private String handle;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column activity_client.ACTIVITY_CLIENT
     *
     * @mbggenerated
     */
    private String activityClient;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column activity_client.DESCRIPTION
     *
     * @mbggenerated
     */
    private String description;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column activity_client.TYPE
     *
     * @mbggenerated
     */
    private String type;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column activity_client.VISIBLE
     *
     * @mbggenerated
     */
    private String visible;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column activity_client.CREATED_DATE_TIME
     *
     * @mbggenerated
     */
    private Date createdDateTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column activity_client.MODIFIED_DATE_TIME
     *
     * @mbggenerated
     */
    private Date modifiedDateTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column activity_client.MODIFIED_USER
     *
     * @mbggenerated
     */
    private String modifiedUser;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column activity_client.HANDLE
     *
     * @return the value of activity_client.HANDLE
     *
     * @mbggenerated
     */
    public String getHandle() {
        return handle;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column activity_client.HANDLE
     *
     * @param handle the value for activity_client.HANDLE
     *
     * @mbggenerated
     */
    public void setHandle(String handle) {
        this.handle = handle == null ? null : handle.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column activity_client.ACTIVITY_CLIENT
     *
     * @return the value of activity_client.ACTIVITY_CLIENT
     *
     * @mbggenerated
     */
    public String getActivityClient() {
        return activityClient;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column activity_client.ACTIVITY_CLIENT
     *
     * @param activityClient the value for activity_client.ACTIVITY_CLIENT
     *
     * @mbggenerated
     */
    public void setActivityClient(String activityClient) {
        this.activityClient = activityClient == null ? null : activityClient.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column activity_client.DESCRIPTION
     *
     * @return the value of activity_client.DESCRIPTION
     *
     * @mbggenerated
     */
    public String getDescription() {
        return description;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column activity_client.DESCRIPTION
     *
     * @param description the value for activity_client.DESCRIPTION
     *
     * @mbggenerated
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column activity_client.TYPE
     *
     * @return the value of activity_client.TYPE
     *
     * @mbggenerated
     */
    public String getType() {
        return type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column activity_client.TYPE
     *
     * @param type the value for activity_client.TYPE
     *
     * @mbggenerated
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column activity_client.VISIBLE
     *
     * @return the value of activity_client.VISIBLE
     *
     * @mbggenerated
     */
    public String getVisible() {
        return visible;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column activity_client.VISIBLE
     *
     * @param visible the value for activity_client.VISIBLE
     *
     * @mbggenerated
     */
    public void setVisible(String visible) {
        this.visible = visible == null ? null : visible.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column activity_client.CREATED_DATE_TIME
     *
     * @return the value of activity_client.CREATED_DATE_TIME
     *
     * @mbggenerated
     */
    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column activity_client.CREATED_DATE_TIME
     *
     * @param createdDateTime the value for activity_client.CREATED_DATE_TIME
     *
     * @mbggenerated
     */
    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column activity_client.MODIFIED_DATE_TIME
     *
     * @return the value of activity_client.MODIFIED_DATE_TIME
     *
     * @mbggenerated
     */
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column activity_client.MODIFIED_DATE_TIME
     *
     * @param modifiedDateTime the value for activity_client.MODIFIED_DATE_TIME
     *
     * @mbggenerated
     */
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column activity_client.MODIFIED_USER
     *
     * @return the value of activity_client.MODIFIED_USER
     *
     * @mbggenerated
     */
    public String getModifiedUser() {
        return modifiedUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column activity_client.MODIFIED_USER
     *
     * @param modifiedUser the value for activity_client.MODIFIED_USER
     *
     * @mbggenerated
     */
    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }
}