package mhp.oee.po.gen;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ResourceReasonLogPOExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Integer offset;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    public ResourceReasonLogPOExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSiteIsNull() {
            addCriterion("SITE is null");
            return (Criteria) this;
        }

        public Criteria andSiteIsNotNull() {
            addCriterion("SITE is not null");
            return (Criteria) this;
        }

        public Criteria andSiteEqualTo(String value) {
            addCriterion("SITE =", value, "site");
            return (Criteria) this;
        }

        public Criteria andSiteNotEqualTo(String value) {
            addCriterion("SITE <>", value, "site");
            return (Criteria) this;
        }

        public Criteria andSiteGreaterThan(String value) {
            addCriterion("SITE >", value, "site");
            return (Criteria) this;
        }

        public Criteria andSiteGreaterThanOrEqualTo(String value) {
            addCriterion("SITE >=", value, "site");
            return (Criteria) this;
        }

        public Criteria andSiteLessThan(String value) {
            addCriterion("SITE <", value, "site");
            return (Criteria) this;
        }

        public Criteria andSiteLessThanOrEqualTo(String value) {
            addCriterion("SITE <=", value, "site");
            return (Criteria) this;
        }

        public Criteria andSiteLike(String value) {
            addCriterion("SITE like", value, "site");
            return (Criteria) this;
        }

        public Criteria andSiteNotLike(String value) {
            addCriterion("SITE not like", value, "site");
            return (Criteria) this;
        }

        public Criteria andSiteIn(List<String> values) {
            addCriterion("SITE in", values, "site");
            return (Criteria) this;
        }

        public Criteria andSiteNotIn(List<String> values) {
            addCriterion("SITE not in", values, "site");
            return (Criteria) this;
        }

        public Criteria andSiteBetween(String value1, String value2) {
            addCriterion("SITE between", value1, value2, "site");
            return (Criteria) this;
        }

        public Criteria andSiteNotBetween(String value1, String value2) {
            addCriterion("SITE not between", value1, value2, "site");
            return (Criteria) this;
        }

        public Criteria andResrceIsNull() {
            addCriterion("RESRCE is null");
            return (Criteria) this;
        }

        public Criteria andResrceIsNotNull() {
            addCriterion("RESRCE is not null");
            return (Criteria) this;
        }

        public Criteria andResrceEqualTo(String value) {
            addCriterion("RESRCE =", value, "resrce");
            return (Criteria) this;
        }

        public Criteria andResrceNotEqualTo(String value) {
            addCriterion("RESRCE <>", value, "resrce");
            return (Criteria) this;
        }

        public Criteria andResrceGreaterThan(String value) {
            addCriterion("RESRCE >", value, "resrce");
            return (Criteria) this;
        }

        public Criteria andResrceGreaterThanOrEqualTo(String value) {
            addCriterion("RESRCE >=", value, "resrce");
            return (Criteria) this;
        }

        public Criteria andResrceLessThan(String value) {
            addCriterion("RESRCE <", value, "resrce");
            return (Criteria) this;
        }

        public Criteria andResrceLessThanOrEqualTo(String value) {
            addCriterion("RESRCE <=", value, "resrce");
            return (Criteria) this;
        }

        public Criteria andResrceLike(String value) {
            addCriterion("RESRCE like", value, "resrce");
            return (Criteria) this;
        }

        public Criteria andResrceNotLike(String value) {
            addCriterion("RESRCE not like", value, "resrce");
            return (Criteria) this;
        }

        public Criteria andResrceIn(List<String> values) {
            addCriterion("RESRCE in", values, "resrce");
            return (Criteria) this;
        }

        public Criteria andResrceNotIn(List<String> values) {
            addCriterion("RESRCE not in", values, "resrce");
            return (Criteria) this;
        }

        public Criteria andResrceBetween(String value1, String value2) {
            addCriterion("RESRCE between", value1, value2, "resrce");
            return (Criteria) this;
        }

        public Criteria andResrceNotBetween(String value1, String value2) {
            addCriterion("RESRCE not between", value1, value2, "resrce");
            return (Criteria) this;
        }

        public Criteria andDateTimeIsNull() {
            addCriterion("DATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andDateTimeIsNotNull() {
            addCriterion("DATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andDateTimeEqualTo(Date value) {
            addCriterion("DATE_TIME =", value, "dateTime");
            return (Criteria) this;
        }

        public Criteria andDateTimeNotEqualTo(Date value) {
            addCriterion("DATE_TIME <>", value, "dateTime");
            return (Criteria) this;
        }

        public Criteria andDateTimeGreaterThan(Date value) {
            addCriterion("DATE_TIME >", value, "dateTime");
            return (Criteria) this;
        }

        public Criteria andDateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("DATE_TIME >=", value, "dateTime");
            return (Criteria) this;
        }

        public Criteria andDateTimeLessThan(Date value) {
            addCriterion("DATE_TIME <", value, "dateTime");
            return (Criteria) this;
        }

        public Criteria andDateTimeLessThanOrEqualTo(Date value) {
            addCriterion("DATE_TIME <=", value, "dateTime");
            return (Criteria) this;
        }

        public Criteria andDateTimeIn(List<Date> values) {
            addCriterion("DATE_TIME in", values, "dateTime");
            return (Criteria) this;
        }

        public Criteria andDateTimeNotIn(List<Date> values) {
            addCriterion("DATE_TIME not in", values, "dateTime");
            return (Criteria) this;
        }

        public Criteria andDateTimeBetween(Date value1, Date value2) {
            addCriterion("DATE_TIME between", value1, value2, "dateTime");
            return (Criteria) this;
        }

        public Criteria andDateTimeNotBetween(Date value1, Date value2) {
            addCriterion("DATE_TIME not between", value1, value2, "dateTime");
            return (Criteria) this;
        }

        public Criteria andReasonCodeIsNull() {
            addCriterion("REASON_CODE is null");
            return (Criteria) this;
        }

        public Criteria andReasonCodeIsNotNull() {
            addCriterion("REASON_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andReasonCodeEqualTo(String value) {
            addCriterion("REASON_CODE =", value, "reasonCode");
            return (Criteria) this;
        }

        public Criteria andReasonCodeNotEqualTo(String value) {
            addCriterion("REASON_CODE <>", value, "reasonCode");
            return (Criteria) this;
        }

        public Criteria andReasonCodeGreaterThan(String value) {
            addCriterion("REASON_CODE >", value, "reasonCode");
            return (Criteria) this;
        }

        public Criteria andReasonCodeGreaterThanOrEqualTo(String value) {
            addCriterion("REASON_CODE >=", value, "reasonCode");
            return (Criteria) this;
        }

        public Criteria andReasonCodeLessThan(String value) {
            addCriterion("REASON_CODE <", value, "reasonCode");
            return (Criteria) this;
        }

        public Criteria andReasonCodeLessThanOrEqualTo(String value) {
            addCriterion("REASON_CODE <=", value, "reasonCode");
            return (Criteria) this;
        }

        public Criteria andReasonCodeLike(String value) {
            addCriterion("REASON_CODE like", value, "reasonCode");
            return (Criteria) this;
        }

        public Criteria andReasonCodeNotLike(String value) {
            addCriterion("REASON_CODE not like", value, "reasonCode");
            return (Criteria) this;
        }

        public Criteria andReasonCodeIn(List<String> values) {
            addCriterion("REASON_CODE in", values, "reasonCode");
            return (Criteria) this;
        }

        public Criteria andReasonCodeNotIn(List<String> values) {
            addCriterion("REASON_CODE not in", values, "reasonCode");
            return (Criteria) this;
        }

        public Criteria andReasonCodeBetween(String value1, String value2) {
            addCriterion("REASON_CODE between", value1, value2, "reasonCode");
            return (Criteria) this;
        }

        public Criteria andReasonCodeNotBetween(String value1, String value2) {
            addCriterion("REASON_CODE not between", value1, value2, "reasonCode");
            return (Criteria) this;
        }

        public Criteria andActionCodeIsNull() {
            addCriterion("ACTION_CODE is null");
            return (Criteria) this;
        }

        public Criteria andActionCodeIsNotNull() {
            addCriterion("ACTION_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andActionCodeEqualTo(String value) {
            addCriterion("ACTION_CODE =", value, "actionCode");
            return (Criteria) this;
        }

        public Criteria andActionCodeNotEqualTo(String value) {
            addCriterion("ACTION_CODE <>", value, "actionCode");
            return (Criteria) this;
        }

        public Criteria andActionCodeGreaterThan(String value) {
            addCriterion("ACTION_CODE >", value, "actionCode");
            return (Criteria) this;
        }

        public Criteria andActionCodeGreaterThanOrEqualTo(String value) {
            addCriterion("ACTION_CODE >=", value, "actionCode");
            return (Criteria) this;
        }

        public Criteria andActionCodeLessThan(String value) {
            addCriterion("ACTION_CODE <", value, "actionCode");
            return (Criteria) this;
        }

        public Criteria andActionCodeLessThanOrEqualTo(String value) {
            addCriterion("ACTION_CODE <=", value, "actionCode");
            return (Criteria) this;
        }

        public Criteria andActionCodeLike(String value) {
            addCriterion("ACTION_CODE like", value, "actionCode");
            return (Criteria) this;
        }

        public Criteria andActionCodeNotLike(String value) {
            addCriterion("ACTION_CODE not like", value, "actionCode");
            return (Criteria) this;
        }

        public Criteria andActionCodeIn(List<String> values) {
            addCriterion("ACTION_CODE in", values, "actionCode");
            return (Criteria) this;
        }

        public Criteria andActionCodeNotIn(List<String> values) {
            addCriterion("ACTION_CODE not in", values, "actionCode");
            return (Criteria) this;
        }

        public Criteria andActionCodeBetween(String value1, String value2) {
            addCriterion("ACTION_CODE between", value1, value2, "actionCode");
            return (Criteria) this;
        }

        public Criteria andActionCodeNotBetween(String value1, String value2) {
            addCriterion("ACTION_CODE not between", value1, value2, "actionCode");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationIsNull() {
            addCriterion("MAINTENANCE_NOTIFICATION is null");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationIsNotNull() {
            addCriterion("MAINTENANCE_NOTIFICATION is not null");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationEqualTo(String value) {
            addCriterion("MAINTENANCE_NOTIFICATION =", value, "maintenanceNotification");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationNotEqualTo(String value) {
            addCriterion("MAINTENANCE_NOTIFICATION <>", value, "maintenanceNotification");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationGreaterThan(String value) {
            addCriterion("MAINTENANCE_NOTIFICATION >", value, "maintenanceNotification");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationGreaterThanOrEqualTo(String value) {
            addCriterion("MAINTENANCE_NOTIFICATION >=", value, "maintenanceNotification");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationLessThan(String value) {
            addCriterion("MAINTENANCE_NOTIFICATION <", value, "maintenanceNotification");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationLessThanOrEqualTo(String value) {
            addCriterion("MAINTENANCE_NOTIFICATION <=", value, "maintenanceNotification");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationLike(String value) {
            addCriterion("MAINTENANCE_NOTIFICATION like", value, "maintenanceNotification");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationNotLike(String value) {
            addCriterion("MAINTENANCE_NOTIFICATION not like", value, "maintenanceNotification");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationIn(List<String> values) {
            addCriterion("MAINTENANCE_NOTIFICATION in", values, "maintenanceNotification");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationNotIn(List<String> values) {
            addCriterion("MAINTENANCE_NOTIFICATION not in", values, "maintenanceNotification");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationBetween(String value1, String value2) {
            addCriterion("MAINTENANCE_NOTIFICATION between", value1, value2, "maintenanceNotification");
            return (Criteria) this;
        }

        public Criteria andMaintenanceNotificationNotBetween(String value1, String value2) {
            addCriterion("MAINTENANCE_NOTIFICATION not between", value1, value2, "maintenanceNotification");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderIsNull() {
            addCriterion("MAINTENANCE_ORDER is null");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderIsNotNull() {
            addCriterion("MAINTENANCE_ORDER is not null");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderEqualTo(String value) {
            addCriterion("MAINTENANCE_ORDER =", value, "maintenanceOrder");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderNotEqualTo(String value) {
            addCriterion("MAINTENANCE_ORDER <>", value, "maintenanceOrder");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderGreaterThan(String value) {
            addCriterion("MAINTENANCE_ORDER >", value, "maintenanceOrder");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderGreaterThanOrEqualTo(String value) {
            addCriterion("MAINTENANCE_ORDER >=", value, "maintenanceOrder");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderLessThan(String value) {
            addCriterion("MAINTENANCE_ORDER <", value, "maintenanceOrder");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderLessThanOrEqualTo(String value) {
            addCriterion("MAINTENANCE_ORDER <=", value, "maintenanceOrder");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderLike(String value) {
            addCriterion("MAINTENANCE_ORDER like", value, "maintenanceOrder");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderNotLike(String value) {
            addCriterion("MAINTENANCE_ORDER not like", value, "maintenanceOrder");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderIn(List<String> values) {
            addCriterion("MAINTENANCE_ORDER in", values, "maintenanceOrder");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderNotIn(List<String> values) {
            addCriterion("MAINTENANCE_ORDER not in", values, "maintenanceOrder");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderBetween(String value1, String value2) {
            addCriterion("MAINTENANCE_ORDER between", value1, value2, "maintenanceOrder");
            return (Criteria) this;
        }

        public Criteria andMaintenanceOrderNotBetween(String value1, String value2) {
            addCriterion("MAINTENANCE_ORDER not between", value1, value2, "maintenanceOrder");
            return (Criteria) this;
        }

        public Criteria andMeUserIdIsNull() {
            addCriterion("ME_USER_ID is null");
            return (Criteria) this;
        }

        public Criteria andMeUserIdIsNotNull() {
            addCriterion("ME_USER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andMeUserIdEqualTo(String value) {
            addCriterion("ME_USER_ID =", value, "meUserId");
            return (Criteria) this;
        }

        public Criteria andMeUserIdNotEqualTo(String value) {
            addCriterion("ME_USER_ID <>", value, "meUserId");
            return (Criteria) this;
        }

        public Criteria andMeUserIdGreaterThan(String value) {
            addCriterion("ME_USER_ID >", value, "meUserId");
            return (Criteria) this;
        }

        public Criteria andMeUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("ME_USER_ID >=", value, "meUserId");
            return (Criteria) this;
        }

        public Criteria andMeUserIdLessThan(String value) {
            addCriterion("ME_USER_ID <", value, "meUserId");
            return (Criteria) this;
        }

        public Criteria andMeUserIdLessThanOrEqualTo(String value) {
            addCriterion("ME_USER_ID <=", value, "meUserId");
            return (Criteria) this;
        }

        public Criteria andMeUserIdLike(String value) {
            addCriterion("ME_USER_ID like", value, "meUserId");
            return (Criteria) this;
        }

        public Criteria andMeUserIdNotLike(String value) {
            addCriterion("ME_USER_ID not like", value, "meUserId");
            return (Criteria) this;
        }

        public Criteria andMeUserIdIn(List<String> values) {
            addCriterion("ME_USER_ID in", values, "meUserId");
            return (Criteria) this;
        }

        public Criteria andMeUserIdNotIn(List<String> values) {
            addCriterion("ME_USER_ID not in", values, "meUserId");
            return (Criteria) this;
        }

        public Criteria andMeUserIdBetween(String value1, String value2) {
            addCriterion("ME_USER_ID between", value1, value2, "meUserId");
            return (Criteria) this;
        }

        public Criteria andMeUserIdNotBetween(String value1, String value2) {
            addCriterion("ME_USER_ID not between", value1, value2, "meUserId");
            return (Criteria) this;
        }

        public Criteria andCreatedDateTimeIsNull() {
            addCriterion("CREATED_DATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andCreatedDateTimeIsNotNull() {
            addCriterion("CREATED_DATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedDateTimeEqualTo(Date value) {
            addCriterion("CREATED_DATE_TIME =", value, "createdDateTime");
            return (Criteria) this;
        }

        public Criteria andCreatedDateTimeNotEqualTo(Date value) {
            addCriterion("CREATED_DATE_TIME <>", value, "createdDateTime");
            return (Criteria) this;
        }

        public Criteria andCreatedDateTimeGreaterThan(Date value) {
            addCriterion("CREATED_DATE_TIME >", value, "createdDateTime");
            return (Criteria) this;
        }

        public Criteria andCreatedDateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATED_DATE_TIME >=", value, "createdDateTime");
            return (Criteria) this;
        }

        public Criteria andCreatedDateTimeLessThan(Date value) {
            addCriterion("CREATED_DATE_TIME <", value, "createdDateTime");
            return (Criteria) this;
        }

        public Criteria andCreatedDateTimeLessThanOrEqualTo(Date value) {
            addCriterion("CREATED_DATE_TIME <=", value, "createdDateTime");
            return (Criteria) this;
        }

        public Criteria andCreatedDateTimeIn(List<Date> values) {
            addCriterion("CREATED_DATE_TIME in", values, "createdDateTime");
            return (Criteria) this;
        }

        public Criteria andCreatedDateTimeNotIn(List<Date> values) {
            addCriterion("CREATED_DATE_TIME not in", values, "createdDateTime");
            return (Criteria) this;
        }

        public Criteria andCreatedDateTimeBetween(Date value1, Date value2) {
            addCriterion("CREATED_DATE_TIME between", value1, value2, "createdDateTime");
            return (Criteria) this;
        }

        public Criteria andCreatedDateTimeNotBetween(Date value1, Date value2) {
            addCriterion("CREATED_DATE_TIME not between", value1, value2, "createdDateTime");
            return (Criteria) this;
        }

        public Criteria andModifiedDateTimeIsNull() {
            addCriterion("MODIFIED_DATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andModifiedDateTimeIsNotNull() {
            addCriterion("MODIFIED_DATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedDateTimeEqualTo(Date value) {
            addCriterion("MODIFIED_DATE_TIME =", value, "modifiedDateTime");
            return (Criteria) this;
        }

        public Criteria andModifiedDateTimeNotEqualTo(Date value) {
            addCriterion("MODIFIED_DATE_TIME <>", value, "modifiedDateTime");
            return (Criteria) this;
        }

        public Criteria andModifiedDateTimeGreaterThan(Date value) {
            addCriterion("MODIFIED_DATE_TIME >", value, "modifiedDateTime");
            return (Criteria) this;
        }

        public Criteria andModifiedDateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("MODIFIED_DATE_TIME >=", value, "modifiedDateTime");
            return (Criteria) this;
        }

        public Criteria andModifiedDateTimeLessThan(Date value) {
            addCriterion("MODIFIED_DATE_TIME <", value, "modifiedDateTime");
            return (Criteria) this;
        }

        public Criteria andModifiedDateTimeLessThanOrEqualTo(Date value) {
            addCriterion("MODIFIED_DATE_TIME <=", value, "modifiedDateTime");
            return (Criteria) this;
        }

        public Criteria andModifiedDateTimeIn(List<Date> values) {
            addCriterion("MODIFIED_DATE_TIME in", values, "modifiedDateTime");
            return (Criteria) this;
        }

        public Criteria andModifiedDateTimeNotIn(List<Date> values) {
            addCriterion("MODIFIED_DATE_TIME not in", values, "modifiedDateTime");
            return (Criteria) this;
        }

        public Criteria andModifiedDateTimeBetween(Date value1, Date value2) {
            addCriterion("MODIFIED_DATE_TIME between", value1, value2, "modifiedDateTime");
            return (Criteria) this;
        }

        public Criteria andModifiedDateTimeNotBetween(Date value1, Date value2) {
            addCriterion("MODIFIED_DATE_TIME not between", value1, value2, "modifiedDateTime");
            return (Criteria) this;
        }

        public Criteria andModifiedUserIsNull() {
            addCriterion("MODIFIED_USER is null");
            return (Criteria) this;
        }

        public Criteria andModifiedUserIsNotNull() {
            addCriterion("MODIFIED_USER is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedUserEqualTo(String value) {
            addCriterion("MODIFIED_USER =", value, "modifiedUser");
            return (Criteria) this;
        }

        public Criteria andModifiedUserNotEqualTo(String value) {
            addCriterion("MODIFIED_USER <>", value, "modifiedUser");
            return (Criteria) this;
        }

        public Criteria andModifiedUserGreaterThan(String value) {
            addCriterion("MODIFIED_USER >", value, "modifiedUser");
            return (Criteria) this;
        }

        public Criteria andModifiedUserGreaterThanOrEqualTo(String value) {
            addCriterion("MODIFIED_USER >=", value, "modifiedUser");
            return (Criteria) this;
        }

        public Criteria andModifiedUserLessThan(String value) {
            addCriterion("MODIFIED_USER <", value, "modifiedUser");
            return (Criteria) this;
        }

        public Criteria andModifiedUserLessThanOrEqualTo(String value) {
            addCriterion("MODIFIED_USER <=", value, "modifiedUser");
            return (Criteria) this;
        }

        public Criteria andModifiedUserLike(String value) {
            addCriterion("MODIFIED_USER like", value, "modifiedUser");
            return (Criteria) this;
        }

        public Criteria andModifiedUserNotLike(String value) {
            addCriterion("MODIFIED_USER not like", value, "modifiedUser");
            return (Criteria) this;
        }

        public Criteria andModifiedUserIn(List<String> values) {
            addCriterion("MODIFIED_USER in", values, "modifiedUser");
            return (Criteria) this;
        }

        public Criteria andModifiedUserNotIn(List<String> values) {
            addCriterion("MODIFIED_USER not in", values, "modifiedUser");
            return (Criteria) this;
        }

        public Criteria andModifiedUserBetween(String value1, String value2) {
            addCriterion("MODIFIED_USER between", value1, value2, "modifiedUser");
            return (Criteria) this;
        }

        public Criteria andModifiedUserNotBetween(String value1, String value2) {
            addCriterion("MODIFIED_USER not between", value1, value2, "modifiedUser");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table resource_reason_log
     *
     * @mbggenerated do_not_delete_during_merge
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table resource_reason_log
     *
     * @mbggenerated
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}