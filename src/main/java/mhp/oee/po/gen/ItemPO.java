package mhp.oee.po.gen;

import java.util.Date;

public class ItemPO {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column item.HANDLE
     *
     * @mbggenerated
     */
    private String handle;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column item.SITE
     *
     * @mbggenerated
     */
    private String site;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column item.ITEM
     *
     * @mbggenerated
     */
    private String item;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column item.DESCRIPTION
     *
     * @mbggenerated
     */
    private String description;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column item.REVISION
     *
     * @mbggenerated
     */
    private String revision;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column item.MODEL
     *
     * @mbggenerated
     */
    private String model;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column item.ENABLED
     *
     * @mbggenerated
     */
    private String enabled;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column item.CREATED_DATE_TIME
     *
     * @mbggenerated
     */
    private Date createdDateTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column item.MODIFIED_DATE_TIME
     *
     * @mbggenerated
     */
    private Date modifiedDateTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column item.MODIFIED_USER
     *
     * @mbggenerated
     */
    private String modifiedUser;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column item.HANDLE
     *
     * @return the value of item.HANDLE
     *
     * @mbggenerated
     */
    public String getHandle() {
        return handle;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column item.HANDLE
     *
     * @param handle the value for item.HANDLE
     *
     * @mbggenerated
     */
    public void setHandle(String handle) {
        this.handle = handle == null ? null : handle.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column item.SITE
     *
     * @return the value of item.SITE
     *
     * @mbggenerated
     */
    public String getSite() {
        return site;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column item.SITE
     *
     * @param site the value for item.SITE
     *
     * @mbggenerated
     */
    public void setSite(String site) {
        this.site = site == null ? null : site.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column item.ITEM
     *
     * @return the value of item.ITEM
     *
     * @mbggenerated
     */
    public String getItem() {
        return item;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column item.ITEM
     *
     * @param item the value for item.ITEM
     *
     * @mbggenerated
     */
    public void setItem(String item) {
        this.item = item == null ? null : item.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column item.DESCRIPTION
     *
     * @return the value of item.DESCRIPTION
     *
     * @mbggenerated
     */
    public String getDescription() {
        return description;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column item.DESCRIPTION
     *
     * @param description the value for item.DESCRIPTION
     *
     * @mbggenerated
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column item.REVISION
     *
     * @return the value of item.REVISION
     *
     * @mbggenerated
     */
    public String getRevision() {
        return revision;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column item.REVISION
     *
     * @param revision the value for item.REVISION
     *
     * @mbggenerated
     */
    public void setRevision(String revision) {
        this.revision = revision == null ? null : revision.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column item.MODEL
     *
     * @return the value of item.MODEL
     *
     * @mbggenerated
     */
    public String getModel() {
        return model;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column item.MODEL
     *
     * @param model the value for item.MODEL
     *
     * @mbggenerated
     */
    public void setModel(String model) {
        this.model = model == null ? null : model.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column item.ENABLED
     *
     * @return the value of item.ENABLED
     *
     * @mbggenerated
     */
    public String getEnabled() {
        return enabled;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column item.ENABLED
     *
     * @param enabled the value for item.ENABLED
     *
     * @mbggenerated
     */
    public void setEnabled(String enabled) {
        this.enabled = enabled == null ? null : enabled.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column item.CREATED_DATE_TIME
     *
     * @return the value of item.CREATED_DATE_TIME
     *
     * @mbggenerated
     */
    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column item.CREATED_DATE_TIME
     *
     * @param createdDateTime the value for item.CREATED_DATE_TIME
     *
     * @mbggenerated
     */
    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column item.MODIFIED_DATE_TIME
     *
     * @return the value of item.MODIFIED_DATE_TIME
     *
     * @mbggenerated
     */
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column item.MODIFIED_DATE_TIME
     *
     * @param modifiedDateTime the value for item.MODIFIED_DATE_TIME
     *
     * @mbggenerated
     */
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column item.MODIFIED_USER
     *
     * @return the value of item.MODIFIED_USER
     *
     * @mbggenerated
     */
    public String getModifiedUser() {
        return modifiedUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column item.MODIFIED_USER
     *
     * @param modifiedUser the value for item.MODIFIED_USER
     *
     * @mbggenerated
     */
    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }
}