package mhp.oee.po.extend;

import java.math.BigDecimal;

/**
 * Created by LinZuK on 2016/8/17.
 */
public class PpmReportPO {
    private String resrce;
    private String description;
    private String workArea;
    private String workAreaDes;
    private String lineArea;
    private String lineAreaDes;
    private String shift;
    private String shiftDes;
    private String item;
    private String ppm;
    private String byTime;
    private String model;
    private String itemDes;
    private String firstCal;
    private String asset;
    private String qty;
    private String ppmTheory;

    public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWorkArea() {
        return workArea;
    }

    public void setWorkArea(String workArea) {
        this.workArea = workArea;
    }

    public String getWorkAreaDes() {
        return workAreaDes;
    }

    public void setWorkAreaDes(String workAreaDes) {
        this.workAreaDes = workAreaDes;
    }

    public String getLineArea() {
        return lineArea;
    }

    public void setLineArea(String lineArea) {
        this.lineArea = lineArea;
    }

    public String getLineAreaDes() {
        return lineAreaDes;
    }

    public void setLineAreaDes(String lineAreaDes) {
        this.lineAreaDes = lineAreaDes;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getShiftDes() {
        return shiftDes;
    }

    public void setShiftDes(String shiftDes) {
        this.shiftDes = shiftDes;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getPpm() {
        return ppm;
    }

    public void setPpm(String ppm) {
        this.ppm = ppm;
    }

    public String getByTime() {
        return byTime;
    }

    public void setByTime(String byTime) {
        this.byTime = byTime;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getItemDes() {
        return itemDes;
    }

    public void setItemDes(String itemDes) {
        this.itemDes = itemDes;
    }

    public String getFirstCal() {
        return firstCal;
    }

    public void setFirstCal(String firstCal) {
        this.firstCal = firstCal;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPpmTheory() {
        return ppmTheory;
    }

    public void setPpmTheory(String ppmTheory) {
        this.ppmTheory = ppmTheory;
    }
}
