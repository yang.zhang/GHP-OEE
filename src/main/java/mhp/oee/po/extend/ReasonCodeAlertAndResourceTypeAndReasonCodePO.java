package mhp.oee.po.extend;

import mhp.oee.po.gen.ReasonCodeAlertPO;
import mhp.oee.po.gen.ReasonCodePO;
import mhp.oee.po.gen.ResourceTypePO;

public class ReasonCodeAlertAndResourceTypeAndReasonCodePO extends ReasonCodeAlertPO{
    private String startDateTimeIn;
    private String endDateTimeIn;
    private ResourceTypePO resourceTypePO;
    private ReasonCodePO reasonCodePO;
    private Character viewActionCode;
    
    public ResourceTypePO getResourceTypePO() {
        return resourceTypePO;
    }
    public void setResourceTypePO(ResourceTypePO resourceTypePO) {
        this.resourceTypePO = resourceTypePO;
    }
    public ReasonCodePO getReasonCodePO() {
        return reasonCodePO;
    }
    public void setReasonCodePO(ReasonCodePO reasonCodePO) {
        this.reasonCodePO = reasonCodePO;
    }
    public String getStartDateTimeIn() {
        return startDateTimeIn;
    }
    public void setStartDateTimeIn(String startDateTimeIn) {
        this.startDateTimeIn = startDateTimeIn;
    }
    public String getEndDateTimeIn() {
        return endDateTimeIn;
    }
    public void setEndDateTimeIn(String endDateTimeIn) {
        this.endDateTimeIn = endDateTimeIn;
    }
    public Character getViewActionCode() {
        return viewActionCode;
    }
    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }
}
