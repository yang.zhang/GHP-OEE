package mhp.oee.po.extend;

import mhp.oee.po.gen.ActivityClientPermPO;

public class ActivityClientPermAndActivityClientPO extends ActivityClientPermPO{
    
    private String description;
    private String activityClient;

    public String getActivityClient() {
        return activityClient;
    }

    public void setActivityClient(String activityClient) {
        this.activityClient = activityClient;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
