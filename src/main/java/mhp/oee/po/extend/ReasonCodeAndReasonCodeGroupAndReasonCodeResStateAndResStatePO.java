package mhp.oee.po.extend;

import java.util.Date;

public class ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStatePO {
    private String handle;
    private String site;
    private String reaonCode;
    private String reasonCodeDes;
    private String reasonCodeGroup;
    private String reasonCodeGroupDes;
    private String state;
    private String stateDes;
    private Date startDateTime;
    private Date endDateTime;
    private Date modifiedDateTime;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getReaonCode() {
        return reaonCode;
    }

    public void setReaonCode(String reaonCode) {
        this.reaonCode = reaonCode;
    }

    public String getReasonCodeDes() {
        return reasonCodeDes;
    }

    public void setReasonCodeDes(String reasonCodeDes) {
        this.reasonCodeDes = reasonCodeDes;
    }

    public String getReasonCodeGroup() {
        return reasonCodeGroup;
    }

    public void setReasonCodeGroup(String reasonCodeGroup) {
        this.reasonCodeGroup = reasonCodeGroup;
    }

    public String getReasonCodeGroupDes() {
        return reasonCodeGroupDes;
    }

    public void setReasonCodeGroupDes(String reasonCodeGroupDes) {
        this.reasonCodeGroupDes = reasonCodeGroupDes;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateDes() {
        return stateDes;
    }

    public void setStateDes(String stateDes) {
        this.stateDes = stateDes;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }
}
