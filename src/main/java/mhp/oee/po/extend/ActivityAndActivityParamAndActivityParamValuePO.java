package mhp.oee.po.extend;

import java.util.Date;

/**
 * Created by LinZuK on 2016/7/25.
 */
public class ActivityAndActivityParamAndActivityParamValuePO {

    private String activityParamValueHandle;
    private String activityBo;
    private String activityDesc;
    private String paramId;
    private String paramDescription;
    private String paramDefaultValue;
    private String paramValue;
    private Date activityParamValueModifiedDateTime;

    public String getActivityParamValueHandle() {
        return activityParamValueHandle;
    }

    public void setActivityParamValueHandle(String activityParamValueHandle) {
        this.activityParamValueHandle = activityParamValueHandle;
    }

    public String getActivityBo() {
        return activityBo;
    }

    public void setActivityBo(String activityBo) {
        this.activityBo = activityBo;
    }

    public String getActivityDesc() {
        return activityDesc;
    }

    public void setActivityDesc(String activityDesc) {
        this.activityDesc = activityDesc;
    }

    public String getParamId() {
        return paramId;
    }

    public void setParamId(String paramId) {
        this.paramId = paramId;
    }

    public String getParamDescription() {
        return paramDescription;
    }

    public void setParamDescription(String paramDescription) {
        this.paramDescription = paramDescription;
    }

    public String getParamDefaultValue() {
        return paramDefaultValue;
    }

    public void setParamDefaultValue(String paramDefaultValue) {
        this.paramDefaultValue = paramDefaultValue;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public Date getActivityParamValueModifiedDateTime() {
        return activityParamValueModifiedDateTime;
    }

    public void setActivityParamValueModifiedDateTime(Date activityParamValueModifiedDateTime) {
        this.activityParamValueModifiedDateTime = activityParamValueModifiedDateTime;
    }
}
