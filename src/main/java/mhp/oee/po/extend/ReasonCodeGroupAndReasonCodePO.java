package mhp.oee.po.extend;

/**
 * Created by LinZuK on 2016/8/1.
 */
public class ReasonCodeGroupAndReasonCodePO {
    private String reasonCodeBo;
    private String reasonCode;
    private String reasonCodeDesc;
    private String reasonCodeEnabled;
    private String reasonCodeGroup;
    private String reasonCodeGroupDesc;
    private String reasonCodeGroupEnabled;

    public String getReasonCodeBo() {
        return reasonCodeBo;
    }

    public void setReasonCodeBo(String reasonCodeBo) {
        this.reasonCodeBo = reasonCodeBo;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonCodeDesc() {
        return reasonCodeDesc;
    }

    public void setReasonCodeDesc(String reasonCodeDesc) {
        this.reasonCodeDesc = reasonCodeDesc;
    }

    public String getReasonCodeEnabled() {
        return reasonCodeEnabled;
    }

    public void setReasonCodeEnabled(String reasonCodeEnabled) {
        this.reasonCodeEnabled = reasonCodeEnabled;
    }

    public String getReasonCodeGroup() {
        return reasonCodeGroup;
    }

    public void setReasonCodeGroup(String reasonCodeGroup) {
        this.reasonCodeGroup = reasonCodeGroup;
    }

    public String getReasonCodeGroupDesc() {
        return reasonCodeGroupDesc;
    }

    public void setReasonCodeGroupDesc(String reasonCodeGroupDesc) {
        this.reasonCodeGroupDesc = reasonCodeGroupDesc;
    }

    public String getReasonCodeGroupEnabled() {
        return reasonCodeGroupEnabled;
    }

    public void setReasonCodeGroupEnabled(String reasonCodeGroupEnabled) {
        this.reasonCodeGroupEnabled = reasonCodeGroupEnabled;
    }
}
