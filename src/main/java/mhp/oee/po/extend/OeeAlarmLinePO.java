package mhp.oee.po.extend;

/**
 * Created by LinZuK on 2016/9/14.
 */
public class OeeAlarmLinePO {
    private String paramId;
    private String paramValue;

    public String getParamId() {
        return paramId;
    }

    public void setParamId(String paramId) {
        this.paramId = paramId;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
}
