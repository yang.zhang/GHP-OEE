package mhp.oee.po.extend;

public class ActivityPermAndActivityAndUserAndUserGroupAndUserGroupMemberPO {
    
    private String site;
    private String userId;
    private String activity;
    private String permissionMode;
    
    public String getSite() {
        return site;
    }
    public void setSite(String site) {
        this.site = site;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getActivity() {
        return activity;
    }
    public void setActivity(String activity) {
        this.activity = activity;
    }
    public String getPermissionMode() {
        return permissionMode;
    }
    public void setPermissionMode(String permissionMode) {
        this.permissionMode = permissionMode;
    }
}
