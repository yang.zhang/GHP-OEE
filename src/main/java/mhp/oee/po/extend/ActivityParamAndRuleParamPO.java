package mhp.oee.po.extend;

import java.util.Date;

/**
 * Created by LinZuK on 2016/7/28.
 */
public class ActivityParamAndRuleParamPO {
    private String reasonCodeRuleParamBo;
    private String paramId;
    private String paramDescription;
    private String paramDefaultValue;
    private String paramValue;
    private Date modifiedDateTime;
    private String modifiedUser;
    private String resourceType;

    public String getReasonCodeRuleParamBo() {
        return reasonCodeRuleParamBo;
    }

    public void setReasonCodeRuleParamBo(String reasonCodeRuleParamBo) {
        this.reasonCodeRuleParamBo = reasonCodeRuleParamBo;
    }

    public String getParamId() {
        return paramId;
    }

    public void setParamId(String paramId) {
        this.paramId = paramId;
    }

    public String getParamDescription() {
        return paramDescription;
    }

    public void setParamDescription(String paramDescription) {
        this.paramDescription = paramDescription;
    }

    public String getParamDefaultValue() {
        return paramDefaultValue;
    }

    public void setParamDefaultValue(String paramDefaultValue) {
        this.paramDefaultValue = paramDefaultValue;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }
}
