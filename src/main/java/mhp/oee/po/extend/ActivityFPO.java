package mhp.oee.po.extend;

/**
 * Created by LinZuK on 2016/7/27.
 */
public class ActivityFPO {

    private String activityF;
    private String activityFDesc;

    public String getActivityF() {
        return activityF;
    }

    public void setActivityF(String activityF) {
        this.activityF = activityF;
    }

    public String getActivityFDesc() {
        return activityFDesc;
    }

    public void setActivityFDesc(String activityFDesc) {
        this.activityFDesc = activityFDesc;
    }
}
