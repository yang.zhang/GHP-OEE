package mhp.oee.po.extend;

import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourcePlcPO;

public class ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPO extends ResourcePlcPO {
    
    private ResourcePO resourcePO;
    private String categoryDes;
    private String plcObjectDes;

    public ResourcePO getResourcePO() {
        return resourcePO;
    }

    public void setResourcePO(ResourcePO resourcePO) {
        this.resourcePO = resourcePO;
    }

    public String getCategoryDes() {
        return categoryDes;
    }

    public void setCategoryDes(String categoryDes) {
        this.categoryDes = categoryDes;
    }

    public String getPlcObjectDes() {
        return plcObjectDes;
    }

    public void setPlcObjectDes(String plcObjectDes) {
        this.plcObjectDes = plcObjectDes;
    }
}
