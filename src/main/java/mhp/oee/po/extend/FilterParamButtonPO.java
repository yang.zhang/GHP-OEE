package mhp.oee.po.extend;

import java.util.Date;

/**
 * Created by LinZuK on 2016/7/29.
 */
public class FilterParamButtonPO {
    private Date reasonCodeRuleParamModifiedDateTime;
    private String reasonCodeRuleParamBo;
    private String reasonCodeRuleBoF;
    private String activityFParamDefaultValue;
    private String activityFParamResourceType;
    private String activityFParamResourceTypeDesc;
    private String activityFParamResourceTypeEnabled;
    private String activityFParamValue;
    private Date activityFParamModifiedDateTime;
    private String activityFParamModifiedUser;

    public Date getReasonCodeRuleParamModifiedDateTime() {
        return reasonCodeRuleParamModifiedDateTime;
    }

    public void setReasonCodeRuleParamModifiedDateTime(Date reasonCodeRuleParamModifiedDateTime) {
        this.reasonCodeRuleParamModifiedDateTime = reasonCodeRuleParamModifiedDateTime;
    }

    public String getReasonCodeRuleParamBo() {
        return reasonCodeRuleParamBo;
    }

    public void setReasonCodeRuleParamBo(String reasonCodeRuleParamBo) {
        this.reasonCodeRuleParamBo = reasonCodeRuleParamBo;
    }

    public String getReasonCodeRuleBoF() {
        return reasonCodeRuleBoF;
    }

    public void setReasonCodeRuleBoF(String reasonCodeRuleBoF) {
        this.reasonCodeRuleBoF = reasonCodeRuleBoF;
    }

    public String getActivityFParamDefaultValue() {
        return activityFParamDefaultValue;
    }

    public void setActivityFParamDefaultValue(String activityFParamDefaultValue) {
        this.activityFParamDefaultValue = activityFParamDefaultValue;
    }

    public String getActivityFParamResourceType() {
        return activityFParamResourceType;
    }

    public void setActivityFParamResourceType(String activityFParamResourceType) {
        this.activityFParamResourceType = activityFParamResourceType;
    }

    public String getActivityFParamResourceTypeDesc() {
        return activityFParamResourceTypeDesc;
    }

    public void setActivityFParamResourceTypeDesc(String activityFParamResourceTypeDesc) {
        this.activityFParamResourceTypeDesc = activityFParamResourceTypeDesc;
    }

    public String getActivityFParamResourceTypeEnabled() {
        return activityFParamResourceTypeEnabled;
    }

    public void setActivityFParamResourceTypeEnabled(String activityFParamResourceTypeEnabled) {
        this.activityFParamResourceTypeEnabled = activityFParamResourceTypeEnabled;
    }

    public String getActivityFParamValue() {
        return activityFParamValue;
    }

    public void setActivityFParamValue(String activityFParamValue) {
        this.activityFParamValue = activityFParamValue;
    }

    public String getActivityFParamModifiedUser() {
        return activityFParamModifiedUser;
    }

    public void setActivityFParamModifiedUser(String activityFParamModifiedUser) {
        this.activityFParamModifiedUser = activityFParamModifiedUser;
    }

    public Date getActivityFParamModifiedDateTime() {
        return activityFParamModifiedDateTime;
    }

    public void setActivityFParamModifiedDateTime(Date activityFParamModifiedDateTime) {
        this.activityFParamModifiedDateTime = activityFParamModifiedDateTime;
    }
}
