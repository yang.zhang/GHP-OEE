package mhp.oee.po.extend;

import mhp.oee.po.gen.ActivityPermPO;

public class ActivityPermAndActivityPO extends ActivityPermPO{
    private String activity;
    private String activityDes;
    private String sequenceId;
    
    public String getActivity() {
        return activity;
    }
    public void setActivity(String activity) {
        this.activity = activity;
    }
    public String getActivityDes() {
        return activityDes;
    }
    public void setActivityDes(String activityDes) {
        this.activityDes = activityDes;
    }
    public String getSequenceId() {
        return sequenceId;
    }
    public void setSequenceId(String sequenceId) {
        this.sequenceId = sequenceId;
    }
}
