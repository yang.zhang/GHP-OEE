package mhp.oee.po.extend;

import java.util.Date;

/**
 * Created by LinZuK on 2016/7/21.
 */
public class ResourceAndWorkCenterShiftAndWorkCenterPO {
    private String shift;
    private String description;
    private Date startTime;
    private Date endTime;

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
