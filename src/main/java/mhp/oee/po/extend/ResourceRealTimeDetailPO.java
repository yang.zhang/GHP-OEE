package mhp.oee.po.extend;

/**
 * Created by DingLJ on 2017/5/1 001.
 */
public class ResourceRealTimeDetailPO {


    private String resource;
    private String resourceType;
    private String workCenter;
    private String line;
    private String elapsedTime;
    private String reasonCode;
    private long productionCount;
    private String productionType;


    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getWorkCenter() {
        return workCenter;
    }

    public void setWorkCenter(String workCenter) {
        this.workCenter = workCenter;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(String elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public long getProductionCount() {
        return productionCount;
    }

    public void setProductionCount(long productionCount) {
        this.productionCount = productionCount;
    }

    public String getProductionType() {
        return productionType;
    }

    public void setProductionType(String productionType) {
        this.productionType = productionType;
    }
}
