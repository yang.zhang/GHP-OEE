package mhp.oee.po.extend;

import java.util.Date;


public class ModifiedPO {
    private Date modifiedDateTime;
    private String modifiedDateTimeString;
    private String modifiedUser;

    public String getModifiedDateTimeString() {
        return modifiedDateTimeString;
    }

    public void setModifiedDateTimeString(String modifiedDateTimeString) {
        this.modifiedDateTimeString = modifiedDateTimeString;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }
}
