package mhp.oee.po.extend;

import java.util.List;

import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourcePlcPO;

public class ResrceAndResourcePlcPO extends ResourcePO {

    private List<ResourcePlcPO> resourcePlcPOs;
    public List<ResourcePlcPO> getResourcePlcPOs() {
        return resourcePlcPOs;
    }
    public void setResourcePlcPOs(List<ResourcePlcPO> resourcePlcPOs) {
        this.resourcePlcPOs = resourcePlcPOs;
    }

}