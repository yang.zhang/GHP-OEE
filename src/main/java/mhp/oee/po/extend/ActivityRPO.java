package mhp.oee.po.extend;

/**
 * Created by LinZuK on 2016/7/27.
 */
public class ActivityRPO {

    private String activityR;
    private String activityRDesc;

    public String getActivityR() {
        return activityR;
    }

    public void setActivityR(String activityR) {
        this.activityR = activityR;
    }

    public String getActivityRDesc() {
        return activityRDesc;
    }

    public void setActivityRDesc(String activityRDesc) {
        this.activityRDesc = activityRDesc;
    }
}
