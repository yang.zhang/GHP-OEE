package mhp.oee.po.extend;


import java.util.List;
import mhp.oee.po.gen.ActivityGroupPO;

public class ActivityAndActivityGroupAndActivityGroupActivityPO extends ActivityGroupPO{
    private List<ActivityGroupPO> activityGroupPO;
    private String activityBo;
    private String activity;
    private String activityDes;

    public List<ActivityGroupPO> getActivityGroupVO() {
        return activityGroupPO;
    }

    public void setActivityGroupVO(List<ActivityGroupPO> activityGroupPO) {
        this.activityGroupPO = activityGroupPO;
    }

    public String getActivityBo() {
        return activityBo;
    }

    public void setActivityBo(String activityBo) {
        this.activityBo = activityBo;
    }
    
    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getActivityDes() {
        return activityDes;
    }

    public void setActivityDes(String activityDes) {
        this.activityDes = activityDes;
    }
}
