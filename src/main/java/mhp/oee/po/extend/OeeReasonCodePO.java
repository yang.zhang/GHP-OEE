package mhp.oee.po.extend;

import java.math.BigDecimal;

/**
 * Created by LinZuK on 2016/8/12.
 */
public class OeeReasonCodePO {
    private String reasonCode;
    private String description;
    private BigDecimal intersectionTimeDurationSum;
    private BigDecimal qty;

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getIntersectionTimeDurationSum() {
        return intersectionTimeDurationSum;
    }

    public void setIntersectionTimeDurationSum(BigDecimal intersectionTimeDurationSum) {
        this.intersectionTimeDurationSum = intersectionTimeDurationSum;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }
}
