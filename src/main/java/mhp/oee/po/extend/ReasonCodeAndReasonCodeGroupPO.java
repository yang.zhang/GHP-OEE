package mhp.oee.po.extend;

import mhp.oee.po.gen.ReasonCodeGroupPO;
import mhp.oee.po.gen.ReasonCodePO;

public class ReasonCodeAndReasonCodeGroupPO extends ReasonCodePO{

    private ReasonCodeGroupPO reasonCodeGroupPO;

    public ReasonCodeGroupPO getReasonCodeGroupPO() {
        return reasonCodeGroupPO;
    }

    public void setReasonCodeGroupPO(ReasonCodeGroupPO reasonCodeGroupPO) {
        this.reasonCodeGroupPO = reasonCodeGroupPO;
    }

}
