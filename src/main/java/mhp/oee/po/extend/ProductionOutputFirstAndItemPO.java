package mhp.oee.po.extend;

import mhp.oee.po.gen.ProductionOutputFirstPO;

/**
 * Created by LinZuK on 2016/8/18.
 */
public class ProductionOutputFirstAndItemPO extends ProductionOutputFirstPO {
    private String model;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
