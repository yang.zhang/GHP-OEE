package mhp.oee.po.extend;


import java.util.List;

import mhp.oee.po.gen.AlertCommentPO;
import mhp.oee.po.gen.ResourceAlertLogPO;

public class ResourceAlertLogAndAlertCommentPo extends ResourceAlertLogPO{
	private List<AlertCommentPO> AlertCommentPOList;
	

	public List<AlertCommentPO> getAlertCommentPOList() {
		return AlertCommentPOList;
	}

	public void setAlertCommentPOList(List<AlertCommentPO> alertCommentPOList) {
		AlertCommentPOList = alertCommentPOList;
	}
}
