package mhp.oee.po.extend;

import java.math.BigDecimal;

/**
 * Created by LinZuK on 2016/8/12.
 */
public class OeeReasonCodeGroupPO {
    private String reasonCodeGroup;
    private String description;
    private BigDecimal intersectionTimeDurationSum;
    private String percent;
    private BigDecimal qty;

    public String getReasonCodeGroup() {
        return reasonCodeGroup;
    }

    public void setReasonCodeGroup(String reasonCodeGroup) {
        this.reasonCodeGroup = reasonCodeGroup;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getIntersectionTimeDurationSum() {
        return intersectionTimeDurationSum;
    }

    public void setIntersectionTimeDurationSum(BigDecimal intersectionTimeDurationSum) {
        this.intersectionTimeDurationSum = intersectionTimeDurationSum;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }
}
