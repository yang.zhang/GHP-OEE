package mhp.oee.po.extend;

/**
 * Created by LinZuK on 2016/8/31.
 */
public class ScheduleJobPO {
    private String activity;
    private String description;
    private String paramId;
    private String paramDefaultValue;
    private String classOrProgram;
    private String enabled;

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParamId() {
        return paramId;
    }

    public void setParamId(String paramId) {
        this.paramId = paramId;
    }

    public String getParamDefaultValue() {
        return paramDefaultValue;
    }

    public void setParamDefaultValue(String paramDefaultValue) {
        this.paramDefaultValue = paramDefaultValue;
    }

    public String getClassOrProgram() {
        return classOrProgram;
    }

    public void setClassOrProgram(String classOrProgram) {
        this.classOrProgram = classOrProgram;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }
}
