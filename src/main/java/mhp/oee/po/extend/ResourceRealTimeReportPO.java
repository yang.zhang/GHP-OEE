package mhp.oee.po.extend;

import java.util.Date;

public class ResourceRealTimeReportPO {
    
    private String site;
    private String handle;
    private String workArea;
    private String lineArea;
    private String resrce;
    private String resrceDes;
    private String workAreaDes;
    private String lineAreaDes;
    private String resourceState;	
    private String ppm;
    private Double intPPM;
    private String colorLight;
    private String alertCount;
    private Double intAlertCount;
    private String haltCount;
    private Double intHaltCount;
    private Date dateTime;
    private Integer qtyTotal;
    private String strQtyTotal;
    private Date alertTime;
    private String description;
    private String item;
    
    public Date getAlertTime() {
		return alertTime;
	}
	public void setAlertTime(Date alertTime) {
		this.alertTime = alertTime;
	}
    
    public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getSite() {
        return site;
    }
    public void setSite(String site) {
        this.site = site;
    }
    public String getHandle() {
        return handle;
    }
    public void setHandle(String handle) {
        this.handle = handle;
    }
    public String getWorkArea() {
        return workArea;
    }
    public void setWorkArea(String workArea) {
        this.workArea = workArea;
    }
    public String getLineArea() {
        return lineArea;
    }
    public void setLineArea(String lineArea) {
        this.lineArea = lineArea;
    }
    public String getResrce() {
        return resrce;
    }
    public void setResrce(String resrce) {
        this.resrce = resrce;
    }
    public String getResrceDes() {
        return resrceDes;
    }
    public void setResrceDes(String resrceDes) {
        this.resrceDes = resrceDes;
    }
    public String getWorkAreaDes() {
        return workAreaDes;
    }
    public void setWorkAreaDes(String workAreaDes) {
        this.workAreaDes = workAreaDes;
    }
    public String getLineAreaDes() {
        return lineAreaDes;
    }
    public void setLineAreaDes(String lineAreaDes) {
        this.lineAreaDes = lineAreaDes;
    }
    public String getResourceState() {
        return resourceState;
    }
    public void setResourceState(String resourceState) {
        this.resourceState = resourceState;
    }
    public String getPpm() {
        return ppm;
    }
    public void setPpm(String ppm) {
        this.ppm = ppm;
    }
    public String getColorLight() {
        return colorLight;
    }
    public void setColorLight(String colorLight) {
        this.colorLight = colorLight;
    }
    public String getAlertCount() {
        return alertCount;
    }
    public void setAlertCount(String alertCount) {
        this.alertCount = alertCount;
    }
    public String getHaltCount() {
        return haltCount;
    }
    public void setHaltCount(String haltCount) {
        this.haltCount = haltCount;
    }
    public Date getDateTime() {
        return dateTime;
    }
    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
    public Integer getQtyTotal() {
        return qtyTotal;
    }
    public void setQtyTotal(Integer qtyTotal) {
        this.qtyTotal = qtyTotal;
    }
    public String getStrQtyTotal() {
        return strQtyTotal;
    }
    public void setStrQtyTotal(String strQtyTotal) {
        this.strQtyTotal = strQtyTotal;
    }
    public Double getIntPPM() {
        return intPPM;
    }
    public void setIntPPM(Double intPPM) {
        this.intPPM = intPPM;
    }
    public Double getIntAlertCount() {
        return intAlertCount;
    }
    public void setIntAlertCount(Double intAlertCount) {
        this.intAlertCount = intAlertCount;
    }
    public Double getIntHaltCount() {
        return intHaltCount;
    }
    public void setIntHaltCount(Double intHaltCount) {
        this.intHaltCount = intHaltCount;
    }
    
    
}
