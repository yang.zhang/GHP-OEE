package mhp.oee.po.extend;

/**
 * Created by DingLJ on 2017/4/26 026.
 */
public class ResourceRealTimeInfoPO {

    private String dimensionCode;
    private String dimensionName;
    private long resourceCount;
    private long productionCount;
    private long redCount;
    private long yellowCount;
    private long greenCount;

    private Double redRate;
    private Double yellowRate;
    private Double greenRate;

    public String getDimensionCode() {
        return dimensionCode;
    }

    public void setDimensionCode(String dimensionCode) {
        this.dimensionCode = dimensionCode;
    }

    public String getDimensionName() {
        return dimensionName;
    }

    public void setDimensionName(String dimensionName) {
        this.dimensionName = dimensionName;
    }

    public long getResourceCount() {
        return resourceCount;
    }

    public void setResourceCount(long resourceCount) {
        this.resourceCount = resourceCount;
    }

    public long getProductionCount() {
        return productionCount;
    }

    public void setProductionCount(long productionCount) {
        this.productionCount = productionCount;
    }

    public long getRedCount() {
        return redCount;
    }

    public void setRedCount(long redCount) {
        this.redCount = redCount;
    }

    public long getYellowCount() {
        return yellowCount;
    }

    public void setYellowCount(long yellowCount) {
        this.yellowCount = yellowCount;
    }

    public long getGreenCount() {
        return greenCount;
    }

    public void setGreenCount(long greenCount) {
        this.greenCount = greenCount;
    }

    public Double getRedRate() {
        return redRate;
    }

    public void setRedRate(Double redRate) {
        this.redRate = redRate;
    }

    public Double getYellowRate() {
        return yellowRate;
    }

    public void setYellowRate(Double yellowRate) {
        this.yellowRate = yellowRate;
    }

    public Double getGreenRate() {
        return greenRate;
    }

    public void setGreenRate(Double greenRate) {
        this.greenRate = greenRate;
    }
}
