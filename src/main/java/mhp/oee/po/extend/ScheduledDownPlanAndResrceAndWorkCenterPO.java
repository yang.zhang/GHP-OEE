package mhp.oee.po.extend;

import java.util.Date;
public class ScheduledDownPlanAndResrceAndWorkCenterPO{
    private String handle;
    private String site;
    private String resrce;
    private String asset;
    private String resourceBo;
    private String strt;
    private String resrceDes;
    private String workCenter;
    private String workCenterDes;
    private String line;
    private String lineDes;
    private Date   startDateTime;
    private Date   endDateTime;
    private String reasonCode;
    private Date modifiedDateTime;
    private String reasonCodeDes;
    
    public String getHandle() {
        return handle;
    }
    public void setHandle(String handle) {
        this.handle = handle;
    }
    public String getSite() {
        return site;
    }
    public void setSite(String site) {
        this.site = site;
    }
    public String getResrce() {
        return resrce;
    }
    public void setResrce(String resrce) {
        this.resrce = resrce;
    }
    public String getResourceBo() {
        return resourceBo;
    }
    public void setResourceBo(String resourceBo) {
        this.resourceBo = resourceBo;
    }
    public String getStrt() {
        return strt;
    }
    public void setStrt(String strt) {
        this.strt = strt;
    }
    public String getResrceDes() {
        return resrceDes;
    }
    public void setResrceDes(String resrceDes) {
        this.resrceDes = resrceDes;
    }
    public String getWorkCenter() {
        return workCenter;
    }
    public void setWorkCenter(String workCenter) {
        this.workCenter = workCenter;
    }
    public String getWorkCenterDes() {
        return workCenterDes;
    }
    public void setWorkCenterDes(String workCenterDes) {
        this.workCenterDes = workCenterDes;
    }
    public String getLine() {
        return line;
    }
    public void setLine(String line) {
        this.line = line;
    }
    public String getLineDes() {
        return lineDes;
    }
    public void setLineDes(String lineDes) {
        this.lineDes = lineDes;
    }
    public Date getStartDateTime() {
        return startDateTime;
    }
    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }
    public Date getEndDateTime() {
        return endDateTime;
    }
    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }
    public String getReasonCode() {
        return reasonCode;
    }
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }
    public String getAsset() {
        return asset;
    }
    public void setAsset(String asset) {
        this.asset = asset;
    }
    public String getReasonCodeDes() {
        return reasonCodeDes;
    }
    public void setReasonCodeDes(String reasonCodeDes) {
        this.reasonCodeDes = reasonCodeDes;
    }
}
