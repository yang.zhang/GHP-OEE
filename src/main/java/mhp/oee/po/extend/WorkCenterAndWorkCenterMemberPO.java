package mhp.oee.po.extend;

/**
 * Created by LinZuK on 2016/7/22.
 */
public class WorkCenterAndWorkCenterMemberPO {

    private String workCenter;
    private String description;

    public String getWorkCenter() {
        return workCenter;
    }

    public void setWorkCenter(String workCenter) {
        this.workCenter = workCenter;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
