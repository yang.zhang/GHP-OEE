package mhp.oee.po.extend;

import mhp.oee.po.gen.PlcAccessTypePO;
import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourcePlcInfoPO;

public class ResourcePlcInfoAndResrceAndPlcAccessTypePO extends ResourcePlcInfoPO{
    private int id;
    private ResourcePO resourcePO;
    private PlcAccessTypePO plcAccessTypePO;

    public PlcAccessTypePO getPlcAccessTypePO() {
        return plcAccessTypePO;
    }

    public void setPlcAccessTypePO(PlcAccessTypePO plcAccessTypePO) {
        this.plcAccessTypePO = plcAccessTypePO;
    }

    public ResourcePO getResourcePO() {
        return resourcePO;
    }

    public void setResourcePO(ResourcePO resourcePO) {
        this.resourcePO = resourcePO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}