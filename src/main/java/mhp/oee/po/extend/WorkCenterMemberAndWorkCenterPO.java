package mhp.oee.po.extend;

import mhp.oee.po.gen.WorkCenterMemberPO;
import mhp.oee.po.gen.WorkCenterPO;

public class WorkCenterMemberAndWorkCenterPO extends WorkCenterMemberPO {

    private WorkCenterPO workCenterPO;

    public WorkCenterPO getWorkCenterPO() {
        return workCenterPO;
    }

    public void setWorkCenterPO(WorkCenterPO workCenterPO) {
        this.workCenterPO = workCenterPO;
    }
}
