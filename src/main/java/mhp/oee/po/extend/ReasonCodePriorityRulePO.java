package mhp.oee.po.extend;

import mhp.oee.po.gen.ReasonCodePriorityPO;
import mhp.oee.po.gen.ReasonCodeRulePO;

public class ReasonCodePriorityRulePO {

    private ReasonCodePriorityPO reasonCodePriorityPO;

    private ReasonCodeRulePO reasonCodeRulePO;


    public ReasonCodePriorityPO getReasonCodePriorityPO() {
        return reasonCodePriorityPO;
    }

    public void setReasonCodePriorityPO(ReasonCodePriorityPO reasonCodePriorityPO) {
        this.reasonCodePriorityPO = reasonCodePriorityPO;
    }

    public ReasonCodeRulePO getReasonCodeRulePO() {
        return reasonCodeRulePO;
    }

    public void setReasonCodeRulePO(ReasonCodeRulePO reasonCodeRulePO) {
        this.reasonCodeRulePO = reasonCodeRulePO;
    }
}