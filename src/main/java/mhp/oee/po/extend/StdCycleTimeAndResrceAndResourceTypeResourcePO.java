package mhp.oee.po.extend;

import mhp.oee.po.gen.StdCycleTimePO;

public class StdCycleTimeAndResrceAndResourceTypeResourcePO extends StdCycleTimePO {
    private String description;
    private String asset;
    private String resourceTypeBo;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getResourceTypeBo() {
        return resourceTypeBo;
    }

    public void setResourceTypeBo(String resourceTypeBo) {
        this.resourceTypeBo = resourceTypeBo;
    }
}
