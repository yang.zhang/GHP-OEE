package mhp.oee.po.extend;

import mhp.oee.po.gen.ItemPO;
import mhp.oee.po.gen.ProductionOutputTargetPO;
import mhp.oee.po.gen.ResourcePO;

public class ProductionOutputTargetAndResrcePO extends ProductionOutputTargetPO {
    private String model;
    private ResourcePO resourcePO;
    private ItemPO itemPO;

    public ResourcePO getResourcePO() {
        return resourcePO;
    }

    public void setResourcePO(ResourcePO resourcePO) {
        this.resourcePO = resourcePO;
    }

    public ItemPO getItemPO() {
        return itemPO;
    }

    public void setItemPO(ItemPO itemPO) {
        this.itemPO = itemPO;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
