package mhp.oee.po.extend;

import mhp.oee.po.gen.ResourcePO;
import mhp.oee.po.gen.ResourceTypePO;
import mhp.oee.po.gen.ResourceTypeResourcePO;

public class ResourceTypeResourceAndResourceAndResourceTypePO extends ResourceTypeResourcePO{

    private ResourcePO resourcePO;
    private ResourceTypePO resourceTypePO;

    public ResourceTypePO getResourceTypePO() {
        return resourceTypePO;
    }

    public void setResourceTypePO(ResourceTypePO resourceTypePO) {
        this.resourceTypePO = resourceTypePO;
    }

    public ResourcePO getResourcePO() {
        return resourcePO;
    }

    public void setResourcePO(ResourcePO resourcePO) {
        this.resourcePO = resourcePO;
    }
}