package mhp.oee.po.extend;

import java.util.Date;

/**
 * Created by LinZuK on 2016/7/27.
 */
public class ReasonCodePriorityAndReasonCodePO {

    private String reasonCodePriorityBo;    // REASON_CODE_PRIORITY_BO       REASON_CODE_PRIORITY      HANDLE
    private String reasonCodeBo;             // REASON_CODE_BO                REASON_CODE_PRIORITY      REASON_CODE_BO
    private String reasonCode;               // REASON_CODE                   REASON_CODE               REASON_CODE
    private String reasonCodeDesc;          // REASON_CODE_DESC              REASON_CODE               DESCRIPTION
    private String priority;                 // PRIORITY                      REASON_CODE_PRIORITY      PRIORITY
    private String subPriority;             // SUB_PRIORITY                  REASON_CODE_PRIORITY      SUB_PRIORITY
    private Date effDateTime;             // EFF_DATE_TIME                 REASON_CODE_PRIORITY      START_DATE_TIME
    private String effDateTimeString;
    private Date effEndDateTime;
    private String effEndDateTimeString;
    private String used;                     // USED                          REASON_CODE_PRIORITY      USED

    public String getEffDateTimeString() {
        return effDateTimeString;
    }

    public void setEffDateTimeString(String effDateTimeString) {
        this.effDateTimeString = effDateTimeString;
    }

    public String getEffEndDateTimeString() {
        return effEndDateTimeString;
    }

    public void setEffEndDateTimeString(String effEndDateTimeString) {
        this.effEndDateTimeString = effEndDateTimeString;
    }

    public Date getEffEndDateTime() {
        return effEndDateTime;
    }

    public void setEffEndDateTime(Date effEndDateTime) {
        this.effEndDateTime = effEndDateTime;
    }

    public String getReasonCodePriorityBo() {
        return reasonCodePriorityBo;
    }

    public void setReasonCodePriorityBo(String reasonCodePriorityBo) {
        this.reasonCodePriorityBo = reasonCodePriorityBo;
    }

    public String getReasonCodeBo() {
        return reasonCodeBo;
    }

    public void setReasonCodeBo(String reasonCodeBo) {
        this.reasonCodeBo = reasonCodeBo;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonCodeDesc() {
        return reasonCodeDesc;
    }

    public void setReasonCodeDesc(String reasonCodeDesc) {
        this.reasonCodeDesc = reasonCodeDesc;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getSubPriority() {
        return subPriority;
    }

    public void setSubPriority(String subPriority) {
        this.subPriority = subPriority;
    }

    public Date getEffDateTime() {
        return effDateTime;
    }

    public void setEffDateTime(Date effDateTime) {
        this.effDateTime = effDateTime;
    }

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }
}
