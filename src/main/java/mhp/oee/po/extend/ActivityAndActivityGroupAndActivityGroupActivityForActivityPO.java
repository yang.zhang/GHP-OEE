package mhp.oee.po.extend;


import java.util.List;
import mhp.oee.po.gen.ActivityPO;

public class ActivityAndActivityGroupAndActivityGroupActivityForActivityPO extends ActivityPO{
    private List<ActivityPO> activityPO;

    public List<ActivityPO> getActivityVO() {
        return activityPO;
    }

    public void setActivityVO(List<ActivityPO> activityPO) {
        this.activityPO = activityPO;
    }
    
    
}
