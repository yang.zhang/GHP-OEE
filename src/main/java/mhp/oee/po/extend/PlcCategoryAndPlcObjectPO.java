package mhp.oee.po.extend;

import java.util.List;

import mhp.oee.po.gen.PlcCategoryPO;
import mhp.oee.po.gen.PlcObjectPO;

public class PlcCategoryAndPlcObjectPO extends PlcCategoryPO {
    private List<PlcObjectPO> plcObjectPOs;

    public List<PlcObjectPO> getPlcObjectPOs() {
        return plcObjectPOs;
    }

    public void setPlcObjectPOs(List<PlcObjectPO> plcObjectPOs) {
        this.plcObjectPOs = plcObjectPOs;
    }
}