package mhp.oee.po.extend;

import mhp.oee.po.gen.PlcCategoryPO;
import mhp.oee.po.gen.PlcObjectPO;

public class PlcObjectAndPlcCategoryPO extends PlcObjectPO {
    private PlcCategoryPO plcCategoryPO;

    public PlcCategoryPO getPlcCategoryPO() {
        return plcCategoryPO;
    }

    public void setPlcCategoryPO(PlcCategoryPO plcCategoryPO) {
        this.plcCategoryPO = plcCategoryPO;
    }
}