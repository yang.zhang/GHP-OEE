package mhp.oee.job.reasoncodereconizejob;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.ReasonCodePriorityComponent;
import mhp.oee.component.production.ResourceReasonCodeLogComponent;
import mhp.oee.po.gen.ReasonCodePriorityPO;
import mhp.oee.po.gen.ResourceReasonCodeLogPO;
import mhp.oee.utils.JdbcHelper;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by LinZuK on 2016/9/9.
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class JobService {

    @InjectableLogger
    private static Logger logger;

    @Autowired
    private ResourceReasonCodeLogComponent resourceReasonCodeLogComponent;
    @Autowired
    ReasonCodePriorityComponent reasonCodePriorityComponent;

    public void insertResourceReasonCodeLogList(String modifiedUser, List<ResourceReasonCodeLogPO> rrclList,Map<String,String> sql_map) {
        
    	for (ResourceReasonCodeLogPO resourceReasonCodeLogPO : rrclList) {
            logger.info("SUCCESS for resource:" + resourceReasonCodeLogPO.getResourceBo() + ":  startDateTime: "
                    + resourceReasonCodeLogPO.getStartDateTime() + ", endDateTime: "
                    + resourceReasonCodeLogPO.getEndDateTime() + ", reasonCode: "
                    + resourceReasonCodeLogPO.getReasonCode());
            // TODO insert the data into resourceReasonCodeLog table and
            resourceReasonCodeLogPO.setModifiedUser(modifiedUser);
           String sql = resourceReasonCodeLogComponent.createOrUpdateResourceReasonCodeLog(resourceReasonCodeLogPO);
           sql_map.put(resourceReasonCodeLogPO.getHandle(), sql);
    	}    	
    	
    }

    public void updateReasonCodePriorityList(String jobName, List<ReasonCodePriorityPO> notUsedReasonCodePriorityPOs) {
        for (ReasonCodePriorityPO notUsedReasonCodePriorityPO : notUsedReasonCodePriorityPOs) {
            logger.info("Need to update used priority: " + notUsedReasonCodePriorityPO.getHandle() + " before: " + notUsedReasonCodePriorityPO.getUsed());
            notUsedReasonCodePriorityPO.setUsed("true");
            notUsedReasonCodePriorityPO.setModifiedUser(jobName);
            reasonCodePriorityComponent.updateReasonCodePriority(notUsedReasonCodePriorityPO, false);
        }
    }

    public void deleteResourceReasonCodeLogList(Date recognizeStartDateTime, Date recognizeEndDateTime, String resourceBo) {
        resourceReasonCodeLogComponent.deleteResourceReasonCodeLogByRange(recognizeStartDateTime, recognizeEndDateTime, resourceBo);
    }
}
