package mhp.oee.job.reasoncodereconizejob;

import mhp.oee.po.gen.ReasonCodePriorityPO;

/**
 * Created by LinZuK on 2016/8/29.
 */
public class HitData {
    private boolean isHit;
    private String activityRule;
    private ReasonCodePriorityPO reasonCodePriorityPO;

    public HitData(boolean isHit, String activityRule, ReasonCodePriorityPO reasonCodePriorityPO) {
        this.isHit = isHit;
        this.activityRule = activityRule;
        this.reasonCodePriorityPO = reasonCodePriorityPO;
    }

    public boolean isHit() {
        return isHit;
    }

    public void setHit(boolean hit) {
        isHit = hit;
    }

    public ReasonCodePriorityPO getReasonCodePriorityPO() {
        return reasonCodePriorityPO;
    }

    public void setReasonCodePriorityPO(ReasonCodePriorityPO reasonCodePriorityPO) {
        this.reasonCodePriorityPO = reasonCodePriorityPO;
    }

    public String getActivityRule() {
        return activityRule;
    }

    public void setActivityRule(String activityRule) {
        this.activityRule = activityRule;
    }
}
