package mhp.oee.job.reasoncodereconizejob;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.handle.ReasonCodeBOHandle;
import mhp.oee.common.handle.ResourceStateBOHandle;
import mhp.oee.common.handle.ResourceTimeLogBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.plant.*;
import mhp.oee.component.production.ResourceAlertLogComponent;
import mhp.oee.component.production.ResourceReasonLogComponent;
import mhp.oee.po.gen.*;
import mhp.oee.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

@Component
public class DetermineReasonCode {

	private static final String RULE0001 = "RULE0001";
	private static final String RULE0002 = "RULE0002";
	private static final String RULE0003 = "RULE0003";
	private static final String RULE0004 = "RULE0004";
	private static final String RULE0005 = "RULE0005";
	private static final String RULE0006 = "RULE0006";
	private static final String RULE0007 = "RULE0007";
	private static final String RULE0008 = "RULE0008";
	private static final String RULE0009 = "RULE0009";
	private static final String RULE0010 = "RULE0010";

	@InjectableLogger
	private static Logger logger;

	@Autowired
	ReasonCodeResStateComponent reasonCodeResStateComponent;
	@Autowired
	ScheduledDownPlanComponent scheduledDownPlanComponent;
	@Autowired
	ReasonCodeRuleComponent reasonCodeRuleComponent;
	@Autowired
	ResourceReasonLogComponent resourceReasonLogComponent;
	@Autowired
	ReasonCodeRuleParamComponent reasonCodeRuleParamComponent;
	@Autowired
	ResourceComponent resourceComponent;
	@Autowired
	WorkCenterComponent workCenterComponent;
	@Autowired
	WorkCenterShiftComponent workCenterShiftComponent;
	@Autowired
	ResourceAlertLogComponent resourceAlertLogComponent;
	@Autowired
	ReasonCodeAlertComponent reasonCodeAlertComponent;

	public boolean checkReasonCodeByActivity(List<ResourceTimeLogPO> resourceTimeLogPOs, int resourceTimeLogIndex,
			ResourceTimeLogPO resourceTimeLogPO, ReasonCodePriorityPO reasonCodePriorityPO, String activity,
			String resourceBo) {
		ResourceStateBOHandle resourceStateBOHandle = new ResourceStateBOHandle(resourceTimeLogPO.getSite(),
				resourceTimeLogPO.getResourceState());
		if (activity.equalsIgnoreCase(RULE0001)) {
			if (resourceTimeLogPO.getResourceState().equalsIgnoreCase("P")) {
				// return
				// reasonCodeResStateComponent.isReasonCodeBoMatchResourceState(resourceStateBOHandle.getValue(),
				// reasonCodePriorityPO.getReasonCodeBo());
				return checkFilter(reasonCodePriorityPO.getHandle(), resourceTimeLogPO, resourceBo);
			}
		}
		if (activity.equalsIgnoreCase(RULE0002)) {
			// if there is a cross set of the time zones,
			// it matches by the reason code
			if (!resourceTimeLogPO.getResourceState().equalsIgnoreCase("p")) {
				List<ScheduledDownPlanPO> ret = scheduledDownPlanComponent.readRecordsWithinTimeRange(
						reasonCodePriorityPO.getReasonCodeBo(), resourceTimeLogPO.getResourceBo(),
						resourceTimeLogPO.getStartDateTime(), resourceTimeLogPO.getEndDateTime());
				if (ret != null) {
					return checkFilter(reasonCodePriorityPO.getHandle(), resourceTimeLogPO, resourceBo);
				}
			}
		}
		if (activity.equalsIgnoreCase(RULE0003)) {
			if (!resourceTimeLogPO.getResourceState().equalsIgnoreCase("P")) {
				String reasonCode = new ReasonCodeBOHandle(reasonCodePriorityPO.getReasonCodeBo()).getReasonCode();
				// List<ResourceReasonLogPO> rrlList =
				// resourceReasonLogComponent.readRecordsByStartTime(resourceTimeLogPO,
				// reasonCode, "START");
				// if (rrlList.size() == 0) {
				// return false;
				// }
				// if (rrlList.size() > 1) {
				// logger.info("find more than one manul select reason code in
				// one time range for "
				// + resourceTimeLogPO.getHandle());
				// return false;
				// } else if (rrlList.size() < 1) {
				// return false;
				// }
				// 判断第一格是否有start节点，且后面是否有end节点
				int startScanPoint = resourceTimeLogIndex;
				int endScanPoint = resourceTimeLogPOs.size() - 1;
				List<ResourceReasonLogPO> rrlEndList = new LinkedList<ResourceReasonLogPO>();
				List<ResourceReasonLogPO> rrlStartList = new LinkedList<ResourceReasonLogPO>(); // 返回不为空的话，至少一个，最多两个
				Integer endPointOffset = scanEndsBetweenTwoStart(resourceTimeLogPOs, startScanPoint, endScanPoint,
						reasonCode, rrlEndList, rrlStartList);
				if (null == endPointOffset) {
					return false;
				}
				return checkFilter(reasonCodePriorityPO.getHandle(), resourceTimeLogPO, resourceBo);
			}
		}
		if (activity.equalsIgnoreCase(RULE0004)) {
			if (!resourceTimeLogPO.getResourceState().equalsIgnoreCase("p")) {
				String reasonCode = new ReasonCodeBOHandle(reasonCodePriorityPO.getReasonCodeBo()).getReasonCode();
				List<ResourceReasonLogPO> rrlList = resourceReasonLogComponent
						.readRecordsByResourceTimeLog(resourceTimeLogPO, reasonCode);
				if (rrlList.size() > 1) {
					String reasoncodeMore = rrlList.get(0).getReasonCode();
					for (ResourceReasonLogPO rrlp : rrlList) {
						if (!rrlp.getReasonCode().equals(reasoncodeMore)) {
							logger.info("find more than one manul select reason code in one time range for "
									+ resourceTimeLogPO.getHandle());
							return false;
						}
					}
					return checkFilter(reasonCodePriorityPO.getHandle(), resourceTimeLogPO, resourceBo);
				} else if (rrlList.size() < 1) {
					return false;
				}
				return checkFilter(reasonCodePriorityPO.getHandle(), resourceTimeLogPO, resourceBo);
			}
		}
		if (activity.equalsIgnoreCase(RULE0005)) {
			if (!resourceTimeLogPO.getResourceState().equalsIgnoreCase("p")) {
				String reasonCodeRuleBo = reasonCodeRuleComponent
						.getReasonCodeRuleByPriorityBO(reasonCodePriorityPO.getHandle(), "RECOGNITION");
				List<ReasonCodeRuleParamPO> paramList = reasonCodeRuleParamComponent.getRuleParam(reasonCodeRuleBo);
				Boolean hitStartTime = false;
				ArrayList<String> stList = new ArrayList<String>();
				ArrayList<String> etList = new ArrayList<String>();
				for (ReasonCodeRuleParamPO reasonCodeRuleParamPO : paramList) {
					if (reasonCodeRuleParamPO.getParamId().startsWith("ST")) {
						stList.add(reasonCodeRuleParamPO.getParamValue());
					}
					if (reasonCodeRuleParamPO.getParamId().startsWith("ED")) {
						etList.add(reasonCodeRuleParamPO.getParamValue());
					}
				}
				for (int i = 0; i < stList.size() && i < etList.size(); i++) {
					try {
						hitStartTime = Utils.compareDateToStringWithinSameDay(resourceTimeLogPO.getStartDateTime(),
								stList.get(i), etList.get(i));
						if (hitStartTime) {
							return checkFilter(reasonCodePriorityPO.getHandle(), resourceTimeLogPO, resourceBo);
						}
					} catch (ParseException e) {
						logger.error(e.getMessage());
					}
				}
				return false;
			}
		}
		if (activity.equalsIgnoreCase(RULE0006)) {
			if (!resourceTimeLogPO.getResourceState().equalsIgnoreCase("p")) {
				ResourcePO resourcePO = resourceComponent.getResourceByHandle(resourceTimeLogPO.getResourceBo());
				List<WorkCenterShiftPO> workCenterShiftPOs = workCenterShiftComponent.getWorkCenterShiftByWorkCenterBO(
						workCenterComponent.readWorkCenterBo(resourcePO.getSite(), resourcePO.getWorkArea()));
				String reasonCodeRuleBo = reasonCodeRuleComponent
						.getReasonCodeRuleByPriorityBO(reasonCodePriorityPO.getHandle(), "RECOGNITION");
				List<ReasonCodeRuleParamPO> paramList = reasonCodeRuleParamComponent.getRuleParam(reasonCodeRuleBo);
				int preMinute = 0;
				int postMinute = 0;
				Date startDate = new Date();
				Date endDate = new Date();
				for (int i = 0; i < paramList.size(); i++) {
					if (paramList.get(i).getParamId().equalsIgnoreCase("VA01")) {
						preMinute = Integer.parseInt(paramList.get(i).getParamValue());
					}
					if (paramList.get(i).getParamId().equalsIgnoreCase("VA02")) {
						postMinute = Integer.parseInt(paramList.get(i).getParamValue());
					}
				}
				for (int i = 0; i < workCenterShiftPOs.size(); i++) {
					try {
						startDate = Utils.addMinuteToDate(
								Utils.parseDateFromReferenceDate(workCenterShiftPOs.get(i).getEndTime(),
										resourceTimeLogPO.getStartDateTime()),
								preMinute, false);
						endDate = Utils.addMinuteToDate(
								Utils.parseDateFromReferenceDate(workCenterShiftPOs.get(i).getEndTime(),
										resourceTimeLogPO.getStartDateTime()),
								postMinute, true);
						if (Utils.compareDate(startDate, resourceTimeLogPO.getStartDateTime())
								&& Utils.compareDate(resourceTimeLogPO.getStartDateTime(), endDate)) {
							return checkFilter(reasonCodePriorityPO.getHandle(), resourceTimeLogPO, resourceBo);
						}
					} catch (ParseException e) {
						logger.error(e.getMessage());
					}
				}
				return false;
			}
		}
		if (activity.equalsIgnoreCase(RULE0007)) {
			if (!resourceTimeLogPO.getResourceState().equalsIgnoreCase("p")) {
				String reasonCodeRuleBo = reasonCodeRuleComponent
						.getReasonCodeRuleByPriorityBO(reasonCodePriorityPO.getHandle(), "RECOGNITION");
				List<ReasonCodeRuleParamPO> paramList = reasonCodeRuleParamComponent.getRuleParam(reasonCodeRuleBo);
				Long diffTime = Utils.getDiffMinuteTime(resourceTimeLogPO.getStartDateTime(),
						resourceTimeLogPO.getEndDateTime());
				if (paramList.size() > 0) {
					int minMinute = Integer.parseInt(paramList.get(0).getParamValue());
					if (minMinute <= diffTime) {
						return checkFilter(reasonCodePriorityPO.getHandle(), resourceTimeLogPO, resourceBo);
					}
				}
			}
		}
		if (activity.equalsIgnoreCase(RULE0008)) {
			if (!resourceTimeLogPO.getResourceState().equalsIgnoreCase("p")) {
				String reasonCodeRuleBo = reasonCodeRuleComponent
						.getReasonCodeRuleByPriorityBO(reasonCodePriorityPO.getHandle(), "RECOGNITION");
				List<ReasonCodeRuleParamPO> paramList = reasonCodeRuleParamComponent.getRuleParam(reasonCodeRuleBo);
				double preMinute = 0.0;
				double postMinute = 0.0;
				for (int i = 0; i < paramList.size(); i++) {
					if (paramList.get(i).getParamId().equalsIgnoreCase("VA01")) {
						preMinute = Double.parseDouble(paramList.get(i).getParamValue());
					}
					if (paramList.get(i).getParamId().equalsIgnoreCase("VA02")) {
						postMinute = Double.parseDouble(paramList.get(i).getParamValue());
					}
				}
				Date startTime = Utils.addSecondToDate(resourceTimeLogPO.getStartDateTime(), preMinute, false);
				Date endTime = Utils.addSecondToDate(resourceTimeLogPO.getStartDateTime(), postMinute, true);
				ResrceBOHandle resrceBOHandle = new ResrceBOHandle(resourceTimeLogPO.getResourceBo());
				try {
					List<ResourceAlertLogPO> resourceAlertLogPOs = resourceAlertLogComponent
							.readRecordsWithinTimeRange(startTime, endTime, resrceBOHandle.getResrce());
					for (ResourceAlertLogPO resourceAlertLogPO : resourceAlertLogPOs) {
						if (reasonCodeAlertComponent.isReasonCodeMatchAlert(reasonCodePriorityPO.getReasonCodeBo(),
								resourceAlertLogPO.getDescription())) {
							return checkFilter(reasonCodePriorityPO.getHandle(), resourceTimeLogPO, resourceBo);
						}
					}
				} catch (BusinessException e) {
					return false;
				}
			}
		}
		if (activity.equalsIgnoreCase(RULE0009)) {
			if (!resourceTimeLogPO.getResourceState().equalsIgnoreCase("p")) {
				String reasonCode = new ReasonCodeBOHandle(reasonCodePriorityPO.getReasonCodeBo()).getReasonCode();
				List<ResourceReasonLogPO> rrlList = resourceReasonLogComponent
						.readRecordsByResourceTimeLog(resourceTimeLogPO, reasonCode);
				/*if (rrlList.size() > 1) {
					logger.info("find more than one manul select reason code in one time range for "
							+ resourceTimeLogPO.getHandle());
					return false;
				} else*/ if (rrlList.size() < 1) {
					return false;
				}
				return checkFilter(reasonCodePriorityPO.getHandle(), resourceTimeLogPO, resourceBo);
			}
		}
		if (activity.equalsIgnoreCase(RULE0010)) {
			if (!resourceTimeLogPO.getResourceState().equalsIgnoreCase("p")) {
				return checkFilter(reasonCodePriorityPO.getHandle(), resourceTimeLogPO, resourceBo);
			}
		}
		return false;
	}

	// split the time for the
	public void addtionalReasonCode(List<ResourceReasonCodeLogPO> resultList, ResourceReasonCodeLogPO currentMember,
			ReasonCodePriorityPO reasonCodePriorityPO, LinkedList<ResourceTimeLogPO> resourceTimeLogPOs,
			int resourceTimeLogIndex) {
		String activity = reasonCodeRuleComponent.getActivityNameByReasonCodePriorityBO(
				reasonCodePriorityPO.getHandle() == null ? "" : reasonCodePriorityPO.getHandle(), "RECOGNITION");
		if (activity.equalsIgnoreCase(RULE0003)) {
			resultList.remove(resultList.size() - 1);
			// 查找start切点
			// int startScanPoint = resourceTimeLogIndex;
			// int endScanPoint = resourceTimeLogPOs.size() - 1;
			// List<ResourceReasonLogPO> rrlEndList = new
			// LinkedList<ResourceReasonLogPO>();
			// Integer endPointOffset = scanEndPoint(resourceTimeLogPOs,
			// startScanPoint, endScanPoint, currentMember.getReasonCode(),
			// rrlEndList);
			// 判断第一格是否有start节点，且后面是否有end节点
			final int startScanPoint = resourceTimeLogIndex;
			final int endScanPoint = resourceTimeLogPOs.size() - 1;
			List<ResourceReasonLogPO> rrlEndList = new LinkedList<ResourceReasonLogPO>();
			List<ResourceReasonLogPO> rrlStartList = new LinkedList<ResourceReasonLogPO>(); // 返回不为空的话，至少一个，最多两个
			Integer endPointOffset = scanEndsBetweenTwoStart(resourceTimeLogPOs, startScanPoint, endScanPoint,
					currentMember.getReasonCode(), rrlEndList, rrlStartList);
			if (endPointOffset == null) {
				return;
			} else {
				ResourceReasonLogPO rrlStart = rrlStartList.get(0);
				ResourceReasonLogPO rrlEnd = rrlEndList.get(rrlEndList.size() - 1);
				ResourceTimeLogPO startRTL = resourceTimeLogPOs.get(startScanPoint);
				ResourceTimeLogPO endRTL = resourceTimeLogPOs.get(endPointOffset);
				String maintenanceOrder = rrlEnd.getMaintenanceOrder();

				// 查找前半段的reasonCode
				String reasonCodeRuleBo = reasonCodeRuleComponent
						.getReasonCodeRuleByPriorityBO(reasonCodePriorityPO.getHandle(), "RECOGNITION");
				List<ReasonCodeRuleParamPO> paramList = reasonCodeRuleParamComponent.getRuleParam(reasonCodeRuleBo);
				String preReasonCode = "";
				for (int i = 0; i < paramList.size(); i++) {
					if (paramList.get(i).getParamId().equalsIgnoreCase("PA01")) {
						preReasonCode = paramList.get(i).getParamValue();
					}
				}
				// 将数据切割成前后两段
				Date midTime = rrlStart.getDateTime(); // 切点时间
				ResourceReasonCodeLogPO[] splitList = splitResourceReasonCodeLogByTime(midTime, preReasonCode,
						currentMember.getReasonCode(), currentMember);
				ResourceReasonCodeLogPO preObj = splitList[0]; // 前半段
				ResourceReasonCodeLogPO afterObj = splitList[1]; // 后半段

				if (endPointOffset == startScanPoint) { // 同一格
					// 将后半段数据再切割成前后两端
					Date afterMidTime = rrlEnd.getDateTime(); // 后半段切点时间
					ResourceReasonCodeLogPO[] afterSplitList = splitResourceReasonCodeLogByTime(afterMidTime,
							afterObj.getReasonCode(), "", afterObj);
					ResourceReasonCodeLogPO afterPreObj = afterSplitList[0]; // 后前半段
					ResourceReasonCodeLogPO afterAfterObj = afterSplitList[1]; // 后后半段
					afterPreObj.setMaintenanceOrder(maintenanceOrder);
					resultList.add(preObj);
					resultList.add(afterPreObj);
					// 重新拿去做原因识别
					addResourceTimeLog(resourceTimeLogPOs, startScanPoint + 1, afterAfterObj);
				} else if (endPointOffset - startScanPoint == 1) { // 如果是相邻，那么第二段肯定是P，所以只要切割成两段就好
					afterObj.setMaintenanceOrder(maintenanceOrder);
					resultList.add(preObj);
					resultList.add(afterObj);
				} else if (endPointOffset - startScanPoint > 1) {
					if ("P".equals(endRTL.getResourceState())) {
						Date startTime = rrlStart.getDateTime();
						Date endTime = endRTL.getStartDateTime();
						afterObj = createResourceReasonCodeLogPO(startTime, endTime, currentMember.getReasonCode(),
								currentMember);
						afterObj.setMaintenanceOrder(maintenanceOrder);
						resultList.add(preObj);
						resultList.add(afterObj);
						// 删除中间的RTL P不删除最后一段
						for (int i = startScanPoint + 1; i <= endPointOffset - 1; i++) {
							resourceTimeLogPOs.set(i, null);
						}
					} else if ("D".equals(endRTL.getResourceState())) {
						Date startTime = rrlStart.getDateTime();
						Date endTime = rrlEnd.getDateTime();
						afterObj = createResourceReasonCodeLogPO(startTime, endTime, currentMember.getReasonCode(),
								currentMember);
						afterObj.setMaintenanceOrder(maintenanceOrder);
						resultList.add(preObj);
						resultList.add(afterObj);
						// 删除中间的RTL D要删除最后一段
						for (int i = startScanPoint + 1; i <= endPointOffset; i++) {
							resourceTimeLogPOs.set(i, null);
						}
						// 重新拿去做原因识别
						ResourceReasonCodeLogPO lastObj = createResourceReasonCodeLogPO(endTime,
								endRTL.getEndDateTime(), "", endRTL);
						addResourceTimeLog(resourceTimeLogPOs, startScanPoint + 1, lastObj);
					}
				}
			}
		}
		if (activity.equalsIgnoreCase(RULE0004)) {
			resultList.remove(resultList.size() - 1);
			String reasonCode = new ReasonCodeBOHandle(reasonCodePriorityPO.getReasonCodeBo()).getReasonCode();
			List<ResourceReasonLogPO> rrlList = resourceReasonLogComponent.readRecordsByTimeRange(
					currentMember.getStartDateTime(), currentMember.getEndDateTime(),
					new ResrceBOHandle(currentMember.getResourceBo()).getResrce(), currentMember.getSite(), reasonCode);
			if (rrlList.size() > 0) {
				String reasonCodeRuleBo = reasonCodeRuleComponent
						.getReasonCodeRuleByPriorityBO(reasonCodePriorityPO.getHandle(), "RECOGNITION");
				List<ReasonCodeRuleParamPO> paramList = reasonCodeRuleParamComponent.getRuleParam(reasonCodeRuleBo);

				String preReasonCode = "";
				for (int i = 0; i < paramList.size(); i++) {
					if (paramList.get(i).getParamId().equalsIgnoreCase("PA01")) {
						preReasonCode = paramList.get(i).getParamValue();
					}
				}
				ResourceReasonCodeLogPO[] splitList = splitResourceReasonCodeLogByTime(rrlList.get(0).getDateTime(),
						preReasonCode, currentMember.getReasonCode(), currentMember);
				if (splitList.length > 1) {

					BigDecimal elapsed_time = splitList[0].getElapsedTime();
					int time_compare = elapsed_time.compareTo(new BigDecimal(60000));
					if (time_compare > 0) {
						resultList.add(splitList[0]);
						resultList.add(splitList[1]);
					} else {
						splitList[1].setStartDateTime(splitList[0].getStartDateTime());
						splitList[1].setElapsedTime(splitList[0].getElapsedTime().add(splitList[1].getElapsedTime()));
						resultList.add(splitList[1]);
					}

				} else {
					resultList.add(splitList[0]);
				}

			}
		} else if (activity.equalsIgnoreCase(RULE0009)) {
			ResourceReasonCodeLogPO resourceReasonCodeLogPOBefore = resultList.get(resultList.size() - 1);
			resultList.remove(resultList.size() - 1);
			// 获取人工点选的那个点
			String reasonCode = new ReasonCodeBOHandle(reasonCodePriorityPO.getReasonCodeBo()).getReasonCode();
			List<ResourceReasonLogPO> rrlList = resourceReasonLogComponent.readRecordsByTimeRange(
					currentMember.getStartDateTime(), currentMember.getEndDateTime(),
					new ResrceBOHandle(currentMember.getResourceBo()).getResrce(), currentMember.getSite(), reasonCode);
			/*if (rrlList.size() != 1) {
				return;
			}*/

			String reasonCodeRuleBo = reasonCodeRuleComponent
					.getReasonCodeRuleByPriorityBO(reasonCodePriorityPO.getHandle(), "RECOGNITION");
			List<ReasonCodeRuleParamPO> paramList = reasonCodeRuleParamComponent.getRuleParam(reasonCodeRuleBo);
			String preReasonCode = "";
			List<String> reasonCodeList = null;
			double preMinute = 0.0;
			double postMinute = 0.0;
			for (int i = 0; i < paramList.size(); i++) {
				if (paramList.get(i).getParamId().equalsIgnoreCase("PA01")) {
					preReasonCode = paramList.get(i).getParamValue();
				}
				if (paramList.get(i).getParamId().equalsIgnoreCase("PA02")) {
					reasonCodeList = Arrays.asList(paramList.get(i).getParamValue().split(","));
				}
				if (paramList.get(i).getParamId().equalsIgnoreCase("VA01")) {
					preMinute = Double.parseDouble(paramList.get(i).getParamValue());
				}
				if (paramList.get(i).getParamId().equalsIgnoreCase("VA02")) {
					postMinute = Double.parseDouble(paramList.get(i).getParamValue());
				}
			}
			for (int i = 0; i < reasonCodeList.size(); i++) {
				reasonCodeList.set(i,
						new ReasonCodeBOHandle(currentMember.getSite(), reasonCodeList.get(i)).getValue());
			}
			Date startTime = Utils.addSecondToDate(currentMember.getStartDateTime(), preMinute, false);
			Date endTime = Utils.addSecondToDate(currentMember.getStartDateTime(), postMinute, true);
			ResrceBOHandle resrceBOHandle = new ResrceBOHandle(currentMember.getResourceBo());
			try {
				List<ResourceAlertLogPO> resourceAlertLogPOs = resourceAlertLogComponent
						.readRecordsWithinTimeRange(startTime, endTime, resrceBOHandle.getResrce());
				for (String rs : reasonCodeList) {
					for (ResourceAlertLogPO resourceAlertLogPO : resourceAlertLogPOs) {
						if (reasonCodeAlertComponent.isReasonCodeMatchAlert(rs, resourceAlertLogPO.getDescription())) {
							ResourceReasonCodeLogPO finalResult = new ResourceReasonCodeLogPO();
							finalResult.setSite(currentMember.getSite());
							finalResult.setElapsedTime(new BigDecimal(Utils
									.getDiffTime(currentMember.getStartDateTime(), currentMember.getEndDateTime())));
							finalResult.setStartDateTime(currentMember.getStartDateTime());
							finalResult.setResourceBo(currentMember.getResourceBo());
							finalResult.setResourceState(currentMember.getResourceState());
							finalResult.setEndDateTime(currentMember.getEndDateTime());
							finalResult.setStrt(currentMember.getStrt());
							finalResult.setReasonCode(new ReasonCodeBOHandle(rs).getReasonCode());
							ResourceReasonCodeLogPO[] splitList = splitResourceReasonCodeLogByTime(
									rrlList.get(0).getDateTime(), preReasonCode, finalResult.getReasonCode(),
									finalResult);

							BigDecimal elapsed_time = splitList[0].getElapsedTime();
							int time_compare = elapsed_time.compareTo(new BigDecimal(60000));
							if (time_compare > 0) {
								resultList.add(splitList[0]);
								resultList.add(splitList[1]);
							} else {
								splitList[1].setStartDateTime(splitList[0].getStartDateTime());
								splitList[1].setElapsedTime(
										splitList[0].getElapsedTime().add(splitList[1].getElapsedTime()));
								resultList.add(splitList[1]);
							}
							return;
						}
					}
				}
			} catch (BusinessException e) {
				e.printStackTrace();
				resultList.add(resourceReasonCodeLogPOBefore);
				return;
			}
			ResourceReasonCodeLogPO[] splitList = splitResourceReasonCodeLogByTime(rrlList.get(0).getDateTime(),
					preReasonCode, currentMember.getReasonCode(), currentMember);
			if (splitList.length > 1) {

				BigDecimal elapsed_time = splitList[0].getElapsedTime();
				int time_compare = elapsed_time.compareTo(new BigDecimal(60000));
				if (time_compare > 0) {
					resultList.add(splitList[0]);
					resultList.add(splitList[1]);
				} else {
					splitList[1].setStartDateTime(splitList[0].getStartDateTime());
					splitList[1].setElapsedTime(splitList[0].getElapsedTime().add(splitList[1].getElapsedTime()));
					resultList.add(splitList[1]);
				}

			} else {
				resultList.add(splitList[0]);
			}
		}
	}

	// 多次点选内部切分方法
	public void addtionalReasonCodeForMorePress(List<ResourceReasonCodeLogPO> resultList,
			ResourceReasonCodeLogPO currentMember, ReasonCodePriorityPO reasonCodePriorityPO,
			LinkedList<ResourceTimeLogPO> resourceTimeLogPOs, int resourceTimeLogIndex) {
		resultList.remove(resultList.size() - 1);
		String reasonCode = new ReasonCodeBOHandle(reasonCodePriorityPO.getReasonCodeBo()).getReasonCode();
		List<ResourceReasonLogPO> rrlList = resourceReasonLogComponent.readRecordsByTimeRange(
				currentMember.getStartDateTime(), currentMember.getEndDateTime(),
				new ResrceBOHandle(currentMember.getResourceBo()).getResrce(), currentMember.getSite(), reasonCode);
		if (rrlList.size() > 0) {
			String reasonCodeRuleBo = reasonCodeRuleComponent
					.getReasonCodeRuleByPriorityBO(reasonCodePriorityPO.getHandle(), "RECOGNITION");
			List<ReasonCodeRuleParamPO> paramList = reasonCodeRuleParamComponent.getRuleParam(reasonCodeRuleBo);

			String preReasonCode = "";
			for (int i = 0; i < paramList.size(); i++) {
				if (paramList.get(i).getParamId().equalsIgnoreCase("PA01")) {
					preReasonCode = paramList.get(i).getParamValue();
				}
			}
			ResourceReasonCodeLogPO[] splitList = splitResourceReasonCodeLogByTime(rrlList.get(0).getDateTime(),
					preReasonCode, currentMember.getReasonCode(), currentMember);
			if (splitList[0].getElapsedTime().intValue() != 0) {
				resultList.add(splitList[0]);
			}
			resultList.add(splitList[1]);
		}
	}

	private void addResourceTimeLog(LinkedList<ResourceTimeLogPO> resourceTimeLogPOs, int index,
			ResourceReasonCodeLogPO rrcl) {
		ResourceTimeLogPO rtl = new ResourceTimeLogPO();
		rtl.setStartDateTime(rrcl.getStartDateTime());
		rtl.setEndDateTime(rrcl.getEndDateTime());
		rtl.setElapsedTime(new BigDecimal(Utils.getDiffTime(rrcl.getStartDateTime(), rrcl.getEndDateTime())));
		rtl.setResourceBo(rrcl.getResourceBo());
		rtl.setResourceState(rrcl.getResourceState());
		rtl.setSite(rrcl.getSite());
		rtl.setStrt(rrcl.getStrt());
		rtl.setHandle(new ResourceTimeLogBOHandle(rtl.getSite(), rtl.getResourceBo(), rtl.getStrt()).getValue());
		rtl.setCreatedDateTime(rrcl.getCreatedDateTime());
		rtl.setModifiedDateTime(rrcl.getModifiedDateTime());
		rtl.setModifiedUser(rrcl.getModifiedUser());
		resourceTimeLogPOs.add(index, rtl);
	}

	private ResourceReasonCodeLogPO createResourceReasonCodeLogPO(Date startTime, Date endTime, String reasonCode,
			ResourceReasonCodeLogPO originResourceReasonCodeLogPO) {
		ResourceReasonCodeLogPO rrcl = new ResourceReasonCodeLogPO();
		rrcl.setStartDateTime(startTime);
		rrcl.setEndDateTime(endTime);
		rrcl.setReasonCode(reasonCode);
		rrcl.setSite(originResourceReasonCodeLogPO.getSite());
		rrcl.setElapsedTime(new BigDecimal(Utils.getDiffTime(startTime, endTime)));
		rrcl.setResourceBo(originResourceReasonCodeLogPO.getResourceBo());
		rrcl.setResourceState(originResourceReasonCodeLogPO.getResourceState());
		rrcl.setStrt(Utils.datetimeMsToStr(startTime));
		return rrcl;
	}

	private ResourceReasonCodeLogPO createResourceReasonCodeLogPO(Date startTime, Date endTime, String reasonCode,
			ResourceTimeLogPO originResourceTimeLogPO) {
		ResourceReasonCodeLogPO rrcl = new ResourceReasonCodeLogPO();
		rrcl.setStartDateTime(startTime);
		rrcl.setEndDateTime(endTime);
		rrcl.setReasonCode(reasonCode);
		rrcl.setSite(originResourceTimeLogPO.getSite());
		rrcl.setElapsedTime(new BigDecimal(Utils.getDiffTime(startTime, endTime)));
		rrcl.setResourceBo(originResourceTimeLogPO.getResourceBo());
		rrcl.setResourceState(originResourceTimeLogPO.getResourceState());
		rrcl.setStrt(Utils.datetimeMsToStr(startTime));
		return rrcl;
	}

	// 从Start格开始，一直向后扫描，直到找到第二个Start，返回两个Start之间的所有End点，如果第一格没有Start，就返回null，
	// 否则，返回最后一次扫描到的end点所在格的offset
	private Integer scanEndsBetweenTwoStart(List<ResourceTimeLogPO> resourceTimeLogPOs, // 待检测的list
			int startScanPoint, // 开始检测的点
			int endScanPoint, // 结束检测点
			String reasonCode, // 检测点的 reasonCode
			List<ResourceReasonLogPO> rrlEndList, // 检测到的END点
			List<ResourceReasonLogPO> rrlStartList) { // 检测到的START点
		Integer lastEndOffset = null; // 指向最新的END节点
		boolean hasFindHeadStart = false;
		boolean firstRRL = true;
		for (int offset = startScanPoint; offset <= endScanPoint; offset++) {
			ResourceTimeLogPO resourceTimeLogPO = resourceTimeLogPOs.get(offset);
			if (null == resourceTimeLogPO)
				continue;
			// ResourceReasonCodeLogPO
			List<ResourceReasonLogPO> rrlStartAndEndPointList = resourceReasonLogComponent
					.readRecordsByStartTime(resourceTimeLogPO, reasonCode);
			for (ResourceReasonLogPO rrlStartAndEndPoint : rrlStartAndEndPointList) {
				if ("START".equals(rrlStartAndEndPoint.getActionCode())) {
					if (!hasFindHeadStart) { // 第一个START
						rrlStartList.add(rrlStartAndEndPoint);
						hasFindHeadStart = true;
					} else { // 第N个START
						if (null == lastEndOffset) { // 找到第二个START却没找到END
							rrlStartList.add(rrlStartAndEndPoint);
						} else { // 找到第二个START，中间有找到END了
							rrlStartList.add(rrlStartAndEndPoint);
							return lastEndOffset;
						}
					}
				} else if ("END".equals(rrlStartAndEndPoint.getActionCode())) {
					if (!hasFindHeadStart) { // 还没找到第一个START情况下，出现了END，忽略

					} else { // 已经有START出现过了，就记录END
						lastEndOffset = offset;
						rrlEndList.add(rrlStartAndEndPoint);
					}
				}
			}
			// 如果第一格
			if (firstRRL) {
				firstRRL = false;
				// 如果没找到第一格的Start
				if (!hasFindHeadStart) {
					return null;
				}
			}
		}
		return lastEndOffset;
	}

	public boolean checkFilter(String reasonCodePriorityBO, ResourceTimeLogPO resourceTimeLogPO, String resourceBo) {
		String reasonCodeRuleBo = reasonCodeRuleComponent.getReasonCodeRuleByPriorityBO(reasonCodePriorityBO, "FILTER");
		if (reasonCodeRuleBo.equalsIgnoreCase("")) {
			return true;
		} else {
			String resourceType = reasonCodeRuleParamComponent.getResourceTypeByResourceBo(resourceBo);
			if (StringUtils.isBlank(resourceType)) {
				return false;
			}
			List<ReasonCodeRuleParamPO> paramList = reasonCodeRuleParamComponent.getRuleParam(reasonCodeRuleBo,
					resourceType);
			if (paramList.size() == 2) {
				if (ParamUtils.checkManualF(paramList.get(0).getParamValue(), paramList.get(1).getParamValue(),
						resourceTimeLogPO.getStartDateTime(), resourceTimeLogPO.getEndDateTime())) {
					return true;
				}
			}
			if (paramList.size() == 4) {
				if (ParamUtils.checkManualF(paramList.get(0).getParamValue(), paramList.get(2).getParamValue(),
						resourceTimeLogPO.getStartDateTime(), resourceTimeLogPO.getEndDateTime())
						&& ParamUtils.checkManualF(paramList.get(1).getParamValue(), paramList.get(3).getParamValue(),
								resourceTimeLogPO.getStartDateTime(), resourceTimeLogPO.getEndDateTime())) {
					return true;
				}
			}
			if (paramList.size() == 0) {
				return true;
			}
			return false;
		}
	}

	private ResourceReasonCodeLogPO[] splitResourceReasonCodeLogByTime(Date splitTime, String preReasonCode,
			String afterReasonCode, ResourceReasonCodeLogPO originResourceReasonCodeLogPO) {
		ResourceReasonCodeLogPO preLog = new ResourceReasonCodeLogPO();
		preLog.setSite(originResourceReasonCodeLogPO.getSite());
		preLog.setElapsedTime(
				new BigDecimal(Utils.getDiffTime(originResourceReasonCodeLogPO.getStartDateTime(), splitTime)));
		preLog.setStartDateTime(originResourceReasonCodeLogPO.getStartDateTime());
		preLog.setResourceBo(originResourceReasonCodeLogPO.getResourceBo());
		preLog.setResourceState(originResourceReasonCodeLogPO.getResourceState());
		preLog.setEndDateTime(splitTime);
		preLog.setReasonCode(preReasonCode);
		preLog.setStrt(originResourceReasonCodeLogPO.getStrt());

		ResourceReasonCodeLogPO postLog = new ResourceReasonCodeLogPO();
		postLog.setSite(originResourceReasonCodeLogPO.getSite());
		postLog.setElapsedTime(
				new BigDecimal(Utils.getDiffTime(splitTime, originResourceReasonCodeLogPO.getEndDateTime())));
		postLog.setStartDateTime(splitTime);
		postLog.setResourceBo(originResourceReasonCodeLogPO.getResourceBo());
		postLog.setResourceState(originResourceReasonCodeLogPO.getResourceState());
		postLog.setEndDateTime(originResourceReasonCodeLogPO.getEndDateTime());
		postLog.setReasonCode(afterReasonCode);
		postLog.setStrt(Utils.datetimeMsToStr(splitTime));

		return new ResourceReasonCodeLogPO[] { preLog, postLog };
	}

}
