package mhp.oee.job.reasoncodereconizejob;

import mhp.oee.common.exception.BusinessException;
import mhp.oee.common.exception.ErrorCodeEnum;
import mhp.oee.common.exception.ExistingRecordException;
import mhp.oee.common.exception.ServerNodeException;
import mhp.oee.common.handle.ReasonCodeAlertBOHandle;
import mhp.oee.common.handle.ReasonCodeBOHandle;
import mhp.oee.common.handle.ResrceBOHandle;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.job.JobLogComponent;
import mhp.oee.component.plant.*;
import mhp.oee.component.production.ResourceAlertLogComponent;
import mhp.oee.component.production.ResourceReasonCodeLogComponent;
import mhp.oee.component.production.ResourceTimeLogComponent;
import mhp.oee.config.JobDispatchConfig;
import mhp.oee.dao.gen.ActivityParamPOMapper;
import mhp.oee.dao.gen.ReasonCodeAlertPOMapper;
import mhp.oee.dao.gen.ResourceAlertLogPOMapper;
import mhp.oee.dao.gen.ResourcePlcInfoPOMapper;
import mhp.oee.dao.gen.ResourceReasonLogPOMapper;
import mhp.oee.po.extend.ReasonCodePriorityRulePO;
import mhp.oee.job.quartz.quartzutils.ScheduleJob;
import mhp.oee.job.quartz.quartzutils.SpringContextUtil;
import mhp.oee.po.gen.*;
import mhp.oee.po.gen.ResourceReasonLogPOExample.Criteria;
import mhp.oee.utils.AsyncService;
import mhp.oee.utils.JdbcHelper;
import mhp.oee.utils.Utils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

@Service
public class RecognizeReasonCodeJob implements Job {

	@InjectableLogger
	private static Logger logger;

	ResourceTimeLogComponent resourceTimeLogComponent;
	ScheduledDownPlanComponent scheduledDownPlanComponent;
	ReasonCodePriorityComponent reasonCodePriorityComponent;
	ResourceReasonCodeLogComponent resourceReasonCodeLogComponent;
	ResourceComponent resourceComponent;
	ReasonCodeRuleComponent reasonCodeRuleComponent;
	ReasonCodeResStateComponent reasonCodeResStateComponent;
	DetermineReasonCode determineReasonCode;
	WorkCenterShiftComponent workCenterShiftComponent;
	ResourceAlertLogComponent resourceAlertLogComponent;
	ReasonCodeAlertComponent reasonCodeAlertComponent;
	ReasonCodeRuleParamComponent reasonCodeRuleParamComponent;
	JobLogComponent jobLogComponent;
	JobService jobService;
	AsyncService jobThreadPool;
	ResourceReasonLogPOMapper resourceReasonLogPOMapper;
	ReasonCodeAlertPOMapper reasonCodeAlertPOMapper;
	ResourceAlertLogPOMapper resourceAlertLogPOMapper;
	ResourcePlcInfoPOMapper resourcePlcInfoPOMapper;
	ActivityParamPOMapper activityParamPOMapper;
	JobDispatchConfig jobDispatchConfig;

	private static SimpleDateFormat sdfForDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static SimpleDateFormat stringForDate = new SimpleDateFormat("yyyy-MM-dd");

	private static final String JOB_NAME = "JOB_REASON_CODE_RECOGNITION";

	private int fetchSize;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		this.setBeans();

		ScheduleJob job = (ScheduleJob)context.getMergedJobDataMap().get("scheduleJob");
		String planedTime = job.getPlanedTime();

		System.out.println("=========");
		System.out.println(planedTime);
		System.out.println("=========");


		try {
			// lock and log start
			logger.info("开始争抢锁：" + JOB_NAME);
			Date startDateTime = new Date(); // 实际开始时间
			System.out.println("job开始："+sdfForDateTime.format(startDateTime));


			String startDate = stringForDate.format(startDateTime);
			String sPlannedDateTime = startDate + " " + planedTime;
			Date plannedDateTime = sdfForDateTime.parse(sPlannedDateTime); // 计划开始时间


			String serverNode = Utils.getCurrentServerNode();
			if(Integer.parseInt(serverNode) > this.jobDispatchConfig.getServerNodes()) {
				throw new ServerNodeException();
			}

			String handle = jobLogComponent.jobStartLog(JOB_NAME, plannedDateTime, startDateTime, serverNode);
			while(handle == null) {
				serverNode = Utils.getNextServerNode();
				if(Integer.parseInt(serverNode) > this.jobDispatchConfig.getServerNodes()) {
					throw new ServerNodeException();
				}
				handle = jobLogComponent.jobStartLog(JOB_NAME, plannedDateTime, startDateTime, serverNode);
			}
			logger.info("当前实例运行：" + JOB_NAME + " serverNode: " + serverNode + " " + Utils.datetimeMsToStr(plannedDateTime));

			// execute
			logger.info("开始执行原因识别作业");


			int time_range =24;

			int sqlSize =100000;

			int batchSize =5000;

			int threadTimes = 1;

			jobThreadPool.setPoolSize(threadTimes);
			logger.info("当前job线程数为："+jobThreadPool.getPoolSize());

//
//			Date before = sdfForDateTime.parse("2017-05-03 09:00:00");
//			Date now = sdfForDateTime.parse("2017-05-04 09:00:00");
//			buildResourceReasonCodePO(before, now,sqlSize,batchSize, serverNode);

			Date now = new Date();
			buildResourceReasonCodePO(Utils.addMinuteToDate(now, time_range * 60, false), now,sqlSize,batchSize, serverNode);
			logger.info("结束执行原因识别作业");

			// log end
			jobLogComponent.jobEndLog(handle, fetchSize);
		} catch (ParseException e) {
			logger.info("JOB执行错误");
		} catch (ServerNodeException e) {
			logger.info("JOB执行错误");
			logger.error("ServerNode was null or empty");
		}
	}



	public ResourceReasonCodeLogPO buildResourceReasonCodePO(Date beginDate, Date endDate,int sqlSize,int batchSize, String serverNodeIndex) {
		List<Future<RecognizeData>> futureList = new LinkedList<Future<RecognizeData>>();

		ResourcePlcInfoPOExample resourcePlcInfoPOExample = new ResourcePlcInfoPOExample();
		// need to be update used priority

		fetchSize = getCurrentServerNodeFetchSize(Integer.parseInt(serverNodeIndex));

		resourcePlcInfoPOExample.setLimit(fetchSize);
		resourcePlcInfoPOExample.setOffset(Utils.getOffset(fetchSize, Integer.parseInt(serverNodeIndex)));

		List<ResourcePlcInfoPO> resourceList = resourcePlcInfoPOMapper.selectByExample(resourcePlcInfoPOExample);
		System.out.println("取得resource PLC 数量为             ====>>>>>>>      " + resourceList.size());
		System.out.println(resourceList);



		for (final ResourcePlcInfoPO resourcePlcPO : resourceList) {
			//if(!resourcePlcPO.getResourceBo().equals("ResourceBO:1030,AWIN0001"))continue;
			// 寻找开始时间右边最近的reason_code_time_log的开始时间作为识别的开始时间
			final Date recognizeStartDateTime = resourceReasonCodeLogComponent.findRecognizeStartTime(beginDate,
					resourcePlcPO.getResourceBo());
			final Date recognizeEndDateTime = endDate;
			// 删除交集处的识别结果
			jobService.deleteResourceReasonCodeLogList(recognizeStartDateTime, recognizeEndDateTime,
					resourcePlcPO.getResourceBo());
			// 将要执行的原因识别丢入固定线程数的线程池，提高原因识别速度
			Future<RecognizeData> future = jobThreadPool.submitTask(new Callable<RecognizeData>() {
				@Override
				public RecognizeData call() throws Exception {
					try {
						// 待insert的list
						LinkedList<ResourceReasonCodeLogPO> resourceReasnCodeLogPOList = new LinkedList<ResourceReasonCodeLogPO>();
						List<ReasonCodePriorityPO> notUsedReasonCodePriorityPOs = new LinkedList<ReasonCodePriorityPO>();

						// query the RTL and cut the border time
						List<ResourceTimeLogPO> resourceTimeLogPOs = resourceTimeLogComponent
								.readRecordsStartTimeWithinTimeRang(recognizeStartDateTime, recognizeEndDateTime,
										resourcePlcPO.getResourceBo());

						if (resourceTimeLogPOs != null) {
							// 执行原因代码识别
							setResourceReasonCodeLogByRTL(recognizeEndDateTime, resourceReasnCodeLogPOList,
									new LinkedList<ResourceTimeLogPO>(resourceTimeLogPOs),
									notUsedReasonCodePriorityPOs);
						}

						return new RecognizeData(resourceReasnCodeLogPOList, notUsedReasonCodePriorityPOs);
					} catch (Exception e) {
						e.printStackTrace();
						logger.error(e.getMessage());
						return null;
					}
				}
			});
			// 收集future
			futureList.add(future);
		}

		// 从future中获取识别完的原因代码，然后进行数据库操作
		try {
			double total = (double) futureList.size();
			double index = 0D;
			logger.info("progress: 0.00%");
			Map<String,String> sql_map= new HashMap<String,String>();
			for (Future<RecognizeData> future : futureList) {
				// 统计执行百分比
				index++;
				double progress = 100 * index / total;
				progress = new BigDecimal(progress).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
				logger.info("progress: " + progress + "%");
				// 按顺序阻塞获取识别完的原因代码
				RecognizeData data = future.get();
				if (null == data)
					continue;
				// inset or update data
				// 插入新的识别结果
				jobService.insertResourceReasonCodeLogList(JOB_NAME, data.getResourceReasnCodeLogPOList(),sql_map);
				// 更新优先级used字段
				jobService.updateReasonCodePriorityList(JOB_NAME, data.getNotUsedReasonCodePriorityPOs());
				if(sql_map.size()>sqlSize){
					JdbcHelper.batchInsert(sql_map.values(),batchSize);
					sql_map.clear();
				}
			}
			JdbcHelper.batchInsert(sql_map.values(),batchSize);
			System.out.println("数据插入结束："+sdfForDateTime.format(new Date()));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return null;
	}

	private int getCurrentServerNodeFetchSize(int serverNodeIndex) {
		ResourcePlcInfoPOExample resourcePlcInfoPOExample = new ResourcePlcInfoPOExample();
		int total = resourcePlcInfoPOMapper.countByExample(resourcePlcInfoPOExample);

		int serverNode = this.jobDispatchConfig.getServerNodes();

		int everageNumber = total/serverNode;
		System.out.println(everageNumber);

		List<Integer> serverNodeFetchSizeList = new ArrayList<Integer>();

		for(int i = 1; i < serverNode + 1; i++){
			if(i == serverNode) {
				serverNodeFetchSizeList.add(total - ((serverNode - 1)*everageNumber));
			} else {
				serverNodeFetchSizeList.add(everageNumber);
			}
		}
		return serverNodeFetchSizeList.get(serverNodeIndex - 1);
	}

	private void setResourceReasonCodeLogByRTL(Date now, List<ResourceReasonCodeLogPO> resourceReasnCodeLogPOList,
											   LinkedList<ResourceTimeLogPO> resourceTimeLogPOs, List<ReasonCodePriorityPO> notUsedReasonCodePriorityPOs) {
		int rtlSize = resourceTimeLogPOs.size();
		/*String site="";
		if(rtlSize>0){
			site=resourceTimeLogPOs.get(0).getSite();
		}
		List<ReasonCodePriorityPO> reasonCodePriorityPOs=null;
		try {
			reasonCodePriorityPOs = reasonCodePriorityComponent
					.readRecordsByTimeRange(site, now);
		} catch (BusinessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		for (int resourceTimeLogIndex = 0; resourceTimeLogIndex < rtlSize; resourceTimeLogIndex++) {
			ResourceTimeLogPO resourceTimeLogPO = resourceTimeLogPOs.get(resourceTimeLogIndex);
			if (null == resourceTimeLogPO)
				continue; // 在循环内通过设置null来表示删除元素

			ResourceReasonCodeLogPO resourceReasonCodeLogPO = new ResourceReasonCodeLogPO();
			resourceReasonCodeLogPO.setSite(resourceTimeLogPO.getSite());
			resourceReasonCodeLogPO.setElapsedTime(resourceTimeLogPO.getElapsedTime());
//			resourceReasonCodeLogPO.setElapsedTime(new BigDecimal(
//					Utils.getDiffTime(resourceTimeLogPO.getStartDateTime(), resourceTimeLogPO.getEndDateTime())));
			resourceReasonCodeLogPO.setStartDateTime(resourceTimeLogPO.getStartDateTime());
			resourceReasonCodeLogPO.setResourceBo(resourceTimeLogPO.getResourceBo());
			resourceReasonCodeLogPO.setResourceState(resourceTimeLogPO.getResourceState());
			resourceReasonCodeLogPO.setEndDateTime(resourceTimeLogPO.getEndDateTime());
			resourceReasonCodeLogPO.setStrt(resourceTimeLogPO.getStrt());
			resourceReasonCodeLogPO.setModifiedUser(JOB_NAME);
			try {
				String reasonCode = "";
				// TODO：AA modify read by site
//				List<ReasonCodePriorityPO> reasonCodePriorityPOs = reasonCodePriorityComponent
//						.readRecordsByTimeRange(resourceTimeLogPO.getSite(), now);

				List<ReasonCodePriorityRulePO> reasonCodePriorityRulePOs = reasonCodePriorityComponent.readReasonCodePriorityRule(resourceTimeLogPO.getSite(),now,null);

				List<HitData> hitList = new LinkedList<HitData>();
				Boolean hasHit = false;
				String hitPriority = "";
				String hitSubPriority = "";
				for (ReasonCodePriorityRulePO reasonCodePriorityRulePO : reasonCodePriorityRulePOs) {
					ReasonCodePriorityPO reasonCodePriorityPO = reasonCodePriorityRulePO.getReasonCodePriorityPO();
					ReasonCodeRulePO reasonCodeRulePO = reasonCodePriorityRulePO.getReasonCodeRulePO();

					if ("false".equals(reasonCodePriorityPO.getUsed())) {
						notUsedReasonCodePriorityPOs.add(reasonCodePriorityPO);
					}
					// if has found a reasoncodePriority
					// and current reasoncodepriority
					// is less than the found one, break the circulation
					if (hasHit && (!reasonCodePriorityPO.getPriority().equals(hitPriority)
							|| !reasonCodePriorityPO.getSubPriority().equals(hitSubPriority))) {
						break;
					}
					//RULE000*
//					String activityRule = reasonCodeRuleComponent
//							.getActivityNameByReasonCodePriorityBO(reasonCodePriorityPO.getHandle(), "RECOGNITION");
					String activityRule = reasonCodeRulePO != null ? reasonCodeRulePO.getActivity() : "";


					// // TODO: BB is hit by different rule
					// modify return for RULE0008
					boolean isHitByActivity = determineReasonCode.checkReasonCodeByActivity(resourceTimeLogPOs,
							resourceTimeLogIndex, resourceTimeLogPO, reasonCodePriorityPO, activityRule,
							resourceTimeLogPO.getResourceBo());
					if (isHitByActivity) {
						hitList.add(new HitData(isHitByActivity, activityRule, reasonCodePriorityPO));
						hasHit = true;
						hitPriority = reasonCodePriorityPO.getPriority();
						hitSubPriority = reasonCodePriorityPO.getSubPriority();
					}
				}
				// hit more than 1 reasonCodePriority with the same priority
				if (hitList.size() > 1) {
					if (allRuleEqualTo(hitList, "RULE0008")) { // hit multi
						// reasonCodePriority
						// and activity
						// is RULE0008
						// The following is the disgusting patch for the
						// RULE0008.
						// This rule use the additional time as priority
						ArrayList<ResourceAlertLogPO> hitAlertList = new ArrayList<ResourceAlertLogPO>();
						ArrayList<String> hitReasonAlertList = new ArrayList<String>();
						for (int i = 0; i < hitList.size(); i++) {
							ReasonCodePriorityPO priority = hitList.get(i).getReasonCodePriorityPO();
							if (!reasonCodeAlertComponent.isReasonCodeExisting(priority.getReasonCodeBo())) {
								throw new BusinessException(ErrorCodeEnum.MULTIPLE_REASON_CODE,
										"find more than one reason code for rtl entry:"
												+ resourceTimeLogPO.getHandle());
							}
							String reasonCodeRuleBo = reasonCodeRuleComponent.getReasonCodeRuleByPriorityBOAndActivity(
									priority.getHandle(), "RECOGNITION", "RULE0008");
							List<ReasonCodeRuleParamPO> paramList = reasonCodeRuleParamComponent
									.getRuleParam(reasonCodeRuleBo);
							double preMinute = 0.0;
							double postMinute = 0.0;
							for (int j = 0; j < paramList.size(); j++) {
								if (paramList.get(j).getParamId().equalsIgnoreCase("VA01")) {
									preMinute = Double.parseDouble(paramList.get(j).getParamValue());
								}
								if (paramList.get(j).getParamId().equalsIgnoreCase("VA02")) {
									postMinute = Double.parseDouble(paramList.get(j).getParamValue());
								}
							}
							Date startTime = Utils.addSecondToDate(resourceTimeLogPO.getStartDateTime(), preMinute,
									false);
							Date endTime = Utils.addSecondToDate(resourceTimeLogPO.getEndDateTime(), postMinute, true);
							ResrceBOHandle resrceBOHandle = new ResrceBOHandle(resourceTimeLogPO.getResourceBo());
							try {
								List<ResourceAlertLogPO> resourceAlertLogPOs = resourceAlertLogComponent
										.readRecordsWithinTimeRange(startTime, endTime, resrceBOHandle.getResrce());
								for (ResourceAlertLogPO resourceAlertLogPO : resourceAlertLogPOs) {
									if (reasonCodeAlertComponent.isReasonCodeMatchAlert(priority.getReasonCodeBo(),
											resourceAlertLogPO.getDescription())) {
										hitAlertList.add(resourceAlertLogPO);
										hitReasonAlertList.add(priority.getReasonCodeBo());
										break;
									}
								}
							} catch (BusinessException e) {
								logger.error(e.getErrorJson());
							}
						}
						// find the earlest entry
						if (hitReasonAlertList.size() > 0) {
							String earliestReasonCode = hitReasonAlertList.get(0);
							ResourceAlertLogPO earliestAlertPO = hitAlertList.get(0);
							for (int i = 0; i < hitReasonAlertList.size(); i++) {
								if (Utils.compareDate(hitAlertList.get(i).getDateTime(),
										earliestAlertPO.getDateTime())) {
									earliestReasonCode = hitReasonAlertList.get(i);
								}
							}
							resourceReasonCodeLogPO
									.setReasonCode(new ReasonCodeBOHandle(earliestReasonCode).getReasonCode());
							resourceReasnCodeLogPOList.add(resourceReasonCodeLogPO);
						}
					} else if (allRuleEqualToTwo(hitList, "RULE0004","RULE0009")) {
						try{//内部用了太多get(index)方法，为了安全try一下
							Date sTime = resourceTimeLogPO.getStartDateTime();
							Date eTime = resourceTimeLogPO.getEndDateTime();
							ResrceBOHandle rBOHandle = new ResrceBOHandle(resourceTimeLogPO.getResourceBo());
							ResourceReasonLogPOExample rexample = new ResourceReasonLogPOExample();
							Criteria cra = rexample.createCriteria();
							cra.andResrceEqualTo(rBOHandle.getResrce()).andDateTimeBetween(sTime, eTime)
									.andSiteEqualTo(resourceTimeLogPO.getSite());
							rexample.setOrderByClause("DATE_TIME");
							List<ResourceReasonLogPO> pointTotal = resourceReasonLogPOMapper.selectByExample(rexample);

							List<Date> dateList = new LinkedList<Date>();
							dateList.add(sTime);
							for (int i = 0; i < pointTotal.size(); i++) {
								dateList.add(pointTotal.get(i).getDateTime());
							}
							dateList.add(eTime);

							BigDecimal prd_elapsedtime=new BigDecimal(Utils.getDiffTime(dateList.get(0), dateList.get(1)));
							int compare_to_time = prd_elapsedtime.compareTo(new BigDecimal(60000));
							ResourceReasonCodeLogPO rrclpoFir=null;
							if(compare_to_time>0){//PRD处理时间大于60秒
								rrclpoFir = (ResourceReasonCodeLogPO) resourceReasonCodeLogPO.clone();
								rrclpoFir.setEndDateTime(dateList.get(1));
								rrclpoFir.setElapsedTime(prd_elapsedtime);
								rrclpoFir.setReasonCode("NP08");
							}
							ResourceReasonCodeLogPO rrclpo = (ResourceReasonCodeLogPO) resourceReasonCodeLogPO.clone();
//							ReasonCodePriorityPO priority = hitList.get(0).getReasonCodePriorityPO();
							reasonCode = pointTotal.get(0).getReasonCode();
//							String reasonCodeFlag;
//							for (HitData hitData : hitList) {
//								reasonCodeFlag = new ReasonCodeBOHandle(hitData.getReasonCodePriorityPO().getReasonCodeBo())
//										.getReasonCode();
//								if (reasonCode.equals(reasonCodeFlag)){
//									priority = hitData.getReasonCodePriorityPO();
//									break;
//								}
//							}
							rrclpo.setReasonCode(reasonCode);
//							resourceReasnCodeLogPOList.add(rrclpo);
//							determineReasonCode.addtionalReasonCodeForMorePress(resourceReasnCodeLogPOList, rrclpo,
//									priority, resourceTimeLogPOs, resourceTimeLogIndex);
//							ResourceReasonCodeLogPO resReasonLogLast = resourceReasnCodeLogPOList.get(resourceReasnCodeLogPOList.size() - 1);
//
//							resReasonLogLast.setReasonCode(reasonCode);
//
//							resReasonLogLast.setEndDateTime(dateList.get(2));
//							resReasonLogLast.setElapsedTime(new BigDecimal(Utils.getDiffTime(dateList.get(1), dateList.get(2))));
//							resReasonLogLast = checkAdditive(resReasonLogLast,hitList);
							if(compare_to_time>0){
								rrclpo.setStartDateTime(dateList.get(1));
								rrclpo.setElapsedTime(new BigDecimal(Utils.getDiffTime(dateList.get(1), dateList.get(2))));
							}else{
								rrclpo.setStartDateTime(dateList.get(0));
								rrclpo.setElapsedTime(new BigDecimal(Utils.getDiffTime(dateList.get(0), dateList.get(2))));
							}

							rrclpo.setEndDateTime(dateList.get(2));
							rrclpo.setStrt(Utils.datetimeMsToStr(dateList.get(1)));
							if(rrclpoFir!=null&&!rrclpoFir.getStrt().equals(rrclpo.getStrt())){
								resourceReasnCodeLogPOList.add(rrclpoFir);
							}

							rrclpo = checkAdditive(rrclpo,hitList);
							resourceReasnCodeLogPOList.add(rrclpo);
							ResourceReasonCodeLogPO rrclpoOther = null;
							for (int j = 0; j < pointTotal.size() - 1; j++) {
								rrclpoOther = (ResourceReasonCodeLogPO) resourceReasonCodeLogPO.clone();
								reasonCode = pointTotal.get(j + 1).getReasonCode();
								rrclpoOther.setReasonCode(reasonCode);
								rrclpoOther.setStartDateTime(dateList.get(j + 2));
								rrclpoOther.setEndDateTime(dateList.get(j + 3));
								rrclpoOther.setElapsedTime(
										new BigDecimal(Utils.getDiffTime(dateList.get(j + 2), dateList.get(j + 3))));
								rrclpoOther.setStrt(Utils.datetimeMsToStr(dateList.get(j + 2)));
								resourceReasnCodeLogPOList.add(checkAdditive(rrclpoOther,hitList));
//								resourceReasnCodeLogPOList.add(rrclpoOther);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					} else { // hit multi reasonCodePriority and activity is not
						// RULE0008
						reasonCode = "";
						String comment = allReasonCodeToString(hitList);
						resourceReasonCodeLogPO.setReasonCode(reasonCode);
						resourceReasonCodeLogPO.setComment(comment);
						resourceReasnCodeLogPOList.add(resourceReasonCodeLogPO);
					}
				}
				// hit no reasonCodePriority
				if (hitList.size() == 0) {
					// can not find reasonCodePriority for the reasonCode
					reasonCode = "";
					resourceReasonCodeLogPO.setReasonCode(reasonCode);
					resourceReasnCodeLogPOList.add(resourceReasonCodeLogPO);
				}
				// hit the exact one reasonCodePriority
				if (hitList.size() == 1) {
					ReasonCodePriorityPO priority = hitList.get(0).getReasonCodePriorityPO();
					reasonCode = new ReasonCodeBOHandle(priority.getReasonCodeBo()).getReasonCode();
					resourceReasonCodeLogPO.setReasonCode(reasonCode);
					resourceReasnCodeLogPOList.add(resourceReasonCodeLogPO);
					// TODO: CC split time zone
					determineReasonCode.addtionalReasonCode(resourceReasnCodeLogPOList, // 结果集
							resourceReasonCodeLogPO, // 当前要放入的结果
							priority, // 当前要放入的结果对应的priority
							resourceTimeLogPOs, resourceTimeLogIndex);
				}
			} catch (BusinessException e) {
				e.printStackTrace();
				logger.error(e.getErrorJson());
			} finally {
				// 重新调整list的size
				rtlSize = resourceTimeLogPOs.size();
			}
		}
	}
	public ResourceReasonCodeLogPO checkAdditive(ResourceReasonCodeLogPO resourceReasonCodeLogPO,List<HitData> hitList){
		//目前需要对点选现场岗位处理和未知原因进行处理
		String reasonCode = resourceReasonCodeLogPO.getReasonCode();
		ReasonCodePriorityPO priority = null;
//		String reasonCodeFlag;
//		for (HitData hitData : hitList) {
//			reasonCodeFlag = new ReasonCodeBOHandle(hitData.getReasonCodePriorityPO().getReasonCodeBo())
//					.getReasonCode();
//			if (reasonCode.equals(reasonCodeFlag)){
//				priority = hitData.getReasonCodePriorityPO();
//				break;
//			}
//		}
		List<ReasonCodePriorityPO> reasonCodePriorityPOs = null;
		try {
			reasonCodePriorityPOs = reasonCodePriorityComponent
					.readRecordsByTimeRange(resourceReasonCodeLogPO.getSite(), new Date());
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		for(ReasonCodePriorityPO reasonCodePriorityPO:reasonCodePriorityPOs){
			if(new ReasonCodeBOHandle(reasonCodePriorityPO.getReasonCodeBo()).getReasonCode().equals(reasonCode)&&
					reasonCodePriorityPO.getPriority().equals("B")&&reasonCodePriorityPO.getSubPriority().equals("2")){
				priority = reasonCodePriorityPO;
			}
		}

		if("NP05".equals(reasonCode)||"NP11".equals(reasonCode)){
			if(resourceReasonCodeLogPO.getElapsedTime().intValue()>600000){
				resourceReasonCodeLogPO.setReasonCode("SD10");
			}else{
				String reasonCodeRuleBo = reasonCodeRuleComponent
						.getReasonCodeRuleByPriorityBO(priority==null?"":priority.getHandle(), "RECOGNITION");
				List<ReasonCodeRuleParamPO> paramList = reasonCodeRuleParamComponent.getRuleParam(reasonCodeRuleBo);
				double preMinute = 0.0;
				double postMinute = 0.0;
				for (int i = 0; i < paramList.size(); i++) {
					if (paramList.get(i).getParamId().equalsIgnoreCase("VA01")) {
						preMinute = Double.parseDouble(paramList.get(i).getParamValue());
					}
					if (paramList.get(i).getParamId().equalsIgnoreCase("VA02")) {
						postMinute = Double.parseDouble(paramList.get(i).getParamValue());
					}
				}
				Date startTime = Utils.addSecondToDate(resourceReasonCodeLogPO.getStartDateTime(), preMinute, false);
				Date endTime = Utils.addSecondToDate(resourceReasonCodeLogPO.getStartDateTime(), postMinute, true);
				ResrceBOHandle resrceBOHandle = new ResrceBOHandle(resourceReasonCodeLogPO.getResourceBo());
				ResourceAlertLogPOExample ralExample = new ResourceAlertLogPOExample();
				ralExample.createCriteria().andResrceEqualTo(resrceBOHandle.getResrce()).andDateTimeBetween(startTime, endTime);
				ralExample.setOrderByClause("DATE_TIME");
				List<ResourceAlertLogPO> resourceAlertLogPOs = resourceAlertLogPOMapper.selectByExample(ralExample);

				for (ResourceAlertLogPO resourceAlertLogPO : resourceAlertLogPOs) {
					String resrceType = resourceAlertLogPO.getResrce().substring(0, 4);
					ReasonCodeAlertPOExample example = new ReasonCodeAlertPOExample();
					example.createCriteria().andDescriptionEqualTo(resourceAlertLogPO.getDescription())
							.andResourceTypeBoLike("%"+resrceType);
					List<ReasonCodeAlertPO> reasonCodeAlertPOList = reasonCodeAlertPOMapper.selectByExample(example);
					if (!reasonCodeAlertPOList.isEmpty()) {
						ReasonCodeAlertBOHandle reasonCodeAlertBOHandle = new ReasonCodeAlertBOHandle(reasonCodeAlertPOList.get(0).getHandle());
						resourceReasonCodeLogPO.setReasonCode(reasonCodeAlertBOHandle.getReasonCode());
						break;
					}
				}
			}
		}
		return resourceReasonCodeLogPO;
	}


	private void processBoundaryTime(List<ResourceTimeLogPO> rTLList, Date beginDate, Date finishDate) {
		if (rTLList.size() > 0) {
			if (Utils.compareDate(rTLList.get(0).getStartDateTime(), beginDate)) {
				rTLList.get(0).setStartDateTime(beginDate);
			}
			if (Utils.compareDate(finishDate, rTLList.get(rTLList.size() - 1).getEndDateTime())
					|| rTLList.get(rTLList.size() - 1).getEndDateTime() == null) {
				rTLList.get(rTLList.size() - 1).setEndDateTime(finishDate);
			}
		}
	}

	private boolean allRuleEqualTo(List<HitData> hitList, String rule) {
		for (HitData hitData : hitList) {
			if (!hitData.getActivityRule().equalsIgnoreCase(rule)) {
				return false;
			}
		}
		return true;
	}
	private boolean allRuleEqualToTwo(List<HitData> hitList, String rule ,String ruleAnother) {
		for (HitData hitData : hitList) {
			if (!(hitData.getActivityRule().equalsIgnoreCase(rule)||hitData.getActivityRule().equalsIgnoreCase(ruleAnother))) {
				return false;
			}
		}
		return true;
	}

	private String allReasonCodeToString(List<HitData> hitList) {
		StringBuilder sb = new StringBuilder("");
		for (HitData hitData : hitList) {
			String reasonCode = new ReasonCodeBOHandle(hitData.getReasonCodePriorityPO().getReasonCodeBo())
					.getReasonCode();
			sb.append(',').append(reasonCode);
		}
		String str = sb.toString();
		return str.startsWith(",") ? str.substring(1) : str;
	}


	private class RecognizeData {
		private LinkedList<ResourceReasonCodeLogPO> resourceReasnCodeLogPOList = null;
		private List<ReasonCodePriorityPO> notUsedReasonCodePriorityPOs = null;

		public RecognizeData(LinkedList<ResourceReasonCodeLogPO> resourceReasnCodeLogPOList,
							 List<ReasonCodePriorityPO> notUsedReasonCodePriorityPOs) {
			this.resourceReasnCodeLogPOList = resourceReasnCodeLogPOList;
			this.notUsedReasonCodePriorityPOs = notUsedReasonCodePriorityPOs;
		}

		public LinkedList<ResourceReasonCodeLogPO> getResourceReasnCodeLogPOList() {
			return resourceReasnCodeLogPOList;
		}

		public void setResourceReasnCodeLogPOList(LinkedList<ResourceReasonCodeLogPO> resourceReasnCodeLogPOList) {
			this.resourceReasnCodeLogPOList = resourceReasnCodeLogPOList;
		}

		public List<ReasonCodePriorityPO> getNotUsedReasonCodePriorityPOs() {
			return notUsedReasonCodePriorityPOs;
		}

		public void setNotUsedReasonCodePriorityPOs(List<ReasonCodePriorityPO> notUsedReasonCodePriorityPOs) {
			this.notUsedReasonCodePriorityPOs = notUsedReasonCodePriorityPOs;
		}
	}


	public void execute(String handle) throws ExistingRecordException {
		this.setBeans();

		JobLogPO jobLogPO = jobLogComponent.getIdleJobLog(handle);

		if(jobLogPO == null) {
			throw new ExistingRecordException("Job_LOG_TABLE", null);
		}

		String serverNode = jobLogPO.getServerNode();

		logger.info("当前实例运行：" + JOB_NAME + " serverNode: " + serverNode);

		// execute
		logger.info("开始执行原因识别作业");
		logger.info("当前job线程数为：" + jobThreadPool.getPoolSize());

		Date oldDate = jobLogPO.getStartDateTime();

		int sqlSize =100000;
		int batchSize =5000;

		buildResourceReasonCodePO(Utils.addMinuteToDate(oldDate, 24 * 60, false), oldDate, sqlSize, batchSize, serverNode);

		logger.info("结束执行原因识别作业");

		// log end
		jobLogComponent.jobEndLog(handle, fetchSize);

		logger.info("JOB执行结束");
	}

	public void setBeans() {

		jobLogComponent =(JobLogComponent) SpringContextUtil.getBean("jobLogComponent");

		jobDispatchConfig = (JobDispatchConfig)SpringContextUtil.getBean("jobDispatchConfig");
		activityParamPOMapper = (ActivityParamPOMapper)SpringContextUtil.getBean("activityParamPOMapper");
		resourcePlcInfoPOMapper = (ResourcePlcInfoPOMapper)SpringContextUtil.getBean("resourcePlcInfoPOMapper");
		resourceAlertLogPOMapper = (ResourceAlertLogPOMapper)SpringContextUtil.getBean("resourceAlertLogPOMapper");
		reasonCodeAlertPOMapper = (ReasonCodeAlertPOMapper)SpringContextUtil.getBean("reasonCodeAlertPOMapper");

		resourceTimeLogComponent = (ResourceTimeLogComponent)SpringContextUtil.getBean("resourceTimeLogComponent");
		scheduledDownPlanComponent= (ScheduledDownPlanComponent)SpringContextUtil.getBean("scheduledDownPlanComponent");
		reasonCodePriorityComponent = (ReasonCodePriorityComponent)SpringContextUtil.getBean("reasonCodePriorityComponent");
		resourceReasonCodeLogComponent = (ResourceReasonCodeLogComponent)SpringContextUtil.getBean("resourceReasonCodeLogComponent");
		resourceComponent = (ResourceComponent )SpringContextUtil.getBean("resourceComponent");

		reasonCodeRuleComponent = (ReasonCodeRuleComponent)SpringContextUtil.getBean("reasonCodeRuleComponent");
		reasonCodeResStateComponent = (ReasonCodeResStateComponent)SpringContextUtil.getBean("reasonCodeResStateComponent");
		determineReasonCode = (DetermineReasonCode)SpringContextUtil.getBean("determineReasonCode");
		workCenterShiftComponent = (WorkCenterShiftComponent)SpringContextUtil.getBean("workCenterShiftComponent");
		resourceAlertLogComponent = (ResourceAlertLogComponent)SpringContextUtil.getBean("resourceAlertLogComponent");

		reasonCodeAlertComponent = (ReasonCodeAlertComponent)SpringContextUtil.getBean("reasonCodeAlertComponent");
		reasonCodeRuleParamComponent = (ReasonCodeRuleParamComponent)SpringContextUtil.getBean("reasonCodeRuleParamComponent");
		jobService = (JobService)SpringContextUtil.getBean("jobService");
		jobThreadPool = (AsyncService)SpringContextUtil.getBean("jobThreadPool");
		resourceReasonLogPOMapper = (ResourceReasonLogPOMapper)SpringContextUtil.getBean("resourceReasonLogPOMapper");
	}
}