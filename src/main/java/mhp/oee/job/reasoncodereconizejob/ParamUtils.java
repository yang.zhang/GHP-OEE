package mhp.oee.job.reasoncodereconizejob;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.utils.Utils;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ParamUtils {

	@InjectableLogger
	private static Logger logger;

	private static final String GREATER = ">";
	private static final String LESS = "<";
	private static final String EQUAL = "=";
	private static final String NOT_EQUAL = "<>";
	private static final String GREATER_OR_EQUAL = ">=";
	private static final String LESS_OR_EQUAL = "<=";

	public static boolean checkManualF(String symbol, String minute, Date startDate, Date endDate) {
		long diffMilliseconds = Utils.getDiffTime(startDate, endDate);
		long millisecond = Long.parseLong(minute) * 60 * 1000;
		if (symbol.equalsIgnoreCase(GREATER)) {
			return diffMilliseconds > millisecond;
		} else if (symbol.equalsIgnoreCase(GREATER_OR_EQUAL)) {
			return diffMilliseconds >= millisecond;
		} else if (symbol.equalsIgnoreCase(LESS)) {
			return diffMilliseconds < millisecond;
		} else if (symbol.equalsIgnoreCase(LESS_OR_EQUAL)) {
			return diffMilliseconds <= millisecond;
		} else if (symbol.equalsIgnoreCase(EQUAL)) {
			return diffMilliseconds == millisecond;
		} else if (symbol.equalsIgnoreCase(NOT_EQUAL)) {
			return diffMilliseconds != millisecond;
		}
		return false;
	}

}
