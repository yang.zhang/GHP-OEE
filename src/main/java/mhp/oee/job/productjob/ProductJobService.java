package mhp.oee.job.productjob;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.production.ProductionOutputComponent;
import mhp.oee.po.gen.ProductionOutputFirstPO;
import mhp.oee.po.gen.ProductionOutputRealtimePO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by LinZuK on 2016/9/29.
 */
@Service
@Transactional(rollbackFor = { Exception.class })
public class ProductJobService {
    @InjectableLogger
    private static Logger logger;

    @Autowired
    private ProductionOutputComponent productionOutputComponent;

    // 读取24小时内的 PRODUCTION_OUTPUT_FIRST 数据
    public List<ProductionOutputFirstPO> read24HourProductOutFirstData() {
        return productionOutputComponent.readProductionOutputFirst(24);
    }

    // 读取满足条件的 PRODUCTON_OUTPUT_REALTIME 数据
    public List<ProductionOutputRealtimePO> readProductionOutputRealtimeByCondition(ProductionOutputFirstPO po) {
        return productionOutputComponent.readProductionOutputRealtimeByCondition(po.getSite(), po.getResrce(), po.getItem(), po.getStartDateTime(), po.getEndDateTime());
    }

    public int updateProductOutFirst(ProductionOutputFirstPO updatePo) {
        return productionOutputComponent.updateProductionOutputFirst(updatePo);
    }
}
