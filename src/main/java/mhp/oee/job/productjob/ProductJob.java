package mhp.oee.job.productjob;

import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.component.job.JobLogComponent;
import mhp.oee.job.quartz.quartzutils.SpringContextUtil;
import mhp.oee.po.gen.ProductionOutputFirstPO;
import mhp.oee.po.gen.ProductionOutputRealtimePO;
import mhp.oee.utils.Utils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by LinZuK on 2016/9/29.
 */
@Service
public class ProductJob implements Job {

    @InjectableLogger
    private static Logger logger;

    private JobLogComponent jobLogComponent;
    private ProductJobService productJobService;

    private static final String JOB_NAME = "PRODUCT_JOB";

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        jobLogComponent = (JobLogComponent)SpringContextUtil.getBean("jobLogComponent");
        productJobService = (ProductJobService)SpringContextUtil.getBean("jobLogComponent");

        try {
            // lock and log start
            logger.info("开始争抢锁：" + JOB_NAME);
            Date startDateTime = new Date(); // 实际开始时间
            Date plannedDateTime = Utils.strToReportDateTime(Utils.reportDateTimeToStr(startDateTime)); // 计划开始时间
            String handle = jobLogComponent.jobStartLog(JOB_NAME, plannedDateTime, startDateTime, Utils.getMonoServer());
            if (null != handle) {
                logger.info("当前实例抢到锁：" + JOB_NAME + " " + Utils.datetimeMsToStr(plannedDateTime));
            } else {
                logger.info("当前实例没有抢到锁：" + JOB_NAME + " " + Utils.datetimeMsToStr(plannedDateTime));
                return;
            }

            // execute
            logger.info("开始执行产量维护作业");
            runProductJob();
            logger.info("结束执行产量维护作业");

            // log end
            jobLogComponent.jobEndLog(handle);
        } catch (Exception e) {
            logger.info("JOB执行错误");
        }
    }

    private void runProductJob() {
        List<ProductionOutputFirstPO> readyForUpdatePOs = new LinkedList<ProductionOutputFirstPO>();

        // 从表PRODUCTION_OUTPUT_FIRST中抓取创建时间（CREATED_DATE_TIME）距当前时间24小时内的记录
        List<ProductionOutputFirstPO> firstPOs = productJobService.read24HourProductOutFirstData();
        // 遍历集合A的每一条记录
        for (ProductionOutputFirstPO firstPO : firstPOs) {
            // 访问表PRODUCTON_OUTPUT_REALTIME取满足以下条件的记录的QTY_TOTAL合计值
            BigDecimal qtyTotalSum = new BigDecimal(0);
            List<ProductionOutputRealtimePO> realtimePOs = productJobService.readProductionOutputRealtimeByCondition(firstPO);
            for (ProductionOutputRealtimePO realtimePO : realtimePOs) {
                qtyTotalSum = qtyTotalSum.add(realtimePO.getQtyTotal());
            }
            // 若QTY_TOTAL合计值 <> （ A.QTY_YIELD + A.QTY_SCRAP）
            if (!qtyTotalSum.equals(firstPO.getQtyYield().add(firstPO.getQtyScrap()))) {
                // 则 A.QTY_YIELD = QTY_TOTAL合计值 – A.QTY_SCRAP
                firstPO.setQtyYield(qtyTotalSum.subtract(firstPO.getQtyScrap()));
                readyForUpdatePOs.add(firstPO);
            }
        }

        // 将标记为QTY_YIELD修改过的记录更新回表PRODUCTION_OUTPUT_FIRST
        // 同时更新下这条记录的MOFIFIED_DATE_TIME (= 当前修改时间)和 MODIFIED_USER（= 当前账号）
        for (ProductionOutputFirstPO updatePo : readyForUpdatePOs) {
            updatePo.setModifiedUser(JOB_NAME); //  updatePo.setModifiedDateTime(new Date()); 底层已经实现更新时间
            int ret = productJobService.updateProductOutFirst(updatePo);
            logger.info("更新产量维护数据：handle=" + updatePo.getHandle() + " qtyYield=" + updatePo.getQtyYield() + " 更新记录数=" + ret);
        }
    }


}
