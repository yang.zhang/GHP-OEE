package mhp.oee.job.quartz;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.job.quartz.quartzutils.ScheduleJob;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by LinZuK on 2016/8/31.
 */
@Service
public class TestJob {

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    @Autowired
    private DbJobProvider dbJobProvider;

    public void execute() throws SchedulerException {
        List<ScheduleJob> jobs = dbJobProvider.getScheduleJobs();
        System.out.println(gson.toJson(jobs));
    }

}
