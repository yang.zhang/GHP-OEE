package mhp.oee.job.quartz;

import mhp.oee.job.quartz.quartzutils.ScheduleJob;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by LinZuK on 2016/8/31.
 */
@Service
public class TestJobProvider implements IJobProvider {

    @Override
    public List<ScheduleJob> getScheduleJobs() {
        List<ScheduleJob> jobs = new LinkedList<ScheduleJob>();
        ScheduleJob job = new ScheduleJob();
        job.setJobName("jobName");
        job.setJobGroup("jobGroup");
        job.setJobStatus("1");
        job.setCronExpression("0 18 19 * * ?");
        job.setDescription("description");
        job.setTargetObject("productJob");
//        job.setClazz("mhp.oee.job.quartz.TestJob"); // 用springBean，这个参数可以没有
        job.setTargetMethod("run");
        job.setIsSpringBean("1");
        job.setConcurrent("0");
        jobs.add(job);
        return jobs;
    }
}
