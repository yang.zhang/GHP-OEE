package mhp.oee.job.quartz;

import mhp.oee.job.quartz.quartzutils.ScheduleJob;

import java.util.List;

/**
 * Created by LinZuK on 2016/8/31.
 */
public interface IJobProvider {
    /** 提供最新的job状态 */
    List<ScheduleJob> getScheduleJobs();
}
