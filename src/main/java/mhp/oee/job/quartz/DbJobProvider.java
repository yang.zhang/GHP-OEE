package mhp.oee.job.quartz;

import mhp.oee.config.JobDispatchConfig;
import mhp.oee.dao.extend.ScheduleJobPOMapper;
import mhp.oee.job.quartz.quartzutils.ScheduleJob;
import mhp.oee.job.quartz.quartzutils.SpringContextUtil;
import mhp.oee.po.extend.ScheduleJobPO;
import mhp.oee.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by LinZuK on 2016/8/31.
 */
@Service
public class DbJobProvider implements IJobProvider {

    @Autowired
    private ScheduleJobPOMapper scheduleJobPOMapper;

    @Override
    public List<ScheduleJob> getScheduleJobs() {
        Map<String, ScheduleJob> jobMap = new HashMap<String, ScheduleJob>();
        List<ScheduleJobPO> scheduleJobPOs = scheduleJobPOMapper.selectAllJob();
        for (ScheduleJobPO scheduleJobPO : scheduleJobPOs) {

            if(scheduleJobPO.getParamId().equalsIgnoreCase("SERVER_NODE")) {
                JobDispatchConfig config = (JobDispatchConfig) SpringContextUtil.getBean("jobDispatchConfig");
                config.setServerNodes(Integer.parseInt(scheduleJobPO.getParamDefaultValue()));
                System.out.println("server_node==>>" + config);
                continue;
            }

            String key = scheduleJobPO.getActivity()+"."+scheduleJobPO.getParamId();
            ScheduleJob job = jobMap.get(key);
            if (null == job) job = new ScheduleJob();
            job = generateJob(job, scheduleJobPO);
            if (null == job) continue;
            jobMap.put(key, job);
        }

        List<ScheduleJob> jobs = new LinkedList<ScheduleJob>();
        for (String key : jobMap.keySet()) {
            ScheduleJob job = jobMap.get(key);
            if (StringUtils.isNotBlank(job.getCronExpression())
                    && StringUtils.isNotBlank(job.getTargetObject())
                    && StringUtils.isNotBlank(job.getTargetMethod())) {
                jobs.add(job);
            }
        }

        return jobs;
    }

    private ScheduleJob generateJob(ScheduleJob job, ScheduleJobPO scheduleJobPO) {
        try {
            String cron;
            if (scheduleJobPO.getParamDefaultValue().trim().contains(":")) { // 几点执行 HH:mm:ss
                Date time = Utils.strToTime(scheduleJobPO.getParamDefaultValue().trim());
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(time);
                int hour24 = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                int second = calendar.get(Calendar.SECOND);
                cron = second + " " + minute + " " + hour24 + " * * ?";
            } else { // 每隔几分钟执行
                String minute = scheduleJobPO.getParamDefaultValue().trim();
                cron = "0 0/"+minute+" * * * ?";
            }

            // 执行类和方法
            String[] target = scheduleJobPO.getClassOrProgram().split("\\.");
            if (target.length != 2) return null;
            String targetObject = target[0];
            String targetMethod = target[1];

            job.setJobGroup(scheduleJobPO.getActivity());
            job.setJobName(scheduleJobPO.getParamId());
            job.setJobStatus("true".equalsIgnoreCase(scheduleJobPO.getEnabled()) ? "1" : "0");
            job.setDescription(scheduleJobPO.getDescription());
            job.setIsSpringBean("1");
            job.setConcurrent("0");
            job.setCronExpression(cron);
            job.setTargetObject(targetObject);
            job.setTargetMethod(targetMethod);
            if(scheduleJobPO.getParamId().contains("RUN_TIME")){
                job.setPlanedTime(scheduleJobPO.getParamDefaultValue());
            }

            return job;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
