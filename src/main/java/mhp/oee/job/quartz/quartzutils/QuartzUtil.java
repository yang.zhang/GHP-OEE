package mhp.oee.job.quartz.quartzutils;

import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by LinZuK on 2016/8/31.
 */
@Service
public class QuartzUtil {
    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;

    // 主工作需要保持名称唯一
    private List<String> jobNames = new LinkedList<String>();

    public boolean addJobName(String jobName) {
        return jobNames.add(jobName);
    }

    public boolean containsJobName(String jobName) {
        return jobNames.contains(jobName);
    }

    // 获取调度器
    public Scheduler getScheduler() {
        return schedulerFactoryBean.getScheduler();
    }

    /** * 创建子任务 * @param job * @throws Exception */
    public void createChildJob(ScheduleJob job) throws Exception {
        job.setJobName(getUUID()+job.getJobName()); //只是当前任务的名字而已，暂时的标记作用，不影响配置
        MethodInvokingJobDetailFactoryBean methodInvJobDetailFB = new MethodInvokingJobDetailFactoryBean();
        // 设置Job组名称
        methodInvJobDetailFB.setGroup(job.getJobGroup());
        // 设置Job名称
        methodInvJobDetailFB.setName(job.getJobName()); // 注意设置的顺序，如果在管理Job类提交到计划管理类之后设置就会设置不上
        // 定义的任务类为Spring的定义的Bean则调用 getBean方法
        if (job.getIsSpringBean().equals("1")) {// 是Spring中定义的Bean
            methodInvJobDetailFB
                    .setTargetObject(SpringContextUtil.getApplicationContext().getBean(job.getTargetObject()));
        } else {// 不是就直接new
            methodInvJobDetailFB.setTargetObject(Class.forName(job.getClazz()).newInstance());
        }
        // 设置任务方法
        methodInvJobDetailFB.setTargetMethod(job.getTargetMethod());
        // 将管理Job类提交到计划管理类
        methodInvJobDetailFB.afterPropertiesSet();

        /** 并发设置 */
        methodInvJobDetailFB.setConcurrent(job.getConcurrent().equals("1"));

        JobDetail jobDetail = methodInvJobDetailFB.getObject();// 动态
        jobDetail.getJobDataMap().put("scheduleJob", job);

        // 不按照表达式
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(job.getJobName(), job.getJobGroup())
                .build();
        /** 原理： 因为是立即执行,没有用到表达式嘛，所以按照实际的调度创建顺序依次执行 */
        Scheduler scheduler = getScheduler();
        scheduler.standby(); //暂时停止 任务都安排完之后统一启动 解决耗时任务按照顺序部署后执行紊乱的问题
        scheduler.scheduleJob(jobDetail, trigger);// 注入到管理类
    }

    private String getUUID(){
        String s = UUID.randomUUID().toString();
        return s.substring(0, 8) + s.substring(9, 13) + s.substring(14, 18) + s.substring(19, 23) + s.substring(24);
    }

    public void cleanAllJobs() throws SchedulerException {
        Scheduler scheduler = getScheduler();
        Set<TriggerKey> triggerKeys = scheduler.getTriggerKeys(GroupMatcher.anyTriggerGroup());
        for (TriggerKey triggerKey : triggerKeys) {
            Trigger trigger = scheduler.getTrigger(triggerKey);
            if (null != trigger
                    && !"DEFAULT.managerTriggerBean".equals(triggerKey.toString())) {
                scheduler.pauseTrigger(triggerKey);// 停止触发器
                scheduler.unscheduleJob(triggerKey);// 移除触发器
                scheduler.deleteJob(trigger.getJobKey());// 删除任务
            }
        }
    }

}
