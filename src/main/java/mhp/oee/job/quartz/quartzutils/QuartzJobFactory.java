package mhp.oee.job.quartz.quartzutils;

import mhp.oee.common.logging.InjectableLogger;
import org.quartz.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.stereotype.Service;

/**
 * Created by LinZuK on 2016/8/31.
 */
@Service
public class QuartzJobFactory {
    @InjectableLogger
    private static Logger logger;
    @Autowired
    private QuartzUtil quartzUtil;

    /** * 创建一个定时任务，并做安排 * * @param scheduler * @param job * @throws SchedulerException * @throws Exception */
    public void createSheduler(Scheduler scheduler, ScheduleJob job) throws Exception {
        // 在工作状态可用时,即job_status = 1 ,开始创建
        if (job.getJobStatus().equals("1")) {
            // 新建一个基于Spring的管理Job类
            JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
            // 设置Job名称
            jobDetailFactoryBean.setName(job.getJobName());

            jobDetailFactoryBean.setJobClass(SpringContextUtil.getApplicationContext().getBean(job.getTargetObject()).getClass());
            jobDetailFactoryBean.setApplicationContext(SpringContextUtil.getApplicationContext());

            // 将管理Job类提交到计划管理类
            jobDetailFactoryBean.afterPropertiesSet();

            JobDetail jobDetail = jobDetailFactoryBean.getObject();// 动态
            jobDetail.getJobDataMap().put("scheduleJob", job);
            //jobName存入到队列 每隔一段时间就会扫描所以需要时检测
            if(!quartzUtil.containsJobName(job.getJobName())){
                quartzUtil.addJobName(job.getJobName());
            }

            // 表达式调度构建器
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(job.getCronExpression());
            // 按新的cronExpression表达式构建一个新的trigger
            CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(job.getJobName(), job.getJobGroup())
                    .withSchedule(scheduleBuilder).build();

            scheduler.scheduleJob(jobDetail, trigger);// 注入到管理类
//            logger.info(job.getJobGroup() + "." + job.getJobName() + "创建完毕");
        }
    }

}
