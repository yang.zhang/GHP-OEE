package mhp.oee.job.quartz;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mhp.oee.common.logging.InjectableLogger;
import mhp.oee.job.quartz.quartzutils.QuartzJobFactory;
import mhp.oee.job.quartz.quartzutils.QuartzUtil;
import mhp.oee.job.quartz.quartzutils.ScheduleJob;
import org.quartz.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by LinZuK on 2016/8/31.
 */
public class JobLoader implements IJobLoader {
    @InjectableLogger
    private static Logger logger;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();


    @Autowired
    private QuartzJobFactory jobFactory;
    @Autowired
    private QuartzUtil quartzUtil;

    private IJobProvider jobProvider;
    public JobLoader(IJobProvider jobProvider) {
        this.jobProvider = jobProvider;
    }

    /**
     * 扫面数据库,查看是否有计划任务的变动
     * */
    @Override
    public void scanAndUpdateJob() {
        try {
//            logger.info("scanAndUpdateJob");
            Scheduler scheduler = quartzUtil.getScheduler();

            // clean jobs
            quartzUtil.cleanAllJobs();

            // load jobs
            List<ScheduleJob> jobList = jobProvider.getScheduleJobs();
            logger.info("load jobs: " + gson.toJson(jobList));
            if (jobList.size() != 0) {
                for (ScheduleJob newJob : jobList) {
                    TriggerKey triggerKey = TriggerKey.triggerKey(newJob.getJobName(), newJob.getJobGroup());
                    CronTrigger oldTrigger = (CronTrigger) scheduler.getTrigger(triggerKey);
                    if (null == oldTrigger) {
                        jobFactory.createSheduler(scheduler, newJob);
                    } else {// Trigger已存在，那么更新相应的定时设置
                        updateScheduler(newJob, triggerKey, oldTrigger);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("加载jobs时报错：" + e.getMessage());
        }
    }

    private void updateScheduler(ScheduleJob newJob, TriggerKey triggerKey, CronTrigger oldTrigger)
            throws SchedulerException {
        Scheduler scheduler = quartzUtil.getScheduler();
        if (newJob.getJobStatus().equals("1")) {// 0禁用 1启用
            if (!oldTrigger.getCronExpression().equalsIgnoreCase(newJob.getCronExpression())) {
                // 表达式调度构建器
                CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(newJob.getCronExpression());
                // 按新的cronExpression表达式重新构建trigger
                CronTrigger newTrigger = oldTrigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
                // 按新的trigger重新设置job执行
                scheduler.rescheduleJob(triggerKey, newTrigger);
//                logger.info(newJob.getJobGroup() + "." + newJob.getJobName() + " 更新完毕,目前cron表达式为:" + newJob.getCronExpression()
//                        + " isSpringBean：" + newJob.getIsSpringBean() + " concurrent: " + newJob.getConcurrent());
            }
        } else if (newJob.getJobStatus().equals("0")) {
            scheduler.pauseTrigger(triggerKey);// 停止触发器
            scheduler.unscheduleJob(triggerKey);// 移除触发器
            scheduler.deleteJob(oldTrigger.getJobKey());// 删除任务
//            logger.info(newJob.getJobGroup() + "." + newJob.getJobName() + "删除完毕");
        }

    }

}
