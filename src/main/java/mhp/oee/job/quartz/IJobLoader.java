package mhp.oee.job.quartz;

/**
 * Created by LinZuK on 2016/8/31.
 */
public interface IJobLoader {
    // 扫描数据库并更新job
    void scanAndUpdateJob();
}
