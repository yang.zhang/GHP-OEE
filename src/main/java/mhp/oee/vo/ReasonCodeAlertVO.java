package mhp.oee.vo;

import java.util.Date;

import mhp.oee.utils.Utils;

public class ReasonCodeAlertVO {
    private String resourceTypeBo;
    private String alertSequenceId;
    private String reasonCodeBo;
    private String strt;
    private String description;
    private String descriptionUnicode;
    private String used;
    private Date startDateTime;
    private Date endDateTime;

    public String getResourceTypeBo() {
        return resourceTypeBo;
    }

    public void setResourceTypeBo(String resourceTypeBo) {
        this.resourceTypeBo = resourceTypeBo;
    }

    public String getAlertSequenceId() {
        return alertSequenceId;
    }

    public void setAlertSequenceId(String alertSequenceId) {
        this.alertSequenceId = alertSequenceId;
    }

    public String getReasonCodeBo() {
        return reasonCodeBo;
    }

    public void setReasonCodeBo(String reasonCodeBo) {
        this.reasonCodeBo = reasonCodeBo;
    }

    public String getStrt() {
        return strt;
    }

    public void setStrt(String strt) {
        this.strt = strt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public void transferToUnicode() {
        this.setDescriptionUnicode(Utils.string2Unicode(description));
    }

    public String getDescriptionUnicode() {
        return descriptionUnicode;
    }

    public void setDescriptionUnicode(String descriptionUnicode) {
        this.descriptionUnicode = descriptionUnicode;
    }

}