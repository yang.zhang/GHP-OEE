package mhp.oee.vo;

import java.util.Date;

public class ResourceStatusDetailVO {
    private Date date;
    private String strDate;
    private String shift;
    private String prdUserId;
    private String meUserId;
    private String resource;
    private String asset;
    private String lineArea;
    private String resourceAddr;
    private String resourceDesc;
    private String strStartTime;
    private String strEndTime;
    private Date startTime;
    private Date endTime;
    private String resourceState;
    private String duration;
    private String item;
    private String model;
    private String strStartDateTime;
    private String strEndDateTime;
    private Date startDateTime;
    private Date endDateTime;
    private Date reasonCodeStartTime;
    private Date reasonCodeEndTime;
    private String reasonCodeDesc;
    private Integer qtyInput;
    private Integer qtyYield;
    private Integer qtyScrap;
    private Integer qtyTotal;
    private String resourceCode;
    private String alertInfo;
    private Date alertDateTime;
    private String strAlertDateTime;
    private String comment;
    private Date reasonCodeTime;
    private String strReasonCodeTime;
    private String reasonCodeDescription;
    private String workAreaDesc;
	private String resourceTypeDesc;
    private String resourceType;
    private String commentCode;
    private String alertCommentDescription;
    public String getCommentCode() {
    	return commentCode;
    }
    public void setCommentCode(String commentCode) {
    	this.commentCode = commentCode;
    }
    public String getAlertCommentDescription() {
    	return alertCommentDescription;
    }
    public void setAlertCommentDescription(String alertCommentDescription) {
    	this.alertCommentDescription = alertCommentDescription;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public String getStrDate() {
        return strDate;
    }
    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }
    public String getShift() {
        return shift;
    }
    public void setShift(String shift) {
        this.shift = shift;
    }
    public String getPrdUserId() {
        return prdUserId;
    }
    public void setPrdUserId(String prdUserId) {
        this.prdUserId = prdUserId;
    }
    public String getMeUserId() {
        return meUserId;
    }
    public void setMeUserId(String meUserId) {
        this.meUserId = meUserId;
    }
    public String getResource() {
        return resource;
    }
    public void setResource(String resource) {
        this.resource = resource;
    }
    public String getAsset() {
        return asset;
    }
    public void setAsset(String asset) {
        this.asset = asset;
    }
    public String getLineArea() {
        return lineArea;
    }
    public void setLineArea(String lineArea) {
        this.lineArea = lineArea;
    }
    public String getResourceAddr() {
        return resourceAddr;
    }
    public void setResourceAddr(String resourceAddr) {
        this.resourceAddr = resourceAddr;
    }
    public String getResourceDesc() {
        return resourceDesc;
    }
    public void setResourceDesc(String resourceDesc) {
        this.resourceDesc = resourceDesc;
    }
    public String getStrStartTime() {
        return strStartTime;
    }
    public void setStrStartTime(String strStartTime) {
        this.strStartTime = strStartTime;
    }
    public String getStrEndTime() {
        return strEndTime;
    }
    public void setStrEndTime(String strEndTime) {
        this.strEndTime = strEndTime;
    }
    public Date getStartTime() {
        return startTime;
    }
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
    public Date getEndTime() {
        return endTime;
    }
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    public String getResourceState() {
        return resourceState;
    }
    public void setResourceState(String resourceState) {
        this.resourceState = resourceState;
    }
    public String getDuration() {
        return duration;
    }
    public void setDuration(String duration) {
        this.duration = duration;
    }
    public String getItem() {
        return item;
    }
    public void setItem(String item) {
        this.item = item;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public String getStrStartDateTime() {
        return strStartDateTime;
    }
    public void setStrStartDateTime(String strStartDateTime) {
        this.strStartDateTime = strStartDateTime;
    }
    public String getStrEndDateTime() {
        return strEndDateTime;
    }
    public void setStrEndDateTime(String strEndDateTime) {
        this.strEndDateTime = strEndDateTime;
    }
    public Date getStartDateTime() {
        return startDateTime;
    }
    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }
    public Date getEndDateTime() {
        return endDateTime;
    }
    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }
    public Date getReasonCodeStartTime() {
        return reasonCodeStartTime;
    }
    public void setReasonCodeStartTime(Date reasonCodeStartTime) {
        this.reasonCodeStartTime = reasonCodeStartTime;
    }
    public Date getReasonCodeEndTime() {
        return reasonCodeEndTime;
    }
    public void setReasonCodeEndTime(Date reasonCodeEndTime) {
        this.reasonCodeEndTime = reasonCodeEndTime;
    }
    public String getReasonCodeDesc() {
        return reasonCodeDesc;
    }
    public void setReasonCodeDesc(String reasonCodeDesc) {
        this.reasonCodeDesc = reasonCodeDesc;
    }
    public Integer getQtyInput() {
        return qtyInput;
    }
    public void setQtyInput(Integer qtyInput) {
        this.qtyInput = qtyInput;
    }
    public Integer getQtyYield() {
        return qtyYield;
    }
    public void setQtyYield(Integer qtyYield) {
        this.qtyYield = qtyYield;
    }
    public Integer getQtyScrap() {
        return qtyScrap;
    }
    public void setQtyScrap(Integer qtyScrap) {
        this.qtyScrap = qtyScrap;
    }
    public Integer getQtyTotal() {
        return qtyTotal;
    }
    public void setQtyTotal(Integer qtyTotal) {
        this.qtyTotal = qtyTotal;
    }
    public String getResourceCode() {
        return resourceCode;
    }
    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }
    public String getAlertInfo() {
        return alertInfo;
    }
    public void setAlertInfo(String alertInfo) {
        this.alertInfo = alertInfo;
    }
    public Date getAlertDateTime() {
		return alertDateTime;
	}
	public void setAlertDateTime(Date alertDateTime) {
		this.alertDateTime = alertDateTime;
	}
	public String getStrAlertDateTime() {
		return strAlertDateTime;
	}
	public void setStrAlertDateTime(String strAlertDateTime) {
		this.strAlertDateTime = strAlertDateTime;
	}
	public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
	public Date getReasonCodeTime() {
		return reasonCodeTime;
	}
	public void setReasonCodeTime(Date reasonCodeTime) {
		this.reasonCodeTime = reasonCodeTime;
	}
	public String getReasonCodeDescription() {
		return reasonCodeDescription;
	}
	public void setReasonCodeDescription(String reasonCodeDescription) {
		this.reasonCodeDescription = reasonCodeDescription;
	}
	public String getStrReasonCodeTime() {
		return strReasonCodeTime;
	}
	public void setStrReasonCodeTime(String strReasonCodeTime) {
		this.strReasonCodeTime = strReasonCodeTime;
	}
	public String getWorkAreaDesc() {
		return workAreaDesc;
	}
	public void setWorkAreaDesc(String workAreaDesc) {
		this.workAreaDesc = workAreaDesc;
	}
	public String getResourceTypeDesc() {
		return resourceTypeDesc;
	}
	public void setResourceTypeDesc(String resourceTypeDesc) {
		this.resourceTypeDesc = resourceTypeDesc;
	}
	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
}
