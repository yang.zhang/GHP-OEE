package mhp.oee.vo;

import java.util.Date;

import mhp.oee.po.gen.ActivityPermPO;

public class ActivityVO {

    private String activity;
    private String description;
    private String enabled;
    private String visible;
    private Integer sequenceId;
    private String executionType;
    private String classOrProgram;
    private Date firstModifiedDateTime;

    private String handle;
    private Date createdDateTime;
    private Date modifiedDateTime;
    private String modifiedUser;

    private Character viewActionCode;

    private ActivityPermPO permPO;


    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public Date getFirstModifiedDateTime() {
        return firstModifiedDateTime;
    }

    public void setFirstModifiedDateTime(Date firstModifiedDateTime) {
        this.firstModifiedDateTime = firstModifiedDateTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId) {
        this.sequenceId = sequenceId;
    }

    public String getExecutionType() {
        return executionType;
    }

    public void setExecutionType(String executionType) {
        this.executionType = executionType;
    }

    public String getClassOrProgram() {
        return classOrProgram;
    }

    public void setClassOrProgram(String classOrProgram) {
        this.classOrProgram = classOrProgram;
    }

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((handle == null) ? 0 : handle.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ActivityVO other = (ActivityVO) obj;
        if (handle == null) {
            if (other.handle != null)
                return false;
        } else if (!handle.equals(other.handle))
            return false;
        return true;
    }

    public ActivityPermPO getPermPO() {
        return permPO;
    }

    public void setPermPO(ActivityPermPO permPO) {
        this.permPO = permPO;
    }

}