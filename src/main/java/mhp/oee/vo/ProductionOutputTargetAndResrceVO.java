package mhp.oee.vo;

import mhp.oee.po.gen.ItemPO;
import mhp.oee.po.gen.ProductionOutputTargetPO;
import mhp.oee.po.gen.ResourcePO;

public class ProductionOutputTargetAndResrceVO extends ProductionOutputTargetPO implements Comparable<ProductionOutputTargetAndResrceVO>{
    private ResourcePO resourcePO;
    private ItemPO itemPO;
    private String model;
    private Character viewActionCode;

    public ResourcePO getResourcePO() {
        return resourcePO;
    }
    public void setResourcePO(ResourcePO resourcePO) {
        this.resourcePO = resourcePO;
    }
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
    
    public Character getViewActionCode() {
        return viewActionCode;
    }
    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }
    public ItemPO getItemPO() {
        return itemPO;
    }
    public void setItemPO(ItemPO itemPO) {
        this.itemPO = itemPO;
    }
    @Override
    public int compareTo(ProductionOutputTargetAndResrceVO obj) {
        if (this.resourcePO.getResrce().compareTo(obj.getResourcePO().getResrce())== 0) {
            if (this.getItem().compareTo(obj.getItem()) == 0) {
                if (this.getShift().compareTo(obj.getShift()) == 0) {
                    return this.getTargetDate().compareTo(obj.getTargetDate());
                } else {
                    return this.getShift().compareTo(obj.getShift());
                }
            } else {
                return this.getItem().compareTo(obj.getItem());
            }
        } else {
            return this.resourcePO.getResrce().compareTo(obj.getResourcePO().getResrce());
        }
    }
    @Override
    public String toString() {
        return "ProductionOutputTargetAndResrceVO [resourcePO=" + resourcePO + ", model=" + model + ", viewActionCode="
                + viewActionCode + ", getSite()=" + getSite() + ", getItem()="
                + getItem() + ", getResrce()=" + getResrce()
                + ", getTargeQtyYield()=" + getTargeQtyYield() + ", getTargeQtyYieldUnit()=" + getTargeQtyYieldUnit()
                + ", getTargetDate()=" + getTargetDate() + ", getShift()=" + getShift() + ", getShiftDescription()="
                + getShiftDescription() + ", getIsDefaultValue()=" + getIsDefaultValue() + "]";
    }
    
    
    
}
