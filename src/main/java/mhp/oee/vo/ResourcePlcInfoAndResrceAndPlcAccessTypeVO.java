package mhp.oee.vo;

import mhp.oee.po.extend.ResourcePlcInfoAndResrceAndPlcAccessTypePO;

public class ResourcePlcInfoAndResrceAndPlcAccessTypeVO extends ResourcePlcInfoAndResrceAndPlcAccessTypePO{
   
    private Character viewActionCode;

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }
    
}