package mhp.oee.vo;

/**
 * Created by LinZuK on 2016/7/29.
 */
public class ActivityParamIdAndValueVO {
    private String activityFParamId;
    private String activityFParamValue;

    public String getActivityFParamId() {
        return activityFParamId;
    }

    public void setActivityFParamId(String activityFParamId) {
        this.activityFParamId = activityFParamId;
    }

    public String getActivityFParamValue() {
        return activityFParamValue;
    }

    public void setActivityFParamValue(String activityFParamValue) {
        this.activityFParamValue = activityFParamValue;
    }
}
