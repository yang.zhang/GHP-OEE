package mhp.oee.vo;

import java.util.Date;

import mhp.oee.utils.Utils;

public class ResourcePlcVO {

    private String handle;
    private String site;
    private String resrce; // manually added against PO
    private String resrceDes;
    private String plcAddress;
    private String plcValue;
    private String category;
    private String categoryDes;
    private String plcObject;
    private String plcObjectDes;
    private String description;
    private String logPath;
    private String descriptionUnicode;
    private String logPathUnicode;
    private Date modifiedDateTime;
    private Character viewActionCode;
    private int pageIndex;
    private int pageCount;
    private String resourceBo;

	public String getResourceBo() {
        return resourceBo;
    }

    public void setResourceBo(String resourceBo) {
        this.resourceBo = resourceBo;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getResrceDes() {
        return resrceDes;
    }

    public void setResrceDes(String resrceDes) {
        this.resrceDes = resrceDes;
    }

    public String getCategoryDes() {
        return categoryDes;
    }

    public void setCategoryDes(String categoryDes) {
        this.categoryDes = categoryDes;
    }

    public String getPlcObjectDes() {
        return plcObjectDes;
    }

    public void setPlcObjectDes(String plcObjectDes) {
        this.plcObjectDes = plcObjectDes;
    }

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public String getPlcAddress() {
        return plcAddress;
    }

    public void setPlcAddress(String plcAddress) {
        this.plcAddress = plcAddress;
    }

    public String getPlcValue() {
        return plcValue;
    }

    public void setPlcValue(String plcValue) {
        this.plcValue = plcValue;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPlcObject() {
        return plcObject;
    }

    public void setPlcObject(String plcObject) {
        this.plcObject = plcObject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogPath() {
        return logPath;
    }

    public void setLogPath(String logPath) {
        this.logPath = logPath;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public void transferToUnicode() {
        this.setDescriptionUnicode(Utils.string2Unicode(description));
        this.setLogPathUnicode(Utils.string2Unicode(logPath));
    }

    public String getDescriptionUnicode() {
        return descriptionUnicode;
    }

    public void setDescriptionUnicode(String descriptionUnicode) {
        this.descriptionUnicode = descriptionUnicode;
    }

    public String getLogPathUnicode() {
        return logPathUnicode;
    }

    public void setLogPathUnicode(String logPathUnicode) {
        this.logPathUnicode = logPathUnicode;
    }

}