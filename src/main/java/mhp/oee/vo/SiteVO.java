package mhp.oee.vo;

import mhp.oee.po.gen.SitePO;

public class SiteVO extends SitePO {

    private Character viewActionCode;

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }
}
