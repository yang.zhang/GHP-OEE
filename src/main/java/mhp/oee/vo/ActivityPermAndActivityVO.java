package mhp.oee.vo;


import mhp.oee.po.extend.ActivityPermAndActivityPO;

public class ActivityPermAndActivityVO extends ActivityPermAndActivityPO {
    private Character viewActionCode;
    private String activityGroup;
    private String activityGroupDes;

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }
    public String getActivityGroup() {
        return activityGroup;
    }

    public void setActivityGroup(String activityGroup) {
        this.activityGroup = activityGroup;
    }

    public String getActivityGroupDes() {
        return activityGroupDes;
    }

    public void setActivityGroupDes(String activityGroupDes) {
        this.activityGroupDes = activityGroupDes;
    }
}
