package mhp.oee.vo;

import java.util.List;

/**
 * Created by LinZuK on 2016/7/29.
 */
public class ActivityFParamVO {
    private String activityFParamResourceTypeBo;
    private String activityFParamResourceType;
    private String activityFParamResourceTypeDesc;
    private String activityFParamResourceTypeEnabled;
    private String activityFParamValue;
    private String activityFParamModifiedDateTime;
    private String activityFParamModifiedUser;

    public String getActivityFParamResourceTypeBo() {
        return activityFParamResourceTypeBo;
    }

    public void setActivityFParamResourceTypeBo(String activityFParamResourceTypeBo) {
        this.activityFParamResourceTypeBo = activityFParamResourceTypeBo;
    }

    public String getActivityFParamResourceType() {
        return activityFParamResourceType;
    }

    public void setActivityFParamResourceType(String activityFParamResourceType) {
        this.activityFParamResourceType = activityFParamResourceType;
    }

    public String getActivityFParamResourceTypeDesc() {
        return activityFParamResourceTypeDesc;
    }

    public void setActivityFParamResourceTypeDesc(String activityFParamResourceTypeDesc) {
        this.activityFParamResourceTypeDesc = activityFParamResourceTypeDesc;
    }

    public String getActivityFParamResourceTypeEnabled() {
        return activityFParamResourceTypeEnabled;
    }

    public void setActivityFParamResourceTypeEnabled(String activityFParamResourceTypeEnabled) {
        this.activityFParamResourceTypeEnabled = activityFParamResourceTypeEnabled;
    }

    public String getActivityFParamValue() {
        return activityFParamValue;
    }

    public void setActivityFParamValue(String activityFParamValue) {
        this.activityFParamValue = activityFParamValue;
    }

    public String getActivityFParamModifiedDateTime() {
        return activityFParamModifiedDateTime;
    }

    public void setActivityFParamModifiedDateTime(String activityFParamModifiedDateTime) {
        this.activityFParamModifiedDateTime = activityFParamModifiedDateTime;
    }

    public String getActivityFParamModifiedUser() {
        return activityFParamModifiedUser;
    }

    public void setActivityFParamModifiedUser(String activityFParamModifiedUser) {
        this.activityFParamModifiedUser = activityFParamModifiedUser;
    }
}
