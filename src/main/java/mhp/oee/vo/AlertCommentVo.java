package mhp.oee.vo;

import java.util.List;

import mhp.oee.po.extend.ResourceAlertLogAndAlertCommentPo;
import mhp.oee.po.gen.AlertCommentPO;
import mhp.oee.po.gen.ResourceAlertLogPO;

public class AlertCommentVo {

	private int commentSize;
	private List<ResourceAlertLogPO> pos;
	private List<AlertCommentPO> commentList;
	
	public List<AlertCommentPO> getCommentList() {
		return commentList;
	}
	public void setCommentList(List<AlertCommentPO> commentList) {
		this.commentList = commentList;
	}
	public int getCommentSize() {
		return commentSize;
	}
	public void setCommentSize(int commentSize) {
		this.commentSize = commentSize;
	}
	public List<ResourceAlertLogPO> getPos() {
		return pos;
	}
	public void setPos(List<ResourceAlertLogPO> pos) {
		this.pos = pos;
	}
	
}
