package mhp.oee.vo;

import java.util.Date;

/**
 * Created by LinZuK on 2016/8/11.
 */
public class RuleParamVo {
    private String reasonCodeRuleParamBo;
    private String activityFParamId;
    private String activityFParamValue;
    private String paramId;
    private String paramSetValue;
    private String paramValue;
    private String resourceType;
    private String resourceTypeDesc;
    private Date modifiedDateTime;

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getReasonCodeRuleParamBo() {
        return reasonCodeRuleParamBo;
    }

    public void setReasonCodeRuleParamBo(String reasonCodeRuleParamBo) {
        this.reasonCodeRuleParamBo = reasonCodeRuleParamBo;
    }

    public String getActivityFParamId() {
        return activityFParamId;
    }

    public void setActivityFParamId(String activityFParamId) {
        this.activityFParamId = activityFParamId;
    }

    public String getActivityFParamValue() {
        return activityFParamValue;
    }

    public void setActivityFParamValue(String activityFParamValue) {
        this.activityFParamValue = activityFParamValue;
    }

    public String getParamId() {
        return paramId;
    }

    public void setParamId(String paramId) {
        this.paramId = paramId;
    }

    public String getParamSetValue() {
        return paramSetValue;
    }

    public void setParamSetValue(String paramSetValue) {
        this.paramSetValue = paramSetValue;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceTypeDesc() {
        return resourceTypeDesc;
    }

    public void setResourceTypeDesc(String resourceTypeDesc) {
        this.resourceTypeDesc = resourceTypeDesc;
    }
}
