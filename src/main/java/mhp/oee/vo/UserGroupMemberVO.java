package mhp.oee.vo;

import mhp.oee.po.gen.UserGroupMemberPO;

public class UserGroupMemberVO extends UserGroupMemberPO {
    private Character viewActionCode;

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }
}