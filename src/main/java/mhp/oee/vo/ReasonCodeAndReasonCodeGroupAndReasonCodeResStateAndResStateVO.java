package mhp.oee.vo;

import java.util.Date;

public class ReasonCodeAndReasonCodeGroupAndReasonCodeResStateAndResStateVO {
    private String handle;
    private String site;
    private String reaonCode;
    private String reasonCodeDes;
    private String reasonCodeGroup;
    private String reasonCodeGroupDes;
    private String state;
    private String stateDes;
    private Date startDateTime;
    private Date endDateTime;
    private String dateTime;
    private String startDateTimeIn;
    private String endDateTimeIn;
    private Date modifiedDateTime;
    private int pageIndex;
    private int pageCount;

    public String getStartDateTimeIn() {
        return startDateTimeIn;
    }

    public void setStartDateTimeIn(String startDateTimeIn) {
        this.startDateTimeIn = startDateTimeIn;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getReaonCode() {
        return reaonCode;
    }

    public void setReaonCode(String reaonCode) {
        this.reaonCode = reaonCode;
    }

    public String getReasonCodeDes() {
        return reasonCodeDes;
    }

    public void setReasonCodeDes(String reasonCodeDes) {
        this.reasonCodeDes = reasonCodeDes;
    }

    public String getReasonCodeGroup() {
        return reasonCodeGroup;
    }

    public void setReasonCodeGroup(String reasonCodeGroup) {
        this.reasonCodeGroup = reasonCodeGroup;
    }

    public String getReasonCodeGroupDes() {
        return reasonCodeGroupDes;
    }

    public void setReasonCodeGroupDes(String reasonCodeGroupDes) {
        this.reasonCodeGroupDes = reasonCodeGroupDes;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateDes() {
        return stateDes;
    }

    public void setStateDes(String stateDes) {
        this.stateDes = stateDes;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getEndDateTimeIn() {
        return endDateTimeIn;
    }

    public void setEndDateTimeIn(String endDateTimeIn) {
        this.endDateTimeIn = endDateTimeIn;
    }
}
