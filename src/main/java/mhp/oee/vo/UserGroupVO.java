package mhp.oee.vo;

import mhp.oee.po.gen.UserGroupPO;

public class UserGroupVO extends UserGroupPO{
    private Character viewActionCode;

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }
}