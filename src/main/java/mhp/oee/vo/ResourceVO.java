package mhp.oee.vo;

import java.util.Date;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class ResourceVO {
    private String handle;
    private String site;
    private String resrce;
    private String description;
    private String asset;
    private String subAsset;
    private String lineArea;
    private String lineAreaDes;
    private String workArea;
    private String workAreaDes;
    private String workshop;
    private String enabled;
    private String eqpUserId;
    private String configuredPlcInfo;
    private Date createdDateTime;
    private Date modifiedDateTime;
    private String modifiedUser;
    private Character viewActionCode;
    private String field;
    
    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getSubAsset() {
        return subAsset;
    }

    public void setSubAsset(String subAsset) {
        this.subAsset = subAsset;
    }

    public String getLineArea() {
        return lineArea;
    }

    public void setLineArea(String lineArea) {
        this.lineArea = lineArea;
    }

    public String getWorkArea() {
        return workArea;
    }

    public void setWorkArea(String workArea) {
        this.workArea = workArea;
    }

    public String getWorkshop() {
        return workshop;
    }

    public void setWorkshop(String workshop) {
        this.workshop = workshop;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getEqpUserId() {
        return eqpUserId;
    }

    public void setEqpUserId(String eqpUserId) {
        this.eqpUserId = eqpUserId;
    }

    public String getConfiguredPlcInfo() {
        return configuredPlcInfo;
    }

    public void setConfiguredPlcInfo(String configuredPlcInfo) {
        this.configuredPlcInfo = configuredPlcInfo;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

    public String getLineAreaDes() {
        return lineAreaDes;
    }

    public void setLineAreaDes(String lineAreaDes) {
        this.lineAreaDes = lineAreaDes;
    }

    public String getWorkAreaDes() {
        return workAreaDes;
    }

    public void setWorkAreaDes(String workAreaDes) {
        this.workAreaDes = workAreaDes;
    }

    
    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public int hashCode() {
        return handle.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ResourceVO) {
            ResourceVO vo = (ResourceVO) obj;
            return (handle.equals(vo.handle));
        }
        return super.equals(obj);
    }
}
