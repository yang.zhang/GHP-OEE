package mhp.oee.vo;

import java.util.List;

public class PlcInfoLineVO {

    private WorkCenterVO line;
    private List<PlcInfoResourceTypeVO> plcInfoResourceType;

    public WorkCenterVO getLine() {
        return line;
    }

    public void setLine(WorkCenterVO line) {
        this.line = line;
    }

    public List<PlcInfoResourceTypeVO> getPlcInfoResourceType() {
        return plcInfoResourceType;
    }

    public void setPlcInfoResourceType(List<PlcInfoResourceTypeVO> plcInfoResourceType) {
        this.plcInfoResourceType = plcInfoResourceType;
    }

}
