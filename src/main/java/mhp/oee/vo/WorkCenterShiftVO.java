package mhp.oee.vo;

import java.math.BigDecimal;
import java.util.Date;

public class WorkCenterShiftVO {

    private String handle;
    private String workCenterBo;
    private String shift;
    private Date startTime;
    private Date endTime;
    private BigDecimal shiftDuration;
    private String description;
    private String enabled;
    private Date modifiedDateTime;
    private Character viewActionCode;

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getWorkCenterBo() {
        return workCenterBo;
    }

    public void setWorkCenterBo(String workCenterBo) {
        this.workCenterBo = workCenterBo;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public BigDecimal getShiftDuration() {
        return shiftDuration;
    }

    public void setShiftDuration(BigDecimal shiftDuration) {
        this.shiftDuration = shiftDuration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

}
