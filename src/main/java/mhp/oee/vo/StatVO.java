package mhp.oee.vo;

public class StatVO {
    private int itemCount;
    private int alertCount;
    private int reasonCodeCount;
    private int outputCount;
    private int haltCount;
    private int rtReasonCodeCount;
    private String duration;
    
    public int getOutputCount() {
        return outputCount;
    }
    public void setOutputCount(int outputCount) {
        this.outputCount = outputCount;
    }
    public int getRtReasonCodeCount() {
		return rtReasonCodeCount;
	}
	public void setRtReasonCodeCount(int rtReasonCount) {
		this.rtReasonCodeCount = rtReasonCount;
	}
    public int getItemCount() {
        return itemCount;
    }
    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }
    public int getAlertCount() {
        return alertCount;
    }
    public void setAlertCount(int alertCount) {
        this.alertCount = alertCount;
    }
    public int getReasonCodeCount() {
        return reasonCodeCount;
    }
    public void setReasonCodeCount(int reasonCodeCount) {
        this.reasonCodeCount = reasonCodeCount;
    }
    public int getHaltCount() {
        return haltCount;
    }
    public void setHaltCount(int haltCount) {
        this.haltCount = haltCount;
    }
    public String getDuration() {
        return duration;
    }
    public void setDuration(String duration) {
        this.duration = duration;
    }
}
