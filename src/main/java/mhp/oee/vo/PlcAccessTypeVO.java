package mhp.oee.vo;

import mhp.oee.po.gen.PlcAccessTypePO;

public class PlcAccessTypeVO extends PlcAccessTypePO{

    private Character viewActionCode;

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }
}