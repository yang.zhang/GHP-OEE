package mhp.oee.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
public class ScheduledDownPlanAndResrceAndWorkCenterVO implements Comparable<ScheduledDownPlanAndResrceAndWorkCenterVO>{
    private String handle;
    private String site;
    private String resrce;
    private String asset;
    private String resourceBo;
    private String strt;
    private String resrceDes;
    private String workCenter;
    private String workCenterDes;
    private String line;
    private String lineDes;
    private String startDateTimeIn;
    private String endDateTimeIn;
    private Date startDateTime;
    private Date endDateTime;
    private String reasonCode;
    private Date modifiedDateTime;
    private Character viewActionCode;
    private String reasonCodeDes;
    
    public String getSite() {
        return site;
    }
    public void setSite(String site) {
        this.site = site;
    }
    public String getResrce() {
        return resrce;
    }
    public void setResrce(String resrce) {
        this.resrce = resrce;
    }
    public String getResrceDes() {
        return resrceDes;
    }
    public void setResrceDes(String resrceDes) {
        this.resrceDes = resrceDes;
    }
    public String getWorkCenter() {
        return workCenter;
    }
    public void setWorkCenter(String workCenter) {
        this.workCenter = workCenter;
    }
    public String getWorkCenterDes() {
        return workCenterDes;
    }
    public void setWorkCenterDes(String workCenterDes) {
        this.workCenterDes = workCenterDes;
    }
    public String getLine() {
        return line;
    }
    public void setLine(String line) {
        this.line = line;
    }
    public String getLineDes() {
        return lineDes;
    }
    public void setLineDes(String lineDes) {
        this.lineDes = lineDes;
    }
    public String getStartDateTimeIn() {
        return startDateTimeIn;
    }
    public void setStartDateTimeIn(String startDateTimeIn) {
        this.startDateTimeIn = startDateTimeIn;
    }
    public String getEndDateTimeIn() {
        return endDateTimeIn;
    }
    public void setEndDateTimeIn(String endDateTimeIn) {
        this.endDateTimeIn = endDateTimeIn;
    }
    public Date getStartDateTime() {
        return startDateTime;
    }
    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }
    public Date getEndDateTime() {
        return endDateTime;
    }
    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }
    public String getReasonCode() {
        return reasonCode;
    }
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }
    public Character getViewActionCode() {
        return viewActionCode;
    }
    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }
    public String getHandle() {
        return handle;
    }
    public void setHandle(String handle) {
        this.handle = handle;
    }
    public String getResourceBo() {
        return resourceBo;
    }
    public void setResourceBo(String resourceBo) {
        this.resourceBo = resourceBo;
    }
    public String getStrt() {
        return strt;
    }
    public void setStrt(String strt) {
        this.strt = strt;
    }
    
    public String getAsset() {
        return asset;
    }
    public void setAsset(String asset) {
        this.asset = asset;
    }
    @Override
    public int compareTo(ScheduledDownPlanAndResrceAndWorkCenterVO vo) {
        List<Character> orderList = new ArrayList<Character>();
        orderList.add('D');  
        orderList.add('U');  
        orderList.add('C');  
        int io1 = orderList.indexOf(this.viewActionCode);
        int io2 = orderList.indexOf(vo.getViewActionCode());
        return io1 - io2;
    }
    @Override
    public String toString() {
        return "ScheduledDownPlanAndResrceAndWorkCenterVO [site=" + site + ", resrce=" + resrce + ", viewActionCode="
                + viewActionCode + "]";
    }
    public String getReasonCodeDes() {
        return reasonCodeDes;
    }
    public void setReasonCodeDes(String reasonCodeDes) {
        this.reasonCodeDes = reasonCodeDes;
    }
    
}
