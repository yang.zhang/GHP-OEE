package mhp.oee.vo;

public class WorkCenterMemberVO {

    private String handle;
    private String workCenterBo;
    private String workCenterGbo;
    private Character viewActionCode;

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getWorkCenterBo() {
        return workCenterBo;
    }

    public void setWorkCenterBo(String workCenterBo) {
        this.workCenterBo = workCenterBo;
    }

    public String getWorkCenterGbo() {
        return workCenterGbo;
    }

    public void setWorkCenterGbo(String workCenterGbo) {
        this.workCenterGbo = workCenterGbo;
    }

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

}