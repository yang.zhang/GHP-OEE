package mhp.oee.vo;

import java.util.List;

import mhp.oee.po.extend.ResourceTypeResourceAndResourceAndResourceTypePO;

public class PlcInfoResourceTypeVO {
    private ResourceTypeVO resourceTypeVO;
    private List<ResourceTypeResourceAndResourceAndResourceTypePO> ResourceVOs;

    public ResourceTypeVO getResourceTypeVO() {
        return resourceTypeVO;
    }

    public void setResourceTypeVO(ResourceTypeVO resourceTypeVO) {
        this.resourceTypeVO = resourceTypeVO;
    }

    public List<ResourceTypeResourceAndResourceAndResourceTypePO> getResourceVOs() {
        return ResourceVOs;
    }

    public void setResourceVOs(List<ResourceTypeResourceAndResourceAndResourceTypePO> resourceVOs) {
        ResourceVOs = resourceVOs;
    }

}
