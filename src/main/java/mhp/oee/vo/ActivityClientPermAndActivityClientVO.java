package mhp.oee.vo;

import mhp.oee.po.extend.ActivityClientPermAndActivityClientPO;

public class ActivityClientPermAndActivityClientVO extends ActivityClientPermAndActivityClientPO{
    
    private Character viewActionCode;

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }
    
}
