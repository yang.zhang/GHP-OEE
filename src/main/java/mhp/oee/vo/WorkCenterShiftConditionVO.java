package mhp.oee.vo;


public class WorkCenterShiftConditionVO {

    private String shift;
    private String description;
    private String startTime;
    private String endTime;

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public int hashCode() {
        return shift.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof WorkCenterShiftConditionVO) {
            WorkCenterShiftConditionVO vo = (WorkCenterShiftConditionVO) obj;
            return (shift.equals(vo.shift));
        }
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "WorkCenterShiftConditionVO [shift=" + shift + ", description=" + description + "]";
    }
}