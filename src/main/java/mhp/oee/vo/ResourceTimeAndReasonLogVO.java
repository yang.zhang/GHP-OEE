package mhp.oee.vo;

import java.sql.Timestamp;

public class ResourceTimeAndReasonLogVO extends AbstractVO {

    private String site;
    private String resrce;
    private String resourceState;
    private Timestamp startDateTime;
    private Timestamp endDateTime;
    private String activity;
    private int elapsedTime;
    private String reasonCodeBo;
    private Timestamp createDateTime;
    private Timestamp modifiedDateTime;
    private Timestamp dateTime;
    private String reasonActivity;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public String getResourceState() {
        return resourceState;
    }

    public void setResourceState(String resourceState) {
        this.resourceState = resourceState;
    }

    public Timestamp getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Timestamp startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Timestamp getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Timestamp endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public int getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(int elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public String getReasonCodeBo() {
        return reasonCodeBo;
    }

    public void setReasonCodeBo(String reasonCodeBo) {
        this.reasonCodeBo = reasonCodeBo;
    }

    public Timestamp getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(Timestamp createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Timestamp getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Timestamp modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public String getReasonActivity() {
        return reasonActivity;
    }

    public void setReasonActivity(String reasonActivity) {
        this.reasonActivity = reasonActivity;
    }

}
