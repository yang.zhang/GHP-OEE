package mhp.oee.vo;

import java.util.List;

public class PlcInfoConditionVO {

    private WorkCenterVO workCenterVO;
    private List<PlcInfoLineVO> plcInfoLineVOs;
    private String site;
    private String workCenter;
    private String line;
    private String resourceType;
    private String[] resource;
    private String hasResource;
    private int pageIndex;
    private int pageCount;
    private int limit;
    private int offset;

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getHasResource() {
        return hasResource;
    }

    public void setHasResource(String hasResource) {
        this.hasResource = hasResource;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public WorkCenterVO getWorkCenterVO() {
        return workCenterVO;
    }

    public void setWorkCenterVO(WorkCenterVO workCenterVO) {
        this.workCenterVO = workCenterVO;
    }

    public List<PlcInfoLineVO> getPlcInfoLineVOs() {
        return plcInfoLineVOs;
    }

    public void setPlcInfoLineVOs(List<PlcInfoLineVO> plcInfoLineVOs) {
        this.plcInfoLineVOs = plcInfoLineVOs;
    }

    public String getWorkCenter() {
        return workCenter;
    }

    public void setWorkCenter(String workCenter) {
        this.workCenter = workCenter;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String[] getResource() {
        return resource;
    }

    public void setResource(String[] resource) {
        this.resource = resource;
    }
}
