package mhp.oee.vo;

import java.math.BigDecimal;

/**
 *  Add by SuMingzhi on 2016/12/20
 */
public class OeeOeeTrendVO{
    private String byTime;
    private String shiftStartTime;
	private String byTimeChart;
	private String byTimeMonth;
	private String byTimeYear;
	private String a;
    private String p;
    private String q;
    private String oee;
    private BigDecimal oeeValue;
    private BigDecimal qty;
    private String model;
    private String workArea;
    private String workCenter;
    private String resrceType;
    private String resrce;
    private String asset;
    private String resrceDescription;
    private BigDecimal ppmTherory;
    private String item;
    private String userId;

    public String getByTimeYear() {
    	return byTimeYear;
    }
    
    public void setByTimeYear(String byTimeYear) {
    	this.byTimeYear = byTimeYear;
    }
    public String getByTimeMonth() {
    	return byTimeMonth;
    }
    
    public void setByTimeMonth(String byTimeMonth) {
    	this.byTimeMonth = byTimeMonth;
    }
    
    public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getShiftStartTime() {
    	return shiftStartTime;
    }
    
    public void setShiftStartTime(String shiftStartTime) {
    	this.shiftStartTime = shiftStartTime;
    }
    public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getWorkArea() {
		return workArea;
	}

	public void setWorkArea(String workArea) {
		this.workArea = workArea;
	}

	public String getWorkCenter() {
		return workCenter;
	}

	public void setWorkCenter(String workCenter) {
		this.workCenter = workCenter;
	}

	public String getResrceType() {
		return resrceType;
	}

	public void setResrceType(String resrceType) {
		this.resrceType = resrceType;
	}

	public String getResrce() {
		return resrce;
	}

	public void setResrce(String resrce) {
		this.resrce = resrce;
	}

	public String getAsset() {
		return asset;
	}

	public void setAsset(String asset) {
		this.asset = asset;
	}

	public String getResrceDescription() {
		return resrceDescription;
	}

	public void setResrceDescription(String resrceDescription) {
		this.resrceDescription = resrceDescription;
	}

	public BigDecimal getPpmTherory() {
		return ppmTherory;
	}

	public void setPpmTherory(BigDecimal ppmTherory) {
		this.ppmTherory = ppmTherory;
	}

	public String getByTime() {
        return byTime;
    }

    public void setByTime(String byTime) {
        this.byTime = byTime;
    }

    public String getByTimeChart() {
        return byTimeChart;
    }

    public void setByTimeChart(String byTimeChart) {
        this.byTimeChart = byTimeChart;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public String getOee() {
        return oee;
    }

    public void setOee(String oee) {
        this.oee = oee;
    }

    public BigDecimal getOeeValue() {
        return oeeValue;
    }

    public void setOeeValue(BigDecimal oeeValue) {
        this.oeeValue = oeeValue;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }
}
