package mhp.oee.vo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zhangyun
 * @E-mail: ZhangYun@ATLBattery.com
 */
public class RealtimePpmVO {
    private String resourceBo;
    private Date dateTime;
    private BigDecimal realtimePpm;
    private Date createDateTime;
    private Date modifiedDateTime;
    private String modifiedUser;
    private Character viewActionCode;

    public String getResourceBo() {
        return resourceBo;
    }

    public void setResourceBo(String resourceBo) {
        this.resourceBo = resourceBo;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public BigDecimal getRealtimePpm() {
        return realtimePpm;
    }

    public void setRealtimePpm(BigDecimal realtimePpm) {
        this.realtimePpm = realtimePpm;
    }

    public Date getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

}
