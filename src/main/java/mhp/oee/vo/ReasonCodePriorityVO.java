package mhp.oee.vo;

import java.util.Date;

public class ReasonCodePriorityVO {
    private String handle;
    private String reasonCodeBo;
    private String priority;
    private String subPriority;
    private String strt;
    private String inStartDateTime;
    private Date startDateTime;
    private Date endDateTime;
    private String used;
    private Date firstModifiedDateTime;
    private Date createdDateTime;
    private Date modifiedDateTime;
    private String modifiedUser;
    private Character viewActionCode;

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getReasonCodeBo() {
        return reasonCodeBo;
    }

    public void setReasonCodeBo(String reasonCodeBo) {
        this.reasonCodeBo = reasonCodeBo;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getSubPriority() {
        return subPriority;
    }

    public void setSubPriority(String subPriority) {
        this.subPriority = subPriority;
    }

    public String getStrt() {
        return strt;
    }

    public void setStrt(String strt) {
        this.strt = strt;
    }

    public String getInStartDateTime() {
        return inStartDateTime;
    }

    public void setInStartDateTime(String inStartDateTime) {
        this.inStartDateTime = inStartDateTime;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }

    public Date getFirstModifiedDateTime() {
        return firstModifiedDateTime;
    }

    public void setFirstModifiedDateTime(Date firstModifiedDateTime) {
        this.firstModifiedDateTime = firstModifiedDateTime;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

}
