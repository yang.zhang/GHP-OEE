package mhp.oee.vo;

public class ResrceAndWorkCenterAndWorkCenterShiftVO {
    private String site;
    private String resrce;
    private String workCenter;
    private String shift;
    private String shiftDes;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public String getWorkCenter() {
        return workCenter;
    }

    public void setWorkCenter(String workCenter) {
        this.workCenter = workCenter;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getShiftDes() {
        return shiftDes;
    }

    public void setShiftDes(String shiftDes) {
        this.shiftDes = shiftDes;
    }
}
