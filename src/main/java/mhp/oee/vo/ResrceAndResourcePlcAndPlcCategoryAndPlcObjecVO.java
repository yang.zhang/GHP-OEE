package mhp.oee.vo;

import java.util.ArrayList;
import java.util.List;

import mhp.oee.po.extend.ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPO;

public class ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO extends ResrceAndResourcePlcAndPlcCategoryAndPlcObjecPO  implements Comparable<ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO>{
    private Character viewActionCode;
    private int index;
    private int count;

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public int compareTo(ResrceAndResourcePlcAndPlcCategoryAndPlcObjecVO o) {
        List<Character> orderList = new ArrayList<Character>();
        orderList.add('D');  
        orderList.add('U');  
        orderList.add('C');  
        int io1 = orderList.indexOf(this.viewActionCode);
        int io2 = orderList.indexOf(o.getViewActionCode());
        return io1 - io2;
    }
}
