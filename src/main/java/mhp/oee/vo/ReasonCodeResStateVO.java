package mhp.oee.vo;

import java.util.Date;

public class ReasonCodeResStateVO {

    private String handle;
    private String reasonCodeBo;
    private String resourceStateBo;
    private String strt;
    private Date startDateTime;
    private Date endDateTime;
    private Character viewActionCode;
    private String startDateTimeIn;
    private Date modifiedDateTime;

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getReasonCodeBo() {
        return reasonCodeBo;
    }

    public void setReasonCodeBo(String reasonCodeBo) {
        this.reasonCodeBo = reasonCodeBo;
    }

    public String getResourceStateBo() {
        return resourceStateBo;
    }

    public void setResourceStateBo(String resourceStateBo) {
        this.resourceStateBo = resourceStateBo;
    }

    public String getStrt() {
        return strt;
    }

    public void setStrt(String strt) {
        this.strt = strt;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

    public String getStartDateTimeIn() {
        return startDateTimeIn;
    }

    public void setStartDateTimeIn(String startDateTimeIn) {
        this.startDateTimeIn = startDateTimeIn;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }
}