package mhp.oee.vo;

public class ProductionOutputFirstAndFollowUpVO {
    
    private String workArea;
    private String workAreaDes;
    private String lineArea;
    private String lineAreaDes;
    private String resrce;
    private String resrceDes;
    private String model;
    private String item; 
    private String itemDes;
    private String byTime;
    /**
     * gridQtySum,gridQtyYield,gridQtyScrap是前台grid显示的字段,
     * qtySum,qtyYield,qtyScrap用户echart显示。
     * 为了不影响echart数据显示新增加了gridQtySum,gridQtyYield,gridQtyScrap三个字段
     */
    private String gridQtySum;
    private String gridQtyYield;
    private String gridQtyScrap;
    private String qtySum;
    private String qtyYield;
    private String qtyScrap;
    private String qtyUnconfirm;
    private String firstCal;
    private String qtyYieldResult;
    private String qtyScrapResult;
    private String resultCal;
    private String resultCalStrt;
    private String firstCalStrt;
    
    public String getWorkArea() {
        return workArea;
    }
    public void setWorkArea(String workArea) {
        this.workArea = workArea;
    }
    public String getWorkAreaDes() {
        return workAreaDes;
    }
    public void setWorkAreaDes(String workAreaDes) {
        this.workAreaDes = workAreaDes;
    }
    public String getLineArea() {
        return lineArea;
    }
    public void setLineArea(String lineArea) {
        this.lineArea = lineArea;
    }
    public String getLineAreaDes() {
        return lineAreaDes;
    }
    public void setLineAreaDes(String lineAreaDes) {
        this.lineAreaDes = lineAreaDes;
    }
    public String getResrce() {
        return resrce;
    }
    public void setResrce(String resrce) {
        this.resrce = resrce;
    }
    public String getResrceDes() {
        return resrceDes;
    }
    public void setResrceDes(String resrceDes) {
        this.resrceDes = resrceDes;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public String getItem() {
        return item;
    }
    public void setItem(String item) {
        this.item = item;
    }
    public String getItemDes() {
        return itemDes;
    }
    public void setItemDes(String itemDes) {
        this.itemDes = itemDes;
    }
    public String getByTime() {
        return byTime;
    }
    public void setByTime(String byTime) {
        this.byTime = byTime;
    }
    public String getQtySum() {
        return qtySum;
    }
    public void setQtySum(String qtySum) {
        this.qtySum = qtySum;
    }
    public String getQtyYield() {
        return qtyYield;
    }
    public void setQtyYield(String qtyYield) {
        this.qtyYield = qtyYield;
    }
    public String getQtyScrap() {
        return qtyScrap;
    }
    public void setQtyScrap(String qtyScrap) {
        this.qtyScrap = qtyScrap;
    }
    public String getQtyUnconfirm() {
        return qtyUnconfirm;
    }
    public void setQtyUnconfirm(String qtyUnconfirm) {
        this.qtyUnconfirm = qtyUnconfirm;
    }
    public String getFirstCal() {
        return firstCal;
    }
    public void setFirstCal(String firstCal) {
        this.firstCal = firstCal;
    }
    public String getQtyYieldResult() {
        return qtyYieldResult;
    }
    public void setQtyYieldResult(String qtyYieldResult) {
        this.qtyYieldResult = qtyYieldResult;
    }
    public String getQtyScrapResult() {
        return qtyScrapResult;
    }
    public void setQtyScrapResult(String qtyScrapResult) {
        this.qtyScrapResult = qtyScrapResult;
    }
    public String getResultCal() {
        return resultCal;
    }
    public void setResultCal(String resultCal) {
        this.resultCal = resultCal;
    }
    public String getResultCalStrt() {
        return resultCalStrt;
    }
    public void setResultCalStrt(String resultCalStrt) {
        this.resultCalStrt = resultCalStrt;
    }
    public String getFirstCalStrt() {
        return firstCalStrt;
    }
    public void setFirstCalStrt(String firstCalStrt) {
        this.firstCalStrt = firstCalStrt;
    }
    public String getGridQtySum() {
        return gridQtySum;
    }
    public void setGridQtySum(String gridQtySum) {
        this.gridQtySum = gridQtySum;
    }
    public String getGridQtyYield() {
        return gridQtyYield;
    }
    public void setGridQtyYield(String gridQtyYield) {
        this.gridQtyYield = gridQtyYield;
    }
    public String getGridQtyScrap() {
        return gridQtyScrap;
    }
    public void setGridQtyScrap(String gridQtyScrap) {
        this.gridQtyScrap = gridQtyScrap;
    }
}
