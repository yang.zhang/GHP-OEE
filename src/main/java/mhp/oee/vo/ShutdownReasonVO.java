package mhp.oee.vo;


public class ShutdownReasonVO {
	
	private String byTime;
	private String resourceTypeCode;
	private String resourceTypeDescription;
	private String workCenterCode;				//by workCenter 生产区域编码
	private String workCenterDescription;		//by workCenter	生产区域描述
	private String lineAreaCode;				//by lineArea 拉线编码
	private String lineAreaDescription;			//by lineArea	拉线描述
	private String asset;					//资产号
	private String resrceCode;					//by workCenter 设备编码
	private String resrceDescription;			//by workCenter	设备描述
	private String reasonCode;
	private String reasonCodeDescription;
	private Double totalTime;
	private String strTotalTime;               //时长，用于前端显示
	private Double totaPproportionNum;
	private String strTotaPproportionNum;       //总时长占比
	private Double dtProportionNum;
	private String strDtProportionNum;          //DT占比
	private String totaPproportionStr;
	private String dtProportionStr;
	private String[] reasonCodeDescriptions;	//原因代码描述（Echarts 数组格式）
	private Double[] totaPproportions;			//总时长占比（Echarts 数组格式）
	private String[] strTotaPproportions;
	private Double[] dtProportions;				//DT 时长占比（Echarts 数组格式）
	private String[] strDtProportions;
	private Double shutdownTimes;				//停机次数
	
	public String getByTime() {
		return byTime;
	}
	public void setByTime(String byTime) {
		this.byTime = byTime;
	}
	public String getReasonCodeDescription() {
		return reasonCodeDescription;
	}
	public void setReasonCodeDescription(String reasonCodeDescription) {
		this.reasonCodeDescription = reasonCodeDescription;
	}
	public Double getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(Double totalTime) {
		this.totalTime = totalTime;
	}
	public Double getTotaPproportionNum() {
		return totaPproportionNum;
	}
	public void setTotaPproportionNum(Double totaPproportionNum) {
		this.totaPproportionNum = totaPproportionNum;
	}
	public Double getDtProportionNum() {
		return dtProportionNum;
	}
	public void setDtProportionNum(Double dtProportionNum) {
		this.dtProportionNum = dtProportionNum;
	}
	public String getTotaPproportionStr() {
		return totaPproportionStr;
	}
	public void setTotaPproportionStr(String totaPproportionStr) {
		this.totaPproportionStr = totaPproportionStr;
	}
	public String getDtProportionStr() {
		return dtProportionStr;
	}
	public void setDtProportionStr(String dtProportionStr) {
		this.dtProportionStr = dtProportionStr;
	}
	public String getResourceTypeDescription() {
		return resourceTypeDescription;
	}
	public void setResourceTypeDescription(String resourceTypeDescription) {
		this.resourceTypeDescription = resourceTypeDescription;
	}
	public String getResourceTypeCode() {
		return resourceTypeCode;
	}
	public void setResourceTypeCode(String resourceTypeCode) {
		this.resourceTypeCode = resourceTypeCode;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String[] getReasonCodeDescriptions() {
		return reasonCodeDescriptions;
	}
	public void setReasonCodeDescriptions(String[] reasonCodeDescriptions) {
		this.reasonCodeDescriptions = reasonCodeDescriptions;
	}
	public Double[] getTotaPproportions() {
		return totaPproportions;
	}
	public void setTotaPproportions(Double[] totaPproportions) {
		this.totaPproportions = totaPproportions;
	}
	public Double[] getDtProportions() {
		return dtProportions;
	}
	public void setDtProportions(Double[] dtProportions) {
		this.dtProportions = dtProportions;
	}
	public String getWorkCenterCode() {
		return workCenterCode;
	}
	public void setWorkCenterCode(String workCenterCode) {
		this.workCenterCode = workCenterCode;
	}
	public String getWorkCenterDescription() {
		return workCenterDescription;
	}
	public void setWorkCenterDescription(String workCenterDescription) {
		this.workCenterDescription = workCenterDescription;
	}
	public String getLineAreaCode() {
		return lineAreaCode;
	}
	public void setLineAreaCode(String lineAreaCode) {
		this.lineAreaCode = lineAreaCode;
	}
	public String getLineAreaDescription() {
		return lineAreaDescription;
	}
	public void setLineAreaDescription(String lineAreaDescription) {
		this.lineAreaDescription = lineAreaDescription;
	}
	public String getResrceCode() {
		return resrceCode;
	}
	public void setResrceCode(String resrceCode) {
		this.resrceCode = resrceCode;
	}
	public String getResrceDescription() {
		return resrceDescription;
	}
	public void setResrceDescription(String resrceDescription) {
		this.resrceDescription = resrceDescription;
	}
	public String getAsset() {
		return asset;
	}
	public void setAsset(String asset) {
		this.asset = asset;
	}
	public Double getShutdownTimes() {
		return shutdownTimes;
	}
	public void setShutdownTimes(Double shutdownTimes) {
		this.shutdownTimes = shutdownTimes;
	}
    public String getStrTotalTime() {
        return strTotalTime;
    }
    public void setStrTotalTime(String strTotalTime) {
        this.strTotalTime = strTotalTime;
    }
    public String getStrTotaPproportionNum() {
        return strTotaPproportionNum;
    }
    public void setStrTotaPproportionNum(String strTotaPproportionNum) {
        this.strTotaPproportionNum = strTotaPproportionNum;
    }
    public String getStrDtProportionNum() {
        return strDtProportionNum;
    }
    public void setStrDtProportionNum(String strDtProportionNum) {
        this.strDtProportionNum = strDtProportionNum;
    }
    public String[] getStrDtProportions() {
        return strDtProportions;
    }
    public void setStrDtProportions(String[] strDtProportions) {
        this.strDtProportions = strDtProportions;
    }
    public String[] getStrTotaPproportions() {
        return strTotaPproportions;
    }
    public void setStrTotaPproportions(String[] strTotaPproportions) {
        this.strTotaPproportions = strTotaPproportions;
    }
    
}
