package mhp.oee.vo;

import java.util.Date;

public class ActivityParamVO {
    private String handle;
    private String activity;
    private String activityBo;
    private String paramId;
    private String paramDescription;
    private String paramDefaultValue;
    private String paramSetValue;
    private Date modifiedDateTime;
    private Date firstModifiedDateTime;
    private Character viewActionCode;

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getActivityBo() {
        return activityBo;
    }

    public void setActivityBo(String activityBo) {
        this.activityBo = activityBo;
    }

    public Date getFirstModifiedDateTime() {
        return firstModifiedDateTime;
    }

    public void setFirstModifiedDateTime(Date firstModifiedDateTime) {
        this.firstModifiedDateTime = firstModifiedDateTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getParamId() {
        return paramId;
    }

    public void setParamId(String paramId) {
        this.paramId = paramId;
    }

    public String getParamDescription() {
        return paramDescription;
    }

    public void setParamDescription(String paramDescription) {
        this.paramDescription = paramDescription;
    }

    public String getParamDefaultValue() {
        return paramDefaultValue;
    }

    public void setParamDefaultValue(String paramDefaultValue) {
        this.paramDefaultValue = paramDefaultValue;
    }

    public String getParamSetValue() {
        return paramSetValue;
    }

    public void setParamSetValue(String paramSetValue) {
        this.paramSetValue = paramSetValue;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

}