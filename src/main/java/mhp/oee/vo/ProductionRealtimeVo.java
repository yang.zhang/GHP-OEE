package mhp.oee.vo;


public class ProductionRealtimeVo {
	
	
	
	private String columnTime;				//用于展示的列名
	private String startTimeForCompare;		//列的开始时间	
	private String endTimeForCompare;		//列的结束时间
	private int startDateAddDay;			//开始日期是否加一天
	private int endDateAddDay;				//结束日期是否加一天
	private int deNum;						//除数的一半
	
	
	
	
	
	public String getStartTimeForCompare() {
		return startTimeForCompare;
	}
	public void setStartTimeForCompare(String startTimeForCompare) {
		this.startTimeForCompare = startTimeForCompare;
	}
	public String getEndTimeForCompare() {
		return endTimeForCompare;
	}
	public void setEndTimeForCompare(String endTimeForCompare) {
		this.endTimeForCompare = endTimeForCompare;
	}
	public String getColumnTime() {
		return columnTime;
	}
	public void setColumnTime(String columnTime) {
		this.columnTime = columnTime;
	}
	public int getStartDateAddDay() {
		return startDateAddDay;
	}
	public void setStartDateAddDay(int startDateAddDay) {
		this.startDateAddDay = startDateAddDay;
	}
	public int getEndDateAddDay() {
		return endDateAddDay;
	}
	public void setEndDateAddDay(int endDateAddDay) {
		this.endDateAddDay = endDateAddDay;
	}
	public int getDeNum() {
		return deNum;
	}
	public void setDeNum(int deNum) {
		this.deNum = deNum;
	}
	
}
