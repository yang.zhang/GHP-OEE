package mhp.oee.vo;

import java.util.Date;

/**
 * Created by LinZuK on 2016/7/29.
 */
public class ResourceTypeAndActivityParamValueVO {
    private String reasonCodeRuleParamBo;
    private Date reasonCodeRuleParamModifiedDateTime;
    private String resourceType;
    private String activityFParamValue;

    public Date getReasonCodeRuleParamModifiedDateTime() {
        return reasonCodeRuleParamModifiedDateTime;
    }

    public void setReasonCodeRuleParamModifiedDateTime(Date reasonCodeRuleParamModifiedDateTime) {
        this.reasonCodeRuleParamModifiedDateTime = reasonCodeRuleParamModifiedDateTime;
    }

    public String getReasonCodeRuleParamBo() {
        return reasonCodeRuleParamBo;
    }

    public void setReasonCodeRuleParamBo(String reasonCodeRuleParamBo) {
        this.reasonCodeRuleParamBo = reasonCodeRuleParamBo;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getActivityFParamValue() {
        return activityFParamValue;
    }

    public void setActivityFParamValue(String activityFParamValue) {
        this.activityFParamValue = activityFParamValue;
    }

}
