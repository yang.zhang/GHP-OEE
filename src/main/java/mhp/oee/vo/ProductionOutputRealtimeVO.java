package mhp.oee.vo;

import java.math.BigDecimal;
import java.util.Date;

public class ProductionOutputRealtimeVO {

    private String site;
    private String resrce;
    private String item;
    private Date startDateTime;
    private Date endDateTime;
    private BigDecimal qtyYield;
    private BigDecimal qtyScrap;
    private BigDecimal qtyInput;
    private BigDecimal qtyTotal;
    private Date modifiedDateTime;
    private Character viewActionCode;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResrce() {
        return resrce;
    }

    public void setResrce(String resrce) {
        this.resrce = resrce;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public BigDecimal getQtyYield() {
        return qtyYield;
    }

    public void setQtyYield(BigDecimal qtyYield) {
        this.qtyYield = qtyYield;
    }

    public BigDecimal getQtyScrap() {
        return qtyScrap;
    }

    public void setQtyScrap(BigDecimal qtyScrap) {
        this.qtyScrap = qtyScrap;
    }

    public BigDecimal getQtyInput() {
        return qtyInput;
    }

    public void setQtyInput(BigDecimal qtyInput) {
        this.qtyInput = qtyInput;
    }

    public BigDecimal getQtyTotal() {
        return qtyTotal;
    }

    public void setQtyTotal(BigDecimal qtyTotal) {
        this.qtyTotal = qtyTotal;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

}