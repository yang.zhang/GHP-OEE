package mhp.oee.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActivityGroupVO {
    private String activityGroup;
    private Character viewActionCode;
    private String handle;
    private String description;
    private Date createdDateTime;
    private Date modifiedDateTime;
    private Date firstModifiedDateTime;
    private String modifiedUser;

    private List<ActivityVO> activityVOs = new ArrayList<ActivityVO>();

    public String getActivityGroup() {
        return activityGroup;
    }

    public void setActivityGroup(String activityGroup) {
        this.activityGroup = activityGroup;
    }

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    public Date getFirstModifiedDateTime() {
        return firstModifiedDateTime;
    }

    public void setFirstModifiedDateTime(Date firstModifiedDateTime) {
        this.firstModifiedDateTime = firstModifiedDateTime;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((handle == null) ? 0 : handle.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ActivityGroupVO other = (ActivityGroupVO) obj;
        if (handle == null) {
            if (other.handle != null)
                return false;
        } else if (!handle.equals(other.handle))
            return false;
        return true;
    }

    public List<ActivityVO> getActivityVOs() {
        return activityVOs;
    }

    public void setActivityVOs(List<ActivityVO> activityVOs) {
        this.activityVOs = activityVOs;
    }

}