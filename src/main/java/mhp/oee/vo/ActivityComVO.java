package mhp.oee.vo;

import java.util.List;

public class ActivityComVO {
    private String activityGroup;
    private List<ActivityVO> avaliableActivityVos;
    private List<ActivityVO> assigendActivityVos;

    public String getActivityGroup() {
        return activityGroup;
    }

    public void setActivityGroup(String activityGroup) {
        this.activityGroup = activityGroup;
    }

    public List<ActivityVO> getAvaliableActivityVos() {
        return avaliableActivityVos;
    }

    public void setAvaliableActivityVos(List<ActivityVO> avaliableActivityVos) {
        this.avaliableActivityVos = avaliableActivityVos;
    }

    public List<ActivityVO> getAssigendActivityVos() {
        return assigendActivityVos;
    }

    public void setAssigendActivityVos(List<ActivityVO> assigendActivityVos) {
        this.assigendActivityVos = assigendActivityVos;
    }

}
