package mhp.oee.vo;

import mhp.oee.po.extend.FilterParamButtonPO;

import java.util.List;

/**
 * Created by LinZuK on 2016/8/10.
 */
public class FilterParamVO {
    private String activityFParamId;
    private String activityFParamDesc;
    private List<FilterParamButtonPO> params;

    public String getActivityFParamId() {
        return activityFParamId;
    }

    public void setActivityFParamId(String activityFParamId) {
        this.activityFParamId = activityFParamId;
    }

    public String getActivityFParamDesc() {
        return activityFParamDesc;
    }

    public void setActivityFParamDesc(String activityFParamDesc) {
        this.activityFParamDesc = activityFParamDesc;
    }

    public List<FilterParamButtonPO> getParams() {
        return params;
    }

    public void setParams(List<FilterParamButtonPO> params) {
        this.params = params;
    }
}
