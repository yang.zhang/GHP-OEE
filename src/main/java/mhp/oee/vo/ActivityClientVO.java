package mhp.oee.vo;

import java.util.Date;

public class ActivityClientVO extends AbstractVO {

    private String handle;
    private String activityClient;
    private String description;
    private String type;
    private String visible;
    private Date modifiedDateTime;
    private Date firstModifiedDateTime;
    private Character viewActionCode;
    private String activityClientBo;

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getActivityClient() {
        return activityClient;
    }

    public void setActivityClient(String activityClient) {
        this.activityClient = activityClient;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public Date getFirstModifiedDateTime() {
        return firstModifiedDateTime;
    }

    public void setFirstModifiedDateTime(Date firstModifiedDateTime) {
        this.firstModifiedDateTime = firstModifiedDateTime;
    }

    public String getActivityClientBo() {
        return activityClientBo;
    }

    public void setActivityClientBo(String activityClientBo) {
        this.activityClientBo = activityClientBo;
    }
}
