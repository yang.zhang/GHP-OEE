package mhp.oee.vo;

import mhp.oee.po.gen.AlertCommentPO;

public class AlertComentManagerVO extends AlertCommentPO{

    private Character viewActionCode;

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }
}