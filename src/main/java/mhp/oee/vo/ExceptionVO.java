package mhp.oee.vo;

public class ExceptionVO {
    
    private int code;
    private String message;
    private String errorJson;
    
    public ExceptionVO(){
       super(); 
    }
    
    public ExceptionVO(int code, String message, String errorJson){
        super(); 
        this.code = code;
        this.message = message;
        this.errorJson = errorJson;
    }
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorJson() {
        return errorJson;
    }

    public void setErrorJson(String errorJson) {
        this.errorJson = errorJson;
    }
}
