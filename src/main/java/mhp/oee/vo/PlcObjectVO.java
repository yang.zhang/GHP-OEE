package mhp.oee.vo;

import java.util.Date;

public class PlcObjectVO {

    private String handle;
    private String site;
    private String plcObject;
    private String category;
    private String plcObjectDescription;
    private Date modifiedDateTime;
    private Character viewActionCode;

    public String getHandle() {
        return handle;
    }
    public void setHandle(String handle) {
        this.handle = handle;
    }
    public String getSite() {
        return site;
    }
    public void setSite(String site) {
        this.site = site;
    }
    public String getPlcObject() {
        return plcObject;
    }
    public void setPlcObject(String plcObject) {
        this.plcObject = plcObject;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getPlcObjectDescription() {
        return plcObjectDescription;
    }
    public void setPlcObjectDescription(String plcObjectDescription) {
        this.plcObjectDescription = plcObjectDescription;
    }
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }
    public Character getViewActionCode() {
        return viewActionCode;
    }
    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

}