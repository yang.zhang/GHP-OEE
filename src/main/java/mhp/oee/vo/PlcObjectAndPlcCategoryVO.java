package mhp.oee.vo;

import mhp.oee.po.extend.PlcObjectAndPlcCategoryPO;

public class PlcObjectAndPlcCategoryVO extends PlcObjectAndPlcCategoryPO {
    
    private Character viewActionCode;

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }
    
}