package mhp.oee.vo;

import mhp.oee.po.gen.ResourceCurrentDataPO;

public class ResourceCurrentDataVO extends ResourceCurrentDataPO {
    private String resource;
    private String resourceName;
    private String itemName;
    private String operatorName;
    private String userName;
    private String passWord;
    private String code;
    private String msg;
    private String type;

	private Integer module;
	private String site;

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public Integer getModule() {
		return module;
	}

	public void setModule(Integer module) {
		this.module = module;
	}

	public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public static ResourceCurrentDataVO getResourceCurrentDataVO(){
		ResourceCurrentDataVO resourceCurrentDataVO = new ResourceCurrentDataVO();
		resourceCurrentDataVO.setUserName("00071858");
		resourceCurrentDataVO.setPassWord("111");
		return resourceCurrentDataVO;
		
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
