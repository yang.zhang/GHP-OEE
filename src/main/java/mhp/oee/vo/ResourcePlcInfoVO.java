package mhp.oee.vo;

import java.util.Date;

import mhp.oee.po.gen.PlcAccessTypePO;
import mhp.oee.po.gen.ResourcePO;

public class ResourcePlcInfoVO {

    private String handle;
    private String site;
    private String resourceBo;
    private String plcIp;
    private String pcIp;
    private String plcPort;
    private String plcAccessType;
    private Date createdDateTime;
    private Date modifiedDateTime;
    private String modifiedUser;
    private Character viewActionCode;
    private PlcAccessTypePO plcAccessTypePO;
    private ResourcePO resourcePO;

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

    public PlcAccessTypePO getPlcAccessTypePO() {
        return plcAccessTypePO;
    }

    public void setPlcAccessTypePO(PlcAccessTypePO plcAccessTypePO) {
        this.plcAccessTypePO = plcAccessTypePO;
    }

    public ResourcePO getResourcePO() {
        return resourcePO;
    }

    public void setResourcePO(ResourcePO resourcePO) {
        this.resourcePO = resourcePO;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle == null ? null : handle.trim();
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site == null ? null : site.trim();
    }

    public String getResourceBo() {
        return resourceBo;
    }

    public void setResourceBo(String resourceBo) {
        this.resourceBo = resourceBo == null ? null : resourceBo.trim();
    }

    public String getPlcIp() {
        return plcIp;
    }

    public void setPlcIp(String plcIp) {
        this.plcIp = plcIp == null ? null : plcIp.trim();
    }

    public String getPlcPort() {
        return plcPort;
    }

    public void setPlcPort(String plcPort) {
        this.plcPort = plcPort == null ? null : plcPort.trim();
    }

    public String getPlcAccessType() {
        return plcAccessType;
    }

    public void setPlcAccessType(String plcAccessType) {
        this.plcAccessType = plcAccessType == null ? null : plcAccessType.trim();
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }

    public String getPcIp() {
        return pcIp;
    }

    public void setPcIp(String pcIp) {
        this.pcIp = pcIp;
    }
}