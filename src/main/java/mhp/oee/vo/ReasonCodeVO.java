package mhp.oee.vo;

import java.util.Date;

import mhp.oee.po.gen.ReasonCodeGroupPO;

public class ReasonCodeVO {

    private String handle;
    private String site;
    private String reasonCode;
    private String description;
    private String reasonCodeGroup;
    private String reasonCodeGroupDes;
    private String enabled;
    private Date createdDateTime;
    private Date modifiedDateTime;
    private String modifiedUser;
    private Character viewActionCode;
    private ReasonCodeGroupPO reasonCodeGroupPO;
    
    public ReasonCodeGroupPO getReasonCodeGroupPO() {
        return reasonCodeGroupPO;
    }

    public void setReasonCodeGroupPO(ReasonCodeGroupPO reasonCodeGroupPO) {
        this.reasonCodeGroupPO = reasonCodeGroupPO;
    }

    public String getReasonCodeGroupDes() {
        return reasonCodeGroupDes;
    }

    public void setReasonCodeGroupDes(String reasonCodeGroupDes) {
        this.reasonCodeGroupDes = reasonCodeGroupDes;
    }

    public Character getViewActionCode() {
        return viewActionCode;
    }

    public void setViewActionCode(Character viewActionCode) {
        this.viewActionCode = viewActionCode;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle == null ? null : handle.trim();
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site == null ? null : site.trim();
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode == null ? null : reasonCode.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getReasonCodeGroup() {
        return reasonCodeGroup;
    }

    public void setReasonCodeGroup(String reasonCodeGroup) {
        this.reasonCodeGroup = reasonCodeGroup == null ? null : reasonCodeGroup.trim();
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled == null ? null : enabled.trim();
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }
}