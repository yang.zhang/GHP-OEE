======== MAVEN LOCAL REPOSITORY ========

Make sure to run below maven command so as to install 3rd party JARs.

$ mvn install:install-file -Dfile=/path/tc-sec-ume-api-impl-7.3010.jar -DgroupId=com.sap -DartifactId=tc-sec-ume-api-impl -Dversion=7.3010 -Dpackaging=jar
$ mvn install:install-file -Dfile=/path/tc-je-security-impl-7.3010.jar -DgroupId=com.sap -DartifactId=tc-je-security-impl -Dversion=7.3010 -Dpackaging=jar
$ mvn install:install-file -Dfile=/path/ngdbc.jar -DgroupId=com.sap -DartifactId=hana-connector-java -Dversion=1.0 -Dpackaging=jar
$ mvn install:install-file -Dfile=/path/ws4EAP.jar -DgroupId=com.sap -DartifactId=ws4EAP -Dversion=1.0 -Dpackaging=jar
$ mvn install:install-file -Dfile=/path/webservice4andon.jar -DgroupId=com.sap -DartifactId=webservice4andon -Dversion=1.0 -Dpackaging=jar

## webservice 生产环境jar
mvn install:install-file -Dfile=maven_local_repo/prowebservicepandon.jar -DgroupId=com.sap -DartifactId=webservice4andon -Dversion=2.0 -Dpackaging=jar


== Official Reference ==
https://maven.apache.org/guides/mini/guide-3rd-party-jars-local.html
