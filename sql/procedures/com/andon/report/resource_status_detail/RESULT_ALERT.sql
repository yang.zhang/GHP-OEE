
 /********* Begin Procedure Script ************/
 BEGIN
	/*获取一条记录，开始时间和结束时间*/
	selectTemp=SELECT
				ADD_DAYS(startDate,-1) AS STARTDATE,
				endDate AS ENDDATE
	FROM SYS.DUMMY;

	/*获取多条记录----开始时间到结束时间*/
	mulDate=SELECT
			ADD_DAYS(A.STARTDATE, B.ROW_ID) AS STARTDATE,
			ENDDATE
			FROM :selectTemp AS A
			INNER JOIN MHP_OEE.REPORT_INDEX AS B
			ON B.ROW_ID <= DAYS_BETWEEN(A.STARTDATE, A.ENDDATE);

	resrceTable_r = SELECT
					HANDLE,
					SITE,
					RESRCE,
					DESCRIPTION,
					ASSET,
					WORK_AREA,
					LINE_AREA
	FROM MHP_OEE.RESRCE
	WHERE SITE=:site;

	resrceTable_r = APPLY_FILTER(:resrceTable_r, :workArea);

	/*关联设备的三张表和生产区域的两张表*/
	resrceTable = SELECT
					R.HANDLE,
					R.SITE,
					R.RESRCE,
					R.DESCRIPTION AS RESRCE_DESC,
					R.ASSET,
					R.WORK_AREA,
					R.LINE_AREA,
					RTR.RESOURCE_BO,
					RT.RESOURCE_TYPE,
					RT.DESCRIPTION  AS RESOURCE_TYPE_DESCRIPTION,
					RT.ENABLED
	FROM :resrceTable_r AS R
	INNER JOIN MHP_OEE.RESOURCE_TYPE_RESOURCE AS RTR ON RTR.RESOURCE_BO=R.HANDLE
	INNER JOIN MHP_OEE.RESOURCE_TYPE AS RT ON RT.HANDLE=RTR.RESOURCE_TYPE_BO
	WHERE RT.ENABLED='true';



	/*取拉线描述*/
	resrceTable_add_linedesc=SELECT
					A.HANDLE,
					A.SITE,
					A.RESRCE,
					A.RESRCE_DESC,
					A.ASSET,
					A.WORK_AREA,
					A.LINE_AREA,
					B.DESCRIPTION AS LINE_AREA_DESC,
					A.RESOURCE_BO,
					A.RESOURCE_TYPE,
					A.RESOURCE_TYPE_DESCRIPTION
					FROM :resrceTable AS A
					LEFT JOIN MHP_OEE.WORK_CENTER AS B ON B.WORK_CENTER=A.LINE_AREA;

	filter_work_center=SELECT
						WC.HANDLE,
						WC.SITE,
						WC.WORK_CENTER AS WORK_AREA,
						WC.HANDLE AS WORK_CENTER_BO
						from MHP_OEE.WORK_CENTER AS WC
						WHERE  WC.SITE=:site;

	filter_work_center = APPLY_FILTER(:filter_work_center, :workArea);


	/*取班次描述*/
	resrceTable_shift=SELECT
					A.HANDLE,
					A.SITE,
					A.RESRCE,
					A.RESRCE_DESC,
					A.ASSET,
					A.WORK_AREA,
					A.LINE_AREA,
					A.LINE_AREA_DESC,
					/*WC.WORK_CENTER,*/
					WC.HANDLE AS WORK_CENTER_BO,
					WCS.SHIFT,
					WCS.DESCRIPTION AS SHIFT_DESC,
					WCS.SHIFT_DURATION AS SHIFT_DURATION,
					WCS.START_TIME,
					WCS.END_TIME,
					A.RESOURCE_BO,
					A.RESOURCE_TYPE_DESCRIPTION,
					A.RESOURCE_TYPE
					FROM :resrceTable_add_linedesc AS A
	INNER JOIN :filter_work_center AS WC ON WC.SITE=A.SITE AND A.WORK_AREA = WC.WORK_AREA
	INNER JOIN MHP_OEE.WORK_CENTER_SHIFT AS WCS ON WCS.WORK_CENTER_BO=WC.HANDLE AND WCS.ENABLED='true';


	resrceTable_shift = APPLY_FILTER(:resrceTable_shift, :lineArea);
	resrceTable_shift = APPLY_FILTER(:resrceTable_shift, :resourceType);
	resrceTable_shift = APPLY_FILTER(:resrceTable_shift, :resrce);
	resrceTable_shift = APPLY_FILTER(:resrceTable_shift, :shift);


	/*交叉组合多条记录*/
	resrce_shift_start_date_time=SELECT
		A.HANDLE,
		A.SITE,
		A.RESRCE,
		A.RESRCE_DESC,
		A.ASSET,
		A.WORK_AREA,
		A.LINE_AREA,
		A.LINE_AREA_DESC,
		A.WORK_CENTER_BO,
		A.SHIFT,
		A.SHIFT_DESC,
		A.SHIFT_DURATION,
	    CAST( TO_VARCHAR(B.STARTDATE,'YYYY-MM-DD') || ' ' || CAST(A.START_TIME AS NVARCHAR) AS SECONDDATE ) AS SHIFT_START_DATE_TIME,
		A.RESOURCE_BO,
		A.RESOURCE_TYPE,
		A.RESOURCE_TYPE_DESCRIPTION,
		B.STARTDATE
		FROM :resrceTable_shift AS A INNER JOIN :mulDate AS B ON 1=1;

	resrce_shift_end_date_time=SELECT
		A.HANDLE,
		A.SITE,
		A.RESRCE,
		A.RESRCE_DESC,
		A.ASSET,
		A.WORK_AREA,
		A.LINE_AREA,
		A.LINE_AREA_DESC,
		A.WORK_CENTER_BO,
		A.SHIFT,
		A.SHIFT_DESC,
		A.SHIFT_DURATION,
		A.SHIFT_START_DATE_TIME,
		ADD_SECONDS(A.SHIFT_START_DATE_TIME, A.SHIFT_DURATION) AS SHIFT_END_DATE_TIME,
		A.RESOURCE_BO,
		A.RESOURCE_TYPE,
		A.RESOURCE_TYPE_DESCRIPTION,
		A.STARTDATE
		FROM :resrce_shift_start_date_time AS A;

	/*取ME负责人
	var_table = SELECT
				A.HANDLE,
				A.SITE,
				A.RESRCE,
				A.RESRCE_DESC,
				A.ASSET,
				A.WORK_AREA,
				A.LINE_AREA,
				A.LINE_AREA_DESC,
				A.WORK_CENTER_BO,
				A.SHIFT,
				A.SHIFT_DESC,
				A.SHIFT_START_DATE_TIME,
				A.SHIFT_END_DATE_TIME,
				A.RESOURCE_BO,
				A.STARTDATE,
				B.ME_USER_ID
	FROM :yyy AS A
	LEFT JOIN MHP_OEE.RESOURCE_ME_USER AS B ON B.RESOURCE_BO=A.RESOURCE_BO AND B.SHIFT=A.SHIFT
	AND B.ENABLED='true';
	*/
	filter_resrce_shift_end_date_time=SELECT
						A.HANDLE,
						A.SITE,
						A.RESRCE,
						A.RESRCE_DESC,
						A.ASSET,
						A.WORK_AREA,
						A.LINE_AREA,
						A.LINE_AREA_DESC,
						A.WORK_CENTER_BO,
						A.SHIFT,
						A.SHIFT_DESC,
						A.SHIFT_DURATION,
						CASE WHEN :start_date_time>SHIFT_START_DATE_TIME THEN :start_date_time ELSE SHIFT_START_DATE_TIME END AS SHIFT_START_DATE_TIME,
						CASE WHEN :end_date_time<SHIFT_END_DATE_TIME THEN :end_date_time ELSE SHIFT_END_DATE_TIME END AS SHIFT_END_DATE_TIME,
						A.RESOURCE_BO,
						A.RESOURCE_TYPE,
						A.RESOURCE_TYPE_DESCRIPTION,
						A.STARTDATE
						FROM :resrce_shift_end_date_time AS A;

	resrce_time_log = SELECT
				A.HANDLE,
				A.SITE,
				A.RESRCE,
				A.RESRCE_DESC,
				A.RESOURCE_TYPE,
				A.RESOURCE_TYPE_DESCRIPTION,
				A.ASSET,
				A.WORK_AREA,
				A.LINE_AREA,
				A.LINE_AREA_DESC,
				A.WORK_CENTER_BO,
				A.SHIFT,
				A.SHIFT_DESC,
				A.SHIFT_START_DATE_TIME,
				A.SHIFT_END_DATE_TIME,
				A.RESOURCE_BO,
				A.STARTDATE,
				null AS ME_USER_ID,
				B.START_DATE_TIME,
				B.END_DATE_TIME,
				B.ELAPSED_TIME,
				C.STATE AS RESOURCE_STATE,
				C.DESCRIPTION AS RESOURCE_STATE_DESC
	FROM :filter_resrce_shift_end_date_time AS A
	LEFT JOIN MHP_OEE.RESOURCE_TIME_LOG AS B ON B.SITE=A.SITE AND B.RESOURCE_BO=A.RESOURCE_BO
	AND (B.START_DATE_TIME<=A.SHIFT_START_DATE_TIME AND (B.END_DATE_TIME>=A.SHIFT_START_DATE_TIME OR B.END_DATE_TIME is NULL)
	OR (B.START_DATE_TIME>=A.SHIFT_START_DATE_TIME AND B.START_DATE_TIME<=A.SHIFT_END_DATE_TIME))
	LEFT JOIN MHP_OEE.RESOURCE_STATE AS C ON  C.STATE=B.RESOURCE_STATE AND C.SITE=A.SITE
	WHERE A.SHIFT_END_DATE_TIME>A.SHIFT_START_DATE_TIME
	AND (
		B.END_DATE_TIME is not null
		or B.HANDLE  in(
			select HANDLE from(
				select  HANDLE,ROW_NUMBER() over(partition by rtl.RESOURCE_BO order by rtl.START_DATE_TIME desc) as num
				from MHP_OEE.RESOURCE_TIME_LOG rtl
				--where rtl.START_DATE_TIME > '2016-09-05 00:25:00'
				where rtl.END_DATE_TIME is null
				) a where a.num = 1
				--order by a.RESOURCE_BO
		)
	);


	filter_resource_alert_log_before=SELECT
								SITE,
								RESRCE,
								DATE_TIME,
								DESCRIPTION,
								COMMENT_CODE

							  FROM MHP_OEE.RESOURCE_ALERT_LOG
							  WHERE SITE=:site AND DATE_TIME>=:start_date_time AND DATE_TIME<:end_date_time;

	filter_resource_alert_log = APPLY_FILTER(:filter_resource_alert_log_before, :comment_code_in);
	var_resource_state=SELECT
			A.HANDLE,
			A.SITE,
			A.RESRCE,
			A.RESRCE_DESC,
			A.RESOURCE_TYPE,
			A.RESOURCE_TYPE_DESCRIPTION,
			A.ASSET,
			A.LINE_AREA,
			A.LINE_AREA_DESC,
			A.WORK_CENTER_BO,
			A.SHIFT,
			A.SHIFT_DESC,
			A.SHIFT_START_DATE_TIME,
			A.SHIFT_END_DATE_TIME,
			A.RESOURCE_BO,
			A.STARTDATE,
			A.ME_USER_ID,
			A.START_DATE_TIME,
			A.END_DATE_TIME,
			A.RESOURCE_STATE,
			A.ELAPSED_TIME,
			A.RESOURCE_STATE_DESC,
			B.DESCRIPTION AS ALERT_INFO,
			B.DATE_TIME,
			B.COMMENT_CODE
	FROM :resrce_time_log AS A
	INNER JOIN :filter_resource_alert_log AS B ON
	B.SITE=A.SITE AND B.RESRCE=A.RESRCE
	AND B.DATE_TIME>=A.START_DATE_TIME
	AND B.DATE_TIME<A.END_DATE_TIME;


	all_collection=SELECT
			A.STARTDATE,
			A.SHIFT,
			A.SHIFT_DESC,
			A.SHIFT_START_DATE_TIME,
			A.SHIFT_END_DATE_TIME,
			A.RESOURCE_BO,
			A.RESRCE_DESC,
			A.RESOURCE_TYPE,
			A.RESOURCE_TYPE_DESCRIPTION,
			A.ME_USER_ID,
			A.RESRCE,
			A.ASSET,
			A.LINE_AREA_DESC,
			A.START_DATE_TIME,
			A.END_DATE_TIME,
			A.RESOURCE_STATE,
			A.ELAPSED_TIME,
			A.RESOURCE_STATE_DESC,
			A.ALERT_INFO,
			A.DATE_TIME,
			A.COMMENT_CODE
	FROM :var_resource_state AS A;


	 intersection_time =SELECT
	 		A.STARTDATE,
			A.SHIFT,
			A.SHIFT_DESC,
			A.SHIFT_START_DATE_TIME,
			A.SHIFT_END_DATE_TIME,
			A.RESOURCE_BO,
			A.RESRCE_DESC,
			A.RESOURCE_TYPE,
			A.RESOURCE_TYPE_DESCRIPTION,
			A.ME_USER_ID,
			A.RESRCE,
			A.ASSET,
			A.LINE_AREA_DESC,
			A.START_DATE_TIME,
			A.END_DATE_TIME,
			CASE WHEN SHIFT_START_DATE_TIME > START_DATE_TIME THEN SHIFT_START_DATE_TIME ELSE START_DATE_TIME END AS INTERSECTION_START_TIME,
			CASE WHEN SHIFT_END_DATE_TIME < END_DATE_TIME THEN SHIFT_END_DATE_TIME ELSE END_DATE_TIME END AS INTERSECTION_END_TIME,
			A.RESOURCE_STATE,
			A.ELAPSED_TIME,
			A.RESOURCE_STATE_DESC,
			A.ALERT_INFO,
			A.DATE_TIME,
			A.COMMENT_CODE
	 FROM :all_collection AS A;


	 intersection_time_with_fu = SELECT
	 		A.STARTDATE,
			A.SHIFT,
			A.SHIFT_DESC,
			A.SHIFT_START_DATE_TIME,
			A.SHIFT_END_DATE_TIME,
			A.RESOURCE_BO,
			A.RESRCE_DESC,
			A.RESOURCE_TYPE,
			A.RESOURCE_TYPE_DESCRIPTION,
			A.ME_USER_ID,
			A.RESRCE,
			A.ASSET,
			A.LINE_AREA_DESC,
			A.START_DATE_TIME,
			A.END_DATE_TIME,
			A.INTERSECTION_START_TIME,
			A.INTERSECTION_END_TIME,
			SECONDS_BETWEEN(A.INTERSECTION_START_TIME, A.INTERSECTION_END_TIME) AS INTERSECTION_TIME_DURATION,
			A.RESOURCE_STATE,
			A.ELAPSED_TIME,
			A.RESOURCE_STATE_DESC,
			A.ALERT_INFO,
			DATE_TIME,
			A.COMMENT_CODE
			FROM :intersection_time AS A;


	intersection_time_without_fu = SELECT
			row_number() over() as ROW_ID,
	 		A.STARTDATE,
			A.SHIFT,
			A.SHIFT_DESC,
			A.RESOURCE_BO,
			A.RESRCE_DESC,
			A.RESOURCE_TYPE,
			A.RESOURCE_TYPE_DESCRIPTION,
			A.ME_USER_ID,
			A.RESRCE,
			A.ASSET,
			A.LINE_AREA_DESC,
			A.START_DATE_TIME,
			A.END_DATE_TIME,
			A.INTERSECTION_START_TIME,
			A.INTERSECTION_END_TIME,
			A.INTERSECTION_TIME_DURATION,
			A.RESOURCE_STATE,
			A.ELAPSED_TIME,
			A.RESOURCE_STATE_DESC,
			A.ALERT_INFO,
			A.DATE_TIME,
			A.COMMENT_CODE
			FROM :intersection_time_with_fu AS A
			WHERE A.INTERSECTION_TIME_DURATION >= 0;


			var_between_date=SELECT
					A.HANDLE,
					A.RESOURCE_BO,
					A.USER_BO,
					A.LOGIN_DATE_TIME,
					A.LOGIN_MODE,
					A.ACTION_CODE,
					B.INTERSECTION_START_TIME,
					B.INTERSECTION_END_TIME,
					B.COMMENT_CODE
					FROM MHP_OEE.CLIENT_LOGIN_LOG AS A
					INNER JOIN :intersection_time_without_fu  AS B ON
					A.RESOURCE_BO=B.RESOURCE_BO
					 WHERE A.LOGIN_MODE='ONSITE'
					 AND A.USER_DEPARTMENT = 'PRD'
					 AND A.LOGIN_DATE_TIME>B.INTERSECTION_START_TIME
					 AND A.LOGIN_DATE_TIME<B.INTERSECTION_END_TIME;

	  /*取PRD操作员*/
	 var_prd_between=SELECT
	 			A.ROW_ID,
				A.STARTDATE,
				A.SHIFT,
				A.SHIFT_DESC,
				A.RESRCE_DESC,
				A.RESOURCE_TYPE,
				A.RESOURCE_TYPE_DESCRIPTION,
				A.ME_USER_ID,
				A.RESRCE,
				A.ASSET,
				A.LINE_AREA_DESC,
				A.START_DATE_TIME,
				A.END_DATE_TIME,
				A.INTERSECTION_START_TIME,
				A.INTERSECTION_END_TIME,
				A.INTERSECTION_TIME_DURATION,
				A.RESOURCE_BO,
				A.ELAPSED_TIME,
				A.RESOURCE_STATE,
				A.RESOURCE_STATE_DESC,
				D.USER_ID,
				null as USER_BO,
				B.LOGIN_DATE_TIME,
				A.ALERT_INFO,
				A.DATE_TIME,
				A.COMMENT_CODE
				FROM :intersection_time_without_fu AS A
	 			LEFT JOIN (
	 			SELECT HANDLE,INTERSECTION_START_TIME,INTERSECTION_END_TIME,RESOURCE_BO,USER_BO,LOGIN_DATE_TIME,LOGIN_MODE,ACTION_CODE,C
	 			FROM (
					SELECT HANDLE,INTERSECTION_START_TIME,INTERSECTION_END_TIME,RESOURCE_BO,USER_BO,LOGIN_DATE_TIME,LOGIN_MODE,ACTION_CODE,
					row_number()OVER(PARTITION BY INTERSECTION_START_TIME,INTERSECTION_END_TIME,RESOURCE_BO order by LOGIN_DATE_TIME) C
					FROM :var_between_date
					)
				WHERE C=1)AS B
			ON B.RESOURCE_BO=A.RESOURCE_BO AND A.INTERSECTION_START_TIME=B.INTERSECTION_START_TIME
				AND A.INTERSECTION_END_TIME=B.INTERSECTION_END_TIME
			LEFT JOIN MHP_OEE.USR AS D ON D.HANDLE=B.USER_BO;


			/*最大的开始时间*/
			var_max_date=SELECT
					A.RESOURCE_BO,
					A.USER_BO,
					A.LOGIN_DATE_TIME,
					A.LOGIN_MODE,
					A.ACTION_CODE,
					B.INTERSECTION_START_TIME,
					B.INTERSECTION_END_TIME
					FROM MHP_OEE.CLIENT_LOGIN_LOG AS A
					INNER JOIN :intersection_time_without_fu  AS B ON
					A.RESOURCE_BO=B.RESOURCE_BO
					WHERE A.LOGIN_MODE='ONSITE' AND A.ACTION_CODE='LOGIN'
					AND A.USER_DEPARTMENT = 'PRD'
					AND A.LOGIN_DATE_TIME<B.INTERSECTION_START_TIME;

	 /*取PRD操作员*/
	 var_prd_before=SELECT
	 			A.ROW_ID,
				A.STARTDATE,
				A.SHIFT,
				A.SHIFT_DESC,
				A.RESRCE_DESC,
				A.RESOURCE_TYPE,
				A.RESOURCE_TYPE_DESCRIPTION,
				A.ME_USER_ID,
				A.RESRCE,
				A.ASSET,
				A.LINE_AREA_DESC,
				A.START_DATE_TIME,
				A.END_DATE_TIME,
				A.INTERSECTION_START_TIME,
				A.INTERSECTION_END_TIME,
				A.INTERSECTION_TIME_DURATION,
				A.RESOURCE_BO,
				A.ELAPSED_TIME,
				A.RESOURCE_STATE,
				A.RESOURCE_STATE_DESC,
				D.USER_ID,
				null as USER_BO,
				B.LOGIN_DATE_TIME,
				A.ALERT_INFO,
				A.DATE_TIME
	 		FROM :intersection_time_without_fu AS A
	 		LEFT JOIN (
	 			SELECT INTERSECTION_START_TIME,INTERSECTION_END_TIME,RESOURCE_BO,USER_BO,LOGIN_DATE_TIME,LOGIN_MODE,ACTION_CODE,C FROM (
				SELECT INTERSECTION_START_TIME,INTERSECTION_END_TIME,RESOURCE_BO,USER_BO,LOGIN_DATE_TIME,LOGIN_MODE,ACTION_CODE,
				row_number()OVER(PARTITION BY INTERSECTION_START_TIME,INTERSECTION_END_TIME,RESOURCE_BO
				order by LOGIN_DATE_TIME DESC) C
				FROM :var_max_date)
				WHERE C=1)
				AS B ON B.RESOURCE_BO=A.RESOURCE_BO AND A.INTERSECTION_START_TIME=B.INTERSECTION_START_TIME
				AND A.INTERSECTION_END_TIME=B.INTERSECTION_END_TIME
				LEFT JOIN MHP_OEE.USR AS D ON D.HANDLE=B.USER_BO;


	 		/*最小的结束时间*/
	 		var_min_date=SELECT
					A.RESOURCE_BO,
					A.USER_BO,
					A.LOGIN_DATE_TIME,
					A.LOGIN_MODE,
					A.ACTION_CODE,
					B.INTERSECTION_START_TIME,
					B.INTERSECTION_END_TIME
					FROM MHP_OEE.CLIENT_LOGIN_LOG AS A
					INNER JOIN :intersection_time_without_fu  AS B ON
					A.RESOURCE_BO=B.RESOURCE_BO
					WHERE A.LOGIN_MODE='ONSITE' AND A.ACTION_CODE='LOGOUT'
					AND A.USER_DEPARTMENT = 'PRD'
					AND A.LOGIN_DATE_TIME>=B.INTERSECTION_END_TIME;

	 var_prd_after=SELECT
	 			A.ROW_ID,
				A.STARTDATE,
				A.SHIFT,
				A.SHIFT_DESC,
				A.RESRCE_DESC,
				A.RESOURCE_TYPE,
				A.RESOURCE_TYPE_DESCRIPTION,
				A.ME_USER_ID,
				A.RESRCE,
				A.ASSET,
				A.LINE_AREA_DESC,
				A.START_DATE_TIME,
				A.END_DATE_TIME,
				A.INTERSECTION_START_TIME,
				A.INTERSECTION_END_TIME,
				A.INTERSECTION_TIME_DURATION,
				A.RESOURCE_BO,
				A.ELAPSED_TIME,
				A.RESOURCE_STATE,
				A.RESOURCE_STATE_DESC,
				D.USER_ID,
				B.USER_BO,
				B.LOGIN_DATE_TIME,
				A.ALERT_INFO,
				A.DATE_TIME
	 		FROM :intersection_time_without_fu AS A
	 		LEFT JOIN (
	 			SELECT INTERSECTION_START_TIME,INTERSECTION_END_TIME,RESOURCE_BO,USER_BO,LOGIN_DATE_TIME,LOGIN_MODE,ACTION_CODE,C FROM (
				SELECT INTERSECTION_START_TIME,INTERSECTION_END_TIME,RESOURCE_BO,USER_BO,LOGIN_DATE_TIME,LOGIN_MODE,ACTION_CODE,
				row_number()OVER(PARTITION BY INTERSECTION_START_TIME,INTERSECTION_END_TIME,RESOURCE_BO
				order by LOGIN_DATE_TIME) C
				FROM :var_min_date)
				WHERE C=1)
				AS B ON B.RESOURCE_BO=A.RESOURCE_BO AND A.INTERSECTION_START_TIME=B.INTERSECTION_START_TIME
				AND A.INTERSECTION_END_TIME=B.INTERSECTION_END_TIME
	 		LEFT JOIN MHP_OEE.USR AS D ON D.HANDLE=B.USER_BO;


	 		var_prd_before_and_after=SELECT
	 			A.ROW_ID,
	 			A.STARTDATE,
				A.SHIFT,
				A.SHIFT_DESC,
				A.RESRCE_DESC,
				A.RESOURCE_TYPE,
				A.RESOURCE_TYPE_DESCRIPTION,
				A.ME_USER_ID,
				A.RESRCE,
				A.ASSET,
				A.LINE_AREA_DESC,
				A.START_DATE_TIME,
				A.END_DATE_TIME,
				A.INTERSECTION_START_TIME,
				A.INTERSECTION_END_TIME,
				A.INTERSECTION_TIME_DURATION,
				A.RESOURCE_BO,
				A.ELAPSED_TIME,
				A.RESOURCE_STATE,
				A.RESOURCE_STATE_DESC,
				CASE
				WHEN B.LOGIN_DATE_TIME is not null and A.LOGIN_DATE_TIME is not null THEN B.USER_ID
				WHEN B.LOGIN_DATE_TIME is not null and A.LOGIN_DATE_TIME is null THEN B.USER_ID
				WHEN B.LOGIN_DATE_TIME is null or A.LOGIN_DATE_TIME is null THEN null
				ELSE A.USER_ID END AS USER_ID,
				B.USER_BO,
				CASE WHEN B.LOGIN_DATE_TIME is null THEN B.LOGIN_DATE_TIME ELSE A.LOGIN_DATE_TIME END AS LOGIN_DATE_TIME,
				A.ALERT_INFO,
				A.DATE_TIME
	 		FROM :var_prd_after AS A
	 		INNER JOIN :var_prd_before AS B ON
	 		B.RESOURCE_BO=A.RESOURCE_BO
	 		AND A.ROW_ID = B.ROW_ID
	 		WHERE A.INTERSECTION_START_TIME=B.INTERSECTION_START_TIME
			AND A.INTERSECTION_END_TIME=B.INTERSECTION_END_TIME;


	 		result_prd=SELECT
				A.STARTDATE,
				A.SHIFT,
				A.SHIFT_DESC,
				A.RESRCE_DESC,
				A.RESOURCE_TYPE,
				A.RESOURCE_TYPE_DESCRIPTION,
				A.ME_USER_ID,
				A.RESRCE,
				A.ASSET,
				A.LINE_AREA_DESC,
				A.START_DATE_TIME,
				A.END_DATE_TIME,
				A.INTERSECTION_START_TIME,
				A.INTERSECTION_END_TIME,
				A.INTERSECTION_TIME_DURATION,
				A.ELAPSED_TIME,
				A.RESOURCE_STATE_DESC,
				A.ALERT_INFO,
				A.DATE_TIME AS ALERT_DATE_TIME,
				CASE WHEN A.USER_ID is not null THEN A.USER_ID ELSE B.USER_ID END AS USER_ID,
				B.USER_BO,
				CASE WHEN A.LOGIN_DATE_TIME is not null THEN A.LOGIN_DATE_TIME ELSE B.LOGIN_DATE_TIME END AS LOGIN_DATE_TIME,
				A.RESOURCE_BO,

				A.COMMENT_CODE
	 			FROM :var_prd_between AS A INNER JOIN :var_prd_before_and_after AS B ON B.RESOURCE_BO=A.RESOURCE_BO AND A.INTERSECTION_START_TIME=B.INTERSECTION_START_TIME
	 			AND A.ROW_ID = B.ROW_ID
				AND A.INTERSECTION_END_TIME=B.INTERSECTION_END_TIME;

	 result_prd_last = SELECT RP.*,AC.DESCRIPTION AS ALERT_COMMENT_DESCRIPTION FROM :result_prd AS RP
	 				   LEFT JOIN MHP_OEE.ALERT_COMMENT AS AC
							  ON  AC.COMMENT_CODE=RP.COMMENT_CODE;
	 var_out = SELECT *, 1 AS QTY FROM :result_prd_last;

 END /********* End Procedure Script ************/