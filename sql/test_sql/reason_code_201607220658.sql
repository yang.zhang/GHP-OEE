﻿INSERT INTO `mhp-oee`.reason_code (HANDLE,SITE,REASON_CODE,DESCRIPTION,REASON_CODE_GROUP,ENABLED,CREATED_DATE_TIME,MODIFIED_DATE_TIME,MODIFIED_USER) VALUES 
('ReasonCodeBO:TEST_R,NM01','TEST_R','NM01','设备故障CM','NS00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
,('ReasonCodeBO:TEST_R,NM02','TEST_R','NM02','自检品质异常','NS00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
,('ReasonCodeBO:TEST_R,NM03','TEST_R','NM03','自检功能异常','NS00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
,('ReasonCodeBO:TEST_R,NP01','TEST_R','NP01','交接班','NS00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
,('ReasonCodeBO:TEST_R,NP04','TEST_R','NP04','物料更换','NS00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
,('ReasonCodeBO:TEST_R,NP05','TEST_R','NP05','现场岗位处理','NS00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
,('ReasonCodeBO:TEST_R,NP08','TEST_R','NP08','待PRD处理','NS00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
,('ReasonCodeBO:TEST_R,NP11','TEST_R','NP11','未知原因','NS00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
,('ReasonCodeBO:TEST_R,PR01','TEST_R','PR01','正常生产','PR00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
,('ReasonCodeBO:TEST_R,SD01','TEST_R','SD01','PM','SD00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
;
INSERT INTO `mhp-oee`.reason_code (HANDLE,SITE,REASON_CODE,DESCRIPTION,REASON_CODE_GROUP,ENABLED,CREATED_DATE_TIME,MODIFIED_DATE_TIME,MODIFIED_USER) VALUES 
('ReasonCodeBO:TEST_R,SD02','TEST_R','SD02','盘点','SD00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
,('ReasonCodeBO:TEST_R,SD05','TEST_R','SD05','设备升级改造','SD00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
,('ReasonCodeBO:TEST_R,SD08','TEST_R','SD08','吃饭时间','SD00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
,('ReasonCodeBO:TEST_R,SD09','TEST_R','SD09','样品制件','SD00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
,('ReasonCodeBO:TEST_R,SD10','TEST_R','SD10','无生产计划','SD00','true',STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),STR_TO_DATE('2016-07-01 09:00:00','%Y-%m-%d %H:%i:%s'),'TESTER')
;