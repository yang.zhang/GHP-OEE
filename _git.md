## git 常用命令
git clone [url]  --克隆git项目到本地上
git status --查看状态
git add [文件目录] --添加文件
git commit --提交到本地仓库
git push --推送到git服务器上

git pull --拉取git服务器上的代码

git branch --查看本地分支
git branch -a  --查看远程所有分支

git checkout -b [分支名称]  --创建一个本地分支
git push origin [分支名称] --推送本地分支到远程

备注: 代码冲突建议利用idea去比较解决冲突

# git 打标签
git tag # 在控制台打印出当前仓库的所有标签
git tag -l ‘v0.1.*’ # 搜索符合模式的标签
## 创建轻量标签
$ git tag v0.1.2-light
## 创建附注标签
$ git tag -a v0.1.2 -m “0.1.2版本”
## 切换到标签
git checkout [tagname]
## 查看标签的版本信息：
$ git show v0.1.2
## 删除标签
$ git tag -d v0.1.2 # 删除标签
## 标签发布
通常的git push不会将标签对象提交到git服务器，我们需要进行显式的操作：
$ git push origin v0.1.2 # 将v0.1.2标签提交到git服务器
$ git push origin –tags # 将本地所有标签一次性提交到git服务器