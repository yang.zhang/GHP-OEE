/*===========================================================
创建表：
（用于局部）
1.临时存储压缩数据表RAW
2.用于计算的表GT
3.计算结果表RESULT_THR
=============================================================*/

-- 能量
CREATE COLUMN TABLE GT_ENER_LEVEL_1
(
BATTERY_ID           NVARCHAR(200)                  not null ,
VT_TIME              INTEGER                        not null ,
Y            		 DECIMAL(38,20)                 not null ,
ABS_S                DECIMAL(38,20)                 null 
);
CREATE INDEX IDX_GT_ENER_LEVEL_1_1 ON GT_ENER_LEVEL_1(BATTERY_ID );
CREATE INDEX IDX_GT_ENER_LEVEL_1_2 ON GT_ENER_LEVEL_1(VT_TIME    );
CREATE INDEX IDX_GT_ENER_LEVEL_1_3 ON GT_ENER_LEVEL_1(Y  ); 
	
CREATE COLUMN TABLE GT_ENER_LEVEL_2
(
BATTERY_ID           NVARCHAR(200)                 not null ,
VT_TIME              INTEGER                       not null ,
Y            		 DECIMAL(38,20)                not null ,
ABS_S                DECIMAL(38,20)                null ,
RANK_S               INTEGER                       null
); 
CREATE INDEX IDX_GT_ENER_LEVEL_2_1 ON GT_ENER_LEVEL_2(BATTERY_ID, VT_TIME );
	
CREATE COLUMN TABLE GT_ENER_LEVEL_3
(
   	ROW_NUM              INTEGER                    not null ,
   	BATTERY_ID           NVARCHAR(200)                  not null ,
   	VT_TIME              INTEGER                        not null ,
   	Y            		 DECIMAL(38,20)                 not null ,
   	ABS_S                DECIMAL(38,20)                     null ,
   	RANK_S               INTEGER                            null ,
   	FIRST_N              DECIMAL(38,20)                     null 
);     
CREATE INDEX IDX_GT_ENER_LEVEL_3_1 ON GT_ENER_LEVEL_3(BATTERY_ID );
CREATE INDEX IDX_GT_ENER_LEVEL_3_2 ON GT_ENER_LEVEL_3(VT_TIME    );
CREATE INDEX IDX_GT_ENER_LEVEL_3_3 ON GT_ENER_LEVEL_3(Y  ); 
CREATE INDEX IDX_GT_ENER_LEVEL_3_4 ON GT_ENER_LEVEL_3(RANK_S  );    
CREATE INDEX IDX_GT_ENER_LEVEL_3_5 ON GT_ENER_LEVEL_3(FIRST_N  );
	
CREATE COLUMN TABLE GT_ENER_LEVEL_4
(
   	ROW_NUM              INTEGER                    not null ,
   	BATTERY_ID           NVARCHAR(200)                  not null ,
   	VT_TIME              INTEGER                        not null ,
   	Y            		 DECIMAL(38,20)                 not null ,
   	ABS_S                DECIMAL(38,20)                    null ,
   	RANK_S               INTEGER                           null ,
   	FIRST_N              DECIMAL(38,20)                    null ,   
   	PREV_ROW             INTEGER                       null ,  
   	NEXT_ROW             INTEGER                       null
);     
CREATE INDEX IDX_GT_ENER_LEVEL_4_1 ON GT_ENER_LEVEL_4(BATTERY_ID );
CREATE INDEX IDX_GT_ENER_LEVEL_4_2 ON GT_ENER_LEVEL_4(VT_TIME    );
CREATE INDEX IDX_GT_ENER_LEVEL_4_3 ON GT_ENER_LEVEL_4(Y  ); 
CREATE INDEX IDX_GT_ENER_LEVEL_4_4 ON GT_ENER_LEVEL_4(NEXT_ROW  );    
CREATE INDEX IDX_GT_ENER_LEVEL_4_5 ON GT_ENER_LEVEL_4(PREV_ROW  );  

CREATE COLUMN TABLE GT_ENER_LEVEL_5 
(
   	ROW_NUM              INTEGER                    not null ,
   	BATTERY_ID           NVARCHAR(200)                  not null ,
   	VT_TIME              INTEGER                        not null ,
   	Y            		 DECIMAL(38,20)                 not null ,
   	ABS_S                DECIMAL(38,20)                     null ,
   	RANK_S               INTEGER                            null ,
   	FIRST_N              DECIMAL(38,20)                     null ,   
   	NEW_PREV_ROW         INTEGER                        null , 
   	NEW_NEXT_ROW         INTEGER                        null 
);    
CREATE INDEX IDX_GT_ENER_LEVEL_5_1 ON GT_ENER_LEVEL_5(BATTERY_ID );
CREATE INDEX IDX_GT_ENER_LEVEL_5_2 ON GT_ENER_LEVEL_5(VT_TIME    );
CREATE INDEX IDX_GT_ENER_LEVEL_5_3 ON GT_ENER_LEVEL_5(ROW_NUM  );     
CREATE INDEX IDX_GT_ENER_LEVEL_5_4 ON GT_ENER_LEVEL_5(NEW_PREV_ROW  ); 


CREATE COLUMN TABLE GT_ENER_LEVEL_5A 
(
   	ROW_NUM              INTEGER                    not null ,
   	BATTERY_ID           NVARCHAR(200)                  not null ,
   	VT_TIME              INTEGER                        not null ,
   	Y            		 DECIMAL(38,20)                 not null ,
   	ABS_S                DECIMAL(38,20)                     null ,
   	RANK_S               INTEGER                            null ,
   	FIRST_N              DECIMAL(38,20)                     null ,   
   	NEW_PREV_ROW         INTEGER                        null ,
   	NEW_PREV_VT_TIME     INTEGER                            null ,
   	NEW_PREV_FIRST_N     DECIMAL(38,20)                     null ,
   	NEW_NEXT_ROW         INTEGER                        null 
);    

CREATE INDEX IDX_GT_ENER_LEVEL_5A_1 ON GT_ENER_LEVEL_5A(BATTERY_ID );
CREATE INDEX IDX_GT_ENER_LEVEL_5A_2 ON GT_ENER_LEVEL_5A(NEW_NEXT_ROW);
CREATE INDEX IDX_GT_ENER_LEVEL_5A_3 ON GT_ENER_LEVEL_5A(ROW_NUM  );   


CREATE COLUMN TABLE GT_ENER_LEVEL_5B 
(
   	ROW_NUM              INTEGER                    not null ,
   	BATTERY_ID           NVARCHAR(200)                  not null ,
   	VT_TIME              INTEGER                        not null ,
   	Y            		 DECIMAL(38,20)                 not null ,
   	ABS_S                DECIMAL(38,20)                     null ,
   	RANK_S               INTEGER                            null ,
   	FIRST_N              DECIMAL(38,20)                     null ,   
   	NEW_PREV_ROW         INTEGER                        null ,
   	NEW_PREV_VT_TIME     INTEGER                            null ,
   	NEW_PREV_FIRST_N     DECIMAL(38,20)                     null ,
   	NEW_NEXT_ROW         INTEGER                        null ,
   	NEW_NEXT_VT_TIME     INTEGER                            null ,
   	NEW_NEXT_FIRST_N     DECIMAL(38,20)                     null  
);    

CREATE INDEX IDX_GT_ENER_LEVEL_5B_1 ON GT_ENER_LEVEL_5B(BATTERY_ID );
CREATE INDEX IDX_GT_ENER_LEVEL_5B_2 ON GT_ENER_LEVEL_5B(ROW_NUM  ); 
	
CREATE COLUMN TABLE GT_ENER_LEVEL_5C 
(
   	ROW_NUM              INTEGER                    not null ,
   	BATTERY_ID           NVARCHAR(200)                  not null ,
   	INX_ROW_NUM          INTEGER                    not null 
);    

CREATE INDEX IDX_GT_ENER_LEVEL_5C_1 ON GT_ENER_LEVEL_5C(BATTERY_ID );
CREATE INDEX IDX_GT_ENER_LEVEL_5C_2 ON GT_ENER_LEVEL_5C(ROW_NUM  );    
CREATE INDEX IDX_GT_ENER_LEVEL_5C_3 ON GT_ENER_LEVEL_5C(INX_ROW_NUM  ); 
	
CREATE COLUMN TABLE GT_ENER_LEVEL_6 (
   	ROW_NUM              INTEGER                    not null ,
   	BATTERY_ID           NVARCHAR(200)                  not null ,
   	VT_TIME              INTEGER                        not null ,
   	Y            		 DECIMAL(38,20)                 not null ,
   	ABS_S                DECIMAL(38,20)                     null ,
   	RANK_S               INTEGER                            null ,
   	FIRST_N              DECIMAL(38,20)                     null ,
   	K                    DECIMAL(38,20)                     null ,
   	B                    DECIMAL(38,20)                     null , 
   	INX_ROW_NUM          INTEGER                        null , 
   	NEW_PREV_ROW         INTEGER                        null ,
   	new_prev_VT_TIME     INTEGER                            null ,
   	new_prev_FIRST_N     DECIMAL(38,20)                     null ,
   	NEW_NEXT_ROW         INTEGER                        null ,
   	new_NEXT_VT_TIME     INTEGER                            null ,
   	new_NEXT_FIRST_N     DECIMAL(38,20)                     null  
);  
CREATE INDEX IDX_GT_ENER_LEVEL_6_1 ON GT_ENER_LEVEL_6(BATTERY_ID );
CREATE INDEX IDX_GT_ENER_LEVEL_6_2 ON GT_ENER_LEVEL_6(VT_TIME    );
CREATE INDEX IDX_GT_ENER_LEVEL_6_3 ON GT_ENER_LEVEL_6(ROW_NUM  ); 
	  
	
CREATE COLUMN TABLE GT_ENER_LEVEL_8 (
   ROW_NUM              INTEGER                    not null ,
   BATTERY_ID           NVARCHAR(200)                  not null ,
   VT_TIME              INTEGER                        not null ,
   Y            		DECIMAL(38,20)                 not null ,
   ABS_S                DECIMAL(38,20)                    null ,
   RANK_S               INTEGER                           null ,
   FIRST_N              DECIMAL(38,20)                    null ,
   K                    DECIMAL(38,20)                    null ,
   B                    DECIMAL(38,20)                    null ,
   CAL_MISSING          DECIMAL(38,20)                    null , 
   NEW_CAL_Y    		DECIMAL(38,20)                    null  
) ;
CREATE INDEX IDX_GT_ENER_LEVEL_8_1 ON GT_ENER_LEVEL_8(BATTERY_ID, ROW_NUM );
	
CREATE COLUMN TABLE GT_ENER_LEVEL_9 
(
   ROW_NUM              INTEGER                    not null ,
   BATTERY_ID           NVARCHAR(200)                  not null ,
   VT_TIME              INTEGER                        not null ,
   Y            		DECIMAL(38,20)                 not null ,
   ABS_S                DECIMAL(38,20)                     null ,
   RANK_S               INTEGER                            null ,
   FIRST_N              DECIMAL(38,20)                     null ,
   K                    DECIMAL(38,20)                     null ,
   B                    DECIMAL(38,20)                     null ,
   CAL_MISSING          DECIMAL(38,20)                     null , 
   NEW_CAL_Y    		DECIMAL(38,20)                     null ,
   MEAN_Y               DECIMAL(38,20)                     null ,
   Y_MINUS_Y_SQR        DECIMAL(38,20)                     null ,
   Y_MINUS_Y_Y_SQR      DECIMAL(38,20)                     null 
) ;
CREATE INDEX IDX_GT_ENER_LEVEL_9_1 ON GT_ENER_LEVEL_9(ROW_NUM );
CREATE INDEX IDX_GT_ENER_LEVEL_9_2 ON GT_ENER_LEVEL_9(VT_TIME);
CREATE INDEX IDX_GT_ENER_LEVEL_9_3 ON GT_ENER_LEVEL_9(BATTERY_ID);

CREATE COLUMN TABLE ENER_RAW 
(
   BATTERY_ID           NVARCHAR(200)                 not null ,
   VT_TIME              INTEGER                       not null ,
   Y            		DECIMAL(38,20)                not null 
);
CREATE INDEX IDX_ENER_RAW_1 ON ENER_RAW(VT_TIME);
CREATE INDEX IDX_ENER_RAW_2 ON ENER_RAW(Y);
CREATE INDEX IDX_ENER_RAW_3 ON ENER_RAW(BATTERY_ID);

CREATE COLUMN TABLE ENER_RESULT_THRESHOLD
(
BATTERY_ID NVARCHAR(200) NOT NULL,
R_SQRT     DECIMAL(38,9)     NULL
);
CREATE INDEX IDX_ENER_RESULT_THRESHOLD_1 ON ENER_RESULT_THRESHOLD(BATTERY_ID);

CREATE GLOBAL TEMPORARY COLUMN TABLE "GT_NOT_ZIP_ENER_DATA_ALL"(
REMARK NVARCHAR(80),
ST_BUSINESS_CYCLE INTEGER,
STEP_ID INTEGER
);

