CREATE PROCEDURE DOZIP_CAP_TEST(OUT res varchar(2000))
AS
BEGIN
	/*============================================================
	参数：
	NONE_ZIP_UPPPER_NUMBER 压缩上限
	PRE_ZIP_NUMBER 压缩保留前N个点
	POST_ZIP_NUMBER 压缩保留后N个点
	R_SQUARED 拟合度
	START_SAMPLE_PERCENTAGE 开始压缩百分比
	SAMPLE_INCREASE 压缩步长
	OUT : 
	res varchar(2000)(IF SUCCESS RETURN 0	IF ERROR RETURN ERROR_CODE&ERROR_MESSAGE)
	==============================================================*/
	DECLARE pre_zip_number INTEGER :=10;
	DECLARE post_zip_number INTEGER :=10;
	DECLARE current_sample_percentage DECIMAL(38,2) :=0.1;
	DECLARE none_zip_uppper_number DECIMAL(38,2) :=50;
	DECLARE sample_increase DECIMAL(38,2) :=0.1;
	DECLARE targetR  DECIMAL(38,4) :=0.99; 
	DECLARE cnt INTEGER;
	DECLARE i INTEGER;
	DECLARE tl_cnt INTEGER :=0;
	
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
		SELECT 'SQL ERROR[' || ::SQL_ERROR_CODE || ']:' || ::SQL_ERROR_MESSAGE INTO res FROM DUMMY;
		UPDATE TECH_ZIP_STATUS A SET C_STATUS=2,MODIFIED_DATE_TIME=CURRENT_TIMESTAMP,MODIFIED_USER='JOB_USER' 
		WHERE EXISTS(SELECT 1 FROM "GT_NOT_ZIP_CAP_DATA_ALL" B 
		WHERE A.REMARK=B.REMARK AND A.ST_BUSINESS_CYCLE=B.ST_BUSINESS_CYCLE AND A.STEP_ID=B.STEP_ID AND C_STATUS=0);
		TRUNCATE TABLE "GT_NOT_ZIP_CAP_DATA_ALL";
		TRUNCATE TABLE CAP_RAW;
		TRUNCATE TABLE GT_CAP_LEVEL_9;
	END;
	
	SELECT COUNT(1) INTO cnt FROM MD_ZIP_CONFIG;
	IF :cnt <> 0 THEN
		SELECT START_SAMPLE_PERCENTAGE,SAMPLE_INCREASE,PRE_ZIP_NUMBER,POST_ZIP_NUMBER,NONE_ZIP_UPPPER_NUMBER 
		INTO current_sample_percentage,sample_increase,pre_zip_number,post_zip_number,none_zip_uppper_number 
		FROM MD_ZIP_CONFIG LIMIT 1;
		
		-- SELECT PRE_ZIP_NUMBER INTO pre_zip_number FROM 
		SELECT R_SQUARED INTO targetR FROM MD_ZIP_CONFIG LIMIT 1;
		
		SELECT COUNT(1) INTO tl_cnt FROM M_TEMPORARY_TABLES WHERE SCHEMA_NAME ='TES' AND TABLE_NAME = 'GT_NOT_ZIP_CAP_DATA_ALL';
		IF :tl_cnt=0 THEN
			CREATE GLOBAL TEMPORARY COLUMN TABLE "GT_NOT_ZIP_CAP_DATA_ALL"(
			REMARK NVARCHAR(80),
			ST_BUSINESS_CYCLE INTEGER,
			STEP_ID INTEGER
			);
		END IF;
		TRUNCATE TABLE "GT_NOT_ZIP_CAP_DATA_ALL";
		INSERT INTO "GT_NOT_ZIP_CAP_DATA_ALL"
		SELECT REMARK,ST_BUSINESS_CYCLE,STEP_ID FROM TECH_ZIP_STATUS WHERE C_STATUS=0;
		
		
		CREATE LOCAL TEMPORARY COLUMN TABLE "#not_zip_cap_data" LIKE "GT_NOT_ZIP_CAP_DATA_ALL" WITH NO DATA;
		INSERT INTO "#not_zip_cap_data" SELECT A.REMARK,A.ST_BUSINESS_CYCLE,A.STEP_ID FROM "GT_NOT_ZIP_CAP_DATA_ALL" A, TX_ORIGINAL_PROCESS_DATA B
		WHERE A.REMARK=B.REMARK AND A.ST_BUSINESS_CYCLE=B.ST_BUSINESS_CYCLE AND A.STEP_ID=B.STEP_ID 
		GROUP BY A.REMARK, A.ST_BUSINESS_CYCLE, A.STEP_ID 
		HAVING COUNT(1)>:none_zip_uppper_number;
		
		CREATE LOCAL TEMPORARY COLUMN TABLE "#zip_rawdata_inx"
		(
			REMARK NVARCHAR(200),
			ST_BUSINESS_CYCLE INTEGER,
			STEP_ID INTEGER,
			ROW_NUM INTEGER,
			SEQUENCE_ID INTEGER
		);
		INSERT INTO "#zip_rawdata_inx"
		SELECT B.REMARK,B.ST_BUSINESS_CYCLE, B.STEP_ID,row_number() OVER (PARTITION BY B.REMARK,B.ST_BUSINESS_CYCLE,B.STEP_ID ORDER BY B.SEQUENCE_ID), B.SEQUENCE_ID
		FROM "#not_zip_cap_data" A,"TX_ORIGINAL_PROCESS_DATA" B
		WHERE A.ST_BUSINESS_CYCLE=B.ST_BUSINESS_CYCLE AND A.STEP_ID=B.STEP_ID AND A.REMARK = B.REMARK;
		
		TRUNCATE TABLE CAP_RAW;
		INSERT INTO CAP_RAW SELECT B.REMARK || ',' ||B.ST_BUSINESS_CYCLE|| ',' || B.STEP_ID, B.SEQUENCE_ID, 
		CASE WHEN B.PV_CHARGE_CAPACITY <> 0 THEN B.PV_CHARGE_CAPACITY ELSE PV_DISCHARGE_CAPACITY END 
		FROM "#not_zip_cap_data" A,"TX_ORIGINAL_PROCESS_DATA" B, 
		(SELECT REMARK, ST_BUSINESS_CYCLE, STEP_ID, COUNT(1) TOTAL 
		FROM TX_ORIGINAL_PROCESS_DATA GROUP BY REMARK, ST_BUSINESS_CYCLE, STEP_ID) C ,"#zip_rawdata_inx" D
		WHERE A.ST_BUSINESS_CYCLE=B.ST_BUSINESS_CYCLE AND A.STEP_ID=B.STEP_ID AND A.REMARK = B.REMARK
		AND  A.ST_BUSINESS_CYCLE=C.ST_BUSINESS_CYCLE AND A.STEP_ID=C.STEP_ID AND A.REMARK = C.REMARK
		AND  A.ST_BUSINESS_CYCLE=D.ST_BUSINESS_CYCLE AND A.STEP_ID=D.STEP_ID AND A.REMARK = D.REMARK
		AND B.SEQUENCE_ID = D.SEQUENCE_ID
		AND (D.ROW_NUM >:pre_zip_number AND D.ROW_NUM<=(C.TOTAL-:post_zip_number));-- (NOT IN 10%)
		COMMIT;
		
		
		
		FOR i IN 1.. 100
		DO 
			
			SELECT COUNT(1) INTO cnt FROM CAP_RAW;
			IF :cnt=0 THEN 
				BREAK;
			END IF;
			IF :cnt>0 THEN
				CALL "CAP_COMPRESSION_TEST"(:current_sample_percentage);
			END IF;
			
			INSERT INTO TX_ZIP_PROCESS_DATA
			(
			HANDLE, SITE,REMARK,SFC,RESOURCE_ID,CHANNEL_ID,SEQUENCE_ID,CYCLE,STEP_ID,
			STEP_NAME,TEST_TIME_DURATION,TIMESTAMP,SV_IC_RANGE,SV_IV_RANGE,
			PV_VOLTAGE,PV_CURRENT,PV_IR,PV_TEMPERATURE,PV_CHARGE_CAPACITY,PV_DISCHARGE_CAPACITY,
			PV_CHARGE_ENERGY,PV_DISCHARGE_ENERGY,PV_SUB_CHANNEL_1,PV_SUB_CHANNEL_2,
			PV_SUB_CHANNEL_3,PV_SUB_CHANNEL_4,PV_SUB_CHANNEL_5,PV_SUB_CHANNEL_6,PV_DATA_FLAG,PV_WORK_TYPE,TX_IS_EXCEPTIONAL,
			TX_ALERT_CURRENT,TX_ALERT_VOLTAGE,TX_ALERT_TEMPERATURE,TX_ALERT_CAPACITY,TX_ALERT_DURATION,TX_ALERT_CATEGORY1,TX_ALERT_CATEGORY2,
			TX_ROOT_REMARK, ST_BUSINESS_CYCLE, ZIP_CATEGORY, CREATED_USER, MODIFIED_USER 
			)
			SELECT 'TxZipProcessDataBO:'||D.SITE||','||D.REMARK||','||D.SFC||','||D.RESOURCE_ID||','||D.CHANNEL_ID||','||D.SEQUENCE_ID||',C', 
			D.SITE,D.REMARK,D.SFC,D.RESOURCE_ID,D.CHANNEL_ID,D.SEQUENCE_ID,D.CYCLE,D.STEP_ID,
			D.STEP_NAME,D.TEST_TIME_DURATION,D.TIMESTAMP,D.SV_IC_RANGE,D.SV_IV_RANGE,
			D.PV_VOLTAGE,D.PV_CURRENT,D.PV_IR,D.PV_TEMPERATURE,D.PV_CHARGE_CAPACITY,D.PV_DISCHARGE_CAPACITY,
			D.PV_CHARGE_ENERGY,D.PV_DISCHARGE_ENERGY,D.PV_SUB_CHANNEL_1,D.PV_SUB_CHANNEL_2,
			D.PV_SUB_CHANNEL_3,D.PV_SUB_CHANNEL_4,D.PV_SUB_CHANNEL_5,D.PV_SUB_CHANNEL_6,
			D.PV_DATA_FLAG,D.PV_WORK_TYPE,D.TX_IS_EXCEPTIONAL,
			D.TX_ALERT_CURRENT,D.TX_ALERT_VOLTAGE,D.TX_ALERT_TEMPERATURE,D.TX_ALERT_CAPACITY,D.TX_ALERT_DURATION,D.TX_ALERT_CATEGORY1,D.TX_ALERT_CATEGORY2,
			D.TX_ROOT_REMARK, D.ST_BUSINESS_CYCLE, 'C', 'JOB_USER','JOB_USER'
			FROM CAP_RESULT_THRESHOLD A, "#not_zip_cap_data" B, GT_CAP_LEVEL_9 C, TX_ORIGINAL_PROCESS_DATA D
			WHERE A.R_SQRT>:targetR 
			AND C.FIRST_N IS NOT NULL 
			AND A.BATTERY_ID = C.BATTERY_ID 
			AND A.BATTERY_ID = D.REMARK || ',' ||D.ST_BUSINESS_CYCLE|| ',' || D.STEP_ID
			AND C.VT_TIME = D.SEQUENCE_ID  
			AND B.ST_BUSINESS_CYCLE=D.ST_BUSINESS_CYCLE
			AND B.STEP_ID = D.STEP_ID 
			AND B.REMARK = D.REMARK;
			COMMIT;
			
			current_sample_percentage := :current_sample_percentage + :sample_increase;
			DELETE FROM CAP_RAW 
			WHERE EXISTS (SELECT 1 FROM CAP_RESULT_THRESHOLD WHERE CAP_RESULT_THRESHOLD.R_SQRT>:targetR AND CAP_RAW.BATTERY_ID = CAP_RESULT_THRESHOLD.BATTERY_ID );
			COMMIT;
		END FOR;
		
		-- 小于NONE_ZIP_UPPPER_NUMBER无需压缩，直接进压缩库
		INSERT INTO TX_ZIP_PROCESS_DATA
		(
		HANDLE, SITE,REMARK,SFC,RESOURCE_ID,CHANNEL_ID,SEQUENCE_ID,CYCLE,STEP_ID,
		STEP_NAME,TEST_TIME_DURATION,TIMESTAMP,SV_IC_RANGE,SV_IV_RANGE,
		PV_VOLTAGE,PV_CURRENT,PV_IR,PV_TEMPERATURE,PV_CHARGE_CAPACITY,PV_DISCHARGE_CAPACITY,
		PV_CHARGE_ENERGY,PV_DISCHARGE_ENERGY,PV_SUB_CHANNEL_1,PV_SUB_CHANNEL_2,
		PV_SUB_CHANNEL_3,PV_SUB_CHANNEL_4,PV_SUB_CHANNEL_5,PV_SUB_CHANNEL_6,PV_DATA_FLAG,PV_WORK_TYPE,TX_IS_EXCEPTIONAL,
		TX_ALERT_CURRENT,TX_ALERT_VOLTAGE,TX_ALERT_TEMPERATURE,TX_ALERT_CAPACITY,TX_ALERT_DURATION,TX_ALERT_CATEGORY1,TX_ALERT_CATEGORY2,
		TX_ROOT_REMARK, ST_BUSINESS_CYCLE, ZIP_CATEGORY, CREATED_USER, MODIFIED_USER 
		)
		SELECT 'TxZipProcessDataBO:'||D.SITE||','||D.REMARK||','||D.SFC||','||D.RESOURCE_ID||','||D.CHANNEL_ID||','||D.SEQUENCE_ID||',C', 
		D.SITE,D.REMARK,D.SFC,D.RESOURCE_ID,D.CHANNEL_ID,D.SEQUENCE_ID,D.CYCLE,D.STEP_ID,
		D.STEP_NAME,D.TEST_TIME_DURATION,D.TIMESTAMP,D.SV_IC_RANGE,D.SV_IV_RANGE,
		D.PV_VOLTAGE,D.PV_CURRENT,D.PV_IR,D.PV_TEMPERATURE,D.PV_CHARGE_CAPACITY,D.PV_DISCHARGE_CAPACITY,
		D.PV_CHARGE_ENERGY,D.PV_DISCHARGE_ENERGY,D.PV_SUB_CHANNEL_1,D.PV_SUB_CHANNEL_2,
		D.PV_SUB_CHANNEL_3,D.PV_SUB_CHANNEL_4,D.PV_SUB_CHANNEL_5,D.PV_SUB_CHANNEL_6,
		D.PV_DATA_FLAG,D.PV_WORK_TYPE,D.TX_IS_EXCEPTIONAL,
		D.TX_ALERT_CURRENT,D.TX_ALERT_VOLTAGE,D.TX_ALERT_TEMPERATURE,D.TX_ALERT_CAPACITY,D.TX_ALERT_DURATION,D.TX_ALERT_CATEGORY1,D.TX_ALERT_CATEGORY2,
		D.TX_ROOT_REMARK, D.ST_BUSINESS_CYCLE, 'C', 'JOB_USER','JOB_USER'
		FROM (SELECT A.ST_BUSINESS_CYCLE, A.REMARK, A. STEP_ID FROM TX_ORIGINAL_PROCESS_DATA A, "GT_NOT_ZIP_CAP_DATA_ALL" B
		WHERE A.ST_BUSINESS_CYCLE=B.ST_BUSINESS_CYCLE AND A.STEP_ID=B.STEP_ID AND A.REMARK = B.REMARK
		GROUP BY A.REMARK, A.ST_BUSINESS_CYCLE, A.STEP_ID
		HAVING COUNT(1)<=:none_zip_uppper_number) C, TX_ORIGINAL_PROCESS_DATA D
		WHERE C.ST_BUSINESS_CYCLE=D.ST_BUSINESS_CYCLE AND C.STEP_ID=D.STEP_ID AND C.REMARK = D.REMARK;
		
		
		UPDATE TECH_ZIP_STATUS A SET C_STATUS=1 ,MODIFIED_DATE_TIME=CURRENT_TIMESTAMP,MODIFIED_USER='JOB_USER'
		WHERE EXISTS(SELECT 1 FROM TX_ORIGINAL_PROCESS_DATA B , "GT_NOT_ZIP_CAP_DATA_ALL" C
		WHERE A.REMARK=B.REMARK AND A.ST_BUSINESS_CYCLE=B.ST_BUSINESS_CYCLE AND A.STEP_ID=B.STEP_ID
		AND A.REMARK=B.REMARK AND A.ST_BUSINESS_CYCLE=B.ST_BUSINESS_CYCLE AND A.STEP_ID=B.STEP_ID 
		GROUP BY C.REMARK, C.ST_BUSINESS_CYCLE, C.STEP_ID 
		HAVING COUNT(1)<=:none_zip_uppper_number); 
		
		INSERT INTO TX_ZIP_PROCESS_DATA
		(
		HANDLE, SITE,REMARK,SFC,RESOURCE_ID,CHANNEL_ID,SEQUENCE_ID,CYCLE,STEP_ID,
		STEP_NAME,TEST_TIME_DURATION,TIMESTAMP,SV_IC_RANGE,SV_IV_RANGE,
		PV_VOLTAGE,PV_CURRENT,PV_IR,PV_TEMPERATURE,PV_CHARGE_CAPACITY,PV_DISCHARGE_CAPACITY,
		PV_CHARGE_ENERGY,PV_DISCHARGE_ENERGY,PV_SUB_CHANNEL_1,PV_SUB_CHANNEL_2,
		PV_SUB_CHANNEL_3,PV_SUB_CHANNEL_4,PV_SUB_CHANNEL_5,PV_SUB_CHANNEL_6,PV_DATA_FLAG,PV_WORK_TYPE,TX_IS_EXCEPTIONAL,
		TX_ALERT_CURRENT,TX_ALERT_VOLTAGE,TX_ALERT_TEMPERATURE,TX_ALERT_CAPACITY,TX_ALERT_DURATION,TX_ALERT_CATEGORY1,TX_ALERT_CATEGORY2,
		TX_ROOT_REMARK, ST_BUSINESS_CYCLE, ZIP_CATEGORY, CREATED_USER, MODIFIED_USER 
		) 
		SELECT 'TxZipProcessDataBO:'||B.SITE||','||B.REMARK||','||B.SFC||','||B.RESOURCE_ID||','||B.CHANNEL_ID||','||B.SEQUENCE_ID||',C', 
		B.SITE,B.REMARK,B.SFC,B.RESOURCE_ID,B.CHANNEL_ID,B.SEQUENCE_ID,B.CYCLE,B.STEP_ID,
		B.STEP_NAME,B.TEST_TIME_DURATION,B.TIMESTAMP,B.SV_IC_RANGE,B.SV_IV_RANGE,
		B.PV_VOLTAGE,B.PV_CURRENT,B.PV_IR,B.PV_TEMPERATURE,B.PV_CHARGE_CAPACITY,B.PV_DISCHARGE_CAPACITY,
		B.PV_CHARGE_ENERGY,B.PV_DISCHARGE_ENERGY,B.PV_SUB_CHANNEL_1,B.PV_SUB_CHANNEL_2,
		B.PV_SUB_CHANNEL_3,B.PV_SUB_CHANNEL_4,B.PV_SUB_CHANNEL_5,B.PV_SUB_CHANNEL_6,
		B.PV_DATA_FLAG,B.PV_WORK_TYPE,B.TX_IS_EXCEPTIONAL,
		B.TX_ALERT_CURRENT,B.TX_ALERT_VOLTAGE,B.TX_ALERT_TEMPERATURE,B.TX_ALERT_CAPACITY,B.TX_ALERT_DURATION,B.TX_ALERT_CATEGORY1,B.TX_ALERT_CATEGORY2,
		B.TX_ROOT_REMARK, B.ST_BUSINESS_CYCLE, 'C', 'JOB_USER','JOB_USER' 
		FROM "#not_zip_cap_data" A, TX_ORIGINAL_PROCESS_DATA B, 
		(SELECT REMARK, ST_BUSINESS_CYCLE, STEP_ID, COUNT(1) TOTAL 
		FROM TX_ORIGINAL_PROCESS_DATA GROUP BY REMARK, ST_BUSINESS_CYCLE, STEP_ID) C,"#zip_rawdata_inx" D
		WHERE A.ST_BUSINESS_CYCLE=B.ST_BUSINESS_CYCLE AND A.STEP_ID=B.STEP_ID AND A.REMARK = B.REMARK 
		AND  A.ST_BUSINESS_CYCLE=C.ST_BUSINESS_CYCLE AND A.STEP_ID=C.STEP_ID AND A.REMARK = C.REMARK
		AND  A.ST_BUSINESS_CYCLE=D.ST_BUSINESS_CYCLE AND A.STEP_ID=D.STEP_ID AND A.REMARK = D.REMARK
		AND B.SEQUENCE_ID = D.SEQUENCE_ID
		AND (D.ROW_NUM<=:pre_zip_number OR D.ROW_NUM>(C.TOTAL-:post_zip_number));
		
		UPDATE TECH_ZIP_STATUS A SET C_STATUS=1 ,MODIFIED_DATE_TIME=CURRENT_TIMESTAMP,MODIFIED_USER='JOB_USER'
		WHERE EXISTS(SELECT 1 FROM "#not_zip_cap_data" B 
		WHERE A.REMARK=B.REMARK AND A.ST_BUSINESS_CYCLE=B.ST_BUSINESS_CYCLE AND A.STEP_ID=B.STEP_ID);
		
		TRUNCATE TABLE "GT_NOT_ZIP_CAP_DATA_ALL";
		TRUNCATE TABLE CAP_RAW;
		TRUNCATE TABLE GT_CAP_LEVEL_9;
	END IF;
	res := '0';
END;