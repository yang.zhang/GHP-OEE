package com.hand.ejb;

import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.hand.common.CustomActivityMaintenanceApplicationInterface;
import com.sap.me.activity.ActivityBOHandle;
import com.sap.me.activity.ActivityConfigurationServiceInterface;
import com.sap.me.activity.ActivityFullConfiguration;
import com.sap.me.activity.ActivityOption;
import com.sap.me.common.ObjectReference;
import com.sap.me.extension.Services;
import com.sap.me.frame.BasicBOBeanException;
import com.sap.me.frame.Data;
import com.sap.me.frame.Utils;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.frame.transitionutils.Exceptions;
import com.sap.me.globalization.KnownProductGlobalizationServices;
import com.sap.me.system.base.ApplicationVO;
import com.sap.me.system.base.BasicApplication;
import com.visiprise.globalization.GlobalizationService;
import com.visiprise.globalization.ResourceBundleGlobalizationServiceInterface;

/**
 * @author Jun.Liu
 * @Description: 客制化作业维护
 * @date Dec 8, 2016 10:27:45 PM
 */
public class CustomActivityMaintenanceApplicationBean extends BasicApplication implements CustomActivityMaintenanceApplicationInterface {
    private static final long serialVersionUID = 1L;

    // 日志消息ID
    private static final String MESSAGE_ID = "com.hand.ejb.CustomActivityMaintenanceApplicationBean";

    private ResourceBundleGlobalizationServiceInterface lstSrv;
    private ActivityConfigurationServiceInterface activityConfiService;

    /**
     * 初始化界面
     */
    public ApplicationVO clear(ApplicationVO appVO) throws BasicBOBeanException {
        return new ApplicationVO();
    }

    /**
     * 读取输入的作业信息
     */
    public ApplicationVO retrieve(ApplicationVO appVO) throws BasicBOBeanException {
        Long startTime = System.currentTimeMillis();
        Utils.traceMsg(MESSAGE_ID + " retrieve() start execution ", Utils.DEBUGGING);

        lstSrv = GlobalizationService.getUserService(KnownProductGlobalizationServices.LOCALE_SPECIFIC_TEXT);
        activityConfiService = Services.getService("com.sap.me.activity", "ActivityConfigurationService");

        String activity = appVO.getValue(ACTIVITY);
        // 校验输入值
        if (Utils.isEmpty(activity)) {
            throw new BasicBOBeanException(20002, new Data("NAME", lstSrv.getString("activity.default.LABEL")));
        }

        String description = null;
        List<String> rulesList = new ArrayList<String>();
        List<String> settingList = new ArrayList<String>();

        try {
            Context context = new InitialContext();
            CustomReadActivityLocal ejb = (CustomReadActivityLocal) context.lookup("ejb:/appName=hand.com/apps~ear, jarName=hand.com~apps~ejb.jar, beanName=CustomReadActivityBean, interfaceName=com.hand.ejb.CustomReadActivityLocal");
            Data resultData = ejb.readActivity(activity);
            if (!Utils.isEmpty(resultData)) {
                for (Data value : resultData) {
                    description = value.getString("DESCRIPTION", "");
                    rulesList.add(value.getString("EXEC_UNIT_OPTION", ""));
                    settingList.add(value.getString("SETTING", ""));
                }
            }
        } catch (NamingException e) {
            Utils.traceMsg(e, Utils.EXCEPTION);
        }
/*
        try {
            String activityRef = new ActivityBOHandle(activity).getValue();
            // 读取作业信息
            ObjectReference objectRef = new ObjectReference();
            objectRef.setRef(activityRef);
            ActivityFullConfiguration activityFullConfi = activityConfiService.readActivity(objectRef);

            if (activityFullConfi != null) {
                description = activityFullConfi.getDescription();
                List<ActivityOption> activityOptionsList = activityFullConfi.getActivityOptionList();
                if (!Utils.isEmpty(activityOptionsList)) {
                    for (ActivityOption option : activityOptionsList) {
                        rulesList.add(option.getExecUnitOption());
                        settingList.add(option.getSetting());
                    }
                }
            }
        } catch (BusinessException e) {
            Utils.traceMsg(e, Utils.EXCEPTION);
            throw Exceptions.convert(e);
        }
*/
        // 定义返回ApplicationVO
        ApplicationVO retVO = new ApplicationVO();
        retVO.setValue(ACTIVITY, activity);
        retVO.setValue(DESCRIPTION, description);
        retVO.setValues(ACTIVITY_RULE, rulesList);
        retVO.setValues(ACTIVITY_SETTTING, settingList);

        Long endTime = System.currentTimeMillis();
        Utils.traceMsg(MESSAGE_ID + " retrieve() end execution ", Utils.DEBUGGING);
        Utils.traceMsg(MESSAGE_ID + " retrieve() execution time in ms " + (endTime - startTime), Utils.DEBUGGING);
        return retVO;
    }
}
