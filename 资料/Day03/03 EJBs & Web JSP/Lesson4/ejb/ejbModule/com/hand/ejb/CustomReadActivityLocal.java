package com.hand.ejb;

import javax.ejb.Local;

import com.sap.me.frame.Data;

/**
 * @author Jun.Liu
 * @Description: SQL读取作业信息接口
 * @date Dec 9, 2016 9:21:26 AM
 */
@Local
public interface CustomReadActivityLocal {
    
    public Data readActivity(String activityID);
    
}
