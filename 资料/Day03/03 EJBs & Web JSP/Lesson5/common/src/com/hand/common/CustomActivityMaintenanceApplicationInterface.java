package com.hand.common;

import com.sap.me.frame.BasicBOBeanException;
import com.sap.me.system.base.ApplicationInterface;
import com.sap.me.system.base.ApplicationVO;

/**
 * @author Jun.Liu
 * @Description: CustomActivityMaintenanceApplicationBean接口声明
 * @date Dec 8, 2016 10:26:53 PM
 */
public interface CustomActivityMaintenanceApplicationInterface extends ApplicationInterface {

    public static final String ACTIVITY = "ACTIVITY";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String COMMENTS = "COMMENTS";
    public static final String ACTIVITY_RULE = "ACTIVITY_RULE";
    public static final String ACTIVITY_SETTTING = "ACTIVITY_SETTTING";

    /**
     * ActivitySave
     */
    public ApplicationVO doSave(ApplicationVO inVO) throws BasicBOBeanException;
}
