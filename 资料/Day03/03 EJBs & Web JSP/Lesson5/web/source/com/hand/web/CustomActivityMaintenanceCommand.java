package com.hand.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hand.common.CustomActivityMaintenanceApplicationInterface;
import com.sap.me.coral.core.command.AbstractCommand;
import com.sap.me.coral.core.command.CommandResult;
import com.sap.me.coral.core.model.Form;
import com.sap.me.frame.BasicBOBeanException;
import com.sap.me.system.base.ApplicationVO;

/**
 * @author Jun.Liu
 * @Description: 客制化作业维护Command事件
 * @date Dec 8, 2016 10:39:15 PM
 */
public class CustomActivityMaintenanceCommand {

    /**
     * ActivityRetrieve
     */
    public static class RetrieveCommand extends AbstractCommand {
        private static final long serialVersionUID = 1L;

        public RetrieveCommand(String name) {
            super(name);
        }

        public CommandResult handleCmd(HttpServletRequest req, HttpServletResponse resp, Form form) throws BasicBOBeanException {
            form.validateKeyFields();
            CustomActivityMaintenanceApplicationInterface service = (CustomActivityMaintenanceApplicationInterface) getService(req);
            ApplicationVO inVO = form.getVO();
            ApplicationVO retVO = service.retrieve(inVO);
            CommandResult cmdResult = new CommandResult(form);
            cmdResult.getForm().setVO(retVO);
            return cmdResult;
        }
    }

    /**
     * ActivitySave
     */
    public static class SaveCommand extends AbstractCommand {
        private static final long serialVersionUID = 1L;

        public SaveCommand(String name) {
            super(name);
        }

        public CommandResult handleCmd(HttpServletRequest req, HttpServletResponse resp, Form form) throws BasicBOBeanException {
            form.validateKeyFields();
            CustomActivityMaintenanceApplicationInterface service = (CustomActivityMaintenanceApplicationInterface) getService(req);
            ApplicationVO inVO = form.getVO();
            ApplicationVO retVO = service.doSave(inVO);
            CommandResult cmdResult = new CommandResult(form);
            cmdResult.getForm().setVO(retVO);
            return cmdResult;
        }
    }

    /**
     * ActivityDelete
     */
    public static class DeleteCommand extends AbstractCommand {
        private static final long serialVersionUID = 1L;

        public DeleteCommand(String name) {
            super(name);
        }

        public CommandResult handleCmd(HttpServletRequest req, HttpServletResponse resp, Form form) throws BasicBOBeanException {
            form.validateKeyFields();
            CustomActivityMaintenanceApplicationInterface service = (CustomActivityMaintenanceApplicationInterface) getService(req);
            ApplicationVO inVO = form.getVO();
            ApplicationVO retVO = service.delete(inVO);
            CommandResult cmdResult = new CommandResult(form);
            cmdResult.getForm().setVO(retVO);
            return cmdResult;
        }
    }
}
