package com.hand.ejb;

import javax.ejb.Stateless;

import com.sap.me.frame.Data;
import com.sap.me.frame.JNDIUtils;
import com.sap.me.frame.SystemBase;
import com.sap.me.frame.jdbc.DynamicQuery;
import com.sap.me.frame.jdbc.DynamicQueryFactory;

/**
 * @author Jun.Liu
 * @Description: SQL读取作业信息Bean
 * @date Dec 9, 2016 9:22:35 AM
 */
@Stateless
public class CustomReadActivityBean implements CustomReadActivityLocal {

    SystemBase systemBase = null;

    /**
     * SQL读取作业信息
     */
    @Override
    public Data readActivity(String activityID) {
        systemBase = new SystemBase();
        systemBase.init(JNDIUtils.DEFAULT_DATASOURCE, false);
        DynamicQuery dq = DynamicQueryFactory.newInstance();
        
        dq.clear();
        dq.append("SELECT A.HANDLE, A.ACTIVITY, A.DESCRIPTION, B.EXEC_UNIT_OPTION, B.SETTING");
        dq.append(" FROM ACTIVITY A LEFT OUTER JOIN ACTIVITY_OPTION B ON A.HANDLE = B.ACTIVITY_BO");
        dq.append(" WHERE A.ACTIVITY = ").appendString(activityID);
        return systemBase.executeQuery(dq);
    }
}
