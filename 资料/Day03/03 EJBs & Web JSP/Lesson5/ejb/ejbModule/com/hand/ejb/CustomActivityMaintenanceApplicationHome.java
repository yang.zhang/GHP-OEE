package com.hand.ejb;

/**
 * @author Jun.Liu
 * @Description: CustomActivityMaintenanceApplicationBean EJBLocalHome接口声明
 * @date Dec 8, 2016 10:29:23 PM
 */
public interface CustomActivityMaintenanceApplicationHome extends javax.ejb.EJBLocalHome {
    public static final String COMP_NAME = "java:comp/env/ejb/CustomActivityMaintenanceApplicationLocal";
    public static final String JNDI_NAME = "ejbs/local/CustomActivityMaintenanceApplication";

    public CustomActivityMaintenanceApplication create() throws javax.ejb.CreateException;
}
