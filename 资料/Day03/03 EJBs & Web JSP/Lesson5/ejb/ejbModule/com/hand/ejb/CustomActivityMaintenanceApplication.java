package com.hand.ejb;

import com.hand.common.CustomActivityMaintenanceApplicationInterface;

/**
 * @author Jun.Liu
 * @Description: CustomActivityMaintenanceApplicationBean EJBLocalObject接口声明
 * @date Dec 8, 2016 10:30:31 PM
 */
public interface CustomActivityMaintenanceApplication extends javax.ejb.EJBLocalObject, CustomActivityMaintenanceApplicationInterface {

}
