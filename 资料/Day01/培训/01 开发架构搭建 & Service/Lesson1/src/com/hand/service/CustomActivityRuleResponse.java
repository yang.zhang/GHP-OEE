package com.hand.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Jun.Liu
 * @Description: 客制化Service CustomActivityConfigService 输出参数
 * @date Dec 7, 2016 11:31:33 PM
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomActivityRuleResponse", propOrder = { "ruleValue" })
public class CustomActivityRuleResponse {
    private static final long serialVersionUID = 1L;

    public CustomActivityRuleResponse() {
        super();
    }

    // 作业规则值
    @XmlElement
    protected String ruleValue;

    public String getRuleValue() {
        return ruleValue;
    }

    public void setRuleValue(String ruleValue) {
        this.ruleValue = ruleValue;
    }
}
