package com.hand.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Jun.Liu
 * @Description: 客制化Service CustomActivityConfigService 输入参数
 * @date Dec 7, 2016 11:31:21 PM
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomActivityRuleRequest", propOrder = { "activityID", "ruleName" })
public class CustomActivityRuleRequest {
    private static final long serialVersionUID = 1L;

    public CustomActivityRuleRequest() {
        super();
    }

    // 作业名称
    @XmlElement
    protected String activityID;
    // 作业规则名称
    @XmlElement
    protected String ruleName;

    public String getActivityID() {
        return activityID;
    }

    public void setActivityID(String activityID) {
        this.activityID = activityID;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }
}
