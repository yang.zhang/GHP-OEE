package com.hand.service.impl;

import java.util.List;

import com.hand.service.CustomActivityRuleRequest;
import com.hand.service.CustomActivityRuleResponse;
import com.sap.me.activity.ActivityConfigurationServiceInterface;
import com.sap.me.activity.ActivityOption;
import com.sap.me.activity.FindActivityOptionsRequest;
import com.sap.me.extension.Services;
import com.sap.me.frame.Utils;
import com.sap.me.frame.domain.BusinessException;

/**
 * @author Jun.Liu
 * @Description: 客制化Service CustomActivityConfigService
 * @date Dec 7, 2016 11:31:53 PM
 */
public class CustomActivityConfigService implements CustomActivityConfigServiceInterface {
    private ActivityConfigurationServiceInterface activityService;

    /**
     * 根据作业名称及对应规则名称读取作业规则值
     */
    @Override
    public CustomActivityRuleResponse findActivtyOption(CustomActivityRuleRequest request) throws BusinessException {
        activityService = Services.getService("com.sap.me.activity", "ActivityConfigurationService");

        CustomActivityRuleResponse response = new CustomActivityRuleResponse();
        String activityID = request.getActivityID();
        String ruleName = request.getRuleName();

        // 调用API读取作业规则值列表
        FindActivityOptionsRequest findRequest = new FindActivityOptionsRequest();
        findRequest.setActivity(activityID);
        List<ActivityOption> findResponse = activityService.findActivityOptions(findRequest);
        if (!Utils.isEmpty(findResponse)) {
            // 循环列表获取ruleName对应值
            for (ActivityOption value : findResponse) {
                String option = value.getExecUnitOption();
                if (option != null && option.trim().equals(ruleName)) {
                    response.setRuleValue(value.getSetting());
                }
            }
        }

        return response;
    }

}
