package com.hand.service.impl;

import com.hand.service.CustomActivityRuleRequest;
import com.hand.service.CustomActivityRuleResponse;
import com.sap.me.frame.domain.BusinessException;

/**
 * @author Jun.Liu
 * @Description: 客制化Service CustomActivityConfigService 接口
 * @date Dec 7, 2016 11:32:25 PM
 */
public interface CustomActivityConfigServiceInterface {

    public CustomActivityRuleResponse findActivtyOption(CustomActivityRuleRequest request) throws BusinessException;
}
