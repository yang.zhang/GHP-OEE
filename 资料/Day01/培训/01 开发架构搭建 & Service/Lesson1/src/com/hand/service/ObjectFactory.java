package com.hand.service;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * @author Jun.Liu
 * @Description: 输入输出方法类XML注册
 * @date Dec 7, 2016 11:31:45 PM
 */
@XmlRegistry
public class ObjectFactory {
    public ObjectFactory() {
    }

    public CustomActivityRuleRequest createCustomActivityRuleRequest() {
        return new CustomActivityRuleRequest();
    }

    public CustomActivityRuleResponse createCustomActivityRuleResponse() {
        return new CustomActivityRuleResponse();
    }

    public CustomShopOrderRequest createCustomShopOrderRequest() {
        return new CustomShopOrderRequest();
    }

    public CustomShopOrderResponse createCustomShopOrderResponse() {
        return new CustomShopOrderResponse();
    }
}