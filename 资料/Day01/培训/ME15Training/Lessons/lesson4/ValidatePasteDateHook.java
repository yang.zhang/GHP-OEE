package com.hand.hook;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.sap.me.activity.HookContextInterface;
import com.sap.me.activity.HookContextSetter;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.production.FindCollectedSfcDataFieldsByContextRequest;
import com.sap.me.production.FindCollectedSfcDataFieldsByContextResponse;
import com.sap.me.production.SfcDataField;
import com.sap.me.production.SfcDataServiceInterface;
import com.sap.me.production.StartHookDTO;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.tc.logging.SimpleLogger;
import com.visiprise.frame.service.ext.ActivityInterface;
import com.visiprise.globalization.DateGlobalizationServiceInterface;
import com.visiprise.globalization.GlobalizationService;
import com.visiprise.globalization.KnownGlobalizationServices;

/**
 * @author Oksana Zubchenko
 * @Description Hook should be attached to PRE_START hook point
 Hook挂在开始开始之前
 Hook should validate that the date  stored in the PASTE_DATE sfc data field is expired or not 
 Hook 验证sfc中字段PASTE_DATE对应的日期是否过期
 if current time is earlier than the paste date then throw custom exception "Paste is not ready ".
 如果当前时间早于对应的值就抛出自定义异常“Paste is not Ready”
 User Argument field should be : validate = true or validate = false .if validate = false then hook validation process should be skipped 
 定义用户参数字段
 *
 */
public class ValidatePasteDateHook implements ActivityInterface<StartHookDTO>, HookContextSetter {

	private static final long serialVersionUID = 1L;
	private static final String SFC_DATA_SERVICE = "SfcDataService";
	private static final String COM_SAP_ME_PRODUCTION = "com.sap.me.production";
	private SfcDataServiceInterface sfcDataService;
	private HookContextInterface hookContext;
	private String validate = "false";
	private static final Location loc = Location.getLocation("com.hand.hook.ValidatePasteDateHook");
	private static final String MESSAGE_ID = "MEExtension:ValidatePasteDateHook";

	public void execute(StartHookDTO dto) throws Exception {
		//初始化API
		initServices();
		//日期API
		DateGlobalizationServiceInterface dateSrv = (DateGlobalizationServiceInterface) GlobalizationService.getInvariantService(KnownGlobalizationServices.DATE);
		// You can optionally use the SimpleLogger NetWeaver API to log a message to the default trace file
		//日志文件
		SimpleLogger.log(Severity.INFO, Category.SYS_SERVER, loc, MESSAGE_ID, "validate user argument = " + validate);
		// if 'validate' user argument is 'true'
		//
		if (Boolean.valueOf(validate)) {
			String pasteDate = null;
			// Read the PASTE_DATE data field for current SFC
			//读取当前SFC中的数据字段
			FindCollectedSfcDataFieldsByContextRequest sfcDataRequest = new FindCollectedSfcDataFieldsByContextRequest();
			sfcDataRequest.setSfcRef(dto.getSfcBO().getValue());
			FindCollectedSfcDataFieldsByContextResponse response = sfcDataService.findCollectedSfcDataFieldsByContext(sfcDataRequest);
			List<SfcDataField> sfcDataList = response.getSfcDataFieldList();
			//遍历数据字段，获取PASTE_DATE对应的值
			for (SfcDataField attrValue : sfcDataList) {
				// Need to read only PASTE_DATE sfc data field
				if ("PASTE_DATE".equals(attrValue.getAttribute())) {
					pasteDate = (String) attrValue.getValue();
					break;
				}
			}
			if (pasteDate == null)
				return;
			long pasteTime = getPasteTime(pasteDate);
            // if paste date is greater than current date time then throw custom exception
			//如果获取的对应的值大于当前之间，抛出Paste is not ready 异常
			if (dateSrv.createDateTime().getTimeInMillis() < pasteTime) {
				// throw custom exception
				// 20102.simple = Paste is not ready
				throw new BusinessException(20102);
			}
		}
	}

	/**
	 * Initialization of services that are represented as fields.
	 */
	 //封装初始化API的方法
	private void initServices() {
		sfcDataService = Services.getService(COM_SAP_ME_PRODUCTION, SFC_DATA_SERVICE);

	}

	/**
	 * Parses String representation of the paste date and return time in milliseconds (long).
	 * 
	 * @param pasteDate string representation of the paste date
	 * @return paste date in milliseconds
	 * @throws BusinessException if format of the paste date is not valid.
	 */
	 //时间转换
	private long getPasteTime(String pasteDate) throws BusinessException {
		try {
			return Long.parseLong(pasteDate);
		} catch (NumberFormatException e) {
			// throw custom exception
			// 20101.simple = Paste Date format is not valid
			throw new BusinessException(20101, e);
		}

	}

	/**
	 * Obtain the 'validate' value from the User Argument
	 */
	public void setHookContext(final HookContextInterface hookContext) {
		this.hookContext = hookContext;
		String userArgument = hookContext.getUserArguments();
		if (userArgument != null && userArgument.contains("=")) {
			userArgument = userArgument.toString().trim();
			//将User Argument的值以键值对的形式存放在Map中
			Map userArguments = parseUserArgument(userArgument, ";");
			// get validate user argument
			validate = (String) userArguments.get("validate");
		}

	}

	// Build Map object for all user arguments defined for hook
	public static Map parseUserArgument(String userArgument, String delimiter) {

		HashMap params = new HashMap();
		if (userArgument == null) {
			return params;
		}
		//将userArgument以；分割
		StringTokenizer tknz = new StringTokenizer(userArgument, delimiter);
		while (tknz.hasMoreTokens()) {
			String pair = tknz.nextToken();
			// suppose we get "name=value" pair
			int eqSignPos = pair.indexOf("=");
			if (eqSignPos > 0) {
				String name = pair.substring(0, eqSignPos).trim();
				String value = pair.substring(eqSignPos + 1).trim();
				params.put(name, value);
			}
		}
		return params;
	}

}
