package com.vendorID.ws.production;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.sap.me.demand.CreateShopOrderRequest;
import com.sap.me.demand.ReleaseShopOrderRequest;
import com.sap.me.demand.ReleaseShopOrderResponse;
import com.sap.me.demand.ShopOrderFullConfiguration;
import com.sap.me.demand.ShopOrderServiceInterface;
import com.sap.me.demand.ShopOrderStatus;
import com.sap.me.demand.ShopOrderType;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.frame.utils.I18nUtility;
import com.sap.me.production.CollectSfcDataRequest;
import com.sap.me.production.SfcDataField;
import com.sap.me.production.SfcDataServiceInterface;
import com.visiprise.globalization.DateGlobalizationServiceInterface;
import com.visiprise.globalization.GlobalizationService;
import com.visiprise.globalization.KnownGlobalizationServices;
import com.sap.engine.services.webservices.espbase.configuration.ann.dt.AuthenticationDT;
import com.sap.engine.services.webservices.espbase.configuration.ann.dt.AuthenticationEnumsAuthenticationLevel;
import com.sap.engine.services.webservices.espbase.configuration.ann.rt.AuthenticationRT;


@AuthenticationDT(authenticationLevel = AuthenticationEnumsAuthenticationLevel.BASIC)
@AuthenticationRT(AuthenticationMethod = "sapsp:HTTPBasic")
@WebService
public class SFCProductionService {
	@WebMethod
	public SFCProductionResponse createSfc(@WebParam(name = "SfcRequest") SFCProductionRequest sfcRequest) throws SFCProductionException {
		SFCProductionResponse response = new SFCProductionResponse();

		try {
			String site = sfcRequest.getSite();
			ShopOrderServiceInterface shopOrderService = Services.getService("com.sap.me.demand", "ShopOrderService" , site);
			SfcDataServiceInterface sfcDataService= Services.getService("com.sap.me.production", "SfcDataService", site);
			DateGlobalizationServiceInterface dateSrv = (DateGlobalizationServiceInterface) GlobalizationService.getInvariantService(KnownGlobalizationServices.DATE);

			//String itemRef = buildItem(sfcRequest.getSite(), sfcRequest.getMaterial(), sfcRequest.getVersion());
			String itemRef = new ItemBOHandle(sfcRequest.getSite(), sfcRequest.getMaterial(), sfcRequest.getVersion() ).getValue();
			CreateShopOrderRequest shopOrderRequest = new CreateShopOrderRequest();
			shopOrderRequest.setPlannedItemRef(itemRef);
			shopOrderRequest.setQuantityToBuild(BigDecimal.valueOf(1));
			shopOrderRequest.setStatus(ShopOrderStatus.Releasable);
			shopOrderRequest.setShopOrderType(ShopOrderType.PRODUCTION);
			shopOrderRequest.setPriority(new BigDecimal(500));
			// 1. Creates Shop Order
			ShopOrderFullConfiguration shopOrderFCfg = shopOrderService.createShopOrder(shopOrderRequest);

			// Release the shop order, causing SFCs to be created for the item
			ReleaseShopOrderRequest releaseShopOrderReq = new ReleaseShopOrderRequest();
			releaseShopOrderReq.setShopOrderRef(shopOrderFCfg.getShopOrderRef());
			releaseShopOrderReq.setQuantityToRelease(BigDecimal.valueOf(1));
			// 2. Releases the given shop order
			ReleaseShopOrderResponse releaseShopOrderResponse = shopOrderService.releaseShopOrder(releaseShopOrderReq);
			// Released SFCs data
			String sfcRef = releaseShopOrderResponse.getReleasedSfcList().get(0).getSfcRef();

			// Build sfc data request
			CollectSfcDataRequest collectSfcData = new CollectSfcDataRequest();
			List<String> sfcList = new ArrayList<String>();
			sfcList.add(sfcRef);
			collectSfcData.setSfcList(sfcList);
			List<SfcDataField> sfcDataFieldList = new ArrayList<SfcDataField>();
			SfcDataField sfcPasteDataField = new SfcDataField();
			sfcPasteDataField.setAttribute("PASTE_DATE");
			long expTimeLong = dateSrv.createDateTime().getTimeInMillis() + (Long.parseLong(sfcRequest.getExpTime()) * 3600 * 1000);
			sfcPasteDataField.setValue(String.valueOf(expTimeLong));
			sfcDataFieldList.add(sfcPasteDataField);

			SfcDataField sfcCommentsDataField = new SfcDataField();
			sfcCommentsDataField.setAttribute("COMMENTS");
			sfcCommentsDataField.setValue(sfcRequest.getComments());
			sfcDataFieldList.add(sfcCommentsDataField);

			collectSfcData.setSfcDataFieldList(sfcDataFieldList);
			// 3. The method logs given data against given SFCs
			sfcDataService.collectSfcData(collectSfcData);
			// Build Web Service Response
			response.setSite(sfcRequest.getSite());
			response.setSfc(sfcRef);
			response.setRouter(releaseShopOrderResponse.getRouterRef());
			response.setShopOrder(shopOrderFCfg.getShopOrderRef());

		} catch (BusinessException ex) {
			throw new SFCProductionException(I18nUtility.getMessageForException(ex));
		}
		return response;

	}
	/*private String buildItem(String site, String item, String revision) {
		StringBuffer sb = new StringBuffer();
		sb.append("ItemBO").append(':').append(site).append(',');
		sb.append(item).append(',').append(revision);
		return sb.toString();
	}*/

}
