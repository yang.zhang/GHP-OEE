package com.vendorID.ws.production;

public class SFCProductionException extends Exception {

	public SFCProductionException(String message) {
		super(message);
	}

}
