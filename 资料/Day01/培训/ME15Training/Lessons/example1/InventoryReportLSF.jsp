<%@page pageEncoding="UTF-8"%>
<%@ page language="java"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@ taglib prefix="fh" uri="http://java.sun.com/jsf/html"%>
<%@ taglib prefix="h" uri="http://java.sap.com/jsf/html"%>
<%@ taglib prefix="sap" uri="http://java.sap.com/jsf/html/extended"%>
<f:view>
	<sap:html>
	<sap:head>
		<script>
	UCF_DomUtil.attachEvent(window, 'beforeunload', windowUnload);
	

     function windowUnload() {
         if (window.parent == undefined) {
          //   window.alert('windowLoaded: window.parent is undefined');
             return;
         }
         var btnControl = document.getElementById('inventoryReportForm:windowCloseButton');
         if (btnControl) {
             btnControl.click();
         }   
     }
    
    
      function inventoryDetails() {
         var url = '/trainingwar/com/vendorID/lsf/InventoryDetailsLSF.jsf';
         var myWindow = window.open(url, 'Details', 'menubar=no height=400 width=600  scrollbars=yes status=no location=no resizable=yes');  
         setTimeout('myWindow.focus()', 100);
    }
	</script>
	</sap:head>
	<sap:body id="inventoryReportPage" focusId="inventoryReportForm"
		browserHistory="disabled" height="100%">
		
		<f:attribute name="sap-ui-ls" value="true" />
		<h:form id="inventoryReportForm">
			<sap:panel id="fieldButtonPanel" width="100%" height="100%"
				isCollapsible="false" contentAreaDesign="transparent"
				isCollapsed="false">
				<f:facet name="header">
					<h:outputText
						value="#{gapiI18nTransformer['INV_REPORT.page_title']}" />
				</f:facet>
                <sap:ajaxUpdate render="#{sap:toClientId('fieldButtonPanel')}"/>

				<sap:panelGrid width="100%" height="100%" cellHalign="start"
					cellValign="top" columns="1">
					<sap:panelGroup backgroundDesign="transparent" width="100%"
						halign="start" valign="middle" height="2em" padding="5 5 5 5">
						<!-- Message area -->						
						<fh:outputText id="messageBar"
							value="#{inventoryReportBean.message}" rendered="false" />
					</sap:panelGroup>
					<sap:panelGroup backgroundDesign="transparent" width="100%"
						halign="start" valign="top" height="2em" padding="5 5 5 5">
						<sap:panelGrid cellHalign="start" cellValign="top" width="100%"
							columns="1">
							<sap:panelGroup>
						        <!-- * Material : label  -->																						
								<fh:outputLabel value="*"
									style="font-weight: bold; font-size: 15pt;color:#FF0000"></fh:outputLabel>
								<fh:outputLabel
									value="#{gapiI18nTransformer['INV_REPORT.material.LABEL']}"
									style="font-weight: bold; font-size: 10pt;"></fh:outputLabel>
								<fh:outputLabel value=":"
									style="font-weight: bold; font-size: 10pt;"></fh:outputLabel>
						        <!-- * Material input field with browse - submitOnFieldHelp=true -->																																	
								<sap:commandInputText id="MATERIAL"
									value="#{inventoryReportBean.material}"
									submitOnFieldHelp="true" 
									actionListener="#{inventoryReportBean.showMaterialBrowse}"
									submitOnTabout="false" submitOnChange="false" >
									<f:attribute name="upperCase" value="true" />
								</sap:commandInputText>
								
								<!-- Material browse -->
								<sap:panelPopup id="materialBrowsePopup"
									rendered="#{inventoryReportBean.materialBrowseRendered}"
									mode="modeless" height="400px" width="400px">
									<f:facet name="header">
										<h:outputText value="Browse for Material" />
									</f:facet>
									<f:facet name="popupButtonArea">
										<h:panelGroup>
											<h:commandButton value="OK"
												actionListener="#{inventoryReportBean.rowSelected}"></h:commandButton>
											<h:commandButton value="Cancel"
												actionListener="#{inventoryReportBean.closeMaterialBrowse}"></h:commandButton>
										</h:panelGroup>
									</f:facet>
									<sap:panelGrid width="100%" height="100%">
										<sap:dataTable value="#{inventoryReportBean.materialList}"
											var="materialBrowseVar" id="materialBrowseTable" width="100%"
											height="100%" columnReorderingEnabled="true">
											<sap:rowSelector value="#{inventoryReportBean.selected}"
												id="rowSelector" selectionMode="single"
												selectionBehaviour="client"
												actionListener="#{inventoryReportBean.rowSelected}"
												submitOnRowDoubleClick="true" />
											<sap:column id="materialColumn" position="0">
												<f:facet name="header">
													<h:outputText id="materialHeaderText"
														value="#{gapiI18nTransformer['MATERIAL_CUSTOM_DATA.material.LABEL']}" />
												</f:facet>
												<h:outputText value="#{materialBrowseVar.material}"
													id="materialText" />
											</sap:column>
											<sap:column id="versionColumn" position="1">
												<f:facet name="header">
													<h:outputText id="versionHeaderText"
														value="#{gapiI18nTransformer['MATERIAL_CUSTOM_DATA.version.LABEL']}" />
												</f:facet>
												<h:outputText value="#{materialBrowseVar.version}"
													id="versionText" />
											</sap:column>
										</sap:dataTable>
									</sap:panelGrid>
								</sap:panelPopup>
						        <!-- * Version : label  -->																
								<sap:outputLabel value="   " />
								<fh:outputLabel value="*"
									style="font-weight: bold; font-size: 15pt;color:#FF0000"></fh:outputLabel>
								<fh:outputLabel
									value="#{gapiI18nTransformer['INV_REPORT.version.LABEL']}"
									style="font-weight: bold; font-size: 10pt;"></fh:outputLabel>
								<fh:outputLabel value=":"
									style="font-weight: bold; font-size: 10pt;"></fh:outputLabel>
						        <!-- * Version input field  -->																										
								<sap:commandInputText id="VERSION" submitOnChange="false"
									submitOnFieldHelp="false"
									value="#{inventoryReportBean.version}">
									<f:attribute name="upperCase" value="true" />
								</sap:commandInputText>

							</sap:panelGroup>
						</sap:panelGrid>
					</sap:panelGroup>

					<sap:panelGroup backgroundDesign="transparent" width="100%"
						halign="start" valign="top" height="2em" padding="5 5 5 5">
						<sap:panelGrid cellHalign="start" cellValign="top" width="100%" columns="1">
							<sap:panelGroup>
								<fh:outputLabel value="Inv.Status" 
									style="font-weight: bold; font-size: 10pt;"></fh:outputLabel>
								<fh:outputLabel value=":" 
									style="font-weight: bold; font-size: 10pt;"></fh:outputLabel>
								<sap:commandOneMenu id="INVENTORY_STATUS" 
									value="#{inventoryReportBean.inventoryStatus}">
									<f:selectItem itemValue="ALL" itemLabel="All" />
									<f:selectItem itemValue="AVAILABLE" itemLabel="Available" />
									<f:selectItem itemValue="HOLD" itemLabel="Hold" />
									<f:selectItem itemValue="QUARANTINE" itemLabel="Quarantine" />
								</sap:commandOneMenu>
							</sap:panelGroup>
						</sap:panelGrid>
					</sap:panelGroup>

					<sap:panelGroup backgroundDesign="transparent" width="100%"
						halign="center" valign="top" height="3em" padding="5 5 5 5">
						<!-- 'Retrieve' button  -->												
						<sap:commandButtonLarge id="RETRIEVE"
							value="#{gapiI18nTransformer['retrieve.default.BUTTON']}"
							width="85px" height="20px"
							action="#{inventoryReportBean.readInventory}">
							<sap:ajaxUpdate />
						</sap:commandButtonLarge>
						<sap:outputLabel value="   " />
						<!-- 'Clear' button  -->						
						<sap:commandButtonLarge id="CLEAR"
							value="#{gapiI18nTransformer['INV_REPORT.clear.text.BUTTON']}"
							width="90px" imagePosition="start" height="20px"
							action="#{inventoryReportBean.clear}">
							<sap:ajaxUpdate />
						</sap:commandButtonLarge>
						<!-- Hidden close window button  -->
						<sap:commandButtonLarge id="windowCloseButton" value=""
							width="0px" height="0px"
							action="#{inventoryReportBean.processWindowClosed}">
							<sap:ajaxUpdate />
						</sap:commandButtonLarge>
					</sap:panelGroup>

					<sap:panel id="displayPanel" width="100%" height="100%"
						isCollapsible="false" contentAreaDesign="transparent"
						isCollapsed="false">

						<f:facet name="header">
							<h:outputText value="Inventory Report Result" />
						</f:facet>
						
						<sap:ajaxUpdate render="#{sap:toClientId('displayPanel')}"/>
						
						<sap:panelGrid width="100%" height="100%" cellHalign="start"
							cellValign="top" columns="1">

							<sap:panelGroup backgroundDesign="transparent" width="100%"
								halign="middle" valign="top">
								<sap:dataTable binding="#{inventoryTableBeanConfigurator.table}"
									value="#{inventoryReportBean.inventoryList}" first="0"
									var="rows" id="materialCustomDataTable" width="100%"
									height="100%" columnReorderingEnabled="true" rendered="true">
								</sap:dataTable>
							</sap:panelGroup>
						</sap:panelGrid>
					</sap:panel>
				</sap:panelGrid>
			</sap:panel>
		</h:form>
	</sap:body>
	</sap:html>
</f:view>
