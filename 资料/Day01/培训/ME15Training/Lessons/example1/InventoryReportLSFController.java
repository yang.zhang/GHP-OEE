package com.vendorID.lsf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.el.MethodExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import com.sap.me.common.ObjectReference;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.frame.domain.DomainServiceFactory;
import com.sap.me.frame.domain.DomainServiceInterface;
import com.sap.me.frame.service.CommonMethods;
import com.sap.me.frame.utils.I18nUtility;
import com.sap.me.inventory.domain.InventoryDO;
import com.sap.me.productdefinition.domain.ItemDO;
import com.sap.me.status.StatusBasicConfiguration;
import com.sap.me.status.StatusServiceInterface;
import com.sap.me.wpmf.BaseManagedBean;
import com.sap.me.wpmf.TableColumnSortListener;
import com.sap.me.wpmf.TableConfigurator;
import com.sap.me.wpmf.TableSortComparator;
import com.sap.me.wpmf.convert.GapiDateTimeConverter;
import com.sap.me.wpmf.util.FacesUtility;
import com.sap.ui.faces.component.html.HtmlCommandButton;
import com.sap.ui.faces.component.sap.UIColumn;
import com.sap.ui.faces.event.sap.SortActionEvent;

public class InventoryReportLSFController extends BaseManagedBean implements TableColumnSortListener {

	private String material;
	private String version;
	private String message;
	private List<InventoryItem> inventoryList;
	private TableConfigurator tableConfigBean = null;
	private UIColumn currentSortColumn = null;
	private String inventoryStatus = "ALL";
	StatusServiceInterface statusService = Services.getService("com.sap.me.status", "StatusService");
	DomainServiceInterface<InventoryDO> inventoryDOService = DomainServiceFactory.getServiceByClass(InventoryDO.class);
	String[] columnDefs = new String[] { "inventory;INV_REPORT.inventory.LABEL", "qty;INV_REPORT.qtyOnHand.LABEL", "status;INV_REPORT.status.LABEL", "hasBeenUsed;INV_REPORT.hasBeenUsed.LABEL",
			"createdDate;INV_REPORT.createdDate.LABEL", "details;INV_REPORT.details.LABEL" };
	String[] listColumnNames = new String[] { "INVENTORY", "QTY", "STATUS", "HAS_BEEN_USED", "CREATED_DATE", "DETAILS" };
	private Boolean materialBrowseRendered;
	private boolean selected;
	private List<MaterialItem> materialList = new ArrayList<MaterialItem>();
	DomainServiceInterface<ItemDO> itemDOService = DomainServiceFactory.getServiceByClass(ItemDO.class);


	public InventoryReportLSFController() {
		// Initialize Material Browse list
		initMaterialList();
	}
// Clear button action
	public void clear() {
//Removes an Object from the session map 
		FacesUtility.removeSessionMapValue("inventoryReportBean");
		FacesUtility.removeSessionMapValue("inventoryDetailsBean_INVENTORY");
// Clear message bar
		displayMessageBar(false);
// findComponent() - Helper method to find a component given an input parent component and id of component to find 
// getViewRoot() - Return the root component that is associated with this request
		UIComponent tablePanel = findComponent(FacesUtility.getFacesContext().getViewRoot(), "inventoryReportForm:displayPanel");
		if (tablePanel != null) {
//Adds UIComponent to the list of controls that are re-rendered in the request
			FacesUtility.addControlUpdate(tablePanel);
		}
	}

	public void displayMessageBar(boolean render) {
		// findComponent() - Helper method to find a component given an input parent component and id of component to find
		HtmlOutputText messageBar = (HtmlOutputText) findComponent(FacesUtility.getFacesContext().getViewRoot(), "inventoryReportForm:messageBar");
		// Display message or not on GUI
		messageBar.setRendered(render);
		messageBar.setStyle("color:#0000FF;font-weight:bold;font-size:10pt;");
		UIComponent fieldButtonPanel = findComponent(FacesUtility.getFacesContext().getViewRoot(), "inventoryReportForm:fieldButtonPanel");
		if (fieldButtonPanel != null) {
			//Adds UIComponent to the list of controls that are re-rendered in the request
			FacesUtility.addControlUpdate(fieldButtonPanel);
		}
	}
	
	
	// Activity closed , window unload
	public void processWindowClosed() {
		//Removes an Object from the session map 
		FacesUtility.removeSessionMapValue("materialBrowseBean");
		FacesUtility.removeSessionMapValue("inventoryReportBean");
		FacesUtility.removeSessionMapValue("inventoryDetailsBean_INVENTORY");
		FacesUtility.removeSessionMapValue("inventoryDetailsBean");
	}

   // Returns Table Configurator object 
	public TableConfigurator getTableConfigBean() {
		return tableConfigBean;
	}

	public void setTableConfigBean(TableConfigurator config) {
		this.tableConfigBean = config;
	 	//Get the column to bean property bindings. (e.g. column Foo corresponds to property foo in the bean) 
		// Execute this code when first time opened activity
		if (tableConfigBean.getColumnBindings() == null || tableConfigBean.getColumnBindings().size() < 1) {
			//set the Name of the List specified in List Maintenance
			tableConfigBean.setListName(null);
			//Bind table column with property from Java Bean class
			tableConfigBean.setColumnBindings(getColumnFieldMapping());
			//Sets the column names for the table 
			tableConfigBean.setListColumnNames(listColumnNames);
			//Sets the flag indicating if row selection is allowed. 
			tableConfigBean.setAllowSelections(false);
			//Sets the flag indicating if  multi selection is allowed 
			tableConfigBean.setMultiSelectType(false);
			
			GapiDateTimeConverter dateTimeConv = new GapiDateTimeConverter();
			// Supported format types : "both" , "date" , "time" 
			dateTimeConv.setFormatType("both");
			// "long" or "short"
			dateTimeConv.setDateStyle("short");
			// "long" or "short"
			dateTimeConv.setTimeStyle("short");
			// Set GapiDateTimeConverter converter for the CREATED_DATE column 
			tableConfigBean.setColumnConverter(dateTimeConv, "CREATED_DATE");

			// create a cell renderer here. Let's try something similar to a Details column
			tableConfigBean.setCellRenderer("DETAILS", new HtmlCommandButton());
			//configure the render cell for this table for "DETAILS" column
			configureCellRenderer("DETAILS");

            // Create the list of sortable column names
			List<String> sortableCols = new ArrayList<String>();
			sortableCols.add("INVENTORY");
			sortableCols.add("QTY");
			
		//	Set the list of sortable column names 
			tableConfigBean.setSortableColumns(sortableCols);
	     // Add a TableColumnSortListener to this configurator
			tableConfigBean.addColumnSortListener(this);
			//Configure the table using the predefined values for column titles and bindings, etc
			tableConfigBean.configureTable();

		}
	}
	protected HashMap<String, String> getColumnFieldMapping() {
		//String[] columnDefs = new String[] { "inventory;INV_REPORT.inventory.LABEL"};
		//String[] listColumnNames = new String[] { "INVENTORY"};
		HashMap<String, String> columnFieldMap = new HashMap<String, String>();
		String[] columns = columnDefs;
		for (int i = 0; i < columns.length; i++) {
			columnFieldMap.put(listColumnNames[i], columns[i]);
		}
		return columnFieldMap;
	}
	 /**
     * configure the render cell for this table for given column.
     * @param colName
     */
	private void configureCellRenderer(String colName) {
		if (colName.equals("DETAILS")) {
			//Get the current Cell renderer for the given column
			HtmlCommandButton detailComp = (HtmlCommandButton) tableConfigBean.getCellRenderer(colName);
			String url =  "/com/vendorID/icons/icon_detail.gif";
			detailComp.setImage(url);
			detailComp.setAlt("Details");
			String binding = "#{inventoryReportBean.processDetailsAction}";
			//Create a method expression 
			//createMethodExpression(String bindingString,Class returnType,Class[] args)
			MethodExpression lstnr = FacesUtility.createMethodExpression(binding, null, new Class[0]);
			//Set the MethodExpression pointing at the application action to be invoked
			detailComp.setActionExpression(lstnr);

		}
	}
    // Click Detail image 
	public void processDetailsAction() {
		//Return the data object representing the data for the currently selected row index, if any.
		InventoryItem rowData = (InventoryItem) tableConfigBean.getTable().getRowData();
		FacesUtility.removeSessionMapValue("inventoryDetailsBean");
		//Sets an Object into the session map 
		FacesUtility.setSessionMapValue("inventoryDetailsBean_INVENTORY", rowData.getInventory());
		//Appends JavaScripts that will be executes on the client after the response is processed
		//Java Script function in InventoryReportLSF.jsp
		FacesUtility.addScriptCommand("window.inventoryDetails();");
	}
 	
	// Clicked Retrieve button
	public void readInventory() {
		message = null;
		inventoryList = new ArrayList<InventoryItem>();
		// Clear message bar
		displayMessageBar(false);
		try {
			String site = CommonMethods.getSite();
			InventoryDO inventoryDO = new InventoryDO();
			inventoryDO.setSite(site);
			String itemRef = buildItem( site, material, version );
			inventoryDO.setItemRef(itemRef);
			String status = getInventoryStatus();
			String statusRef = null;
			if ("HOLD".equals(status))
				statusRef = buildStatusHandle(site, "1002");
			else if ("AVAILABLE".equals(status))
				statusRef = buildStatusHandle(site, "1001");
			else if ("QUARANTINE".equals(status))
				statusRef = buildStatusHandle(site, "1003");
			if (statusRef != null)
				inventoryDO.setStatusRef(statusRef.toString());
			Collection<InventoryDO> inventoryDOList = inventoryDOService.readByExample(inventoryDO);
            // Build report data to display 
			for (InventoryDO inventory : inventoryDOList) {
				InventoryItem invItem = new InventoryItem();
				invItem.setInventory(inventory.getInventoryId());
				invItem.setQty(inventory.getQtyOnHand());
				StatusBasicConfiguration statusBasicConfiguration = statusService.findStatusByRef(new ObjectReference(inventory.getStatusRef()));
				invItem.setStatus(statusBasicConfiguration.getStatusDescription());
				invItem.setCreatedDate(inventory.getReceiveDateTime());
				if (inventory.isHasBeenUsed() != null)
					invItem.setHasBeenUsed(inventory.isHasBeenUsed().booleanValue());
				else
					invItem.setHasBeenUsed(false);
				inventoryList.add(invItem);
			}
			if (inventoryList.size() == 0) {
				//noRecords.MESSAGE = Your query returned no data
				message = FacesUtility.getLocaleSpecificText("noRecords.MESSAGE");
				displayMessageBar(true);

			}
		} catch (BusinessException e) {
			message = I18nUtility.getMessageForException(e);
			displayMessageBar(true);

		}
		// Make sure you add the table to the list of control updates so that
		// the new model value will be shown on the UI.
		UIComponent tablePanel = findComponent(FacesUtility.getFacesContext().getViewRoot(), "inventoryReportForm:displayPanel");
		if (tablePanel != null) {
			//Adds UIComponent to the list of controls that are re-rendered in the request
			FacesUtility.addControlUpdate(tablePanel);
		}

	}
	public void processColumnSort(SortActionEvent sortEvent) {
		//The object on which the Event initially occurred
		currentSortColumn = (UIColumn) sortEvent.getSource();
		inventoryList = performUserPreferredSorting((List<InventoryItem>) tableConfigBean.getTable().getValue());
		// Make sure you add the table to the list of control updates so that
		// the sorted model value will be shown on the UI.
		UIComponent tablePanel = findComponent(FacesUtility.getFacesContext().getViewRoot(), "inventoryReportForm:displayPanel");
		if (tablePanel != null) {
			FacesUtility.addControlUpdate(tablePanel);
		}
	}

	/**
	 * Process User preferred column sorting based on the last sorted column
	 * detail/order.
	 * 
	 * @param worklist
	 * @return ArrayList The sorted worklist
	 */
	@SuppressWarnings("unchecked")
	private List performUserPreferredSorting(List inventoryItemList) {
		if (inventoryItemList == null)
			return inventoryItemList;
		if (currentSortColumn == null) {
			// If null, no user sort was performed in the session.
			return inventoryItemList;
		}
		String columnName = currentSortColumn.getId();
		if (columnName == null)
			return inventoryItemList;
		if (!currentSortColumn.isSortable())
			return inventoryItemList;
		String sortState = currentSortColumn.getSortState();
		if (sortState == null)
			sortState = "ascending";
		String bindingProperty = tableConfigBean.getColumnBindingProperty(columnName);
		Collections.sort(inventoryItemList, new TableSortComparator<InventoryItem>(bindingProperty, TableSortComparator.SortState.valueOf(sortState)));
		return inventoryItemList;
	}


	public void setInventoryList(List<InventoryItem> inventoryList) {
		this.inventoryList = inventoryList;
	}

	public List<InventoryItem> getInventoryList() {
		return inventoryList;
	}

	public String getMaterial() {

		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getVersion() {

		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getMessage() {

		return message;
	}

	public void setMessage(String message) {

		this.message = message;
	}

	public String getInventoryStatus() {

		return inventoryStatus;
	}

	public void setInventoryStatus(String inventoryStatus) {
		this.inventoryStatus = inventoryStatus;
	}
	private String buildItem(String site, String item, String revision) {
		StringBuffer sb = new StringBuffer();
		sb.append("ItemBO").append(':').append(site).append(',');
		sb.append(item).append(',').append(revision);
		return sb.toString();
	}
	
	private String buildStatusHandle(String site, String status) {
		StringBuffer sb = new StringBuffer();
	    sb.append("StatusBO").append(":").append(site).append(",");
	    sb.append(status);
		return sb.toString();
	}
	
	// Read all materials for current site
	public void initMaterialList() {
		this.materialList = new ArrayList<MaterialItem>();
		String site = CommonMethods.getSite();
		ItemDO itemDO = new ItemDO();
		itemDO.setSite(site);
		// Get all items on the current site
		Collection<ItemDO> materialDOList = itemDOService.readByExample(itemDO);
		for (ItemDO materialDO : materialDOList) {
			MaterialItem materailItem = new MaterialItem();
			materailItem.setMaterial(materialDO.getItem());
			materailItem.setVersion(materialDO.getRevision());
			materailItem.setSelected(new Boolean(false));
			materialList.add(materailItem);
		}
	}
	public boolean isSelected() {

		MaterialItem currentRow  = getCurrentSelectedRowMaterailBrowseTable();
		if (currentRow != null) {
			return currentRow.getSelected().booleanValue();
		}
		return false;
	}
	/**
	 * @nooverride
	 * @param selected set the selection of the current row
	 */
	public void setSelected(boolean selected) {
		MaterialItem currentRow = getCurrentSelectedRowMaterailBrowseTable();
		currentRow.setSelected(new Boolean(selected));
	}

	private MaterialItem getCurrentSelectedRowMaterailBrowseTable() {

		Map<String, Object> requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
		MaterialItem currentRow = (MaterialItem) requestMap.get("materialBrowseVar");
		return currentRow;
	}
	public void rowSelected(ActionEvent event) {

		closeMaterialBrowse(event);
		// set value for input field
		MaterialItem selectedMaterial = getSelectedMaterial();
		if (selectedMaterial != null) {
			this.material = selectedMaterial.getMaterial();
			this.version = selectedMaterial.getVersion();
		}
	}
	private MaterialItem getSelectedMaterial() {
		MaterialItem selectedMaterialItem = null;
		if (materialList != null) {
			for (int i = 0; i < this.materialList.size(); i++) {
				MaterialItem materialItem = materialList.get(i);
				if (materialItem != null && materialItem.getSelected()) {
					selectedMaterialItem = materialItem;
					break;
				}
			}
		}
		return selectedMaterialItem;
	}
	public List<MaterialItem> getMaterialList() {
		return this.materialList;
	}


	public void showMaterialBrowse(ActionEvent event) {

		this.materialBrowseRendered = true;
	}

	public void closeMaterialBrowse(ActionEvent event) {

		this.materialBrowseRendered = false;
	}
	public Boolean getMaterialBrowseRendered() {

		return materialBrowseRendered;
	}

	public void setMaterialBrowseRendered(Boolean materialBrowseRendered) {
		this.materialBrowseRendered = materialBrowseRendered;
	}

}