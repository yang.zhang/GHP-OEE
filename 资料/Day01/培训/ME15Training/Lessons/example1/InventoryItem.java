package com.vendorID.lsf;

import java.math.BigDecimal;

import com.visiprise.globalization.util.DateTimeInterface;

public class InventoryItem {

	private String inventory;
	private BigDecimal qty;
	private String status;
	private DateTimeInterface createdDate ;
	private boolean hasBeenUsed = false;
	private boolean details = true;

	public boolean isDetails() {
		return details;
	}

	
	public void setDetails(boolean details) {
		this.details = details;
	}

	public boolean isHasBeenUsed() {
		return hasBeenUsed;
	}

	public void setHasBeenUsed(boolean hasBeenUsed) {
		this.hasBeenUsed = hasBeenUsed;
	}

	
	public void setInventory(String inventory) {
		this.inventory = inventory;
	}

	public String getInventory() {
		return inventory;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public BigDecimal getQty() {
		return qty;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
	
	public DateTimeInterface getCreatedDate() {
		return createdDate;
	}

	
	public void setCreatedDate(DateTimeInterface createdDate) {
		this.createdDate = createdDate;
	}

}
