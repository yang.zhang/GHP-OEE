package com.vendorID.lsf;

import com.sap.me.wpmf.util.FacesUtility;

public class InventoryDetailsBean {

	private String detailsString;

	public InventoryDetailsBean() {
		detailsString = (String) FacesUtility.getSessionMapValue("inventoryDetailsBean_INVENTORY");

	}

	public void close() {
		FacesUtility.removeSessionMapValue("inventoryDetailsBean_INVENTORY");
		FacesUtility.removeSessionMapValue("inventoryDetailsBean");
		//Appends JavaScripts that will be executes on the client after the response is processed
		// Close the current window
		FacesUtility.addScriptCommand("window.close();");

	}

	public String getDetailsString() {

		return "You want to look details for INVENTORY " + detailsString;
	}

	public void setDetailsString(String detailsString) {
		this.detailsString = detailsString;
	}

}

