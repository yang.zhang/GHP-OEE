<%@page pageEncoding="UTF-8"%>
<%@ page language="java"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@ taglib prefix="fh" uri="http://java.sun.com/jsf/html"%>
<%@ taglib prefix="h" uri="http://java.sap.com/jsf/html"%>
<%@ taglib prefix="sap" uri="http://java.sap.com/jsf/html/extended"%>
<f:view>

	<sap:html>
	<sap:head>
		<script>
	window.focus();
	
</script>
	</sap:head>

	<sap:body id="invendotryDetailId" focusId="invDetailForm"
		browserHistory="disabled" height="100%">
		<h:form id="invDetailForm">
			<sap:panel id="displayPanel" width="100%" height="100%"
				isCollapsible="false" contentAreaDesign="transparent"
				isCollapsed="false">
				<f:facet name="header">
					<h:outputText value="Inventory Details" />
				</f:facet>
				<sap:ajaxUpdate render="#{sap:toClientId('displayPanel')}"/>
				<sap:panelGrid width="100%" height="100%" cellHalign="start"
					cellValign="top" columns="1">
					<sap:panelGroup backgroundDesign="transparent" width="100%"
						halign="start" valign="top">
						<sap:outputTextFormatted id="textViewControl"
							value="#{inventoryDetailsBean.detailsString}" />
					</sap:panelGroup>
					<sap:panelGroup backgroundDesign="transparent" width="100%"
						halign="center" valign="top" height="1em" padding="5 5 5 5">
						<sap:commandButtonLarge id="close" value="Close" width="80px"
							height="20px" action="#{inventoryDetailsBean.close}">
							<sap:ajaxUpdate />
						</sap:commandButtonLarge>
					</sap:panelGroup>
				</sap:panelGrid>
			</sap:panel>
		</h:form>
	</sap:body>
	</sap:html>
</f:view>

