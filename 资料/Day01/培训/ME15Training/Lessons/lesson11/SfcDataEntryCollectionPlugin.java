package com.vendorID.wpmf.web.podplugin;

import java.util.ArrayList;
import java.util.List;

import com.sap.me.activity.ActivityConfigurationServiceInterface;
import com.sap.me.activity.ActivityOption;
import com.sap.me.activity.FindActivityOptionsRequest;
import com.sap.me.common.ObjectReference;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.production.CollectSfcDataRequest;
import com.sap.me.production.SfcDataField;
import com.sap.me.production.SfcDataServiceInterface;
import com.sap.me.production.podclient.SfcSelection;
import com.sap.me.wpmf.MessageType;
import com.sap.me.wpmf.util.ExceptionHandler;
import com.sap.me.wpmf.util.MessageHandler;
import com.visiprise.globalization.DateGlobalizationServiceInterface;
import com.visiprise.globalization.GlobalizationService;
import com.visiprise.globalization.KnownGlobalizationServices;

//extend BasePodPlugin , base class for all the SAPME Pod plugins
public class SfcDataEntryCollectionPlugin  extends com.sap.me.production.podclient.BasePodPlugin {
	//The "execute()" method is the only method that must be implemented for use in the WPMF
	public void execute() throws Exception {
		try {
			DateGlobalizationServiceInterface dateSrv = (DateGlobalizationServiceInterface) GlobalizationService.getInvariantService(KnownGlobalizationServices.DATE);
			SfcDataServiceInterface sfcDataService = Services.getService("com.sap.me.production", "SfcDataService");

			//getActivityID() - Returns the Activity ID for the Plugin instance 
			List<ActivityOption> activityOptions = getActivityOptions(getActivityID());
			// default value
			String expTime = "4";
			for (ActivityOption activityOption : activityOptions) {
				if ("EXP_TIME".equals(activityOption.getExecUnitOption())) {
					expTime = activityOption.getSetting();
					break;
				}
			}
			//Returns a resolved list of SFC selections based on the selection criteria
			List<SfcSelection> selectionList = getPodSelectionModel().getResolvedSfcs();
			if (selectionList == null || selectionList.isEmpty()) {
				// Display Error on the POD
				//20105.simple = Select Valid SFCs to execute Non-UI Sfc Data POD Plugin
				MessageHandler.handle("20105.simple", null, MessageType.ERROR);
				return;
			}
			CollectSfcDataRequest collectSfcData = new CollectSfcDataRequest();
			List<String> sfcList = new ArrayList<String>();
			for (SfcSelection sfc : selectionList) {
				if (sfc.getSfc() == null)
					continue;
				ObjectReference sfcHandle = new ObjectReference(sfc.getSfc().getSfcRef());
				sfcList.add(sfcHandle.getRef());
				
			}
			if (sfcList.isEmpty()) {
        		// Use main message area Display Error on the POD
				MessageHandler.handle("20105.simple", null, MessageType.ERROR);
				return;
			}
			// Set valid SFCs
			collectSfcData.setSfcList(sfcList);
			
			List<SfcDataField> sfcDataFieldList = new ArrayList<SfcDataField>();
			SfcDataField sfcPasteDataField = new SfcDataField();
			sfcPasteDataField.setAttribute("PASTE_DATE");
			long expTimeLong = dateSrv.createDateTime().getTimeInMillis() + (Long.parseLong(expTime) * 3600 * 1000);
			sfcPasteDataField.setValue(String.valueOf(expTimeLong));
			sfcDataFieldList.add(sfcPasteDataField);

			SfcDataField sfcCommentsDataField = new SfcDataField();
			sfcCommentsDataField.setAttribute("COMMENTS");
			sfcCommentsDataField.setValue("Write Paste Date from POD Non-UI Plugin");
			sfcDataFieldList.add(sfcCommentsDataField);

			collectSfcData.setSfcDataFieldList(sfcDataFieldList);
			//The method logs given data against given SFCs. 
			sfcDataService.collectSfcData(collectSfcData);

			// To display  message in the POD message area use the following
			//sfcDataPlugin.success.message = Non-UI Sfc Data POD Plugin Executed Successfully
			MessageHandler.handle("sfcDataPlugin.success.message", null, MessageType.SUCCESS);
			// called to end processing of plugin and start next plugin in chain
			complete();
		} catch (BusinessException bEx) {
			// To display exception to the POD message area use the following
			ExceptionHandler.handle(bEx);
		}

	}

	private List<ActivityOption> getActivityOptions(String activityId) throws BusinessException {
		ActivityConfigurationServiceInterface activityConfigService = Services.getService("com.sap.me.activity", "ActivityConfigurationService");
		FindActivityOptionsRequest request = new FindActivityOptionsRequest();
		request.setActivity(activityId);
//		Returns a collection of the activity options for specified activity configuration that is defined uniquely by activity Id. 
		List<ActivityOption> response = activityConfigService.findActivityOptions(request);
		return response;
	}


}
