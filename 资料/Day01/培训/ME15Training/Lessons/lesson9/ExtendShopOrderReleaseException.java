package com.vendorID.serviceext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.sap.me.activity.ServiceExtension;
import com.sap.me.demand.ReleaseShopOrderRequest;
import com.sap.me.demand.ShopOrderQuantityException;
import com.vendorID.exception.ShopOrderReleaseQtyException;
import com.visiprise.frame.service.ext.InvocationContext;
import com.visiprise.frame.service.ext.InvocationContextSetter;
import com.visiprise.frame.service.ext.TransactionContextInterface;
import com.visiprise.frame.service.ext.TransactionContextSetter;

//public ReleaseShopOrderResponse releaseShopOrder(ReleaseShopOrderRequest request)
//throws  ShopOrderNotFoundException,
//	    ShopOrderItemException,
//	    ShopOrderInputException,
//	    ShopOrderStatusException,
//	    ShopOrderQuantityException,
//	    RepetitiveOrderDueException,
//	    ShopOrderRmaException,
//	    ShopOrderUpdateException,
//	    ShopOrderRecursionException,
//	    ShopOrderItemStatusException,
//	    ShopOrderItemNewException,
//	    ShopOrderItemTypeException,
//	    ShopOrderBomStatusException,
//	    ShopOrderBomNewException,
//	    WorkCenterRequiredException,
//	    WorkCenterPermitException,
//	    ItemRouterException,
//	    RouterStatusException,
//	    OperationStatusException,
//	    SfcCountException,
//	    UsedSfcException,
//	    BusinessException;

public class ExtendShopOrderReleaseException extends ServiceExtension<Object> implements InvocationContextSetter, TransactionContextSetter {

	private static final long serialVersionUID = 1L;
	private InvocationContext invocationContext;
	private TransactionContextInterface transactionContext;

	@Override
	public void execute(Object exception) throws Exception {
		if (exception instanceof ShopOrderQuantityException) {
			execute((ShopOrderQuantityException) exception);
		}
	}

	public void execute(ShopOrderQuantityException exception) throws Exception {
		if (invocationContext.getInput() instanceof ReleaseShopOrderRequest) {
			ReleaseShopOrderRequest releaseShopOrderRequest = (ReleaseShopOrderRequest) invocationContext.getInput();
			String shopOrderRef = releaseShopOrderRequest.getShopOrderRef();
			BigDecimal qtyToRelease = releaseShopOrderRequest.getQuantityToRelease();
            // get shop order value from shop order handle
			String shopOrderValue = getShopOrder(shopOrderRef);
			
			invocationContext.setException(new ShopOrderReleaseQtyException(shopOrderValue, qtyToRelease));

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.visiprise.frame.service.ext.InvocationContextSetter#setInvocationContext(com.visiprise.frame.service.ext.InvocationContext)
	 */
	public void setInvocationContext(InvocationContext context) {
		invocationContext = context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.visiprise.frame.service.ext.TransactionContextSetter#setTransactionContext(com.visiprise.frame.service.ext.TransactionContextInterface)
	 */
	public void setTransactionContext(TransactionContextInterface context) {
		transactionContext = context;
	}

	private String getShopOrder(String input) {
		ArrayList<String> list = new ArrayList<String>();
		StringTokenizer t = new StringTokenizer(input, ":,");
		// Get all of the pieces
		while (t.hasMoreTokens()) {
			// Add them to a temp list
			list.add(t.nextToken());
		}
		return (String) list.get(2);
	}

}