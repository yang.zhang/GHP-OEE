package com.vendorID.exception;

import java.math.BigDecimal;

import com.sap.me.frame.domain.BusinessException;

public class ShopOrderReleaseQtyException extends BusinessException {

	private String shopOrder;
	private BigDecimal qty;
	
	public ShopOrderReleaseQtyException(String shopOrder, BigDecimal qty) {
		super(20104);
		this.shopOrder = shopOrder;
		this.qty = qty;
	}	
	
	public String getShopOrder(){
		return shopOrder;
	}
	
	public BigDecimal getQty(){
		return qty;
	}		
}