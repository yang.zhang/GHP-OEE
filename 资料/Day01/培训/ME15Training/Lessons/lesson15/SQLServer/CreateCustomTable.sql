CREATE TABLE SAPDEV_CODE_XX (
    PLANT_CODE          nvarchar(10)   NOT NULL ,
    SEQUENCE            numeric(38, 0) NOT NULL ,
    LAST_USED_DATE_TIME datetime       NOT NULL 
)
go