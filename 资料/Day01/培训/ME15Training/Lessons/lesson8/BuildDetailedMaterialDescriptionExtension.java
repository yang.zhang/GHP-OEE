package com.vendorID.serviceext;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.sap.me.activity.ServiceExtension;
import com.sap.me.common.ObjectReference;
import com.sap.me.extension.Services;
import com.sap.me.productdefinition.ItemBasicConfiguration;
import com.sap.me.productdefinition.ItemConfigurationServiceInterface;
import com.sap.me.production.RetrieveWorklistItemsRequest;
import com.sap.me.production.WorklistItem;
import com.visiprise.frame.service.ext.InvocationContext;
import com.visiprise.frame.service.ext.InvocationContextSetter;
import com.visiprise.frame.service.ext.TransactionContextInterface;
import com.visiprise.frame.service.ext.TransactionContextSetter;

//public List<WorklistItem> findWorklistItems(RetrieveWorklistItemsRequest request) throws BusinessException;

public class BuildDetailedMaterialDescriptionExtension extends ServiceExtension<Object> implements InvocationContextSetter, TransactionContextSetter {

	private static final long serialVersionUID = 1L;
	private static final String ITEM_CONFIGURATION_SERVICE = "ItemConfigurationService";
	private static final String COM_SAP_ME_PRODUCTDEFINITION = "com.sap.me.productdefinition";
	private ItemConfigurationServiceInterface itemConfigurationService;
	private InvocationContext invocationContext;
	private TransactionContextInterface transactionContext;

	
	@Override
	public void execute(Object request) throws Exception {
		if (request instanceof RetrieveWorklistItemsRequest) {
			execute((RetrieveWorklistItemsRequest) request);
		}

	}

	public void execute(RetrieveWorklistItemsRequest request) throws Exception {
		initServices();
		if (invocationContext.getResult() instanceof List) {
			// get findWorklistItems() method return result
			List<WorklistItem> worklistResult = (List<WorklistItem>) invocationContext.getResult();
			for (WorklistItem worklistItem : worklistResult) {
				// Return basic item configuration retrieved by the passed reference (object ID).
				ItemBasicConfiguration itemBasicConfig = itemConfigurationService.findItemConfigurationByRef(new ObjectReference(worklistItem.getItemRef()));
				String unitOfMeasure = itemBasicConfig.getUnitOfMeasurement();
				String description = itemBasicConfig.getDescription();
				BigDecimal lotSize = itemBasicConfig.getLotSize();
				//Build item description = Description / Unit of Measure / Lot Size
				worklistItem.setItemDescription(description + "/" + unitOfMeasure + "/" + lotSize);
			}
			// sort by shopOrderRef
			Collections.sort(worklistResult, new Comparator<WorklistItem>() {
				public int compare(WorklistItem o1, WorklistItem o2) {
					String shopOrderRef1 = ((WorklistItem) o1).getShopOrderRef();
					String shopOrderRef2 = ((WorklistItem) o2).getShopOrderRef();
					return shopOrderRef1.compareTo(shopOrderRef2);
				}
			});
			//override findWorklistItems() method return result
			invocationContext.setResult(worklistResult);
		}
	}

	/**
	 * Initialization of services that are represented as fields.
	 */
	private void initServices() {
		itemConfigurationService = Services.getService(COM_SAP_ME_PRODUCTDEFINITION, ITEM_CONFIGURATION_SERVICE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.visiprise.frame.service.ext.InvocationContextSetter#setInvocationContext(com.visiprise.frame.service.ext.InvocationContext)
	 */
	public void setInvocationContext(InvocationContext context) {
		invocationContext = context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.visiprise.frame.service.ext.TransactionContextSetter#setTransactionContext(com.visiprise.frame.service.ext.TransactionContextInterface)
	 */
	public void setTransactionContext(TransactionContextInterface context) {
		transactionContext = context;
	}

	
}
