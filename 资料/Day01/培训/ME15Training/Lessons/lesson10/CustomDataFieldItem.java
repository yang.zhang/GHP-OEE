package com.vendorID.customdata;


public class CustomDataFieldItem {
	private String dataField;
	private String description;
	
	public void setDataField(String dataField) {
		this.dataField = dataField;
	}
	public String getDataField() {
		return dataField;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDescription() {
		return description;
	}

}

