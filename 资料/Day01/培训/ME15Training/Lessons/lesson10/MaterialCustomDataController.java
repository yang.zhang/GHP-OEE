package com.vendorID.customdata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.el.MethodExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.faces.convert.BooleanConverter;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import javax.faces.event.MethodExpressionValueChangeListener;
import javax.faces.event.ValueChangeEvent;

import com.sap.me.common.CustomValue;
import com.sap.me.common.ObjectAliasEnum;
import com.sap.me.customdata.CustomDataServiceInterface;
import com.sap.me.customdata.ReadCustomDataRequest;
import com.sap.me.customdata.SaveCustomDataRequest;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.frame.domain.DomainServiceFactory;
import com.sap.me.frame.domain.DomainServiceInterface;
import com.sap.me.frame.service.CommonMethods;
import com.sap.me.frame.utils.I18nUtility;
import com.sap.me.productdefinition.domain.ItemDO;
import com.sap.me.production.podclient.BasePodPlugin;
import com.sap.me.wpmf.TableConfigurator;
import com.sap.me.wpmf.util.FacesUtility;
import com.sap.ui.faces.component.sap.HtmlCommandBooleanCheckbox;
import com.sap.ui.faces.component.sap.UICommandInputText;

public class MaterialCustomDataController extends BasePodPlugin implements ActionListener {

	private String material;
	private String version;
	private Boolean materialBrowseRendered;
	private List<MaterialItem> materialList = new ArrayList<MaterialItem>();
	private String message;
	private List<MaterialCustomDataItem> materialCustomDataList;
	private TableConfigurator tableConfigBean = null;
	private boolean selected;

	CustomDataServiceInterface customDataService = Services.getService("com.sap.me.customdata", "CustomDataService");
	DomainServiceInterface<ItemDO> itemDOService = DomainServiceFactory.getServiceByClass(ItemDO.class);
	String[] listColumnNames = new String[] { "SELECT", "CUSTOM_DATA_FIELD", "CUSTOM_DATA_VALUE" };
	String[] columnDefs = new String[] { "select;MATERIAL_CUSTOM_DATA.select.LABEL", "dataField;MATERIAL_CUSTOM_DATA.customDataField.LABEL", "dataValue;MATERIAL_CUSTOM_DATA.customDataValue.LABEL" };

	public MaterialCustomDataController() {
		// Initialize Material Browse list
		initMaterialList();
	}

	public void clear() {
		// clear bean caches
		//Removes an Object from the session map 
		FacesUtility.removeSessionMapValue("materialCustomDataBean");
		FacesUtility.removeSessionMapValue("customDataBrowseBean_currentBrowseComponentId");
		FacesUtility.removeSessionMapValue("customDataBrowseBean");
		// Clear message bar area
		this.message = null;
		displayMessageBar(false);
		// findComponent() - Helper method to find a component given an input parent component and id of component to find
		// getViewRoot() - Return the root component that is associated with this request
		UIComponent tablePanel = findComponent(FacesUtility.getFacesContext().getViewRoot(), "materialCustomDataForm:displayPanel");
		if (tablePanel != null) {
			// Adds UIComponent to the list of controls that are re-rendered in the request
			FacesUtility.addControlUpdate(tablePanel);
		}
	}

	// Activity closed , window unload
	public void processWindowClosed() {
		//Removes an Object from the session map 
		FacesUtility.removeSessionMapValue("materialCustomDataBean");
		FacesUtility.removeSessionMapValue("customDataBrowseBean");
		FacesUtility.removeSessionMapValue("customDataBrowseBean_currentBrowseComponentId");
	}
	 // Returns Table Configurator object 
	public TableConfigurator getTableConfigBean() {
		return tableConfigBean;
	}

	public void setTableConfigBean(TableConfigurator config) {
		this.tableConfigBean = config;
		// Execute this code when first time opened activity
		if (tableConfigBean.getColumnBindings() == null || tableConfigBean.getColumnBindings().size() < 1) {
			//set the Name of the List specified in List Maintenance
			tableConfigBean.setListName(null);
			//Bind table column with property from MaterialItem Java Bean class
			tableConfigBean.setColumnBindings(getColumnFieldMaping());
			//Sets the column names for the table 
			tableConfigBean.setListColumnNames(listColumnNames);
			//Sets the flag indicating if row selection is allowed. 
			tableConfigBean.setAllowSelections(false);
			//Sets the flag indicating if  multi selection is allowed 
			tableConfigBean.setMultiSelectType(false);
			ArrayList<String> editCols = new ArrayList<String>();
			// make these columns editable
			editCols.add("CUSTOM_DATA_FIELD");
			editCols.add("CUSTOM_DATA_VALUE");
			editCols.add("SELECT");
			// Set the list of editable columns
			tableConfigBean.setEditableColumns(editCols);
            //Configure CUSTOM_DATA_FIELD
			UICommandInputText inputTextCell = new UICommandInputText();
			// don't do any actions on changes and tab out from cell
			inputTextCell.setSubmitOnChange(false);
			inputTextCell.setSubmitOnTabout(false);
			inputTextCell.getAttributes().put("upperCase", "true");
			// shows browse icon
			inputTextCell.setSubmitOnFieldHelp(true);
			// Add a ActionListener to this configurator , process the action when you click on browse icon
			inputTextCell.addActionListener(this);	
			tableConfigBean.setCellEditor("CUSTOM_DATA_FIELD", inputTextCell);
            //Configure CUSTOM_DATA_VALUE
			UICommandInputText inputTextCell1 = new UICommandInputText();
			inputTextCell1.setSubmitOnChange(false);
			tableConfigBean.setCellEditor("CUSTOM_DATA_VALUE", inputTextCell1);
            //Configure SELECT
			// Lets try to render a check box for the boolean column
			HtmlCommandBooleanCheckbox checkBox =  new HtmlCommandBooleanCheckbox();
			tableConfigBean.setCellEditor("SELECT", checkBox);
			// let's add an event handler to this.
			configureCellEditor("SELECT");
			// Set your converter for the "SELECT" column
			tableConfigBean.setColumnConverter(new BooleanConverter(), "SELECT");
			// Set the "SELECT" column width 
			tableConfigBean.setColumnWidth("SELECT", "45px");

			// Configure the table using the predefined values for column titles and bindings, etc
			tableConfigBean.configureTable();
		}
	}

	protected HashMap<String, String> getColumnFieldMaping() {
//		String[] listColumnNames = new String[] { "SELECT", "CUSTOM_DATA_FIELD", "CUSTOM_DATA_VALUE" };
//		String[] columnDefs = new String[] { "select;MATERIAL_CUSTOM_DATA.select.LABEL", "dataField;MATERIAL_CUSTOM_DATA.customDataField.LABEL", "dataValue;MATERIAL_CUSTOM_DATA.customDataValue.LABEL" };
		HashMap<String, String> columnFieldMap = new HashMap<String, String>();
		String[] columns = columnDefs;
		for (int i = 0; i < columns.length; i++) {
			columnFieldMap.put(listColumnNames[i], columns[i]);
		}
		return columnFieldMap;
	}

	// Action handler for "SELECT" check-box column changes
	private void configureCellEditor(String colName) {
		//Get the current Cell renderer for the given column
		UIComponent editCell = tableConfigBean.getCellEditor(colName);
		if (editCell instanceof javax.faces.component.EditableValueHolder) {
			Class[] clazz = new Class[1];
			clazz[0] = ValueChangeEvent.class;
			String editCellBinding = "#{materialCustomDataBean.processEditCellChange}";
			MethodExpression lstnr = FacesUtility.createMethodExpression(editCellBinding, null, clazz);
			MethodExpressionValueChangeListener changeListener = new MethodExpressionValueChangeListener(lstnr);
			((javax.faces.component.EditableValueHolder) editCell).addValueChangeListener(changeListener);
		}

	}
	/**
	 * The value change event handler for editable cells that modify cell values. This keeps track of modified rows.
	 * 
	 * @param event the value change event.
	 */
	public void processEditCellChange(ValueChangeEvent event) {
		String oldVal = (event.getOldValue() != null) ? event.getOldValue().toString() : "";
		String newVal = (event.getNewValue() != null) ? event.getNewValue().toString() : "";
		if (oldVal.equals(newVal))
			return;
		// If you checked - unchecked check box in table we should clean message bar area
		this.message = null;
		displayMessageBar(false);
	}

	public void insertNewRow() {
		materialCustomDataList = (List<MaterialCustomDataItem>) tableConfigBean.getTable().getValue();
		if (materialCustomDataList == null || materialCustomDataList.size() == 0)
			materialCustomDataList = new ArrayList<MaterialCustomDataItem>();
		materialCustomDataList.add(new MaterialCustomDataItem());
		// clean message bar
		this.message = null;
		displayMessageBar(false);
		UIComponent tablePanel = findComponent(FacesUtility.getFacesContext().getViewRoot(), "materialCustomDataForm:displayPanel");
		if (tablePanel != null) {
			// Adds UIComponent to the list of controls that are re-rendered in the request
			FacesUtility.addControlUpdate(tablePanel);
		}
	}

	public void deletSelectedRow() {
		this.message = null;
		displayMessageBar(false);
		materialCustomDataList = (List<MaterialCustomDataItem>) tableConfigBean.getTable().getValue();
		if (materialCustomDataList == null || materialCustomDataList.size() == 0)
			return;
		List<MaterialCustomDataItem> materialCustomDataNewList = new ArrayList<MaterialCustomDataItem>();
		for (MaterialCustomDataItem materialCustomDataItem : materialCustomDataList) {
			if (materialCustomDataItem.isSelect()) {
				continue;
			}
			materialCustomDataNewList.add(materialCustomDataItem);
		}
		materialCustomDataList = materialCustomDataNewList;
		UIComponent tablePanel = findComponent(FacesUtility.getFacesContext().getViewRoot(), "materialCustomDataForm:displayPanel");
		if (tablePanel != null) {
			// Adds UIComponent to the list of controls that are re-rendered in the request
			FacesUtility.addControlUpdate(tablePanel);
		}

	}

	public void saveRows() {
		try {
			materialCustomDataList = (List<MaterialCustomDataItem>) tableConfigBean.getTable().getValue();
			String site = CommonMethods.getSite();
			String itemRef = buildItem(site, material, version);
			SaveCustomDataRequest saveCustomDataRequest = new SaveCustomDataRequest();
			List<CustomValue> customData = new ArrayList<CustomValue>();
			for (MaterialCustomDataItem materialCustomDataItem : materialCustomDataList) {
				CustomValue customValue = new CustomValue();
				customValue.setName(materialCustomDataItem.getDataField());
				customValue.setValue(materialCustomDataItem.getDataValue());
				customData.add(customValue);
			}
			saveCustomDataRequest.setCustomData(customData);
			// If set to false (default), then the fields in the request will completely replace all custom fields stored 
			//in the database. All the fields that are not in the request, will be removed.
			saveCustomDataRequest.setDoFieldMerge(false);
			saveCustomDataRequest.setObjectAlias(ObjectAliasEnum.ITEM.value());
			saveCustomDataRequest.setRef(itemRef.toString());
			customDataService.saveCustomData(saveCustomDataRequest);
			// Returns the locale specific text for the input key
			message = FacesUtility.getLocaleSpecificText("recordSaved.MESSAGE");
			// Display success message on GUI
			displayMessageBar(true);
		} catch (BusinessException e) {
			message = I18nUtility.getMessageForException(e);
			// Display error message on GUI
			displayMessageBar(true);	
		}
        catch (Exception ex) {
			// Display error message on GUI
			message = "Runtime Exception: " + ex.getLocalizedMessage();
			displayMessageBar(true);
		}
		
		// Make sure you add the table to the list of control updates so that  the new model value will be shown on the UI.
		UIComponent tablePanel = findComponent(FacesUtility.getFacesContext().getViewRoot(), "materialCustomDataForm:displayPanel");
		if (tablePanel != null) {
			FacesUtility.addControlUpdate(tablePanel);
		}

	}

	public void displayMessageBar(boolean render) {
		HtmlOutputText messageBar = (HtmlOutputText) findComponent(FacesUtility.getFacesContext().getViewRoot(), "materialCustomDataForm:messageBar");
		messageBar.setRendered(render);
		// Blue bold message
		messageBar.setStyle("color:#0000FF;font-weight:bold;font-size:10pt;");
		UIComponent fieldButtonPanel = findComponent(FacesUtility.getFacesContext().getViewRoot(), "materialCustomDataForm:fieldButtonPanel");
		if (fieldButtonPanel != null) {
			FacesUtility.addControlUpdate(fieldButtonPanel);
		}
	}

	public void readMaterialCustomData() {
		this.message = null;
		materialCustomDataList = new ArrayList<MaterialCustomDataItem>();
		displayMessageBar(false);
		try {

			String site = CommonMethods.getSite();
			String itemRef = buildItem(site, material, version);
			ReadCustomDataRequest request = new ReadCustomDataRequest();
			request.setObjectAlias(ObjectAliasEnum.ITEM.value());
			request.setRef(itemRef.toString());
			Collection<CustomValue> customValuesList = customDataService.readCustomDataForObject(request);

			for (CustomValue customValue : customValuesList) {
				MaterialCustomDataItem materialCustomDataItem = new MaterialCustomDataItem();
				materialCustomDataItem.setDataField(customValue.getName());
				materialCustomDataItem.setDataValue((String) customValue.getValue());
				materialCustomDataItem.setSelect(false);
				materialCustomDataList.add(materialCustomDataItem);
			}
			if (materialCustomDataList.size() == 0) {
				// Display message on GUI
				message = FacesUtility.getLocaleSpecificText("noRecords.MESSAGE");
				displayMessageBar(true);

			}
		} catch (BusinessException e) {
			// Display error message on GUI
			message = I18nUtility.getMessageForException(e);
			displayMessageBar(true);
			

		} catch (Exception ex) {
			// Display error message on GUI
			message = "Runtime Exception: " + ex.getLocalizedMessage() ;
			displayMessageBar(true);
		}
		// Make sure you add the table to the list of control updates so that  the new model value will be shown on the UI.
		UIComponent tablePanel = findComponent(FacesUtility.getFacesContext().getViewRoot(), "materialCustomDataForm:displayPanel");
		if (tablePanel != null) {
			FacesUtility.addControlUpdate(tablePanel);
		}

	}

	// Action handle for browse clicking in cell
	public void processAction(ActionEvent event) throws AbortProcessingException {
		// The object on which the Event initially occurred
		UICommandInputText currentBrowseComponent = (UICommandInputText) event.getSource();
		// getClientId() returns a client-side identifier for this component
		// example materialCustomDataForm:materialCustomDataTable:0:j_id1
		String currentBrowseComponentId = currentBrowseComponent.getClientId(FacesContext.getCurrentInstance());
		FacesUtility.removeSessionMapValue("customDataBrowseBean");
		FacesUtility.setSessionMapValue("customDataBrowseBean_currentBrowseComponentId", currentBrowseComponentId);
		// Java Script function in MaterialCustomDataView.jsp
		FacesUtility.addScriptCommand("window.customDataFieldBrowse();");		
	}
	// Read all materials for current site
	public void initMaterialList() {
		this.materialList = new ArrayList<MaterialItem>();
		String site = CommonMethods.getSite();
		ItemDO itemDO = new ItemDO();
		// set current site to domain object for material
		itemDO.setSite(site);
		// Get all materials on the current site
		Collection<ItemDO> materialDOList = itemDOService.readByExample(itemDO);
		for (ItemDO materialDO : materialDOList) {
			MaterialItem materailItem = new MaterialItem();
			materailItem.setMaterial(materialDO.getItem());
			materailItem.setVersion(materialDO.getRevision());
			materailItem.setSelected(new Boolean(false));
			materialList.add(materailItem);
		}
	}
	
	private String buildItem(String site, String item, String revision) {
		StringBuffer sb = new StringBuffer();
		sb.append("ItemBO").append(':').append(site).append(',');
		sb.append(item).append(',').append(revision);
		return sb.toString();
	}

	public boolean isSelected() {

		MaterialItem currentRow  = getCurrentSelectedRowMaterailBrowseTable();
		if (currentRow != null) {
			return currentRow.getSelected().booleanValue();
		}
		return false;
	}

	/**
	 * @nooverride
	 * @param selected set the selection of the current row
	 */
	public void setSelected(boolean selected) {
		MaterialItem currentRow = getCurrentSelectedRowMaterailBrowseTable();
		currentRow.setSelected(new Boolean(selected));
	}

	private MaterialItem getCurrentSelectedRowMaterailBrowseTable() {

		Map<String, Object> requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
		MaterialItem currentRow = (MaterialItem) requestMap.get("materialBrowseVar");
		return currentRow;
	}

	// Row in browse selected , double click  
	public void rowSelected(ActionEvent event) {
        // Close browse
		closeMaterialBrowse(event);
		// set value for material field and version field
		MaterialItem selectedMaterial = getSelectedMaterial();
		if (selectedMaterial != null) {
			this.material = selectedMaterial.getMaterial();
			this.version = selectedMaterial.getVersion();
		}
	}

	private MaterialItem getSelectedMaterial() {
		MaterialItem selectedMaterialItem = null;
		if (materialList != null) {
			for (int i = 0; i < this.materialList.size(); i++) {
				MaterialItem materialItem = materialList.get(i);
				if (materialItem != null && materialItem.getSelected()) {
					selectedMaterialItem = materialItem;
					break;
				}
			}
		}
		return selectedMaterialItem;
	}
	
	public void setMaterialCustomDataList(List<MaterialCustomDataItem> materialCustomDataList) {
		this.materialCustomDataList = materialCustomDataList;
	}
	public List<MaterialCustomDataItem> getMaterialCustomDataList() {
		return materialCustomDataList;
	}

	public String getMaterial() {

		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getVersion() {

		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public Boolean getMaterialBrowseRendered() {

		return materialBrowseRendered;
	}

	public void setMaterialBrowseRendered(Boolean materialBrowseRendered) {
		this.materialBrowseRendered = materialBrowseRendered;
	}
	
	public String getMessage() {

		return message;
	}

	public void setMessage(String message) {

		this.message = message;
	}

	
	public List<MaterialItem> getMaterialList() {
		return this.materialList;
	}

    // Show browse for material
	public void showMaterialBrowse(ActionEvent event) {

		this.materialBrowseRendered = true;
	}
    // Hide browse for material
	public void closeMaterialBrowse(ActionEvent event) {

		this.materialBrowseRendered = false;
	}
	
	
	
}
