package com.vendorID.customdata;

public class MaterialCustomDataItem {

	private String dataField;
	private String dataValue;
	private boolean select;

	public void setDataField(String dataField) {
		this.dataField = dataField;
	}
	public String getDataField() {
		return dataField;
	}
	public void setDataValue(String dataValue) {
		this.dataValue = dataValue;
	}

	public String getDataValue() {
		return dataValue;
	}
	public void setSelect(boolean select) {
		this.select = select;
	}

	public boolean isSelect() {
		return select;
	}
}

