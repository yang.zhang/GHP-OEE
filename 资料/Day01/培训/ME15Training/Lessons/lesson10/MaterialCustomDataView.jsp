<%@page pageEncoding="UTF-8"%>
<%@ page language="java"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@ taglib prefix="fh" uri="http://java.sun.com/jsf/html"%>
<%@ taglib prefix="h" uri="http://java.sap.com/jsf/html"%>
<%@ taglib prefix="sap" uri="http://java.sap.com/jsf/html/extended"%>
<f:view>
	<sap:html>
	<sap:head>
	<script>
	UCF_DomUtil.attachEvent(window, 'beforeunload', windowUnload);
     function windowUnload() {
         if (window.parent == undefined) {
          //   window.alert('windowLoaded: window.parent is undefined');
             return;
         }
         var btnControl = document.getElementById('materialCustomDataForm:windowCloseButton');
         if (btnControl) {
             btnControl.click();
         }   
     }
    
    
      function customDataFieldBrowse() {
         var url = '/trainingwar/com/vendorID/customdata/CustomDataFieldBrowse.jsf';
         var myWindow = window.open(url, 'Browse', 'menubar=no,height=400,width=600,scrollbars=yes,status=no,location=no,resizable=yes,dependent=yes');  
         setTimeout('myWindow.focus()', 100);
    }
	</script>
	</sap:head>
	<sap:body id="materialCustomBody" focusId="materialCustomDataForm"
		browserHistory="disabled"  height="100%">
		<h:form id="materialCustomDataForm">
			<sap:panel id="fieldButtonPanel" width="100%" height="100%"
				isCollapsible="false" contentAreaDesign="transparent" isCollapsed="false">
				<!-- Header area title  -->
				<f:facet name="header">
					<h:outputText
						value="#{gapiI18nTransformer['MATERIAL_CUSTOM_DATA.page_title']}" />
				</f:facet>
				<!-- The ajaxUpdate component enables server side ajax updates of specified UI components -->
                <sap:ajaxUpdate render="#{sap:toClientId('fieldButtonPanel')}"/>
				<sap:panelGrid width="100%" height="100%" cellHalign="start"
					cellValign="top" columns="1">
					<sap:panelGroup backgroundDesign="transparent" width="100%"
						halign="start" valign="middle" height="2em" padding="5 5 5 5" >
						<!-- Message area -->
						<fh:outputText id="messageBar" value="#{materialCustomDataBean.message}" rendered="false"/>
					</sap:panelGroup>
					<sap:panelGroup backgroundDesign="transparent" width="100%"
						halign="start" valign="top" height="2em" padding="5 5 5 5">
						<sap:panelGrid cellHalign="start"
							cellValign="top" width="100%" columns="1">
							<sap:panelGroup>
						        <!-- * Material : label  -->															
								<fh:outputLabel value="*"
									style="font-weight: bold; font-size: 15pt;color:#FF0000"></fh:outputLabel>
								<fh:outputLabel
									value="#{gapiI18nTransformer['MATERIAL_CUSTOM_DATA.material.LABEL']}"
									style="font-weight: bold; font-size: 10pt;"></fh:outputLabel>
								<fh:outputLabel value=":"
									style="font-weight: bold; font-size: 10pt;"></fh:outputLabel>
						        <!-- * Material input field with browse - submitOnFieldHelp=true -->																								
								<sap:commandInputText id="materialInput"
									value="#{materialCustomDataBean.material}"
									submitOnFieldHelp="true"
									actionListener="#{materialCustomDataBean.showMaterialBrowse}"
									submitOnTabout="false" submitOnChange="false">
									<f:attribute name="upperCase" value="true" />
								</sap:commandInputText>
								<!-- Material browse -->
								<sap:panelPopup id="materialBrowsePopup"
									rendered="#{materialCustomDataBean.materialBrowseRendered}"
									mode="modeless" height="400px" width="400px">
									<f:facet name="header">
										<h:outputText value="Browse for Material" />
									</f:facet>
									<f:facet name="popupButtonArea">
										<h:panelGroup>
											<h:commandButton value="OK"
												actionListener="#{materialCustomDataBean.rowSelected}"></h:commandButton>
											<h:commandButton value="Cancel"
												actionListener="#{materialCustomDataBean.closeMaterialBrowse}"></h:commandButton>
										</h:panelGroup>
									</f:facet>
									<sap:panelGrid width="100%" height="100%">
                                                                            <!-- 'value' attribute it is the current value of this component, i.e. the table data model instance   -->
										<sap:dataTable value="#{materialCustomDataBean.materialList}"
											var="materialBrowseVar" id="materialBrowseTable" width="100%"
											height="100%" columnReorderingEnabled="true">
											<sap:rowSelector value="#{materialCustomDataBean.selected}"
												id="rowSelector" selectionMode="single"
												selectionBehaviour="client"
												actionListener="#{materialCustomDataBean.rowSelected}"
												submitOnRowDoubleClick="true" />
											<sap:column id="materialColumn" position="0">
												<f:facet name="header">
													<h:outputText id="materialHeaderText"
														value="#{gapiI18nTransformer['MATERIAL_CUSTOM_DATA.material.LABEL']}" />
												</f:facet>
												<h:outputText value="#{materialBrowseVar.material}"
													id="materialText" />
											</sap:column>
											<sap:column id="versionColumn" position="1">
												<f:facet name="header">
													<h:outputText id="versionHeaderText"
														value="#{gapiI18nTransformer['MATERIAL_CUSTOM_DATA.version.LABEL']}" />
												</f:facet>
												<h:outputText value="#{materialBrowseVar.version}"
													id="versionText" />
											</sap:column>
										</sap:dataTable>
									</sap:panelGrid>
								</sap:panelPopup>
						        <!-- * Version : label  -->								
							    <sap:outputLabel value="   " />
								<fh:outputLabel value="*"
									style="font-weight: bold; font-size: 15pt;color:#FF0000"></fh:outputLabel>
								<fh:outputLabel
									value="#{gapiI18nTransformer['MATERIAL_CUSTOM_DATA.version.LABEL']}"
									style="font-weight: bold; font-size: 10pt;"></fh:outputLabel>
								<fh:outputLabel  value=":"
									style="font-weight: bold; font-size: 10pt;"></fh:outputLabel>
						        <!-- * Version input field  -->																	
								<sap:commandInputText id="VERSION" submitOnChange="false"
									submitOnFieldHelp="false"
									value="#{materialCustomDataBean.version}">
									<f:attribute name="upperCase" value="true" />
								</sap:commandInputText>
								
							</sap:panelGroup>					
						</sap:panelGrid>
					</sap:panelGroup>

					<sap:panelGroup backgroundDesign="transparent" width="100%"
						halign="center" valign="top" height="3em" padding="5 5 5 5">
						<!-- 'Retrieve' button  -->						
						<sap:commandButtonLarge id="RETRIEVE" value="Retrieve"
							width="85px" height="20px"
							action="#{materialCustomDataBean.readMaterialCustomData}">
							<sap:ajaxUpdate />
						</sap:commandButtonLarge>
						<sap:outputLabel value="   " />
						<!-- 'Clear' button  -->
						<sap:commandButtonLarge id="CLEAR" value="Clear" width="90px"
							imagePosition="start" height="20px"
							action="#{materialCustomDataBean.clear}"
							image="/com/vendorID/icons/clear.png">
							<sap:ajaxUpdate />
						</sap:commandButtonLarge>
						<!-- Hidden close window button  -->						
						<sap:commandButtonLarge id="windowCloseButton" value=""
							width="0px" height="0px"
							action="#{materialCustomDataBean.processWindowClosed}">
							<sap:ajaxUpdate />
						</sap:commandButtonLarge>
					</sap:panelGroup>

					<sap:panel id="displayPanel" width="100%" height="100%"
						isCollapsible="false" contentAreaDesign="transparent"
						isCollapsed="false">

						<f:facet name="header">
							<!-- Panel title -->
							<h:outputText value="Material Custom Data Result" />
						</f:facet>
						<!-- The ajaxUpdate component enables server side ajax updates of specified UI components -->
						<sap:ajaxUpdate render="#{sap:toClientId('displayPanel')}"/>
						
						<sap:panelGrid width="100%" height="100%" cellHalign="start"
							cellValign="top" columns="1">
							<sap:panelGroup backgroundDesign="transparent" width="100%"
								halign="left" valign="top" height="1em" padding="5 5 5 5">
								<!-- 'Insert New' button  -->
								<sap:commandButton id="InsertNew" value="Insert New"
									width="70px" action="#{materialCustomDataBean.insertNewRow}">
									<sap:ajaxUpdate />
								</sap:commandButton>
								<sap:outputLabel value="   " />
								<!-- 'Delete Row' button  -->
								<sap:commandButton id="DeleteSelected" value="Delete Row"
									width="80px"
									action="#{materialCustomDataBean.deletSelectedRow}">
									<sap:ajaxUpdate />
								</sap:commandButton>
								<sap:outputLabel value="   " />
								<!-- 'Save' button  -->								
								<sap:commandButton id="Save" value="Save" width="60px"
									action="#{materialCustomDataBean.saveRows}">
									<sap:ajaxUpdate />
								</sap:commandButton>
						</sap:panelGroup>
							<sap:panelGroup backgroundDesign="transparent" width="100%"
								halign="middle" valign="top">
						<!--Data Table-->
	                    <!-- 'value' attribute it is the current value of this component, i.e. the table data model instance   -->
					    <!-- 'binding' attribute it is the ValueExpression linking this component to a property in a backing bean   -->		
								<sap:dataTable
									binding="#{materialCustomDataBeanConfigurator.table}"
									value="#{materialCustomDataBean.materialCustomDataList}"
									first="0" var="rows" id="materialCustomDataTable" width="100%"
									height="100%" columnReorderingEnabled="true" rendered="true" />
								
							</sap:panelGroup>
						</sap:panelGrid>
					</sap:panel>

				</sap:panelGrid>
			</sap:panel>
		</h:form>
	</sap:body>
	</sap:html>
</f:view>
