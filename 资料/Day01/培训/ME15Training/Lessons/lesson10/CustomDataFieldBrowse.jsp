<%@page pageEncoding="UTF-8"%>
<%@ page language="java"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@ taglib prefix="fh" uri="http://java.sun.com/jsf/html"%>
<%@ taglib prefix="h" uri="http://java.sap.com/jsf/html"%>
<%@ taglib prefix="sap" uri="http://java.sap.com/jsf/html/extended"%>
<f:view>

	<sap:html>
	<sap:head>
		<script>
	window.focus();
    function setCustomDataField (customField , tableComponentID){
      var customFieldTableCompID = window.opener.document.getElementById(tableComponentID);
      customFieldTableCompID.value = customField;
      customFieldTableCompID.focus();
      window.close();
    }
	</script>
	</sap:head>

	<sap:body id="customFieldBrowse" focusId="customFieldBrowseForm"
		browserHistory="disabled" height="100%">
		<h:form id="customFieldBrowseForm">
			<sap:panel id="displayPanel" width="100%" height="100%"
				isCollapsible="false" contentAreaDesign="transparent"
				isCollapsed="false">
				<!-- Header area title  -->				
				<f:facet name="header">
					<h:outputText value="Browse for Data Field" />
				</f:facet>
			<sap:ajaxUpdate render="#{sap:toClientId('displayPanel')}"/>	
			<sap:panelGrid width="100%" height="100%" cellHalign="start"
					cellValign="top" columns="1">
					<sap:panelGroup backgroundDesign="transparent" width="100%"
						halign="middle" valign="top">
						<!--Data Table-->
	                    <!-- 'value' attribute it is the current value of this component, i.e. the table data model instance   -->
					    <!-- 'binding' attribute it is the ValueExpression linking this component to a property in a backing bean   -->
						<sap:dataTable binding="#{customFieldsDataBeanConfigurator.table}"
							value="#{customDataBrowseBean.dataFieldsList}" first="0"
							var="rows" id="materialTable" width="100%" height="100%"
							columnReorderingEnabled="true" rendered="true">
						</sap:dataTable>
					</sap:panelGroup>
					<sap:panelGroup backgroundDesign="transparent" width="100%"
						halign="center" valign="top" height="1em" padding="5 5 5 5">
						<!-- OK button  -->
						<sap:commandButtonLarge id="submit" value="OK" width="80px"
							height="20px" action="#{customDataBrowseBean.okAction}">
							<sap:ajaxUpdate />
						</sap:commandButtonLarge>					
						<sap:outputLabel value="   " />
						<!-- Cancel button  -->
						<sap:commandButtonLarge id="cancel" value="Cancel" width="80px"
							height="20px" action="#{customDataBrowseBean.cancelAction}">
							<sap:ajaxUpdate />
						</sap:commandButtonLarge>
					</sap:panelGroup>
		    	</sap:panelGrid>
			</sap:panel>
		</h:form>
	</sap:body>
	</sap:html>
</f:view>