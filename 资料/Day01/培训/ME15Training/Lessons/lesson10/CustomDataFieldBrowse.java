package com.vendorID.customdata;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.faces.event.ActionEvent;

import com.sap.me.common.ObjectAliasEnum;
import com.sap.me.customdata.CustomDataConfigurationServiceInterface;
import com.sap.me.customdata.CustomFieldBasicConfiguration;
import com.sap.me.customdata.FindCustomFieldDefinitionRequest;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.wpmf.TableConfigurator;
import com.sap.me.wpmf.util.FacesUtility;
import com.sap.ui.faces.component.sap.UIRowSelector;
import com.sap.ui.faces.event.sap.TypedActionEvent;

public class CustomDataFieldBrowse {

	private List<CustomDataFieldItem> dataFieldsList;
	private TableConfigurator configBean = null;
	String[] columnDefs = new String[] { "dataField;CUSTOM_DATA_FIELDS_BROWSE.dataField.LABEL", "description;CUSTOM_DATA_FIELDS_BROWSE.description.LABEL" };
	String[] listColumnNames = new String[] { "DATA_FIELD", "DESCRIPTION" };
	CustomDataConfigurationServiceInterface customDataConfigurationService = Services.getService("com.sap.me.customdata", "CustomDataConfigurationService");
	public CustomDataFieldBrowse() {}
	
	
	public TableConfigurator getConfigBean() {
		return configBean;
	}
	public void setConfigBean(TableConfigurator configBean) {
		this.configBean = configBean;
		if (configBean.getColumnBindings() == null || configBean.getColumnBindings().size() < 1) {
			configBean.setListName(null);
			configBean.setColumnBindings(getColumnFieldMapping());
			configBean.setListColumnNames(listColumnNames);
			configBean.setAllowSelections(true);
			configBean.setMultiSelectType(false);
			// enable row double click
			configBean.setDoubleClick(true);
			configBean.setSelectionActionExpression("#{customDataBrowseBean.processSelectAction}");
			configBean.configureTable();
		}
	}
	protected HashMap<String, String> getColumnFieldMapping() {
		HashMap<String, String> columnFieldMap = new HashMap<String, String>();
		String[] columns = columnDefs;
		for (int i = 0; i < columns.length; i++) {
			columnFieldMap.put(listColumnNames[i], columns[i]);
		}
		return columnFieldMap;
	}
	
	public void setDataFieldsList(List<CustomDataFieldItem> dataFieldsList) {
		this.dataFieldsList = dataFieldsList;
	}
	
	public List<CustomDataFieldItem> getDataFieldsList() throws BusinessException {
		dataFieldsList = new ArrayList<CustomDataFieldItem>();
		
		FindCustomFieldDefinitionRequest findCustomFieldDefinitionRequest = new FindCustomFieldDefinitionRequest();
		findCustomFieldDefinitionRequest.setObjectAlias(ObjectAliasEnum.ITEM.value());
		Collection<CustomFieldBasicConfiguration>  customFieldsBasicConfigList  = customDataConfigurationService.findSiteSpecificCustomFieldConfigurationsByCategory(findCustomFieldDefinitionRequest );
		for (CustomFieldBasicConfiguration customFieldBasicConfiguration : customFieldsBasicConfigList) {
			CustomDataFieldItem customDataFieldItem = new CustomDataFieldItem();
			customDataFieldItem.setDataField(customFieldBasicConfiguration.getFieldName());
			customDataFieldItem.setDescription(customFieldBasicConfiguration.getFieldLabel());

			dataFieldsList.add(customDataFieldItem);
		}

		return dataFieldsList;
	}

	  // Double click on row in Material Browse
	public void processSelectAction(ActionEvent event) {
		TypedActionEvent tevent = null;
		if (event instanceof TypedActionEvent) {
			tevent = (TypedActionEvent) event;
			Enum eventType = tevent.getEventType();
			if (eventType instanceof UIRowSelector.EventType) {
				UIRowSelector.EventType rowSelectorEventType = (UIRowSelector.EventType) eventType;
				if (rowSelectorEventType == UIRowSelector.EventType.ROW_DOUBLE_CLICK) {
					List<CustomDataFieldItem> selectedOnes = (List<CustomDataFieldItem>) configBean.getSelectedItems();
					if (selectedOnes != null && selectedOnes.size() > 0){
					CustomDataFieldItem customDataFieldItem = selectedOnes.get(0);
					// client-side identifier for cell in table where you pressed the browse icon 
					String tableComponentID = (String) FacesUtility.getSessionMapValue("customDataBrowseBean_currentBrowseComponentId");
					// Java Script function in CustomDataFieldBrowse.jsp
					StringBuffer buf = new StringBuffer("window.setCustomDataField('");
					buf.append(customDataFieldItem.getDataField());
					buf.append("','");
					buf.append(tableComponentID);
					buf.append("');");
					FacesUtility.addScriptCommand(buf.toString());
				 } 
				}
			}
		}
        configBean.clearSelectedRows();
	}
	
	
 
    // Click OK button
	public void okAction() {
	if (configBean.getSelectedItems() != null && configBean.getSelectedItems().size() > 0) {
		CustomDataFieldItem customDataFieldItem = (CustomDataFieldItem) configBean.getSelectedItems().get(0);
		if (customDataFieldItem == null)
			// Close the current window
			FacesUtility.addScriptCommand("window.close();");
		// Cell ID value in custom fields table where you pressed browse icon
		String tableComponentID = (String) FacesUtility.getSessionMapValue("customDataBrowseBean_currentBrowseComponentId");
		// Java Script function in CustomDataFieldBrowse.jsp
		StringBuffer buf = new StringBuffer("window.setCustomDataField('");
		buf.append(customDataFieldItem.getDataField());
		buf.append("','");
		buf.append(tableComponentID);
		buf.append("');");
		FacesUtility.addScriptCommand(buf.toString());
        configBean.clearSelectedRows();
	 }
	}

	public void cancelAction() {
		// Close the current window
		FacesUtility.addScriptCommand("window.close();");
        configBean.clearSelectedRows();
	}
	
}

