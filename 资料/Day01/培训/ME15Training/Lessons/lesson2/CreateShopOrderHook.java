package com.vendorID.hook;

import java.math.BigDecimal;

import com.sap.me.demand.CreateShopOrderRequest;
import com.sap.me.demand.ShopOrderFullConfiguration;
import com.sap.me.demand.ShopOrderServiceInterface;
import com.sap.me.demand.ShopOrderStatus;
import com.sap.me.demand.ShopOrderType;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.production.CompleteHookDTO;
import com.sap.me.production.StartHookDTO;
import com.visiprise.frame.service.ext.ActivityInterface;

/**
 * @author Oksana Zubchenko
 *
 */
public class CreateShopOrderHook implements ActivityInterface<Object> {

	private static final long serialVersionUID = 1L;
	ShopOrderServiceInterface shopOrderService;

	public void execute(Object dto) throws Exception {
		if (dto instanceof StartHookDTO) {
			execute((StartHookDTO) dto);
		} else if (dto instanceof CompleteHookDTO) {
			execute((CompleteHookDTO) dto);
		}
	}

	// execute() method for POST_START
	public void execute(StartHookDTO dto) throws Exception {

		CreateShopOrderRequest shopOrderRequest = new CreateShopOrderRequest();
		shopOrderRequest.setShopOrder("SO_AFTER_START" + dto.getDateTime().getTimeInMillis());
		// Build Planned Item Reference (HANDLE)
		String itemRef = buildItem(dto.getSite(), "TRAINING_MATERIAL", "A");
		// Set planned material to shop order
		shopOrderRequest.setPlannedItemRef(itemRef);
		shopOrderRequest.setStatus(ShopOrderStatus.Releasable);
		shopOrderRequest.setShopOrderType(ShopOrderType.PRODUCTION);
		shopOrderRequest.setPriority(new BigDecimal(500));
		// QTY Build = 5
		shopOrderRequest.setQuantityToBuild(new BigDecimal(5));
		// Call createShopOrder() method to create shop order in ME
		ShopOrderFullConfiguration soFullConfig = shopOrderService.createShopOrder(shopOrderRequest);
		/*
		// Un-comment for Lesson#5
		// Call custom service
		   callCustomReleaseService(soFullConfig.getShopOrderRef(),dto.getSite());
		 
		*/
	}

	// execute() method for PRE_COMPLETE
	public void execute(CompleteHookDTO dto) throws Exception {
		CreateShopOrderRequest shopOrderRequest = new CreateShopOrderRequest();
		shopOrderRequest.setShopOrder("SO_AFTER_COMPL" + dto.getDateTime().getTimeInMillis());
		// Build Planned Item Reference (HANDLE)
		String itemRef = buildItem(dto.getSite(), "TRAINING_MATERIAL", "A");
		// Set planned material to shop order
		shopOrderRequest.setPlannedItemRef(itemRef);
		shopOrderRequest.setStatus(ShopOrderStatus.Releasable);
		shopOrderRequest.setShopOrderType(ShopOrderType.REPETITIVE);
		shopOrderRequest.setPriority(new BigDecimal(500));
		// QTY Build = 10
		shopOrderRequest.setQuantityToBuild(new BigDecimal(10));
		// Call createShopOrder() method to create shop order in ME
		shopOrderService.createShopOrder(shopOrderRequest);
	}

	/**
	 *	Setter method for ShopOrderServiceInterface service
	 */
	public void setShopOrderService(ShopOrderServiceInterface shopOrderService) {
		this.shopOrderService = shopOrderService;
	}

	/**
	 *	Build item handle.
	 */
	private String buildItem(String site, String item, String revision) {
		StringBuffer sb = new StringBuffer();
		sb.append("ItemBO").append(':').append(site).append(',');
		sb.append(item).append(',').append(revision);
		return sb.toString();
	}
	/*
	// Un-comment for Lesson#5	
	private void callCustomReleaseService(String shopOrderRef , String site) throws BusinessException{
		  CustomReleaseSOInterface customReleaseSFC = Services.getService("com.vendorID.service","CustomReleaseSOService");
		  CustomShopOrderRequest request = new CustomShopOrderRequest();
		  request.setShopOrderRef(shopOrderRef);
		  request.setSite(site);
		  customReleaseSFC.release(request);
		  
	 }
	 */
	
	
}
