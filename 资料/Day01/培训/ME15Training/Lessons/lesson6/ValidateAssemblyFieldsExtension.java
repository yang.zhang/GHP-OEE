package com.vendorID.serviceext;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.sap.me.activity.ActivityOptionField;
import com.sap.me.activity.ActivityOptionValidationException;
import com.sap.me.activity.ActivityRuntimeType;
import com.sap.me.activity.ExecutionType;
import com.sap.me.activity.ServiceExtension;
import com.sap.me.production.AssemblyDataField;
import com.sap.me.production.AssemblyDataValidationRequest;
import com.vendorID.exception.SerialNumberInvalidException;
import com.visiprise.frame.service.ext.InvocationContext;
import com.visiprise.frame.service.ext.InvocationContextSetter;
import com.visiprise.frame.service.ext.TransactionContextInterface;
import com.visiprise.frame.service.ext.TransactionContextSetter;
import com.visiprise.globalization.util.DateTimeInterface;

//public ValidateDataFieldsResponse validateAssemblyFields(AssemblyDataValidationRequest request)
//throws ComponentFromVendorOnHoldException, InvalidComponentVendorException,
//InvalidAssemblyDataException, BusinessException;

@ExecutionType(ActivityRuntimeType.SYNC)
public class ValidateAssemblyFieldsExtension extends ServiceExtension<Object> implements TransactionContextSetter, InvocationContextSetter {
	// Extension option property
	private String validationMask = new String("00");
	// Extension option boolean property for check-box
	private Boolean validate = new Boolean(true);
	// Extension option date property for calendar
	private DateTimeInterface date;
	// Extension option select property
	private ExtensionPointNodeType extensionPointNodeType = ExtensionPointNodeType.ROOT;
	public enum ExtensionPointNodeType {
	    ROOT,
	    MODULE,
	    SERVICE,
	    METHOD,
	    INVOKE_CATEGORY,
	    EXTENSION;
	}
	
	// Interface providing access to the transaction context
	private TransactionContextInterface transactionContext;
	private InvocationContext invocationContext;
	@Override
	public void execute(Object request) throws Exception {
		if (request instanceof AssemblyDataValidationRequest) {
			execute((AssemblyDataValidationRequest) request);
		}
	}

	public void execute(AssemblyDataValidationRequest request) throws Exception {
		List<AssemblyDataField> assemblyDataFields = request.getAssemblyDataFields();
		// Iterate through all data fields and find "EXTERNAL_SERIAL"
		for (AssemblyDataField assemblyDataField : assemblyDataFields) {
			if ("EXTERNAL_SERIAL".equals(assemblyDataField.getAttribute())) {
				if (!isValid(assemblyDataField.getValue())) {
					//Un-comment for Lesson #17
					/*
					Context ctx = new InitialContext();
					CustomActivityLogLocal activityLogEjb = (CustomActivityLogLocal) ctx.lookup("ejb:/appName=vendorID.com/apps~ear, jarName=vendorID.com~apps~ejb.jar, beanName=CustomActivityLogBean, interfaceName=com.vendorID.ejb.CustomActivityLogLocal");
					AssemblyDataValidationRequest assemblyData = new AssemblyDataValidationRequest();
					assemblyData.setSfcRef(request.getSfcRef());
					assemblyData.setItemRef(request.getItemRef());
					activityLogEjb.logError(assemblyData);
					*/
					// Throw an exception if the field is empty
					throw new SerialNumberInvalidException(assemblyDataField.getValue(), this.validationMask);
				}
			}
		}
	}

	private boolean isValid(String value) {
		if (value.startsWith(this.validationMask))
			return false;
		else
			return true;
	}

	// Setter method for the VALIDATION_MASK extension option.
	@ActivityOptionField (required=true)
	public void setValidationMask(String validationMask) {
		this.validationMask = validationMask;
	}

	// Method to return a default value for the VALIDATION_MASK when a new
	// service extension configuration is created.
	public String getValidationMask() {
		return this.validationMask;
	}

	// Method to validate the VALIDATION_MASK value when the service
	// extension configuration is saved.
	public void validateValidationMask(String validationMask) throws ActivityOptionValidationException {
		if (validationMask == null || validationMask.length() == 0) {
		   throw new ActivityOptionValidationException(null,"VALIDATION_MASK");
		}
	}

	@ActivityOptionField
	public void setValidate(Boolean validate) {
		this.validate = validate;
	}

	@ActivityOptionField
	public void setDate(DateTimeInterface date) {
		this.date = date;
	}
	
	@ActivityOptionField
	public void setExtensionPointNodeType(ExtensionPointNodeType extensionPointNodeType) {
		this.extensionPointNodeType = extensionPointNodeType;
	}
	public ExtensionPointNodeType getExtensionPointNodeType() {
		return this.extensionPointNodeType ;
	}

	@Override
	// Setter to accept the TranactionContextSetter interface
	public void setTransactionContext(TransactionContextInterface transactionContext) {
		this.transactionContext = transactionContext;
	}

	@Override
	public void setInvocationContext(InvocationContext context) {
		this.invocationContext = context;
	}

}