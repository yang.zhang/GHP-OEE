package com.vendorID.exception;

import com.sap.me.frame.domain.BusinessException;

public class SerialNumberInvalidException extends BusinessException {

	private String serialNumber;
	private String badChar;
	
	public SerialNumberInvalidException(String serialNumber, String badChar) {
		super(20103);
		this.serialNumber = serialNumber;
		this.badChar = badChar;
	}	
	
	public String getSerialNumber(){
		return serialNumber;
	}
	
	public String getBadChar(){
		return badChar;
	}		
}