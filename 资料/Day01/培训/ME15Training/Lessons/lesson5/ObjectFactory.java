package com.vendorID.service;

import javax.xml.bind.annotation.XmlRegistry;

import com.vendorID.service.dto.CustomShopOrderRequest;
import com.vendorID.service.dto.CustomShopOrderResponse;

@XmlRegistry
public class ObjectFactory {
	public ObjectFactory() {
	}

	public CustomShopOrderRequest createCustomShopOrderRequest() {
		return new CustomShopOrderRequest();
	}
	public CustomShopOrderResponse createCustomShopOrderResponse() {
		return new CustomShopOrderResponse();
	}
}
