package com.vendorID.service.dto;


import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomShopOrderRequest", propOrder = {
    "shopOrderRef" ,
    "site" 
})
public class CustomShopOrderRequest implements Serializable  {
	
	    public CustomShopOrderRequest() {
	        super();
	    }
	    
	    @XmlElement
	    protected String shopOrderRef;
	    @XmlElement
	    protected String site;
	   
	    
	    public String getShopOrderRef() {
	        return shopOrderRef;
	    }
	    public void setShopOrderRef(String shopOrderRef) {
	        this.shopOrderRef = shopOrderRef;
	    }
	    public String getSite() {
	        return site;
	    }
	    public void setSite(String site) {
	        this.site = site;
	    }
	   
}