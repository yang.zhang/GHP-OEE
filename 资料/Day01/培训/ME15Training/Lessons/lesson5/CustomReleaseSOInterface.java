package com.vendorID.services;

import com.sap.me.frame.domain.BusinessException;
import com.vendorID.service.dto.CustomShopOrderRequest;
import com.vendorID.service.dto.CustomShopOrderResponse;



public interface CustomReleaseSOInterface {
	
	public CustomShopOrderResponse release (CustomShopOrderRequest request) throws BusinessException ;

}