package com.vendorID.services.impl;

import java.math.BigDecimal;

import com.sap.me.demand.ReleaseShopOrderRequest;
import com.sap.me.demand.ShopOrderServiceInterface;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.messaging.CreateFreeFormMessageRequest;
import com.sap.me.messaging.FindMessageTypeRequest;
import com.sap.me.messaging.MessageServiceInterface;
import com.sap.me.messaging.MessageTypeBasicConfiguration;
import com.sap.me.messaging.MessageTypeConfigurationServiceInterface;
import com.vendorID.service.dto.CustomShopOrderRequest;
import com.vendorID.service.dto.CustomShopOrderResponse;
import com.vendorID.services.CustomReleaseSOInterface;



public class CustomReleaseSOService implements CustomReleaseSOInterface {
    ShopOrderServiceInterface shopOrderService;
    private MessageServiceInterface messageService;
	private MessageTypeConfigurationServiceInterface messageTypeService;

    private void init() {
		messageService = Services.getService("com.sap.me.messaging","MessageService");
		messageTypeService = Services.getService("com.sap.me.messaging","MessageTypeConfigurationService");
	}
	@Override
	public CustomShopOrderResponse release (CustomShopOrderRequest request) throws BusinessException {
		init();
		ReleaseShopOrderRequest releaseShopOrderReq = new ReleaseShopOrderRequest();
		releaseShopOrderReq.setShopOrderRef(request.getShopOrderRef());
		releaseShopOrderReq.setQuantityToRelease(BigDecimal.valueOf(3));
		// Release three SFCs for passed shop order
		shopOrderService.releaseShopOrder(releaseShopOrderReq);
		MessageTypeBasicConfiguration messageType = resolveMessageType("FREE_FORM");
		CreateFreeFormMessageRequest message = new CreateFreeFormMessageRequest(messageType.getRef(), "Shop Order Message");
		message.setBody("Shop Order was created and released :" + request.getShopOrderRef());
		// Create FREE_FROM message
		messageService.createFreeFormMessage(message);
		CustomShopOrderResponse response = new CustomShopOrderResponse();
		response.setShopOrderRef(request.getShopOrderRef());
		response.setSite(request.getSite());
		return response;
		
	}
	public void setShopOrderService(ShopOrderServiceInterface shopOrderService) {
		this.shopOrderService = shopOrderService;
    }

	protected MessageTypeBasicConfiguration resolveMessageType(String messageType) throws BusinessException {
		FindMessageTypeRequest messageTypeRequest = new FindMessageTypeRequest(messageType);
		MessageTypeBasicConfiguration result = messageTypeService.findMessageTypeConfigurationByName(messageTypeRequest);
		return result;
	}

}