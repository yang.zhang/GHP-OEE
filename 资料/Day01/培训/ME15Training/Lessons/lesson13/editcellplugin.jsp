<%@page pageEncoding="UTF-8"%>
<%@ page language="java"%>
<%@ taglib prefix="h" uri="http://java.sap.com/jsf/html"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@ taglib prefix="sap" uri="http://java.sap.com/jsf/html/extended"%>

<f:subview id="EditCellView">
	<sap:panel id="EditCellView" binding="#{tableCellEditPlugin.container}"
		width="100%" height="100%" isCollapsible="false"
		contentAreaDesign="transparent" borderDesign="none">
        <!-- Header area title  -->
		<f:facet name="header">
			<h:outputText id="headerTitle" value="Edit Cell List" />
		</f:facet>
		<sap:ajaxUpdate render="#{sap:toClientId('EditCellView')}"/>
		
		<sap:panelGrid width="100%" height="100%">
			<sap:panelRow>
				<sap:panelGroup backgroundDesign="transparent" halign="start"
					valign="top">
					<sap:panelGrid
						binding="#{tableCellEditPlugin.pluginMessageMatrixLayout}" />
				</sap:panelGroup>
			</sap:panelRow>
			<sap:panelRow>
				<sap:panelGroup>
					<sap:panel isCollapsible="false">
						<!--Data Table-->
	                    <!-- 'value' attribute it is the current value of this component, i.e. the table data model instance   -->
					    <!-- 'binding' attribute it is the ValueExpression linking this component to a property in a backing bean   -->
						<sap:dataTable binding="#{tableCellEditPlugin.configBean.table}"
							rows="5" value="#{tableCellEditPlugin.cellEditList}" var="row"
							id="sfcDataTable">
						</sap:dataTable>
					</sap:panel>
				</sap:panelGroup>
			</sap:panelRow>
			<sap:panelRow cellHalign="center" cellValign="top">
				<sap:panelGroup>
				     <!-- Button -->
				     <!-- 'action' attribute it is the MethodExpression representing the application action 
				     to invoke when this component is activated by the user.   -->
					<sap:commandButton id="close"
						value="#{gapiI18nTransformer['SFC_STEP_STATUS_PLUGIN.close.text.BUTTON']}"
						width="80px" action="#{tableCellEditPlugin.closePlugin}">
						<sap:ajaxUpdate />
					</sap:commandButton>
				</sap:panelGroup>
			</sap:panelRow>
		</sap:panelGrid>
	</sap:panel>
</f:subview>
