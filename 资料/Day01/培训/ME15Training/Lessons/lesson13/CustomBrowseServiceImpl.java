package com.vendorID.services.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.me.frame.domain.BusinessException;
import com.sap.me.production.FindSfcDataBySfcRequest;
import com.sap.me.production.FindSfcDataBySfcResponse;
import com.sap.me.production.SfcDataField;
import com.sap.me.production.SfcDataServiceInterface;
import com.vendorID.service.dto.CustomBrowseServiceDTO;
import com.vendorID.service.dto.CustomBrowseServiceRequest;
import com.vendorID.services.CustomBrowseServiceInterface;

public class CustomBrowseServiceImpl implements CustomBrowseServiceInterface {
	private SfcDataServiceInterface sfcDataService;
	private String sfcRef;

	public String getSfcRef() {
		return sfcRef;
	}

	public void setSfcRef(String sfcRef) {
		this.sfcRef = sfcRef;
	}

	public SfcDataServiceInterface getSfcDataService() {
		return sfcDataService;
	}

	public void setSfcDataService(SfcDataServiceInterface sfcDataService) {
		this.sfcDataService = sfcDataService;
	}

	@Override
	public List<CustomBrowseServiceDTO> findSfcData(CustomBrowseServiceRequest inputDTO) throws BusinessException {
		List<CustomBrowseServiceDTO> list = new ArrayList<CustomBrowseServiceDTO>();
		FindSfcDataBySfcRequest request = new FindSfcDataBySfcRequest();
		FindSfcDataBySfcResponse response = null;
		request.setSfcRef(inputDTO.getSfcRef());
		response = this.sfcDataService.findSfcDataBySfc(request);
		List<SfcDataField> dataFieldList = response.getSfcDataFieldList();
		if (dataFieldList != null && dataFieldList.size() > 0) {
			Iterator<SfcDataField> iterator = dataFieldList.iterator();
			while (iterator.hasNext()) {
				SfcDataField df = iterator.next();
				CustomBrowseServiceDTO dto = new CustomBrowseServiceDTO();
				if (inputDTO.getSfcDataField() != null && df.getAttribute().startsWith(inputDTO.getSfcDataField()))
				{
					dto.setAttribute(df.getAttribute());
					dto.setValue(df.getValue());
					list.add(dto);
					continue;
				}
				if (inputDTO.getSfcDataField() == null) {
					dto.setAttribute(df.getAttribute());
					dto.setValue(df.getValue());
					list.add(dto);
				}
			}
		}
		return list;
	}
}
