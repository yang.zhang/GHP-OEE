package com.vendorID.service.dto;

public class CustomBrowseServiceRequest {
	private String sfcRef;
	private String sfcDataField;

	public String getSfcRef() {
		return sfcRef;
	}

	public void setSfcRef(String sfcRef) {
		this.sfcRef = sfcRef;
	}

	public String getSfcDataField() {
		return sfcDataField;
	}

	public void setSfcDataField(String sfcDataField) {
		this.sfcDataField = sfcDataField;
	}
}
