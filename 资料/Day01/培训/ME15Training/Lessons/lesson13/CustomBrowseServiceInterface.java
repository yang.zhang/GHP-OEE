package com.vendorID.services;

import java.util.List;

import com.sap.me.frame.domain.BusinessException;
import com.vendorID.service.dto.CustomBrowseServiceDTO;
import com.vendorID.service.dto.CustomBrowseServiceRequest;

public interface CustomBrowseServiceInterface {

	List<CustomBrowseServiceDTO> findSfcData(CustomBrowseServiceRequest dto) throws BusinessException;
}