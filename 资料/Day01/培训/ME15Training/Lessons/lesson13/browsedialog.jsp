<%@page pageEncoding="UTF-8"%>
<%@ page language="java"%>
<%@ taglib prefix="h" uri="http://java.sap.com/jsf/html"%>
<%@ taglib prefix="f" uri="http://java.sap.com/jsf/core"%>
<%@ taglib prefix="sap" uri="http://java.sap.com/jsf/html/extended"%>
<f:subview id="browseDialogView">
    <sap:panel id="browseDialog" width="100%" height="100%" contentAreaDesign="transparent" borderDesign="none"
               isCollapsible="false" binding="#{customBrowsePlugin.container}">

        <f:facet name="header">
           <h:outputText value="#{customBrowsePlugin.header}" />
           
        </f:facet>
		<sap:ajaxUpdate render="#{sap:toClientId('browseDialog')}"/>

        <sap:panelGrid cellBackgroundDesign="transparent" width="100%" height="100%" cellHalign="start" cellValign="top">

            <sap:panelRow>
                <sap:panelGroup backgroundDesign="transparent" width="100%" >
                    <sap:panel isCollapsible="false" width="100%" contentAreaDesign="transparent" borderDesign="none">
                        <sap:panelGrid binding="#{customBrowsePlugin.pluginMessageLayout}" width="100%" />
                    </sap:panel>
                </sap:panelGroup>
            </sap:panelRow>

            <sap:panelRow>
                <sap:panelGroup backgroundDesign="transparent" width="100%" height="100%" >
                    <sap:panel isCollapsible="false" width="100%" height="100%" contentAreaDesign="transparent" borderDesign="none">
                        <sap:dataTable id="browse_table" binding="#{customBrowsePlugin.configBean.table}"
                                        width="100%" height="100%" rows="20"
                                        value="#{customBrowsePlugin.data}" var="row" first="0">
                        </sap:dataTable>
                    </sap:panel>
                </sap:panelGroup>
            </sap:panelRow>
            <sap:panelRow>
                <sap:panelGroup backgroundDesign="transparent" width="100%" height="0px" >
                    <sap:commandButtonLarge id="submit" width="0px" height="0px"
                                            action="#{customBrowsePlugin.okAction}"/>

                    <sap:commandButtonLarge id="cancel" width="0px" height="0px"
                                            action="#{customBrowsePlugin.cancelAction}"/>
                </sap:panelGroup>
            </sap:panelRow>
        </sap:panelGrid>
    </sap:panel>

</f:subview>