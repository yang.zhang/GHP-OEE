package com.vendorID.schedule;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;

import com.sap.me.common.ArchiveRequest;
import com.sap.me.common.ArchiveResponse;
import com.sap.me.demand.FindShopOrderByKeyFieldsRequest;
import com.sap.me.demand.ShopOrderArchiveServiceInterface;
import com.sap.me.demand.ShopOrderBasicConfiguration;
import com.sap.me.demand.ShopOrderServiceInterface;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.frame.utils.I18nUtility;
import com.sap.scheduler.runtime.JobContext;
import com.sap.scheduler.runtime.JobParameter;
import com.sap.scheduler.runtime.mdb.MDBJobImplementation;

/**
 * Message-Driven Bean implementation class for: ShopOrderArchiveJob
 * 
 */
@MessageDriven(activationConfig = { @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"), 
@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "JobDefinition=\'ShopOrderArchiveJob\'") })
public class ShopOrderArchiveJob extends MDBJobImplementation {

	/**
	 * Default constructor.
	 */
	public ShopOrderArchiveJob() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onJob(JobContext jobContext) throws Exception {
		Logger logger = jobContext.getLogger();
		String shopOrder = null;
		JobParameter shopOrderParameter = jobContext.getJobParameter("ShopOrder");
		if (shopOrderParameter != null) {
			shopOrder = shopOrderParameter.getStringValue();
			logger.info("Shop Order input: " + shopOrder);
			ShopOrderServiceInterface svc = Services.getService("com.sap.me.demand", "ShopOrderService", "MYSITE");
			FindShopOrderByKeyFieldsRequest request = new FindShopOrderByKeyFieldsRequest();
			request.setShopOrder(shopOrder);
			JobParameter resultParameter = jobContext.getJobParameter("ArchiveResult");
			try {
				ShopOrderBasicConfiguration config = svc.findShopOrderByKeyFields(request);
				if (config != null && config.getShopOrderRef() != null) {
					String shopOrderRef = config.getShopOrderRef();
					ShopOrderArchiveServiceInterface archive = Services.getService("com.sap.me.demand", "ShopOrderArchiveService", "MYSITE");
					ArchiveRequest arcRequest = new ArchiveRequest();
					arcRequest.setArchiving(true);
					arcRequest.setIdentifier(shopOrderRef);
					ArchiveResponse response = archive.archive(arcRequest);
					resultParameter.setStringValue(" # ShopOrders archived:" + response.getTotalNumber());
					jobContext.setJobParameter(resultParameter);
					logger.info("Archive Response: " + resultParameter.getStringValue());
				}
			} catch (BusinessException ex) {
				logger.severe("Error executing Shop Order Archive:" + I18nUtility.getMessageForException(ex));
			}
		}
	}

}
