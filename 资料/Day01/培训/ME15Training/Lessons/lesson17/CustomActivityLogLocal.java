package com.vendorID.ejb;

import javax.ejb.Local;

import com.sap.me.frame.domain.BusinessException;
import com.sap.me.production.AssemblyDataValidationRequest;

@Local
public interface CustomActivityLogLocal {
	public void logError(AssemblyDataValidationRequest assemblyData) throws BusinessException ;

}
