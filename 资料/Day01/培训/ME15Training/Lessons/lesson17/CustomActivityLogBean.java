package com.vendorID.ejb;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.sap.me.activity.ActivityLogServiceInterface;
import com.sap.me.activity.LogActivityRequest;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.production.AssemblyDataValidationRequest;

@Stateless 
public class CustomActivityLogBean implements CustomActivityLogLocal{

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void logError(AssemblyDataValidationRequest assemblyData) throws BusinessException {
		// Get ActivityLog API
		ActivityLogServiceInterface activityLogService = Services.getService("com.sap.me.activity", "ActivityLogService");
		// Create production request
		LogActivityRequest logRequest = new LogActivityRequest();
		logRequest.setEvent("customLog:FailedAssembly");
		logRequest.setItemRef(assemblyData.getItemRef());
		logRequest.setSfcRef(assemblyData.getSfcRef());
		// force to do activity log even if Log Activity field is un-checked
		logRequest.setForceActivityLog(Boolean.TRUE);
		// call logActivity() method to create an activity log entry in ACTIVITY_LOG table
		activityLogService.logActivity(logRequest);
	}
}
