<%@page pageEncoding="UTF-8"%>
<%@ page language="java"%>
<%@ taglib prefix="h" uri="http://java.sap.com/jsf/html"%>
<%@ taglib prefix="f" uri="http://java.sap.com/jsf/core"%>
<%@ taglib prefix="sap" uri="http://java.sap.com/jsf/html/extended"%>

<f:subview id="sfcReportPluginView">

	<sap:panel id="sfcReportPlugin" binding="#{sfcReportPlugin.container}"
		width="100%" height="100%" isCollapsible="false"
		contentAreaDesign="transparent" borderDesign="none">
        <!-- Header area title  -->
		<f:facet name="header">
			<h:outputText id="headerTitle"
				value="SFC Report POD Plugin" />
		</f:facet>
		<f:facet name="toolbar">
			<sap:toolBar>
				<f:facet name="rightAlignedItems">
					<h:panelGroup>
						<h:commandButton id="helpButton"
							image="/com/vendorID/icons/help.png"
							title="#{gapiI18nTransformer['toolTip.help.TEXT']}"
							action="#{sfcReportPlugin.showPluginHelp}">
							<sap:ajaxUpdate />
						</h:commandButton>
					</h:panelGroup>
				</f:facet>
			</sap:toolBar>
		</f:facet>
		<sap:panelGrid width="100%" height="100%">
			<sap:panelRow>
				<sap:panelGroup backgroundDesign="transparent" halign="start"
					valign="top">
					<sap:panelGrid binding="#{sfcReportPlugin.pluginMessageLayout}" />
				</sap:panelGroup>
			</sap:panelRow>
			<sap:panelRow>
				<sap:panelGroup>
					<sap:panel isCollapsible="false">
						<!--Data Table-->
	                    <!-- 'value' attribute it is the current value of this component, i.e. the table data model instance   -->
					    <!-- 'binding' attribute it is the ValueExpression linking this component to a property in a backing bean   -->
						<sap:dataTable binding="#{sfcReportPlugin.tableConfig.table}"
							rows="5" value="#{sfcReportPlugin.sfcList}" var="row" 
							id="sfcDataTable">
						</sap:dataTable>
					</sap:panel>
				</sap:panelGroup>
			</sap:panelRow>
			<sap:panelRow cellHalign="center" cellValign="top" cellpadding="2px 4px">
				<sap:panelGroup>
				    <sap:commandButton id="details" 
						value="#{gapiI18nTransformer['SFC_STEP_STATUS_PLUGIN.view.text.BUTTON']}"
						width="85px" action="#{sfcReportPlugin.viewSfcStepGUI}">
						<sap:ajaxUpdate />
					</sap:commandButton>
		            <sap:outputLabel value="   " />
					<sap:commandButton id="close"
						value="#{gapiI18nTransformer['SFC_STEP_STATUS_PLUGIN.close.text.BUTTON']}"
						width="80px" action="#{sfcReportPlugin.closePlugin}"
						rendered="#{!sfcReportPlugin.defaultPlugin}">
						<sap:ajaxUpdate />
					</sap:commandButton>
				</sap:panelGroup>
			</sap:panelRow>
		</sap:panelGrid>
	</sap:panel>
</f:subview>