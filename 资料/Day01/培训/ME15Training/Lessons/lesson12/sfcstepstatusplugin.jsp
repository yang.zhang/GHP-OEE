<%@page pageEncoding="UTF-8"%>
<%@ page language="java"%>
<%@ taglib prefix="h" uri="http://java.sap.com/jsf/html"%>
<%@ taglib prefix="f" uri="http://java.sap.com/jsf/core"%>
<%@ taglib prefix="sap" uri="http://java.sap.com/jsf/html/extended"%>

<f:subview id="sfcStepStatusView">

	<sap:panel id="sfcReportPlugin"
		binding="#{sfcStepStatusPlugin.container}" width="100%" height="100%"
		isCollapsible="false" contentAreaDesign="transparent"
		borderDesign="none">
        <!-- Header area title  -->
		<f:facet name="header">
			<h:outputText id="headerTitle"
				value="Sfc Step Status Plugin" />
		</f:facet>
		<f:facet name="toolbar">
			<sap:toolBar>
				<f:facet name="rightAlignedItems">
					<h:panelGroup>
						<h:commandButton id="helpButton"
							image="/com/vendorID/icons/help.png"
							title="#{gapiI18nTransformer['toolTip.help.TEXT']}"
							action="#{sfcReportPlugin.showPluginHelp}">
							<sap:ajaxUpdate />
						</h:commandButton>
					</h:panelGroup>
				</f:facet>
			</sap:toolBar>
		</f:facet>

		<sap:panelGrid width="100%" height="100%">
			<sap:panelRow>
				<sap:panelGroup backgroundDesign="transparent" halign="start"
					valign="top">
					<sap:panelGrid
						binding="#{sfcStepStatusPlugin.pluginMessageMatrixLayout}" />
				</sap:panelGroup>
			</sap:panelRow>
			<sap:panelRow>
				<sap:panelGroup>
					<sap:panel isCollapsible="false">
						<!--Data Table-->
	                    <!-- 'value' attribute it is the current value of this component, i.e. the table data model instance   -->
					    <!-- 'binding' attribute it is the ValueExpression linking this component to a property in a backing bean   -->
						<sap:dataTable binding="#{sfcStepStatusPlugin.tableConfig.table}"
							rows="10" value="#{sfcStepStatusPlugin.sfcStepList}" var="row"
							id="sfcDataTable">
						</sap:dataTable>
					</sap:panel>
				</sap:panelGroup>
			</sap:panelRow>
			<sap:panelRow cellHalign="center" cellValign="top">
				<sap:panelGroup>
					<sap:commandButton id="close"
						value="#{gapiI18nTransformer['SFC_STEP_STATUS_PLUGIN.close.text.BUTTON']}"
						width="80px" action="#{sfcStepStatusPlugin.closePlugin}">
						<sap:ajaxUpdate />
					</sap:commandButton>
				</sap:panelGroup>
			</sap:panelRow>
		</sap:panelGrid>
	</sap:panel>
</f:subview>