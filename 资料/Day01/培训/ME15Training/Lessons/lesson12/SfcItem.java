package com.vendorID.wpmf.web.podplugin;

import java.math.BigDecimal;

public class SfcItem {

	private String sfc;
	private String status;
	private String materialVersion;
	private BigDecimal priority;
	private String shopOrder;
	private BigDecimal qty;

	public void setSfc(String sfc) {
		this.sfc = sfc;
	}

	public String getSfc() {
		return sfc;
	}

	public void setShopOrder(String shopOrder) {
		this.shopOrder = shopOrder;
	}

	public String getShopOrder() {
		return shopOrder;
	}

	public void setMaterialVersion(String materialVersion) {
		this.materialVersion = materialVersion;
	}

	public String getMaterialVersion() {
		return materialVersion;
	}

	
	public void setStatus(String status) {
		this.status = status;
	}


	public String getStatus() {
		return status;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public BigDecimal getQty() {
		return qty;
	}

	
	public void setPriority(BigDecimal priority) {
		this.priority = priority;
	}

	
	public BigDecimal getPriority() {
		return priority;
	}

}
