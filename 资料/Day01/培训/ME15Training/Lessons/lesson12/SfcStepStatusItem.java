package com.vendorID.wpmf.web.podplugin;

import java.math.BigDecimal;

public class SfcStepStatusItem {
	private String routerVersion;
	private String stepId;
	private String operation;
	private String status;
	private BigDecimal qtyInWork;
	private BigDecimal qtyInQueue;
	private BigDecimal qtyCompleted;


	public void setStepId(String stepId) {
		this.stepId = stepId;
	}


	public String getStepId() {
		return stepId;
	}

	public void setRouterVersion(String routerVersion) {
		this.routerVersion = routerVersion;
	}

	public String getRouterVersion() {
		return routerVersion;
	}


	public void setOperation(String operation) {
		this.operation = operation;
	}


	public String getOperation() {
		return operation;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getStatus() {
		return status;
	}


	public void setQtyInWork(BigDecimal qtyInWork) {
		this.qtyInWork = qtyInWork;
	}


	public BigDecimal getQtyInWork() {
		return qtyInWork;
	}

	public void setQtyInQueue(BigDecimal qtyInQueue) {
		this.qtyInQueue = qtyInQueue;
	}

	public BigDecimal getQtyInQueue() {
		return qtyInQueue;
	}

	public void setQtyCompleted(BigDecimal qtyCompleted) {
		this.qtyCompleted = qtyCompleted;
	}


	public BigDecimal getQtyCompleted() {
		return qtyCompleted;
	}

}
