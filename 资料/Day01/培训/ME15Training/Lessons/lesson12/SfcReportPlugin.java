package com.vendorID.wpmf.web.podplugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import javax.faces.event.ActionEvent;

import com.sap.me.common.ObjectReference;
import com.sap.me.demand.ShopOrderFullConfiguration;
import com.sap.me.demand.ShopOrderServiceInterface;
import com.sap.me.demand.ShopOrderSfc;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.production.SfcBasicData;
import com.sap.me.production.SfcStateServiceInterface;
import com.sap.me.production.podclient.BasePodPlugin;
import com.sap.me.production.podclient.PodClientTableConfigurator;
import com.sap.me.production.podclient.SfcChangeEvent;
import com.sap.me.production.podclient.SfcChangeListenerInterface;
import com.sap.me.production.podclient.SfcSelection;
import com.sap.me.status.StatusBasicConfiguration;
import com.sap.me.status.StatusServiceInterface;
import com.sap.me.wpmf.MessageType;
import com.sap.me.wpmf.TableColumnSortListener;
import com.sap.me.wpmf.TableConfigurator;
import com.sap.me.wpmf.TableSortComparator;
import com.sap.me.wpmf.util.ExceptionHandler;
import com.sap.me.wpmf.util.FacesUtility;
import com.sap.me.wpmf.util.MessageHandler;
import com.sap.ui.faces.component.sap.UIColumn;
import com.sap.ui.faces.component.sap.UIRowSelector;
import com.sap.ui.faces.event.sap.SortActionEvent;
import com.sap.ui.faces.event.sap.TypedActionEvent;

public class SfcReportPlugin extends BasePodPlugin implements SfcChangeListenerInterface, TableColumnSortListener {

	private List<SfcItem> sfcList;
	private TableConfigurator tableConfig;
	private SfcStateServiceInterface sfcStateService = Services.getService("com.sap.me.production", "SfcStateService");
	private ShopOrderServiceInterface shopOrderService = Services.getService("com.sap.me.demand", "ShopOrderService");
	StatusServiceInterface statusService = Services.getService("com.sap.me.status", "StatusService");

	private boolean defaultPlugin = false;

	private UIColumn currentSortColumn = null;

	@Override
	public void beforeLoad() throws Exception {
		//Resolves an EL (expression language) Expression to get table configuration from faces-config.xml
		TableConfigurator table = (TableConfigurator) FacesUtility.resolveExpression("#{sfcDataConfigurator}");
		setTableConfig(table);
		// Retrieve data to be displayed in POD Plugin table
		populateTable();
		savePluginInSession();
		
	}
	
	private SfcReportPlugin findPluginInSession() {
		SfcReportPlugin sessionPlugin = (SfcReportPlugin) FacesUtility.getSessionMapValue("sfcReportPlugin");
		return sessionPlugin;
	}

	private void savePluginInSession() {
		SfcReportPlugin sessionPlugin = findPluginInSession();
		if (this != sessionPlugin) {
			FacesUtility.setSessionMapValue("sfcReportPlugin", this);
    }
	}
	
	/**
	 * Default constructor. Registers the implemented listeners to the WPMF
	 * framework.
	 */
	public SfcReportPlugin() {
		super();
		//Adds the implemented listeners to the framework 
		getPluginEventManager().addPluginListeners(this.getClass());
		//Returns whether this is the default plugin or not 
		defaultPlugin = super.isDefaultPlugin();
		//called to end processing of plugin and start next plugin in chain
		complete();
	}
	
	public boolean isDefaultPlugin() {
		return defaultPlugin;
	}

	public void populateTable() {
		try {
			//Will clear the input Plugin message area 
			MessageHandler.clear(this);
			sfcList = null;
			TableConfigurator table = getTableConfig();
			if (table != null && table.getSelectedItems() != null && table.getSelectedItems().size() > 0) {
				//Clears the selected rows from the local model
				table.clearSelectedRows();
			}
			//Returns a resolved list of SFC selections based on the selection criteria
			List<SfcSelection> resolvedSfcList = getPodSelectionModel().getResolvedSfcs();
			if (resolvedSfcList != null) {
				for (SfcSelection sfc : resolvedSfcList) {
					if (sfc.getSfc() == null)
						continue;
					ObjectReference sfcRef = new ObjectReference(sfc.getSfc().getSfcRef());
					SfcBasicData sfcBasicData = sfcStateService.findSfcDataByRef(sfcRef);
					if (sfcBasicData == null)
						continue;
					ObjectReference shopOrderRef = new ObjectReference(sfcBasicData.getShopOrderRef());
					ShopOrderFullConfiguration shopOrderConfig = shopOrderService.readShopOrder(shopOrderRef);
					List<ShopOrderSfc> shopOrderSfcList = shopOrderConfig.getShopOrderSfcList();
					if (shopOrderSfcList != null) {
						for (ShopOrderSfc shopOrderSfc : shopOrderSfcList) {
							// Retrieve data to be displayed at POD Plugin table
							buildSfcData(shopOrderSfc.getSfcRef());
						}
					}
				}
			}
		} catch (BusinessException bEx) {
			//Handles the specified exception by logging and displaying a message. 
			//The message will be placed in the plugins Message area 
			ExceptionHandler.handle(bEx, this);
		}
	}

	public void closePlugin() {
		//Closes the current plugin, clears messages in the global area 
		closeCurrentPlugin();
		FacesUtility.setSessionMapValue("sfcReportPlugin", null);
		FacesUtility.setSessionMapValue("sfcDataConfigurator", null);

	}

	public void viewSfcStepGUI() {
         // if row in table is selected and clicked View button
		if (getSelectedItemsSize() > 0) {
			TableConfigurator table = getTableConfig();
			// get first record
			SfcItem selectedElem = (SfcItem) table.getSelectedItems().get(0);
			saveSelectedSfcInSession(selectedElem);
			//Convenience method for Plugin's to execute a button activity. 
			executePluginButtonActivity(getActivityOption(getActivityID(), "SFC_STEP_PLUGIN_ID"));
						
		}
	}

	private void saveSelectedSfcInSession(SfcItem data) {

		FacesUtility.setSessionMapValue("sfcStepStatusPlugin_SFC", data.getSfc());
		FacesUtility.setSessionMapValue("sfcStepStatusPlugin", null);
		FacesUtility.setSessionMapValue("sfcStepStatusConfigurator", null);

	}

	/**
	 * Listener method for SfcChangeEvent. SFC changed at the POD Selection model 
	 * 
	 */
	public void processSfcChange(SfcChangeEvent event) {
		try {
			populateTable();
		} catch (Exception bEx) {
			ExceptionHandler.handle(bEx, this);
		}
	}

	
	private void buildSfcData(String sfcRef) throws BusinessException {
		if (sfcList == null) {
			sfcList = new ArrayList<SfcItem>();
		}
		SfcItem sfcDataItem = new SfcItem();
		SfcBasicData sfcBasicData = sfcStateService.findSfcDataByRef(new ObjectReference(sfcRef));
		String item = parseHandle (sfcBasicData.getItemRef(), 2);
		String version = parseHandle (sfcBasicData.getItemRef(), 3);
		sfcDataItem.setMaterialVersion(item + "/" + version);
		sfcDataItem.setPriority(sfcBasicData.getPriority());
		sfcDataItem.setQty(sfcBasicData.getQty());
		sfcDataItem.setSfc(sfcBasicData.getSfc());
		String shopOrder = parseHandle (sfcBasicData.getShopOrderRef(), 2);
		sfcDataItem.setShopOrder(shopOrder);
		StatusBasicConfiguration status = statusService.findStatusByRef(new ObjectReference(sfcBasicData.getStatusRef()));
		sfcDataItem.setStatus(status.getStatusDescription());

		sfcList.add(sfcDataItem);
	}

	public void setTableConfig(TableConfigurator config) {
		if (config == null) {
			tableConfig = new PodClientTableConfigurator();
		} else {
			tableConfig = config;
		}
		tableConfig.setMultiSelectType(false);
		tableConfig.setDoubleClick(true);
		//Set selection action event handler method using a valid el expression syntax
		// Double clicked on the record in table
		tableConfig.setSelectionActionExpression("#{sfcReportPlugin.processSelectAction}");

		List<String> sortableCols = new ArrayList<String>();
		sortableCols.add("SFC");
		sortableCols.add("STATUS");
		sortableCols.add("QTY");
		sortableCols.add("MATERIAL_VERSION");
		sortableCols.add("SHOP_ORDER");
		sortableCols.add("PRIORITY");

		tableConfig.setSortableColumns(sortableCols);
		//Add a TableColumnSortListener to this configurator
		tableConfig.addColumnSortListener(this);

	}
	/**
	 * Returns the TableConfigurator bean
	 * 
	 * @return configBean the TableConfigurator
	 */
	public TableConfigurator getTableConfig() {
		return tableConfig;
	}
	public void processColumnSort(SortActionEvent sortEvent) {
		currentSortColumn = (UIColumn) sortEvent.getSource();
		performUserPreferredSorting((List<SfcItem>) getTableConfig().getTable().getValue());
	}

	/**
	 * Process User preferred column sorting based on the last sorted column
	 * detail/order.
	 * 
	 * @param worklist
	 * @return ArrayList The sorted worklist
	 */
	@SuppressWarnings("unchecked")
	private List performUserPreferredSorting(List sfcList) {
		if (sfcList == null)
			return sfcList;
		if (currentSortColumn == null) {
			// If null, no user sort was performed in the session.
			return sfcList;
		}
		String columnName = currentSortColumn.getId();
		if (columnName == null)
			return sfcList;
		if (!tableConfig.isSortableColumn(columnName))
			return sfcList;
		String sortState = currentSortColumn.getSortState();
		if (sortState == null)
			sortState = "ascending";
		String bindingProperty = tableConfig.getColumnBindingProperty(columnName);
		
		Collections.sort(sfcList, new TableSortComparator<SfcItem>(bindingProperty, TableSortComparator.SortState.valueOf(sortState)));
		return sfcList;
	}

	public int getSelectedItemsSize() {
		TableConfigurator table = getTableConfig();
		if (table != null && table.getSelectedItems() != null && table.getSelectedItems().size() > 0) {
			return table.getSelectedItems().size();
		}
		return 0;
	}
   // Open Help window
	public void showPluginHelp() {
		//Returns the absolute url by appending the input partial url 
		String helpUrl = FacesUtility.getAbsoluteUrl("/com/vendorID/wpmf/podplugin/SamplePluginHelp.html");
		//Displays the input URL in a Internet Explorer window. 
		postWindow(helpUrl, null, 0, 0, 0, 0, false, true, true, false);
	}
	// Double clicked on the record in table and selection action event handler method is executed 
	public void processSelectAction(ActionEvent event) {
		MessageHandler.clear(this);
		TypedActionEvent tevent = null;
		if (event instanceof TypedActionEvent) {
			tevent = (TypedActionEvent) event;
			Enum eventType = tevent.getEventType();
			if (eventType instanceof UIRowSelector.EventType) {
				UIRowSelector.EventType rowSelectorEventType = (UIRowSelector.EventType) eventType;
				if (rowSelectorEventType == UIRowSelector.EventType.ROW_DOUBLE_CLICK) {
					List<SfcItem> selectedOnes = (List<SfcItem>) tableConfig.getSelectedItems();
					SfcItem selectedElement = selectedOnes.get(0);
					List<SfcSelection> sfcsToProcess = new ArrayList<SfcSelection>();
					SfcSelection sfcSelect = new SfcSelection();
					sfcSelect.setInputId(selectedElement.getSfc());
					sfcsToProcess.add(sfcSelect);
					// Sets the current SFC selection criteria and fire event
					getPodSelectionModel().setSfcs(sfcsToProcess);
					SfcChangeEvent sfcChangeEvent = new SfcChangeEvent(this);
					fireEvent(sfcChangeEvent, SfcChangeListenerInterface.class, "processSfcChange");
            		// Use plugin message area
					MessageHandler.handle("Double Click Action for SFC " + selectedElement.getSfc(), null, MessageType.INFO, this);

				}

			}
		}
	}
	
	private String parseHandle(String input , int comp) {
		ArrayList<String> list = new ArrayList<String>();
		StringTokenizer t = new StringTokenizer(input, ":,");
		// Get all of the pieces
		while (t.hasMoreTokens()) {
			// Add them to a temp list
			list.add(t.nextToken());
		}
		return (String) list.get(comp);
	}

	
	public void setSfcList(List<SfcItem> sfcList) {
		this.sfcList = sfcList;
	}

	public List<SfcItem> getSfcList() {
		return sfcList;
	}

}
