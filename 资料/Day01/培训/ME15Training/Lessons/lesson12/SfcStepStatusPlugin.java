package com.vendorID.wpmf.web.podplugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.frame.service.CommonMethods;
import com.sap.me.production.FindStepMaintDetailsRequest;
import com.sap.me.production.FindStepMaintDetailsResponse;
import com.sap.me.production.SfcStateServiceInterface;
import com.sap.me.production.SfcStepDetails;
import com.sap.me.production.podclient.BasePodPlugin;
import com.sap.me.production.podclient.PodClientTableConfigurator;
import com.sap.me.wpmf.MessageType;
import com.sap.me.wpmf.TableColumnSortListener;
import com.sap.me.wpmf.TableConfigurator;
import com.sap.me.wpmf.TableSortComparator;
import com.sap.me.wpmf.util.ExceptionHandler;
import com.sap.me.wpmf.util.FacesUtility;
import com.sap.me.wpmf.util.MessageHandler;
import com.sap.ui.faces.component.sap.UIColumn;
import com.sap.ui.faces.event.sap.SortActionEvent;

public class SfcStepStatusPlugin extends BasePodPlugin implements TableColumnSortListener {

	private static final long serialVersionUID = -7968380049713345742L;

	private List<SfcStepStatusItem> sfcStepList;
	private TableConfigurator tableConfig;
	private SfcStateServiceInterface sfcStateService = Services.getService("com.sap.me.production", "SfcStateService");
	private UIColumn currentSortColumn = null;
	private String sfc = null;

	/**
	 * Default constructor. Registers the implemented listeners to the WPMF framework.
	 */
	public SfcStepStatusPlugin() {
		super();
		// Adds the implemented listeners to the framework
		getPluginEventManager().addPluginListeners(this.getClass());
		// called to end processing of plugin and start next plugin in chain
		complete();
	}

	@Override
	public void beforeLoad() throws Exception {
		// Resolves an EL (expression language) Expression to get table configuration from faces-config.xml
		TableConfigurator table = (TableConfigurator) FacesUtility.resolveExpression("#{sfcStepStatusConfigurator}");
		setTableConfig(table);
		populateTable();
		savePluginInSession();

	}

	private SfcStepStatusPlugin findPluginInSession() {
		SfcStepStatusPlugin sessionPlugin = (SfcStepStatusPlugin) FacesUtility.getSessionMapValue("sfcStepStatusPlugin");
		return sessionPlugin;
	}

	private void savePluginInSession() {
		SfcStepStatusPlugin sessionPlugin = findPluginInSession();
		if (this != sessionPlugin) {
			FacesUtility.setSessionMapValue("sfcStepStatusPlugin", this);
		}
	}

	
	public void closePlugin() {
		// Closes the current plugin, clears messages in the global area
		closeCurrentPlugin();
		FacesUtility.setSessionMapValue("sfcStepStatusPlugin", null);
		FacesUtility.setSessionMapValue("sfcStepStatusConfigurator", null);

	}
	/**
	 * Clears the model and other states for case plugin activity is closed
	 * (like a cross icon in for Win systems)
	 */
	@Override
	public void processDialogClosed() {
		FacesUtility.setSessionMapValue("sfcStepStatusPlugin", null);
		FacesUtility.setSessionMapValue("sfcStepStatusConfigurator", null);
		super.processDialogClosed();
	}
	
	public void populateTable() {
		// Will clear the input Plugin message area
		MessageHandler.clear(this);
		this.sfcStepList = null;
		// Returns an Object from the session map
		String sfc = (String) FacesUtility.getSessionMapValue("sfcStepStatusPlugin_SFC");
		setSfc(sfc);
		try {
			String site = CommonMethods.getSite();
			String sfcRef = buildSFCBOHandle(site, getSfc());
			FindStepMaintDetailsRequest request = new FindStepMaintDetailsRequest();
			request.setSfcRef(sfcRef);
			FindStepMaintDetailsResponse sfcStepColl = sfcStateService.findStepDetailsBySfc(request);
			List<SfcStepDetails> stepDetails = sfcStepColl.getSfcStepDetailsCollection();
			if (stepDetails != null) {
				for (SfcStepDetails sfcStep : stepDetails) {
					sfcStepMapper(sfcStep);
				}

			}

		} catch (BusinessException bEx) {
			ExceptionHandler.handle(bEx, this);
		}

		MessageHandler.handle("Selected SFC is " + getSfc(), null, MessageType.INFO, this);
	}

	/**
	 * Sets the TableConfigurator bean
	 * 
	 * @param configBean
	 */
	public void setTableConfig(TableConfigurator config) {
		if (config == null) {
			tableConfig = new PodClientTableConfigurator();
		} else {
			tableConfig = config;
		}
		tableConfig.setMultiSelectType(false);
		tableConfig.setDoubleClick(false);
		tableConfig.setAllowSelections(false);

		List<String> sortableCols = new ArrayList<String>();
		sortableCols.add("OPERATION");
		sortableCols.add("ROUTER_VERSION");
		sortableCols.add("STEP_ID");
		sortableCols.add("STATUS");
		sortableCols.add("QTY_IN_WORK");
		sortableCols.add("QTY_IN_QUEUE");
		sortableCols.add("QTY_COMPLETED");

		tableConfig.setSortableColumns(sortableCols);
		tableConfig.addColumnSortListener(this);

	}

	/**
	 * Returns the TableConfigurator bean
	 * 
	 * @return configBean the TableConfigurator
	 */
	public TableConfigurator getTableConfig() {
		return tableConfig;
	}

	/**
	 * Sets the SfcStep list
	 * 
	 * @param sfcStepList
	 */
	public void setSfc(String sfc) {
		this.sfc = sfc;
	}

	/**
	 * Returns the SfcStep list
	 * 
	 * @return sfcStepList
	 */
	public String getSfc() {
		return sfc;
	}

	public void setSfcStepList(List<SfcStepStatusItem> sfcStepList) {
		this.sfcStepList = sfcStepList;
	}

	/**
	 * Returns the SfcStep list
	 * 
	 * @return sfcStepList
	 */
	public List<SfcStepStatusItem> getSfcStepList() {
		return sfcStepList;
	}

	/**
	 * Maps the SfcStep returned from the PAPI to the SampleSfcStepStatusItem(table data object)
	 * 
	 * @param sfcStep
	 */
	private void sfcStepMapper(SfcStepDetails sfcStep) {
		if (sfcStepList == null) {
			sfcStepList = new ArrayList<SfcStepStatusItem>();
		}
		SfcStepStatusItem sampleSfcStepStatusItem = new SfcStepStatusItem();
		if (sfcStep.getStatus() != null)
			sampleSfcStepStatusItem.setStatus(sfcStep.getStatus().value());
		sampleSfcStepStatusItem.setStepId(sfcStep.getStepId());
		sampleSfcStepStatusItem.setOperation(sfcStep.getOperation());
		String router = parseHandle(sfcStep.getRouterRef(), 2);
		String version = parseHandle(sfcStep.getRouterRef(), 4);
		sampleSfcStepStatusItem.setRouterVersion(router + "/" + version);
		sampleSfcStepStatusItem.setQtyCompleted(sfcStep.getQuantityCompleted());
		sampleSfcStepStatusItem.setQtyInQueue(sfcStep.getQuantityInQueue());
		sampleSfcStepStatusItem.setQtyInWork(sfcStep.getQuantityInWork());

		sfcStepList.add(sampleSfcStepStatusItem);
	}

	public void processColumnSort(SortActionEvent sortEvent) {
		currentSortColumn = (UIColumn) sortEvent.getSource();
		performUserPreferredSorting((List<SfcStepStatusItem>) getTableConfig().getTable().getValue());

	}

	/**
	 * Process User preferred column sorting based on the last sorted column detail/order.
	 * 
	 * @param worklist
	 * @return ArrayList The sorted worklist
	 */
	@SuppressWarnings("unchecked")
	private List performUserPreferredSorting(List sfcStepList) {
		if (sfcStepList == null)
			return sfcStepList;
		if (currentSortColumn == null) {
			// If null, no user sort was performed in the session.
			return sfcStepList;
		}
		String columnName = currentSortColumn.getId();
		if (columnName == null)
			return sfcStepList;
		if (!tableConfig.isSortableColumn(columnName))
			return sfcStepList;
		String sortState = currentSortColumn.getSortState();
		if (sortState == null)
			sortState = "ascending";
		String bindingProperty = tableConfig.getColumnBindingProperty(columnName);

		Collections.sort(sfcStepList, new TableSortComparator<SfcStepStatusItem>(bindingProperty, TableSortComparator.SortState.valueOf(sortState)));
		return sfcStepList;
	}

	/**
	 * Returns the Popup dialog width int 40em
	 */
	public int getPopupWidth() {
		return 55;
	}

	/**
	 * Returns the Popup dialig height int 40em
	 */
	public int getPopupHeight() {
		return 30;
	}

	// Gets whether the popup dialog is modal.
	public boolean isPopupModal() {
		return false;
	}

	private String parseHandle(String input, int comp) {
		ArrayList<String> list = new ArrayList<String>();
		StringTokenizer t = new StringTokenizer(input, ":,");
		// Get all of the pieces
		while (t.hasMoreTokens()) {
			// Add them to a temp list
			list.add(t.nextToken());
		}
		return (String) list.get(comp);
	}

	public String buildSFCBOHandle(String site, String sfc) {
		StringBuffer sb = new StringBuffer();
		sb.append("SFCBO").append(':').append(site).append(',');
		sb.append(sfc);
		return sb.toString();
	}

}
