package com.vendorID.printing;

import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import com.sap.me.document.FormatPrintingDataRequest;
import com.sap.me.document.FormatPrintingDataResponse;
import com.sap.me.document.PrintingFormatServiceInterface;
import com.sap.me.frame.domain.BusinessException;

public class CustomPrintingFormat implements PrintingFormatServiceInterface {

	@Override
	public FormatPrintingDataResponse formatPrintingData(FormatPrintingDataRequest request) throws BusinessException {
		String printContent = request.getPrintContent();
		HashMap<String, Object> printContentData = parsePrintContent(printContent, ";");
		String formattedPrintContent = buildPrintContent(printContentData);
		FormatPrintingDataResponse reponse = new FormatPrintingDataResponse();
		reponse.setPrintContent(formattedPrintContent);
		return reponse;
	}

	public HashMap<String, Object> parsePrintContent(String printContent, String delimiter) {
		HashMap<String, Object> printContentData = new HashMap<String, Object>();
		if (printContent == null) {
			return printContentData;
		}
		StringTokenizer tknz = new StringTokenizer(printContent, delimiter);
		while (tknz.hasMoreTokens()) {
			String pair = tknz.nextToken();
			// suppose we get "name=value" pair
			int eqSignPos = pair.indexOf("=");
			if (eqSignPos > 0) {
				String name = pair.substring(0, eqSignPos).trim();
				String value = pair.substring(eqSignPos + 1).trim();
				printContentData.put(name, value);
			}
		}
		return printContentData;
	}

	private String buildPrintContent(HashMap<String, Object> printContentData) {
		String FIELD = "^field";
		String LINE_BREAK_DELIMITER = "\n";
		StringBuffer out = new StringBuffer();
		if (printContentData != null) {
			Object fieldValue = null;
			Iterator<String> keyIter = printContentData.keySet().iterator();
			while (keyIter.hasNext()) {
				String key = keyIter.next();
				fieldValue = printContentData.get(key);
				if (fieldValue != null) {
					out.append(FIELD + " " + key + LINE_BREAK_DELIMITER);
					out.append(fieldValue + LINE_BREAK_DELIMITER);
				}
			}

		}
		return out.toString();
	}

}
