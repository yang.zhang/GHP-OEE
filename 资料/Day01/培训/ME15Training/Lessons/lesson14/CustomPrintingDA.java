package com.vendorID.printing;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.sap.me.common.ObjectReference;
import com.sap.me.document.DocumentConfiguration;
import com.sap.me.document.DocumentConfigurationServiceInterface;
import com.sap.me.document.DocumentOptionValue;
import com.sap.me.document.PrintingDataAcquisitionRequest;
import com.sap.me.document.PrintingDataAcquisitionResponse;
import com.sap.me.document.PrintingDataAcquisitionServiceInterface;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.production.FindCollectedSfcDataFieldsByContextRequest;
import com.sap.me.production.FindCollectedSfcDataFieldsByContextResponse;
import com.sap.me.production.SfcConfiguration;
import com.sap.me.production.SfcDataField;
import com.sap.me.production.SfcDataServiceInterface;
import com.sap.me.production.SfcStateServiceInterface;

public class CustomPrintingDA implements PrintingDataAcquisitionServiceInterface {
	protected static final String SFC = "DocumentOptionBO:SFC_HEADER_DATA";
	protected static final String SFC_DATA = "DocumentOptionBO:SFC_CUSTOM_DATA";
	protected static final String SFC_PACK_DATA = "DocumentOptionBO:SFC_PACK_DATA";
	protected static final String SHOP_ORDER = "DocumentOptionBO:SHOP_ORDER_HEADER_DATA";
	protected static final String ITEM = "DocumentOptionBO:ITEM_CUSTOM_DATA";
	protected static final String OPERATION = "DocumentOptionBO:OPERATION_CUSTOM_DATA";
	protected static final String ROUTER = "DocumentOptionBO:ROUTER_DATA";
	protected static final String BOM = "DocumentOptionBO:BOM_HEADER_DATA";
	protected static final String BOM_COMPONENT = "DocumentOptionBO:BOM_COMPONENT_DATA";
	protected static final String CONTAINER = "DocumentOptionBO:CONTAINER_HEADER_DATA";
	protected static final String PARAMETRIC_DATA = "DocumentOptionBO:PARAMETRIC_DATA";
	protected static final String CONTAINER_ASSEMBLY_METRICS = "DocumentOptionBO:CONTAINER_ASSEMBLY_METRICS";
	protected static final String NC_CODE = "DocumentOptionBO:NC_CODE_CUSTOM_DATA";
	protected static final String NC_DATA = "DocumentOptionBO:NC_DATA";
	protected static final String WORK_INSTRUCTION = "DocumentOptionBO:WORK_INSTRUCTION_DATA";
	protected static final String BOM_ASSEMBLY_METRICS = "DocumentOptionBO:BOM_ASSEMBLY_METRICS";

	@Override
	public PrintingDataAcquisitionResponse acquirePrintingData(PrintingDataAcquisitionRequest request) throws BusinessException {
		// get document configuration from Document Maintenance for current document
		DocumentConfiguration documentConfiguration = getDocumentConfiguration(request.getDocumentRef());
		StringBuffer responseString = new StringBuffer();
		SfcDataServiceInterface sfcDataService = Services.getService("com.sap.me.production", "SfcDataService");
		SfcStateServiceInterface sfcService = Services.getService("com.sap.me.production", "SfcStateService");
		SfcConfiguration sfcConfiguration = sfcService.readSfc(request.getPrintByGbo());

		if (isOptionEnabled(documentConfiguration, SFC)) {
			responseString.append("sfc_header_").append("number").append("=").append(sfcConfiguration.getSfc()).append(";");
			responseString.append("sfc_header_").append("quantity").append("=").append(sfcConfiguration.getQty()).append(";");
			responseString.append("sfc_header_").append("priority").append("=").append(sfcConfiguration.getPriority()).append(";");
		}
		if (isOptionEnabled(documentConfiguration, SFC_DATA)) {
			FindCollectedSfcDataFieldsByContextRequest sfcDataRequest = new FindCollectedSfcDataFieldsByContextRequest();
			sfcDataRequest.setSfcRef(request.getPrintByGbo());
			FindCollectedSfcDataFieldsByContextResponse response = sfcDataService.findCollectedSfcDataFieldsByContext(sfcDataRequest);
			List<SfcDataField> sfcDataList = response.getSfcDataFieldList();
			int z = 0;
			for (SfcDataField attrValue : sfcDataList) {
				responseString.append("sfc_custom_data_").append("attribute_").append(z).append("=").append(attrValue.getAttribute()).append(";");
				responseString.append("sfc_custom_data_").append("value_").append(z).append("=").append(attrValue.getValue()).append(";");
				z++;
			}

		}
		if (isOptionEnabled(documentConfiguration, SHOP_ORDER)) {
			String shopOrder = getShopOrder(sfcConfiguration.getShopOrderRef());
			responseString.append("shoporder_data_").append("number").append("=").append(shopOrder).append(";");

		}

		PrintingDataAcquisitionResponse response = new PrintingDataAcquisitionResponse();
		response.setDocumentConfiguration(documentConfiguration);
		response.setPrintContent(responseString.toString());
		return response;
	}

	protected DocumentConfiguration getDocumentConfiguration(String documentRef) throws BusinessException {
		DocumentConfigurationServiceInterface documentConfigurationService = Services.getService("com.sap.me.document", "DocumentConfigurationService");
		return documentConfigurationService.readDocument(new ObjectReference(documentRef));
	}

	private boolean isOptionEnabled(DocumentConfiguration documentConfiguration, String optionName) throws BusinessException {
		List<DocumentOptionValue> options = documentConfiguration.getDocumentOptionValueList();
		for (DocumentOptionValue option : options) {
			if (optionName != null && optionName.equals(option.getDocumentOptionRef()))
				return option.getValue();
		}
		return false;
	}
	
	private String getShopOrder(String input) {
		ArrayList<String> list = new ArrayList<String>();
		StringTokenizer t = new StringTokenizer(input, ":,");
		// Get all of the pieces
		while (t.hasMoreTokens()) {
			// Add them to a temp list
			list.add(t.nextToken());
		}
		return (String) list.get(2);
	}

}
