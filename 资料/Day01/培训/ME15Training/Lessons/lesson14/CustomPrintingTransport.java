package com.vendorID.printing;

import java.io.FileWriter;

import com.sap.me.appconfig.FindSystemRuleSettingRequest;
import com.sap.me.appconfig.SystemRuleServiceInterface;
import com.sap.me.appconfig.SystemRuleSetting;
import com.sap.me.document.DocumentPrintingFailedException;
import com.sap.me.document.PrintingTransportServiceInterface;
import com.sap.me.document.TransportPrintingDataRequest;
import com.sap.me.document.TransportPrintingDataResponse;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;

public class CustomPrintingTransport implements PrintingTransportServiceInterface {

	@Override
	public TransportPrintingDataResponse transportPrintingData(TransportPrintingDataRequest request) throws BusinessException {
		TransportPrintingDataResponse response = new TransportPrintingDataResponse();

		SystemRuleServiceInterface systemRuleService = Services.getService("com.sap.me.appconfig", "SystemRuleService");

			FindSystemRuleSettingRequest systemRuleRequest = new FindSystemRuleSettingRequest();
			systemRuleRequest.setRuleName("DOC_DIR");
			SystemRuleSetting sysSetting = systemRuleService.findSystemRuleSetting(systemRuleRequest);
			
			String strFormatted = request.getPrintContent();
			String filename = sysSetting.getSetting() + "\\" + "SFC_PRINT_DATA_" + System.currentTimeMillis() + ".dat";
			try {
				writeFile(filename, strFormatted);
			} catch (Exception e) {
				throw new DocumentPrintingFailedException(e.getLocalizedMessage());
			}
		

		return response;

	}
	/**
	 * Writes a file to the file system.
	 * 
	 * @param fileName The name of the rule to read.
	 * @param dataThe content of the file as byte array.           
	 */
	private void writeFile(String fileName, String strFormatted) throws java.io.IOException {
		FileWriter writer = new FileWriter(fileName);
		writer.write(strFormatted);
		writer.close();

	}

}
