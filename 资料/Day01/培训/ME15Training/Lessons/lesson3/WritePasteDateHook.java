package com.vendorID.hook;

import java.util.ArrayList;
import java.util.List;

import com.sap.me.activity.ActivityOption;
import com.sap.me.activity.HookContextInterface;
import com.sap.me.activity.HookContextSetter;
import com.sap.me.extension.Services;
import com.sap.me.production.CollectSfcDataRequest;
import com.sap.me.production.CompleteHookDTO;
import com.sap.me.production.SfcDataField;
import com.sap.me.production.SfcDataServiceInterface;
import com.visiprise.frame.service.ext.ActivityInterface;
import com.visiprise.globalization.DateGlobalizationServiceInterface;
import com.visiprise.globalization.GlobalizationService;
import com.visiprise.globalization.KnownGlobalizationServices;

/**
 * @author Oksana Zubchenko
 *
 */
public class WritePasteDateHook implements ActivityInterface<CompleteHookDTO>, HookContextSetter {

	private static final long serialVersionUID = 1L;
	private static final String SFC_DATA_SERVICE = "SfcDataService";
	private static final String COM_SAP_ME_PRODUCTION = "com.sap.me.production";
	private SfcDataServiceInterface sfcDataService;
	private HookContextInterface hookContext;
	private String expTime;
	private static final String EXP_TIME = "EXP_TIME";

	// Copy execute() method
	public void execute(CompleteHookDTO dto) throws Exception {
		initServices();
		DateGlobalizationServiceInterface dateSrv = 
			(DateGlobalizationServiceInterface) GlobalizationService.getInvariantService(KnownGlobalizationServices.DATE);

		CollectSfcDataRequest collectSfcData = new CollectSfcDataRequest();
		List<String> sfcList = new ArrayList<String> () ;
		sfcList.add(dto.getSfcBO().getValue());
		collectSfcData.setSfcList(sfcList);
		List<SfcDataField> sfcDataFieldList = new ArrayList<SfcDataField> ();
		SfcDataField sfcPasteDataField = new SfcDataField();
		sfcPasteDataField.setAttribute("PASTE_DATE");
		// expiration time = current date in millisecond + 5 hours in milliseconds 
		long expTimeLong = dateSrv.createDateTime().getTimeInMillis() + (Long.parseLong(expTime) * 3600 * 1000) ;
		sfcPasteDataField.setValue(String.valueOf(expTimeLong));		
		sfcDataFieldList.add(sfcPasteDataField);
		
		SfcDataField sfcCommentsDataField = new SfcDataField();
		sfcCommentsDataField.setAttribute("COMMENTS");
		sfcCommentsDataField.setValue("Write Paste Date Activity Hook");
		sfcDataFieldList.add(sfcCommentsDataField);

		collectSfcData.setSfcDataFieldList(sfcDataFieldList);
        //The method logs given data against given SFCs
		sfcDataService.collectSfcData(collectSfcData);
		
	}

	/**
	 *	Initialization of services that are represented as fields.
	 */
	private void initServices() {
		sfcDataService = Services.getService(COM_SAP_ME_PRODUCTION,SFC_DATA_SERVICE);

	}

	
	public void setHookContext(final HookContextInterface hookContext) {
		this.hookContext = hookContext;
		List<ActivityOption> activityOptions = hookContext.getActivityOptions();
		for (ActivityOption activityOption : activityOptions) {
			String optionName = activityOption.getExecUnitOption();
			String optionValue = activityOption.getSetting();
			if (optionName == null) {
				continue;
			}
			if (optionName.equals(EXP_TIME)) {
				expTime = optionValue;
			}
		}
	}

}
