package com.vendorID.printing;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sap.me.activity.ActivityRuntimeType;
import com.sap.me.activity.ExecutionType;
import com.sap.me.activity.ServiceExtension;
import com.sap.me.document.PrintingDataAcquisitionRequest;
import com.sap.me.document.PrintingDataAcquisitionResponse;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.production.FindSfcDataBySfcRequest;
import com.sap.me.production.FindSfcDataBySfcResponse;
import com.sap.me.production.SfcDataServiceInterface;
//Printing extensions cannot be asynchronous since their result may be
//consumed by the next print service in the printing work flow.
@ExecutionType(ActivityRuntimeType.SYNC)
public class SfcDataAcquisitionServiceExtension extends ServiceExtension<PrintingDataAcquisitionRequest> {
	// Sfc data service injected via service config property.
	 SfcDataServiceInterface sfcDataService = null;

	/**
	 * ActivityInterface implementation required for the extension to be
	 * executed.
	 */
	public void execute(PrintingDataAcquisitionRequest printingReq) throws Exception {
		// The result DTO returned by the printing data acquisition service
		// contains the XML data content that we will add SFC data to.
		PrintingDataAcquisitionResponse printingResp = this.invocationContext.getResult();
		// Convert the XML data content to an XML document so that we can work
		// with it easier.
		Document doc = parseDefaultDataAcquisition(printingResp);
		// Use the injected SfcDataService to read the data for the SFC.
		FindSfcDataBySfcResponse findSfcDataBySfc = getSfcData(doc);
		// Add the SFC data to the document and convert back to an XML string
		// so that it is ready for the transport step.
		String xml = addSfcData(findSfcDataBySfc, doc);
		printingResp.setPrintContent(xml);
	}

	/**
	 * Add the SFC data to the document.
	 * 
	 * @param findSfcDataBySfc
	 * @param doc
	 * @return XML with SFC data added
	 * @throws JAXBException
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 */
	private String addSfcData(FindSfcDataBySfcResponse findSfcDataBySfc,
			Document doc) throws JAXBException,
			TransformerFactoryConfigurationError, TransformerException {
		Object obj = findSfcDataBySfc;
		if (findSfcDataBySfc.getClass().getAnnotation(XmlRootElement.class) == null) {
			String className = findSfcDataBySfc.getClass().getSimpleName();
			className = toLowerFirst(className);
			QName rootElement = new QName("",className);
			obj = new JAXBElement(rootElement, findSfcDataBySfc.getClass(),findSfcDataBySfc);
		}
		JAXBContext context = JAXBContext.newInstance("com.sap.me.common:com.sap.me.production", this.getClass().getClassLoader());
		Marshaller marshaller = context.createMarshaller();
		Node sfcDataNode = doc.createDocumentFragment();
		marshaller.marshal(obj, sfcDataNode);
		getSfcPrintingDataElt(doc).appendChild(sfcDataNode);
		removeNamespaces(doc);
		doc.normalizeDocument();
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		StreamResult result = new StreamResult(new StringWriter());
		DOMSource source = new DOMSource(doc);
		transformer.transform(source, result);
		return result.getWriter().toString();
	}
	
	protected void removeNamespaces(Node node) {
		if (node.getPrefix() != null)
			node.setPrefix("");

		if (node.getNamespaceURI() != null)
			node.getOwnerDocument().renameNode(node, "", node.getNodeName());

		removeNamespaceAttributes(node);
		
		NodeList list = node.getChildNodes();
		for (int i = 0; list != null && i < list.getLength(); ++i) {
			removeNamespaces(list.item(i));
		}
	}

	protected void removeNamespaceAttributes(Node node) {
		NamedNodeMap attributes = node.getAttributes();
		if (attributes != null && attributes.getLength() > 0) {
			List<String> attributesToRemove = new ArrayList<String>();
			for (int i = 0; i < attributes.getLength(); i++) {
				Node subNode = attributes.item(i);
				if (subNode.getNodeType() == Node.ATTRIBUTE_NODE) {
					Attr attribute = (Attr) subNode;
					if ((attribute.getPrefix() != null && (attribute.getPrefix().equals("xsi") || attribute.getPrefix().equals("xmlns")))
							|| attribute.getName().equals("xmlns")) {
						attributesToRemove.add(attribute.getName());
					}
				}
			}
			for (String attribute : attributesToRemove)
				node.getAttributes().removeNamedItem(attribute);
		}
	}


	/**
	 * Get the SFC data using the SFC reference from the XML.
	 * 
	 * @param doc
	 * @return FindSfcDataBySfcResponse containing the SFC data
	 * @throws BusinessException
	 */
	private FindSfcDataBySfcResponse getSfcData(Document doc)
			throws BusinessException {
		Element sfcPrintElt = getSfcPrintingDataElt(doc);
		Element sfcRefElt = (Element) sfcPrintElt.getElementsByTagName("sfcRef").item(0);
		FindSfcDataBySfcRequest sfcDataRequest = new FindSfcDataBySfcRequest();
		sfcDataRequest.setSfcRef(sfcRefElt.getFirstChild().getNodeValue());
		return sfcDataService.findSfcDataBySfc(sfcDataRequest);
	}

	/**
	 * Simple helper to retrieve the sfcPrintingData element from the XML
	 * document.
	 * 
	 * @param doc
	 * @return Element The sfcPrintingData element
	 */
	private Element getSfcPrintingDataElt(Document doc) {
		Element sfcPrintElt = (Element) doc.getElementsByTagName("sfcPrintingData").item(0);
		return sfcPrintElt;
	}

	/**
	 * Parse the data acquisition XML into an XML Document.
	 * 
	 * @param printingResp
	 * @return The DOM Document
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private Document parseDefaultDataAcquisition(PrintingDataAcquisitionResponse printingResp) throws ParserConfigurationException, SAXException, IOException {
		String printContent = printingResp.getPrintContent();
		DocumentBuilderFactory docFac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFac.newDocumentBuilder();
		ByteArrayInputStream is = new ByteArrayInputStream(printContent.getBytes());
		return docBuilder.parse(is);
	}

	/**
	 * Setter required to set the SfcDataService configured in the service
	 * configuration.
	 * 
	 * @param sfcDataService
	 */
	public void setSfcDataService(SfcDataServiceInterface sfcDataService) {
		this.sfcDataService = sfcDataService;
	}

	/**
	 * Helper method to create the QName for the JAXB root element required for
	 * mashalling.
	 * 
	 * @param className
	 * @return The name of the class with the first character lower case
	 */
	private String toLowerFirst(String className) {
		return (className.length() > 0) ? Character.toLowerCase(className.charAt(0)) + className.substring(1) : className;
	}
}