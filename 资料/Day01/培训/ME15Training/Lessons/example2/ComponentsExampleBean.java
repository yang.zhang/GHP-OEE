package com.vendorID.wpmf.web.podplugin;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.application.Application;
import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.BooleanConverter;

import com.sap.me.common.AttributeValue;
import com.sap.me.production.podclient.BasePodPlugin;
import com.sap.me.wpmf.BrowseSelectionEventListener;
import com.sap.me.wpmf.InternalTableSelectionEventListener;
import com.sap.me.wpmf.TableConfigurator;
import com.sap.me.wpmf.TableSelectionEvent;
import com.sap.me.wpmf.util.FacesUtility;
import com.sap.ui.faces.component.UISelectItem;
import com.sap.ui.faces.component.html.HtmlOutputLabel;
import com.sap.ui.faces.component.html.HtmlSelectBooleanCheckbox;
import com.sap.ui.faces.component.sap.HtmlCommandOneMenu;
import com.sap.ui.faces.component.sap.HtmlInputDate;
import com.sap.ui.faces.component.sap.UIInputText;
import com.sap.ui.faces.component.sap.UIPanelGrid;
import com.sap.ui.faces.component.sap.UIPanelGroup;
import com.sap.ui.faces.model.DefaultTreeNode;
import com.sap.ui.faces.model.TreeNode;

public class ComponentsExampleBean extends BasePodPlugin implements BrowseSelectionEventListener, InternalTableSelectionEventListener {
	private static final long serialVersionUID = 1L;

	protected Map<String, Object> optionValues = null;
	private UIComponent optionsGridLayout = null;
	private String operation;
	private String resource;
	private String resourceTextLink;
	private TableConfigurator configBean = null;
	private TreeNode treeModelRoot = null;
	private int numberOfNodesCreated = 0; // Counter for Recursion

	private List<CellEditDTO> storage = null;

	public ComponentsExampleBean() {
		super();
		getPluginEventManager().addPluginListeners(this.getClass());

		storage = new ArrayList<CellEditDTO>(30);
		for (int i = 1; i < 32; i++) {
			CellEditDTO dto = new CellEditDTO();
			dto.setSfc("sfc-" + i);
			dto.setQty(new BigDecimal(i));
			dto.setShopOrder("sfc-" + i + "-shoporder");
			dto.setActive(true);
			storage.add(dto);
		}
		getTreeModel();

	}

	/** Returns the map of entered option values. */
	public Map<String, Object> getOptionValues() {
		if (optionValues == null) {
			optionValues = new HashMap<String, Object>();
			for (int i = 0; i < 4; i++) {
				String key = "option" + String.valueOf(i + 1);
				optionValues.put(key, "");
			}
		}
		return optionValues;
	}

	protected void configureOptionComponents(UIPanelGrid gridLayout) {
		if (gridLayout != null) {
			List<UIComponent> children = gridLayout.getChildren();
			if (children != null && children.size() > 0) {
				return;
			}
		}
		for (int i = 0; i < 4; i++) {
			createOptionComponent(gridLayout, i);
		}
	}

	private void createOptionComponent(UIPanelGrid gridLayout, int index) {

		UIPanelGroup nameGroup = new UIPanelGroup();
		nameGroup.setHalign("end");
		nameGroup.setValign("middle");
		nameGroup.setHeight("22");

		HtmlOutputLabel optionLabel = new HtmlOutputLabel();
		optionLabel.setValue("Option #" + String.valueOf(index));
		nameGroup.getChildren().add(optionLabel);

		UIPanelGroup settingGroup = new UIPanelGroup();
		settingGroup.setHalign("start");
		settingGroup.setValign("middle");
		settingGroup.setHeight("22");
		settingGroup.setWidth("200");

		String key = "option" + String.valueOf(index + 1);
		String valueBinding = "#{exampleElementsBean.optionValues[\"" + key + "\"]}";

		UIComponent settingComp = null;
		if (index == 0) {
			UIInputText inputText = new UIInputText();
			ValueExpression expression = createValueExpression(valueBinding, String.class);
			inputText.setValueExpression("value", expression);
			settingComp = inputText;
		} else if (index == 1) {
			HtmlSelectBooleanCheckbox checkbox = new HtmlSelectBooleanCheckbox();
			ValueExpression expression = createValueExpression(valueBinding, Boolean.class);
			checkbox.setValueExpression("value", expression);
			settingComp = checkbox;

		} else if (index == 2) {
			HtmlInputDate inputDate = new HtmlInputDate();
			ValueExpression expression = createValueExpression(valueBinding, Date.class);
			inputDate.setValueExpression("value", expression);
			settingComp = inputDate;

		} else if (index == 3) {
			HtmlCommandOneMenu menu = new HtmlCommandOneMenu();
			ValueExpression expression = createValueExpression(valueBinding, String.class);
			menu.setValueExpression("value", expression);
			for (int i = 0; i < 5; i++) {
				UISelectItem selectItem = new UISelectItem();
				selectItem.setItemValue("value" + String.valueOf(i));
				selectItem.setItemLabel("Menu " + String.valueOf(i + 1));
				menu.getChildren().add(selectItem);
			}
			settingComp = menu;

		}

		if (settingComp != null) {
			settingGroup.getChildren().add(settingComp);
			gridLayout.getChildren().add(nameGroup);
			gridLayout.getChildren().add(settingGroup);
		}

	}

	public ValueExpression createValueExpression(String bindingString, Class returnType) {
		FacesContext fctx = FacesContext.getCurrentInstance();
		ELContext elctx = fctx.getELContext();
		Application thisApp = FacesContext.getCurrentInstance().getApplication();
		ExpressionFactory exprFactory = thisApp.getExpressionFactory();
		ValueExpression expression = exprFactory.createValueExpression(elctx, bindingString, returnType);
		return expression;
	}

	public void closePlugin() {
		closeCurrentPlugin();
		FacesUtility.setSessionMapValue("exampleElementsBean", null);

	}

	// The method signature should be:
	// public List<AttributeValue> anyMethodName( EditableValueHolder comp)
	public List<AttributeValue> getCriteria(EditableValueHolder comp) {
		String primSrchVal = (String) comp.getValue();
		ArrayList<AttributeValue> result = new ArrayList<AttributeValue>();
		AttributeValue attValue = new AttributeValue("operation", primSrchVal);
		result.add(attValue);
		return result;
	}

	public void setConfigBean(TableConfigurator configBean) {
		this.configBean = configBean;
		configBean.setListName(null);
		configBean.setAllowSelections(false);
		String[] cnames = { "SFC", "SHOP_ORDER", "QTY", "ACTIVE" };
		configBean.setListColumnNames(cnames);

		Map<String, String> bindings = new HashMap<String, String>();
		bindings.put("SFC", "sfc");
		bindings.put("SHOP_ORDER", "shopOrder");
		bindings.put("QTY", "qty");
		bindings.put("ACTIVE", "active");
		configBean.setColumnBindings(bindings);

		configBean.setColumnConverter(new BooleanConverter(), "ACTIVE");

		configBean.setAllowSelections(true);
		configBean.setMultiSelectType(true);
		configBean.setSelectionBehavior("server");
		configBean.addInternalTableSelectionEventListener(this);

		configBean.configureTable();
	}

	public TreeNode getTreeModel() {
		if (treeModelRoot != null) {
			return treeModelRoot;
		}
		if (storage.isEmpty()) {
			treeModelRoot = null;
			return treeModelRoot;
		}
		treeModelRoot = new DefaultTreeNode(storage.get(0), null);
		numberOfNodesCreated = 1; // the root - currentLevel == 0
		createTreeRecursive(treeModelRoot, storage.size(), 2, 1, 4); // currentLeve
		return treeModelRoot;
	}

	public void processTableSelectionEvent(TableSelectionEvent event) {
		
	}

	private void createTreeRecursive(final TreeNode node, final int numberOfNodes, final int numberOfChildren, final int currentLevel, final int maxLevel) {

		// root.children --> currentLevel == 1,...
		if (currentLevel >= maxLevel) {
			return;
		}

		for (int i = 0; i < numberOfChildren; i++) {
			if (numberOfNodesCreated >= numberOfNodes) {
				break; // level can be not full
			}
			TreeNode child = new DefaultTreeNode(storage.get(numberOfNodesCreated++), node);
			createTreeRecursive(child, numberOfNodes, numberOfChildren, currentLevel + 1, maxLevel);
		}
	}

	public UIComponent getOptionsGridLayout() {
		return optionsGridLayout;
	}

	public void setOptionsGridLayout(UIComponent gridLayout) {
		configureOptionComponents((UIPanelGrid) gridLayout);
		this.optionsGridLayout = gridLayout;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getResourceTextLink() {
		if (resourceTextLink == null)
			return "Select Resource >";
		else
			return this.resourceTextLink;
	}

	public void setResourceTextLink(String resourceTextLink) {
		this.resourceTextLink = resourceTextLink;
	}

	public void processLinkBrowseCallBack() {
		Object val = this.getLastBrowseFieldValue();
		this.resourceTextLink = (String) val;
	}

}
