<%@ page pageEncoding="UTF-8"%>
<%@ page language="java"%>
<%@ taglib prefix="h" uri="http://java.sap.com/jsf/html"%>
<%@ taglib prefix="f" uri="http://java.sap.com/jsf/core"%>
<%@ taglib prefix="sap" uri="http://java.sap.com/jsf/html/extended"%>
<%@ taglib prefix="fh" uri="http://java.sun.com/jsf/html"%>

<f:subview id="testrender">
	<sap:panelTabbed id="panelTabbed" width="100%" height="100%" isCollapsible="false">
		<sap:panel id="mainPanel" isCollapsible="false" isCollapsed="false" width="100%" height="100%" borderDesign="none" contentAreaDesign="transparent" binding="#{exampleElementsBean.container}">
			<f:facet name="header">
				<h:outputText value="Main"/>
			</f:facet>
			<sap:ajaxUpdate render="#{sap:toClientId('mainPanel')}"/>
			
			<sap:panelGrid height="100%" width="100%">
				<sap:panelRow>
					<sap:panelGroup backgroundDesign="transparent" width="100%" height="100%" padding="0px">
						<sap:panelGrid  width="96%" cellpadding="4px" cellHalign="start" cellValign="top">
							<sap:panelGrid height="100%" width="100%" cellHalign="start" cellValign="top" binding="#{exampleElementsBean.optionsGridLayout}"/>
						</sap:panelGrid>
						<sap:panelGrid cellHalign="start" cellValign="top">
							<sap:outputLabel value="Custom Operation Browse"/>
							<sap:commandInputText id="operation" width="16em" submitOnChange="true" submitOnFieldHelp="true" value="#{exampleElementsBean.operation}" title="Custom Simple Operation Browse">
								<f:attribute name="upperCase" value="true"/>
								<f:actionListener type="com.sap.me.wpmf.browse.BrowseListener"/>
								<f:attribute name="REQUESTING_PLUGIN_NAME" value="exampleElementsBean"/>
								<f:attribute name="SEARCH_METHOD_NAME" value="exampleElementsBean.getCriteria"/>
								<f:attribute name="SERVICE_MODULE" value="com.vendorID.service"/>
								<f:attribute name="SERVICE_NAME" value="SimpleOperationBrowseService"/>
								<f:attribute name="BROWSE_PLUGIN_NAME" value="defaultCoreBrowsePlugin"/>
								<f:attribute name="IS_MULTI_SELECT" value="false"/>
								<f:attribute name="BROWSE_DIALOG_WIDTH" value="20em"/>
								<f:attribute name="BROWSE_DIALOG_HEIGHT" value="25em"/>
								<sap:ajaxUpdate/>
							</sap:commandInputText>
						</sap:panelGrid>
						<sap:panelGrid cellHalign="start" cellValign="top">
							<sap:outputLabel  value="Legacy Resource Browse"/>
							<sap:commandInputText id="resourcebrowse" submitOnChange="true" submitOnFieldHelp="true" value="#{exampleElementsBean.resource}" actionListener="#{exampleElementsBean.browseActionListener}">
								<f:attribute name="upperCase" value="true"/>
								<f:attribute name="browseable" value="true"/>
								<f:attribute name="legacyBrowse" value="true"/>
								<f:attribute name="browseId" value="RESOURCE"/>
								<f:attribute name="browseCustom" value="standard"/>
								<f:attribute name="browseSelectionModel" value="single"/>
								<f:attribute name="browseDialogWidth" value="20em"/>
								<f:attribute name="browseDialogHeight" value="25em"/>
								<sap:ajaxUpdate/>
							</sap:commandInputText>
						</sap:panelGrid>
						<sap:panelGrid cellHalign="start" cellValign="top">
							<sap:outputLabel  value="Select Resource" id="resourceTextLinkLabel" for="resourceTextLink"/>
							<h:commandLink id="resourceTextLink" value="#{exampleElementsBean.resourceTextLink}" actionListener="#{exampleElementsBean.browseActionListener}">
								<f:attribute name="upperCase" value="true"/>
								<f:attribute name="browseable" value="true"/>
								<f:attribute name="legacyBrowse" value="true"/>
								<f:attribute name="browseId" value="RESOURCE"/>
								<f:attribute name="browseCustom" value="standard"/>
								<f:attribute name="browseSelectionModel" value="single"/>
								<f:attribute name="browseDialogWidth" value="20em"/>
								<f:attribute name="browseDialogHeight" value="25em"/>
								<f:attribute name="browseCallBack" value="exampleElementsBean.processLinkBrowseCallBack"/>
								<sap:ajaxUpdate/>
							</h:commandLink>
						</sap:panelGrid>
					</sap:panelGroup>
				</sap:panelRow>
				<sap:panelRow>
					<sap:panelGroup height="100%" valign="bottom" halign="center">
						<sap:panelGrid cellspacing="5">
							<sap:panelRow>
								<sap:panelGroup>
									<sap:commandButton value="Close" action="#{exampleElementsBean.closePlugin}"/>
								</sap:panelGroup>
							</sap:panelRow>
						</sap:panelGrid>
					</sap:panelGroup>
				</sap:panelRow>
			</sap:panelGrid>
		</sap:panel>
		<sap:panel id="optionsPanelTab" width="100%" height="100%" isCollapsible="false" contentAreaDesign="transparent">
			<f:facet name="header">
				<h:outputText value="Splitter"/>
			</f:facet>
			<sap:ajaxUpdate render="#{sap:toClientId('optionsPanelTab')}"/>
			<sap:panelSplitter id="splitterContainer" orientation="vertical" splitterPosition="100%" collapseDirection="none" isCollapsed="false" splitterType="invisible">
				<f:facet name="first">
					<sap:panelSplitter orientation="vertical" splitterPosition="50%">
						<f:facet name="first">
							<sap:panel isCollapsible="false">
								<sap:treeDataTable binding="#{treeTableBeanConfigurator.table}" value="#{exampleElementsBean.treeModel}" var="row" varTreeNode="currentNode" first="0" rows="5" id="tree_table" width="100%"/>
							</sap:panel>
						</f:facet>
						<f:facet name="second">
							<sap:panelSplitter orientation="horizontal" splitterPosition="50%">
								<f:facet name="first">
									<h:selectOneRadio id="radio" layout="pageDirection" required="false">
										<f:selectItem itemLabel="Item 1" itemValue="Item1"/>
										<f:selectItem itemLabel="Item 2" itemValue="Item2"/>
										<f:selectItem itemLabel="Item 3" itemValue="Item3"/>
										<f:selectItem itemLabel="Item 4" itemValue="Item4"/>
									</h:selectOneRadio>
								</f:facet>
								<f:facet name="second">	
								<fh:commandButton id="redButton" value="Red Button" style="background-color: #FF0000"/> 
								</f:facet>
							</sap:panelSplitter>
						</f:facet>
					</sap:panelSplitter>
				</f:facet>
			</sap:panelSplitter>
		</sap:panel>
	</sap:panelTabbed>
</f:subview>



