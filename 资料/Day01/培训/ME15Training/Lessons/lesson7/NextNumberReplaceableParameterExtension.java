package com.vendorID.serviceext;

import java.util.List;

import com.sap.me.activity.ServiceExtension;
import com.sap.me.common.AttributeValue;
import com.sap.me.common.NameValueObject;
import com.sap.me.common.ObjectReference;
import com.sap.me.demand.ShopOrderFullConfiguration;
import com.sap.me.demand.ShopOrderServiceInterface;
import com.sap.me.extension.Services;
import com.sap.me.numbering.GenerateNextNumberRequest;

import com.visiprise.frame.service.ext.InvocationContext;
import com.visiprise.frame.service.ext.InvocationContextSetter;
import com.visiprise.frame.service.ext.TransactionContextInterface;
import com.visiprise.frame.service.ext.TransactionContextSetter;
	
//public GenerateNextNumberResponse generateNextNumber(GenerateNextNumberRequest request) throws NextNumberNotLoadedException, 
//NextNumberSequenceExceededException, NextNumberConcurrencyException, NextNumberInvalidFormatException, 
//NextNumberLengthException, BusinessException;

public class NextNumberReplaceableParameterExtension  extends ServiceExtension<Object> implements InvocationContextSetter, TransactionContextSetter {

	private static final long serialVersionUID = 1L;
	private InvocationContext invocationContext;
	private TransactionContextInterface transactionContext;
	private static final String COM_SAP_ME_DEMAND = "com.sap.me.demand";
	private static final String SHOP_ORDER_SERVICE = "ShopOrderService";
	private ShopOrderServiceInterface shopOrderService;


	@Override
	public void execute(Object request) throws Exception {
		if (request instanceof GenerateNextNumberRequest) {
			execute((GenerateNextNumberRequest) request);
		}		
	}	
	
	public void execute(GenerateNextNumberRequest request) throws Exception {
		initServices();
		// SFC Release type in Next Number Maintenance
		if ("SFCRELEASE".equals(request.getNextNumberType().name())) {
			String shopOrderRef = request.getShopOrderRef();
			ShopOrderFullConfiguration soData = shopOrderService.readShopOrder(new ObjectReference(shopOrderRef));
			// Gets the value of the customFields property.
			// This accessor method returns a reference to the live list, not a snapshot.
			// Therefore any modification you make to the returned list affect the object.
			// List of Name/Value pairs to be used for string replacement during generation where value can be an object.
			List<NameValueObject> customFields = request.getCustomFields();
			for (AttributeValue attrValue : soData.getCustomData()) {
				if ("COLOR".equals(attrValue.getAttribute())) {
					customFields.add(new NameValueObject("COLOR", attrValue.getValue()));
					continue;
				}
				if ("SALES_ORDER_NUMBER".equals(attrValue.getAttribute())) {
					customFields.add(new NameValueObject("SALES_ORDER_NUMBER", attrValue.getValue()));
					continue;
				}
			}
		}
	}

	/**
	 * Initialization of services that are represented as fields.
	 */
	private void initServices() {
		shopOrderService = Services.getService(COM_SAP_ME_DEMAND, SHOP_ORDER_SERVICE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.visiprise.frame.service.ext.InvocationContextSetter#setInvocationContext(com.visiprise.frame.service.ext.InvocationContext)
	 */
	public void setInvocationContext(InvocationContext context) {
		invocationContext = context;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.visiprise.frame.service.ext.TransactionContextSetter#setTransactionContext(com.visiprise.frame.service.ext.TransactionContextInterface)
	 */
	public void setTransactionContext(TransactionContextInterface context) {
		transactionContext = context;

	}


}
