<!-- "$Revision$" -->
<%@ taglib uri="jstl-c" prefix="c"%>
<%@ taglib uri="core" prefix="app"%>

<app:html>
	<app:head>
		<title>
			<app:text name="application.name.TEXT"/>: <app:text name="SY030.title.TEXT"/> - <app:text name="company.name.TEXT"/>
		</title>
		
		<script language="JavaScript">
			function doOnLoad(){
				var focusField = PageHelper.getTextField("ACTIVITY");
                focusField.setFocus();
			}
		
		    function doClear(){
		    	PageHelper.doClear("MAIN", "ActivityClear" , "<app:text name="saveRecord.default.MESSAGE"/>");
		    }
	    
			function doRetrieve(){
				PageHelper.doRetrieve("MAIN", "ActivityRetrieve", "<app:text name="saveRecord.default.MESSAGE"/>" );
	        }
	        
	        function doSave(){
	        	PageHelper.doSave("MAIN", "ActivitySave", "<app:text name="saveRecord.default.MESSAGE"/>");
	        }

	        function doDelete(){
	            PageHelper.doCommand("MAIN", "ActivityDelete");	
            }

            function doPrint(){
            	PageHelper.doCommand("MAIN", "ActivityPrint");
            }
            
            function insertNew(){
                var table = PageHelper.getTable("RULES_TABLE");
                var row = table.createRow();
                table.insertRow(row);
            }

            function removeSelected(){
                var table = PageHelper.getTable("RULES_TABLE");
                table.deleteSelectedRow();
            }
		</script>
	</app:head>

	<app:body onload="doOnLoad();">
		<!-- Main application form. -->
        <app:form id="MAIN" action="/application/setup/CustomActivityMaintenance">
        	<app:pageHeader title="SY030.title.TEXT" />
        	
			<app:toolBar>
				<app:toolBarRetrieve/>
	       	 	<app:toolBarSave/>
	       	 	<app:toolBarClear/>
			</app:toolBar>
			
			<!-- Display any error messages -->
          	<app:statusMessages/>
          	
          	<table width="99%" border="0" cellpadding="4" cellspacing="0">
          		<tr>
	            	<!-- Activity -->
					<td class="text_label" width="25%">
	                    <app:label id="ACTIVITY_LABEL" indicatorType="required" name="ACTIVITY.default.LABEL"/>
	                </td>
	                <td class="text">
	                    <app:textField id="ACTIVITY" name="ACTIVITY" browseId="ActivityPOD" browseCustom="true" 
	                    	uppercase="true" guiPropsKey="activity" />
	                </td>
				</tr>
				<tr>
	            	<!-- Description -->
					<td class="text_label" width="25%">
	                    <app:label id="DESCRIPTION_LABEL" name="description.default.LABEL"/>
	                </td>
	                <td class="text">
	                    <app:textField id="DESCRIPTION" name="DESCRIPTION"/>
	                </td>
				</tr>
				<tr>
	                <!-- Comments -->
					<td class="text_label" width="25%">
	                    <app:label id="COMMENTS_LABEL" name="CustomActivity.comments.LABEL"/>
	                </td>
	                <td class="text">
	                    <app:textField id="COMMENTS" name="COMMENTS" />
	                </td>
				</tr>
				<tr>
	                <!-- Printer -->
					<td class="text_label" width="25%">
	                    <app:label id="PRINTER_LABEL" name="PRINTER.default.LABEL"/>
	                </td>
	                <td class="text">
	                	<app:textField id="PRINTER" name="PRINTER" browseId="Printer" uppercase="true" guiPropsKey="printer"/>
	                </td>
				</tr>
             </table>

			 <!-- Button -->
			 <table width="100%" border="0" cellpadding="2" cellspacing="2">
				<tr>
					<td align="middle">
						<!-- Delete -->
	                    <app:button id="DeleteButton" onclick="doDelete()" title="CustomActivity.delete.TEXT">
	                    	<app:text name="delete.default.BUTTON"/>
	                    </app:button>
	                    &nbsp;&nbsp;&nbsp;&nbsp;
	                    <!-- Print -->
                        <app:button id="PrintButton" onclick="doPrint()" title="docReprint.TOOLTIP.print">
                        	<app:text name="SY510.print.LABEL"/>
                        </app:button>
	                </td>
				</tr>
			 </table>	
			             
             <app:table id="RULES_TABLE" selectionModel="single">
             	<app:tableMenu menuItems="_INSERT_NEW,_REMOVE_SELECTED"/>
             	<app:tableRow>
             		<!-- Rule Name -->
             		<app:tableCell headerKey="rule.systemRules.LABEL" width="50%">
             			<app:textField id="ACTIVITY_RULE" name="ACTIVITY_RULE" uppercase="true" guiPropsKey="action.default" />
					</app:tableCell>
					<!-- Rule Value -->
             		<app:tableCell headerKey="activityMtnc.setting.LABEL" width="50%">
             			<app:textField id="ACTIVITY_SETTTING" name="ACTIVITY_SETTTING" uppercase="true" guiPropsKey="activityMtnc.setting" />
					</app:tableCell>
				</app:tableRow>
			</app:table>        	
        </app:form>
	</app:body>
</app:html>