package com.hand.printing;

import java.io.Serializable;

/**
 * @author Jun.Liu
 * @Description: 表格列内容
 * @date Dec 9, 2016 1:27:52 PM
 */
public class CustomActivityRuleItemData implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String option;
    private String setting;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getSetting() {
        return setting;
    }

    public void setSetting(String setting) {
        this.setting = setting;
    }
}
