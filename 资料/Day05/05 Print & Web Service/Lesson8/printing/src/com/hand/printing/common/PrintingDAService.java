package com.hand.printing.common;

import com.sap.me.common.ObjectReference;
import com.sap.me.document.DocumentConfiguration;
import com.sap.me.document.DocumentConfigurationServiceInterface;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.frame.service.CommonMethods;
import com.visiprise.frame.mapping.bean.BeanMappingUtils;
import com.visiprise.globalization.DateGlobalizationServiceInterface;
import com.visiprise.globalization.GlobalizationService;
import com.visiprise.globalization.KnownGlobalizationServices;

/**
 * 拷贝源程序
 * 
 * @author Jun.Liu
 * @Description: TODO
 * @date Jan 8, 2016 3:00:56 PM
 */
public class PrintingDAService {
    protected static final String CONTAINER = "DocumentOptionBO:CONTAINER_HEADER_DATA";
    protected static final String CONTAINER_ASSEMBLY_METRICS = "DocumentOptionBO:CONTAINER_ASSEMBLY_METRICS";

    public DocumentConfiguration getDocumentConfiguration(String documentRef) throws BusinessException {
        DocumentConfigurationServiceInterface documentConfigurationService = Services.getService("com.sap.me.document", "DocumentConfigurationService");
        return documentConfigurationService.readDocument(new ObjectReference(documentRef));
    }

    public DocumentPrintingData acquireDocumentPrintingData(DocumentConfiguration documentConfiguration) {
        DateGlobalizationServiceInterface printDateService = GlobalizationService.getUserService(KnownGlobalizationServices.DATE);

        DocumentPrintingData response = BeanMappingUtils.map(documentConfiguration, DocumentPrintingData.class);
        response.setSite(CommonMethods.getSite());
        response.setPrintDate(printDateService.createDateTime());
        response.setPrintUser(CommonMethods.getUserId());
        return response;
    }
}
