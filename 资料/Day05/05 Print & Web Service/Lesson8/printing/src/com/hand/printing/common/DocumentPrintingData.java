package com.hand.printing.common;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.sap.me.document.DocumentConfiguration;
import com.sap.me.productdefinition.Adapter1;
import com.visiprise.globalization.util.DateTimeInterface;

/**
 * 拷贝源程序
 * 
 * @author Jun.Liu
 * @Description: TODO
 * @date Jan 8, 2016 3:00:56 PM
 */
public class DocumentPrintingData extends DocumentConfiguration {
    private static final long serialVersionUID = 1L;

    private String site;
    @XmlElement(namespace = "", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1.class)
    @XmlSchemaType(name = "dateTime")
    private DateTimeInterface printDate;
    private String printUser;

    public DocumentPrintingData() {
        super();
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public DateTimeInterface getPrintDate() {
        return printDate;
    }

    public void setPrintDate(DateTimeInterface printDate) {
        this.printDate = printDate;
    }

    public String getPrintUser() {
        return printUser;
    }

    public void setPrintUser(String printUser) {
        this.printUser = printUser;
    }
}
