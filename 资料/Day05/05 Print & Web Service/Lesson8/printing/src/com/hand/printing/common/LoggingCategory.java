package com.hand.printing.common;

import com.sap.tc.logging.Category;

/**
 * 拷贝源程序
 * 
 * @author Jun.Liu
 * @Description: TODO
 * @date Jan 8, 2016 3:00:56 PM
 */
public enum LoggingCategory {

    APPLICATION(Category.APPLICATIONS, "ME/Application"),

    ARCHIVING(Category.APPLICATIONS, "ME/Archiving"),

    BACKGROUND_PROCESSING(Category.APPLICATIONS, "ME/BackgroundProcessing"),

    CONFIGURATION_DATA_TRANSFER(Category.APPLICATIONS, "ME/ConfigurationDataTransfer"),

    EXTENSION_CONFIGURATION(Category.APPLICATIONS, "ME/Extension/Configuration"),

    EXTENSION_EXECUTION(Category.APPLICATIONS, "ME/Extension/Execution"),

    INTEGRATION(Category.APPLICATIONS, "ME/Integration"),

    LOCALIZATION(Category.APPLICATIONS, "ME/Localization"),

    PRINTING(Category.APPLICATIONS, "ME/Printing"),

    /**
     * Category of SYS_SECURITY/Authentication
     */
    SECURITY_AUTHENTICATION(Category.SYS_SECURITY, "Authentication"),

    /**
     * Category of SYS_SECURITY/Authorization
     */
    SECURITY_AUTHORIZATION(Category.SYS_SECURITY, "Authorization"),

    /**
     * Category of SYS_SECURITY/Audit/UserRetrieve
     */
    SECURITY_USER_RETRIEVAL(Category.SYS_SECURITY, "Audit/UserRetrieve"),

    /**
     * Category of SYS_SECURITY/Audit/IntrusionPrevention
     */
    SECURITY_PREVENTION(Category.SYS_SECURITY, "Audit/IntrusionPrevention"),

    SERVICE_CONFIGURATION(Category.APPLICATIONS, "ME/ServiceConfiguration");

    private String name;
    private Category rootCategory;
    private String subcategory;

    LoggingCategory(Category root, String subcategory) {
        this.rootCategory = root;
        this.subcategory = subcategory;
        this.name = root.getName() + "/" + subcategory;
    }

    public String getName() {
        return name;
    }

    public Category getRootCategory() {
        return rootCategory;
    }

    public String getSubCategoryName() {
        return subcategory;
    }
}