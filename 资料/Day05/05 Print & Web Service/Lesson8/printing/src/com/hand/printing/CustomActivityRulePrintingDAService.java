package com.hand.printing;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.hand.ejb.CustomReadActivityLocal;
import com.hand.printing.common.LoggingCategory;
import com.hand.printing.common.PrintingDAService;
import com.hand.printing.common.PrintingDAUtility;
import com.sap.me.document.DocumentConfiguration;
import com.sap.me.document.PrintingDataAcquisitionRequest;
import com.sap.me.document.PrintingDataAcquisitionResponse;
import com.sap.me.document.PrintingDataAcquisitionServiceInterface;
import com.sap.me.frame.Data;
import com.sap.me.frame.Utils;
import com.sap.me.frame.domain.BusinessException;

/**
 * @author Jun.Liu
 * @Description: 数据收集Class文件
 * @date Dec 9, 2016 1:24:52 PM
 */
public class CustomActivityRulePrintingDAService implements PrintingDataAcquisitionServiceInterface {
    private static final Logger logger = Logger.getLogger(LoggingCategory.PRINTING.getName());

    @Override
    public PrintingDataAcquisitionResponse acquirePrintingData(PrintingDataAcquisitionRequest request) throws BusinessException {
        long time = System.currentTimeMillis();

        PrintingDAService printDAService = new PrintingDAService();
        DocumentConfiguration documentConfi = printDAService.getDocumentConfiguration(request.getDocumentRef());

        CustomActivityRulePrintContent printContent = new CustomActivityRulePrintContent();
        // 读取打印配置信息
        printContent.setDocumentPrintingData(printDAService.acquireDocumentPrintingData(documentConfi));
        // 收集打印数量
        acquirePrintingData(request.getPrintByGbo(), printContent);

        PrintingDataAcquisitionResponse response = new PrintingDataAcquisitionResponse();
        response.setDocumentConfiguration(documentConfi);
        response.setPrintContent(PrintingDAUtility.marshal(printContent));

        logger.log(Level.INFO, response.getPrintContent());

        if (Utils.isTraceEnabled(Utils.TIMING)) {
            Utils.traceMsg("ActivityRulePrintingDAService execution time = " + (System.currentTimeMillis() - time) + " msec", Utils.TIMING);
        }
        return response;
    }

    private void acquirePrintingData(String printByGbo, CustomActivityRulePrintContent printContent) {
        printContent.setActivity(printByGbo);

        // 读取作业信息
        String description = null;
        List<CustomActivityRuleItemData> itemList = new ArrayList<CustomActivityRuleItemData>();

        try {
            Context context = new InitialContext();
            CustomReadActivityLocal ejb = (CustomReadActivityLocal) context.lookup("ejb:/appName=hand.com/apps~ear, jarName=hand.com~apps~ejb.jar, beanName=CustomReadActivityBean, interfaceName=com.hand.ejb.CustomReadActivityLocal");
            Data resultData = ejb.readActivity(printByGbo);
            if (!Utils.isEmpty(resultData)) {
                int count = 0;
                for (Data value : resultData) {
                    description = value.getString("DESCRIPTION", "");

                    CustomActivityRuleItemData item = new CustomActivityRuleItemData();
                    count++;
                    item.setId(String.valueOf(count));
                    item.setOption(value.getString("EXEC_UNIT_OPTION", ""));
                    item.setSetting(value.getString("SETTING", ""));
                    itemList.add(item);
                }
            }
        } catch (NamingException e) {
            Utils.traceMsg(e, Utils.EXCEPTION);
        }

        printContent.setDescription(description);
        printContent.setActivityRuleItemDatas(itemList);
    }

}
