package com.hand.printing;

import com.hand.printing.common.PrintingTransportService;
import com.sap.me.document.PrintingTransportServiceInterface;
import com.sap.me.document.TransportPrintingDataRequest;
import com.sap.me.document.TransportPrintingDataResponse;
import com.sap.me.frame.domain.BusinessException;

/**
 * @author Jun.Liu
 * @Description: 数据传输Class文件
 * @date Dec 9, 2016 1:37:50 PM
 */
public class CustomActivityRulePrintingTransportService implements PrintingTransportServiceInterface {

    @Override
    public TransportPrintingDataResponse transportPrintingData(TransportPrintingDataRequest request) throws BusinessException {

        TransportPrintingDataResponse response = new TransportPrintingDataResponse();
        PrintingTransportService printingTransportService = new PrintingTransportService();
        response = printingTransportService.transportPrintingData(request);

        return response;
    }

}
