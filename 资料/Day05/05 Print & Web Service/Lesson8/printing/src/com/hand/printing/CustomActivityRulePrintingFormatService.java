package com.hand.printing;

import com.sap.me.document.FormatPrintingDataRequest;
import com.sap.me.document.FormatPrintingDataResponse;
import com.sap.me.document.PrintingFormatServiceInterface;
import com.sap.me.frame.Utils;
import com.sap.me.frame.domain.BusinessException;

/**
 * @author Jun.Liu
 * @Description: 数据格式化Class文件
 * @date Dec 9, 2016 1:37:13 PM
 */
public class CustomActivityRulePrintingFormatService implements PrintingFormatServiceInterface {

    @Override
    public FormatPrintingDataResponse formatPrintingData(FormatPrintingDataRequest request) throws BusinessException {
        long time = System.currentTimeMillis();

        try {
            return new FormatPrintingDataResponse(request.getPrintContent());
        } finally {
            if (Utils.isTraceEnabled(Utils.TIMING)) {
                Utils.traceMsg("FormatPrintingDataResponse execution time = " + (System.currentTimeMillis() - time) + " msec", Utils.TIMING);
            }
        }
    }

}
