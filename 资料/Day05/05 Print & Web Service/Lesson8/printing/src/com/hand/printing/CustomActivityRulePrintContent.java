package com.hand.printing;

import java.util.List;

import com.hand.printing.common.DocumentPrintingData;

/**
 * @author Jun.Liu
 * @Description: 打印内容
 * @date Dec 9, 2016 1:27:07 PM
 */
public class CustomActivityRulePrintContent {

    private DocumentPrintingData documentPrintingData;
    private String activity;
    private String description;
    private List<CustomActivityRuleItemData> activityRuleItemDatas;
    private String documentNote;

    public DocumentPrintingData getDocumentPrintingData() {
        return documentPrintingData;
    }

    public void setDocumentPrintingData(DocumentPrintingData documentPrintingData) {
        this.documentPrintingData = documentPrintingData;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CustomActivityRuleItemData> getActivityRuleItemDatas() {
        return activityRuleItemDatas;
    }

    public void setActivityRuleItemDatas(List<CustomActivityRuleItemData> activityRuleItemDatas) {
        this.activityRuleItemDatas = activityRuleItemDatas;
    }

    public String getDocumentNote() {
        return documentNote;
    }

    public void setDocumentNote(String documentNote) {
        this.documentNote = documentNote;
    }
}
