package com.hand.printing.common;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.me.frame.domain.BusinessRuntimeException;
import com.sap.me.frame.xml.DOMHelper;

/**
 * 拷贝源程序PrintingDataAcquisitionService, 用于将数据转为XML格式
 * 
 * @author Jun.Liu
 * @Description: TODO
 * @date Jan 8, 2016 3:00:56 PM
 */
public class PrintingDAUtility {

    /**
     * Internal method to perform marshaling.
     */
    @SuppressWarnings("unchecked")
    public static final String marshal(Object payload) {
        try {
            Document document = DOMHelper.createDocument();
            JAXBContext jaxbContext = JAXBUtils.createJaxbContext(payload.getClass());

            Object element;
            if (payload.getClass().getAnnotation(XmlRootElement.class) == null) {
                element = new JAXBElement(new QName("", payload.getClass().getSimpleName()), payload.getClass(), payload);
            } else {
                element = payload;
            }

            jaxbContext.createMarshaller().marshal(element, document);

            removeNamespaces(document);
            document.normalizeDocument();
            return DOMHelper.convertXmlToString(document, true, "xml");
        } catch (JAXBException e) {
            throw new BusinessRuntimeException(e);
        } catch (TransformerException e) {
            throw new BusinessRuntimeException(e);
        }
    }

    protected static void removeNamespaces(Node node) {
        if (node.getPrefix() != null)
            node.setPrefix("");

        if (node.getNamespaceURI() != null)
            node.getOwnerDocument().renameNode(node, "", node.getNodeName());

        removeNamespaceAttributes(node);

        NodeList list = node.getChildNodes();
        for (int i = 0; list != null && i < list.getLength(); ++i) {
            removeNamespaces(list.item(i));
        }
    }

    protected static void removeNamespaceAttributes(Node node) {
        NamedNodeMap attributes = node.getAttributes();
        if (attributes != null && attributes.getLength() > 0) {
            List<String> attributesToRemove = new ArrayList<String>();
            for (int i = 0; i < attributes.getLength(); i++) {
                Node subNode = attributes.item(i);
                if (subNode.getNodeType() == Node.ATTRIBUTE_NODE) {
                    Attr attribute = (Attr) subNode;
                    if ((attribute.getPrefix() != null && (attribute.getPrefix().equals("xsi") || attribute.getPrefix().equals("xmlns"))) || attribute.getName().equals("xmlns")) {
                        attributesToRemove.add(attribute.getName());
                    }
                }
            }
            for (String attribute : attributesToRemove)
                node.getAttributes().removeNamedItem(attribute);
        }
    }
}
