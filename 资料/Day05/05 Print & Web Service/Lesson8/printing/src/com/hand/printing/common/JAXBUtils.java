package com.hand.printing.common;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

/**
 * 拷贝源程序
 * 
 * @author Jun.Liu
 * @Description: TODO
 * @date Jan 8, 2016 3:00:56 PM
 */
public class JAXBUtils {

    static MRUCache mruCache = new MRUCache(MRUCache.CACHE_SIZE);

    public static JAXBContext createJaxbContext(Class<?> forClass) throws JAXBException {
        return getCachedObject(forClass);
    }

    private static JAXBContext getCachedObject(Class forClass) throws JAXBException {
        String className = forClass.getName();
        Object cachedObject = mruCache.get(className);
        JAXBContext jc = null;
        if (cachedObject == null) {
            jc = JAXBContext.newInstance(forClass);
            mruCache.put(className, jc);
        } else {
            jc = (JAXBContext) cachedObject;
        }
        return jc;
    }

}
