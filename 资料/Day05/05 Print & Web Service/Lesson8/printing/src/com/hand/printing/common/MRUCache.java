package com.hand.printing.common;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Cache for storing MRU objects... If we try to put new value into cache and cache is full then we remove last used element
 */
public class MRUCache {

    public static final int CACHE_SIZE = 10;

    private Map mruMap;
    private final int cacheSize;

    public MRUCache(final int cacheSize) {
        this.cacheSize = cacheSize;
        mruMap = Collections.synchronizedMap(createMRUMap());
    }

    private LinkedHashMap createMRUMap() {
        int capacity = (int) Math.ceil(cacheSize / 0.75f) + 1;

        return new LinkedHashMap(capacity, 0.75f, true) {
            private static final long serialVersionUID = 1940895961382434295L;

            /*
             * (non-Javadoc)
             * 
             * @see java.util.LinkedHashMap#removeEldestEntry(java.util.Map.Entry)
             */
            protected boolean removeEldestEntry(Map.Entry eldest) {
                return size() > cacheSize;
            }
        };
    }

    public Object put(Object key, Object value) {
        return mruMap.put(key, value);
    }

    public boolean contains(Object key) {
        return mruMap.containsKey(key);
    }

    public Object get(Object key) {
        return mruMap.get(key);
    }

    public int size() {
        return mruMap.size();
    }

    public void remove(Object key) {
        mruMap.remove(key);
    }

}
