package com.hand.printing.common;

import com.visiprise.globalization.DateGlobalizationServiceInterface;
import com.visiprise.globalization.GlobalizationService;
import com.visiprise.globalization.KnownGlobalizationServices;
import com.visiprise.globalization.util.DateFormatStyle;
import com.visiprise.globalization.util.DateTimeInterface;

/**
 * 拷贝源程序
 * 
 * @author Jun.Liu
 * @Description: TODO
 * @date Jan 8, 2016 3:00:56 PM
 */
public class FileNameFactory {

    // Singleton variable
    private static FileNameFactory fileNameFactory_ = null;
    private static final Object lockCreate_ = new Object();
    private static final Object lock_ = new Object();
    private static final Object lockShift_ = new Object();

    private final static int maxtimeCount = 0x07FFFFFF;

    private int timeCount;
    private final int timeBaseCount;

    private StringBuffer fileNameBuff = new StringBuffer();
    private int shift = 0;
    private boolean ifAcquired = false;
    private Object transactObject;
    private DateTimeInterface currentDate;

    private FileNameFactory() {
        DateGlobalizationServiceInterface dateServ = GlobalizationService.getUserService(KnownGlobalizationServices.DATE);
        timeBaseCount = (int) ((currentDate = dateServ.createDateTime()).getTimeInMillis() & maxtimeCount);
        timeCount = timeBaseCount + 1;
    }

    /**
     * Singleton method
     */
    public static FileNameFactory getFileNameFactory() {

        synchronized (lockCreate_) {
            if (fileNameFactory_ == null)
                return (fileNameFactory_ = new FileNameFactory());
        }// sync

        return fileNameFactory_;
    }

    /**
     * @param fileNamePrefix
     *            the String of the file name
     * @return The unique file name as String
     * 
     */
    public String getNewFileName(String fileNamePrefix) {
        String result_ = "";

        synchronized (lock_) {

            fileNameBuff.setLength(0);

            timeCount = (timeCount + maxtimeCount) % maxtimeCount + 1;

            if (timeCount == timeBaseCount + 1) {
                addShift();
            }
            DateGlobalizationServiceInterface dateServ = GlobalizationService.getUserService(KnownGlobalizationServices.DATE);
            fileNameBuff.append(fileNamePrefix).append('-').append(shift).append('-').append(dateServ.formatDate(DateFormatStyle.ISO_COMBINED, currentDate)).append('-').append(timeCount);

            result_ = fileNameBuff.toString();

        }

        return result_;
    }

    /**
     * Makes the file name unique if it is necessary.
     * 
     * @param fileNamePrefix
     *            the String of the file name
     * @return The unique file name as String
     */
    public String shiftName(String fileNamePrefix) {
        int localShift = shift;

        synchronized (lock_) {
            if (localShift == shift) {
                addShift();
            }

            return getNewFileName(fileNamePrefix);
        } // sync

    }

    /**
     * Acquire this object before to begin some transaction
     * 
     * @return boolean flag if this object was really acquired.
     */
    public boolean acquire(Object transact_Object) {
        boolean status_ = false;

        if (!ifAcquired && transact_Object != null) {
            synchronized (lockShift_) {
                if (!ifAcquired) {
                    this.transactObject = transact_Object;
                    return ifAcquired = true;
                }
            }// sync
        }
        return status_;
    }

    //
    public void release(Object transact_Object) {
        if (this.transactObject == transact_Object) {
            synchronized (lockShift_) {
                ifAcquired = false;
                this.transactObject = null;
            }
        }
    }

    /**
     * Increments the shift.
     */
    private void addShift() {
        DateGlobalizationServiceInterface dateServ = GlobalizationService.getUserService(KnownGlobalizationServices.DATE);
        currentDate = dateServ.createDateTime();
        shift++;
    }

}