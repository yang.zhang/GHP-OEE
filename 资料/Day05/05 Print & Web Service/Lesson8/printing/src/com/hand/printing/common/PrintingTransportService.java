package com.hand.printing.common;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

import com.sap.me.appconfig.FindSystemRuleSettingRequest;
import com.sap.me.appconfig.SystemRuleServiceInterface;
import com.sap.me.appconfig.SystemRuleSetting;
import com.sap.me.common.ObjectReference;
import com.sap.me.document.DocumentConfiguration;
import com.sap.me.document.DocumentFlavorNotSupportedException;
import com.sap.me.document.DocumentNotConfiguredException;
import com.sap.me.document.DocumentPrintingFailedException;
import com.sap.me.document.InvalidPdfDirectoryException;
import com.sap.me.document.InvalidTemplateDirectoryException;
import com.sap.me.document.InvalidTemplateException;
import com.sap.me.document.NoPrintersFoundException;
import com.sap.me.document.PdfDirectoryNotWriteableException;
import com.sap.me.document.PrinterConfigurationServiceInterface;
import com.sap.me.document.PrinterFullConfiguration;
import com.sap.me.document.PrinterNotConfiguredException;
import com.sap.me.document.PrinterNotFoundException;
import com.sap.me.document.TemplateDirectoryNotConfiguredException;
import com.sap.me.document.TemplateNotConfiguredException;
import com.sap.me.document.TransportPrintingDataRequest;
import com.sap.me.document.TransportPrintingDataResponse;
import com.sap.me.extension.Services;
import com.sap.me.frame.BOHandle;
import com.sap.me.frame.Utils;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.frame.service.CommonMethods;
import com.sap.tc.adobe.pdfdocument.api.IPDFCreationContext;
import com.sap.tc.adobe.pdfdocument.api.IPDFDocument;
import com.sap.tc.adobe.pdfdocument.api.IPDFDocumentHandler;
import com.sap.tc.adobe.pdfdocument.api.IPDLCreationContext;
import com.sap.tc.adobe.pdfdocument.api.PDFDocumentFactory;
import com.sap.tc.adobe.pdfdocument.api.PDLDestination;

/**
 * 拷贝源程序
 * 
 * @author Jun.Liu
 * @Description: TODO
 * @date Jan 8, 2016 3:00:56 PM
 */
public class PrintingTransportService {

    private static final int DEFAULT_BUFFER_SIZE_FOR_FILE_OPERATIONS = 1024; // Default buffer size for file reading
    private static final String FORMAT_ARGUMENT_FORMAT_KEY = "PRINT_FORMAT";
    private static final String FORMAT_ARGUMENT_XDC_KEY = "XDC";

    // Adobe Post Script format
    private static final String PRINT_FORMAT_PS = "PS";

    // HP Printer Command Language format
    private static final String PRINT_FORMAT_PCL = "PCL";

    // Zebra Programming Language format
    private static final String PRINT_FORMAT_ZPL = "ZPL";

    // Adobe PDF Post Script format
    private static final String PRINT_FORMAT_PDF = "PDF";

    // Intermec
    private static final String PRINT_FORMAT_IPL = "IPL";

    // Datamax
    private static final String PRINT_FORMAT_DPL = "DPL";

    // Toshiba
    private static final String PRINT_FORMAT_TPCL = "TPCL";

    // Printers that support PostScript language level 2 (monochrome and color)
    private static final String DEFAULT_XDC_PS = "ps_plain.xdc";

    // Printers that support the HP PCL 5e printer language (monochrome)
    private static final String DEFAULT_XDC_PCL = "hppcl5e.xdc";

    // Zebra label printers (monochrome) at 300 dpi (12 dots/mm)
    private static final String DEFAULT_XDC_ZPL = "zpl300.xdc";

    // Intermec 203 DPI
    private static final String DEFAULT_XDC_IPL = "ipl203.xdc";

    // Datamax 300 DPI
    private static final String DEFAULT_XDC_DPL = "dpl300.xdc";

    // Toshiba 305 DPI
    private static final String DEFAULT_XDC_TPCL = "tpcl305.xdc";

    // Adobe PDF 1.5
    private static final String DEFAULT_XDC_PDF = "acrobat6.xdc";

    private enum UserArgumentTypeEnum {
        FORMAT, TRANSPORT;
    }

    private SystemRuleServiceInterface systemRuleService;
    private PrinterConfigurationServiceInterface printerService;
    private String documentName;
    private String templateDirectory;
    private String pdfDirectory;
    private File templateFile;
    private static Logger logger = Logger.getLogger(LoggingCategory.PRINTING.getName());

    public TransportPrintingDataResponse transportPrintingData(TransportPrintingDataRequest request) throws BusinessException {
        long time = System.currentTimeMillis();
        printerService = Services.getService("com.sap.me.document", "PrinterConfigurationService");
        TransportPrintingDataResponse response = new TransportPrintingDataResponse();

        documentName = request.getDocumentConfiguration().getDocument();
        if (documentName == null) {
            DocumentNotConfiguredException e = new DocumentNotConfiguredException();
            logger.log(Level.SEVERE, "Document is missing", e);
            throw e;
        }

        String printerRef = request.getPrinterRef();
        PrinterFullConfiguration printerConfiguration = printerService.readPrinter(new ObjectReference(printerRef));
        String printer = printerConfiguration.getPrinter();
        if (printer == null || printer.length() == 0) {
            PrinterNotConfiguredException e = new PrinterNotConfiguredException();
            logger.log(Level.SEVERE, "Printer is missing", e);
            throw e;
        }

        String printContent = request.getPrintContent();
        DocumentConfiguration documentConfiguration = request.getDocumentConfiguration();

        writePrintContentForDebugging(printContent);
        validateTemplateDirectory();
        validateTemplateFile(request.getDocumentConfiguration().getTemplate());

        if (Utils.isTraceEnabled(Utils.TIMING)) {
            Utils.traceMsg("PrintingTransportAdsService validated time = " + (System.currentTimeMillis() - time) + " msec", Utils.TIMING);
            time = System.currentTimeMillis();
        }

        boolean storePdf = new Boolean(retrieveSystemRule("STORE_PDF_DOC"));
        if (storePdf) {
            validatePdfDirectory();
            writePdfFile(printContent, request.getPrintByGbo());
            if (Utils.isTraceEnabled(Utils.TIMING)) {
                Utils.traceMsg("PrintingTransportAdsService PDF stored time = " + (System.currentTimeMillis() - time) + " msec", Utils.TIMING);
                time = System.currentTimeMillis();
            }
        }

        printDocument(documentConfiguration, printContent, printer);
        if (Utils.isTraceEnabled(Utils.TIMING)) {
            Utils.traceMsg("PrintingTransportAdsService finished time = " + (System.currentTimeMillis() - time) + " msec", Utils.TIMING);
            time = System.currentTimeMillis();
        }
        return response;
    }

    /**
     * Reads template directory configured in System Rules and confirms the directory exists.
     */
    private void validateTemplateDirectory() throws BusinessException {
        templateDirectory = retrieveSystemRule("DOC_TEMPLATE_DIR");
        if (templateDirectory == null || templateDirectory.trim().length() <= 0) {
            TemplateDirectoryNotConfiguredException e = new TemplateDirectoryNotConfiguredException(documentName);
            logger.log(Level.SEVERE, "Document Printing Template Directory is missing for document " + documentName, e);
            throw e;
        }

        if (!templateDirectory.endsWith(File.separator)) {
            templateDirectory += File.separator;
        }

        if (!new File(templateDirectory).isDirectory()) {
            InvalidTemplateDirectoryException e = new InvalidTemplateDirectoryException(templateDirectory, documentName);
            logger.log(Level.SEVERE, "Document Printing Template Directory " + templateDirectory + " is invalid for document" + documentName, e);
            throw e;
        }
    }

    /**
     * Reads PDF directory configured in System Rules and confirms the directory exists and has write permission.
     */
    private void validatePdfDirectory() throws BusinessException {
        pdfDirectory = retrieveSystemRule("DOC_DIR");
        if (!pdfDirectory.endsWith(File.separator)) {
            pdfDirectory += File.separator;
        }
        if (!new File(pdfDirectory).isDirectory()) {
            InvalidPdfDirectoryException e = new InvalidPdfDirectoryException(pdfDirectory, documentName);
            logger.log(Level.SEVERE, "Document Printing Temporary Directory " + pdfDirectory + " is invalid for document " + documentName, e);
            throw e;
        }
        if (!new File(pdfDirectory).canWrite()) {
            PdfDirectoryNotWriteableException e = new PdfDirectoryNotWriteableException(pdfDirectory, documentName);
            logger.log(Level.SEVERE, "Cannot write to " + pdfDirectory + " specified in Document Printing Temporary Directory for document " + documentName, e);
            throw e;
        }
    }

    /**
     * Confirms the template file exists in the configured template directory.
     * 
     * @param templateName
     *            The template to use for printing.
     */
    private void validateTemplateFile(String templateName) throws BusinessException {
        if (templateName == null) {
            TemplateNotConfiguredException e = new TemplateNotConfiguredException();
            logger.log(Level.SEVERE, "Template is missing", e);
            throw e;
        }
        templateFile = new File(templateDirectory + templateName);
        if (!templateFile.exists()) {
            InvalidTemplateException e = new InvalidTemplateException(templateName, documentName);
            logger.log(Level.SEVERE, "Template " + templateName + " is invalid for document " + documentName, e);
            throw e;
        }
    }

    /**
     * Writes the print content to <code>usr\sap\<instance>\<server><code>
     * directory as a file named "mePrintContent.xml"
     * <p>
     * The file is only written if DEBUGGING is enabled in SAP ME application
     * settings. This functionality is only provided for troubleshooting
     * purposes.
     * 
     * @param printContent
     *            The formatted content from the print request.
     */
    private void writePrintContentForDebugging(String printContent) throws BusinessException {
        if (Utils.isTraceEnabled(Utils.DEBUGGING)) {
            try {
                writeFile("mePrintContent.xml", printContent.getBytes("UTF-8"));
            } catch (Exception e) {
                DocumentPrintingFailedException ex = new DocumentPrintingFailedException(e.getLocalizedMessage());
                logger.log(Level.SEVERE, "Document printing failed: " + e.getLocalizedMessage(), ex);
                throw ex;
            }
        }
    }

    /**
     * Writes PDF file to the configured directory location.
     * 
     * @param printContent
     *            The formatted content from the print request.
     * @param documentConfiguration
     *            The configuration settings for the document from Document Maintenance.
     */
    private void writePdfFile(String printContent, String printByGbo) throws BusinessException {

        // write PDF file
        String site = CommonMethods.getSite();
        InputStream pdfStream;
        try {
            pdfStream = createPdf(printContent.getBytes("UTF-8"), templateFile);
        } catch (UnsupportedEncodingException e) {
            DocumentPrintingFailedException ex = new DocumentPrintingFailedException(e.getLocalizedMessage());
            logger.log(Level.SEVERE, "Document printing failed: " + e.getLocalizedMessage(), ex);
            throw ex;
        }

        long time = System.currentTimeMillis();
        BOHandle printByHandle = new BOHandle(printByGbo);
        FileNameFactory filenameFactory = FileNameFactory.getFileNameFactory();
        String pdfFileName = pdfDirectory + filenameFactory.getNewFileName(site + "-" + documentName) + "-" + printByHandle.getComponent(2) + ".pdf";
        try {
            writeFile(pdfFileName, pdfStream);
        } catch (IOException e) {
            DocumentPrintingFailedException ex = new DocumentPrintingFailedException(e.getLocalizedMessage());
            logger.log(Level.SEVERE, "Document printing failed: " + e.getLocalizedMessage(), ex);
            throw ex;
        } finally {
            Utils.safeClose(pdfStream);
            if (Utils.isTraceEnabled(Utils.TIMING)) {
                Utils.traceMsg("...writePdfFile write file time = " + (System.currentTimeMillis() - time) + " msec", Utils.TIMING);
            }

        }
    }

    /**
     * Sends the document to the configured printer.
     * 
     * @param printContent
     *            The formatted content from the print request.
     * @param documentConfiguration
     *            The configuration settings for the document from Document Maintenance.
     * @param printer
     *            The name of the destination printer.
     */
    private void printDocument(DocumentConfiguration documentConfiguration, String printContent, String printer) throws BusinessException {
        // get printing format
        String printingFormat = PRINT_FORMAT_PS;
        String xdcFileName = null;
        String[] args = getUserArguments(documentConfiguration, UserArgumentTypeEnum.TRANSPORT);
        if (args != null) {
            for (String arg : args) {
                if (arg != null && arg.startsWith(FORMAT_ARGUMENT_FORMAT_KEY)) {
                    printingFormat = arg.substring(FORMAT_ARGUMENT_FORMAT_KEY.length() + 1);
                } else if (arg != null && arg.startsWith(FORMAT_ARGUMENT_XDC_KEY)) {
                    xdcFileName = arg.substring(FORMAT_ARGUMENT_XDC_KEY.length() + 1);
                }
            }
        }
        // print out the document
        InputStream pdlStream = null;

        long time = System.currentTimeMillis();

        try {
            pdlStream = createPDL(printContent, templateFile, printingFormat, xdcFileName);
            // if printer name = "DEBUG" write printing output to file x:/usr/sap/N74/J04/j2ee/cluster/server0/mePrintOutput.dat
            // to check the output format instead of sending it to printer
            if ("DEBUG".equals(printer)) {
                writeFile("mePrintOutput.dat", pdlStream);
            } else {
                printPDL(printer, pdlStream);
            }
        } catch (Exception e) {
            DocumentPrintingFailedException ex = new DocumentPrintingFailedException(e.getLocalizedMessage());
            logger.log(Level.SEVERE, "Document printing failed: " + e.getLocalizedMessage(), ex);
            throw ex;
        } finally {
            Utils.safeClose(pdlStream);
        }

        if (Utils.isTraceEnabled(Utils.TIMING)) {
            Utils.traceMsg("...printDocument printing time = " + (System.currentTimeMillis() - time) + " msec", Utils.TIMING);
            time = System.currentTimeMillis();
        }
    }

    /**
     * Retrieves Format or Transport user arguments and splits into individual values where user argument contains multiple values delimited by semicolon.
     * 
     * @param documentConfigurateion
     *            The name of the rule to read.
     * @param userArgumentType
     *            Type of user argument - FORMAT or TRANSPORT.
     * @return The individual user arguments.
     */
    private String[] getUserArguments(DocumentConfiguration documentConfiguration, UserArgumentTypeEnum userArgumentType) {
        String args = null;

        switch (userArgumentType) {
        case FORMAT:
            args = documentConfiguration.getFormatUserArg();
            break;
        case TRANSPORT:
            args = documentConfiguration.getTransportUserArg();
            break;
        }
        ;

        return args == null ? null : args.split(";");
    }

    /**
     * Create PDF using Adobe Document Services via PDF-Object API by a template and data XML
     * 
     * @param data
     *            The data in XML format to be included into PDF
     * @param template
     *            An XML(XDP) template created at design time in Adobe LifeCycle Designer
     * @return PDF bytes
     */
    private InputStream createPdf(byte[] data, File templateFile) throws BusinessException {
        IPDFDocumentHandler ipdfDocumentHandler = com.sap.tc.adobe.pdfdocument.api.PDFDocumentFactory.getDocumentHandler();
        IPDFCreationContext creationContext = ipdfDocumentHandler.getPDFCreationContext();
        creationContext.setData(data);
        ByteArrayOutputStream outputDoc = getTemplateAsByteArray(templateFile);
        creationContext.setTemplate(outputDoc);
        creationContext.setInteractive(false);
        IPDFDocument document;
        try {
            long time = System.currentTimeMillis();
            document = creationContext.execute(com.sap.tc.adobe.pdfdocument.api.PDFDocumentFactory.getDocumentHandler().getExecutionEnvironment());
            if (Utils.isTraceEnabled(Utils.TIMING)) {
                Utils.traceMsg("...createPdf context execution time = " + (System.currentTimeMillis() - time) + " msec", Utils.TIMING);
                time = System.currentTimeMillis();
            }
            return document.getPDFAsStream();
        } catch (Exception e) {
            DocumentPrintingFailedException ex = new DocumentPrintingFailedException(e.getLocalizedMessage());
            logger.log(Level.SEVERE, "Document printing failed: " + e.getLocalizedMessage(), ex);
            throw ex;
        }
    }

    /**
     * Read template file from the file system to the array of bytes
     * 
     * @param templateFile
     *            file object for the printing template
     * @return array of bytes that contains document template
     * @throws BusinessException
     */
    private ByteArrayOutputStream getTemplateAsByteArray(File templateFile) throws BusinessException {
        ByteArrayOutputStream outputDoc = new ByteArrayOutputStream();
        InputStream is = null;
        try {
            is = new FileInputStream(templateFile);
            byte buf[] = new byte[DEFAULT_BUFFER_SIZE_FOR_FILE_OPERATIONS];
            int len;
            while ((len = is.read(buf)) > 0) {
                outputDoc.write(buf, 0, len);
            }
            outputDoc.close();
        } catch (FileNotFoundException fnfException) {
            DocumentPrintingFailedException ex = new DocumentPrintingFailedException(fnfException.getLocalizedMessage());
            logger.log(Level.SEVERE, "Document printing failed: " + fnfException.getLocalizedMessage(), ex);
            throw ex;
        } catch (IOException ioException) {
            DocumentPrintingFailedException ex = new DocumentPrintingFailedException(ioException.getLocalizedMessage());
            logger.log(Level.SEVERE, "Document printing failed: " + ioException.getLocalizedMessage(), ex);
            throw ex;
        } finally {
            Utils.safeClose(is);
        }
        return outputDoc;
    }

    /**
     * Create PDL using Adobe Document Services via PDF-Object API by a template and data XML
     * 
     * @param data
     *            The data in XML format to be included into PDF
     * @param template
     *            An XML(XDP) template created at design time in Adobe LifeCycle Designer
     * @param printingFormat
     *            The supported formats PS (Adobe Post Script), PCL (HP Printer Command Language format) or ZPL (Zebra Programming Language) Also added IPL, DPL, IPDL and PDF
     * @param xdcFileName
     *            The template file name. If not specified there will be used default XDC templates: For postscript(PS) printing: ps_plain.xdc Printers that support PostScript language level 2 (monochrome and color) For PCL: hppcl5e.xdc Printers that support the HP PCL 5e printer language (monochrome) For ZPL: zpl300.xdc Zebra label printers (monochrome) at 300 dpi (12 dots/mm)
     * 
     * @return PDF bytes
     */
    private InputStream createPDL(String printContent, File templateFile, String printingFormat, String xdcFileName) throws BusinessException {
        IPDLCreationContext creationContext = PDFDocumentFactory.getDocumentHandler().getPDLCreationContext();
        try {
            creationContext.setData(printContent.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            DocumentPrintingFailedException ex = new DocumentPrintingFailedException(e.getLocalizedMessage());
            logger.log(Level.SEVERE, "Document printing failed: " + e.getLocalizedMessage(), ex);
            throw ex;
        }

        ByteArrayOutputStream outputDoc = getTemplateAsByteArray(templateFile);
        creationContext.setTemplate(outputDoc);
        PDLDestination pdlType = getPdlDestination(printingFormat);
        creationContext.setPrintFormat(pdlType);

        // set default xdc if not specified
        if (xdcFileName == null || xdcFileName.trim().length() <= 0) {
            xdcFileName = getDefaultXdc(printingFormat);
        }
        creationContext.setXDC(xdcFileName);

        long time = System.currentTimeMillis();
        try {
            IPDFDocument document = creationContext.execute(com.sap.tc.adobe.pdfdocument.api.PDFDocumentFactory.getDocumentHandler().getExecutionEnvironment());
            if (Utils.isTraceEnabled(Utils.TIMING)) {
                Utils.traceMsg("...createPDL context executed = " + (System.currentTimeMillis() - time) + " msec", Utils.TIMING);
                time = System.currentTimeMillis();
            }
            return document.getPDLDocument().getPDLAsStream();
        } catch (Exception e) {
            DocumentPrintingFailedException ex = new DocumentPrintingFailedException(e.getLocalizedMessage());
            logger.log(Level.SEVERE, "Document printing failed: " + e.getLocalizedMessage(), ex);
            throw ex;
        } finally {

            if (Utils.isTraceEnabled(Utils.TIMING)) {
                Utils.traceMsg("...createPDL document created = " + (System.currentTimeMillis() - time) + " msec", Utils.TIMING);
                time = System.currentTimeMillis();
            }
        }
    }

    private String getDefaultXdc(String printingFormat) {
        String xdcFileName = null;
        if (PRINT_FORMAT_PS.equalsIgnoreCase(printingFormat)) {
            xdcFileName = DEFAULT_XDC_PS;
        } else if (PRINT_FORMAT_PCL.equalsIgnoreCase(printingFormat)) {
            xdcFileName = DEFAULT_XDC_PCL;
        } else if (PRINT_FORMAT_ZPL.equalsIgnoreCase(printingFormat)) {
            xdcFileName = DEFAULT_XDC_ZPL;
        } else if (PRINT_FORMAT_IPL.equalsIgnoreCase(printingFormat)) {
            xdcFileName = DEFAULT_XDC_IPL;
        } else if (PRINT_FORMAT_DPL.equalsIgnoreCase(printingFormat)) {
            xdcFileName = DEFAULT_XDC_DPL;
        } else if (PRINT_FORMAT_TPCL.equalsIgnoreCase(printingFormat)) {
            xdcFileName = DEFAULT_XDC_TPCL;
        } else if (PRINT_FORMAT_PDF.equalsIgnoreCase(printingFormat)) {
            xdcFileName = DEFAULT_XDC_PDF;
        }
        return xdcFileName;
    }

    /**
     * Converts configured printing format String to PDLDestination.
     * 
     * @param printingFormat
     *            The printing format configured.
     * @return The PDLDestination value.
     */
    private PDLDestination getPdlDestination(String printingFormat) {
        if (printingFormat == null || printingFormat.trim().length() <= 0)
            printingFormat = "PS";
        PDLDestination dest = PDLDestination.getDestination(printingFormat.toUpperCase(Locale.ENGLISH));
        if (dest == null)
            dest = PDLDestination.PS;
        return dest;
    }

    /**
     * Print document to the printing device
     * 
     * @param printerName
     *            The name of the destination printer.
     * @param pdlStream
     *            A PDL document input stream.
     */
    public void printPDL(String printerName, InputStream pdlStream) throws BusinessException {
        printPDL(printerName, pdlStream, false);
    }

    private void printPDL(String printerName, InputStream pdlStream, boolean refreshPrintServices) throws BusinessException {
        if (refreshPrintServices)
            refreshSystemPrinterList();

        PrintService foundService = null;
        AttributeSet unsupportedSet = null;

        DocAttributeSet docAttributes = new HashDocAttributeSet();
        PrintRequestAttributeSet printAttributes = new HashPrintRequestAttributeSet();

        DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        if (printServices == null || printServices.length == 0) {
            if (!refreshPrintServices) {
                printPDL(printerName, pdlStream, true); // refresh printer list and try again
                return;
            } else {
                NoPrintersFoundException ex = new NoPrintersFoundException();
                logger.log(Level.SEVERE, "No printers found in the host system", ex);
                throw ex;
            }
        }

        for (PrintService printService : printServices) {
            if (printService.getName() != null && printService.getName().endsWith(printerName)) {
                foundService = printService;
                try {
                    unsupportedSet = printService.getUnsupportedAttributes(flavor, printAttributes);
                    if (unsupportedSet == null) {
                        DocPrintJob printJob = printService.createPrintJob();
                        Doc doc = new SimpleDoc(pdlStream, flavor, docAttributes);
                        try {
                            printJob.print(doc, printAttributes);
                        } catch (PrintException e) {
                            DocumentPrintingFailedException ex = new DocumentPrintingFailedException(e.getLocalizedMessage());
                            logger.log(Level.SEVERE, "Document printing failed: " + e.getLocalizedMessage(), ex);
                            throw ex;
                        }
                        return;
                    }
                } catch (IllegalArgumentException iae) {
                    Utils.traceMsg("DocFlavor not supported by service: " + iae.getMessage(), Utils.ERRORTRACE);
                    // DocFlavor not supported by service, skiping.
                }
            }
        }

        if (foundService == null) {
            if (!refreshPrintServices) {
                printPDL(printerName, pdlStream, true); // refresh printer list and try again
                return;
            } else {
                PrinterNotFoundException ex = new PrinterNotFoundException(printerName);
                logger.log(Level.SEVERE, "Printer " + printerName + " was not found in the host system", ex);
                throw ex;
            }
        }

        logSupportedflavors(printerName, unsupportedSet, foundService);

        DocumentFlavorNotSupportedException ex = new DocumentFlavorNotSupportedException(printerName, flavor.toString());
        logger.log(Level.SEVERE, "Printer " + printerName + " does not support printing to " + flavor.toString(), ex);
        throw ex;
    }

    private void logSupportedflavors(String printerName, AttributeSet unsupportedSet, PrintService foundService) {
        StringBuffer sb = new StringBuffer();
        sb.append("Printer " + printerName + " doesn't support attributes: ");
        if (unsupportedSet != null)
            for (int i = 0; i < unsupportedSet.size(); i++) {
                sb.append("[" + unsupportedSet.toArray()[i].getName() + "]");
            }

        sb.append("\nSupported flavors are: ");
        if (foundService.getSupportedDocFlavors() != null)
            for (DocFlavor flavor : foundService.getSupportedDocFlavors()) {
                sb.append("[" + flavor.toString() + "]");
            }

        sb.append("\nSupported attribute categories are: ");
        if (foundService.getSupportedAttributeCategories() != null)
            for (Class<?> a : foundService.getSupportedAttributeCategories()) {
                sb.append("[" + a.getName() + "]");
            }

        Utils.traceMsg(sb.toString(), Utils.ERRORTRACE);
    }

    /**
     * Printer list does not necessarily refresh if you change the list of printers within the O/S; you can run this to refresh if necessary.
     */
    public static void refreshSystemPrinterList() {
        try {
            Class[] classes = PrintServiceLookup.class.getDeclaredClasses();
            for (int i = 0; i < classes.length; i++) {
                if ("javax.print.PrintServiceLookup$Services".equals(classes[i].getName())) {
                    sun.awt.AppContext.getAppContext().remove(classes[i]);
                    // System.out.println("Refreshed Printer List");
                    break;
                }
            }
        } catch (Exception e) {
            Utils.traceMsg(e, Utils.ERRORTRACE);
        }

    }

    /**
     * Retrieves a system rule value from System Rules configuration.
     * 
     * @param ruleName
     *            The name of the system rule to retrieve.
     * @return The system rule value.
     */
    private String retrieveSystemRule(String ruleName) throws BusinessException {
        systemRuleService = Services.getService("com.sap.me.appconfig", "SystemRuleService");
        FindSystemRuleSettingRequest request = new FindSystemRuleSettingRequest();
        request.setRuleName(ruleName);

        SystemRuleSetting setting = systemRuleService.findSystemRuleSetting(request);
        return setting.getSetting().toString();
    }

    /**
     * Writes a file to the file system.
     * 
     * @param fileName
     *            The name of the rule to read.
     * @param stream
     *            The content of the file as input stream.
     */
    private void writeFile(String fileName, InputStream stream) throws java.io.IOException {
        DataInputStream dis = new DataInputStream(stream);
        java.io.File file = new java.io.File(fileName);
        FileOutputStream fos = new FileOutputStream(file);

        try {
            byte[] buf = new byte[DEFAULT_BUFFER_SIZE_FOR_FILE_OPERATIONS];
            int len;
            while ((len = dis.read(buf)) > 0) {
                fos.write(buf, 0, len);
            }
        } finally {
            Utils.safeClose(dis);
            Utils.safeClose(fos);
        }
    }

    /**
     * Writes a file to the file system.
     * 
     * @param fileName
     *            The name of the rule to read.
     * @param data
     *            The content of the file as byte array.
     */
    private void writeFile(String fileName, byte[] data) throws java.io.IOException {
        java.io.File file = new java.io.File(fileName);
        FileOutputStream fos = new FileOutputStream(file);
        try {
            fos.write(data);
        } finally {
            Utils.safeClose(fos);
        }
    }

    /**
     * Setter injection for the system rule service
     * 
     * @param systemRuleService
     */
    public void setSystemRuleService(SystemRuleServiceInterface systemRuleService) {
        this.systemRuleService = systemRuleService;
    }

    /**
     * Setter injection for the printer service
     * 
     * @param printerService
     */
    public void setPrinterService(PrinterConfigurationServiceInterface printerService) {
        this.printerService = printerService;
    }
}
