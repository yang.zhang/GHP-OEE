package com.hand.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.hand.common.CustomActivityMaintenanceApplicationInterface;
import com.hand.service.CustomActivityRuleRequest;
import com.hand.service.CustomActivityRuleResponse;
import com.hand.service.impl.CustomActivityConfigServiceInterface;
import com.sap.me.activity.ActivityBOHandle;
import com.sap.me.activity.ActivityConfigurationServiceInterface;
import com.sap.me.activity.ActivityFullConfiguration;
import com.sap.me.activity.ActivityOption;
import com.sap.me.common.ObjectReference;
import com.sap.me.document.DocumentConfiguration;
import com.sap.me.document.FormatPrintingDataRequest;
import com.sap.me.document.FormatPrintingDataResponse;
import com.sap.me.document.PrinterBOHandle;
import com.sap.me.document.PrintingDataAcquisitionRequest;
import com.sap.me.document.PrintingDataAcquisitionResponse;
import com.sap.me.document.PrintingDataAcquisitionServiceInterface;
import com.sap.me.document.PrintingFormatServiceInterface;
import com.sap.me.document.PrintingTransportServiceInterface;
import com.sap.me.document.TransportPrintingDataRequest;
import com.sap.me.document.TransportPrintingDataResponse;
import com.sap.me.document.domain.DocumentDO;
import com.sap.me.extension.Services;
import com.sap.me.frame.BasicBOBeanException;
import com.sap.me.frame.Data;
import com.sap.me.frame.JNDIUtils;
import com.sap.me.frame.ServiceLocator;
import com.sap.me.frame.SystemBase;
import com.sap.me.frame.Utils;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.frame.domain.DomainServiceFactory;
import com.sap.me.frame.domain.DomainServiceInterface;
import com.sap.me.frame.jdbc.DynamicBatchInsertQuery;
import com.sap.me.frame.jdbc.DynamicQuery;
import com.sap.me.frame.jdbc.DynamicQueryFactory;
import com.sap.me.frame.mapping.data.MapperFactory;
import com.sap.me.frame.transitionutils.Exceptions;
import com.sap.me.frame.web.core.util.WebKeys;
import com.sap.me.globalization.KnownProductGlobalizationServices;
import com.sap.me.status.StatusBOHandle;
import com.sap.me.system.base.ApplicationVO;
import com.sap.me.system.base.BasicApplication;
import com.sap.me.system.common.ApplicationMessage;
import com.visiprise.frame.configuration.ServiceReference;
import com.visiprise.frame.mapping.bean.BeanMappingUtils;
import com.visiprise.globalization.DateGlobalizationServiceInterface;
import com.visiprise.globalization.GlobalizationService;
import com.visiprise.globalization.KnownGlobalizationServices;
import com.visiprise.globalization.ResourceBundleGlobalizationServiceInterface;

/**
 * @author Jun.Liu
 * @Description: 客制化作业维护
 * @date Dec 8, 2016 10:27:45 PM
 */
public class CustomActivityMaintenanceApplicationBean extends BasicApplication implements CustomActivityMaintenanceApplicationInterface {
    private static final long serialVersionUID = 1L;

    // 日志消息ID
    private static final String MESSAGE_ID = "com.hand.ejb.CustomActivityMaintenanceApplicationBean";

    private DateGlobalizationServiceInterface dateService;
    private ResourceBundleGlobalizationServiceInterface lstSrv;
    private ActivityConfigurationServiceInterface activityConfiService;
    private CustomActivityConfigServiceInterface customActivityConfigService;
    private DomainServiceInterface<DocumentDO> documentDOService;

    SystemBase systemBase = null;

    /**
     * 初始化界面
     */
    public ApplicationVO clear(ApplicationVO appVO) throws BasicBOBeanException {
        return new ApplicationVO();
    }

    /**
     * 读取输入的作业信息
     */
    public ApplicationVO retrieve(ApplicationVO appVO) throws BasicBOBeanException {
        Long startTime = System.currentTimeMillis();
        Utils.traceMsg(MESSAGE_ID + " retrieve() start execution ", Utils.DEBUGGING);

        lstSrv = GlobalizationService.getUserService(KnownProductGlobalizationServices.LOCALE_SPECIFIC_TEXT);
        activityConfiService = Services.getService("com.sap.me.activity", "ActivityConfigurationService");

        String activity = appVO.getValue(ACTIVITY);

        // 校验输入值
        if (Utils.isEmpty(activity)) {
            throw new BasicBOBeanException(20002, new Data("NAME", lstSrv.getString("activity.default.LABEL")));
        }

        String description = null;
        List<String> rulesList = new ArrayList<String>();
        List<String> settingList = new ArrayList<String>();

        // 读取客制化表信息
        String comments = getComments(activity);

        try {
            String activityRef = new ActivityBOHandle(activity).getValue();
            // 读取作业信息
            ObjectReference objectRef = new ObjectReference();
            objectRef.setRef(activityRef);
            ActivityFullConfiguration activityFullConfi = activityConfiService.readActivity(objectRef);

            if (activityFullConfi != null) {
                description = activityFullConfi.getDescription();
                List<ActivityOption> activityOptionsList = activityFullConfi.getActivityOptionList();
                if (!Utils.isEmpty(activityOptionsList)) {
                    for (ActivityOption option : activityOptionsList) {
                        rulesList.add(option.getExecUnitOption());
                        settingList.add(option.getSetting());
                    }
                }
            }
        } catch (BusinessException e) {
            Utils.traceMsg(e, Utils.EXCEPTION);
            throw Exceptions.convert(e);
        }

        // 定义返回ApplicationVO
        ApplicationVO retVO = new ApplicationVO();
        retVO.setValue(ACTIVITY, activity);
        retVO.setValue(COMMENTS, comments);
        retVO.setValue(DESCRIPTION, description);
        retVO.setValues(ACTIVITY_RULE, rulesList);
        retVO.setValues(ACTIVITY_SETTTING, settingList);

        Long endTime = System.currentTimeMillis();
        Utils.traceMsg(MESSAGE_ID + " retrieve() end execution ", Utils.DEBUGGING);
        Utils.traceMsg(MESSAGE_ID + " retrieve() execution time in ms " + (endTime - startTime), Utils.DEBUGGING);
        return retVO;
    }

    /**
     * 保存输入的作业信息
     */
    @SuppressWarnings("unchecked")
    public ApplicationVO doSave(ApplicationVO appVO) throws BasicBOBeanException {
        Long startTime = System.currentTimeMillis();
        Utils.traceMsg(MESSAGE_ID + " doSave() start execution ", Utils.DEBUGGING);

        activityConfiService = Services.getService("com.sap.me.activity", "ActivityConfigurationService");

        String activity = appVO.getValue(ACTIVITY);
        String description = appVO.getValue(DESCRIPTION);
        String comments = appVO.getValue(COMMENTS);
        List<String> rulesList = appVO.getValues(ACTIVITY_RULE);
        List<String> settingList = appVO.getValues(ACTIVITY_SETTTING);

        // 保存作业信息
        try {
            String activityRef = new ActivityBOHandle(activity).getValue();
            // 读取作业信息
            ObjectReference objectRef = new ObjectReference();
            objectRef.setRef(activityRef);
            ActivityFullConfiguration activityFullConfi = activityConfiService.readActivity(objectRef);

            // 整理最新的Option信息
            List<ActivityOption> activityOptionsList = new ArrayList<ActivityOption>();
            int ruleCount = Utils.isEmpty(rulesList) ? 0 : rulesList.size();
            for (int i = 0; i < ruleCount; i++) {
                ActivityOption option = new ActivityOption();
                option.setExecUnitOption(rulesList.get(i));
                option.setSetting(settingList.get(i));
                activityOptionsList.add(option);
            }

            // 更新activityFullConfi为最新值
            activityFullConfi.setDescription(description);
            activityFullConfi.setActivityOptionList(activityOptionsList);

            // 保存信息
            activityConfiService.updateActivity(activityFullConfi);
        } catch (BusinessException e) {
            Utils.traceMsg(e, Utils.EXCEPTION);
            throw Exceptions.convert(e);
        }

        // 保存自建表信息
        saveComments(activity, comments);

        // 定义返回ApplicationVO
        ApplicationVO retVO = retrieve(appVO);
        retVO.setApplicationMessage(WebKeys.CONFIRMATION_MESSAGE, new ApplicationMessage("recordChanged.default.MESSAGE"));

        Long endTime = System.currentTimeMillis();
        Utils.traceMsg(MESSAGE_ID + " doSave() end execution ", Utils.DEBUGGING);
        Utils.traceMsg(MESSAGE_ID + " doSave() execution time in ms " + (endTime - startTime), Utils.DEBUGGING);
        return retVO;
    }

    /**
     * 删除自建表中的作业信息
     */
    public ApplicationVO delete(ApplicationVO appVO) throws BasicBOBeanException {
        Long startTime = System.currentTimeMillis();
        Utils.traceMsg(MESSAGE_ID + " delete() start execution ", Utils.DEBUGGING);

        String activity = appVO.getValue(ACTIVITY);
        String comments = appVO.getValue(COMMENTS);

        // 删除自建表信息
        deleteComments(activity, comments);

        // 定义返回ApplicationVO
        ApplicationVO retVO = retrieve(appVO);
        retVO.setApplicationMessage(WebKeys.CONFIRMATION_MESSAGE, new ApplicationMessage("CustomActivity.delete.MESSAGE"));

        Long endTime = System.currentTimeMillis();
        Utils.traceMsg(MESSAGE_ID + " delete() end execution ", Utils.DEBUGGING);
        Utils.traceMsg(MESSAGE_ID + " delete() execution time in ms " + (endTime - startTime), Utils.DEBUGGING);
        return retVO;
    }

    /**
     * 打印
     */
    @Override
    public ApplicationVO print(ApplicationVO appVO) throws BasicBOBeanException {
        customActivityConfigService = Services.getService("com.hand.service", "CustomActivityConfigService");

        String site = appVO.getValue("LOGON_SITE");
        String activityID = appVO.getValue("ACTIVITY_ID");
        String activity = appVO.getValue(ACTIVITY);
        String printer = appVO.getValue(PRINTER);

        try {
            // 获取打印模板
            CustomActivityRuleRequest request = new CustomActivityRuleRequest();
            request.setActivityID(activityID);
            request.setRuleName("DOCUMENT_NAME");
            CustomActivityRuleResponse response = customActivityConfigService.findActivtyOption(request);
            String document = response.getRuleValue();

            // 获取打印模板配置信息
            Data classData = getDocumentClassName(site, document);
            String documentRef = classData.getString("DOCUMENT", null);
            String acquisitionClass = classData.getString("ACQUISITION_CLASS", null);
            String formatterClass = classData.getString("FORMATTER_CLASS", null);
            String transportClass = classData.getString("TRANSPORT_CLASS", null);

            // 步骤Acquire
            Object acquired = acquirePrintingData(acquisitionClass, documentRef, activity);

            // 步骤Format
            Object formatted = formatPrintingData(acquired, formatterClass);

            // 步骤Transport
            FormatPrintingDataRequest formatRequest = BeanMappingUtils.map(acquired, FormatPrintingDataRequest.class);
            transportPrintingData(formatted, transportClass, site, printer, formatRequest.getDocumentConfiguration(), activity);
        } catch (BusinessException e) {
            Utils.traceMsg(e, Utils.EXCEPTION);
            throw Exceptions.convert(e);
        }

        // 重新读取数据以显示
        ApplicationVO retVO = retrieve(appVO);
        retVO.setApplicationMessage(WebKeys.INFORMATION_MESSAGE, new ApplicationMessage("Z_SY030.print.MESSAGE"));

        return retVO;
    }

    /**
     * 读取客制化表信息
     */
    private String getComments(String activity) {
        systemBase = new SystemBase();
        systemBase.init(JNDIUtils.DEFAULT_DATASOURCE, false);
        DynamicQuery dq = DynamicQueryFactory.newInstance();

        String activityRef = new ActivityBOHandle(activity).getValue();
        dq.clear();
        dq.append("SELECT COMMENTS FROM Z_ACTIVITY");
        dq.append(" WHERE HANDLE = ").appendString(activityRef);
        Data resultData = systemBase.executeQuery(dq);
        if (!Utils.isEmpty(resultData)) {
            return resultData.getString("COMMENTS", "", 0);
        }

        return null;
    }

    /**
     * 保存自建表信息
     */
    private void saveComments(String activity, String comments) {
        dateService = (DateGlobalizationServiceInterface) GlobalizationService.getUserService(KnownGlobalizationServices.DATE);

        systemBase = new SystemBase();
        systemBase.init(JNDIUtils.DEFAULT_DATASOURCE, false);
        DynamicBatchInsertQuery di;

        // Delete
        deleteComments(activity, comments);

        // Insert
        String activityRef = new ActivityBOHandle(activity).getValue();
        di = DynamicQueryFactory.insertBatchInstance("Z_ACTIVITY", systemBase.getDBConnection(), 100);
        di.insertColumn("HANDLE", activityRef);
        di.insertColumn("COMMENTS", comments);
        di.insertColumn("MODIFIED_DATE_TIME", dateService.createDateTime());
        systemBase.executeBatchUpdate(di);
        di.flush();
    }

    /**
     * 删除自建表信息
     */
    private void deleteComments(String activity, String comments) {
        systemBase = new SystemBase();
        systemBase.init(JNDIUtils.DEFAULT_DATASOURCE, false);
        DynamicQuery dq = DynamicQueryFactory.newInstance();

        String activityRef = new ActivityBOHandle(activity).getValue();

        // Delete
        dq.clear();
        dq.append("DELETE FROM Z_ACTIVITY WHERE HANDLE = ").appendString(activityRef);
        systemBase.executeUpdate(dq);
        dq.flush();
    }

    /**
     * 获取打印模板配置信息
     */
    private Data getDocumentClassName(String site, String document) {
        documentDOService = DomainServiceFactory.getServiceByClass(DocumentDO.class);

        Data classData = new Data();

        DocumentDO documentDO = new DocumentDO();
        documentDO.setSite(site);
        documentDO.setDocument(document);
        documentDO.setStatusRef(new StatusBOHandle(site, "101").getValue());
        documentDO.setCurrentRevision(true);
        Collection<DocumentDO> documentDOList = documentDOService.readByExample(documentDO);
        if (documentDOList != null && documentDOList.size() > 0) {
            for (DocumentDO d : documentDOList) {
                classData.put("DOCUMENT", d.getId());
                classData.put("ACQUISITION_CLASS", d.getDataAcquisitionClass());
                classData.put("FORMATTER_CLASS", d.getFormatterClass());
                classData.put("TRANSPORT_CLASS", d.getTransportClass());
                classData.put("DOCUMENT_TYPE_BO", d.getDocumentTypeRef());
            }
        }

        return classData;
    }

    /**
     * 执行acquisitionClass对应的Class文件
     */
    private Object acquirePrintingData(String acquisitionClass, String documentRef, String printByGbo) throws BasicBOBeanException {
        Object acquired = null;

        try {
            if (acquisitionClass != null && !acquisitionClass.equals("")) {
                if (ServiceReference.isValidFQName(acquisitionClass)) {
                    Object service = ServiceLocator.getService(acquisitionClass);
                    PrintingDataAcquisitionRequest acquisitionRequest = new PrintingDataAcquisitionRequest();

                    acquisitionRequest.setDocumentRef(documentRef);
                    acquisitionRequest.setPrintByGbo(printByGbo);
                    acquired = ((PrintingDataAcquisitionServiceInterface) service).acquirePrintingData(acquisitionRequest);
                }
            }
        } catch (BusinessException e) {
            Utils.traceMsg(e, Utils.EXCEPTION);
            throw Exceptions.convert(e);
        }

        if (!(acquired instanceof PrintingDataAcquisitionResponse))
            throw new IllegalArgumentException("PrintingFormatServiceInterface can only be used with PrintingDataAcquisitionServiceInterface");

        return acquired;
    }

    /**
     * 执行formatterClass对应的Class文件
     */
    private Object formatPrintingData(Object acquired, String formatterClass) throws BasicBOBeanException {
        Object formatted = null;

        try {
            if (formatterClass != null && !formatterClass.equals("")) {
                if (ServiceReference.isValidFQName(formatterClass)) {
                    Object service = ServiceLocator.getService(formatterClass);
                    if (service instanceof PrintingFormatServiceInterface) {
                        FormatPrintingDataRequest formatRequest = BeanMappingUtils.map(acquired, FormatPrintingDataRequest.class);
                        formatted = ((PrintingFormatServiceInterface) service).formatPrintingData(formatRequest);
                    }
                }
            }
        } catch (BusinessException e) {
            Utils.traceMsg(e, Utils.EXCEPTION);
            throw Exceptions.convert(e);
        }

        return formatted;
    }

    /**
     * 执行transportClass对应的Class文件
     */
    private void transportPrintingData(Object formatted, String transportClass, String site, String printerName, DocumentConfiguration docConfi, String printByGbo) throws BasicBOBeanException {
        try {
            if (transportClass != null && !transportClass.equals("")) {
                if (ServiceReference.isValidFQName(transportClass)) {
                    Object service = ServiceLocator.getService(transportClass);
                    if (service instanceof PrintingTransportServiceInterface) {
                        if (!(formatted instanceof FormatPrintingDataResponse))
                            throw new IllegalArgumentException("PrintingTransportServiceInterface can only be used with PrintingFormatServiceInterface");

                        TransportPrintingDataRequest transportRequest = BeanMappingUtils.map(formatted, TransportPrintingDataRequest.class);
                        String printerRefForTransporter = null;
                        if (printerName != null) {
                            printerRefForTransporter = new PrinterBOHandle(site, printerName).toString();
                        }

                        transportRequest.setDocumentConfiguration(docConfi);
                        transportRequest.setPrintByGbo(printByGbo);
                        transportRequest.setPrinterRef(printerRefForTransporter);
                        TransportPrintingDataResponse transportResponse = ((PrintingTransportServiceInterface) service).transportPrintingData(transportRequest);
                        MapperFactory.mapToData(transportResponse);
                    }
                }
            }
        } catch (BusinessException e) {
            Utils.traceMsg(e, Utils.EXCEPTION);
            throw Exceptions.convert(e);
        }
    }
}
