package com.hand.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Jun.Liu
 * @Description: CustomActivityConfigWebService.findActivtyOption()传出参数
 * @date Dec 16, 2016 2:25:14 PM
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "ruleValue", "message" })
public class CustomActivityRuleWSResponse {
    // 作业规则值
    @XmlElement
    protected String ruleValue;
    // 消息内容
    @XmlElement
    protected String message;

    public String getRuleValue() {
        return ruleValue;
    }

    public void setRuleValue(String ruleValue) {
        this.ruleValue = ruleValue;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
