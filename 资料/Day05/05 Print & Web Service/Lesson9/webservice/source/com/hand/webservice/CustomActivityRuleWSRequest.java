package com.hand.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Jun.Liu
 * @Description: CustomActivityConfigWebService.findActivtyOption()传入参数
 * @date Dec 16, 2016 2:25:14 PM
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "site", "activityID", "ruleName" })
public class CustomActivityRuleWSRequest {
    // 作业名称
    @XmlElement
    protected String site;
    // 作业名称
    @XmlElement
    protected String activityID;
    // 作业规则名称
    @XmlElement
    protected String ruleName;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getActivityID() {
        return activityID;
    }

    public void setActivityID(String activityID) {
        this.activityID = activityID;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }
}
