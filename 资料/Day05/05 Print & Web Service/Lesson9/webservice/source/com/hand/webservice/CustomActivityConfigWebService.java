package com.hand.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.hand.service.CustomActivityRuleRequest;
import com.hand.service.CustomActivityRuleResponse;
import com.hand.service.impl.CustomActivityConfigServiceInterface;
import com.sap.engine.services.webservices.espbase.configuration.ann.dt.AuthenticationDT;
import com.sap.engine.services.webservices.espbase.configuration.ann.dt.AuthenticationEnumsAuthenticationLevel;
import com.sap.engine.services.webservices.espbase.configuration.ann.rt.AuthenticationRT;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;

/**
 * @author Jun.Liu
 * @Description: 客制化Web Service CustomActivityConfigService
 * @date Dec 16, 2016 2:21:54 PM
 */
@AuthenticationDT(authenticationLevel = AuthenticationEnumsAuthenticationLevel.BASIC)
@AuthenticationRT(AuthenticationMethod = "sapsp:HTTPBasic")
@WebService
public class CustomActivityConfigWebService {
    private CustomActivityConfigServiceInterface customActivityConfigService;

    @WebMethod
    public CustomActivityRuleWSResponse findActivtyOption(@WebParam(name = "CustomWSRequest") CustomActivityRuleWSRequest request) {
        customActivityConfigService = Services.getService("com.hand.service", "CustomActivityConfigService", request.getSite());

        CustomActivityRuleWSResponse response = new CustomActivityRuleWSResponse();

        String activityID = request.getActivityID();
        String ruleName = request.getRuleName();
        try {

            CustomActivityRuleRequest cusRequest = new CustomActivityRuleRequest();
            cusRequest.setActivityID(activityID);
            cusRequest.setRuleName(ruleName);
            CustomActivityRuleResponse cusResponse = customActivityConfigService.findActivtyOption(cusRequest);
            if (cusResponse != null) {
                response.setRuleValue(cusResponse.getRuleValue());
            }
        } catch (BusinessException e) {
            response.setMessage(e.getLocalizedMessage());
        }

        return response;
    }
}
