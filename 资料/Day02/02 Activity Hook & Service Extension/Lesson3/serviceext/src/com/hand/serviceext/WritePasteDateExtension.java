package com.hand.serviceext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.sap.me.activity.ActivityOptionField;
import com.sap.me.activity.ServiceExtension;
import com.sap.me.extension.Services;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.production.CollectSfcDataRequest;
import com.sap.me.production.CompleteSfcBasicRequest;
import com.sap.me.production.CompleteSfcBatchRequest;
import com.sap.me.production.SfcDataField;
import com.sap.me.production.SfcDataServiceInterface;
import com.visiprise.frame.service.ext.InvocationContext;
import com.visiprise.frame.service.ext.InvocationContextSetter;
import com.visiprise.frame.service.ext.TransactionContextInterface;
import com.visiprise.frame.service.ext.TransactionContextSetter;
import com.visiprise.globalization.DateGlobalizationServiceInterface;
import com.visiprise.globalization.GlobalizationService;
import com.visiprise.globalization.KnownGlobalizationServices;

/**
 * @author Jun.Liu
 * @Description: SFC完成时自动写入数据字段COMMENTS和PASTE_DATE. 其中COMMENTS为固定值"Write Paste Date Activity Hook", PASTE_DATE为当前时间加上Activity Rule:EXP_TIME中维护的时长.
 * @date Dec 13, 2016 2:13:55 PM
 */
public class WritePasteDateExtension extends ServiceExtension<Object> implements InvocationContextSetter, TransactionContextSetter {
    private static final long serialVersionUID = 1L;

    private InvocationContext invocationContext;
    private TransactionContextInterface transactionContext;

    private SfcDataServiceInterface sfcDataService;
    private DateGlobalizationServiceInterface dateSrv;

    private BigDecimal expTime = new BigDecimal(5);

    @Override
    public void execute(Object dto) throws Exception {
        if (dto instanceof CompleteSfcBatchRequest) {
            CompleteSfcBatchRequest pdto = (CompleteSfcBatchRequest) dto;
            executePostComplete(pdto);
        }
    }

    private void executePostComplete(CompleteSfcBatchRequest pdto) throws BusinessException {
        sfcDataService = Services.getService("com.sap.me.production", "SfcDataService");
        dateSrv = (DateGlobalizationServiceInterface) GlobalizationService.getInvariantService(KnownGlobalizationServices.DATE);
		
		//API传入参数的变量
        CollectSfcDataRequest collectSfcData = new CollectSfcDataRequest();

        // 待处理的SFC列表
        List<String> sfcList = new ArrayList<String>();
        List<CompleteSfcBasicRequest> completeSfcList = pdto.getCompleteSfcList();
        for (CompleteSfcBasicRequest value : completeSfcList) {
            String sfcRef = value.getSfcRef();
            sfcList.add(sfcRef);
        }
        collectSfcData.setSfcList(sfcList);

        List<SfcDataField> sfcDataFieldList = new ArrayList<SfcDataField>();
        // 参数PASTE_DATE
        SfcDataField sfcPasteDataField = new SfcDataField();
        sfcPasteDataField.setAttribute("PASTE_DATE");
        long expTimeLong = dateSrv.createDateTime().getTimeInMillis() + (expTime.longValue() * 3600 * 1000);
        sfcPasteDataField.setValue(String.valueOf(expTimeLong));
        sfcDataFieldList.add(sfcPasteDataField);

        // 参数COMMENTS
        SfcDataField sfcCommentsDataField = new SfcDataField();
        sfcCommentsDataField.setAttribute("COMMENTS");
        sfcCommentsDataField.setValue("Write Paste Date Activity Hook");
        sfcDataFieldList.add(sfcCommentsDataField);

        collectSfcData.setSfcDataFieldList(sfcDataFieldList);

        sfcDataService.collectSfcData(collectSfcData);
    }

    public BigDecimal getExpTime() {
        return expTime;
    }

    @ActivityOptionField
    public void setExpTime(BigDecimal expTime) {
        this.expTime = expTime;
    }

    @Override
    public void setTransactionContext(TransactionContextInterface transactionContext) {
        this.transactionContext = transactionContext;
    }

    @Override
    public void setInvocationContext(InvocationContext context) {
        this.invocationContext = context;
    }

    public InvocationContext getInvocationContext() {
        return invocationContext;
    }

    public TransactionContextInterface getTransactionContext() {
        return transactionContext;
    }
}
