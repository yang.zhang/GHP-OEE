package com.hand.hook;

import java.util.ArrayList;
import java.util.List;

import com.hand.hook.exception.ActivityRuleIsEmptyException;
import com.sap.me.activity.ActivityOption;
import com.sap.me.activity.HookContextInterface;
import com.sap.me.activity.HookContextSetter;
import com.sap.me.extension.Services;
import com.sap.me.frame.Utils;
import com.sap.me.production.CollectSfcDataRequest;
import com.sap.me.production.PostCompleteHookDTO;
import com.sap.me.production.SfcDataField;
import com.sap.me.production.SfcDataServiceInterface;
import com.visiprise.frame.service.ext.ActivityInterface;
import com.visiprise.globalization.DateGlobalizationServiceInterface;
import com.visiprise.globalization.GlobalizationService;
import com.visiprise.globalization.KnownGlobalizationServices;

/**
 * @author Jun.Liu
 * @Description: SFC完成时自动写入数据字段COMMENTS和PASTE_DATE. 其中COMMENTS为固定值"Write Paste Date Activity Hook", PASTE_DATE为当前时间加上Activity Rule:EXP_TIME中维护的时长.
 * @date Dec 6, 2016 2:55:24 PM
 */
public class WritePasteDateHook implements ActivityInterface<PostCompleteHookDTO>, HookContextSetter {
    private static final long serialVersionUID = 1L;
    private static final String EXP_TIME = "EXP_TIME";

    private SfcDataServiceInterface sfcDataService;
    private DateGlobalizationServiceInterface dateSrv;

    private HookContextInterface hookContext;

    @Override
    public void execute(PostCompleteHookDTO dto) throws Exception {
        if (dto instanceof PostCompleteHookDTO) {
            PostCompleteHookDTO pdto = (PostCompleteHookDTO) dto;
            executePostComplete(pdto);
        }
    }

    private void executePostComplete(PostCompleteHookDTO dto) throws Exception {
        sfcDataService = Services.getService("com.sap.me.production", "SfcDataService");
        dateSrv = (DateGlobalizationServiceInterface) GlobalizationService.getInvariantService(KnownGlobalizationServices.DATE);

        // 获取作业规则EXP_TIME的维护值并检查
        String expTime = getActivityRule();
        if (Utils.isEmpty(expTime) || (!Utils.isEmpty(expTime) && !checkIsNumber(expTime))) {
            throw new ActivityRuleIsEmptyException(dto.getActivity(), EXP_TIME);
        }
		////API传入参数的变量
        CollectSfcDataRequest collectSfcData = new CollectSfcDataRequest();

        // 待处理的SFC列表
		/**
			获取SFC号
			
		**/
        List<String> sfcList = new ArrayList<String>();
		//sfc编号
        sfcList.add(dto.getSfcBO().getValue());
		
        collectSfcData.setSfcList(sfcList);

        List<SfcDataField> sfcDataFieldList = new ArrayList<SfcDataField>();
        // 参数PASTE_DATE
        SfcDataField sfcPasteDataField = new SfcDataField();
        sfcPasteDataField.setAttribute("PASTE_DATE");
        long expTimeLong = dateSrv.createDateTime().getTimeInMillis() + (Long.parseLong(expTime) * 3600 * 1000);
        sfcPasteDataField.setValue(String.valueOf(expTimeLong));
        sfcDataFieldList.add(sfcPasteDataField);
        // 参数COMMENTS
        SfcDataField sfcCommentsDataField = new SfcDataField();
        sfcCommentsDataField.setAttribute("COMMENTS");
        sfcCommentsDataField.setValue("Write Paste Date Activity Hook");
        sfcDataFieldList.add(sfcCommentsDataField);

        collectSfcData.setSfcDataFieldList(sfcDataFieldList);

        sfcDataService.collectSfcData(collectSfcData);
    }

    @Override
    public void setHookContext(HookContextInterface hookContext) {
        this.hookContext = hookContext;
    }

    /**
     * @Description: 获取作业规则值
     */
    private String getActivityRule() {
        List<ActivityOption> activityOptions = hookContext.getActivityOptions();
        if (activityOptions == null) {
            return null;
        }

        for (ActivityOption activityOption : activityOptions) {
            String option = activityOption.getExecUnitOption();
            if (option != null && option.trim().equals(EXP_TIME.trim())) {
                return activityOption.getSetting();
            }
        }

        return null;
    }

    /**
     * @Description: 检查输入值是否为数字
     */
    private boolean checkIsNumber(String value) {
        if (Utils.isEmpty(value))
            return false;

        return value.matches("^[0-9]\\d*$");
    }
}
