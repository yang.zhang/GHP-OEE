package com.hand.hook.exception;

import com.sap.me.frame.domain.BusinessException;

/**
 * @author Jun.Liu
 * @Description: 作业 %ACTIVITY_ID% 的规则 %RULE_NAME% 为空或无效
 * @date Dec 6, 2016 2:55:24 PM
 */
public class ActivityRuleIsEmptyException extends BusinessException {
    private static final long serialVersionUID = 1L;

    private final static int ERROR_CODE = 20001;
    private String activityID;
    private String ruleName;

    public ActivityRuleIsEmptyException(String activityID, String ruleName) {
        super(ERROR_CODE);
        this.activityID = activityID;
        this.ruleName = ruleName;
    }

    public String getActivityID() {
        return activityID;
    }

    public String getRuleName() {
        return ruleName;
    }
}
