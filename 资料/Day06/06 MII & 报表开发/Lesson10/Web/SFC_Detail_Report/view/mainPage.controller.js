jQuery.sap.require('sap.ui.sfcdetail.model.miiData');
jQuery.sap.require("sap.ui.core.util.Export");
jQuery.sap.require("sap.ui.core.util.ExportTypeCSV");
jQuery.sap.require("jquery.sap.storage");
sap.ui.controller("view.mainPage", {

    /**
     * Called when a controller is instantiated and its View controls (if available) are already created.
     * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
     * @memberOf mainPage
     */
    onInit: function() {
        sap.ui.getCore().byId('tf-site').setValue(jQuery.sap.getUriParameters().get("Site"));
    },

    toUpperCase: function(oEvent) {
        var oTextField = oEvent.getSource();
        oTextField.setValue(oTextField.getValue().toUpperCase());
    },

    getTableColumn: function(oController) {
        // Extension part for Detail Info. Table
        var oPanelMatrix = new sap.ui.commons.layout.MatrixLayout({
            layoutFixed: true,
            width: '100%',
            columns: 2,
            rows: [new sap.ui.commons.layout.MatrixLayoutRow({
                height: '25px',
                width: '100%',
                cells: [
                    new sap.ui.commons.layout.MatrixLayoutCell({
                        hAlign: sap.ui.commons.layout.HAlign.Begin,
                        content: [
                            new sap.ui.commons.Label({
                                text: "{i18n>oPanelResult_oRowCountLabel}",
                                requiredAtBegin: true,
                                textAlign: "Right"
                            }),
                            new sap.ui.commons.TextView({
                                id: "tv-rowcount",
                                text: "0",
                                design: sap.ui.commons.LabelDesign.Bold
                            })
                        ]
                    }),
                    new sap.ui.commons.layout.MatrixLayoutCell({
                        hAlign: "End",
                        content: [
                            new sap.ui.commons.Button({
                                text: "{i18n>oPanelResult_oExportBtn}",
                                icon: "sap-icon://excel-attachment",
                                style: sap.ui.commons.ButtonStyle.Emph,
                                press: this.exportResult
                            })
                        ]
                    })
                ]
            })]
        });

        var oTable1 = new sap.ui.table.Table('dt-result-datatable', {
            selectionMode: sap.ui.table.SelectionMode.Single,
            fixedColumnCount: 1,
            visibleRowCount: 18,
            extension: [
                new sap.ui.commons.layout.HorizontalLayout({
                    content: [
                        oPanelMatrix
                    ]
                })
            ]
        });

        return sap.ui.controller("view.mainPage").addColumn(oTable1);
    },

    addColumn: function(oTable1) {
        oTable1.addColumn(new sap.ui.table.Column({
            label: new sap.ui.commons.Label({text: "{i18n>text_sfc}"}),
            template: new sap.ui.commons.TextView().bindProperty("text", "SFC"),
            sortProperty: "SFC",
            filterProperty: "SFC",
            width: '150px'
        }));

        oTable1.addColumn(new sap.ui.table.Column({
            label: new sap.ui.commons.Label({text: "{i18n>text_shoporder}"}),
            template: new sap.ui.commons.TextView().bindProperty("text", "SHOP_ORDER"),
            sortProperty: "SHOP_ORDER",
            filterProperty: "SHOP_ORDER",
            width: '150px'
        }));

        oTable1.addColumn(new sap.ui.table.Column({
            label: new sap.ui.commons.Label({text: "{i18n>text_item}"}),
            template: new sap.ui.commons.TextView().bindProperty("text", "ITEM"),
            sortProperty: "ITEM",
            filterProperty: "ITEM",
            width: '150px'
        }));

        oTable1.addColumn(new sap.ui.table.Column({
            label: new sap.ui.commons.Label({text: "{i18n>text_operation}"}),
            template: new sap.ui.commons.TextView().bindProperty("text", "OPERATION"),
            sortProperty: "OPERATION",
            filterProperty: "OPERATION",
            width: '150px'
        }));

        oTable1.addColumn(new sap.ui.table.Column({
            label: new sap.ui.commons.Label({text: "{i18n>text_qty}"}),
            template: new sap.ui.commons.TextView().bindProperty("text", "QTY"),
            sortProperty: "QTY",
            filterProperty: "QTY",
            width: '100px'
        }));

        oTable1.addColumn(new sap.ui.table.Column({
            label: new sap.ui.commons.Label({text: "{i18n>text_qty_done}"}),
            template: new sap.ui.commons.TextView().bindProperty("text", "QTY_DONE"),
            sortProperty: "QTY_DONE",
            filterProperty: "QTY_DONE",
            width: '100px'
        }));

        return oTable1;
    },

    getRowCount: function(oModel) {
        if(undefined == oModel.oData.Rowsets.Rowset || undefined == oModel.oData.Rowsets.Rowset[0].Row)
            return 0;
        else
            return oModel.oData.Rowsets.Rowset[0].Row.length;
    },

    createDataGrid: function(oEvent) {
        var oTable = sap.ui.getCore().byId('dt-result-datatable');
        var site = sap.ui.getCore().byId('tf-site').getValue();
        var sfc = sap.ui.getCore().byId('vh-sfc').getValue();

        var oModel = sap.ui.sfcdetail.model.miiData.getSearchResults(site, sfc);

        oTable.setModel(oModel);
        oTable.bindRows('/Rowsets/Rowset/0/Row');
        var rowCount = sap.ui.controller("view.mainPage").getRowCount(oModel);
        sap.ui.getCore().byId('tv-rowcount').setText(rowCount);
    },

    exportResult: function(oEvent) {
        var oExport = new sap.ui.core.util.Export({
            exportType: new sap.ui.core.util.ExportTypeCSV({
                separatorChar: ","
            }),
            models: sap.ui.getCore().byId('dt-result-datatable').getModel(),
            rows: {
                path: "/Rowsets/Rowset/0/Row"
            }
        });

        oExport.addColumn(new sap.ui.core.util.ExportColumn({
            name: "{i18n>text_sfc}",
            template: {
                content: "{SFC}"
            }
        }));
        oExport.addColumn(new sap.ui.core.util.ExportColumn({
            name: "{i18n>text_shoporder}",
            template: {
                content: "{SHOP_ORDER}"
            }
        }));
        oExport.addColumn(new sap.ui.core.util.ExportColumn({
            name: "{i18n>text_item}",
            template: {
                content: "{ITEM}"
            }
        }));
        oExport.addColumn(new sap.ui.core.util.ExportColumn({
            name: "{i18n>text_operation}",
            template: {
                content: "{OPERATION}"
            }
        }));
        oExport.addColumn(new sap.ui.core.util.ExportColumn({
            name: "{i18n>text_qty}",
            template: {
                content: "{QTY}"
            }
        }));
        oExport.addColumn(new sap.ui.core.util.ExportColumn({
            name: "{i18n>text_qty_done}",
            template: {
                content: "{QTY_DONE}"
            }
        }));

        oExport.setModel(oi18nModel, "i18n");
        oExport.saveFile().always(function () {
            this.destroy();
        })
    },
});