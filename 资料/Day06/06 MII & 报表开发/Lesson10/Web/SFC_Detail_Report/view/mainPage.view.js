jQuery.sap.require('sap.ui.sfcdetail.model.miiData');
jQuery.sap.require("sap.ui.core.format.DateFormat");

sap.ui.jsview("view.mainPage", {

    /** Specifies the Controller belonging to this View.
     * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
     * @memberOf mainPage
     */
    getControllerName: function() {
        return "view.mainPage";
    },

    createSearchPanel: function(oController, oLayout) {
        /*
         * Name : Search Panel */
        var oPanelSearch = new sap.ui.commons.Panel('oPanelSearch', {
            width: "100%",
            showCollapseIcon: true,
            areaDesign: sap.ui.commons.enums.AreaDesign.Plain,
            title: new sap.ui.core.Title({
                text: "{i18n>oPanelSearchTitle}",
                icon: "sap-icon://search"
            })
        });

        /*
         * Name : Site Selection - SingleSelection */
        var oCellSiteLabel = new sap.ui.commons.layout.MatrixLayoutCell({
            id: 'oCellSiteLabel',
            hAlign: sap.ui.commons.layout.HAlign.Right,
            content: [
                new sap.ui.commons.Label({
                    id: "tfl-site",
                    text : "{i18n>oPanelSearch_oSiteLabel}",
                    required: true,
                    requiredAtBegin: true
                })
            ]
        });
        var oCellSite = new sap.ui.commons.layout.MatrixLayoutCell({
            id: 'oCellSite',
            hAlign: sap.ui.commons.layout.HAlign.Left,
            content: [
                new sap.ui.commons.TextField({
                    id: "tf-site",
                    editable: false,
                    required: true
                })
            ]
        });

        /*
         * Name : SFC Selection - SingleSelection */
        var oCellSfcLabel = new sap.ui.commons.layout.MatrixLayoutCell({
            id: 'oCellSfcLabel',
            hAlign: sap.ui.commons.layout.HAlign.Right,
            content: [
                new sap.ui.commons.Label({
                    id: "vhl-sfc",
                    text: "{i18n>oPanelSearch_oSfcLabel}",
                    requiredAtBegin: true
                })
            ]
        });
        var oCellSfc = new sap.ui.commons.layout.MatrixLayoutCell({
            id: 'oCellSfc',
            hAlign: sap.ui.commons.layout.HAlign.Left,
            content: [
                new sap.ui.commons.ValueHelpField({
                    id: "vh-sfc",
                    editable: true,
                    value: '',
                    tooltip: "{i18n>oPanelSearch_oSfcValueHelpTip}",
                    change: oController.toUpperCase
                })
            ]
        });

        /*
         * Name : Search - Button */
        var oCellSearch = new sap.ui.commons.layout.MatrixLayoutCell({
            id: "oCellSearch",
            hAlign: sap.ui.commons.layout.HAlign.End,
            content: [
                new sap.ui.commons.Button({
                    text: "{i18n>oPanelSearch_oSearchBtn}",
                    icon: 'sap-icon://search',
                    style: sap.ui.commons.ButtonStyle.Emph,
                    press: function () {
                        sap.ui.getCore().byId("oLayout1").setBusyIndicatorDelay(0).setBusy(true);
                        jQuery.sap.delayedCall(0, this, function() {
                            oController.createDataGrid();
                            sap.ui.getCore().byId("oLayout1").setBusyIndicatorDelay(0).setBusy(false);
                        });
                    }
                }),
                new sap.ui.commons.Label({
                    text: "",
                    width: "20px",
                    id: "tfl-vertical",
                    textAlign: "Right"
                })
            ]
        });

        var oPanelMatrix = new sap.ui.commons.layout.MatrixLayout({
            layoutFixed : true,
            width : '100%',
            columns : 5
        });

        oPanelMatrix.createRow(oCellSiteLabel, oCellSite);
        oPanelMatrix.createRow(oCellSfcLabel, oCellSfc, oCellSearch, "", "");
        oPanelSearch.addContent(oPanelMatrix);
        oLayout.addContent(oPanelSearch);
    },

    createResultGridPanel: function(oController){
        var oPanelResult = new sap.ui.commons.Panel('oPanelResult', {
            width: "100%",
            showCollapseIcon: false
        });
        oPanelResult.setAreaDesign(sap.ui.commons.enums.AreaDesign.Plain);
        oPanelResult.setTitle(new sap.ui.core.Title({
            text: "{i18n>oPanelResultTitle}",
            icon: "sap-icon://business-objects-experience"
        }));

        oPanelResult.addContent(oController.getTableColumn(oController));
        return oPanelResult;
    },

    /** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
     * Since the Controller is given to this method, its event handlers can be attached right away.
     * @memberOf mainPage
     */
    createContent: function(oController) {
        var oLayout1 = new sap.ui.layout.VerticalLayout("oLayout1", {
            width: "100%",
            height: "100%"
        });

        this.createSearchPanel(oController, oLayout1);
        oLayout1.addContent(this.createResultGridPanel(oController));
        oLayout1.setModel(oi18nModel, "i18n");

        return oLayout1;
    },
});
