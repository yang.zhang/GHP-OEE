jQuery.sap.registerModulePath('sap.ui.sfcdetail.model', './model');

var oi18nModel = new sap.ui.model.resource.ResourceModel({
	bundleUrl : "./i18n/i18n.properties",
	bundleLocale : sap.ui.getCore().getConfiguration().getLanguage()
});