jQuery.sap.declare("sap.ui.sfcdetail.model.miiData");
sap.ui.sfcdetail.model.miiData = {};

sap.ui.sfcdetail.model.miiData.getSearchResults = function(site, sfc){
    var oUrl = '/XMII/Illuminator?QueryTemplate=TRAINING/SFC_Detail_Report/getSfcDetailQry&Content-Type=text/json';
    var oModel = new sap.ui.model.json.JSONModel();
    oModel.forceNoCache(true);
    oModel.setSizeLimit(99999);
    oModel.setDefaultBindingMode(sap.ui.model.BindingMode.OneWay);
    oModel.loadData(oUrl, {
        'Param.1' : site,
        'Param.2' : sfc
    }, false,"POST");
    return oModel;
};