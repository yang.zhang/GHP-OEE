package com.hand.wpmf.web.podplugin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jun.Liu
 * @Description: CustomActivityMaintenancePlugin页面字段定义
 * @date Dec 12, 2016 11:23:00 PM
 */
public class CustomActivityMaintenanceBasicModel implements Serializable {

    private static final long serialVersionUID = 1L;
    // 作业
    private String activity;
    // 备注
    private String comments;
    // 表格信息
    private List<CustomActivityRuleItem> ruleList;
    // 保存&删除&清空按钮是否可用
    private boolean btnDisabled;

    public CustomActivityMaintenanceBasicModel(String site) {
        btnDisabled = true;
        setRuleList(new ArrayList<CustomActivityRuleItem>());
    }

    public void clear() {
        setActivity("");
        setComments("");
        setRuleList(new ArrayList<CustomActivityRuleItem>());
        setBtnDisabled(true);
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public List<CustomActivityRuleItem> getRuleList() {
        return ruleList;
    }

    public void setRuleList(List<CustomActivityRuleItem> ruleList) {
        this.ruleList = ruleList;
    }

    public boolean isBtnDisabled() {
        return btnDisabled;
    }

    public void setBtnDisabled(boolean btnDisabled) {
        this.btnDisabled = btnDisabled;
    }
}
