package com.hand.wpmf.web.podplugin;

import java.util.List;

import com.sap.me.frame.domain.BusinessException;

/**
 * @author Jun.Liu
 * @Description: CustomActivityMaintenanceService接口
 * @date Dec 13, 2016 12:01:50 AM
 */
public interface CustomActivityMaintenancePluginServiceInterface {

    /**
     * 客制化作业字段搜索帮助
     */
    public List<CustomActivityMaintenanceItem> findActivitysData(CustomActivityMaintenanceRequest request) throws BusinessException;

    /**
     * 查询功能
     */
    public void retrieve(CustomActivityMaintenanceBasicModel basicModel) throws BusinessException;

    /**
     * 保存功能
     */
    public void update(CustomActivityMaintenanceBasicModel basicModel) throws BusinessException;

    /**
     * 删除功能
     */
    public void delete(CustomActivityMaintenanceBasicModel basicModel) throws BusinessException;
}
