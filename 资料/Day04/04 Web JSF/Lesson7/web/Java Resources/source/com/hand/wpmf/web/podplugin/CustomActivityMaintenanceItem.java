package com.hand.wpmf.web.podplugin;

/**
 * @author Jun.Liu
 * @Description: 搜索帮助传出参数
 * @date Dec 13, 2016 12:07:21 AM
 */
public class CustomActivityMaintenanceItem {
    private String activity;
    private String description;

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
