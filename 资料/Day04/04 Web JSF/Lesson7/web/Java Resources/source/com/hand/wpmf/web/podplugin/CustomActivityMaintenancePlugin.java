package com.hand.wpmf.web.podplugin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;

import com.sap.me.common.AttributeValue;
import com.sap.me.extension.Services;
import com.sap.me.frame.Utils;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.globalization.KnownProductGlobalizationServices;
import com.sap.me.production.podclient.BasePodPlugin;
import com.sap.me.wpmf.ConfirmDialogOpenEvent;
import com.sap.me.wpmf.DeviceConfigurationInterface;
import com.sap.me.wpmf.GlobalDialogOpenListenerInterface;
import com.sap.me.wpmf.MessageType;
import com.sap.me.wpmf.PluginEventManagerInterface;
import com.sap.me.wpmf.PodDeviceSizeEnum;
import com.sap.me.wpmf.PodDeviceTypeEnum;
import com.sap.me.wpmf.PodPanelTypeEnum;
import com.sap.me.wpmf.TableColumnSortListener;
import com.sap.me.wpmf.TableConfigurator;
import com.sap.me.wpmf.TableSortComparator;
import com.sap.me.wpmf.browse.BrowseActionType;
import com.sap.me.wpmf.browse.BrowseDimension;
import com.sap.me.wpmf.util.ExceptionHandler;
import com.sap.me.wpmf.util.FacesUtility;
import com.sap.me.wpmf.util.MessageHandler;
import com.sap.ui.faces.component.sap.UIColumn;
import com.sap.ui.faces.component.sap.UICommandInputText;
import com.sap.ui.faces.event.sap.SortActionEvent;
import com.visiprise.globalization.GlobalizationService;
import com.visiprise.globalization.ResourceBundleGlobalizationServiceInterface;

/**
 * @author Jun.Liu
 * @Description: 客制化作业维护POD页面
 * @date Dec 12, 2016 11:21:42 PM
 */
public class CustomActivityMaintenancePlugin extends BasePodPlugin implements Serializable, TableColumnSortListener, ActionListener {

    private static final long serialVersionUID = 1L;

    // Web字段集合
    private CustomActivityMaintenanceBasicModel basicModel;
    // 表格
    private TableConfigurator tableConfig = null;
    private UIColumn currentSortColumn = null;

    // 页面名称
    private static final String POPUP_TITLE_TEXT = "Z_SY030_PLUGIN.title.TEXT";
    // 在线帮助名称
    private static final String HELP_RESOURCE_KEY = "SY030.default.HELP";

    // API声明
    private ResourceBundleGlobalizationServiceInterface textService;
    private CustomActivityMaintenancePluginServiceInterface customService;

    // 最后一次操作缓存
    private String lastOperation;

    public CustomActivityMaintenancePlugin() {
        super();
        basicModel = new CustomActivityMaintenanceBasicModel(getSite());
        textService = GlobalizationService.getUserService(KnownProductGlobalizationServices.LOCALE_SPECIFIC_TEXT);
        customService = Services.getService("com.hand.webext", "CustomActivityMaintenancePluginService");
    }

    /**
     * 页面加载时执行
     */
    @Override
    public void beforeLoad() throws Exception {
        // 获取POD插件实例
        CustomActivityMaintenancePlugin sessionPlugin = getSessionPlugin();
        if (sessionPlugin != null) {
            getPluginEventManager().addPluginListeners(this.getClass());
        }

        // 从faces-config.xml中获取表格配置信息
        TableConfigurator tableConfig = (TableConfigurator) FacesUtility.resolveExpression("#{customActivityMaintenanceConfigurator}");
        // 定义表格样式
        setTableConfig(tableConfig);
    }

    private CustomActivityMaintenancePlugin getSessionPlugin() {
        CustomActivityMaintenancePlugin sessionPlugin = (CustomActivityMaintenancePlugin) FacesUtility.getSessionMapValue("customActivityMaintenancePlugin");
        if (sessionPlugin == null) {
            sessionPlugin = (CustomActivityMaintenancePlugin) FacesUtility.resolveExpression("#{customActivityMaintenancePlugin}");
        }
        return sessionPlugin;
    }

    @Override
    public void processAction(ActionEvent arg0) throws AbortProcessingException {
    }

    @SuppressWarnings("unchecked")
    @Override
    public void processColumnSort(SortActionEvent sortEvent) {
        currentSortColumn = (UIColumn) sortEvent.getSource();
        performUserPreferredSorting((List<CustomActivityRuleItem>) getTableConfig().getTable().getValue());
    }

    /**
     * 表格数据排序
     */
    @SuppressWarnings("unchecked")
    private List performUserPreferredSorting(List ruleList) {
        if (ruleList == null)
            return ruleList;
        // If null, no user sort was performed in the session.
        if (currentSortColumn == null) {
            return ruleList;
        }
        String columnName = currentSortColumn.getId();
        if (columnName == null)
            return ruleList;
        if (!tableConfig.isSortableColumn(columnName))
            return ruleList;
        String sortState = currentSortColumn.getSortState();
        if (sortState == null)
            sortState = "ascending";
        String bindingProperty = tableConfig.getColumnBindingProperty(columnName);

        Collections.sort(ruleList, new TableSortComparator<CustomActivityRuleItem>(bindingProperty, TableSortComparator.SortState.valueOf(sortState)));
        return ruleList;
    }

    /**
     * 按钮Retrieve
     */
    public void retrieve(ActionEvent event) {
        // 清空异常信息缓存
        ExceptionHandler.clear(this);
        try {
            // 读取数据
            customService.retrieve(basicModel);
            // 设置按钮是否可用
            updateBtnDisabled(basicModel);
        } catch (BusinessException e) {
            ExceptionHandler.clear(this);
            ExceptionHandler.handle(e, this);
        }
    }

    /**
     * 按钮Save
     */
    public void save(ActionEvent event) {
        // 清空异常信息缓存
        ExceptionHandler.clear(this);
        try {
            // 保存数据
            customService.update(basicModel);
            // 设置按钮是否可用
            updateBtnDisabled(basicModel);
            MessageHandler.handle("Z_SY030_PLUGIN.successfulSave.LABEL", null, MessageType.SUCCESS, this);
        } catch (BusinessException e) {
            ExceptionHandler.clear(this);
            ExceptionHandler.handle(e, this);
        }
    }

    /**
     * 按钮Delete
     */
    public void delete(ActionEvent event) {
        // 清空异常信息缓存
        ExceptionHandler.clear(this);
        // 调用确认框
        openConfirmDialog(event);
        setPluginComponentFocus("activity_text");
    }

    private void delete() {
        try {
            // 删除数据
            customService.delete(basicModel);
            // 设置按钮是否可用
            updateBtnDisabled(basicModel);
            MessageHandler.handle("Z_SY030_PLUGIN.successfulDelete.LABEL", null, MessageType.SUCCESS, this);
        } catch (BusinessException e) {
            ExceptionHandler.clear(this);
            ExceptionHandler.handle(e, this);
        }
    }

    /**
     * 按钮Clear
     */
    public void clear(ActionEvent event) {
        // 清空异常信息缓存
        ExceptionHandler.clear(this);
        // 调用确认框
        openConfirmDialog(event);
        setPluginComponentFocus("activity_text");
    }

    private void clear() {
        basicModel.clear();
        // 设置按钮是否可用
        updateBtnDisabled(basicModel);
        // 定义表格样式
        setTableConfig(tableConfig);
        clearActivityText();
    }

    /**
     * 搜索帮助：设置搜索帮助传递值
     */
    public List<AttributeValue> getBrowseFieldCriteria(EditableValueHolder comp) throws BusinessException {
        String primSrchVal = (String) comp.getValue();
        if (Utils.isBlank(primSrchVal)) {
            primSrchVal = "";
        }
        ArrayList<AttributeValue> result = new ArrayList<AttributeValue>();
        result.add(new AttributeValue("activity", primSrchVal));

        return result;
    }

    /**
     * 搜索帮助：设置搜索帮助窗口大小
     */
    public BrowseDimension getDeviceDependentSize(EditableValueHolder comp) {
        BrowseDimension res = null;
        DeviceConfigurationInterface deviceConf = this.getDeviceConfiguration();
        res = getBrowseWinSize(deviceConf);
        return res;
    }

    /**
     * 搜索帮助：结束事件
     */
    public void activityBrowseCallBack(BrowseActionType actionType) {
        ExceptionHandler.clear(this);
    }

    private void updateBtnDisabled(CustomActivityMaintenanceBasicModel basicModel) {
        boolean disable = basicModel.getRuleList() != null ? false : true;
        basicModel.setBtnDisabled(disable);
    }

    private void clearActivityText() {
        // 清空字段内容
        UICommandInputText activityTxt = getInputTextById("activity_text");
        basicModel.setActivity("");
        activityTxt.setValue("");
        FacesUtility.addControlUpdate(activityTxt);
        // 设置光标
        setPluginComponentFocus("activity_text");
    }

    private void setPluginComponentFocus(String id) {
        UIComponent panel = getContainer();
        if (panel != null) {
            UIComponent comp = findComponent(panel, id);
            if (comp != null) {
                setComponentFocus(comp);
            }
        }
    }

    private UICommandInputText getInputTextById(String txtId) {
        UICommandInputText comp = (UICommandInputText) findComponent(getContainer(), txtId);
        return comp;
    }

    /**
     * 在删除或清空操作时, 弹出确认框
     */
    private void openConfirmDialog(ActionEvent event) {
        String btnId = event.getComponent().getId();
        String messageLabel = null;
        if ("delete".equals(btnId)) {
            messageLabel = "Z_SY030_PLUGIN.cfmDelete.LABEL";
        } else if ("clear".equals(btnId)) {
            messageLabel = "Z_SY030_PLUGIN.cfmClear.LABEL";
        } else {
            return;
        }
        ConfirmDialogOpenEvent dialogOpenEvent = createOpenDialogEvent(messageLabel);
        setLastOperation(btnId);
        dialogOpenEvent.setCancelButtonRendered(true);
        dialogOpenEvent.setResizable(false);
        PluginEventManagerInterface eventManager = getPluginEventManager();
        eventManager.fireEvent(dialogOpenEvent, GlobalDialogOpenListenerInterface.class, "processDialogOpen");
    }

    /**
     * 确认框结束事件
     */
    public void processConfirmDialogCallBack() {
        if ("delete".equals(getLastOperation())) {
            delete();
        } else if ("clear".equals(getLastOperation())) {
            clear();
        } else {
            return;
        }
    }

    private ConfirmDialogOpenEvent createOpenDialogEvent(String messageLabel) {
        ConfirmDialogOpenEvent dialogOpenEvent = new ConfirmDialogOpenEvent(this, "confirm.title.TEXT", messageLabel);
        dialogOpenEvent.setOkButtonDefault(true);
        dialogOpenEvent.setResizable(false);
        dialogOpenEvent.setInvokerBeanId(getBeanName());
        return dialogOpenEvent;
    }

    private BrowseDimension getBrowseWinSize(DeviceConfigurationInterface deviceConf) {
        BrowseDimension res = new BrowseDimension(15, 10);
        PodDeviceSizeEnum devSize = deviceConf.getDeviceSize();
        PodDeviceTypeEnum devType = deviceConf.getDeviceType();
        if (devType == PodDeviceTypeEnum.STANDARD) {
            switch (devSize) {
            case SMALL:
            case MEDIUM:
            case LARGE:
            case EXTRA_LARGE:
                res.setSize(38, 15);
                break;
            default:
                res.setSize(38, 15);
            }
        } else if (devType == PodDeviceTypeEnum.TOUCH) {
            switch (devSize) {
            case SMALL:
                res.setSize(60, 30);
                break;
            case MEDIUM:
                res.setSize(60, 30);
                break;
            case LARGE:
            case EXTRA_LARGE:
                res.setSize(60, 30);
            }
        } else {
            res.setSize(15, 10);
        }
        return res;
    }

    public String getTitle() {
        if (getAreaType() != PodPanelTypeEnum.POPUP) {
            return textService.getString(POPUP_TITLE_TEXT);
        }
        return "";
    }

    @Override
    public String getPopupTitle() {
        return POPUP_TITLE_TEXT;
    }

    @Override
    protected String getHelpResourceKey() {
        return HELP_RESOURCE_KEY;
    }

    public boolean isShowClose() {
        return getAreaType() != PodPanelTypeEnum.POPUP && !isDefaultPlugin();
    }

    public boolean isShowReturnToPOD() {
        return getAreaType() == PodPanelTypeEnum.POPUP;
    }

    public TableConfigurator getTableConfig() {
        return tableConfig;
    }

    public void setTableConfig(TableConfigurator tableConfig) {
        // 设置表格属性
        tableConfig.setListName(null);
        tableConfig.setAllowSelections(false);
        // 设置列ID
        String[] columnNames = { "RULE", "SETTING" };
        tableConfig.setListColumnNames(columnNames);
        // 设置排序字段顺序
        ArrayList<String> sortableCols = new ArrayList<String>();
        for (String colName : tableConfig.getListColumnNames()) {
            sortableCols.add(colName);
        }
        tableConfig.setSortableColumns(sortableCols);
        // 设置可编辑列
        ArrayList<String> editCols = new ArrayList<String>();
        editCols.add("SETTING");
        tableConfig.setEditableColumns(editCols);

        this.tableConfig = tableConfig;
    }

    public String getLastOperation() {
        return lastOperation;
    }

    public void setLastOperation(String lastOperation) {
        this.lastOperation = lastOperation;
    }

    public CustomActivityMaintenanceBasicModel getBasicModel() {
        return basicModel;
    }

    public void setBasicModel(CustomActivityMaintenanceBasicModel basicModel) {
        this.basicModel = basicModel;
    }
}
