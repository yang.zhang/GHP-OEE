package com.hand.wpmf.web.podplugin;

import java.util.ArrayList;
import java.util.List;

import com.sap.me.activity.ActivityBOHandle;
import com.sap.me.activity.ActivityConfigurationServiceInterface;
import com.sap.me.activity.ActivityFullConfiguration;
import com.sap.me.activity.ActivityOption;
import com.sap.me.common.ObjectReference;
import com.sap.me.extension.Services;
import com.sap.me.frame.Data;
import com.sap.me.frame.JNDIUtils;
import com.sap.me.frame.SystemBase;
import com.sap.me.frame.Utils;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.frame.jdbc.DynamicBatchInsertQuery;
import com.sap.me.frame.jdbc.DynamicQuery;
import com.sap.me.frame.jdbc.DynamicQueryFactory;
import com.visiprise.globalization.DateGlobalizationServiceInterface;
import com.visiprise.globalization.GlobalizationService;
import com.visiprise.globalization.KnownGlobalizationServices;

/**
 * @author Jun.Liu
 * @Description: 客制化作业维护POD页面
 * @date Dec 13, 2016 12:01:20 AM
 */
public class CustomActivityMaintenancePluginService implements CustomActivityMaintenancePluginServiceInterface {

    // API声明
    private DateGlobalizationServiceInterface dateService;
    private ActivityConfigurationServiceInterface activityConfiService;

    // DB操作
    SystemBase systemBase = null;
    // 设备自定义信息更新
    boolean isUpdated = false;

    @Override
    public List<CustomActivityMaintenanceItem> findActivitysData(CustomActivityMaintenanceRequest request) throws BusinessException {
        systemBase = new SystemBase();
        systemBase.init(JNDIUtils.DEFAULT_DATASOURCE, false);
        DynamicQuery dq = DynamicQueryFactory.newInstance();

        dq.clear();
        dq.append("SELECT ACTIVITY, DESCRIPTION");
        dq.append(" FROM ACTIVITY WHERE ACTIVITY LIKE ").appendString(request.getActivity() + "%");
        Data queryResult = systemBase.executeQuery(dq);

        List<CustomActivityMaintenanceItem> browseList = new ArrayList<CustomActivityMaintenanceItem>();
        if (queryResult != null) {
            for (Data data : queryResult) {
                CustomActivityMaintenanceItem content = new CustomActivityMaintenanceItem();
                content.setActivity(data.getString("ACTIVITY", ""));
                content.setDescription(data.getString("DESCRIPTION", ""));
                browseList.add(content);
            }
        }

        return browseList;
    }

    @Override
    public void retrieve(CustomActivityMaintenanceBasicModel basicModel) throws BusinessException {
        activityConfiService = Services.getService("com.sap.me.activity", "ActivityConfigurationService");

        String activity = basicModel.getActivity();
        String activityRef = new ActivityBOHandle(activity).getValue();

        // 读取客制化表信息
        String comments = getComments(activity);
        basicModel.setComments(comments);

        // 读取作业信息
        ObjectReference objectRef = new ObjectReference();
        objectRef.setRef(activityRef);
        ActivityFullConfiguration activityFullConfi = activityConfiService.readActivity(objectRef);

        List<CustomActivityRuleItem> rulesList = new ArrayList<CustomActivityRuleItem>();
        if (activityFullConfi != null) {
            List<ActivityOption> activityOptionsList = activityFullConfi.getActivityOptionList();
            if (!Utils.isEmpty(activityOptionsList)) {
                for (ActivityOption option : activityOptionsList) {
                    CustomActivityRuleItem content = new CustomActivityRuleItem();
                    content.setRule(option.getExecUnitOption());
                    content.setSetting(option.getSetting());
                    rulesList.add(content);
                }
            }
        }
        basicModel.setRuleList(rulesList);
    }

    @Override
    public void update(CustomActivityMaintenanceBasicModel basicModel) throws BusinessException {
        activityConfiService = Services.getService("com.sap.me.activity", "ActivityConfigurationService");

        String activity = basicModel.getActivity();
        String activityRef = new ActivityBOHandle(activity).getValue();
        List<CustomActivityRuleItem> rulesList = basicModel.getRuleList();

        // 读取作业信息
        ObjectReference objectRef = new ObjectReference();
        objectRef.setRef(activityRef);
        ActivityFullConfiguration activityFullConfi = activityConfiService.readActivity(objectRef);

        // 整理最新的Option信息
        List<ActivityOption> activityOptionsList = new ArrayList<ActivityOption>();
        int ruleCount = Utils.isEmpty(rulesList) ? 0 : rulesList.size();
        for (int i = 0; i < ruleCount; i++) {
            ActivityOption option = new ActivityOption();
            option.setExecUnitOption(rulesList.get(i).getRule());
            option.setSetting(rulesList.get(i).getSetting());
            activityOptionsList.add(option);
        }

        // 更新activityFullConfi为最新值
        activityFullConfi.setActivityOptionList(activityOptionsList);

        // 保存信息
        activityConfiService.updateActivity(activityFullConfi);

        // 保存自建表信息
        saveComments(activity, basicModel.getComments());
    }

    @Override
    public void delete(CustomActivityMaintenanceBasicModel basicModel) throws BusinessException {
        // 删除自建表信息
        deleteComments(basicModel.getActivity(), basicModel.getComments());
        basicModel.setComments("");
    }

    /**
     * 读取客制化表信息
     */
    private String getComments(String activity) {
        systemBase = new SystemBase();
        systemBase.init(JNDIUtils.DEFAULT_DATASOURCE, false);
        DynamicQuery dq = DynamicQueryFactory.newInstance();

        String activityRef = new ActivityBOHandle(activity).getValue();
        dq.clear();
        dq.append("SELECT COMMENTS FROM Z_ACTIVITY");
        dq.append(" WHERE HANDLE = ").appendString(activityRef);
        Data resultData = systemBase.executeQuery(dq);
        if (!Utils.isEmpty(resultData)) {
            return resultData.getString("COMMENTS", "", 0);
        }

        return null;
    }

    /**
     * 保存自建表信息
     */
    private void saveComments(String activity, String comments) {
        dateService = (DateGlobalizationServiceInterface) GlobalizationService.getUserService(KnownGlobalizationServices.DATE);

        systemBase = new SystemBase();
        systemBase.init(JNDIUtils.DEFAULT_DATASOURCE, false);
        DynamicBatchInsertQuery di;

        // Delete
        deleteComments(activity, comments);

        // Insert
        String activityRef = new ActivityBOHandle(activity).getValue();
        di = DynamicQueryFactory.insertBatchInstance("Z_ACTIVITY", systemBase.getDBConnection(), 100);
        di.insertColumn("HANDLE", activityRef);
        di.insertColumn("COMMENTS", comments);
        di.insertColumn("MODIFIED_DATE_TIME", dateService.createDateTime());
        systemBase.executeBatchUpdate(di);
        di.flush();
    }

    /**
     * 删除自建表信息
     */
    private void deleteComments(String activity, String comments) {
        systemBase = new SystemBase();
        systemBase.init(JNDIUtils.DEFAULT_DATASOURCE, false);
        DynamicQuery dq = DynamicQueryFactory.newInstance();

        String activityRef = new ActivityBOHandle(activity).getValue();

        // Delete
        dq.clear();
        dq.append("DELETE FROM Z_ACTIVITY WHERE HANDLE = ").appendString(activityRef);
        systemBase.executeUpdate(dq);
        dq.flush();
    }
}
