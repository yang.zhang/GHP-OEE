package com.hand.wpmf.web.podplugin;

/**
 * @author Jun.Liu
 * @Description: 页面表格信息
 * @date Dec 12, 2016 11:24:57 PM
 */
public class CustomActivityRuleItem {
    private String rule;
    private String setting;

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getSetting() {
        return setting;
    }

    public void setSetting(String setting) {
        this.setting = setting;
    }
}
