package com.hand.wpmf.web.podplugin;

/**
 * @author Jun.Liu
 * @Description: 搜索帮助传入参数
 * @date Dec 13, 2016 12:05:49 AM
 */
public class CustomActivityMaintenanceRequest {
    private String activity;

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }
}
