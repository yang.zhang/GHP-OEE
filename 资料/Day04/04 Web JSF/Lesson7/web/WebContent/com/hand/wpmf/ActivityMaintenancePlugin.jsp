<%@page pageEncoding="UTF-8"%>
<%@ page language="java"%>
<%@ taglib prefix="h" uri="http://java.sap.com/jsf/html"%>
<%@ taglib prefix="f" uri="http://java.sap.com/jsf/core"%>
<%@ taglib prefix="hs" uri="http://java.sun.com/jsf/html"%>
<%@ taglib prefix="sap" uri="http://java.sap.com/jsf/html/extended"%>
<%@ taglib prefix="i" uri="http://www.sap.com/sapme/ui/icons"%>

<f:subview id="customActivityMaintenancePluginView">
	<sap:panel id="customActivityMaintenancePlugin"
		binding="#{customActivityMaintenanceBackingBean.container}" width="100%"
		isCollapsible="false" isCollapsed="false" height="100%"
		contentAreaDesign="transparent" borderDesign="none">
		<f:attribute name="sap-delta-id" value="#{sap:toClientId('customActivityMaintenancePlugin')}" />
		<f:facet name="header">
			<h:panelGroup>
				<h:outputText id="headerTitle" value="" />
			</h:panelGroup>
		</f:facet>
		
		<f:facet name="toolbar">
			<sap:toolBar>
				<f:facet name="rightAlignedItems">
					<h:panelGroup>
						<h:commandButton id="helpButton" image="#{i:deviceDependantUrl('help')}" title="#{gapiI18nTransformer['toolTip.help.TEXT']}" />
					</h:panelGroup>
				</f:facet>
			</sap:toolBar>
		</f:facet>
		
		<sap:panelGrid id="panel_grid" cellHalign="center" cellValign="center" width="100%"
			height="100%">
			<!-- Message -->
			<sap:panelRow cellHalign="start" cellValign="top">
				<sap:panelGroup backgroundDesign="transparent" width="100%" halign="start" valign="top" colSpan="3">
					<sap:panelGrid binding="#{customActivityMaintenanceBackingBean.pluginMessageLayout}" width="100%" />
				</sap:panelGroup>
			</sap:panelRow>
			
			<sap:panelRow cellHalign="center" cellValign="top">
				<sap:panelGroup width="100%" colSpan="2">
					<sap:panelGrid id="criterialArea" width="100%" cellBackgroundDesign="transparent" cellHalign="center" cellValign="top">
						<!-- Activity and Retrieve Button-->
						<sap:panelRow cellpadding="10px 0px 0px 10px">
							<!-- Activity -->
							<sap:panelGroup halign="right" valign="middle">
								<sap:outputLabel id="activity_label" value="#{gapiI18nLabel['ACTIVITY.default.LABEL']}"
									width="6em" />
							</sap:panelGroup>
							<sap:panelGroup halign="left" valign="middle">
								<sap:outputLabel width="0.5em" />
								<sap:commandInputText id="activity_text" width="15em" submitOnChange="true" submitOnFieldHelp="true" 
								    title="#{gapiI18nTransformer['ACTIVITY.default.LABEL']}"
									value="#{customActivityMaintenancePlugin.basicModel.activity}"
									submitOnTabout="false" > 
									<f:actionListener type="com.sap.me.wpmf.browse.BrowseListener" />
		                            <f:attribute name="upperCase" 					value="true" />
		                            <f:attribute name="SERVICE_MODULE" 				value="com.hand.webext" />
		                            <f:attribute name="SERVICE_NAME" 				value="CustomActivityBrowseService" />
		                            <f:attribute name="REQUESTING_PLUGIN_NAME" 		value="customActivityMaintenancePlugin" />
		                            <f:attribute name="SEARCH_METHOD_NAME" 			value="customActivityMaintenancePlugin.getBrowseFieldCriteria" />
		                            <f:attribute name="SIZE_METHOD_NAME" 			value="customActivityMaintenancePlugin.getDeviceDependentSize" />
		                            <f:attribute name="BROWSE_CALLBACK" 			value="customActivityMaintenancePlugin.activityBrowseCallBack"/>  
		                            <f:attribute name="BROWSE_DIALOG_WIDTH" 		value="30em" />
		                            <f:attribute name="BROWSE_DIALOG_HEIGHT" 		value="20em" />	
								</sap:commandInputText>
							</sap:panelGroup>
							<!-- Button -->
							<sap:panelGroup halign="left" valign="middle">
								<sap:commandButton id="retrieve" width="11em"
									actionListener="#{customActivityMaintenancePlugin.retrieve}"
									value="#{gapiI18nTransformer['retrieve.default.BUTTON']}"
									image="#{i:deviceDependantUrl('retrieve')}"
									title="#{gapiI18nTransformer['retrieve.default.BUTTON']}">
								</sap:commandButton>
							</sap:panelGroup>
						</sap:panelRow>
						
						<!-- Comments-->
						<sap:panelRow cellpadding="10px 0px 0px 10px">
							<sap:panelGroup halign="right" valign="middle">
								<sap:outputLabel id="comments_label" value="#{gapiI18nLabel['CustomActivity.comments.LABEL']}"
									width="6em" />
							</sap:panelGroup>
							<sap:panelGroup halign="left" valign="middle">
								<sap:outputLabel width="0.5em" />
								<sap:commandInputText id="comments_text" width="15em" submitOnChange="false" submitOnFieldHelp="false" 
								    title="#{gapiI18nTransformer['CustomActivity.comments.LABEL']}"
									value="#{customActivityMaintenancePlugin.basicModel.comments}" > 
								</sap:commandInputText>
							</sap:panelGroup>
						</sap:panelRow>
					</sap:panelGrid>
				</sap:panelGroup>
			</sap:panelRow>
			
			<!-- Table -->
			<sap:panelRow cellpadding="10px 0px 0px 10px">
				<sap:panelGroup height="100%" rendered="true" halign="middle" valign="top" colSpan="3">
					<sap:dataTable
                        binding="#{customActivityMaintenanceConfigurator.table}"
                        width="100%" height="100%" rows="10"
                        value="#{customActivityMaintenancePlugin.basicModel.ruleList}" var="row"
                        first="0" id="customActivityMaintenancePluginTable">
                    </sap:dataTable>
				</sap:panelGroup>
			</sap:panelRow>
			
			<!-- Button -->
			<sap:panelRow cellpadding="10px 0px 0px 10px">
				<sap:panelGroup id="operationsPanel" halign="center" valign="middle" width="100%">
					<sap:commandButton id="save" width="15em"
						disabled="#{customActivityMaintenancePlugin.basicModel.btnDisabled}"
						actionListener="#{customActivityMaintenancePlugin.save}"
						value="#{gapiI18nTransformer['save.default.BUTTON']}"
						image="#{i:deviceDependantUrl('save')}"
						title="#{gapiI18nTransformer['save.default.BUTTON']}">
					</sap:commandButton>
					<sap:outputLabel width="0.5em" />
					<sap:commandButton id="delete" width="15em"
						disabled="#{customActivityMaintenancePlugin.basicModel.btnDisabled}"
						actionListener="#{customActivityMaintenancePlugin.delete}"
						value="#{gapiI18nTransformer['delete.default.BUTTON']}"
						image="#{i:deviceDependantUrl('delete')}"
						title="#{gapiI18nTransformer['save.default.BUTTON']}">
					</sap:commandButton>
					<sap:outputLabel width="0.5em" />
					<sap:commandButton id="clear" width="15em"
						disabled="#{customActivityMaintenancePlugin.basicModel.btnDisabled}"
						actionListener="#{customActivityMaintenancePlugin.clear}"
						value="#{gapiI18nTransformer['clear.default.BUTTON']}"
						image="#{i:deviceDependantUrl('clear')}"
						title="#{gapiI18nTransformer['clear.default.BUTTON']}">
					</sap:commandButton>
				</sap:panelGroup>
			</sap:panelRow>
		</sap:panelGrid>
	</sap:panel>
</f:subview>