package com.hand.wpmf.podplugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sap.me.common.ObjectReference;
import com.sap.me.demand.SFCBOHandle;
import com.sap.me.extension.Services;
import com.sap.me.frame.Utils;
import com.sap.me.frame.domain.BusinessException;
import com.sap.me.frame.domain.DomainServiceFactory;
import com.sap.me.frame.domain.DomainServiceInterface;
import com.sap.me.frame.service.CommonMethods;
import com.sap.me.production.ProcessLotServiceInterface;
import com.sap.me.production.RemoveMemberRequest;
import com.sap.me.production.SfcBasicData;
import com.sap.me.production.SfcStateServiceInterface;
import com.sap.me.production.domain.MemberGbo;
import com.sap.me.production.domain.ProcessLotMemberDO;
import com.sap.me.production.podclient.BasePodPlugin;
import com.sap.me.production.podclient.SfcSelection;

/**
 * 在自动化生产线中，当产品完成生产某一工序时，需自动解除产品与所使用的载体之间的绑定关系，从而使载体处于空闲状态并可重复使用，因此需客制化开发POD中“完成”操作后的自动解绑功能。
 * 
 * @author Jun.Liu
 * @Description: TODO
 * @date Jan 6, 2016 2:42:06 PM
 */
public class SFCUnbindingPlugin extends BasePodPlugin {

    private static final long serialVersionUID = 1L;

    // 日志消息ID
    private static final String MESSAGE_ID = "com.hand.wpmf.podplugin.SFCUnbindingPlugin";

    // API声明
    private DomainServiceInterface<ProcessLotMemberDO> plMemberService;
    private SfcStateServiceInterface sfcStateService;
    private ProcessLotServiceInterface plService;

    public void execute() throws Exception {
        Long startTime = System.currentTimeMillis();
        Utils.traceMsg(MESSAGE_ID + " execute() start execution ", Utils.DEBUGGING);

        // 当POD的操作&资源&SFC不为空时执行
        if (getPodSelectionModel().getOperation() == null || getPodSelectionModel().getResource() == null || getPodSelectionModel().getSfcs() == null || getPodSelectionModel().getSfcs().isEmpty()) {
            return;
        }

        initService();

        String site = CommonMethods.getSite();
        // 获取当前POD所选中的SFC清单
        List<SfcSelection> sfcList = getPodSelectionModel().getSfcs();
        int sfcCount = Utils.isEmpty(sfcList) ? 0 : sfcList.size();
        for (int i = 0; i < sfcCount; i++) {
            SfcSelection sfcSelection = sfcList.get(i);
            String inputId = sfcSelection.getInputId();
            if (Utils.isEmpty(inputId))
                continue;

            ProcessLotMemberDO plMemberDO = new ProcessLotMemberDO();
            // 当输入的内容为SFC时, 解绑
            if (isSfcOfInputId(site, inputId)) {
                SFCBOHandle sfcBOHandle = new SFCBOHandle(site, inputId);
                MemberGbo mg = new MemberGbo(sfcBOHandle.getValue());
                plMemberDO.setMemberGbo(mg);
                Collection<ProcessLotMemberDO> plMemberDOs = plMemberService.readByExample(plMemberDO);
                if (!Utils.isEmpty(plMemberDOs)) {
                    for (ProcessLotMemberDO member : plMemberDOs) {
                        String sfcRef = member.getMemberGbo().getSfcRef();
                        unbindingSFC(member.getProcessLotRef(), sfcRef);
                    }
                }
            }
        }

        Long endTime = System.currentTimeMillis();
        Utils.traceMsg(MESSAGE_ID + " execute() end execution ", Utils.DEBUGGING);
        Utils.traceMsg(MESSAGE_ID + " execute() execution time in ms " + (endTime - startTime), Utils.DEBUGGING);
    }

    private void initService() {
        plMemberService = DomainServiceFactory.getServiceByClass(ProcessLotMemberDO.class);
        sfcStateService = Services.getService("com.sap.me.production", "SfcStateService", CommonMethods.getSite());
        plService = Services.getService("com.sap.me.production", "ProcessLotService", CommonMethods.getSite());
    }

    private boolean isSfcOfInputId(String site, String inputId) throws BusinessException {
        SfcBasicData sfcBasic = getSfcBasicConfi(site, inputId);
        if (sfcBasic != null)
            return true;

        return false;
    }

    private SfcBasicData getSfcBasicConfi(String site, String sfc) throws BusinessException {
        try {
            SFCBOHandle sfcBO = new SFCBOHandle(site, sfc);
            ObjectReference objectRef = new ObjectReference(sfcBO.getValue());
            return sfcStateService.findSfcDataByRef(objectRef);
        } catch (BusinessException e) {
            return null;
        }
    }

    private void unbindingSFC(String processLotRef, String sfcRef) throws BusinessException {
        RemoveMemberRequest removeRequest = new RemoveMemberRequest();
        List<String> sfcRefList = new ArrayList<String>();
        sfcRefList.add(sfcRef);
        removeRequest.setMemberList(sfcRefList);
        removeRequest.setProcessLotRef(processLotRef);
        plService.removeMember(removeRequest);
    }
}
